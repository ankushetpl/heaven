<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
require("connection.php");
require_once('paygate.payweb3.php');


$data = json_decode($_REQUEST["obj"]);

$payment_type = $data->payment_type;

if($payment_type == 0) {

	$booking_id         = $data->booking_id;
	$serviceflag        = $data->serviceflag;
	$credit             = $data->credit;
	$offer              = $data->offer;
	$offer_id           = $data->offer_id;
	
} else {
	$creditPurchaseId   = $data->creditPurchase_id;

	// $sqlStatus = "select * from tbl_buyCredit where buyCredit_id = '".$creditPurchaseId."' ";
	// echo mysql_error();
	// $resultStatus = mysqli_query($conn,$sqlStatus);
	// $dataTrnx = mysqli_fetch_array($resultStatus);
	// $AMOUNT = ($dataTrnx['buyCredit_amount']) * 100;

}

$AMOUNT             = ($data->AMOUNT) * 100;
$points             = $data->point;

$client_id          = $data->client_id;
$PAYGATE_ID         = $data->PAYGATE_ID;
$REFERENCE          = $data->REFERENCE;
$CURRENCY           = $data->CURRENCY;
$TRANSACTION_DATE   = $data->TRANSACTION_DATE;
$LOCALE             = $data->LOCALE;
$COUNTRY            = $data->COUNTRY;
$EMAIL              = $data->EMAIL;
$PAY_METHOD         = $data->PAY_METHOD;
$PAY_METHOD_DETAIL  = $data->PAY_METHOD_DETAIL;
$NOTIFY_URL         = $data->NOTIFY_URL;
$USER1              = $data->USER1;
$USER2              = $data->USER2;
$USER3              = $data->USER3;
$VAULT              = $data->VAULT;
$VAULT_ID           = $data->VAULT_ID;
$encryption_key     = $data->encryption_key;
$RETURN_URL 		= 'http://18.222.244.130/heaven/payment/paygate_result.php';
$today              = date("Y-m-d H:i:s");

$mandatoryFields = array(
		'PAYGATE_ID'        => filter_var($PAYGATE_ID, FILTER_SANITIZE_STRING),
		'REFERENCE'         => filter_var($REFERENCE, FILTER_SANITIZE_STRING),
		'AMOUNT'            => filter_var($AMOUNT, FILTER_SANITIZE_NUMBER_INT),
		'CURRENCY'          => filter_var($CURRENCY, FILTER_SANITIZE_STRING),
		'RETURN_URL'        => filter_var($RETURN_URL, FILTER_SANITIZE_URL),
		'TRANSACTION_DATE'  => filter_var($TRANSACTION_DATE, FILTER_SANITIZE_STRING),
		'LOCALE'            => filter_var($LOCALE, FILTER_SANITIZE_STRING),
		'COUNTRY'           => filter_var($COUNTRY, FILTER_SANITIZE_STRING),
		'EMAIL'             => filter_var($EMAIL, FILTER_SANITIZE_STRING),
	);

	$optionalFields = array(
		'PAY_METHOD'        => (isset($PAY_METHOD) ? filter_var($PAY_METHOD, FILTER_SANITIZE_STRING) : ''),
		'PAY_METHOD_DETAIL' => (isset($PAY_METHOD_DETAIL) ? filter_var($PAY_METHOD_DETAIL, FILTER_SANITIZE_STRING) : ''),
		'NOTIFY_URL'        => (isset($NOTIFY_URL) ? filter_var($NOTIFY_URL, FILTER_SANITIZE_URL) : ''),
		'USER1'             => (isset($USER1) ? filter_var($USER1, FILTER_SANITIZE_URL) : ''),
		'USER2'             => (isset($USER2) ? filter_var($USER2, FILTER_SANITIZE_URL) : ''),
		'USER3'             => (isset($USER3) ? filter_var($USER3, FILTER_SANITIZE_URL) : ''),
		'VAULT'             => (isset($VAULT) ? filter_var($VAULT, FILTER_SANITIZE_NUMBER_INT) : ''),
		'VAULT_ID'          => (isset($VAULT_ID) ? filter_var($VAULT_ID, FILTER_SANITIZE_STRING) : '')
	);


	$Finaldata = array_merge($mandatoryFields, $optionalFields);

	$PayWeb3 = new PayGate_PayWeb3();
	
	$PayWeb3->setEncryptionKey($encryption_key);
	
	$PayWeb3->setInitiateRequest($Finaldata);

	$returnData = $PayWeb3->doInitiate();

	// echo $PayWeb3->lastError; die();
	if (!isset($PayWeb3->lastError)) {
		 $payRequest_id = $PayWeb3->processRequest['PAY_REQUEST_ID'];
		 $checksum = $PayWeb3->processRequest['CHECKSUM'];
	}


if($payment_type == 0) {			

	$sql = "INSERT INTO `tbl_payment` (`client_id`, `booking_id`, `booking_serviceflag`, `payment_paygate_id`, `payment_reference`, `payment_payrequest_id`, `payment_checksum`, `payment_credit`, `payment_coupon_discount`, `payment_coupon_id`, `payment_cost`, `payment_currency`, `payment_trans_date`, `payment_locale`, `payment_country`, `payment_email`, `payment_method`, `payment_method_detail`, `payment_notify_url`, `payment_user1`, `payment_user2`, `payment_user3`, `payment_vault`, `payment_vault_id`, `payment_encryption_key` ) VALUES ( '".$client_id."', '".$booking_id."', '".$serviceflag."', '".$PAYGATE_ID."', '".$REFERENCE."', '".$payRequest_id."', '".$checksum."', '".$credit."', '".$offer."', '".$offer_id."', '".$AMOUNT."', '".$CURRENCY."', '".$TRANSACTION_DATE."', '".$LOCALE."', '".$COUNTRY."', '".$EMAIL."', 'PAY-GATE', '".$PAY_METHOD_DETAIL."', '".$NOTIFY_URL."', '".$USER1."', '".$USER2."', '".$USER3."', '".$VAULT."', '".$VAULT_ID."', '".$encryption_key."' ) ";
 
	$result = mysqli_query($conn,$sql);	


} else {

	$sql = "INSERT INTO `tbl_payment` (`client_id`, `payment_type`, `creditPurchase_id`,  `payment_creditPoints`, `payment_paygate_id`, `payment_reference`, `payment_payrequest_id`, `payment_checksum`, `payment_cost`, `payment_currency`, `payment_trans_date`, `payment_locale`, `payment_country`, `payment_email`, `payment_method`, `payment_method_detail`, `payment_notify_url`, `payment_user1`, `payment_user2`, `payment_user3`, `payment_vault`, `payment_vault_id`, `payment_encryption_key` ) VALUES ( '".$client_id."', '".$payment_type."', '".$creditPurchaseId."', '".$points."', '".$PAYGATE_ID."', '".$REFERENCE."', '".$payRequest_id."', '".$checksum."', '".$AMOUNT."', '".$CURRENCY."', '".$TRANSACTION_DATE."', '".$LOCALE."', '".$COUNTRY."', '".$EMAIL."', 'PAY-GATE', '".$PAY_METHOD_DETAIL."', '".$NOTIFY_URL."', '".$USER1."', '".$USER2."', '".$USER3."', '".$VAULT."', '".$VAULT_ID."', '".$encryption_key."' ) ";

	
	$result = mysqli_query($conn,$sql);		

}			


?>
				<form role="form" class="form-horizontal text-left" method="post" name="paygate_process_form" action ="<?php echo $PayWeb3::$process_url ?>">
					
					
					<?php if(isset($PayWeb3->processRequest) || isset($PayWeb3->lastError)){ ?>
					<div class="form-group" style="display: none;">
						<label for="request">Request Result</label><br>
						<textarea class="form-control" rows="3" cols="50" id="request"><?php
							if (!isset($PayWeb3->lastError)) {
								foreach($PayWeb3->processRequest as $key => $value){
									echo <<<HTML
							{$key} = {$value}

HTML;
								}
							} else {
								echo $PayWeb3->lastError;
							} ?>
						</textarea>
					</div>
					<?php
						if (!isset($PayWeb3->lastError)) {
							 $isValid = $PayWeb3->validateChecksum($PayWeb3->initiateResponse);

							if($isValid){
								 foreach($PayWeb3->processRequest as $key => $value){
									echo <<<HTML
					<input type="hidden" name="{$key}" value="{$value}" />
HTML;
								}
							} else {
								echo 'Checksums do not match';
							}
						} ?>
					<br>
					<div class="form-group"  style="display: none;">
						<div class=" col-sm-offset-4 col-sm-4">
							<input class="btn btn-success btn-block" type="submit" name="btnSubmit" id="myButton"/>
						</div>
					</div>
					<?php } ?>
					<br>
				</form>

<script type="text/javascript">
	window.onload = function(){
        document.getElementById('myButton').click();
    };
</script>				
