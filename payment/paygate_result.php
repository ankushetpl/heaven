
<?php
	/*
	 * Once the client has completed the transaction on the PayWeb page, they will be redirected to the RETURN_URL set in the initate
	 * Here we will check the transaction status and process accordingly
	 *
	 */

	/*
	 * Sessions used here only because we can't get the PayGate ID, Transaction reference and secret key on the result page.
	 */
    session_name('paygate_payweb3_testing_sample');
    session_start();

	include_once('lib/php/global.inc.php');

	/*
	 * Include the helper PayWeb 3 class
	 */
	error_reporting(0);
	// ini_set('display_errors', 1);
	require('connection.php');
	require_once('paygate.payweb3.php');
	
	/*
	 * insert the returned data as well as the merchant specific data PAYGATE_ID and REFERENCE in array
	 */
	$data = array(
		// 'PAYGATE_ID'         => $_SESSION['pgid'],
		'PAY_REQUEST_ID'     => $_POST['PAY_REQUEST_ID'],
		'TRANSACTION_STATUS' => $_POST['TRANSACTION_STATUS'],
		// 'REFERENCE'          => $_SESSION['reference'],
		'CHECKSUM'           => $_POST['CHECKSUM']
	);

	/*
	 * initiate the PayWeb 3 helper class
	 */
	$PayWeb3 = new PayGate_PayWeb3();
	/*
	 * Set the encryption key of your PayGate PayWeb3 configuration
	 */
	// $PayWeb3->setEncryptionKey($_SESSION['key']);
	/*
	 * Check that the checksum returned matches the checksum we generate
	 */
	$isValid = $PayWeb3->validateChecksum($data)




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <title>PayWeb 3 - Result</title>
		<link rel="stylesheet" href="lib/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/css/core.css">
	</head>
	<body>
		<div class="container-fluid" style="min-width: 320px;">
			
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					
					<img alt="Heaven Sent" src="../assets/front/img/logo.png" style="height: 100px; width: 200px; display: block; margin-left: auto; margin-right: auto; background-color: white;"  />
					
				</div>
				
			</div>
			<br><br><br>
			
			<div class="container">
				<form role="form" class="form-horizontal text-left" action="query.php" method="post" name="query_paygate_form">
					<!-- <div class="form-group">
						<label for="checksumResult" class="col-sm-3 control-label">Checksum result</label>
						<p id="checksumResult" class="form-control-static"><?php echo (!$isValid ? 'The checksums do not match <i class="glyphicon glyphicon-remove text-danger"></i>' : 'Checksums match OK <i class="glyphicon glyphicon-ok text-success"></i>'); ?></p>
					</div>
		            <hr> -->
					<div class="form-group">
						<label for="PAY_REQUEST_ID" class="col-sm-3 control-label">Pay Request ID</label>
						<p id="PAY_REQUEST_ID" class="form-control-static"><?php echo $data['PAY_REQUEST_ID']; ?></p>
					</div>
					<div class="form-group">
						<label for="TRANSACTION_STATUS" class="col-sm-3 control-label">Transaction Status</label>
						<p id="TRANSACTION_STATUS" class="form-control-static"><?php echo $data['TRANSACTION_STATUS']; ?> (<?php echo $PayWeb3->getTransactionStatusDescription($data['TRANSACTION_STATUS']) ?>)</p>
					</div>
					<!-- <div class="form-group">
						<label for="CHECKSUM" class="col-sm-3 control-label">Checksum</label>
						<p id="CHECKSUM" class="form-control-static"><?php echo $data['CHECKSUM']; ?></p>
					</div> -->

					<!-- Hidden fields to post to the Query service -->
					<!-- <input type="hidden" name="PAYGATE_ID" value="<?php echo $data['PAYGATE_ID']; ?>" />
					<input type="hidden" name="PAY_REQUEST_ID" value="<?php echo $data['PAY_REQUEST_ID']; ?>" />
					<input type="hidden" name="REFERENCE" value="<?php echo $data['REFERENCE']; ?>" />
					<input type="hidden" name="encryption_key" value="<?php echo $_SESSION['key']; ?>" /> -->
					<!-- -->

					
				</form>
			</div>
        </div>
		<script type="text/javascript" src="lib/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="lib/js/bootstrap.min.js"></script>
	</body>
</html>

<?php 
$today = date('Y-m-d h:i:s');
if($data['TRANSACTION_STATUS'] == 1) {

	$today  = date("Y-m-d H:i:s");

	$sqlStatus = "select * from tbl_payment where payment_payrequest_id = '".$data['PAY_REQUEST_ID']."' ";
	echo mysql_error();
	$resultStatus = mysqli_query($conn,$sqlStatus);
	$rowCount = mysqli_num_rows($resultStatus);


			$dataTrnx = mysqli_fetch_array($resultStatus);
			$payment_type = $dataTrnx['payment_type'];
			$client_id = $dataTrnx['client_id'];
			$totalCost = ($dataTrnx['payment_cost']) / 100;
			$payment_creditPoints = $dataTrnx['payment_creditPoints'];

			if($payment_type == 0) {

				$booking_id = $dataTrnx['booking_id'];
				$serviceflag = $dataTrnx['booking_serviceflag'];
				$credit = $dataTrnx['payment_credit'];
				$offer = $dataTrnx['payment_coupon_discount'];
				$offer_id = $dataTrnx['payment_coupon_id'];

					// update payment status as success  in payment table
					$sqlPay = "update `tbl_payment` SET payment_status = '1' WHERE client_id = '".$client_id."' AND booking_id = '".$booking_id."' AND payment_payrequest_id = '".$data['PAY_REQUEST_ID']."' ";
					echo mysql_error();
					$resultPay = mysqli_query($conn,$sqlPay);


						// update payment status as success  in booking table

						if($serviceflag == 0) {

							$sqlBooking = "update `tbl_booking` SET booking_payment_status = '1', client_status = '1', booking_credit = '".$credit."', booking_coupon_discount = '".$offer."', booking_cost = '".$totalCost."'  WHERE client_id = '".$client_id."' AND booking_id = '".$booking_id."' AND booking_once_weekly = '".$serviceflag."' ";
							echo mysql_error();
							$resultBooking = mysqli_query($conn,$sqlBooking);

						} else {

							$sqlBookingWeek = "update `tbl_bookweekly` SET booking_payment_status = '1', client_status = '1', booking_credit = '".$credit."', booking_coupon_discount = '".$offer."', booking_cost = '".$totalCost."'  WHERE client_id = '".$client_id."' AND bookweekly_id = '".$booking_id."' AND booking_once_weekly = '".$serviceflag."' ";
							echo mysql_error();
							$resultBookingWeek = mysqli_query($conn,$sqlBookingWeek);

						}


						if($credit != '0' && $offer_id != '0') {

							$sqlCreditOffer = "INSERT INTO `tbl_credituse` (`client_id`, `booking_id`, `booking_once_weekly`, `credituse_promodiscount`, `credituse_creditpoints`, `created_at`, `updated_at`) VALUES ('".$client_id."', '".$booking_id."', '".$serviceflag."', '".$offer_id."', '".$credit."', '".$today."', '".$today."') ";
							$resultCreditOffer = mysqli_query($conn,$sqlCreditOffer);

							// get client's credit points
	  						$sqlCCP = "SELECT * from `tbl_clients` WHERE user_id = '".$client_id."' ";	
	  						$resultCCP = mysqli_query($conn,$sqlCCP);	
	  						$dataPoints = mysqli_fetch_array($resultCCP);
							$client_creditPoints = $dataPoints['credit_points'];

							$deductedCredit = $client_creditPoints - $credit;

							// update clients credit with deduct credit points
							$sqlPayFCP = "update `tbl_clients` SET credit_points = '".$deductedCredit."' WHERE user_id = '".$client_id."' ";
							echo mysql_error();
							$resultPayFCP = mysqli_query($conn,$sqlPayFCP);

						} else if($credit != '0') {

							$sqlCredit = "INSERT INTO `tbl_credituse` (`client_id`, `booking_id`, `booking_once_weekly`, `credituse_creditpoints`, `created_at`, `updated_at`) VALUES ('".$client_id."', '".$booking_id."', '".$serviceflag."', '".$credit."', '".$today."', '".$today."') ";
							$resultCredit = mysqli_query($conn,$sqlCredit);

							// get client's credit points
	  						$sqlCCP = "SELECT * from `tbl_clients` WHERE user_id = '".$client_id."' ";	
	  						$resultCCP = mysqli_query($conn,$sqlCCP);	
	  						$dataPoints = mysqli_fetch_array($resultCCP);
							$client_creditPoints = $dataPoints['credit_points'];

							$deductedCredit = $client_creditPoints - $credit;

							// update clients credit with deduct credit points
							$sqlPayFCP = "update `tbl_clients` SET credit_points = '".$deductedCredit."' WHERE user_id = '".$client_id."' ";
							echo mysql_error();
							$resultPayFCP = mysqli_query($conn,$sqlPayFCP);
							
						} else {

							$sqlOffer = "INSERT INTO `tbl_credituse` (`client_id`, `booking_id`, `booking_once_weekly`, `credituse_promodiscount`, `created_at`, `updated_at`) VALUES ('".$client_id."', '".$booking_id."', '".$serviceflag."', '".$offer_id."', '".$today."', '".$today."') ";
							$resultOffer = mysqli_query($conn,$sqlOffer);
						}

			} else {

				$creditPurchase_id = $dataTrnx['creditPurchase_id'];

				// update payment status as success  in payment table
				$sqlPayC = "update `tbl_payment` SET payment_status = '1' WHERE client_id = '".$client_id."' AND payment_payrequest_id = '".$data['PAY_REQUEST_ID']."' ";
				echo mysql_error();
				$resultPayC = mysqli_query($conn,$sqlPayC);

				// update payment status as success  in payment table
				$sqlPur = " INSERT INTO `tbl_creditPurchase` (`user_id`, `buyCredit_id`, `creditPoints`, `creditAmt`, `payment_status`, `created_at`, `updated_at`) VALUES ('".$client_id."', '".$creditPurchase_id."', '".$totalCost."', '".$payment_creditPoints."', '1', '".$today."', '".$today."'  ) ";
				echo mysql_error();
				$resultPur = mysqli_query($conn,$sqlPur);


					// if (mysqli_affected_rows() > 0) {


  						// get client's credit points
  						$sqlCCP = "SELECT * from `tbl_clients` WHERE user_id = '".$client_id."' ";	
  						$resultCCP = mysqli_query($conn,$sqlCCP);	
  						$dataPoints = mysqli_fetch_array($resultCCP);
						$client_creditPoints = $dataPoints['credit_points'];

						// get set points for purchase amount
						// $sqlPP = "SELECT * from `tbl_buyCredit` WHERE `buyCredit_id` = '".$creditPurchase_id."' ";	
						// $resultPP = mysqli_query($conn,$sqlPP);	
  				// 		$dataPP = mysqli_fetch_array($resultPP);
						// $buyCreditPoint = $dataPP['buyCredit_points'];


						$totalCreditPoints = $client_creditPoints + $payment_creditPoints;

						// save final total point in client account
 						$sqlPayFCP = "update `tbl_clients` SET credit_points = '".$totalCreditPoints."' WHERE user_id = '".$client_id."' ";
						echo mysql_error();
						$resultPayFCP = mysqli_query($conn,$sqlPayFCP);

						// entry in credit tables for history
						$sqlCI = "INSERT INTO `tbl_referralcredit` (`client_id`, `creditPurchase_id`, `referralcredit_type`, `referralcredit_points`, `created_at`, `updated_at` ) VALUES ( '".$client_id."', '".$creditPurchase_id."', '2', '".$payment_creditPoints."', '".$today."', '".$today."' ) ";
  						$resultCI = mysqli_query($conn,$sqlCI);	

					// }

			}

					
		 			 
		
		// } else {
		// 	echo "Invalid payment request";
		// }
} else {
	echo "Invalid transaction";
}
?>