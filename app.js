"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',
    'ngProgress',    
    'websiteApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://192.168.100.125:3000/' // local
     // 'APIURL': 'http://localhost:3000/' // local
    // 'APIURL': 'http://18.222.244.130:9000/' // testing
});

// app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
//     $urlRouterProvider.otherwise('/');    
// });

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    // paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});
