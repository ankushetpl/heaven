"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngMap',
    'ckeditor',
    'ngYoutubeEmbed',
    '720kb.datepicker',
    'ngProgress',
    'backofficeApp'    
]);

// app.constant('APPCONFIG', {
//     'APIURL': 'http://heaven.dev/api/'
// });

app.constant('APPCONFIG', {
    'APIURL': 'http://192.168.100.125:3000/' // local
    // 'APIURL': 'http://localhost:3000/' // local
    // 'APIURL': 'http://18.222.244.130:9000/' // testing    
});

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});

// RANGE
app.filter( 'range', function() {
    var filter = 
        function(arr, lower, upper) {
          for (var i = lower; i <= upper; i++) arr.push(i)
          return arr
        }
    return filter
    }
);

app.service('Map', function($q) {
    
    this.init = function() {
        var options = {
            center: new google.maps.LatLng(40.7127837, -74.00594130000002),
            zoom: 13,
            disableDefaultUI: true    
        }
        this.map = new google.maps.Map(
            document.getElementById("map"), options
        );
        this.places = new google.maps.places.PlacesService(this.map);
    }
    
    this.search = function(str) {
        var d = $q.defer();
        this.places.textSearch({query: str}, function(results, status) {
            if (status == 'OK') {
                d.resolve(results[0]);
            }
            else d.reject(status);
        });
        return d.promise;
    }
    
    this.addMarker = function(res) {
        if(this.marker) this.marker.setMap(null);
        this.marker = new google.maps.Marker({
            map: this.map,
            position: res.geometry.location,
            animation: google.maps.Animation.DROP
        });
        this.map.setCenter(res.geometry.location);
    }
    
});
// Redirect To Another Page
function ngRedirectTo($window) {
    return {
        restrict: 'A',
        link: function(scope, element, attributes) {
            element.bind('click', function (event) {
                //assign ng-Redirect-To attribute value to location
                $window.location.href = attributes.ngRedirectTo;
            });
        }
    };
}
app.directive('ngRedirectTo', ngRedirectTo);
