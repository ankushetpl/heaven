(function () {
    "use strict";
    angular
        .module('buyCreditApp')
        .service('BuyCreditService', BuyCreditService);

    BuyCreditService.$inject = ['$http', 'APPCONFIG', '$q'];

    function BuyCreditService($http, APPCONFIG, $q) {

        /* save BuyCredit */
        function saveBuyCredit(obj) {
            var URL = APPCONFIG.APIURL + 'buyCredit/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.buyCredit_id !== 'undefined' && obj.buyCredit_id !== '') {
                    requestData['buyCredit_id'] = obj.buyCredit_id;
                    URL = APPCONFIG.APIURL + 'buyCredit/edit';
                }

                if (typeof obj.buyCredit_points !== 'undefined' && obj.buyCredit_points !== '') {
                    requestData['buyCredit_points'] = obj.buyCredit_points;
                }

                if (typeof obj.buyCredit_amount !== 'undefined' && obj.buyCredit_amount !== '') {
                    requestData['buyCredit_amount'] = obj.buyCredit_amount;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveBuyCredit();

        /* to get all BuyCredit */
        function getBuyCredits(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'buyCredit';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.buyCredit_id !== 'undefined' && obj.buyCredit_id !== '') {
                    requestData['buyCredit_id'] = parseInt(obj.buyCredit_id);
                }

                if (typeof obj.buyCredit_points !== 'undefined' && obj.buyCredit_points !== '') {
                    requestData['buyCredit_points'] = obj.buyCredit_points;
                }

                if (typeof obj.buyCredit_amount !== 'undefined' && obj.buyCredit_amount !== '') {
                    requestData['buyCredit_amount'] = obj.buyCredit_amount;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getBuyCredits();

        /* to get single BuyCredit */
        function getSingleBuyCredit(buyCredit_id) {
            var URL = APPCONFIG.APIURL + 'buyCredit/view';
            var requestData = {};

            if (typeof buyCredit_id !== undefined && buyCredit_id !== '') {
                requestData['buyCredit_id'] = buyCredit_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleBuyCredit();

        /* to delete a BuyCredit from database */
        function deleteBuyCredit(buyCredit_id) {
            var URL = APPCONFIG.APIURL + 'buyCredit/delete';
            var requestData = {};

            if (typeof buyCredit_id !== undefined && buyCredit_id !== '') {
                requestData['buyCredit_id'] = buyCredit_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteBuyCredit();

        /* to change active/inactive status of BuyCredit */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'buyCredit/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.buyCredit_id != undefined && obj.buyCredit_id != "") {
                    requestData["buyCredit_id"] = obj.buyCredit_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getBuyCredits: getBuyCredits,
            getSingleBuyCredit: getSingleBuyCredit,
            saveBuyCredit: saveBuyCredit,
            deleteBuyCredit: deleteBuyCredit,
            changeStatus: changeStatus
        }

    };//END BuyCreditService()
}());
