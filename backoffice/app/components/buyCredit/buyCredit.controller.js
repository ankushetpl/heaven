(function () {
    "use strict";
    angular.module('buyCreditApp')
        .controller('BuyCreditController', BuyCreditController);

    BuyCreditController.$inject = ['$scope', '$rootScope', '$state', '$location', 'BuyCreditService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function BuyCreditController($scope, $rootScope, $state, $location, BuyCreditService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getBuyCreditList = getBuyCreditList;
        vm.getSingleBuyCredit = getSingleBuyCredit;
        vm.saveBuyCredit = saveBuyCredit;
        vm.deleteBuyCredit = deleteBuyCredit;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Buy Credit';
        vm.totalBuyCredit = 0;
        vm.buyCreditPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.buyCreditForm = { buyCredit_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.buyCredit');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleBuyCredit(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getBuyCreditList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getBuyCreditList(newPage, searchInfo);
        }//END changePage();

        /* to save buyCredit after add and edit  */
        function saveBuyCredit() {
            BuyCreditService.saveBuyCredit(vm.buyCreditForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Buy Credit');
                        $state.go('backoffice.buyCredit');
                    } else {
                        toastr.error(response.data.message, 'Buy Credit');
                    }
                } else {
                    toastr.error(response.data.message, 'Buy Credit');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Buy Credit');
            });
        }//END saveBuyCredit();

        /* to get buyCredit list */
        function getBuyCreditList(newPage, obj) {
            vm.progressbar.start();
            vm.totalBuyCredit = 0;
            vm.buyCreditList = [];
            vm.buyCreditListCheck = false;
            BuyCreditService.getBuyCredits(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.buyCreditListCheck = true;
                    if (response.data.buyCredit && response.data.buyCredit.length > 0) {
                        vm.totalBuyCredit = response.data.total;
                        vm.buyCreditList = response.data.buyCredit;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getBuyCreditList();

        /* to get single buyCredit */
        function getSingleBuyCredit(buyCredit_ids) {
            $location.hash('top');
            $anchorScroll();
            BuyCreditService.getSingleBuyCredit(buyCredit_ids).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleBuyCredit = response.data.buyCredit;
                        vm.buyCreditForm = response.data.buyCredit;
                    } else {
                        toastr.error(response.data.message, 'BuyCredit');
                        $state.go('backoffice.buyCredit');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'buyCredit');
            });
        }//END getSinglebuyCredit();

        /** to delete a buyCredit **/
        function deleteBuyCredit(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this credit point?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    BuyCreditService.deleteBuyCredit(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.buyCreditList.splice(index, 1);
                            vm.totalBuyCredit = vm.totalBuyCredit - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'BuyCredit');
                    });
                }
            });
        }//END deleteBuyCredit();

        /* to change active/inactive status of buyCredit */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { buyCredit_id: status.buyCredit_id, status: statusId };
            BuyCreditService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getBuyCreditList(1, '');
        }//END reset();               
    }

}());
