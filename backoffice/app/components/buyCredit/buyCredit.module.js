(function () {

    angular.module('buyCreditApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var buyCreditPath = 'app/components/buyCredit/';
        $stateProvider
            .state('backoffice.buyCredit', {
                url: 'buyCredit',
                views: {
                    'content@backoffice': {
                        templateUrl: buyCreditPath + 'views/index.html',
                        controller: 'BuyCreditController',
                        controllerAs: 'buyCredit'
                    }
                }
            })
            .state('backoffice.buyCredit.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: buyCreditPath + 'views/form.html',
                        controller: 'BuyCreditController',
                        controllerAs: 'buyCredit'
                    }
                }
            })
            .state('backoffice.buyCredit.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: buyCreditPath + 'views/form.html',
                        controller: 'BuyCreditController',
                        controllerAs: 'buyCredit'
                    }
                }
            })
            .state('backoffice.buyCredit.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: buyCreditPath + 'views/view.html',
                        controller: 'BuyCreditController',
                        controllerAs: 'buyCredit'
                    }
                }
            })
    }

}());