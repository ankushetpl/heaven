(function () {
    "use strict";
    angular
        .module('weekServiceApp')
        .service('WeekServiceService', WeekServiceService);

    WeekServiceService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WeekServiceService($http, APPCONFIG, $q) {

        /* save weekly service */
        function saveWeekService(obj) {
            var URL = APPCONFIG.APIURL + 'weekService/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.week_id !== 'undefined' && obj.week_id !== '') {
                    requestData['week_id'] = obj.week_id;
                    URL = APPCONFIG.APIURL + 'weekService/edit';
                }

                if (typeof obj.week_name !== 'undefined' && obj.week_name !== '') {
                    requestData['week_name'] = obj.week_name;
                }

                if (typeof obj.week_price !== 'undefined' && obj.week_price !== '') {
                    requestData['week_price'] = obj.week_price;
                }

                if (typeof obj.week_day !== 'undefined' && obj.week_day !== '') {
                    requestData['week_day'] = obj.week_day;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }                
            }
            
            return this.runHttp(URL, requestData);
        } //END saveWeekService();

        /* to get all weekly services */
        function getWeekService(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'weekService';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.week_id !== 'undefined' && obj.week_id !== '') {
                    requestData['week_id'] = parseInt(obj.week_id);
                }

                if (typeof obj.week_name !== 'undefined' && obj.week_name !== '') {
                    requestData['week_name'] = obj.week_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getWeekService();

        /* to get single weekly service */
        function getSingleWeekService(week_id) {
            var URL = APPCONFIG.APIURL + 'weekService/view';
            var requestData = {};

            if (typeof week_id !== undefined && week_id !== '') {
                requestData['week_id'] = week_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeekService();

        /* to delete a weekly service from database */
        function deleteWeekService(week_id) {
            var URL = APPCONFIG.APIURL + 'weekService/delete';
            var requestData = {};

            if (typeof week_id !== undefined && week_id !== '') {
                requestData['week_id'] = week_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteWeekService();

        /* to change active/inactive status of weekly service */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'weekService/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.week_id != undefined && obj.week_id != "") {
                    requestData["week_id"] = obj.week_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getWeekService: getWeekService,
            getSingleWeekService: getSingleWeekService,
            saveWeekService: saveWeekService,
            deleteWeekService: deleteWeekService,
            changeStatus: changeStatus
        }

    };//END WeekServiceService()
}());
