(function () {
    "use strict";
    angular.module('weekServiceApp')
        .controller('WeekServiceController', WeekServiceController);

    WeekServiceController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WeekServiceService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function WeekServiceController($scope, $rootScope, $state, $location, WeekServiceService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getWeekServiceList = getWeekServiceList;
        vm.getSingleWeekService = getSingleWeekService;
        vm.saveWeekService = saveWeekService;
        vm.deleteWeekService = deleteWeekService;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.resetForm = resetForm;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Weekly Service';
        vm.totalWeekService = 0;
        vm.weekServicePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.weekServiceForm = { week_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.weekService');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleWeekService(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getWeekServiceList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getWeekServiceList(newPage, searchInfo);
        }//END changePage();

        /* to save weekly service after add and edit  */
        function saveWeekService() {
            WeekServiceService.saveWeekService(vm.weekServiceForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Weekly Service');
                        $state.go('backoffice.weekService');
                    } else {
                        toastr.error(response.data.message, 'Weekly Service');
                    }
                } else {
                    toastr.error(response.data.message, 'Weekly Service');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Weekly Service');
            });
        }//END saveWeekService();

        /* to get weekly service list */
        function getWeekServiceList(newPage, obj) {
            vm.progressbar.start();
            vm.totalWeekService = 0;
            vm.weekServiceList = [];
            vm.weekServiceListCheck = false;
            WeekServiceService.getWeekService(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.weekServiceListCheck = true;
                    if (response.data.weekService && response.data.weekService.length > 0) {
                        vm.totalWeekService = response.data.total;
                        vm.weekServiceList = response.data.weekService;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWeekServiceList();

        /* to get single weekly service */
        function getSingleWeekService(week_id) {
            $location.hash('top');
            $anchorScroll();
            WeekServiceService.getSingleWeekService(week_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleWeekService = response.data.weekService;
                        vm.weekServiceForm = response.data.weekService;
                    } else {
                        toastr.error(response.data.message, 'Weekly Service');
                        $state.go('backoffice.weekService');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Weekly Service');
            });
        }//END getSingleWeekService();

        /** to delete a weekly service **/
        function deleteWeekService(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Weekly Service?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    WeekServiceService.deleteWeekService(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.weekServiceList.splice(index, 1);
                            vm.totalWeekService = vm.totalWeekService - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Weekly Service');
                    });
                }
            });
        }//END deleteWeekService();

        /* to change active/inactive status of weekly service */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { week_id: status.week_id, status: statusId };
            WeekServiceService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getWeekServiceList(1, '');
        }//END reset();   
        
        /* to reset all parameters */
        function resetForm(){
            vm.weekServiceForm = { week_id: '' };
        }//END resetForm();
    }

}());
