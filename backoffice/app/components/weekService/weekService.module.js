(function () {

    angular.module('weekServiceApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var weekServicePath = 'app/components/weekService/';
        $stateProvider
            .state('backoffice.weekService', {
                url: 'weekService',
                views: {
                    'content@backoffice': {
                        templateUrl: weekServicePath + 'views/index.html',
                        controller: 'WeekServiceController',
                        controllerAs: 'weekService'
                    }
                }
            })
            .state('backoffice.weekService.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: weekServicePath + 'views/form.html',
                        controller: 'WeekServiceController',
                        controllerAs: 'weekService'
                    }
                }
            })
            .state('backoffice.weekService.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: weekServicePath + 'views/form.html',
                        controller: 'WeekServiceController',
                        controllerAs: 'weekService'
                    }
                }
            })
            .state('backoffice.weekService.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: weekServicePath + 'views/view.html',
                        controller: 'WeekServiceController',
                        controllerAs: 'weekService'
                    }
                }
            });
    }

}());