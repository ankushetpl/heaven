(function () {

    angular.module('bannerSettingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var bannerSettingPath = 'app/components/bannerSetting/';
        $stateProvider
            .state('backoffice.banner', {
                url: 'banner',
                views: {
                    'content@backoffice': {
                        templateUrl: bannerSettingPath + 'views/index.html',
                        controller: 'BannerController',
                        controllerAs: 'banner'
                    }
                }
            })
            .state('backoffice.banner.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: bannerSettingPath + 'views/form.html',
                        controller: 'BannerController',
                        controllerAs: 'banner'
                    }
                }
            })
            .state('backoffice.banner.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: bannerSettingPath + 'views/form.html',
                        controller: 'BannerController',
                        controllerAs: 'banner'
                    }
                }
            })
            .state('backoffice.banner.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: bannerSettingPath + 'views/view.html',
                        controller: 'BannerController',
                        controllerAs: 'banner'
                    }
                }
            })
    }

}());