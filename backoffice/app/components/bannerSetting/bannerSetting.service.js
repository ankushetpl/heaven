(function () {
    "use strict";
    angular
        .module('bannerSettingApp')
        .service('BannerService', BannerService);

    BannerService.$inject = ['$http', 'APPCONFIG', '$q'];

    function BannerService($http, APPCONFIG, $q) {

        /* save banner */
        function saveBanner(obj) {
            var URL = APPCONFIG.APIURL + 'banner/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.banner_id !== 'undefined' && obj.banner_id !== '') {
                    requestData['banner_id'] = obj.banner_id;
                    URL = APPCONFIG.APIURL + 'banner/edit';
                }

                if (typeof obj.banner_title !== 'undefined' && obj.banner_title !== '') {
                    requestData['banner_title'] = obj.banner_title;
                }

                if (typeof obj.banner_description !== 'undefined' && obj.banner_description !== '') {
                    requestData['banner_description'] = obj.banner_description;
                }

                if (typeof obj.banner_image !== 'undefined' && obj.banner_image !== '') {
                    requestData['banner_image'] = obj.banner_image;
                }

                if (typeof obj.banner_location !== 'undefined' && obj.banner_location !== '') {
                    requestData['banner_location'] = obj.banner_location;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }               
            }
            
            return this.runHttp(URL, requestData);
        } //END saveBanner();

        /* to get all banner */
        function getBanner(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'banner';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.banner_id !== 'undefined' && obj.banner_id !== '') {
                    requestData['banner_id'] = parseInt(obj.banner_id);
                }

                if (typeof obj.banner_title !== 'undefined' && obj.banner_title !== '') {
                    requestData['banner_title'] = obj.banner_title;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.banner_location !== 'undefined' && obj.banner_location !== '') {
                    requestData['banner_location'] = obj.banner_location;
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getBanner();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        /* to get single banner */
        function getSingleBanner(banner_id) {
            var URL = APPCONFIG.APIURL + 'banner/view';
            var requestData = {};

            if (typeof banner_id !== undefined && banner_id !== '') {
                requestData['banner_id'] = banner_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleBanner();

        /* to delete a banner from database */
        function deleteBanner(banner_id) {
            var URL = APPCONFIG.APIURL + 'banner/delete';
            var requestData = {};

            if (typeof banner_id !== undefined && banner_id !== '') {
                requestData['banner_id'] = banner_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteBanner();

        /* to change active/inactive status of banner */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'banner/status';            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.banner_id != undefined && obj.banner_id != "") {
                    requestData["banner_id"] = obj.banner_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getBanner: getBanner,
            getSingleBanner: getSingleBanner,
            saveBanner: saveBanner,
            deleteBanner: deleteBanner,
            changeStatus: changeStatus,
            getImage: getImage
        }

    };//END SettingService()
}());
