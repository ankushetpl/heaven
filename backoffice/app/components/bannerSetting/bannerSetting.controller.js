(function () {
    "use strict";
    angular.module('bannerSettingApp')
        .controller('BannerController', BannerController);

    BannerController.$inject = ['$scope', '$rootScope', '$state', '$location', 'BannerService', 'toastr', 'SweetAlert', '$timeout', 'Upload', 'APPCONFIG', 'ngProgressFactory', '$anchorScroll'];

    function BannerController($scope, $rootScope, $state, $location, BannerService, toastr, SweetAlert, $timeout, Upload, APPCONFIG, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getBannerList = getBannerList;
        vm.getSingleBanner = getSingleBanner;
        vm.saveBanner = saveBanner;
        vm.deleteBanner = deleteBanner;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Banner';
        vm.imageFlag = false;
        vm.totalBanner = 0;
        vm.bannerPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.bannerForm = { banner_id: ''};
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.locationList = [{ id : 'Home', name : 'Home'}, { id : 'Other', name : 'Other'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.banner');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleBanner(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        // /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        // /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getBannerList(1, '');
        }//END sort()

        // /* call when page changes */
        function changePage(newPage, searchInfo) {
            getBannerList(newPage, searchInfo);
        }//END changePage();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    vm.bannerForm.banner_image = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to save banner after add and edit  */
        function saveBanner() {
            vm.progressbar.start();
            vm.isDisabledButton = true;
            if (vm.bannerForm.file !== '') { //check if from is valid
                
                if(vm.bannerForm.file.size > 0 && vm.bannerForm.file.name !== '' ){
                    vm.upload(vm.bannerForm.file); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        BannerService.saveBanner(vm.bannerForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    toastr.success(response.data.message, 'Banner');
                                    $state.go('backoffice.banner');
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Banner');
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Banner');
                            }
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Banner');
                        });
                    }
                }, 9000 );
            }            
        }//END saveBanner();

        /* to get banner list */
        function getBannerList(newPage, obj) {
            vm.progressbar.start();
            vm.totalBanner = 0;
            vm.bannerList = [];
            vm.bannerListCheck = false;
            BannerService.getBanner(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.banner && response.data.banner.length > 0) {
                        vm.totalBanner = response.data.total;
                        vm.bannerList = response.data.banner;
                        
                        angular.forEach(vm.bannerList, function(value, key) {
                            vm.bannerList[key].file = '';
                            var file_id = value.banner_image;
                            BannerService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.bannerList[key].file = responses.data.file.file_base_url;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        });  
                        vm.progressbar.complete();  
                        vm.bannerListCheck = true;
                    }else{
                        vm.progressbar.complete();
                        vm.bannerListCheck = true;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getBannerList();

        // /* to get single banner */
        function getSingleBanner(mail_id) {
            $location.hash('top');
            $anchorScroll();
            BannerService.getSingleBanner(mail_id).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleBanner = response.data.banner;
                        vm.bannerForm = response.data.banner;
                        vm.singleBanner.file = '';
                        vm.bannerForm.file = '';
                        var file_id = response.data.banner.banner_image;
                        BannerService.getImage(file_id).then(function (responses) {
                            if (responses.status == 200) {
                                if (responses.data.file && responses.data.file != '') {
                                    vm.singleBanner.file = responses.data.file.file_base_url;
                                    vm.bannerForm.file = responses.data.file.file_base_url;
                                }
                            }
                        }, function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    } else {
                        toastr.error(response.data.message, 'Banner');
                        $state.go('backoffice.banner');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Banner');
            });
        }//END getSingleBanner();

        // /** to delete a banner **/
        function deleteBanner(id, defaults, index) {
            
            SweetAlert.swal({
                title: "Are you sure you want to delete this banner?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    BannerService.deleteBanner(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.bannerList.splice(index, 1);
                            vm.totalBanner = vm.totalBanner - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Social');
                    });
                }
            });
        }//END deleteBanner();

        // /* to change active/inactive status of mail */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { banner_id: status.banner_id, status: statusId };
            BannerService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        // /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getBannerList(1, '');
        }//END reset();               
    }

}());
