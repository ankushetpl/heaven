(function () {
    "use strict";
    angular
        .module('faqApp')
        .service('FaqService', FaqService);

    FaqService.$inject = ['$http', 'APPCONFIG', '$q'];

    function FaqService($http, APPCONFIG, $q) {

        /* save faq */
        function saveFaq(obj) {
            var URL = APPCONFIG.APIURL + 'faq/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.faq_id !== 'undefined' && obj.faq_id !== '') {
                    requestData['faq_id'] = obj.faq_id;
                    URL = APPCONFIG.APIURL + 'faq/edit';
                }

                if (typeof obj.faq_question !== 'undefined' && obj.faq_question !== '') {
                    requestData['faq_question'] = obj.faq_question;
                }

                if (typeof obj.faq_answer !== 'undefined' && obj.faq_answer !== '') {
                    requestData['faq_answer'] = obj.faq_answer;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = obj.role_id;
                }

                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveFaq();

        /* to get all faqs */
        function getFaqs(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'faq';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }
            
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.faq_id !== 'undefined' && obj.faq_id !== '') {
                    requestData['faq_id'] = parseInt(obj.faq_id);
                }

                if (typeof obj.faq_question !== 'undefined' && obj.faq_question !== '') {
                    requestData['faq_question'] = obj.faq_question;
                }

                if (typeof obj.faq_answer !== 'undefined' && obj.faq_answer !== '') {
                    requestData['faq_answer'] = obj.faq_answer;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = parseInt(obj.role_id);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getFaqs();

        /* to get single faq */
        function getSingleFaq(faq_id) {
            var URL = APPCONFIG.APIURL + 'faq/view';
            var requestData = {};

            if (typeof faq_id !== undefined && faq_id !== '') {
                requestData['faq_id'] = faq_id;
            }

            return this.runHttp(URL, requestData);
        } //END getFaqById();

        /* to delete a faq from database */
        function deleteFaq(faq_id) {
            var URL = APPCONFIG.APIURL + 'faq/delete';
            var requestData = {};

            if (typeof faq_id !== undefined && faq_id !== '') {
                requestData['faq_id'] = faq_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteFaq();

        /* to change active/inactive status of faq */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'faq/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.faq_id != undefined && obj.faq_id != "") {
                    requestData["faq_id"] = obj.faq_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to get all role */
        function getAllRoll(pageNum) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = { pageNum }; 
            requestData['orderColumn'] = 'role_name'; 
            requestData['orderBy'] = 'ASC';          
            return this.runHttp(URL, requestData);
        }//END getAllRoll();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getFaqs: getFaqs,
            getSingleFaq: getSingleFaq,
            saveFaq: saveFaq,
            deleteFaq: deleteFaq,
            changeStatus: changeStatus,
            getAllRoll: getAllRoll
        }

    };//END FaqService()
}());
