(function () {
    "use strict";
    angular.module('faqApp')
        .controller('FaqController', FaqController);

    FaqController.$inject = ['$scope', '$rootScope', '$state', '$location', 'FaqService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function FaqController($scope, $rootScope, $state, $location, FaqService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getFaqsList = getFaqsList;
        vm.getSingleFaq = getSingleFaq;
        vm.saveFaq = saveFaq;
        vm.deleteFaq = deleteFaq;
        vm.changeStatus = changeStatus;
        vm.getAllRoll = getAllRoll;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.getAllRoll();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'FAQs';
        vm.totalFaqs = 0;
        vm.faqsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.faqForm = { faq_id: '', user_id: '1' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.faq');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleFaq(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getFaqsList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getFaqsList(newPage, searchInfo);
        }//END changePage();

        /* to save faq after add and edit  */
        function saveFaq() {
            FaqService.saveFaq(vm.faqForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'FAQ');
                        $state.go('backoffice.faq');
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                    }
                } else {
                    toastr.error(response.data.message, 'FAQ');
                }
            }, function (error) {
                toastr.error('Internal server error', 'FAQ');
            });
        }//END saveFaq();

        /* to get faq list */
        function getFaqsList(newPage, obj) {
            vm.progressbar.start();
            vm.totalFaqs = 0;
            vm.faqList = [];
            vm.faqListCheck = false;
            FaqService.getFaqs(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.faqListCheck = true;
                    if (response.data.faqs && response.data.faqs.length > 0) {
                        vm.totalFaqs = response.data.total;
                        vm.faqList = response.data.faqs;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getFaqsList();

        /* to get single faq */
        function getSingleFaq(faq_id) {
            $location.hash('top');
            $anchorScroll();
            FaqService.getSingleFaq(faq_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.faq;
                        vm.faqForm = response.data.faq;
                    } else {
                        toastr.error(response.data.message, 'FAQ');
                        $state.go('backoffice.faq');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'FAQ');
            });
        }//END getSingleFaq();

        /** to delete a faq **/
        function deleteFaq(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this FAQ?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    FaqService.deleteFaq(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.faqList.splice(index, 1);
                            vm.totalFaqs = vm.totalFaqs - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteFaq();

        /* to change active/inactive status of faq */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { faq_id: status.faq_id, status: statusId };
            FaqService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* all role type */
        function getAllRoll() {
            vm.roleList = [];
            FaqService.getAllRoll('1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllRoll()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getFaqsList(1, '');
        }//END reset();               
    }

}());
