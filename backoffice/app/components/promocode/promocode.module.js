(function () {

    angular.module('promocodeApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var promocodePath = 'app/components/promocode/';
        $stateProvider
            .state('backoffice.promocode', {
                url: 'promocode',
                views: {
                    'content@backoffice': {
                        templateUrl: promocodePath + 'views/index.html',
                        controller: 'PromocodeController',
                        controllerAs: 'promocode'
                    }
                }
            })
            .state('backoffice.promocode.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: promocodePath + 'views/form.html',
                        controller: 'PromocodeController',
                        controllerAs: 'promocode'
                    }
                }
            })
            .state('backoffice.promocode.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: promocodePath + 'views/form.html',
                        controller: 'PromocodeController',
                        controllerAs: 'promocode'
                    }
                }
            })
            .state('backoffice.promocode.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: promocodePath + 'views/view.html',
                        controller: 'PromocodeController',
                        controllerAs: 'promocode'
                    }
                }
            });
    }

}());