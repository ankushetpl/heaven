(function () {
    "use strict";
    angular
        .module('promocodeApp')
        .service('PromocodeService', PromocodeService);

    PromocodeService.$inject = ['$http', 'APPCONFIG', '$q'];

    function PromocodeService($http, APPCONFIG, $q) {

        /* save Promocode */
        function savePromocode(obj) {
            var URL = APPCONFIG.APIURL + 'promocode/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.promo_id !== 'undefined' && obj.promo_id !== '') {
                    requestData['promo_id'] = obj.promo_id;
                    URL = APPCONFIG.APIURL + 'promocode/edit';
                }

                if (typeof obj.promocode_servicetype !== 'undefined' && obj.promocode_servicetype !== '') {
                    requestData['promocode_servicetype'] = obj.promocode_servicetype;
                }

                if (typeof obj.promocode_weektype !== 'undefined' && obj.promocode_weektype !== '') {
                    requestData['promocode_weektype'] = obj.promocode_weektype;
                }

                if (typeof obj.promo_name !== 'undefined' && obj.promo_name !== '') {
                    requestData['promo_name'] = obj.promo_name;
                }

                if (typeof obj.promo_des !== 'undefined' && obj.promo_des !== '') {
                    requestData['promo_des'] = obj.promo_des;
                }

                if (typeof obj.promo_minamount !== 'undefined' && obj.promo_minamount !== '') {
                    requestData['promo_minamount'] = obj.promo_minamount;
                }

                if (typeof obj.promo_discount !== 'undefined' && obj.promo_discount !== '') {
                    requestData['promo_discount'] = obj.promo_discount;
                }

                if (typeof obj.promo_banner !== 'undefined' && obj.promo_banner !== '') {
                    requestData['promo_banner'] = obj.promo_banner;
                }

                if (typeof obj.promo_startdate !== 'undefined' && obj.promo_startdate !== '') {
                    requestData['promo_startdate'] = moment(obj.promo_startdate).format('YYYY-MM-DD');
                }

                if (typeof obj.promo_enddate !== 'undefined' && obj.promo_enddate !== '') {
                    requestData['promo_enddate'] = moment(obj.promo_enddate).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }                
            }
            
            return this.runHttp(URL, requestData);
        } //END savePromocode();

        /* to get all Promocode */
        function getPromocode(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'promocode';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.promo_id !== 'undefined' && obj.promo_id !== '') {
                    requestData['promo_id'] = parseInt(obj.promo_id);
                }

                if (typeof obj.promocode_servicetype !== 'undefined' && obj.promocode_servicetype !== '') {
                    requestData['promocode_servicetype'] = obj.promocode_servicetype;
                }

                if (typeof obj.promocode_weektype !== 'undefined' && obj.promocode_weektype !== '') {
                    requestData['promocode_weektype'] = obj.promocode_weektype;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getPromocode();

        /* to get single Promocode */
        function getSinglePromocode(promo_id) {
            var URL = APPCONFIG.APIURL + 'promocode/view';
            var requestData = {};

            if (typeof promo_id !== undefined && promo_id !== '') {
                requestData['promo_id'] = promo_id;
            }

            return this.runHttp(URL, requestData);
        } //END getPromocodeById();

        /* to delete a Promocode from database */
        function deletePromocode(promo_id) {
            var URL = APPCONFIG.APIURL + 'promocode/delete';
            var requestData = {};

            if (typeof promo_id !== undefined && promo_id !== '') {
                requestData['promo_id'] = promo_id;
            }

            return this.runHttp(URL, requestData);
        } //END deletePromocode();

        /* to change active/inactive status of Promocode */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'promocode/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.promo_id != undefined && obj.promo_id != "") {
                    requestData["promo_id"] = obj.promo_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* Get All types */
        function getAllTypes(){
            var URL = APPCONFIG.APIURL + 'rating/allTypes';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllTypes();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getPromocode: getPromocode,
            getSinglePromocode: getSinglePromocode,
            savePromocode: savePromocode,
            deletePromocode: deletePromocode,
            getAllTypes : getAllTypes,
            changeStatus: changeStatus,
            getImage: getImage
        }

    };//END PromocodeService()
}());
