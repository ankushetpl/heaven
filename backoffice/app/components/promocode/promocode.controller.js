(function () {
    "use strict";
    angular.module('promocodeApp')
        .controller('PromocodeController', PromocodeController);

    PromocodeController.$inject = ['$scope', '$rootScope', '$state', '$location', 'PromocodeService', 'toastr', 'SweetAlert', 'Upload', '$timeout', 'ngProgressFactory', 'APPCONFIG', '$anchorScroll'];

    function PromocodeController($scope, $rootScope, $state, $location, PromocodeService, toastr, SweetAlert, Upload, $timeout, ngProgressFactory, APPCONFIG, $anchorScroll) {
        var vm = this;

        vm.getPromocodeList = getPromocodeList;
        vm.getSinglePromocode = getSinglePromocode;
        vm.savePromocode = savePromocode;
        vm.getAllType = getAllType;
        vm.deletePromocode = deletePromocode;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;  
        vm.resetForm = resetForm;  
        vm.isDisabled = isDisabled;  
        
        vm.getAllType();
        
        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Promo Code';
        vm.totalPromocode = 0;
        vm.promocodePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.promocodeForm = { promo_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.weektypeList = [{ id : 0, name : 'Once'}, { id : 1, name : 'Weekly'}, { id : 2, name : 'Both'}];
        vm.imageFlag = false;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.promocode');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout(function(){
                    vm.getSinglePromocode(path[3]);
                }, 300);                
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getPromocodeList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getPromocodeList(newPage, searchInfo);
        }//END changePage();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status == 1) {
                    vm.promocodeForm.promo_banner = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to save promo code after add and edit  */
        function savePromocode() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            if (typeof vm.promocodeForm.file !== 'undefined' && vm.promocodeForm.file !== '') {
                if(vm.promocodeForm.file.size > 0 && vm.promocodeForm.file.name != '' ){
                    vm.upload(vm.promocodeForm.file); //call upload function   
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){ 
                        PromocodeService.savePromocode(vm.promocodeForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    toastr.success(response.data.message, 'Promo Code');
                                    $state.go('backoffice.promocode');
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Promo Code');
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Promo Code');
                            }
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Promo Code');
                        });
                    }
                }, 9000 );
            }else{
                PromocodeService.savePromocode(vm.promocodeForm).then(function (response) {
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            vm.progressbar.complete();
                            toastr.success(response.data.message, 'Promo Code');
                            $state.go('backoffice.promocode');
                        } else {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error(response.data.message, 'Promo Code');
                        }
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Promo Code');
                    }
                }, function (error) {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error('Internal server error', 'Promo Code');
                });
            }
            
        }//END savePromocode();

        /* to get Promocode list */
        function getPromocodeList(newPage, obj) {
            vm.progressbar.start();
            vm.totalPromocode = 0;
            vm.promocodeList = [];
            vm.promocodeListCheck = false;
            PromocodeService.getPromocode(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.promocodeListCheck = true;
                    if (response.data.promocode && response.data.promocode.length > 0) {
                        vm.totalPromocode = response.data.total;
                        vm.promocodeList = response.data.promocode;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getPromocodeList();

        /* to get single Promocode */
        function getSinglePromocode(promo_id) {
            $location.hash('top');
            $anchorScroll();
            PromocodeService.getSinglePromocode(promo_id).then(function (response) {   
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singlePromocode = response.data.promocode;
                        vm.promocodeForm = response.data.promocode;
                        var file_id = response.data.promocode.promo_banner;
                        
                        if( file_id != 0 ){
                            vm.promocodeForm.file = '';
                            vm.singlePromocode.file = '';
                            PromocodeService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.promocodeForm.file = responses.data.file.file_base_url;
                                        vm.singlePromocode.file = responses.data.file.file_base_url;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }                        
                    } else {
                        toastr.error(response.data.message, 'Promo Code');
                        $state.go('backoffice.promocode');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Promo Code');
            });
        }//END getSinglePromocode();

        /** to delete a Promocode **/
        function deletePromocode(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Promo code?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    PromocodeService.deletePromocode(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.promocodeList.splice(index, 1);
                            vm.totalPromocode = vm.totalPromocode - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Promocode');
                    });
                }
            });
        }//END deletePromocode();

        /* to change active/inactive status of Promocode */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { promo_id: status.promo_id, status: statusId };
            PromocodeService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus();

        /* to get service type */
        function getAllType(){
            PromocodeService.getAllTypes().then(function (response) {
                if (response.status == 200) {
                    if (response.data.type && response.data.type.length > 0) {
                        vm.typeList = response.data.type; 
                    }
                } else {
                    toastr.error(response.data.message, 'Promo Code');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Promo Code');
            });
        }//END getAllType();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getPromocodeList(1, '');
        }//END reset();      
        
        /* to reset all parameters  */
        function resetForm() {
            vm.promocodeForm = { promo_id: '' }; 
        }//END resetForm();
    }

}());
