(function () {

    angular.module('fileUploadApp', ['ngFileUpload']).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var fileUploadPath = 'app/components/fileUpload/';
        $stateProvider
            .state('backoffice.fileUpload', {
                url: 'fileUpload',
                views: {
                    'content@backoffice': {
                        templateUrl: fileUploadPath + 'views/index.html',
                        controller: 'FileUploadController',
                        controllerAs: 'fileUpload'
                    }
                }
            })
            .state('backoffice.fileUpload.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: fileUploadPath + 'views/form.html',
                        controller: 'FileUploadController',
                        controllerAs: 'fileUpload'
                    }
                }
            });            
    }

}());