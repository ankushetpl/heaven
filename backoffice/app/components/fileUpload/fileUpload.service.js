(function () {
    "use strict";
    angular
        .module('fileUploadApp')
        .service('FileUploadService', FileUploadService);

    FileUploadService.$inject = ['$http', 'APPCONFIG', '$q'];

    function FileUploadService($http, APPCONFIG, $q) {

        /* to get all file */
        function getAllFiles(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'files';
            var requestData = {};
            
            return this.runHttp(URL, requestData);
        }//END getAllFiles();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAllFiles: getAllFiles            
        }

    };//END FileUploadService()
}());
