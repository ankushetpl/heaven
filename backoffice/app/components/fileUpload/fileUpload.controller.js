(function () {
    "use strict";
    angular.module('fileUploadApp')
        .controller('FileUploadController', FileUploadController);

    FileUploadController.$inject = ['$scope', '$rootScope', '$state', 'FileUploadService', 'toastr', 'Upload', 'APPCONFIG', 'ngProgressFactory'];

    function FileUploadController($scope, $rootScope, $state, FileUploadService, toastr, Upload, APPCONFIG, ngProgressFactory) {
        var vm = this;
        vm.getAllFilesList = getAllFilesList;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'All Files';
        vm.title = 'Add New';
        vm.fileUploadList = [];

        function getAllFilesList() {
            vm.progressbar.start();
            vm.fileUploadList = [];
            FileUploadService.getAllFiles().then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.files && response.data.files.length > 0) {
                        vm.fileUploadList = response.data.files;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }

        vm.submit = function () { //function to call on form submit
            if (vm.upload_form.file.$valid && vm.file) { //check if from is valid
                vm.upload(vm.file); //call upload function
            }
        };

        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    toastr.success(resp.data.message, 'Success');
                    $state.go('backoffice.fileUpload');
                } else {
                    toastr.error(resp.data.message, 'Error');
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };
    }

}());