(function () {
    "use strict";
    angular
        .module('paymentUnsuccessApp')
        .service('PaymentUnsuccessService', PaymentUnsuccessService);

    PaymentUnsuccessService.$inject = ['$http', 'APPCONFIG', '$q'];

    function PaymentUnsuccessService($http, APPCONFIG, $q) {

        /* to get all faqs */
        function getSuccessPayments(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = parseInt(obj.booking_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }

            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getFaqs();

        /* to get single payment */
        function getSinglePayment(booking_id) {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/View';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSinglePaymentWeek();

        /* to delete a payment from database */
        function deletePayment(booking_id) {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/delete';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        } //END deletePaymentWeek();


        /* to get all faqs */
        function getSuccessPaymentsWeek(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/weekly';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.bookweekly_id !== 'undefined' && obj.bookweekly_id !== '') {
                    requestData['bookweekly_id'] = parseInt(obj.bookweekly_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getFaqs();

        /* to get single payment */
        function getSinglePaymentWeek(bookweekly_id) {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/weeklyView';
            var requestData = {};

            if (typeof bookweekly_id !== undefined && bookweekly_id !== '') {
                requestData['bookweekly_id'] = bookweekly_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSinglePaymentWeek();

        /* to delete a payment from database */
        function deletePaymentWeek(bookweekly_id) {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/weeklyDelete';
            var requestData = {};

            if (typeof bookweekly_id !== undefined && bookweekly_id !== '') {
                requestData['bookweekly_id'] = bookweekly_id;
            }

            return this.runHttp(URL, requestData);
        } //END deletePaymentWeek();        


        /* to get all data booking for CSV */
        function getOnceUnsuccessPayDataCSV() {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/unsuccessOnceCSV';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getOnceBookingDataCSV();        


        /* to get all data booking for CSV */
        function getWeekUnsuccessPayDataCSV() {
            var URL = APPCONFIG.APIURL + 'paymentUnsuccess/unsuccessWeeklyCSV';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getWeekBookingDataCSV();                


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getSuccessPayments: getSuccessPayments,
            getSinglePayment: getSinglePayment,
            deletePayment: deletePayment,
            getSuccessPaymentsWeek: getSuccessPaymentsWeek,
            getSinglePaymentWeek: getSinglePaymentWeek,
            deletePaymentWeek: deletePaymentWeek,
            getOnceUnsuccessPayDataCSV : getOnceUnsuccessPayDataCSV,
            getWeekUnsuccessPayDataCSV : getWeekUnsuccessPayDataCSV
        }

    };
}());
