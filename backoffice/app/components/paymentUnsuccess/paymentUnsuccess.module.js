(function () {

    angular.module('paymentUnsuccessApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var paymentUnsuccessPath = 'app/components/paymentUnsuccess/';
        $stateProvider
            .state('backoffice.paymentUnsuccess', {
                url: 'payUnsuccess',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentUnsuccessPath + 'views/index.html',
                        controller: 'PaymentUnsuccessController',
                        controllerAs: 'paymentUnsuccess'
                    }
                }
            })
            .state('backoffice.paymentUnsuccess.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentUnsuccessPath + 'views/view.html',
                        controller: 'PaymentUnsuccessController',
                        controllerAs: 'paymentUnsuccess'
                    }
                }
            })
            .state('backoffice.paymentUnsuccess.weekly', {
                url: '/weekly',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentUnsuccessPath + 'views/weekly.html',
                        controller: 'PaymentUnsuccessWeekController',
                        controllerAs: 'paymentUnsuccessWeek'
                    }
                }
            })
            .state('backoffice.paymentUnsuccess.weeklyView', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentUnsuccessPath + 'views/weeklyView.html',
                        controller: 'PaymentUnsuccessWeekController',
                        controllerAs: 'paymentUnsuccessWeek'
                    }
                }
            })
    }

}());