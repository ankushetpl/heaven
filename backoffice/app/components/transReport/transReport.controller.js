(function () {
    "use strict";
    angular.module('transReportApp')
        .controller('TransReportController', TransReportController);

    TransReportController.$inject = ['$scope', '$rootScope', '$state', '$location', 'TransReportService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function TransReportController($scope, $rootScope, $state, $location, TransReportService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getTranxList = getTranxList;
        vm.getSingleTranx = getSingleTranx;
        vm.getTranxCSV   = getTranxCSV;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Transactions';
        vm.totalTranx = 0;
        vm.tranxPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};

        vm.statusList = { '0': 'Booking', '1': 'Credit Purchase' };
        
        vm.getTranxList();

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.transReport');
                return false;
            } else {
                vm.progressbar.start();
                vm.getSingleTranx(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getTranxList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getTranxList(newPage, searchInfo);
        }//END changePage();


        /* to get Tranx list */
        function getTranxList(newPage, obj) {
            vm.progressbar.start();
            vm.totalTranx = 0;
            vm.tranxList = [];
            vm.tranxListCheck = false;
            TransReportService.getTransactions(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.tranxListCheck = true;
                    if (response.data.tranx && response.data.tranx.length > 0) {
                        vm.totalTranx = response.data.total;
                        vm.tranxList = response.data.tranx;
                        console.log(vm.tranxList);
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getTranxList();

        /* to get single Tranx */
        function getSingleTranx(payment_id) {
            $location.hash('top');
            $anchorScroll();
            TransReportService.getSingleTranx(payment_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleTranx = response.data.tranx;
                    } else {
                        toastr.error(response.data.message, 'Transaction Report');
                        $state.go('backoffice.transReport');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Transaction Report');
            });
        }//END getSingleTranx();

        /* to get all data in CSV format */
        function getTranxCSV() {
            vm.progressbar.start();
            TransReportService.getAllTranxDataCSV().then(function (response) {

                if (response.status == 200) {
                    if (response.data.tranxData && response.data.tranxData.length > 0) {
                        vm.list = response.data.tranxData;
                        // delete vm.list[0].booking_service_type;
                        var csv = Object.keys(vm.list[0]);
                        var headings = csv.toString();
                    }
                }

                var csv = headings;
                    csv += "\n";
                angular.forEach(vm.list, function(value, key) {
                    var array = []; 
                    array.push([value.payment_id, value.payment_type, value.payment_reference, value.payment_credit, value.payment_coupon_discount, value.payment_cost, value.payment_currency, value.payment_country, value.payment_method, value.payment_by, value.payment_trans_date]);
                    csv += array;
                    csv += "\n";
                });
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'Transaction Report.csv';
                hiddenElement.click();
                vm.progressbar.complete();

            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSingle();            


        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getTranxList(1, '');
        }//END reset();               
    }

}());
