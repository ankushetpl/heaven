(function () {
    "use strict";
    angular
        .module('transReportApp')
        .service('TransReportService', TransReportService);

    TransReportService.$inject = ['$http', 'APPCONFIG', '$q'];

    function TransReportService($http, APPCONFIG, $q) {
 
        /* to get all Transactions */
        function getTransactions(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'transReport';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.payment_id !== 'undefined' && obj.payment_id !== '') {
                    requestData['payment_id'] = parseInt(obj.payment_id);
                }

                if (typeof obj.payment_reference !== 'undefined' && obj.payment_reference !== '') {
                    requestData['payment_reference'] = obj.payment_reference;
                }

                if (typeof obj.payment_type !== 'undefined' && obj.payment_type !== '') {
                    requestData['payment_type'] = obj.payment_type;
                }

                if (typeof obj.payment_trans_date !== 'undefined' && obj.payment_trans_date !== '') {
                    requestData['payment_trans_date'] = moment(obj.payment_trans_date).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getTransactions();

        /* to get single Transactions */
        function getSingleTranx(payment_id) {
            var URL = APPCONFIG.APIURL + 'transReport/view';
            var requestData = {};

            if (typeof payment_id !== undefined && payment_id !== '') {
                requestData['payment_id'] = payment_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleTranx();

        /* to get all data booking for CSV */
        function getAllTranxDataCSV() {
            var URL = APPCONFIG.APIURL + 'transReport/allTranxData';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getAllTranxDataCSV();                

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getTransactions: getTransactions,
            getSingleTranx: getSingleTranx,
            getAllTranxDataCSV : getAllTranxDataCSV
        }

    };//END TransactionsService()
}());
