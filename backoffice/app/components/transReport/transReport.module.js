(function () {

    angular.module('transReportApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var transReportPath = 'app/components/transReport/';
        $stateProvider
            .state('backoffice.transReport', {
                url: 'transReport',
                views: {
                    'content@backoffice': {
                        templateUrl: transReportPath + 'views/index.html',
                        controller: 'TransReportController',
                        controllerAs: 'transReport'
                    }
                }
            })
            .state('backoffice.transReport.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: transReportPath + 'views/view.html',
                        controller: 'TransReportController',
                        controllerAs: 'transReport'
                    }
                }
            });
    }

}());