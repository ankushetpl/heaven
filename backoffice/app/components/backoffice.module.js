(function () {
    'use strict';
    var adminApp = angular.module('backofficeApp', [
        'authApp',
        'dashboardApp',
        'ratingApp',
        'roleApp',
        'cafeUserApp',
        'faqApp',
        'workerUserApp',
        'assessorsUserApp',
        'clientUserApp',
        'adminUserApp',
        'leaveApp',
        'tutorialApp',
        'checklistApp',
        'skillsApp',
        'fileUploadApp',
        'suburbsApp',
        'pageApp',
        'tasklistApp',
        'tasklistClientApp',
        'feedbackApp',
        'claimCategoryApp',
        'claimApp',
        'serviceTypeApp',
        'weekServiceApp',
        'cleaningAreaApp',
        'amenitiesApp',
        'promocodeApp',
        'suburbApp',
        'emailApp',
        'todayBookingApp',
        'allBookingApp',
        'paymentSuccessApp',
        'paymentUnsuccessApp',
        'paymentCancelApp',
        'paymentPendingApp',
        'settingApp',
        'socialSettingApp',
        'emailSettingApp',
        'bannerSettingApp',
        // 'modulesApp',
        // 'subAdminUserApp',
        'leaveManageApp',
        'notificationApp',
        'suspensionApp',
        'earlyPayApp',
        'supportApp',
        'buyCreditApp',
        'transReportApp',
        'walletTopUpApp',
        'paymentRefundApp'
    ]);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        return authServices.checkValidUser(true);
    } //END authenticateUser()

    adminApp.config(funConfig);
    adminApp.run(funRun);

    adminApp.component("leftSidebarComponent", {
        templateUrl: 'app/layouts/left-sidebar.html',
        controller: 'BackofficeController',
        controllerAs: 'siderbar'
    });

    adminApp.component("headerComponent", {
        templateUrl: 'app/layouts/header.html',
        controller: 'DashboardController',
        controllerAs: 'header'
    });

    adminApp.component("footerComponent", {
        templateUrl: 'app/layouts/footer.html'
        // controller: 'BackofficeController',
        // controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('backoffice', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'app/layouts/layout.html'
                    },
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('backoffice.dashboard', {
                url: 'dashboard',
                views: {
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('backoffice.profile', {
                url: 'profile',
                views: {
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/profile.html',
                        controller: 'DashboardController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('backoffice.changePassword', {
                url: 'change-password',
                views: {
                    'content@backoffice': {
                        templateUrl: 'app/components/dashboard/change-password.html',
                        controller: 'BackofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            });
    }

    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions', 'authServices'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions, authServices) {
        $rootScope.isLogin = false;

        $rootScope.$on('auth:login:success', function (event, data) {
            $state.go('backoffice.dashboard');
        }); // Event fire after login successfully

        $rootScope.$on('auth:access:denied', function (event, data) {
            $state.go('auth.login');
        }); //Event fire after check access denied for user

        $rootScope.$on('auth:login:required', function (event, data) {
            $state.go('auth.login');
        }); //Event fire after logout

        // $transitions.onStart({ to: '**' }, function ($transition$) {
        //     $rootScope.showBreadcrumb = true;
        //     authServices.checkValidUser(true);
        //     //console.log($rootScope.user);
        //     //console.log($location.$$path);

        // });
    }

}());