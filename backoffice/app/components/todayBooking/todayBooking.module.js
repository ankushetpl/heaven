(function () {

    angular.module('todayBookingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var todayBookingPath = 'app/components/todayBooking/';
        $stateProvider
            .state('backoffice.todayBooking', {
                url: 'todayBooking',
                views: {
                    'content@backoffice': {
                        templateUrl: todayBookingPath + 'views/index.html',
                        controller: 'TodayBookingController',
                        controllerAs: 'todayBooking'
                    }
                }
            })
            .state('backoffice.todayBooking.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: todayBookingPath + 'views/view.html',
                        controller: 'TodayBookingController',
                        controllerAs: 'todayBooking'
                    }
                }
            })
    }

}());