(function () {
    "use strict";
    angular.module('todayBookingApp')
        .controller('TodayBookingController', TodayBookingController);

    TodayBookingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'TodayBookingService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function TodayBookingController($scope, $rootScope, $state, $location, TodayBookingService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getList = getList;
        vm.getSingle = getSingle;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;

        vm.getList();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Todays Booking';
        vm.total = 0;
        vm.PerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.todayBooking');
                return false;
            } else {
                vm.progressbar.start();
                vm.getSingle(path[3]);
            }
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getList(newPage, searchInfo);
        }//END changePage();

        /* to get all list */
        function getList(newPage, obj) {
            vm.progressbar.start();
            vm.total = 0;
            vm.list = [];
            vm.listCheck = false;
            TodayBookingService.getTodayBooking(newPage, obj, vm.orderInfo).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.listCheck = true;
                    if (response.data.todayBooking && response.data.todayBooking.length > 0) {
                        vm.total = response.data.total;
                        vm.list = response.data.todayBooking;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getList();

        /* to get single booking */
        function getSingle(booking_id) {
            $location.hash('top');
            $anchorScroll();
            TodayBookingService.getSingleBooking(booking_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.single = response.data.todayBooking;
                    } else {
                        toastr.error(response.data.message, 'TodayBooking');
                        $state.go('backoffice.todayBooking');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'TodayBooking');
            });
        }//END getSingle();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getList(1, '');
        }//END reset();               
    }

}());
