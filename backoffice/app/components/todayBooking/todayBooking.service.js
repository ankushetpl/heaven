(function () {
    "use strict";
    angular
        .module('todayBookingApp')
        .service('TodayBookingService', TodayBookingService);

    TodayBookingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function TodayBookingService($http, APPCONFIG, $q) {

        /* to get all  */
        function getTodayBooking(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'todayBooking';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }
            
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = parseInt(obj.booking_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }
                
                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }

                
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getTodayBooking();

        /* to get single booking */
        function getSingleBooking(booking_id) {
            var URL = APPCONFIG.APIURL + 'todayBooking/view';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleBooking();


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getTodayBooking: getTodayBooking,
            getSingleBooking: getSingleBooking
        }

    };//END TodayBookingService()
}());
