(function () {

    angular.module('supportApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var supportPath = 'app/components/support/';
        $stateProvider
            .state('backoffice.support', {
                url: 'support',
                views: {
                    'content@backoffice': {
                        templateUrl: supportPath + 'views/index.html',
                        controller: 'SupportController',
                        controllerAs: 'support'
                    }
                }
            })
            .state('backoffice.support.reply', {
                url: '/reply/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: supportPath + 'views/form.html',
                        controller: 'SupportController',
                        controllerAs: 'support'
                    }
                }
            })
            .state('backoffice.support.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: supportPath + 'views/view.html',
                        controller: 'SupportController',
                        controllerAs: 'support'
                    }
                }
            });
    }

}());