(function () {
    "use strict";
    angular.module('supportApp')
        .controller('SupportController', SupportController);

    SupportController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SupportService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function SupportController($scope, $rootScope, $state, $location, SupportService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getRecords = getRecords;
        vm.getSingleRecord = getSingleRecord;
        vm.saveRecord = saveRecord;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Support Management';
        vm.totalRecord = 0;
        vm.RecordPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.supportForm = { contactUs_id: '' };
        
        vm.getRecords();

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "reply" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.support');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Reply';
                $timeout( function(){
                    vm.progressbar.start();
                    vm.getSingleRecord(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRecords(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRecords(newPage, searchInfo);
        }//END changePage();

        /* to save Support mail after add and edit  */
        function saveRecord() {
            
            SupportService.saveRecord(vm.supportForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Support Management');
                        $state.go('backoffice.support');
                    } else {
                        toastr.error(response.data.message, 'Support Management');
                    }
                } else {
                    toastr.error(response.data.message, 'Support Management');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Support Management');
            });
        }//END saveRecord();

        /* to get list */
        function getRecords(newPage, obj) {
            vm.progressbar.start();
            vm.totalRecord = 0;
            vm.list = [];
            vm.listCheck = false;
            SupportService.getRecords(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.listCheck = true;
                    if (response.data.supportMail && response.data.supportMail.length > 0) {
                        vm.totalRecord = response.data.total;
                        vm.list = response.data.supportMail;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRecords();

        /* to get single Record */
        function getSingleRecord(contactUs_id) {
            $location.hash('top');
            $anchorScroll();
            SupportService.getSingleRecord(contactUs_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleData = response.data.supportMail;
                        vm.supportForm = response.data.supportMail;
                    } else {
                        toastr.error(response.data.message, 'Support Management');
                        $state.go('backoffice.support');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Support');
            });
        }//END getSingleRecord();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRecords(1, '');
        }//END reset();               
    }

}());
