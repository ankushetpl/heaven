(function () {
    "use strict";
    angular
        .module('supportApp')
        .service('SupportService', SupportService);

    SupportService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SupportService($http, APPCONFIG, $q) {

        /* save Record */
        function saveRecord(obj) {
            var URL = APPCONFIG.APIURL + 'support/edit';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.contactUs_id !== 'undefined' && obj.contactUs_id !== '') {
                    requestData['contactUs_id'] = obj.contactUs_id;
                }

                if (typeof obj.contactUs_replyMsg !== 'undefined' && obj.contactUs_replyMsg !== '') {
                    requestData['contactUs_replyMsg'] = obj.contactUs_replyMsg;
                }

            }
            
            return this.runHttp(URL, requestData);
        } //END saveRecord();

        /* to get all records */
        function getRecords(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'support';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }
            
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.contactUs_id !== 'undefined' && obj.contactUs_id !== '') {
                    requestData['contactUs_id'] = parseInt(obj.contactUs_id);
                }

                if (typeof obj.contactUs_from !== 'undefined' && obj.contactUs_from !== '') {
                    requestData['contactUs_from'] = obj.contactUs_from;
                }

                if (typeof obj.contactUs_type !== 'undefined' && obj.contactUs_type !== '') {
                    requestData['contactUs_type'] = obj.contactUs_type;
                }

                if (typeof obj.contactUs_subject !== 'undefined' && obj.contactUs_subject !== '') {
                    requestData['contactUs_subject'] = obj.contactUs_subject;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getRecords();

        /* to get single record */
        function getSingleRecord(contactUs_id) {
            var URL = APPCONFIG.APIURL + 'support/view';
            var requestData = {};

            if (typeof contactUs_id !== undefined && contactUs_id !== '') {
                requestData['contactUs_id'] = contactUs_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleRecord();


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getRecords: getRecords,
            getSingleRecord: getSingleRecord,
            saveRecord: saveRecord
        }

    };//END SupportService()
}());
