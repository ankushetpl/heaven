(function () {
    "use strict";
    angular.module('skillsApp')
        .controller('SkillsController', SkillsController);

    SkillsController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SkillsService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function SkillsController($scope, $rootScope, $state, $location, SkillsService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getSkillsList = getSkillsList;
        vm.getSingleSkill = getSingleSkill;
        vm.saveSkills = saveSkills;
        vm.deleteSkills = deleteSkills;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.getAllMainCatSkills = getAllMainCatSkills;
               
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.getAllMainCatSkills();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Skills';
        vm.totalSkills = 0;
        vm.skillsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.catFlag  = false;
        vm.catEditFlag = false;
        vm.title = 'Add New';
        vm.skillsForm = { skills_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.skills');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleSkill(path[3]);
            }
        } else {
            if(path[2] == "subedit" || path[2] == "subview") {
                vm.progressbar.start();
                vm.catEditFlag = true;
                vm.catFlag = true;
                vm.title = 'Edit Sub';
                vm.getSingleSkill(path[3]);
            } else if(path[2] == "subcreate") {
                vm.catFlag = true;
            }
        }

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getSkillsList(1, '');
        }//END sort()

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getSkillsList(newPage, searchInfo);
        }//END changePage();

        /* to save saveSkills after add and edit  */
        function saveSkills() {
            SkillsService.saveSkills(vm.skillsForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Skills');
                        $state.go('backoffice.skills');
                    } else {
                        toastr.error(response.data.message, 'Skills');
                    }
                } else {
                    toastr.error(response.data.message, 'Skills');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Skills');
            });
        }//END saveSkills();

        /* to get getSkillsList list */
        function getSkillsList(newPage, obj) {
            vm.progressbar.start();
            vm.totalSkills = 0;
            vm.skillsList = [];
            vm.skillsListCheck = false;
            SkillsService.getSkills(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.skillsListCheck = true;
                    if (response.data.skills && response.data.skills.length > 0) {
                        vm.totalSkills = response.data.total;
                        vm.skillsList = response.data.skills;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSkillsList();

        /* to get getSingleSkill  */
        function getSingleSkill(skills_id) {
            $location.hash('top');
            $anchorScroll();
            SkillsService.getSingleSkill(skills_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleSkill = response.data.skills;
                        vm.skillsForm = response.data.skills;
                        if(response.data.skills.parent != 0){
                            var parent = response.data.skills.parent;
                            SkillsService.getCategoryParent(parent).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.data && responses.data.data.length > 0) {
                                        vm.singleSkill.parent = responses.data.data.skills_name;
                                        vm.singleSkill.cat_id = parent;                        
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }
                        
                    } else {
                        toastr.error(response.data.message, 'Skills');
                        $state.go('backoffice.skills');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Skills');
            });
        }//END getSingleSkill();

        /** to delete a skills **/
        function deleteSkills(item, index) {
            vm.flag = false;
            
            if(item.parent == 0 ){
                var parent = item.skills_id;
                SkillsService.checkData(parent).then(function (responses) {
                    if (responses.status == 200) {
                        if (responses.data.data && responses.data.data.length > 0) {
                            SweetAlert.swal("Error!", "Sorry! this skill can't be deleted. Please delete sub skill first!", "error");
                        }else{
                            vm.flag = true;
                        }
                    }
                });
                                  
            }else{
                vm.flag = true;                                    
            }

            $timeout( function(){    
                if(vm.flag == true){
                    SweetAlert.swal({
                        title: "Are you sure you want to delete this Skill?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        html: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            SkillsService.deleteSkills(item.skills_id).then(function (response) {
                                if (response.data.status == 1) {
                                    SweetAlert.swal("Deleted!", response.data.message, "success");
                                    vm.skillsList.splice(index, 1);
                                    vm.totalSkills = vm.totalSkills - 1; 
                                } else {
                                    SweetAlert.swal("Deleted!", response.data.message, "error");
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Skills');
                            });
                        }
                    });
                }
            }, 500);
        }//END deleteSkills();

        /* to change active/inactive status of skills */ 
        function changeStatus(status) {
            vm.flag = false;
            
            if(status.status == 1 && status.parent == 0 ){
                var parent = status.skills_id;
                SkillsService.checkData(parent).then(function (responses) {
                    if (responses.status == 200) {
                        if (responses.data.data && responses.data.data.length > 0) {
                            SweetAlert.swal("Error!", "Sorry! this skill can't be deactivated. Please deactivate sub skill first!", "error");
                        }else{
                            vm.flag = true;
                        }
                    }
                });
                                  
            }else{
                if(status.status == 0 && status.parent != 0){
                    var statusId = 1;
                    var parent = status.parent;
                    SkillsService.getCategoryParent(parent).then(function (responses) {
                        if (responses.status == 200) {
                            if(responses.data.status == 1){
                                if(responses.data.data.status == 0){
                                    SweetAlert.swal("Error!", "Sorry! this skill can't be activated. Please activate Parent skill first!", "error");
                                }else{
                                    vm.flag = true;                                    
                                }
                            }
                        }
                    });
                }else{
                    if(status.status == 0 && status.parent == 0){
                        vm.flag = true;                                    
                        var statusId = 1;
                    }else{
                        vm.flag = true;                                    
                        var statusId = 0;
                    }                    
                }              
            }

            $timeout( function(){    
                if(vm.flag == true){
                    var data = { skills_id: status.skills_id, status: statusId };
                    SkillsService.changeStatus(data).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Success!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            }, 500);
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getSkillsList(1, '');
        }//END reset();

        /* get all parent skill */
        function getAllMainCatSkills(){
            vm.skillsList = [];
            SkillsService.getAllMainCatSkills('0').then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.skillsList = response.data.data;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllMainCatSkills();
    }
}());
