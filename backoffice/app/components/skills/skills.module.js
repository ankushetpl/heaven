(function () {

    angular.module('skillsApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var skillsPath = 'app/components/skills/';
        $stateProvider
            .state('backoffice.skills', {
                url: 'skills',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/index.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/view.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.subview', {
                url: '/subview/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/view.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })            
            .state('backoffice.skills.subcreate', {
                url: '/subcreate',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            })
            .state('backoffice.skills.subedit', {
                url: '/subedit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: skillsPath + 'views/form.html',
                        controller: 'SkillsController',
                        controllerAs: 'skills'
                    }
                }
            });     
    }

}());