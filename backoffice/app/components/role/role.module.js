(function () {

    angular.module('roleApp', []).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var rolePath = 'app/components/role/';
        $stateProvider
            .state('backoffice.role', {
                url: 'role',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/index.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
            .state('backoffice.role.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/form.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
            .state('backoffice.role.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/form.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
            .state('backoffice.role.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: rolePath + 'views/view.html',
                        controller: 'RoleController',
                        controllerAs: 'role'
                    }
                }
            })
    }

}());