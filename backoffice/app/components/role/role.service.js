(function () {
    "use strict";
    angular
        .module('roleApp')
        .service('RoleService', RoleService);

    RoleService.$inject = ['$http', 'APPCONFIG', '$q'];

    function RoleService($http, APPCONFIG, $q) {

        /* save role */
        function saveRole(obj) {
            var URL = APPCONFIG.APIURL + 'role/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = obj.role_id;
                    URL = APPCONFIG.APIURL + 'role/edit';
                }

                if (typeof obj.role_name !== 'undefined' && obj.role_name !== '') {
                    requestData['role_name'] = obj.role_name;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveRole();

        /* to get all roles */
        function getRoles(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = parseInt(obj.role_id);
                }

                if (typeof obj.role_name !== 'undefined' && obj.role_name !== '') {
                    requestData['role_name'] = obj.role_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getRoles();

        /* to get single role */
        function getSingleRole(role_id) {
            var URL = APPCONFIG.APIURL + 'role/view';
            var requestData = {};

            if (typeof role_id !== undefined && role_id !== '') {
                requestData['role_id'] = role_id;
            }

            return this.runHttp(URL, requestData);
        } //END getRoleById();

        /* to delete a role from database */
        function deleteRole(role_id) {
            var URL = APPCONFIG.APIURL + 'role/delete';
            var requestData = {};

            if (typeof role_id !== undefined && role_id !== '') {
                requestData['role_id'] = role_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteRole();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getRoles: getRoles,
            getSingleRole: getSingleRole,
            saveRole: saveRole,
            deleteRole: deleteRole
        }

    };//END RoleService()
}());
