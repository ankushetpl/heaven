(function () {

    angular.module('paymentSuccessApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var paymentSuccessPath = 'app/components/paymentSuccess/';
        $stateProvider
            .state('backoffice.paymentSuccess', {
                url: 'paySuccess',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentSuccessPath + 'views/index.html',
                        controller: 'PaymentSuccessController',
                        controllerAs: 'paymentSuccess'
                    }
                }
            })
            .state('backoffice.paymentSuccess.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentSuccessPath + 'views/view.html',
                        controller: 'PaymentSuccessController',
                        controllerAs: 'paymentSuccess'
                    }
                }
            })
            .state('backoffice.paymentSuccess.weekly', {
                url: '/weekly',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentSuccessPath + 'views/weekly.html',
                        controller: 'PaymentSuccessWeekController',
                        controllerAs: 'paymentSuccessWeek'
                    }
                }
            })
            .state('backoffice.paymentSuccess.weeklyView', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentSuccessPath + 'views/weeklyView.html',
                        controller: 'PaymentSuccessWeekController',
                        controllerAs: 'paymentSuccessWeek'
                    }
                }
            })
    }

}());