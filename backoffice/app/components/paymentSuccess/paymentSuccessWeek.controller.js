(function () {
    "use strict";
    angular.module('paymentSuccessApp')
        .controller('PaymentSuccessWeekController', PaymentSuccessWeekController);

    PaymentSuccessWeekController.$inject = ['$scope', '$rootScope', '$state', '$location', 'PaymentSuccessService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function PaymentSuccessWeekController($scope, $rootScope, $state, $location, PaymentSuccessService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getPaymentWeekList = getPaymentWeekList;
        vm.getSinglePaymentWeek = getSinglePaymentWeek;
        vm.deletePaymentWeek = deletePaymentWeek;
        vm.getWeeklyCSV = getWeeklyCSV;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Payment Bookings';
        vm.total = 0;
        vm.PaymentPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        vm.getPaymentWeekList();
        if (path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.paymentSuccess.weekly');
                return false;
            } else {
                vm.progressbar.start();
                vm.getSinglePaymentWeek(path[3]);
            }
        }
        
        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()


        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getPaymentWeekList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getPaymentWeekList(newPage, searchInfo);
        }//END changePage();

        /* to get payment list */
        function getPaymentWeekList(newPage, obj) {
            vm.progressbar.start();
            vm.total = 0;
            vm.PayListWeek = [];
            vm.PayListWeekCheck = false;
            PaymentSuccessService.getSuccessPaymentsWeek(newPage, obj, vm.orderInfo).then(function (response) {
               
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.PayListWeekCheck = true;
                    if (response.data.paymentWeek && response.data.paymentWeek.length > 0) {
                        vm.total = response.data.total;
                        vm.PayListWeek = response.data.paymentWeek;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getPaymentWeekList();

        /* to get single booking */
        function getSinglePaymentWeek(booking_id) {
            $location.hash('top');
            $anchorScroll();
            PaymentSuccessService.getSinglePaymentWeek(booking_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singlePaymentWeek = response.data.paymentWeek;
                    } else {
                        toastr.error(response.data.message, 'Payment');
                        $state.go('backoffice.payment_success');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Payment');
            });
        }//END getSinglePaymentWeek();

        /** to delete a Payment **/
        function deletePaymentWeek(id, index) {

            SweetAlert.swal({
                title: "Sure you want to delete this Booking?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    PaymentSuccessService.deletePaymentWeek(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.getPaymentWeekList.splice(index, 1);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Payment');
                    });
                }
            });
        }//END deletePaymentWeek();


        /* to get all data in CSV format */
        function getWeeklyCSV() {
            vm.progressbar.start();
            PaymentSuccessService.getWeekBookingDataCSV().then(function (response) {

                if (response.status == 200) {
                    if (response.data.weeklyCSV && response.data.weeklyCSV.length > 0) {
                        vm.list = response.data.weeklyCSV;
                        // delete vm.list[0].booking_service_type;
                        var csv = Object.keys(vm.list[0]);
                        var headings = csv.toString();
                    }
                }

                var csv = headings;
                    csv += "\n";
                angular.forEach(vm.list, function(value, key) {
                    var array = []; 
                    array.push([value.bookweekly_id, value.booking_date, value.booking_time_from, value.booking_time_to, value.booking_apartment, value.booking_street, value.booking_city, value.tasktype_name, value.booking_bedrooms, value.booking_bathrooms, value.booking_baskets, value.booking_other_work, value.booking_start_time, value.booking_end_time, value.booking_cost, value.workers_name, value.workers_phone, value.workers_mobile, value.workers_address, value.clients_name, value.clients_phone, value.clients_mobile, value.clients_address]);
                    csv += array;
                    csv += "\n";
                });
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'Weekly Success Payments.csv';
                hiddenElement.click();
                vm.progressbar.complete();

            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWeeklyCSV();         

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getPaymentWeekList(1, '');
        }//END reset();               
    }

}());
