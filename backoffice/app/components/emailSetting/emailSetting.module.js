(function () {

    angular.module('emailSettingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var emailSettingPath = 'app/components/emailSetting/';
        $stateProvider
            .state('backoffice.emailsetup', {
                url: 'emailsetup',
                views: {
                    'content@backoffice': {
                        templateUrl: emailSettingPath + 'views/index.html',
                        controller: 'EmailSetupController',
                        controllerAs: 'emailsetup'
                    }
                }
            })
            .state('backoffice.emailsetup.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: emailSettingPath + 'views/form.html',
                        controller: 'EmailSetupController',
                        controllerAs: 'emailsetup'
                    }
                }
            })
            .state('backoffice.emailsetup.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: emailSettingPath + 'views/form.html',
                        controller: 'EmailSetupController',
                        controllerAs: 'emailsetup'
                    }
                }
            })
            .state('backoffice.emailsetup.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: emailSettingPath + 'views/view.html',
                        controller: 'EmailSetupController',
                        controllerAs: 'emailsetup'
                    }
                }
            })
    }

}());