(function () {
    "use strict";
    angular
        .module('emailSettingApp')
        .service('EmailSetupService', EmailSetupService);

    EmailSetupService.$inject = ['$http', 'APPCONFIG', '$q'];

    function EmailSetupService($http, APPCONFIG, $q) {

        /* save mail */
        function saveMail(obj) {
            var URL = APPCONFIG.APIURL + 'mail/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.email_id !== 'undefined' && obj.email_id !== '') {
                    requestData['email_id'] = obj.email_id;
                    URL = APPCONFIG.APIURL + 'mail/edit';
                }

                if (typeof obj.email_type !== 'undefined' && obj.email_type !== '') {
                    requestData['email_type'] = obj.email_type;
                }

                if (typeof obj.email_text !== 'undefined' && obj.email_text !== '') {
                    requestData['email_text'] = obj.email_text;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }               
            }
            
            return this.runHttp(URL, requestData);
        } //END saveMail();

        /* to get all mail */
        function getMail(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'mail';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.email_id !== 'undefined' && obj.email_id !== '') {
                    requestData['email_id'] = parseInt(obj.email_id);
                }

                if (typeof obj.email_type !== 'undefined' && obj.email_type !== '') {
                    requestData['email_type'] = obj.email_type;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.default !== 'undefined' && obj.default !== '') {
                    requestData['defaults'] = parseInt(obj.default);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getMail();

        /* to get single social */
        function getSingleMail(mail_id) {
            var URL = APPCONFIG.APIURL + 'mail/view';
            var requestData = {};

            if (typeof mail_id !== undefined && mail_id !== '') {
                requestData['email_id'] = mail_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleMail();

        /* to delete a mail from database */
        function deleteMail(email_id) {
            var URL = APPCONFIG.APIURL + 'mail/delete';
            var requestData = {};

            if (typeof email_id !== undefined && email_id !== '') {
                requestData['email_id'] = email_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteMail();

        /* to change active/inactive status of social */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'mail/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.email_id != undefined && obj.email_id != "") {
                    requestData["email_id"] = obj.email_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change default/not default status of mail */
        function changeStatusDefault(obj) {
            var URL = APPCONFIG.APIURL + 'mail/default';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.email_id != undefined && obj.email_id != "") {
                    requestData["email_id"] = obj.email_id;
                }

                if (obj.default != undefined || obj.default != "") {
                    requestData["default"] = obj.default;
                }

                if (obj.email_type != undefined || obj.email_type != "") {
                    requestData["email_type"] = obj.email_type;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatusDefault()
      
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getMail: getMail,
            getSingleMail: getSingleMail,
            saveMail: saveMail,
            deleteMail: deleteMail,
            changeStatus: changeStatus,
            changeStatusDefault: changeStatusDefault
        }

    };//END SettingService()
}());
