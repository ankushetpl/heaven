(function () {
    "use strict";
    angular.module('emailSettingApp')
        .controller('EmailSetupController', EmailSetupController);

    EmailSetupController.$inject = ['$scope', '$rootScope', '$state', '$location', 'EmailSetupService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function EmailSetupController($scope, $rootScope, $state, $location, EmailSetupService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getMailList = getMailList;
        vm.getSingleMail = getSingleMail;
        vm.saveMail = saveMail;
        vm.deleteMail = deleteMail;
        vm.changeStatus = changeStatus;
        vm.changeStatusDefault = changeStatusDefault;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Email Setup';
        vm.totalMail = 0;
        vm.MailPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.mailForm = { social_id: ''};
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.locationList = [{ id : 'Administrator', name : 'Administrator'}, { id : 'Contact', name : 'Contact'}, { id : 'Support', name : 'Support'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.emailsetup');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleMail(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getMailList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getMailList(newPage, searchInfo);
        }//END changePage();

        /* to save social after add and edit  */
        function saveMail() {
            vm.progressbar.start();
            vm.isDisabledButton = true;
            EmailSetupService.saveMail(vm.mailForm).then(function (response) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Email');
                        $state.go('backoffice.emailsetup');
                    } else {
                        toastr.error(response.data.message, 'Email');
                    }
                } else {
                    toastr.error(response.data.message, 'Email');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Email');
            });
        }//END saveMail();

        /* to get mail list */
        function getMailList(newPage, obj) {
            vm.progressbar.start();
            vm.totalMail = 0;
            vm.mailList = [];
            vm.mailListCheck = false;
            EmailSetupService.getMail(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.mailListCheck = true;
                    if (response.data.email && response.data.email.length > 0) {
                        vm.totalMail = response.data.total;
                        vm.mailList = response.data.email;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getMailList();

        /* to get single mail */
        function getSingleMail(mail_id) {
            $location.hash('top');
            $anchorScroll();
            EmailSetupService.getSingleMail(mail_id).then(function (response) {                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleMail = response.data.email;
                        vm.mailForm = response.data.email;
                    } else {
                        toastr.error(response.data.message, 'Social');
                        $state.go('backoffice.social');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Social');
            });
        }//END getSingleMail();

        /** to delete a mail **/
        function deleteMail(id, defaults, index) {
            
            if (defaults == 1) {
                var message = "Sorry! Default email cannot be deleted";
                SweetAlert.swal(message);
                return false;                
            }

            SweetAlert.swal({
                title: "Are you sure you want to delete this email?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    EmailSetupService.deleteMail(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.mailList.splice(index, 1);
                            vm.totalMail = vm.totalMail - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Social');
                    });
                }
            });
        }//END deleteMail();

        /* to change active/inactive status of mail */
        function changeStatus(status) {

            if (status.defaults == 1) {
                var message = "Sorry! Default email cannot be deactivated";
                SweetAlert.swal(message);
                return false;                
            }

            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { email_id: status.email_id, status: statusId };
            EmailSetupService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to change default/not default status of mail */
        function changeStatusDefault(default1) {
            if (default1.status == 0) {
                var message = "Please approve this email first";
                SweetAlert.swal(message);
                return false;
            }
            
            if (default1.defaults == 1) {
                var message = "Email already as default";
                SweetAlert.swal(message);
                return false;
                var defaultId = 0;
            } else {
                var defaultId = 1;
            }
            
            var data = { email_id: default1.email_id, default: defaultId, email_type : default1.email_type};
            
            EmailSetupService.changeStatusDefault(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                    } 
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatusDefault()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getMailList(1, '');
        }//END reset();               
    }

}());
