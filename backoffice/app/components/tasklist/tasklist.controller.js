(function () {
    "use strict";
    angular.module('tasklistApp')
        .controller('TasklistController', TasklistController);

    TasklistController.$inject = ['$scope', '$rootScope', '$state', '$location', 'TasklistService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function TasklistController($scope, $rootScope, $state, $location, TasklistService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getTasklist = getTasklist;
        vm.getSingleTasklist = getSingleTasklist;
        vm.saveTasklist = saveTasklist;
        vm.getAllType = getAllType;
        vm.deleteTasklist = deleteTasklist;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.range = range;
        vm.isDisabled = isDisabled;

        vm.getAllType();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Task';
        vm.totalTasklist = 0;
        vm.tasklistPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.tasklistForm = { tasklist_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.tasktypeList = [{ id : 0, status : 'Standard'}, { id : 1, status : 'Custom' }];
        
        function range(min, max) {
            vm.durationList = [];
            for (var i = min; i <= max; i++) {
                vm.durationList.push(i);
            }
            return vm.durationList;
        };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.tasklist');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleTasklist(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getTasklist(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getTasklist(newPage, searchInfo);
        }//END changePage();

        /* to save Task after add and edit  */
        function saveTasklist() {           
            TasklistService.saveTasklist(vm.tasklistForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Task List');
                        $state.go('backoffice.tasklist');
                    } else {
                        toastr.error(response.data.message, 'Task List');
                    }
                } else {
                    toastr.error(response.data.message, 'Task List');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Task List');
            });
        }//END saveTasklist();

        /* to get Task list */
        function getTasklist(newPage, obj) {
            vm.progressbar.start();
            vm.totalTasklist = 0;
            vm.tasklist = [];
            vm.tasklistCheck = false;
            TasklistService.getTasklist(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.tasklistCheck = true;
                    if (response.data.tasklist && response.data.tasklist.length > 0) {
                        vm.totalTasklist = response.data.total;
                        vm.tasklist = response.data.tasklist;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getTasklist();

        /* to get single task */
        function getSingleTasklist(tasklist_id) {
            $location.hash('top');
            $anchorScroll();
            TasklistService.getSingleTasklist(tasklist_id).then(function (response) {                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleTasklist = response.data.tasklist;
                        vm.tasklistForm = response.data.tasklist;
                    } else {
                        toastr.error(response.data.message, 'Task List');
                        $state.go('backoffice.tasklist');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Tasklist');
            });
        }//END getSingleTasklist();

        /** to delete a tasklist **/
        function deleteTasklist(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Task?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    TasklistService.deleteTasklist(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.tasklist.splice(index, 1);
                            vm.totalTasklist = vm.totalTasklist - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Task List');
                    });
                }
            });
        }//END deleteTasklist();

        /* to change active/inactive status of task */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { tasklist_id: status.tasklist_id, status: statusId };
            TasklistService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to service type */
        function getAllType(){
            TasklistService.getAllTypes().then(function (response) {
                if (response.status == 200) {
                    if (response.data.type && response.data.type.length > 0) {
                        vm.typeList = response.data.type; 
                    }
                } else {
                    toastr.error(response.data.message, 'Task List');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Task List');
            });
        } //END getAllType();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getTasklist(1, '');
        }//END reset();               
    }

}());
