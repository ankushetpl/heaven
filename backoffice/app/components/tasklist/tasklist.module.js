(function () {

    angular.module('tasklistApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var tasklistPath = 'app/components/tasklist/';
        $stateProvider
            .state('backoffice.tasklist', {
                url: 'tasklist',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistPath + 'views/index.html',
                        controller: 'TasklistController',
                        controllerAs: 'tasklist'
                    }
                }
            })
            .state('backoffice.tasklist.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistPath + 'views/form.html',
                        controller: 'TasklistController',
                        controllerAs: 'tasklist'
                    }
                }
            })
            .state('backoffice.tasklist.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistPath + 'views/form.html',
                        controller: 'TasklistController',
                        controllerAs: 'tasklist'
                    }
                }
            })
            .state('backoffice.tasklist.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistPath + 'views/view.html',
                        controller: 'TasklistController',
                        controllerAs: 'tasklist'
                    }
                }
            });
    }

}());