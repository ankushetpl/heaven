(function () {
    'use strict';
    angular.module('dashboardApp', []).controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$rootScope', '$state', '$location', 'dashboardService', 'toastr', 'ngProgressFactory', '$timeout', 'APPCONFIG', 'Upload', 'authServices', 'SweetAlert', '$anchorScroll'];
    
    function DashboardController($scope, $rootScope, $state, $location, dashboardService, toastr, ngProgressFactory, $timeout, APPCONFIG, Upload, authServices, SweetAlert, $anchorScroll) {
        var vm = this;
        // vm.passwordValidator = passwordValidator;
        
        vm.logout = logout;
        vm.getAllCount = getAllCount;
        vm.getAdminProfile = getAdminProfile;
        vm.updateData = updateData;
        vm.cancalData = cancalData;
        vm.saveProfile = saveProfile;
        vm.adminNotification = adminNotification;

        vm.workerCount = 0;
        vm.clientCount = 0;
        vm.payRequestCount = 0;
        vm.doneBookingCount = 0;
        vm.cafeCount = 0;
        vm.assessorCount = 0;
        vm.editFlag = false;
        vm.imageFlag = false;
        vm.isDisabledButton = false;

        if ($state.current.name == 'backoffice' || $state.current.name == 'backoffice.dashboard') {
            $rootScope.headerTitle = 'Dashboard';
        } else if ($state.current.name == 'backoffice.changePassword') {
            $rootScope.headerTitle = 'Change Password';
        }
        $rootScope.bodyClass = 'fix-header fix-sidebar card-no-border';
        
        vm.progressbar = ngProgressFactory.createInstance();
                
        var user_id = $rootScope.user.user_id;
        var usertype = $rootScope.user.user_role_id;
        
        vm.getAllCount();
        vm.getAdminProfile();
        vm.adminNotification();
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[1] == "profile") {
            $rootScope.headerTitle = 'Profile';
            vm.getAdminProfile();
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        // function passwordValidator(password) {
        //     if (!password) return;
        //     if (password.length < 6) return "Password must be at least " + 6 + " characters long";
        //     if (!password.match(/[A-Z]/)) return "Password must have at least one capital letter";
        //     if (!password.match(/[0-9]/)) return "Password must have at least one number";

        //     return true;
        // };

        /* get all type of counts */
        function getAllCount() {
            vm.logPic = [];
            var total = 0;
            vm.progressbar.start();
            dashboardService.getAllCount().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.workerCount = response.data.worker;
                        vm.clientCount = response.data.client;
                        vm.doneBookingCount = response.data.booking;
                        vm.payRequestCount = response.data.earlyPay;
                        vm.cafeCount = response.data.cafe;
                        vm.assessorCount = response.data.assessor;
                        vm.logPic.file = response.data.logPic;
                        
                        if(vm.logPic.file != 0){
                            var file_id = vm.logPic.file;
                            dashboardService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.logPic.file = responses.data.file.file_base_url;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }
                    }
                }else{
                    vm.progressbar.complete();
                    toastr.error(response.data.message, 'Dashboard');
                }
            }).catch(function (response) {
                vm.progressbar.complete();
                toastr.error("No data found", 'Dashboard');
            });
        }; //END getAllCount()

        /* get profile */
        function getAdminProfile() {
            vm.isDisabled = true;
            vm.profileForm = [];
            dashboardService.getProfileData(user_id, usertype).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        
                        vm.profileForm = response.data.data;
                        
                        vm.profileForm.name = '';
                        vm.profileForm.name = response.data.data.admin_name; 

                        if(response.data.data.admin_image != 0){
                            vm.profileForm.file = '';
                            var file_id = response.data.data.admin_image;
                            dashboardService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.profileForm.file = responses.data.file.file_base_url;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }  
                        
                    } else {
                        toastr.error(response.data.message, 'Dashboard');
                        $state.go('backoffice.dashboard');
                    }
                }
                
            }).catch(function (response) {
                toastr.error(response.data.message, "No data found");
            });
        }; //END getAdminProfile()  

        /* Enable Field */
        function updateData(){
            vm.editFlag = true;
            vm.isDisabled = false;
        } //END updateData();

        /* Disable Field */
        function cancalData(){
            $state.reload();
        } //END cancalData();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    vm.profileForm.admin_image = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to save saveProfile after add and edit  */
        function saveProfile() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            if (vm.profileForm.file !== '') { //check if from is valid
                
                if(vm.profileForm.file.size > 0 && vm.profileForm.file.name !== '' ){
                    vm.upload(vm.profileForm.file); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        dashboardService.saveProfile(vm.profileForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.success(response.data.message, 'Dashboard');
                                    $state.reload();
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Dashboard');
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Dashboard');
                            }
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Dashboard');
                        });
                    }
                }, 2000 );
            } 
            
        } //END saveProfile();

        /* logout */
        function logout() {
            SweetAlert.swal({
                title: "Are you sure you want to Logout?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f62d51", confirmButtonText: "Yes, Logout!",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    vm.progressbar.start();
                    authServices.logout().then(function (response) {
                        if (response.status == 200) {
                            toastr.success("Logout successful", "Logout");
                            vm.progressbar.complete();
                            $state.go('auth.login');
                        }
                    }).catch(function (response) {
                        toastr.error("Unable to Logout!<br/>Try again later", "Error");
                    });
                }
            });
        }//END logout();

        /* Notifications */
        function adminNotification() {
            dashboardService.adminNotification().then(function (response) {

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        if(response.data.not.length > 0) {
                            
                            vm.notData = response.data.not;
                            vm.notCount = response.data.not.length;
                        } else {
                            vm.notData = 'No Notifications';
                        }
                        
                    } else {
                        toastr.error(response.data.message, 'Dashboard');
                        $state.go('backoffice.dashboard');
                    }
                }
            }, function (error) {
                toastr.error('Internal server error', 'Dashboard');
            });
        }//END adminNotification();
    };

}());