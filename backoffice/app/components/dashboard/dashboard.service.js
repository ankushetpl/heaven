(function() {
    "use strict";
    angular
        .module('dashboardApp')
        .service('dashboardService', dashboardService);

    dashboardService.$inject = ['$http', 'APPCONFIG', '$q'];

    function dashboardService($http, APPCONFIG, $q) {
        return {
            getAllCount: getAllCount,
            getProfileData: getProfileData,
            saveProfile: saveProfile,
            getImage: getImage,
            adminNotification: adminNotification
        }

        /* to get count for dashboard */
        function getAllCount() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'dashboard/count';
            $http({
                url: URL,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getAllCount();

        /* to get admin profile data */
        function getProfileData(user_id, usertype) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof usertype !== undefined && usertype !== '') {
                requestData['UserType'] = usertype;
            }

            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getProfileData();

        /* to get single image */
        function getImage(file_id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }

            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getImage();

        /* to update admin profile data */
        function saveProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'user/update';
            var requestData = {};
            
            if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                requestData['user_id'] = obj.user_id;
            }

            if (typeof obj.user_role_id !== undefined && obj.user_role_id !== '') {
                requestData['usertype'] = obj.user_role_id;
            }

            if (typeof obj.admin_name !== undefined && obj.admin_name !== '') {
                requestData['name'] = obj.admin_name;
            }

            if (typeof obj.admin_phone !== undefined && obj.admin_phone !== '') {
                requestData['phone'] = obj.admin_phone;
            }

            if (typeof obj.admin_address !== undefined && obj.admin_address !== '') {
                requestData['admin_address'] = obj.admin_address;
            }

            if (typeof obj.admin_image !== undefined && obj.admin_image !== '') {
                requestData['assessors_image'] = obj.admin_image;
            }

            if (typeof obj.status !== undefined && obj.status !== '') {
                requestData['status'] = obj.status;
            }

            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END saveProfile();   

        /* to get all realy pay requests */
        function adminNotification() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'dashboard/notification';
            $http({
                url: URL,
                method: "POST",
                // data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END adminNotification();

    }; //END dashboardService()
}());