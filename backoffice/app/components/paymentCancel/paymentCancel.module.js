(function () {

    angular.module('paymentCancelApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var paymentCancelPath = 'app/components/paymentCancel/';
        $stateProvider
            .state('backoffice.paymentCancel', {
                url: 'payCancel',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentCancelPath + 'views/index.html',
                        controller: 'PaymentCancelController',
                        controllerAs: 'paymentCancel'
                    }
                }
            })
            .state('backoffice.paymentCancel.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentCancelPath + 'views/view.html',
                        controller: 'PaymentCancelController',
                        controllerAs: 'paymentCancel'
                    }
                }
            })
            .state('backoffice.paymentCancel.weekly', {
                url: '/weekly',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentCancelPath + 'views/weekly.html',
                        controller: 'PaymentCancelWeekController',
                        controllerAs: 'paymentCancelWeek'
                    }
                }
            })
            .state('backoffice.paymentCancel.weeklyView', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentCancelPath + 'views/weeklyView.html',
                        controller: 'PaymentCancelWeekController',
                        controllerAs: 'paymentCancelWeek'
                    }
                }
            })
    }

}());