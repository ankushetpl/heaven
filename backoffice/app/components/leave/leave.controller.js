(function () {
    "use strict";
    angular.module('leaveApp')
        .controller('LeaveController', LeaveController);

    LeaveController.$inject = ['$scope', '$rootScope', '$state', '$location', 'LeaveService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function LeaveController($scope, $rootScope, $state, $location, LeaveService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getLeaveList = getLeaveList;
        vm.getSingleLeave = getSingleLeave;
        vm.saveLeave = saveLeave;
        vm.deleteLeave = deleteLeave;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.resetForm = resetForm;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Leaves';
        vm.totalLeave = 0;
        vm.leavePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.leaveForm = { leaves_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.leave');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleLeave(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getLeaveList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getLeaveList(newPage, searchInfo);
        }//END changePage();

        /* to save leave after add and edit  */
        function saveLeave() {
            LeaveService.saveLeave(vm.leaveForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Leave');
                        $state.go('backoffice.leave');
                    } else {
                        toastr.error(response.data.message, 'Leave');
                    }
                } else {
                    toastr.error(response.data.message, 'Leave');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Leave');
            });
        }//END saveLeave();

        /* to get leave list */
        function getLeaveList(newPage, obj) {
            vm.progressbar.start();
            vm.totalLeave = 0;
            vm.leaveList = [];
            vm.leaveListCheck = false;
            LeaveService.getLeave(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.leaveListCheck = true;
                    if (response.data.leave && response.data.leave.length > 0) {
                        vm.totalLeave = response.data.total;
                        vm.leaveList = response.data.leave;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getLeaveList();

        /* to get single leave */
        function getSingleLeave(leaves_id) {
            $location.hash('top');
            $anchorScroll();
            LeaveService.getSingleLeave(leaves_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleLeave = response.data.leave;
                        vm.leaveForm = response.data.leave;
                    } else {
                        toastr.error(response.data.message, 'Leaves');
                        $state.go('backoffice.leave');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Leaves');
            });
        }//END getSingleLeave();

        /** to delete a leave **/
        function deleteLeave(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Leave?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    LeaveService.deleteLeave(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.leaveList.splice(index, 1);
                            vm.totalLeave = vm.totalLeave - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteLeave();

        /* to change active/inactive status of leave */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            var data = { leaves_id: status.leaves_id, status: statusId };
            LeaveService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getLeaveList(1, '');
        }//END reset();

        /* to reset all parameters  */
        function resetForm() {
            vm.leaveForm = { leaves_id: '' };
        }//END resetForm();
    }

}());
