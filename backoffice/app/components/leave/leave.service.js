(function () {
    "use strict";
    angular
        .module('leaveApp')
        .service('LeaveService', LeaveService);

    LeaveService.$inject = ['$http', 'APPCONFIG', '$q'];

    function LeaveService($http, APPCONFIG, $q) {

        /* save leave */
        function saveLeave(obj) {
            var URL = APPCONFIG.APIURL + 'leave/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.leaves_id !== 'undefined' && obj.leaves_id !== '') {
                    requestData['leaves_id'] = obj.leaves_id;
                    URL = APPCONFIG.APIURL + 'leave/edit';
                }

                if (typeof obj.leaves_name !== 'undefined' && obj.leaves_name !== '') {
                    requestData['leaves_name'] = obj.leaves_name;
                }

                if (typeof obj.leaves_des !== 'undefined' && obj.leaves_des !== '') {
                    requestData['leaves_des'] = obj.leaves_des;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        } //END saveLeave();

        /* to get all leave */
        function getLeave(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'leave';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.leaves_id !== 'undefined' && obj.leaves_id !== '') {
                    requestData['leaves_id'] = parseInt(obj.leaves_id);
                }

                if (typeof obj.leaves_name !== 'undefined' && obj.leaves_name !== '') {
                    requestData['leaves_name'] = obj.leaves_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            return this.runHttp(URL, requestData);
        }//END getLeave();

        /* to get single leave */
        function getSingleLeave(leaves_id) {
            var URL = APPCONFIG.APIURL + 'leave/view';
            var requestData = {};

            if (typeof leaves_id !== undefined && leaves_id !== '') {
                requestData['leaves_id'] = leaves_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleLeave();

        /* to delete a leave from database */
        function deleteLeave(leaves_id) {
            var URL = APPCONFIG.APIURL + 'leave/delete';
            var requestData = {};

            if (typeof leaves_id !== undefined && leaves_id !== '') {
                requestData['leaves_id'] = leaves_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteLeave();

        /* to change active/inactive status of leave */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'leave/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.leaves_id != undefined && obj.leaves_id != "") {
                    requestData["leaves_id"] = obj.leaves_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getLeave: getLeave,
            getSingleLeave: getSingleLeave,
            saveLeave: saveLeave,
            deleteLeave: deleteLeave,
            changeStatus: changeStatus
        }

    };//END LeaveService()
}());
