(function () {

    angular.module('leaveApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var leavePath = 'app/components/leave/';
        $stateProvider
            .state('backoffice.leave', {
                url: 'leave',
                views: {
                    'content@backoffice': {
                        templateUrl: leavePath + 'views/index.html',
                        controller: 'LeaveController',
                        controllerAs: 'leave'
                    }
                }
            })
            .state('backoffice.leave.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: leavePath + 'views/form.html',
                        controller: 'LeaveController',
                        controllerAs: 'leave'
                    }
                }
            })
            .state('backoffice.leave.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: leavePath + 'views/form.html',
                        controller: 'LeaveController',
                        controllerAs: 'leave'
                    }
                }
            })
            .state('backoffice.leave.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: leavePath + 'views/view.html',
                        controller: 'LeaveController',
                        controllerAs: 'leave'
                    }
                }
            })
    }

}());