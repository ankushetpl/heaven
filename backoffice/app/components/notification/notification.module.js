(function () {

    angular.module('notificationApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var notificationPath = 'app/components/notification/';
        $stateProvider
            .state('backoffice.notification', {
                url: 'notification',
                views: {
                    'content@backoffice': {
                        templateUrl: notificationPath + 'views/form.html',
                        controller: 'NotificationController',
                        controllerAs: 'notification'
                    }
                }
            });
            
    }

}());