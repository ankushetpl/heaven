(function () {
    "use strict";
    angular.module('notificationApp')
        .controller('NotificationController', NotificationController);

    NotificationController.$inject = ['$scope', '$rootScope', '$state', '$location', 'NotificationService', 'toastr', 'SweetAlert', '$timeout', 'Upload', 'APPCONFIG', 'ngProgressFactory'];

    function NotificationController($scope, $rootScope, $state, $location, NotificationService, toastr, SweetAlert, $timeout, Upload, APPCONFIG, ngProgressFactory) {
        var vm = this;

        vm.saveNot = saveNot;
        vm.formatDate = formatDate;

        vm.progressbar = ngProgressFactory.createInstance();
        
        $rootScope.headerTitle = 'Notification';
        vm.orderInfo = {};
        vm.imageFlag = false;  
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status == 1) {
                    vm.notForm.notification_img = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.notForm.notification_img = 0;
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload        

        /* to save  after add and edit  */
        function saveNot() {
            vm.progressbar.start();
            vm.isDisabledButton = true;
            if (typeof vm.notForm.file !== 'undefined' && vm.notForm.file !== '') { //check if from is valid
                
                if(vm.notForm.file.size > 0 && vm.notForm.file.name !== '' ){
                    vm.upload(vm.notForm.file); //call upload function   
                }else{
                    vm.imageFlag == true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){       
                        NotificationService.saveData(vm.notForm).then(function (response) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    toastr.success(response.data.message, 'Notification');
                                    $state.go('backoffice.notification');
                                } else {
                                    toastr.error(response.data.message, 'Notification');
                                }
                            } else {
                                toastr.error(response.data.message, 'Notification');
                            }
                        }, function (error) {
                            toastr.error('Internal server error', 'Notification');
                        });
                    }
                }, 3000 );
            } else{
                vm.notForm.notification_img = 0;
                
                NotificationService.saveData(vm.notForm).then(function (response) {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            toastr.success(response.data.message, 'Notification');
                            $state.go('backoffice.notification');                            
                        } else {
                            toastr.error(response.data.message, 'Notification');
                        }
                    } else {
                        toastr.error(response.data.message, 'Notification');
                    }
                }, function (error) {
                    toastr.error('Internal server error', 'Notification');
                });
            }
            
        }//END saveNot();
            
    }

}());
