(function () {
    "use strict";
    angular
        .module('notificationApp')
        .service('NotificationService', NotificationService);

    NotificationService.$inject = ['$http', 'APPCONFIG', '$q'];

    function NotificationService($http, APPCONFIG, $q) {

        /* save  */
        function saveData(obj) {
            var URL = APPCONFIG.APIURL + 'notification/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.user !== 'undefined' && obj.user !== '') {
                    requestData['user'] = obj.user;
                }

                if (typeof obj.message !== 'undefined' && obj.message !== '') {
                    requestData['message'] = obj.message;
                }

                if (typeof obj.notification_img !== 'undefined' && obj.notification_img !== '') {
                    requestData['notification_img'] = obj.notification_img;
                }
            }
            return this.runHttp(URL, requestData);
        } //END save();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            saveData: saveData
        }

    };//END NotificationService()
}());
