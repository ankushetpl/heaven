(function () {

    angular.module('serviceTypeApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var serviceTypePath = 'app/components/serviceType/';
        $stateProvider
            .state('backoffice.serviceType', {
                url: 'serviceType',
                views: {
                    'content@backoffice': {
                        templateUrl: serviceTypePath + 'views/index.html',
                        controller: 'ServiceTypeController',
                        controllerAs: 'serviceType'
                    }
                }
            })
            .state('backoffice.serviceType.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: serviceTypePath + 'views/form.html',
                        controller: 'ServiceTypeController',
                        controllerAs: 'serviceType'
                    }
                }
            })
            .state('backoffice.serviceType.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: serviceTypePath + 'views/form.html',
                        controller: 'ServiceTypeController',
                        controllerAs: 'serviceType'
                    }
                }
            })
            .state('backoffice.serviceType.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: serviceTypePath + 'views/view.html',
                        controller: 'ServiceTypeController',
                        controllerAs: 'serviceType'
                    }
                }
            });
    }

}());