(function () {
    "use strict";
    angular
        .module('serviceTypeApp')
        .service('ServiceTypeService', ServiceTypeService);

    ServiceTypeService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ServiceTypeService($http, APPCONFIG, $q) {

        /* save service type */
        function saveServiceType(obj) {
            var URL = APPCONFIG.APIURL + 'serviceType/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.tasktype_id !== 'undefined' && obj.tasktype_id !== '') {
                    requestData['tasktype_id'] = obj.tasktype_id;
                    URL = APPCONFIG.APIURL + 'serviceType/edit';
                }

                if (typeof obj.tasktype_name !== 'undefined' && obj.tasktype_name !== '') {
                    requestData['tasktype_name'] = obj.tasktype_name;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }                
            }
            
            return this.runHttp(URL, requestData);
        } //END saveServiceType();

        /* to get all service type */
        function getServiceType(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'serviceType';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.tasktype_id !== 'undefined' && obj.tasktype_id !== '') {
                    requestData['tasktype_id'] = parseInt(obj.tasktype_id);
                }

                if (typeof obj.tasktype_name !== 'undefined' && obj.tasktype_name !== '') {
                    requestData['tasktype_name'] = obj.tasktype_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getServiceType();

        /* to get single service type */
        function getSingleServiceType(tasktype_id) {
            var URL = APPCONFIG.APIURL + 'serviceType/view';
            var requestData = {};

            if (typeof tasktype_id !== undefined && tasktype_id !== '') {
                requestData['tasktype_id'] = tasktype_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleServiceType();

        /* to delete a service type from database */
        function deleteServiceType(tasktype_id) {
            var URL = APPCONFIG.APIURL + 'serviceType/delete';
            var requestData = {};

            if (typeof tasktype_id !== undefined && tasktype_id !== '') {
                requestData['tasktype_id'] = tasktype_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteServiceType();

        /* to change active/inactive status of service type */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'serviceType/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.tasktype_id != undefined && obj.tasktype_id != "") {
                    requestData["tasktype_id"] = obj.tasktype_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to check booking type */
        function checkBooking(tasktype_id) {
            var URL = APPCONFIG.APIURL + 'serviceType/checkBooking';
            var requestData = {};

            if (typeof tasktype_id !== undefined && tasktype_id !== '') {
                requestData['tasktype_id'] = tasktype_id;
            }

            return this.runHttp(URL, requestData);
        } //END checkBooking();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getServiceType: getServiceType,
            getSingleServiceType: getSingleServiceType,
            saveServiceType: saveServiceType,
            deleteServiceType: deleteServiceType,
            changeStatus: changeStatus,
            checkBooking: checkBooking
        }

    };//END ServiceTypeService()
}());
