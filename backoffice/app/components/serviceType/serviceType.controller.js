(function () {
    "use strict";
    angular.module('serviceTypeApp')
        .controller('ServiceTypeController', ServiceTypeController);

    ServiceTypeController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ServiceTypeService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function ServiceTypeController($scope, $rootScope, $state, $location, ServiceTypeService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getServiceTypeList = getServiceTypeList;
        vm.getSingleServiceType = getSingleServiceType;
        vm.saveServiceType = saveServiceType;
        vm.deleteServiceType = deleteServiceType;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Service Type';
        vm.totalServiceType = 0;
        vm.serviceTypePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.serviceTypeForm = { tasktype_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.serviceType');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleServiceType(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting service type list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getServiceTypeList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getServiceTypeList(newPage, searchInfo);
        }//END changePage();

        /* to save service type after add and edit  */
        function saveServiceType() {
            ServiceTypeService.saveServiceType(vm.serviceTypeForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Service Type');
                        $state.go('backoffice.serviceType');
                    } else {
                        toastr.error(response.data.message, 'Service Type');
                    }
                } else {
                    toastr.error(response.data.message, 'Service Type');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Service Type');
            });
        }//END saveServiceType();

        /* to get service type list */
        function getServiceTypeList(newPage, obj) {
            vm.progressbar.start();
            vm.totalServiceType = 0;
            vm.serviceTypeList = [];
            vm.serviceTypeListCheck = false;
            ServiceTypeService.getServiceType(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.serviceTypeListCheck = true;
                    if (response.data.serviceType && response.data.serviceType.length > 0) {
                        vm.totalServiceType = response.data.total;
                        vm.serviceTypeList = response.data.serviceType;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getServiceTypeList();

        /* to get single service type */
        function getSingleServiceType(tasktype_id) {
            $location.hash('top');
            $anchorScroll();
            ServiceTypeService.getSingleServiceType(tasktype_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleServiceType = response.data.serviceType;
                        vm.serviceTypeForm = response.data.serviceType;
                    } else {
                        toastr.error(response.data.message, 'Service Type');
                        $state.go('backoffice.serviceType');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Service Type');
            });
        }//END getSingleServiceType();

        /** to delete a service type **/
        function deleteServiceType(id, index) {
            vm.flag = false;
            vm.progressbar.start();
            
            ServiceTypeService.checkBooking(id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        SweetAlert.swal("Error!", "Sorry, You can't delete this service, bookings of this service are not completed", "error");                        
                    }else{
                        vm.flag = true;
                    }
                }
            });
        
            $timeout( function(){
                if(vm.flag == true){
                    SweetAlert.swal({
                        title: "Are you sure you want to delete this Service?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        html: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            ServiceTypeService.deleteServiceType(id).then(function (response) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    SweetAlert.swal("Deleted!", response.data.message, "success");
                                    vm.serviceTypeList.splice(index, 1);
                                    vm.totalServiceType = vm.totalServiceType - 1; 
                                } else {
                                    vm.progressbar.complete();
                                    SweetAlert.swal("Deleted!", response.data.message, "error");
                                }
                            }, function (error) {
                                vm.progressbar.complete();
                                toastr.error(error.data.error, 'Service Type');
                            });
                        }
                    });
                }
            }, 1000);
        }//END deleteServiceType();

        /* to change active/inactive status of service type */
        function changeStatus(status) {
            vm.flag = false;
            vm.progressbar.start();
            if (status.status == 1) {
                var statusId = 0;
                ServiceTypeService.checkBooking(status.tasktype_id).then(function (response) {
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            vm.progressbar.complete();
                            SweetAlert.swal("Error!", "Sorry, You can't change the status, bookings of this service are not completed", "error");                            
                        }else{
                            vm.flag = true;
                        }
                    }
                });
            } else {
                var statusId = 1;
                vm.flag = true;
            }
            
            $timeout( function(){
                if(vm.flag == true){
                    var data = { tasktype_id: status.tasktype_id, status: statusId };
                    ServiceTypeService.changeStatus(data).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {
                                vm.progressbar.complete();
                                SweetAlert.swal("Success!", response.data.message, "success");
                                $state.reload();
                                return true;
                            }
                        } else {
                            vm.progressbar.complete();
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    }, function (error) {
                        vm.progressbar.complete();
                        toastr.error(error.data.error, 'Error');
                    });
                }
            }, 1000);
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getServiceTypeList(1, '');
        }//END reset();               
    }

}());
