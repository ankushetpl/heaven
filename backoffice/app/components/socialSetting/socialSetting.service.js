(function () {
    "use strict";
    angular
        .module('socialSettingApp')
        .service('SocialService', SocialService);

    SocialService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SocialService($http, APPCONFIG, $q) {

        /* save social */
        function saveSocial(obj) {
            var URL = APPCONFIG.APIURL + 'social/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.social_id !== 'undefined' && obj.social_id !== '') {
                    requestData['social_id'] = obj.social_id;
                    URL = APPCONFIG.APIURL + 'social/edit';
                }

                if (typeof obj.social_type !== 'undefined' && obj.social_type !== '') {
                    requestData['social_type'] = obj.social_type;
                }

                if (typeof obj.social_url !== 'undefined' && obj.social_url !== '') {
                    requestData['social_url'] = obj.social_url;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.defaults !== 'undefined' && obj.defaults !== '') {
                    requestData['defaults'] = obj.defaults;
                }               
            }
            
            return this.runHttp(URL, requestData);
        } //END saveSocial();

        /* to get all social */
        function getSocial(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'social';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.social_id !== 'undefined' && obj.social_id !== '') {
                    requestData['social_id'] = parseInt(obj.social_id);
                }

                if (typeof obj.social_type !== 'undefined' && obj.social_type !== '') {
                    requestData['social_type'] = obj.social_type;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.default !== 'undefined' && obj.default !== '') {
                    requestData['defaults'] = parseInt(obj.default);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSocial();

        /* to get single social */
        function getSingleSocial(social_id) {
            var URL = APPCONFIG.APIURL + 'social/view';
            var requestData = {};

            if (typeof social_id !== undefined && social_id !== '') {
                requestData['social_id'] = social_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleSocial();

        /* to delete a social from database */
        function deleteSocial(social_id) {
            var URL = APPCONFIG.APIURL + 'social/delete';
            var requestData = {};

            if (typeof social_id !== undefined && social_id !== '') {
                requestData['social_id'] = social_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteSocial();

        /* to change active/inactive status of social */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'social/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.social_id != undefined && obj.social_id != "") {
                    requestData["social_id"] = obj.social_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change default/not default status of social */
        function changeStatusDefault(obj) {
            var URL = APPCONFIG.APIURL + 'social/default';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.social_id != undefined && obj.social_id != "") {
                    requestData["social_id"] = obj.social_id;
                }

                if (obj.default != undefined || obj.default != "") {
                    requestData["default"] = obj.default;
                }

                if (obj.social_type != undefined || obj.social_type != "") {
                    requestData["social_type"] = obj.social_type;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatusDefault()
      
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getSocial: getSocial,
            getSingleSocial: getSingleSocial,
            saveSocial: saveSocial,
            deleteSocial: deleteSocial,
            changeStatus: changeStatus,
            changeStatusDefault: changeStatusDefault
        }

    };//END SettingService()
}());
