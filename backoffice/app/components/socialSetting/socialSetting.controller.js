(function () {
    "use strict";
    angular.module('socialSettingApp')
        .controller('SocialController', SocialController);

    SocialController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SocialService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function SocialController($scope, $rootScope, $state, $location, SocialService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getSocialList = getSocialList;
        vm.getSingleSocial = getSingleSocial;
        vm.saveSocial = saveSocial;
        vm.deleteSocial = deleteSocial;
        vm.changeStatus = changeStatus;
        vm.changeStatusDefault = changeStatusDefault;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Social Network';
        vm.totalSocial = 0;
        vm.socialPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.socialForm = { social_id: ''};
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.locationList = [{ id : 'Facebook', name : 'Facebook'}, { id : 'Twitter', name : 'Twitter'}, { id : 'Linkedin', name : 'Linkedin'}, { id : 'Google', name : 'Google+' }, { id : 'Pinterest', name : 'Pinterest' }, { id : 'Instagram', name : 'Instagram' }];

        vm.defaultList = [{ id : 0, default : 'Not Default'}, { id : 1, default : 'Default'}];

        $scope.regex = RegExp('^((https?|ftp)://)?([a-z]+[.])?[a-z0-9-]+([.][a-z]{1,4}){1,2}(/.*[?].*)?$', 'i');

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.social');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleSocial(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getSocialList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getSocialList(newPage, searchInfo);
        }//END changePage();

        /* to save social after add and edit  */
        function saveSocial() {
            vm.progressbar.start();
            vm.isDisabledButton = true;
            SocialService.saveSocial(vm.socialForm).then(function (response) {
                vm.progressbar.complete();
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Social');
                        $state.go('backoffice.social');
                    } else {
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Social');
                    }
                } else {
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Social');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Social');
            });
        }//END saveSocial();

        /* to get social list */
        function getSocialList(newPage, obj) {
            vm.progressbar.start();
            vm.totalSocial = 0;
            vm.socialList = [];
            vm.socialListCheck = false;
            SocialService.getSocial(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.socialListCheck = true;
                    if (response.data.social && response.data.social.length > 0) {
                        vm.totalSocial = response.data.total;
                        vm.socialList = response.data.social;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSocialList();

        /* to get single social */
        function getSingleSocial(social_id) {
            $location.hash('top');
            $anchorScroll();
            SocialService.getSingleSocial(social_id).then(function (response) {                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleSocial = response.data.social;
                        vm.socialForm = response.data.social;
                    } else {
                        toastr.error(response.data.message, 'Social');
                        $state.go('backoffice.social');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Social');
            });
        }//END getSingleSocial();

        /** to delete a social **/
        function deleteSocial(id, defaults, index) {
            if (defaults == 1) {
                var message = "Sorry! Default social cannot be deleted";
                SweetAlert.swal(message);
                return false;                
            }

            SweetAlert.swal({
                title: "Are you sure you want to delete this social?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    SocialService.deleteSocial(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.socialList.splice(index, 1);
                            vm.totalSocial = vm.totalSocial - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Social');
                    });
                }
            });
        }//END deleteSocial();

        /* to change active/inactive status of social */
        function changeStatus(status) {

            if (status.defaults == 1) {
                var message = "Sorry! Default social cannot be deactivated";
                SweetAlert.swal(message);
                return false;                
            }

            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { social_id: status.social_id, status: statusId };
            SocialService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to change default/not default status of social */
        function changeStatusDefault(default1) {
            if (default1.status == 0) {
                var message = "Please approve this social first";
                SweetAlert.swal(message);
                return false;
            }
            
            if (default1.defaults == 1) {
                var message = "Social already as default";
                SweetAlert.swal(message);
                return false;
                var defaultId = 0;
            } else {
                var defaultId = 1;
            }
            
            var data = { social_id: default1.social_id, default: defaultId, social_type : default1.social_type};
            
            SocialService.changeStatusDefault(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                    } 
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatusDefault()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getSocialList(1, '');
        }//END reset();               
    }

}());
