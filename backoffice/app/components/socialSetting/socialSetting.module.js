(function () {

    angular.module('socialSettingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var socialSettingPath = 'app/components/socialSetting/';
        $stateProvider
            .state('backoffice.social', {
                url: 'social',
                views: {
                    'content@backoffice': {
                        templateUrl: socialSettingPath + 'views/index.html',
                        controller: 'SocialController',
                        controllerAs: 'social'
                    }
                }
            })
            .state('backoffice.social.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: socialSettingPath + 'views/form.html',
                        controller: 'SocialController',
                        controllerAs: 'social'
                    }
                }
            })
            .state('backoffice.social.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: socialSettingPath + 'views/form.html',
                        controller: 'SocialController',
                        controllerAs: 'social'
                    }
                }
            })
            .state('backoffice.social.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: socialSettingPath + 'views/view.html',
                        controller: 'SocialController',
                        controllerAs: 'social'
                    }
                }
            })
    }

}());