(function () {
    "use strict";
    angular
        .module('walletTopUpApp')
        .service('WalletTopUpService', WalletTopUpService);

    WalletTopUpService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WalletTopUpService($http, APPCONFIG, $q) {

        /* top up clients wallet  */
        function topupWallet(obj) {
            // console.log(obj);
            var URL = APPCONFIG.APIURL + 'walletTopUp/topUp';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                }

                if (typeof obj.points !== 'undefined' && obj.points !== '') {
                    requestData['amount'] = obj.points;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveBuyCredit();

        /* to get all client users data */
        function getClients(pageNum, obj, orderInfo) {
            // console.log(obj);
            // return false;
            var URL = APPCONFIG.APIURL + 'walletTopUp';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.contact !== 'undefined' && obj.contact !== '') {
                    requestData['contact'] = (obj.contact);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getBuyCredits();

        /* to get single client */
        function getClientCreditHistory(client_id) {
            var URL = APPCONFIG.APIURL + 'walletTopUp/view';
            var requestData = {};

            if (typeof client_id !== undefined && client_id !== '') {
                requestData['client_id'] = client_id;
            }

            return this.runHttp(URL, requestData);
        } //END getClientCreditHistory();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            topupWallet: topupWallet,
            getClients: getClients,
            getClientCreditHistory:getClientCreditHistory
        }

    };//END BuyCreditService()
}());
