(function () {

    angular.module('walletTopUpApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var walletTopUpPath = 'app/components/walletTopUp/';
        $stateProvider
            .state('backoffice.walletTopUp', {
                url: 'walletTopUp',
                views: {
                    'content@backoffice': {
                        templateUrl: walletTopUpPath + 'views/index.html',
                        controller: 'WalletTopUpController',
                        controllerAs: 'walletTopUp'
                    }
                }
            })
           // .state('backoffice.walletTopUp.edit', {
           //      url: '/edit/:id',
           //      views: {
           //          'content@backoffice': {
           //              templateUrl: walletTopUpPath + 'views/form.html',
           //              controller: 'WalletTopUpController',
           //              controllerAs: 'walletTopUp'
           //          }
           //      }
           //  })
            .state('backoffice.walletTopUp.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: walletTopUpPath + 'views/view.html',
                        controller: 'WalletTopUpController',
                        controllerAs: 'walletTopUp'
                    }
                }
            })
    }

}());