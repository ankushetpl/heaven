(function () {
    "use strict";
    angular.module('walletTopUpApp')
        .controller('WalletTopUpController', WalletTopUpController);

    WalletTopUpController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WalletTopUpService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function WalletTopUpController($scope, $rootScope, $state, $location, WalletTopUpService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getClientsList = getClientsList;
        vm.getClientCreditHistory = getClientCreditHistory;
        vm.openModal = openModal;
        vm.saveWallet = saveWallet;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Top Up Wallet';
        vm.totalClients = 0;
        vm.clientsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        
        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.walletTopUp');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getClientCreditHistory(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getClientsList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getClientsList(newPage, searchInfo);
        }//END changePage();

        vm.walletForm = {'user_id' : ''};
        function openModal(id) {
            $('#responsive-modalTop').modal('show');
            vm.walletForm = {'user_id' : id};
        }

        /* to save buyCredit after add and edit  */
        function saveWallet() {
            // console.log(vm.walletForm);
            WalletTopUpService.topupWallet(vm.walletForm).then(function (response) {
                // console.log(response);
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $('#responsive-modalTop').modal('hide');
                        toastr.success(response.data.message, 'Top Up');
                        $state.go('backoffice.walletTopUp');
                    } else {
                        toastr.error(response.data.message, 'Top Up');
                    }
                } else {
                    toastr.error(response.data.message, 'Top Up');
                }
            }, function (error) {
                // console.log(error);
                toastr.error('Internal server error', 'Top Up');
            });
        }//END saveWallet();

        /* to get clients list */
        function getClientsList(newPage, obj) {
            vm.progressbar.start();
            vm.totalClient = 0;
            vm.clientList = [];
            vm.clientsListCheck = false;
            // vm.walletForm = {user_id : ''};
            WalletTopUpService.getClients(newPage, obj, vm.orderInfo).then(function (response) {
                // console.log(response);
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.clientsListCheck = true;
                    if (response.data.clients && response.data.clients.length > 0) {
                        vm.totalClient = response.data.total;
                        vm.clientList = response.data.clients;
                        // var clientLength = response.data.clients;
                        // for(var i =0; i < clientLength.length; i++) {
                        //     vm.walletForm.user_id = clientLength[i].user_id;
                        // }
                        // // angular.forEach(vm.clientList, function(value, key) {
                        // //     vm.walletForm.user_id = value.user_id;
                        //     console.log(vm.walletForm.user_id);                             
                        // }); 

                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getClientsList();

        /* to get Credit history */
        function getClientCreditHistory(client_id) {
            $location.hash('top');
            $anchorScroll();
            // console.log(client_id);
            WalletTopUpService.getClientCreditHistory(client_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.creditHistory = response.data.creditHistory;
                        // console.log(vm.creditHistory);
                    } else {
                        toastr.error(response.data.message, 'Wallet TopUp');
                        $state.go('backoffice.walletTopUp');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Wallet TopUp');
            });
        }//END getClientCreditHistory();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getClientsList(1, '');
        }//END reset();               
    }

}());
