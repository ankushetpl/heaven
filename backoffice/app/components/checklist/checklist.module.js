(function () {

    angular.module('checklistApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var checklistPath = 'app/components/checklist/';
        $stateProvider
            .state('backoffice.checklist', {
                url: 'checklist',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/index.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
            .state('backoffice.checklist.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/form.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
            .state('backoffice.checklist.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/form.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            })
            .state('backoffice.checklist.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: checklistPath + 'views/view.html',
                        controller: 'ChecklistController',
                        controllerAs: 'checklist'
                    }
                }
            });
    }

}());