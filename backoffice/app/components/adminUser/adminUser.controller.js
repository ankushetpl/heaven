(function () {
    "use strict";
    angular.module('adminUserApp')
        .controller('AdminUserController', AdminUserController);

    AdminUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AdminUserService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll', '$timeout'];

    function AdminUserController($scope, $rootScope, $state, $location, AdminUserService, toastr, SweetAlert, ngProgressFactory, $anchorScroll, $timeout) {
        var vm = this;

        vm.getAdminUserList = getAdminUserList;
        vm.getSingleAdminUser = getSingleAdminUser;
        vm.saveAdminUser = saveAdminUser;
        vm.deleteAdminUser = deleteAdminUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.sort = sort;
        vm.reset = reset;
        vm.resetForm = resetForm;   
        vm.isDisabled = isDisabled; 

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Administrator';
        vm.totalAdminUser = 0;
        vm.adminUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.adminUserForm = { user_id: '', usertypeFlag: '1', registertypeFlag: '0' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        $scope.regex = /^[@a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.adminUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAdminUser(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting admin user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAdminUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAdminUserList(newPage, searchInfo);
        }//END changePage();

        /* to save admin user after add and edit  */
        function saveAdminUser() {
            AdminUserService.saveAdminUser(vm.adminUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Administrator');
                        $state.go('backoffice.adminUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Administrator');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Administrator');
                            }else{
                                toastr.error(response.data.message, 'Administrator');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Administrator');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Administrator');
            });
        }//END saveAdminUser();

        /* to get admin user list */
        function getAdminUserList(newPage, obj) {
            vm.progressbar.start();
            var userID = $rootScope.user.user_id;
            vm.totalAdminUser = 0;
            vm.adminUserList = [];
            vm.adminUserListCheck = false;
            AdminUserService.getAdminUser(newPage, obj, vm.orderInfo, '1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.users && response.data.users.length > 0) {
                        if(response.data.total > 1){
                            vm.totalAdminUser = response.data.total - 1;
                        }else{
                            vm.totalAdminUser = response.data.total;
                        }
                                                
                        vm.adminUserList = response.data.users;
                        angular.forEach(vm.adminUserList, function(value, key) {
                            if(value.user_id == userID){
                                vm.adminUserList.splice(key);
                            }
                        });
                        vm.adminUserListCheck = true;
                        vm.progressbar.complete();                        
                    }else{
                        vm.progressbar.complete();
                        vm.adminUserListCheck = true;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAdminUserList();

        /* to get single admin user */
        function getSingleAdminUser(user_id) { 
            $location.hash('top');
            $anchorScroll();

            var UserType = 1;           
            AdminUserService.getSingleAdminUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAdminUser = response.data.data;
                        vm.adminUserForm.user_id = response.data.data.user_id;
                        vm.adminUserForm.usertype = response.data.data.user_role_id;
                        vm.adminUserForm.registertype = response.data.data.user_registertype;
                        vm.adminUserForm.user_name = response.data.data.admin_name;
                        vm.adminUserForm.user_email = response.data.data.admin_email;
                        vm.adminUserForm.user_phone = response.data.data.admin_phone;
                        vm.adminUserForm.status = response.data.data.status; 
                        vm.isDisabledEmail = true;
                    } else {
                        toastr.error(response.data.message, 'Administrator');
                        $state.go('backoffice.adminUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Administrator');
            });
        }//END getSingleAdminUser();

        /** to delete a admin user **/
        function deleteAdminUser(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Administrator?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AdminUserService.deleteAdminUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.adminUserList.splice(index, 1);
                            vm.totalAdminUser = vm.totalAdminUser - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Administrator');
                    });
                }
            });
        }//END deleteAdminUser();

        /* to change active/inactive status of admin user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
                var txtSuccess = "Account inactivated successfully";
            }else{
                var statusId = 1;  
                var txtSuccess = "Account activated successfully";
            }
            
            var data = { user_id: status.user_id, status: statusId };
            AdminUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", txtSuccess, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAdminUserList(1, '');
        }//END reset();

        /* to reset all parameters  */
        function resetForm() {
            vm.adminUserForm = { user_id: '', usertypeFlag: '1', registertypeFlag: '0' };
        }//END resetForm();
    }

}());
