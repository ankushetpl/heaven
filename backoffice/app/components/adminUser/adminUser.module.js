(function () {

    angular.module('adminUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var adminUserPath = 'app/components/adminUser/';
        $stateProvider
            .state('backoffice.adminUser', {
                url: 'adminUser',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/index.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
            .state('backoffice.adminUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/form.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
            .state('backoffice.adminUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/form.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
            .state('backoffice.adminUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: adminUserPath + 'views/view.html',
                        controller: 'AdminUserController',
                        controllerAs: 'adminUser'
                    }
                }
            })
    }

}());