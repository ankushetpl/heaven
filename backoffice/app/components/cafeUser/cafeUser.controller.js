(function () {
    "use strict";
    angular.module('cafeUserApp')
        .controller('CafeUserController', CafeUserController);

    CafeUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'CafeUserService', 'toastr', 'SweetAlert', 'Upload', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function CafeUserController($scope, $rootScope, $state, $location, CafeUserService, toastr, SweetAlert, Upload, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getCafeUserList = getCafeUserList;
        vm.getSingleCafeUser = getSingleCafeUser;
        vm.saveCafeUser = saveCafeUser;
        vm.deleteCafeUser = deleteCafeUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.suspendForms = suspendForms; 
        vm.changeSuspend = changeSuspend;
        vm.close = close;
        vm.getAllWorker = getAllWorker;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;
        vm.resetForm = resetForm;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;

        vm.getAllCountries();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Internet Cafe';
        vm.totalCafeUser = 0;
        vm.cafeUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.cafeUserForm = { user_id: '', usertypeFlag: '2', registertypeFlag: '0' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        $scope.regex = /^[@a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.cafeUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleCafeUser(path[3]);
                    vm.getAllWorker(path[3]);
                }, 300 );                
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting Internet Cafe list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getCafeUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getCafeUserList(newPage, searchInfo);
        }//END changePage();

        /* to save Internet Cafe after add and edit  */
        function saveCafeUser() {    
            CafeUserService.saveCafeUser(vm.cafeUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Internet Cafe');
                        $state.go('backoffice.cafeUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Internet Cafe');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Internet Cafe');
                            }else{
                                toastr.error(response.data.message, 'Internet Cafe');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END saveCafeUser();

        /* to get Internet Cafe list */
        function getCafeUserList(newPage, obj) {
            vm.progressbar.start();
            vm.totalCafeUser = 0;
            vm.cafeUserList = [];
            vm.cafeUserListCheck = false;
            CafeUserService.getCafeUser(newPage, obj, vm.orderInfo, '2').then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.cafeUserListCheck = true;
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalCafeUser = response.data.total;
                        vm.cafeUserList = response.data.users;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getCafeUserList();

        /* to get single Internet Cafe */
        function getSingleCafeUser(user_id) { 
            $location.hash('top');
            $anchorScroll();
            var UserType = 2;           
            CafeUserService.getSingleCafeUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleCafeUser = response.data.data;
                        vm.cafeUserForm.user_id = response.data.data.user_id;
                        vm.cafeUserForm.usertype = response.data.data.user_role_id;
                        vm.cafeUserForm.registertype = response.data.data.user_registertype;
                        vm.cafeUserForm.user_name = response.data.data.cafeusers_name;
                        vm.cafeUserForm.user_email = response.data.data.cafeusers_email;
                        vm.cafeUserForm.user_phone = response.data.data.cafeusers_phone;
                        vm.cafeUserForm.user_address = response.data.data.cafeusers_address;
                        vm.cafeUserForm.status = response.data.data.status;
                        vm.cafeUserForm.cafeusers_country = response.data.data.cafeusers_country;
                        vm.cafeUserForm.cafeusers_state = response.data.data.cafeusers_state;
                        vm.cafeUserForm.cafeusers_city = response.data.data.cafeusers_city;
                        vm.cafeUserForm.cafeusers_suburb = response.data.data.cafeusers_suburb;

                        vm.isDisabledEmail = true;
                        vm.getStatesByID(vm.singleCafeUser);
                        vm.getCityByID(vm.singleCafeUser);
                        vm.getSuburbByID(vm.singleCafeUser);

                        if(response.data.data.cafeusers_country != 0){
                            var country = response.data.data.cafeusers_country;
                            vm.singleCafeUser.country = '';
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleCafeUser.country = value.name;
                                }                               
                            }); 
                        }                       

                        if(response.data.data.cafeusers_image != 0){
                            var file_id = response.data.data.cafeusers_image;
                            CafeUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleCafeUser.file = responses.data.file.file_base_url;
                                        vm.singleCafeUser.cafe_image = response.data.data.cafeusers_image;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }
                    } else {
                        toastr.error(response.data.message, 'Internet Cafe');
                        $state.go('backoffice.cafeUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Internet Cafe');
            });
        }//END getSingleCafeUser();

        /** to delete a Internet Cafe **/
        function deleteCafeUser(id, index) {
            var dataCheck = { user_id: id };
            CafeUserService.checkData(dataCheck).then(function(responses) {   
                             
                if (responses.status == 200) {
                    if (responses.data.status == 1 && responses.data.data.length > 0 ) {
                        var message = "Sorry! Internet Cafe cannot be deleted. Please delete workers first.";
                        SweetAlert.swal(message, "Error");
                        return false;                                
                    }

                    if (responses.data.status == 0 ) {                        
                        SweetAlert.swal({
                            title: "Are you sure you want to delete this Internet Cafe?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            html: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                CafeUserService.deleteCafeUser(id).then(function (response) {
                                    if (response.data.status == 1) {
                                        SweetAlert.swal("Deleted!", response.data.message, "success");
                                        vm.cafeUserList.splice(index, 1);
                                        vm.totalCafeUser = vm.totalCafeUser - 1; 
                                    } else {
                                        SweetAlert.swal("Deleted!", response.data.message, "error");
                                    }
                                }, function (error) {
                                    toastr.error(error.data.error, 'Internet Cafe');
                                });
                            }
                        });
                    }
                } else {
                    toastr.error(responses.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });  
            
        }//END deleteCafeUser();

        /* to change active/inactive status of Internet Cafe */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }

            var dataCheck = { user_id: status.user_id };
            CafeUserService.checkData(dataCheck).then(function(responses) {   
                             
                if (responses.status == 200) {
                    if (responses.data.status == 1 && responses.data.data.length > 0 ) {
                        var message = "Sorry! Internet Cafe cannot be deactivated. Please deactivate workers first.";
                        SweetAlert.swal(message, "Error");
                        return false;                                
                    }

                    if (responses.data.status == 0 ) {                        
                        var data = { user_id: status.user_id, status: statusId };
                        CafeUserService.changeStatus(data).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {                        
                                    SweetAlert.swal("Success!", response.data.message, "success");
                                    $state.reload();
                                    return true;                        
                                }
                            } else {
                                toastr.error(response.data.error, 'Error');
                                return false;
                            }
                        },function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                } else {
                    toastr.error(responses.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });  
            
        }//END changeStatus()

        vm.suspendForm = { user_id: '', is_suspend: '', user_role_id: '' };

        /* to show suspend form for Internet Cafe */ 
        function suspendForms(suspend){

            if(suspend.status == 0){
                SweetAlert.swal("Error!", "Sorry! please approve this Internet Cafe first!", "error");
                return false;      
            }

            if(suspend.is_suspend == 1){
                SweetAlert.swal({
                    title: "Are you sure you want to unsuspend this Internet Cafe?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, unsuspend it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    html: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        var data = { user_id: suspend.user_id, is_suspend: suspend.is_suspend, roleID : suspend.user_role_id, cafeusers_suspendStart: '', cafeusers_suspendEnd: '' };
                        CafeUserService.changeSuspend(data).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {                        
                                    SweetAlert.swal("Suspend!", response.data.message, "success");
                                    $state.reload();
                                    return true;                        
                                }else {
                                    SweetAlert.swal("Suspend!", response.data.message, "error");
                                }
                            } else {
                                toastr.error(response.data.error, 'Error');
                                return false;
                            }
                        },function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                });  
            }else{
                vm.suspendForm.user_id = suspend.user_id; 
                vm.suspendForm.is_suspend = suspend.is_suspend;
                vm.suspendForm.roleID = suspend.user_role_id; 
                vm.suspendForm.cafeusers_suspendStart = '';
                vm.suspendForm.cafeusers_suspendEnd = '';

                var logElem = angular.element("#responsiveSuspend");
                logElem.modal("show");                
            }            
        }//END suspendForms()

        /* to change active/inactive suspend of Internet Cafe */ 
        function changeSuspend() {
            SweetAlert.swal({
                title: "Are you sure you want to suspend this Internet Cafe?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, suspend it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var logElem = angular.element(".modal-backdrop");
                    logElem.removeClass("show"); 
                    logElem.addClass("hide"); 

                    var logElem = angular.element("body");
                    logElem.removeClass("modal-open"); 
                    
                    CafeUserService.changeSuspend(vm.suspendForm).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to close model pupup */
        function close(){
            // vm.suspendForm.cafeusers_suspendStart = '';
            // delete vm.suspendForm.cafeusers_suspendStart;
            // vm.suspendForm.$setPristine();
            // vm.suspendForm.$setUntouched();
            // vm.email = vm.password = '';

            vm.getCafeUserList(1, '');   
            $state.go('backoffice.cafeUser');                              
        }//END close();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getCafeUserList(1, '');
        }//END reset();

        /* Get all workers*/
        function getAllWorker(user_id){
            var UserType = 3;  
            vm.totalActive = 0;
            vm.totalInactive = 0;
            vm.totalSuspend = 0;
            vm.totalWorker = 0;
            vm.workerUseList = [];
            CafeUserService.getAllWorker(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1 && response.data.data.length > 0  ) {
                        vm.totalActive = response.data.active;
                        vm.totalInactive = response.data.inactive;
                        vm.totalSuspend = response.data.suspend;
                        vm.workerUseList = response.data.data;
                        vm.totalWorker = response.data.data.length;                        
                    } else {
                        vm.totalWorker = 0;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Internet Cafe');
            });
        }// END getAllWorker();

        /* Get All Countries */
        function getAllCountries(){
            CafeUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){
            CafeUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            CafeUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        delete vm.cafeUserForm.cafeusers_city;
                        vm.hideSuburb = true;
                        delete vm.cafeUserForm.cafeusers_suburb;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            CafeUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }else{
                        vm.hideSuburb = true;
                        delete vm.cafeUserForm.cafeusers_suburb;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getSuburbByID();


         /* to reset all parameters  */
        function resetForm() {
            vm.cafeUserForm = { user_id: '', usertypeFlag: '2', registertypeFlag: '0' };
        }//END resetForm();

    }

}());
