(function () {
    "use strict";
    angular
        .module('assessorsUserApp')
        .service('AssessorsUserService', AssessorsUserService);

    AssessorsUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AssessorsUserService($http, APPCONFIG, $q) {

        /* save assessors user */
        function saveAssessorsUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
                
                if (typeof obj.assessors_country !== undefined && obj.assessors_country !== '') {
                    requestData['cafeusers_country'] = obj.assessors_country;
                }

                if (typeof obj.assessors_state !== undefined && obj.assessors_state !== '') {
                    requestData['cafeusers_state'] = obj.assessors_state;
                }

                if (typeof obj.assessors_city !== undefined && obj.assessors_city !== '') {
                    requestData['cafeusers_city'] = obj.assessors_city;
                }

                if (typeof obj.assessors_suburb !== undefined && obj.assessors_suburb !== '') {
                    requestData['cafeusers_suburb'] = obj.assessors_suburb;
                }

                if (typeof obj.assessors_image !== undefined && obj.assessors_image !== '') {
                    requestData['assessors_image'] = obj.assessors_image;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveAssessorsUser();

        /* to get all assessors */
        function getAssessorsUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';
            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof usertype !== 'undefined' && usertype !== '') {
                requestData['usertype'] = usertype;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.assessors_name !== 'undefined' && obj.assessors_name !== '') {
                    requestData['admin_name'] = obj.assessors_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
                
                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getAssessorsUser();

        /* to get single assessors */
        function getSingleAssessorsUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleAssessorsUser();

        /* to delete a assessors from database */
        function deleteAssessorsUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteAssessorsUser();

        /* to change active/inactive status of assessors */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of assessors */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }  
                
                if (obj.roleID != undefined || obj.roleID != "") {
                    requestData["usertype"] = obj.roleID;
                }

                if (obj.assessors_suspendStart != undefined || obj.assessors_suspendStart != "") {
                    requestData["suspendStart"] = obj.assessors_suspendStart;
                }

                if (obj.assessors_suspendEnd != undefined || obj.assessors_suspendEnd != "") {
                    requestData["suspendEnd"] = obj.assessors_suspendEnd;
                }
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get States by ID
        function getStatesByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allStatesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.assessors_country !== 'undefined' && obj.assessors_country !== '') {
                    requestData['ID'] = parseInt(obj.assessors_country);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get City by ID
        function getCityByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCitiesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.assessors_state !== 'undefined' && obj.assessors_state !== '') {
                    requestData['ID'] = parseInt(obj.assessors_state);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get Suburb by ID
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.assessors_city !== 'undefined' && obj.assessors_city !== '') {
                    requestData['ID'] = parseInt(obj.assessors_city);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAssessorsUser: getAssessorsUser,
            getSingleAssessorsUser: getSingleAssessorsUser,
            saveAssessorsUser: saveAssessorsUser,
            deleteAssessorsUser: deleteAssessorsUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend,
            getImage: getImage,
            getAllCountries: getAllCountries,
            getStatesByID: getStatesByID,
            getCityByID: getCityByID,
            getSuburbByID: getSuburbByID
        }

    };//END AssessorsUserService()
}());
