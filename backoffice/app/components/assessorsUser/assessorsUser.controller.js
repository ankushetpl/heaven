(function () {
    "use strict";
    angular.module('assessorsUserApp')
        .controller('AssessorsUserController', AssessorsUserController);

    AssessorsUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AssessorsUserService', 'toastr', 'SweetAlert', '$timeout', 'Upload', 'APPCONFIG', 'ngProgressFactory', '$anchorScroll'];

    function AssessorsUserController($scope, $rootScope, $state, $location, AssessorsUserService, toastr, SweetAlert, $timeout, Upload, APPCONFIG, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getAssessorsUserList = getAssessorsUserList;
        vm.getSingleAssessorsUser = getSingleAssessorsUser;
        vm.saveAssessorsUser = saveAssessorsUser;
        vm.deleteAssessorsUser = deleteAssessorsUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.suspendForms = suspendForms; 
        vm.changeSuspend = changeSuspend;
        vm.close = close;
        vm.sort = sort;
        vm.reset = reset;
        vm.resetForm = resetForm;  

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;
        vm.isDisabled = isDisabled;

        vm.getAllCountries();
        
        vm.progressbar = ngProgressFactory.createInstance();
        
        $rootScope.headerTitle = 'Assessors';
        vm.totalAssessorsUser = 0;
        vm.assessorsUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.imageFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.assessorsUserForm = { user_id: '', usertypeFlag: '4', registertypeFlag: '0' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        $scope.regex = /^[@a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.assessorsUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleAssessorsUser(path[3]);
                }, 300 );                
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting assessors user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAssessorsUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAssessorsUserList(newPage, searchInfo);
        }//END changePage();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    vm.assessorsUserForm.assessors_image = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to save assessors user after add and edit  */
        function saveAssessorsUser() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            if (typeof vm.assessorsUserForm.file !== 'undefined' && vm.assessorsUserForm.file !== '') { //check if from is valid
                
                if(vm.assessorsUserForm.file.size > 0 && vm.assessorsUserForm.file.name !== '' ){
                    vm.upload(vm.assessorsUserForm.file); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        AssessorsUserService.saveAssessorsUser(vm.assessorsUserForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    toastr.success(response.data.message, 'Assessors');
                                    $state.go('backoffice.assessorsUser');
                                } else {
                                    if (response.data.status == 2) {
                                        vm.progressbar.complete();
                                        vm.isDisabledButton = false;
                                        toastr.error(response.data.message, 'Assessors');
                                    }else{
                                        if (response.data.status == 3) {
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, 'Assessors');
                                        }else{
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, 'Assessors');
                                        }
                                    }                        
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Assessors');
                            }
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Assessors');
                        });
                    }
                }, 5000 );
            }else{
                AssessorsUserService.saveAssessorsUser(vm.assessorsUserForm).then(function (response) {
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            vm.progressbar.complete();
                            toastr.success(response.data.message, 'Assessors');
                            $state.go('backoffice.assessorsUser');
                        } else {
                            if (response.data.status == 2) {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Assessors');
                            }else{
                                if (response.data.status == 3) {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Assessors');
                                }else{
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Assessors');
                                }
                            }                        
                        }
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Assessors');
                    }
                }, function (error) {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error('Internal server error', 'Assessors');
                });
            }
        }//END saveAssessorsUser();

        /* to get assessors user list */
        function getAssessorsUserList(newPage, obj) {
            vm.progressbar.start();
            vm.totalAssessorsUser = 0;
            vm.assessorsUserList = [];
            vm.assessorsUserListCheck = false;
            AssessorsUserService.getAssessorsUser(newPage, obj, vm.orderInfo, '4').then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.assessorsUserListCheck = true;
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalAssessorsUser = response.data.total;
                        vm.assessorsUserList = response.data.users;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAssessorsUserList();

        /* to get single assessors user */
        function getSingleAssessorsUser(user_id) { 
            $location.hash('top');
            $anchorScroll();
            var UserType = 4;           
            AssessorsUserService.getSingleAssessorsUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAssessorsUser = response.data.data;
                        
                        vm.assessorsUserForm.user_id = response.data.data.user_id;
                        vm.assessorsUserForm.usertype = response.data.data.user_role_id;
                        vm.assessorsUserForm.registertype = response.data.data.user_registertype;
                        vm.assessorsUserForm.user_name = response.data.data.assessors_name;
                        vm.assessorsUserForm.user_email = response.data.data.assessors_email;
                        vm.assessorsUserForm.user_phone = response.data.data.assessors_phone;
                        vm.assessorsUserForm.user_address = response.data.data.assessors_address;
                        vm.assessorsUserForm.status = response.data.data.status; 
                        vm.assessorsUserForm.assessors_country = response.data.data.assessors_country;
                        vm.assessorsUserForm.assessors_state = response.data.data.assessors_state;
                        vm.assessorsUserForm.assessors_city = response.data.data.assessors_city;
                        vm.assessorsUserForm.assessors_suburb = response.data.data.assessors_suburb;
                        vm.assessorsUserForm.assessors_image = response.data.data.assessors_image;


                        vm.isDisabledEmail = true;
                        vm.getStatesByID(vm.singleAssessorsUser);
                        vm.getCityByID(vm.singleAssessorsUser);
                        vm.getSuburbByID(vm.singleAssessorsUser);

                        if(response.data.data.assessors_country != 0){
                            var country = response.data.data.assessors_country;
                            vm.singleAssessorsUser.country = '';
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleAssessorsUser.country = value.name;
                                }                               
                            }); 
                        }       

                        
                        if(response.data.data.assessors_image != 0){
                            var file_id = response.data.data.assessors_image;
                            AssessorsUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleAssessorsUser.file = responses.data.file.file_base_url;
                                        vm.assessorsUserForm.file = responses.data.file.file_base_url;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }  

                    } else {
                        toastr.error(response.data.message, 'Assessors');
                        $state.go('backoffice.assessorsUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessors');
            });
        }//END getSingleAssessorsUser();

        /** to delete a assessors **/
        function deleteAssessorsUser(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Assessors ?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AssessorsUserService.deleteAssessorsUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.assessorsUserList.splice(index, 1);
                            vm.totalAssessorsUser = vm.totalAssessorsUser - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessors User');
                    });
                }
            });
        }//END deleteAssessorsUser();

        /* to change active/inactive status of assessors */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            AssessorsUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        vm.suspendForm = { user_id: '', is_suspend: '', user_role_id: '' };

        /* to show suspend form for assessors */ 
        function suspendForms(suspend){

            if(suspend.status == 0){
                SweetAlert.swal("Error!", "Sorry! please approve this Assessors first!", "error");
                return false;      
            }

            if(suspend.is_suspend == 1){
                SweetAlert.swal({
                    title: "Are you sure you want to remove suspension of this assessor?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, unsuspend it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    html: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        var data = { user_id: suspend.user_id, is_suspend: suspend.is_suspend, roleID : suspend.user_role_id, assessors_suspendStart: '', assessors_suspendEnd: '' };
                        AssessorsUserService.changeSuspend(data).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) { 

                                       SweetAlert.swal("Suspend!", 'The assessor suspension removed.', "success");
                                       
                                    $state.reload();
                                    return true;                        
                                }else {
                                    SweetAlert.swal("Suspend!", response.data.message, "error");
                                }
                            } else {
                                toastr.error(response.data.error, 'Error');
                                return false;
                            }
                        },function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                });  
            }else{
                vm.suspendForm.user_id = suspend.user_id; 
                vm.suspendForm.is_suspend = suspend.is_suspend;
                vm.suspendForm.roleID = suspend.user_role_id; 
                vm.suspendForm.assessors_suspendStart = '';
                vm.suspendForm.assessors_suspendEnd = '';

                var logElem = angular.element("#responsiveSuspend");
                logElem.modal("show");                
            }            
        }//END suspendForms()

        /* to change active/inactive suspend of assessors */ 
        function changeSuspend() {
            SweetAlert.swal({
                title: "Are you sure you want to suspend this assessor?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, suspend it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var logElem = angular.element(".modal-backdrop");
                    logElem.removeClass("show"); 
                    logElem.addClass("hide"); 

                    var logElem = angular.element("body");
                    logElem.removeClass("modal-open"); 
                    
                    AssessorsUserService.changeSuspend(vm.suspendForm).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", 'The assessor is suspended successfully.', "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to close model pupup */
        function close(){
            // vm.suspendForm.cafeusers_suspendStart = '';
            // delete vm.suspendForm.cafeusers_suspendStart;
            // vm.suspendForm.$setPristine();
            // vm.suspendForm.$setUntouched();
            // vm.email = vm.password = '';

            vm.getAssessorsUserList(1, '');   
            $state.go('backoffice.assessors');                              
        }//END close();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAssessorsUserList(1, '');
        }//END reset();

        /* Get All Countries */
        function getAllCountries(){
            AssessorsUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Assessors');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessors');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){
            AssessorsUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Assessors');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessors');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            
            AssessorsUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        delete vm.assessorsUserForm.assessors_city;
                        vm.hideSuburb = true;
                        delete vm.assessorsUserForm.assessors_suburb;
                    }
                } else {
                    toastr.error(response.data.message, 'Assessors');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessors');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            AssessorsUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }else{
                        vm.hideSuburb = true;
                        delete vm.assessorsUserForm.assessors_suburb;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Assessors');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessors');
            });
        }// END getSuburbByID();


        /* to reset all parameters  */
        function resetForm() {
            vm.assessorsUserForm = { user_id: '', usertypeFlag: '4', registertypeFlag: '0' };
        }//END resetForm();
    }

}());
