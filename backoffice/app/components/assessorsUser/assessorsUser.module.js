(function () {

    angular.module('assessorsUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var assessorsUserPath = 'app/components/assessorsUser/';
        $stateProvider
            .state('backoffice.assessorsUser', {
                url: 'assessorsUser',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/index.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
            .state('backoffice.assessorsUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/form.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
            .state('backoffice.assessorsUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/form.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
            .state('backoffice.assessorsUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: assessorsUserPath + 'views/view.html',
                        controller: 'AssessorsUserController',
                        controllerAs: 'assessorsUser'
                    }
                }
            })
    }

}());