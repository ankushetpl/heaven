(function () {
    "use strict";
    angular
        .module('sharedApp')
        .service('sharedService', sharedService)
        .factory('sharedSocket', sharedSocket);

    sharedService.$inject = ['$http', 'APPCONFIG', '$q', 'authServices'];

    function sharedService($http, APPCONFIG, $q, authServices) {
        var self = this;
        self.validatePhoneNumber = validatePhoneNumber;
        self.validateNumber = validateNumber;
        self.formatPrice = formatPrice;
        self.updateUserDetails = updateUserDetails;
        self.updateUserPhoto = updateUserPhoto;
        self.changePassword = changePassword;
        self.indexPerPage = indexPerPage;
        
        /* to validate phone number */
        function validatePhoneNumber(number) {
            var regex = /^[+()\d-]+$/;
            if(!regex.test(number) && number != undefined) {
        		return "Invalid phone number";
            } 
	        return true;
        } 

        /* to validate number, no negative value accepted */
        function validateNumber(number) {
            var regex = /^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/;
	        if(!regex.test(number) && number != undefined) {
	            return "Invalid number";
	        } else {
	            if(number == 0 || number < 0) {
	                return "Invalid number";
	            } else {
	                return true;
	            }
	        }
        }
        
        /* to format price */
        function formatPrice(n) {
            n = parseFloat(n);
            return n.toFixed(2).replace(/./g, function (c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }

        /* to update user profile */
        function updateUserDetails(userInfo) {
            console.log(userInfo);
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'front-update-user-details';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("token", authServices.getAuthToken());
                    formData.append("firstname", userInfo.firstname);
                    formData.append("lastname", userInfo.lastname);
                    formData.append("headline", userInfo.headline);
                    formData.append("bio", userInfo.bio);
                    return formData;
                },
                headers: {
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateUserDetails();

        /* to update user profile photo */
        function updateUserPhoto(photo) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'front-update-user-photo';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("token", authServices.getAuthToken());
                    formData.append("photo", photo);
                    return formData;
                },
                headers: {
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateUserPhoto();      

        /* change password */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'change-password';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("token", authServices.getAuthToken());
                    formData.append("oldpassword", obj.oldpassword);
                    formData.append("password", obj.newpassword);
                    formData.append("cpassword", obj.confirmpassword);
                    return formData;
                },
                headers: {
                    'Content-Type': undefined
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        } //END changePassword();

        //to retrive first and last index to be displayed
        function indexPerPage(currentPage, length, countsPerPage) {
            var fromIndex = 0;
            var toIndex = 0;
            var tempIndex = 0;
            if (length != 0) {
                fromIndex = (currentPage - 1) * (countsPerPage) + 1;
                tempIndex = fromIndex + (countsPerPage - 1);
                if (tempIndex > length) {
                    toIndex = fromIndex + (length - 1);
                } else {
                    toIndex = tempIndex;
                }
            } else {
                fromIndex = 0;
                toIndex = 0;
            }
            var index = { 'fromIndex': fromIndex, 'toIndex': toIndex };
            return index;

        }//END indexPerPage()

        return self;

    }; //END sharedService()    

    sharedSocket.$inject = ['$rootScope', 'toastr'];

    function sharedSocket($rootScope, toastr) {
        var socket = io.connect('http://localhost:3000');

        function funOn(eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        }

        function funEmit(eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }

        function addUser(user_id) {
            funEmit('add:user', user_id);
        }

        function sendMessage(sender_id, receiver_id, message) {
            funEmit('send:message', { sender_id, receiver_id, message });
        }

        function getMessage(callback) {
            funOn('receive:message', callback);
        }

        function getMessagesRequest(sender_id, receiver_id, page) {
            funEmit('receive:messages', { sender_id, receiver_id, page });
        }

        function getMessagesResponse(callback) {
            funOn('receive:messages', callback);
        }

        return {
            funOn: funOn,
            funEmit: funEmit,
            addUser: addUser,
            sendMessage: sendMessage,
            getMessagesRequest: getMessagesRequest,
            getMessagesResponse: getMessagesResponse,
            getMessage: getMessage
        };
    }; //END sharedSocket()



}());