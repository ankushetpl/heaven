(function () {

    angular.module('pageApp', [
        'ckeditor'
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var pagePath = 'app/components/page/';
        $stateProvider
            .state('backoffice.page', {
                url: 'page',
                views: {
                    'content@backoffice': {
                        templateUrl: pagePath + 'views/index.html',
                        controller: 'PageController',
                        controllerAs: 'page'
                    }
                }
            })
            .state('backoffice.page.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: pagePath + 'views/form.html',
                        controller: 'PageController',
                        controllerAs: 'page'
                    }
                }
            })
            .state('backoffice.page.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: pagePath + 'views/form.html',
                        controller: 'PageController',
                        controllerAs: 'page'
                    }
                }
            })
            .state('backoffice.page.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: pagePath + 'views/view.html',
                        controller: 'PageController',
                        controllerAs: 'page'
                    }
                }
            })
    }

}());