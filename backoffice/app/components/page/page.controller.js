(function () {
    "use strict";
    angular.module('pageApp')
        .controller('PageController', PageController);

    PageController.$inject = ['$scope', '$rootScope', '$state', '$location', 'PageService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function PageController($scope, $rootScope, $state, $location, PageService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getPageList = getPageList;
        vm.getSinglePage = getSinglePage;
        vm.savePage = savePage;
        vm.deletePage = deletePage;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Static Page';
        vm.totalPage = 0;
        vm.pagePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.pageForm = { page_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];
        vm.locationList = [{ id : 1, menu : 'Menu'}, { id : 2, menu : 'Footer'}];

        $scope.regex = /^[^`~!#@$%\^&*()_+={}|[\]\\:';"<>?,./0-9]*$/;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.page');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSinglePage(path[3]);
            }
        }

        // Editor options.
        $scope.page_html = {
            language: 'en',
            allowedContent: true,
            entities: false
        };

        vm.pageForm.page_html = "<h2>Hello World!</h2>";

        function isDisabled() {
            vm.isDisabled = true;
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getPageList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getPageList(newPage, searchInfo);
        }//END changePage();
        
        /* to save page after add and edit  */
        function savePage() {
            PageService.savePage(vm.pageForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Page');
                        $state.go('backoffice.page');
                    } else {
                        toastr.error(response.data.message, 'Page');
                    }
                } else {
                    toastr.error(response.data.message, 'Page');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Page');
            });
        }//END savePage();

        /* to get pages list */
        function getPageList(newPage, obj) {
            vm.progressbar.start();
            vm.totalPage = 0;
            vm.pageList = [];
            vm.pageListCheck = false;
            PageService.getPage(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.pageListCheck = true;
                    if (response.data.page && response.data.page.length > 0) {
                        vm.totalPage = response.data.total;
                        vm.pageList = response.data.page;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getPageList();

        /* to get single page */
        function getSinglePage(page_id) {
            $location.hash('top');
            $anchorScroll();
            PageService.getSinglePage(page_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singlePage = response.data.page;
                        vm.pageForm = response.data.page;
                    } else {
                        toastr.error(response.data.message, 'Page');
                        $state.go('backoffice.page');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Page');
            });
        }//END getSinglePage();

        /** to delete a page **/
        function deletePage(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Page?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    PageService.deletePage(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.pageList.splice(index, 1);
                            vm.totalPage = vm.totalPage - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Pages');
                    });
                }
            });
        }//END deletePage();

        /* to change active/inactive status of page */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { page_id: status.page_id, status: statusId };
            PageService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getPageList(1, '');
        }//END reset();               
    }

}());
