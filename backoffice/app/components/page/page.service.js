(function () {
    "use strict";
    angular
        .module('pageApp')
        .service('PageService', PageService);

    PageService.$inject = ['$http', 'APPCONFIG', '$q'];

    function PageService($http, APPCONFIG, $q) {

        /* save page */
        function savePage(obj) {
            var URL = APPCONFIG.APIURL + 'page/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.page_id !== 'undefined' && obj.page_id !== '') {
                    requestData['page_id'] = obj.page_id;
                    URL = APPCONFIG.APIURL + 'page/edit';
                }

                if (typeof obj.page_name !== 'undefined' && obj.page_name !== '') {
                    requestData['page_name'] = obj.page_name;
                }

                if (typeof obj.page_html !== 'undefined' && obj.page_html !== '') {
                    requestData['page_html'] = obj.page_html;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.page_location !== 'undefined' && obj.page_location !== '') {
                    requestData['page_location'] = obj.page_location;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END savePage();

        /* to get all pages */
        function getPage(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'page';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.page_id !== 'undefined' && obj.page_id !== '') {
                    requestData['page_id'] = parseInt(obj.page_id);
                }

                if (typeof obj.page_name !== 'undefined' && obj.page_name !== '') {
                    requestData['page_name'] = obj.page_name;
                }

                if (typeof obj.page_location !== 'undefined' && obj.page_location !== '') {
                    requestData['page_location'] = obj.page_location;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            return this.runHttp(URL, requestData);
        }//END getPage();

        /* to get single page */
        function getSinglePage(page_id) {
            var URL = APPCONFIG.APIURL + 'page/view';
            var requestData = {};

            if (typeof page_id !== undefined && page_id !== '') {
                requestData['page_id'] = page_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSinglePage();

        /* to delete a page from database */
        function deletePage(page_id) {
            var URL = APPCONFIG.APIURL + 'page/delete';
            var requestData = {};

            if (typeof page_id !== undefined && page_id !== '') {
                requestData['page_id'] = page_id;
            }

            return this.runHttp(URL, requestData);
        } //END deletePage();

        /* to change active/inactive status of page */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'page/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.page_id != undefined && obj.page_id != "") {
                    requestData["page_id"] = obj.page_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getPage: getPage,
            getSinglePage: getSinglePage,
            savePage: savePage,
            deletePage: deletePage,
            changeStatus: changeStatus
        }

    };//END PageService()
}());
