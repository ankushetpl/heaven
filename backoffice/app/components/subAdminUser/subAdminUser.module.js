(function () {

    angular.module('subAdminUserApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var subAdminPath = 'app/components/subAdminUser/';
        $stateProvider
            .state('backoffice.subAdminUser', {
                url: 'subAdminUser',
                views: {
                    'content@backoffice': {
                        templateUrl: subAdminPath + 'views/index.html',
                        controller: 'SubAdminController',
                        controllerAs: 'subAdminUser'
                    }
                }
            })
            .state('backoffice.subAdminUser.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: subAdminPath + 'views/form.html',
                        controller: 'SubAdminController',
                        controllerAs: 'subAdminUser'
                    }
                }
            })
            .state('backoffice.subAdminUser.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: subAdminPath + 'views/form.html',
                        controller: 'SubAdminController',
                        controllerAs: 'subAdminUser'
                    }
                }
            })
            .state('backoffice.subAdminUser.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: subAdminPath + 'views/view.html',
                        controller: 'SubAdminController',
                        controllerAs: 'subAdminUser'
                    }
                }
            })
    }

}());