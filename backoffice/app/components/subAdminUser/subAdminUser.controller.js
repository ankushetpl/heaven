(function () {
    "use strict";
    angular.module('subAdminUserApp')
        .controller('SubAdminController', SubAdminController);

    SubAdminController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SubAdminService', 'toastr', 'SweetAlert', '$anchorScroll'];

    function SubAdminController($scope, $rootScope, $state, $location, SubAdminService, toastr, SweetAlert, $anchorScroll) {
        var vm = this;

        vm.getAdminUserList = getAdminUserList;
        vm.getSingleAdminUser = getSingleAdminUser;
        vm.saveAdminUser = saveAdminUser;
        // vm.deleteAdminUser = deleteAdminUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        // vm.changeStatus = changeStatus;
        vm.sort = sort;
        vm.reset = reset;
        vm.getAll = getAll;
        vm.getAll();

        $rootScope.headerTitle = 'Sub Admin';
        vm.totalAdminUser = 0;
        vm.adminUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.adminUserForm = { user_id: '', usertypeFlag: '6', registertypeFlag: '0' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.subAdminUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAdminUser(path[3]);
            }
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting admin user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAdminUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAdminUserList(newPage, searchInfo);
        }//END changePage();

        /* to save admin user after add and edit  */
        function saveAdminUser() {
            SubAdminService.saveAdminUser(vm.adminUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        
                        var id = response.data.data;
                        SubAdminService.savePermissions(vm.modulesList, id).then(function (respon) {
                            if (respon.status == 200) {
                                if (respon.data.status == 1) {
                                    toastr.success(response.data.message, 'Sub Admin');
                                    $state.go('backoffice.subAdminUser');
                                }else{
                                    toastr.error(respon.data.message, 'Sub Admin');
                                }
                            } else {
                                toastr.error(respon.data.message, 'Sub Admin');
                            }
                        }, function (error) {
                            toastr.error('Internal server error', 'Sub Admin');
                        });
                       
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Sub Admin');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Sub Admin');
                            }else{
                                toastr.error(response.data.message, 'Sub Admin');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Sub Admin');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Sub Admin');
            });
        }//END saveAdminUser();

        /* to get sub admin user list */
        function getAdminUserList(newPage, obj) {
            vm.totalAdminUser = 0;
            vm.adminUserList = [];
            vm.adminUserListCheck = false;
            SubAdminService.getAdminUser(newPage, obj, vm.orderInfo, '6').then(function (response) {
                if (response.status == 200) {
                    vm.adminUserListCheck = true;
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalAdminUser = response.data.total;
                        vm.adminUserList = response.data.users;                                 
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAdminUserList();

        /* to get single admin user */
        function getSingleAdminUser(user_id) { 
            $location.hash('top');
            $anchorScroll();
            var UserType = 6;   
            vm.permissionList = [];        
            SubAdminService.getSingleAdminUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleAdminUser = response.data.data;
                        vm.adminUserForm.user_id = response.data.data.user_id;
                        vm.adminUserForm.usertype = response.data.data.user_role_id;
                        vm.adminUserForm.registertype = response.data.data.user_registertype;
                        vm.adminUserForm.user_name = response.data.data.admin_name;
                        vm.adminUserForm.user_email = response.data.data.admin_email;
                        vm.adminUserForm.user_phone = response.data.data.admin_phone;
                        vm.adminUserForm.status = response.data.data.status; 
                        vm.isDisabled = true;

                        SubAdminService.getModulesID(user_id).then(function (respon) {
                            if (respon.status == 200) {
                                if (respon.data.data && respon.data.data.length > 0) {
                                    vm.permissionList = respon.data.data;
                                }
                            }
                        }, function (error) {
                            toastr.error(error.data.error, 'Sub Admin');
                        });
                    } else {
                        toastr.error(response.data.message, 'Sub Admin');
                        $state.go('backoffice.subAdminUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Sub Admin');
            });
        }//END getSingleAdminUser();

        /** to delete a admin user **/
        function deleteAdminUser(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Administrator?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    SubAdminService.deleteAdminUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.adminUserList.splice(index, 1);
                            vm.totalAdminUser = vm.totalAdminUser - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Administrator');
                    });
                }
            });
        }//END deleteAdminUser();

        /* to change active/inactive status of admin user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            SubAdminService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAdminUserList(1, '');
        }//END reset();

        /* to get all modules listing */
        function getAll(){
            vm.modulesList = [];
            SubAdminService.getAll().then(function (response) {
                console.log(response);
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.modulesList = response.data.data;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }// END getAll();
    }

}());
