(function () {

    angular.module('feedbackApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var feedbackPath = 'app/components/feedback/';
        $stateProvider
            .state('backoffice.feedback', {
                url: 'feedback',
                views: {
                    'content@backoffice': {
                        templateUrl: feedbackPath + 'views/index.html',
                        controller: 'FeedbackController',
                        controllerAs: 'feedback'
                    }
                }
            })
            // .state('backoffice.feedback.create', {
            //     url: '/create',
            //     views: {
            //         'content@backoffice': {
            //             templateUrl: feedbackPath + 'views/form.html',
            //             controller: 'FeedbackController',
            //             controllerAs: 'feedback'
            //         }
            //     }
            // })
            // .state('backoffice.feedback.edit', {
            //     url: '/edit/:id',
            //     views: {
            //         'content@backoffice': {
            //             templateUrl: feedbackPath + 'views/form.html',
            //             controller: 'FeedbackController',
            //             controllerAs: 'feedback'
            //         }
            //     }
            // })
            .state('backoffice.feedback.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: feedbackPath + 'views/view.html',
                        controller: 'FeedbackController',
                        controllerAs: 'feedback'
                    }
                }
            })
    }

}());