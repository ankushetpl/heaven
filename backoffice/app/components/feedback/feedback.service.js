(function () {
    "use strict";
    angular
        .module('feedbackApp')
        .service('FeedbackService', FeedbackService);

    FeedbackService.$inject = ['$http', 'APPCONFIG', '$q'];

    function FeedbackService($http, APPCONFIG, $q) {

        /* to get all feed back */
        function getFeedback(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'feedback';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.feedback_id !== 'undefined' && obj.feedback_id !== '') {
                    requestData['feedback_id'] = parseInt(obj.feedback_id);
                }

                if (typeof obj.feedback_note !== 'undefined' && obj.feedback_note !== '') {
                    requestData['feedback_note'] = obj.feedback_note;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = parseInt(obj.role_id);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getFeedback();

        /* to get single feed back */
        function getSingleFeedback(feedback_id) {
            var URL = APPCONFIG.APIURL + 'feedback/view';
            var requestData = {};

            if (typeof feedback_id !== undefined && feedback_id !== '') {
                requestData['feedback_id'] = feedback_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleFeedback();

        /* to delete a feed back from database */
        function deleteFeedback(feedback_id) {
            var URL = APPCONFIG.APIURL + 'feedback/delete';
            var requestData = {};

            if (typeof feedback_id !== undefined && feedback_id !== '') {
                requestData['feedback_id'] = feedback_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteFeedback();

        /* to delete Multiple feed back from database */
        function deleteSelected(ID) {
            var URL = APPCONFIG.APIURL + 'feedback/deleteM';
            var requestData = {};

            if (typeof ID !== undefined && ID !== '') {
                requestData['ID'] = ID;
            }

            return this.runHttp(URL, requestData);
        } //END deleteSelected();

        /* to get all role */
        function getAllRoll(pageNum) {
            var URL = APPCONFIG.APIURL + 'role';
            var requestData = { pageNum }; 
            requestData['orderColumn'] = 'role_name'; 
            requestData['orderBy'] = 'ASC';          
            return this.runHttp(URL, requestData);
        }//END getAllRoll();

         /* to get single user name */
        function getUserName(obj) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['UserType'] = parseInt(obj.role_id);
                }

                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }
            }
        return this.runHttp(URL, requestData);
        } //END getUserName();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getFeedback: getFeedback,
            getSingleFeedback: getSingleFeedback,
            deleteFeedback: deleteFeedback,
            getAllRoll: getAllRoll,
            getUserName: getUserName,
            deleteSelected: deleteSelected
        }

    };//END FeedbackService()
}());
