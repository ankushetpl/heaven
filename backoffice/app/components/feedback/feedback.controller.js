(function () {
    "use strict";
    angular.module('feedbackApp')
        .controller('FeedbackController', FeedbackController);

    FeedbackController.$inject = ['$scope', '$rootScope', '$state', '$location', 'FeedbackService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function FeedbackController($scope, $rootScope, $state, $location, FeedbackService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getFeedbackList = getFeedbackList;
        vm.getSingleFeedback = getSingleFeedback;
        vm.deleteFeedback = deleteFeedback;
        vm.deleteSelected = deleteSelected;
        vm.getAllRoll = getAllRoll;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;
        vm.unCheck = unCheck;
                
        vm.getAllRoll();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Feed Back';
        vm.totalFeedback = 0;
        vm.feedbackPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        // vm.feedbackForm = { faq_id: '', user_id: '1' };
        // vm.statusList = { '0': 'Not Approved', '1': 'Approved' };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.feedback');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleFeedback(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        function unCheck(){
            var logElem = angular.element("#selection");
            logElem.prop("checked", false);
        }

        $scope.checkAll = function() {

            if ($scope.statu.checkall) {
                $scope.statu.checkall = true;
                $(':checkbox').each(function(i) {
                    $(this).prop("checked", true);
                });
            } else {
                $scope.statu.checkall = false;
                $(':checkbox').each(function(i) {
                    $(this).prop("checked", false);
                });
            }
        };

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getFeedbackList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getFeedbackList(newPage, searchInfo);
        }//END changePage();

        /* to get feed back list */
        function getFeedbackList(newPage, obj) {
            vm.progressbar.start();
            vm.totalFeedback = 0;
            vm.feedbackList = [];
            vm.feedbackListCheck = false;
            FeedbackService.getFeedback(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.feedbackListCheck = true;
                    if (response.data.feedback && response.data.feedback.length > 0) {
                        vm.totalFeedback = response.data.total;
                        vm.feedbackList = response.data.feedback;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getFeedbackList();

        /* to get single feed back */
        function getSingleFeedback(feedback_id) {
            $location.hash('top');
            $anchorScroll();
            FeedbackService.getSingleFeedback(feedback_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFeedback = response.data.feedback;
                        vm.feedbackForm = response.data.feedback;
                        FeedbackService.getUserName(vm.feedbackForm).then(function (responses) {
                            if (responses.data.status == 1) {
                                if(vm.feedbackForm.role_id == 5 ){
                                    vm.feedbackForm.createName = responses.data.data.clients_name;
                                    vm.singleFeedback.createName = responses.data.data.clients_name;
                                }else{
                                    vm.feedbackForm.createName = responses.data.data.workers_name;
                                    vm.singleFeedback.createName = responses.data.data.workers_name;
                                }
                            } else {
                                toastr.error(responses.data.message, 'Feed back');
                                $state.go('backoffice.feedback');
                            }
                        }, function (error) {
                            toastr.error(error.data.error, 'Feed Back');
                        });
                    } else {
                        toastr.error(response.data.message, 'Feed back');
                        $state.go('backoffice.feedback');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Feed back');
            });
        }//END getSingleFeedback();

        /** to delete a feed back **/
        function deleteFeedback(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Feed Back ?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    FeedbackService.deleteFeedback(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.feedbackList.splice(index, 1);
                            vm.totalFeedback = vm.totalFeedback - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Feed Back');
                    });
                }
            });
        }//END deleteFeedback();

        /* all role type */
        function getAllRoll() {
            vm.roleList = [];
            FeedbackService.getAllRoll('1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllRoll()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getFeedbackList(1, '');
        }//END reset(); 
    
        /** to delete multiple feed back **/
        function deleteSelected() {
            var ID = [];
            var IDNew = [];
            $(':checkbox:checked').each(function(i) {
                ID[i] = $(this).val();
            });
            
            angular.forEach(ID, function(values){
                if(values != 'on'){
                    IDNew.push(values);
                }
            });
            
            if (IDNew.length == 0) {
                SweetAlert.swal("Error!", "Sorry, Please select at least one feedback!", "error");
                return false;
            } else {
                SweetAlert.swal({
                    title: "Are you sure you want to delete this Feed Back ?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    html: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        FeedbackService.deleteSelected(IDNew).then(function (response) {
                            if (response.data.status == 1) {
                                SweetAlert.swal("Deleted!", response.data.message, "success");
                                $state.reload(); 
                            } else {
                                SweetAlert.swal("Deleted!", response.data.message, "error");
                            }
                        }, function (error) {
                            toastr.error(error.data.error, 'Feed Back');
                        });
                    }
                });
            }
        }//END deleteSelected();
        
    }

}());
