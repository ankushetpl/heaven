(function () {
    "use strict";
    angular
        .module('suspensionApp')
        .service('SuspensionService', SuspensionService);

    SuspensionService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SuspensionService($http, APPCONFIG, $q) {

        /* save  */
        function saveData(obj) {
            var URL = APPCONFIG.APIURL + 'suspension/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.category_id !== 'undefined' && obj.category_id !== '') {
                    requestData['category_id'] = obj.category_id;
                    URL = APPCONFIG.APIURL + 'suspension/edit';
                }

                if (typeof obj.category_type !== 'undefined' && obj.category_type !== '') {
                    requestData['category_type'] = obj.category_type;
                }

                if (typeof obj.category_time !== 'undefined' && obj.category_time !== '') {
                    requestData['category_time'] = obj.category_time;
                }

                if (typeof obj.category_period !== 'undefined' && obj.category_period !== '') {
                    requestData['category_period'] = obj.category_period;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = obj.created_at;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

            }
            
            return this.runHttp(URL, requestData);
        } //END saveData();

        /* to get all data */
        function getData(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'suspension';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.category_id !== 'undefined' && obj.category_id !== '') {
                    requestData['category_id'] = parseInt(obj.category_id);
                }

                if (typeof obj.category_type !== 'undefined' && obj.category_type !== '') {
                    requestData['category_type'] = obj.category_type;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = obj.created_at;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getData();

        /* to get single suspension */
        function getSingleData(category_id) {
            var URL = APPCONFIG.APIURL + 'suspension/view';
            var requestData = {};

            if (typeof category_id !== undefined && category_id !== '') {
                requestData['category_id'] = category_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleData();

        /* to delete a data from database */
        function deleteData(category_id) {
            var URL = APPCONFIG.APIURL + 'suspension/delete';
            var requestData = {};

            if (typeof category_id !== undefined && category_id !== '') {
                requestData['category_id'] = category_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteData();

        /* to change active/inactive status of suspension */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'suspension/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.category_id != undefined && obj.category_id != "") {
                    requestData["category_id"] = obj.category_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getData: getData,
            getSingleData: getSingleData,
            saveData: saveData,
            deleteData: deleteData,
            changeStatus: changeStatus
        }

    };//END SuspensionService()
}());
