(function () {

    angular.module('suspensionApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var suspensionPath = 'app/components/suspension/';
        $stateProvider
            .state('backoffice.suspension', {
                url: 'suspension',
                views: {
                    'content@backoffice': {
                        templateUrl: suspensionPath + 'views/index.html',
                        controller: 'SuspensionController',
                        controllerAs: 'suspension'
                    }
                }
            })
            .state('backoffice.suspension.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: suspensionPath + 'views/form.html',
                        controller: 'SuspensionController',
                        controllerAs: 'suspension'
                    }
                }
            })
            .state('backoffice.suspension.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: suspensionPath + 'views/form.html',
                        controller: 'SuspensionController',
                        controllerAs: 'suspension'
                    }
                }
            })
            .state('backoffice.suspension.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: suspensionPath + 'views/view.html',
                        controller: 'SuspensionController',
                        controllerAs: 'suspension'
                    }
                }
            });
    }

}());