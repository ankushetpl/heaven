(function () {
    "use strict";
    angular.module('suspensionApp')
        .controller('SuspensionController', SuspensionController);

    SuspensionController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SuspensionService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function SuspensionController($scope, $rootScope, $state, $location, SuspensionService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getList = getList;
        vm.getSingle = getSingle;
        vm.save = save;
        vm.deleteData = deleteData;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        // $rootScope.headerTitle = 'Suspension Category';
        $rootScope.headerTitle = 'Suspension';
        vm.total = 0;
        vm.PerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.Form = { category_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];
        vm.periodList = [{ id : 0, status: 'Month'}, { id : 1, status: 'Year'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.suspension');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingle(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getList(newPage, searchInfo);
        }//END changePage();

        /* to save data after add and edit  */
        function save() {
            SuspensionService.saveData(vm.Form).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Suspension');
                        $state.go('backoffice.suspension');
                    } else {
                        toastr.error(response.data.message, 'Suspension');
                    }
                } else {
                    toastr.error(response.data.message, 'Suspension');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Suspension');
            });
        }//END save();

        /* to get list */
        function getList(newPage, obj) {
            vm.progressbar.start();
            vm.total = 0;
            vm.list = [];
            vm.listCheck = false;
            SuspensionService.getData(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.listCheck = true;
                    if (response.data.category && response.data.category.length > 0) {
                        vm.total = response.data.total;
                        vm.list = response.data.category;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getList();

        /* to get single */
        function getSingle(category_id) {
            $location.hash('top');
            $anchorScroll();
            SuspensionService.getSingleData(category_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.single = response.data.category;
                        vm.Form = response.data.category;                        
                    } else {
                        toastr.error(response.data.message, 'Suspension');
                        $state.go('backoffice.suspension');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Suspension');
            });
        }//END getSingle();

        /** to delete a Suspension **/
        function deleteData(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this category?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    SuspensionService.deleteData(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.list.splice(index, 1);
                            // getList(index, '');
                            vm.total = vm.total - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Suspension');
                    });
                }
            });
        }//END deleteData();

        /* to change active/inactive status */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { category_id: status.category_id, status: statusId };
            SuspensionService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()
       
        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getList(1, '');
        }//END reset();               
    }

}());
