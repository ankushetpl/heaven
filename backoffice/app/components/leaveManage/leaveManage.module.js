(function () {

    angular.module('leaveManageApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var leaveManagePath = 'app/components/leaveManage/';
        $stateProvider
            .state('backoffice.leaveManage', {
                url: 'leaveManage',
                views: {
                    'content@backoffice': {
                        templateUrl: leaveManagePath + 'views/index.html',
                        controller: 'LeaveManageController',
                        controllerAs: 'leaveManage'
                    }
                }
            })
            .state('backoffice.leaveManage.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: leaveManagePath + 'views/view.html',
                        controller: 'LeaveManageController',
                        controllerAs: 'leaveManage'
                    }
                }
            });
    }

}());