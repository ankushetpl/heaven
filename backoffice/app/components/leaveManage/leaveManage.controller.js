(function () {
    "use strict";
    angular.module('leaveManageApp')
        .controller('LeaveManageController', LeaveManageController);

    LeaveManageController.$inject = ['$scope', '$rootScope', '$state', '$location', 'LeaveManageService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function LeaveManageController($scope, $rootScope, $state, $location, LeaveManageService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getList = getList;
        vm.getSingle = getSingle;
        vm.leaveReasonList = leaveReasonList;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Worker Leaves';
        vm.total = 0;
        vm.PerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        
        vm.leaveReasonList()

        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.leaveManage');
                return false;
            }  else {   
                vm.progressbar.start();             
                vm.getSingle(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getList(newPage, searchInfo);
        }//END changePage();

        /* to get Leaves list */
        function getList(newPage, obj) {
            vm.progressbar.start();
            vm.list = [];
            vm.listCheck = false;
            LeaveManageService.getLeaveData(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.listCheck = true;
                    if (response.data.workerLeaves && response.data.workerLeaves.length > 0) {
                        vm.total = response.data.total;
                        vm.list = response.data.workerLeaves;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getList();

        /* to get single Leaves */
        function getSingle(workerleaves_id) {
            $location.hash('top');
            $anchorScroll();
            LeaveManageService.getSingleData(workerleaves_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.single = response.data.workerLeaves;
                    } else {
                        toastr.error(response.data.message, 'Worker Leaves');
                        $state.go('backoffice.leaveManage');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Worker Leaves');
            });
        }//END getSingle();

        /* get all reason */
        function leaveReasonList(){
            vm.leaveList = [];
            LeaveManageService.leaveReasonList().then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.leaveList = response.data.reasons;
                    } else {
                        toastr.error(response.data.message, 'Worker Leaves');
                        $state.go('backoffice.leaveManage');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Worker Leaves');
            });
        }// END leaveReasonList();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getList(1, '');
        }//END reset();               
    }

}());
