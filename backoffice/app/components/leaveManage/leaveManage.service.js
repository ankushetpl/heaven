(function () {
    "use strict";
    angular
        .module('leaveManageApp')
        .service('LeaveManageService', LeaveManageService);

    LeaveManageService.$inject = ['$http', 'APPCONFIG', '$q'];

    function LeaveManageService($http, APPCONFIG, $q) {

        /* to get all LeaveData */
        function getLeaveData(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'leaveManage';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workerleaves_id !== 'undefined' && obj.workerleaves_id !== '') {
                    requestData['workerleaves_id'] = parseInt(obj.workerleaves_id);
                }

                if (typeof obj.worker_id !== 'undefined' && obj.worker_id !== '') {
                    requestData['worker_id'] = parseInt(obj.worker_id);
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.workerleaves_from !== 'undefined' && obj.workerleaves_from !== '') {
                    requestData['workerleaves_from'] = moment(obj.workerleaves_from).format('YYYY-MM-DD');
                }

                if (typeof obj.workerleaves_leaves_id !== 'undefined' && obj.workerleaves_leaves_id !== '') {
                    requestData['workerleaves_leaves_id'] = parseInt(obj.workerleaves_leaves_id);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }

                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getLeaveData();

        /* to get single leaveManage */
        function getSingleData(workerleaves_id) {
            var URL = APPCONFIG.APIURL + 'leaveManage/view';
            var requestData = {};

            if (typeof workerleaves_id !== undefined && workerleaves_id !== '') {
                requestData['workerleaves_id'] = workerleaves_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleData();

        /* to get leave reason list */
        function leaveReasonList(){
            var URL = APPCONFIG.APIURL + 'leaveManage/allLeaveType';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END leaveReasonList();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getLeaveData: getLeaveData,
            getSingleData: getSingleData,
            leaveReasonList: leaveReasonList
        }

    };//END LeaveManageService()
}());
