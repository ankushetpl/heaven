(function () {
    'use strict';
    angular.module('authApp').controller('AuthController', AuthController);

    AuthController.$inject = ['$scope', '$rootScope', '$location', 'authServices', '$state', 'toastr', '$timeout', 'ngProgressFactory'];

    function AuthController($scope, $rootScope, $location, authServices, $state, toastr, $timeout, ngProgressFactory) {
        var vm = this;
        vm.login = login;
        vm.forgotPassword = forgotPassword;

        vm.user = { usertypeFlag: '1', device_token: '', device_type: '1' };
        
        $rootScope.bodyClass = '';

        vm.progressbar = ngProgressFactory.createInstance();
        
        function login(loginInfo) {
            // $state.go('backoffice.dashboard');
            vm.progressbar.start();
            vm.isDisabledButton = true;
            authServices.checkLogin(loginInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success("Login successful", "Login");
                        var token = response.data.data.device_token;
                        authServices.setAuthToken(token); //Set auth token
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        $state.go('backoffice.dashboard');                        
                    }else{
                        toastr.error(response.data.message, "Error");
                        vm.isDisabledButton = false;
                        vm.progressbar.complete();
                    }                    
                } else {
                    toastr.error(response.data.message, "Error");
                    vm.isDisabledButton = false;
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
                vm.isDisabledButton = false;
                vm.progressbar.complete();
            });
        }; //END login()   

        function forgotPassword(userInfo) {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            authServices.checkForgotPassword(userInfo).then(function (response) {
                if (response.status == 200) {
                    if(response.data.status == 1){
                        toastr.success(response.data.message, "Forgot Password");
                        vm.isDisabledButton = false;
                        $timeout( function(){
                            vm.progressbar.complete();
                            $state.go('auth');                            
                        }, 1500 );
                    }else{
                        toastr.error(response.data.message, "Forgot Password");
                        vm.isDisabledButton = false;
                        vm.progressbar.complete();
                    }                    
                } else {
                    toastr.error(response.data.message, "Forgot Password");
                    vm.isDisabledButton = false;
                    vm.progressbar.complete();
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
                vm.isDisabledButton = false;
                vm.progressbar.complete();
            });
        }; //END forgotPassword() 
    };
}());