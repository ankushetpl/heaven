(function () {
    "use strict";
    angular.module('clientUserApp')
        .controller('ClientUserController', ClientUserController);

    ClientUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ClientUserService', 'toastr', 'SweetAlert', 'Upload', '$timeout', 'APPCONFIG', 'ngProgressFactory', '$anchorScroll'];

    function ClientUserController($scope, $rootScope, $state, $location, ClientUserService, toastr, SweetAlert, Upload, $timeout, APPCONFIG, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getClientUserList = getClientUserList;
        vm.getSingleClientUser = getSingleClientUser;
        vm.saveClientUser = saveClientUser;
        vm.deleteClientUser = deleteClientUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.suspendForms = suspendForms;
        vm.changeSuspend = changeSuspend; 
        vm.sort = sort;
        vm.reset = reset;
        vm.close = close;
        vm.isDisabled = isDisabled;
        vm.resetForm = resetForm;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;

        vm.getAllCountries();

        vm.progressbar = ngProgressFactory.createInstance();
        vm.progressbar.setColor('blue');

        $rootScope.headerTitle = 'Clients';
        vm.totalClientUser = 0;
        vm.clientUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.imageFlag = false;
        vm.title = 'Add New';
        vm.clientUserForm = { user_id: '', usertypeFlag: '5', registertypeFlag: '0' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        $scope.regex = /^[@a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.clientUser');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleClientUser(path[3]);
                }, 300 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting client user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getClientUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getClientUserList(newPage, searchInfo);
        }//END changePage();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status == 1) {
                    vm.clientUserForm.clients_image = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload        

        /* to save client user after add and edit  */
        function saveClientUser() {
            
            if(vm.clientUserForm.clients_mobile === vm.clientUserForm.clients_phone) {
                toastr.error('Contact number and alternate contact number can not be same.', 'Client');

            } else {
                vm.isDisabledButton = true;
                vm.progressbar.start();
                if (typeof vm.clientUserForm.file !== 'undefined' && vm.clientUserForm.file !== '') {
                    if(vm.clientUserForm.file.size > 0 && vm.clientUserForm.file.name != '' ){
                        vm.upload(vm.clientUserForm.file); //call upload function   
                    }else{
                        vm.imageFlag = true;
                    }
                    
                    $timeout( function(){
                        if(vm.imageFlag == true){ 
                            ClientUserService.saveClientUser(vm.clientUserForm).then(function (response) {
                                if (response.status == 200) {
                                    if (response.data.status == 1) {
                                        vm.progressbar.complete();
                                        toastr.success(response.data.message, 'Client');
                                        $state.go('backoffice.clientUser');
                                    } else {
                                        if (response.data.status == 2) {
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, 'Client');
                                        }else{
                                            if (response.data.status == 3) {
                                                vm.progressbar.complete();
                                                vm.isDisabledButton = false;
                                                toastr.error(response.data.message, 'Client');
                                            }else{
                                                vm.progressbar.complete();
                                                vm.isDisabledButton = false;
                                                toastr.error(response.data.message, 'Client');
                                            }
                                        }                        
                                    }
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Client');
                                }
                            }, function (error) {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error('Internal server error', 'Client');
                            });
                        }
                    }, 3000 );
                }else{
                    ClientUserService.saveClientUser(vm.clientUserForm).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {
                                vm.progressbar.complete();
                                toastr.success(response.data.message, 'Client');
                                $state.go('backoffice.clientUser');
                            } else {
                                if (response.data.status == 2) {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Client');
                                }else{
                                    if (response.data.status == 3) {
                                        vm.progressbar.complete();
                                        vm.isDisabledButton = false;
                                        toastr.error(response.data.message, 'Client');
                                    }else{
                                        vm.progressbar.complete();
                                        vm.isDisabledButton = false;
                                        toastr.error(response.data.message, 'Client');
                                    }
                                }                        
                            }
                        } else {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error(response.data.message, 'Client');
                        }
                    }, function (error) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error('Internal server error', 'Client');
                    });
                }
            }
                
        }//END saveClientUser();

        /* to get client user list */
        function getClientUserList(newPage, obj) {
            vm.totalClientUser = 0;
            vm.clientUserList = [];
            vm.clientUserListCheck = false;
            ClientUserService.getClientUser(newPage, obj, vm.orderInfo, '5').then(function (response) {
                if (response.status == 200) {
                    vm.clientUserListCheck = true;
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalClientUser = response.data.total;
                        vm.clientUserList = response.data.users;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getClientUserList();

        /* to get single client user */
        function getSingleClientUser(user_id) { 
            $location.hash('top');
            $anchorScroll();
            var UserType = 5;           
            ClientUserService.getSingleClientUser(user_id, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleClientUser = response.data.data;
                        
                        vm.clientUserForm.user_id = response.data.data.user_id;
                        vm.clientUserForm.usertype = response.data.data.user_role_id;
                        vm.clientUserForm.registertype = response.data.data.user_registertype;
                        vm.clientUserForm.clients_name = response.data.data.clients_name;
                        vm.clientUserForm.clients_email = response.data.data.clients_email;
                        vm.clientUserForm.clients_phone = response.data.data.clients_phone;
                        vm.clientUserForm.clients_mobile = response.data.data.clients_mobile;
                        vm.clientUserForm.clients_address = response.data.data.clients_address;
                        vm.clientUserForm.status = response.data.data.status; 
                        vm.clientUserForm.clients_country = response.data.data.clients_country;
                        vm.clientUserForm.clients_state = response.data.data.clients_state;
                        vm.clientUserForm.clients_city = response.data.data.clients_city;
                        vm.clientUserForm.clients_suburb = response.data.data.clients_suburb;
                        vm.clientUserForm.clients_image = response.data.data.clients_image;

                        vm.isDisabledEmail = true;
                        vm.getStatesByID(vm.singleClientUser);
                        vm.getCityByID(vm.singleClientUser);
                        vm.getSuburbByID(vm.singleClientUser);

                        if(response.data.data.clients_country != 0){
                            var country = response.data.data.clients_country;
                            vm.singleClientUser.country = '';
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleClientUser.country = value.name;
                                }                               
                            }); 
                        } 

                        if(response.data.data.clients_image != 0){
                            var file_id = response.data.data.clients_image;
                            ClientUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleClientUser.file = responses.data.file.file_base_url;
                                        vm.clientUserForm.file = responses.data.file.file_base_url;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }                        
                    } else {
                        toastr.error(response.data.message, 'Client');
                        $state.go('backoffice.clientUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Client');
            });
        }//END getSingleClientUser();

        /** to delete a client user **/
        function deleteClientUser(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Client?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ClientUserService.deleteClientUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.clientUserList.splice(index, 1);
                            vm.totalClientUser = vm.totalClientUser - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Client');
                    });
                }
            });
        }//END deleteClientUser();

        /* to change active/inactive status of client user */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            
            var data = { user_id: status.user_id, status: statusId };
            ClientUserService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        vm.suspendForm = { user_id: '', is_suspend: '', user_role_id: '' };

        /* to show suspend form for Client */ 
        function suspendForms(suspend) {

            if(suspend.status == 0){
                SweetAlert.swal("Error!", "Sorry! please approve this client first!", "error");
                return false;      
            }
            
            if(suspend.is_suspend == 1){
                SweetAlert.swal({
                    title: "Sure you want to unsuspend this Client?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, unsuspend it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    html: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        var data = { user_id: suspend.user_id, is_suspend: suspend.is_suspend, roleID : suspend.user_role_id, clients_suspendStart: '', clients_suspendEnd: '' };
                        ClientUserService.changeSuspend(data).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {                        
                                    SweetAlert.swal("Suspend!", response.data.message, "success");
                                    $state.reload();
                                    return true;                        
                                }else {
                                    SweetAlert.swal("Suspend!", response.data.message, "error");
                                }
                            } else {
                                toastr.error(response.data.error, 'Error');
                                return false;
                            }
                        },function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                }); 
                
            }else{
                vm.suspendForm.user_id = suspend.user_id; 
                vm.suspendForm.is_suspend = suspend.is_suspend;
                vm.suspendForm.roleID = suspend.user_role_id; 
                
                delete vm.suspendForm.clients_suspendStart;
                delete vm.suspendForm.clients_suspendEnd;

                $scope.suspendForm.$setPristine();

                var logElem = angular.element("#responsiveSuspend");
                logElem.modal("show");            
            }                    
        }//END suspendForms()

        /* to change active/inactive suspend of Client */ 
        function changeSuspend() {
            SweetAlert.swal({
                title: "Are you sure you want to suspend this Client?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, suspend it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var logElem = angular.element(".modal-backdrop");
                    logElem.removeClass("show"); 
                    logElem.addClass("hide"); 

                    var logElem = angular.element("body");
                    logElem.removeClass("modal-open"); 
                    
                    ClientUserService.changeSuspend(vm.suspendForm).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to close model pupup */
        function close(){
            delete vm.suspendForm.clients_suspendStart;
            delete vm.suspendForm.clients_suspendEnd;

            $scope.suspendForm.$setPristine();

            vm.getClientUserList(1, '');   
            $state.go('backoffice.clientUser');
        }//END close();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getClientUserList(1, '');
        }//END reset();

        /* Get All Countries */
        function getAllCountries(){
            ClientUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Client');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){
            ClientUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Client');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            
            ClientUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        delete vm.clientUserForm.clients_city;
                        vm.hideSuburb = true;
                        delete vm.clientUserForm.clients_suburb;
                    }
                } else {
                    toastr.error(response.data.message, 'Client');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            ClientUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }else{
                        vm.hideSuburb = true;
                        delete vm.clientUserForm.clients_suburb;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Client');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client');
            });
        }// END getSuburbByID();


        /* to reset all parameters  */
        function resetForm() {
            vm.clientUserForm = { user_id: '', usertypeFlag: '5', registertypeFlag: '0' };
        }//END resetForm();

    }

}());
