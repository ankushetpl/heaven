(function () {
    "use strict";
    angular
        .module('clientUserApp')
        .service('ClientUserService', ClientUserService);

    ClientUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ClientUserService($http, APPCONFIG, $q) {

        /* save client user */
        function saveClientUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.clients_address !== undefined && obj.clients_address !== '') {
                    requestData['address'] = obj.clients_address;
                }

                if (typeof obj.clients_email !== undefined && obj.clients_email !== '') {
                    requestData['email'] = obj.clients_email;
                }

                if (typeof obj.clients_name !== undefined && obj.clients_name !== '') {
                    requestData['name'] = obj.clients_name;
                }

                if (typeof obj.clients_phone !== undefined && obj.clients_phone !== '') {
                    requestData['phone'] = obj.clients_phone;
                }

                if (typeof obj.clients_mobile !== undefined && obj.clients_mobile !== '') {
                    requestData['clients_mobile'] = obj.clients_mobile;
                }

                if (typeof obj.clients_image !== undefined && obj.clients_image !== '') {
                    requestData['assessors_image'] = obj.clients_image;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
                
                if (typeof obj.clients_country !== undefined && obj.clients_country !== '') {
                    requestData['cafeusers_country'] = obj.clients_country;
                }

                if (typeof obj.clients_state !== undefined && obj.clients_state !== '') {
                    requestData['cafeusers_state'] = obj.clients_state;
                }

                if (typeof obj.clients_city !== undefined && obj.clients_city !== '') {
                    requestData['cafeusers_city'] = obj.clients_city;
                }

                if (typeof obj.clients_suburb !== undefined && obj.clients_suburb !== '') {
                    requestData['cafeusers_suburb'] = obj.clients_suburb;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveClientUser();

        /* to get all client user  */
        function getClientUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof usertype !== 'undefined' && usertype !== '') {
                requestData['usertype'] = usertype;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['admin_name'] = obj.clients_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getClientUser();

        /* to get single client */
        function getSingleClientUser(user_id, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleClientUser();

        /* to delete a client from database */
        function deleteClientUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteClientUser();

        /* to change active/inactive status of client user */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'user/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change active/inactive suspend of assessors */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.user_id != undefined && obj.user_id != "") {
                    requestData["user_id"] = obj.user_id;
                }

                if (obj.is_suspend != undefined || obj.is_suspend != "") {
                    requestData["is_suspend"] = obj.is_suspend;
                }  
                
                if (obj.roleID != undefined || obj.roleID != "") {
                    requestData["usertype"] = obj.roleID;
                }

                if (obj.clients_suspendStart != undefined || obj.clients_suspendStart != "") {
                    requestData["suspendStart"] = obj.clients_suspendStart;
                }

                if (obj.clients_suspendEnd != undefined || obj.clients_suspendEnd != "") {
                    requestData["suspendEnd"] = obj.clients_suspendEnd;
                }
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get States by ID
        function getStatesByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allStatesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.clients_country !== 'undefined' && obj.clients_country !== '') {
                    requestData['ID'] = parseInt(obj.clients_country);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get City by ID
        function getCityByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCitiesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.clients_state !== 'undefined' && obj.clients_state !== '') {
                    requestData['ID'] = parseInt(obj.clients_state);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get Suburb by ID
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.clients_city !== 'undefined' && obj.clients_city !== '') {
                    requestData['ID'] = parseInt(obj.clients_city);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getClientUser: getClientUser,
            getSingleClientUser: getSingleClientUser,
            saveClientUser: saveClientUser,
            deleteClientUser: deleteClientUser,
            changeStatus: changeStatus,
            changeSuspend: changeSuspend,
            getImage: getImage,
            getAllCountries: getAllCountries,
            getStatesByID: getStatesByID,
            getCityByID: getCityByID,
            getSuburbByID: getSuburbByID
        }

    };//END ClientUserService()
}());
