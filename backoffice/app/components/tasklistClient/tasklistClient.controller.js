(function () {
    "use strict";
    angular.module('tasklistClientApp')
        .controller('TasklistClientController', TasklistClientController);

    TasklistClientController.$inject = ['$scope', '$rootScope', '$state', '$location', 'TasklistClientService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function TasklistClientController($scope, $rootScope, $state, $location, TasklistClientService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getTasklistClient = getTasklistClient;
        vm.getSingleTasklistClient = getSingleTasklistClient;
        vm.saveTasklist = saveTasklist;
        vm.deleteTasklistClient = deleteTasklistClient;
        vm.changeStatus = changeStatus;
        vm.getAllType = getAllType;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.range = range;
        vm.isDisabled = isDisabled;

        vm.getAllType();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Clients Task';
        vm.totalTasklistClient = 0;
        vm.tasklistClientPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.tasklistForm = { tasklist_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.tasktypeList = [{ id : 2, status : 'Default'}, { id : 0, status : 'Standard'}, { id : 1, status : 'Custom' }];
        
        function range(min, max) {
            vm.durationList = [];
            for (var i = min; i <= max; i++) {
                vm.durationList.push(i);
            }
            return vm.durationList;
        };

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.tasklist');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.getSingleTasklistClient(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getTasklistClient(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getTasklistClient(newPage, searchInfo);
        }//END changePage();

         /* to save client task after add and edit  */
        function saveTasklist() {
            // console.log(vm.tasklistForm);
            TasklistClientService.saveTasklist(vm.tasklistForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Client Task');
                        $state.go('backoffice.tasklistClient');
                    } else {
                        toastr.error(response.data.message, 'Client Task');
                    }
                } else {
                    toastr.error(response.data.message, 'Client Task');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client Task');
            });
        }//END saveTasklist();

        /* to get client task list */
        function getTasklistClient(newPage, obj) {
            vm.progressbar.start();
            vm.totalTasklistClient = 0;
            vm.tasklistClient = [];
            vm.tasklistClientCheck = false;
            TasklistClientService.getTasklistClient(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.tasklistClientCheck = true;
                    if (response.data.tasklistClient && response.data.tasklistClient.length > 0) {
                        vm.totalTasklistClient = response.data.total;
                        vm.tasklistClient = response.data.tasklistClient;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getTasklistClient();

        /* to get single client task */
        function getSingleTasklistClient(tasklist_id) {
            $location.hash('top');
            $anchorScroll();
            TasklistClientService.getSingleTasklistClient(tasklist_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleTasklistClient = response.data.tasklistClient;
                        vm.tasklistForm = response.data.tasklistClient;

                        if(vm.tasklistForm.tasklist_servicetype == 0){
                            delete vm.tasklistForm.tasklist_servicetype;
                        }

                        if(vm.tasklistForm.tasklist_duration == 0){
                            delete vm.tasklistForm.tasklist_duration;    
                        }

                        if(vm.tasklistForm.tasklist_cost == 0){
                            delete vm.tasklistForm.tasklist_cost;    
                        }

                    } else {
                        toastr.error(response.data.message, 'Client Task');
                        $state.go('backoffice.tasklistClient');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Client Task');
            });
        }//END getSingleTasklist();

        /** to delete a tasklist **/
        function deleteTasklistClient(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this client task?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    TasklistClientService.deleteTasklistClient(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.tasklistClient.splice(index, 1);
                            vm.totalTasklistClient = vm.totalTasklistClient - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Client Tasklist');
                    });
                }
            });
        }//END deleteTasklist();

        /* to change active/inactive status of client task */
        function changeStatus(status) {
            
            if(status.status == 0 && status.tasklist_cost == 0 && status.tasklist_duration == 0){
                SweetAlert.swal("Error!", 'Please enter cost and time first!', "error");
                return false;
            }

            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }

            var data = { tasklist_id: status.tasklist_id, status: statusId };
            TasklistClientService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /*get service type */
        function getAllType(){
            TasklistClientService.getAllTypes().then(function (response) {
                if (response.status == 200) {
                    if (response.data.type && response.data.type.length > 0) {
                        vm.typeList = response.data.type; 
                    }
                } else {
                    toastr.error(response.data.message, 'Client Task');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Client Task');
            });
        }// END getAllType();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getTasklistClient(1, '');
        }//END reset();               
    }

}());
