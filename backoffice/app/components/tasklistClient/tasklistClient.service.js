(function () {
    "use strict";
    angular
        .module('tasklistClientApp')
        .service('TasklistClientService', TasklistClientService);

    TasklistClientService.$inject = ['$http', 'APPCONFIG', '$q'];

    function TasklistClientService($http, APPCONFIG, $q) {

        /* save client task */
        function saveTasklist(obj) {
            
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.tasklist_id !== 'undefined' && obj.tasklist_id !== '') {
                    requestData['tasklist_id'] = obj.tasklist_id;
                    URL = APPCONFIG.APIURL + 'tasklistClient/edit';
                }

                if (typeof obj.tasklist_name !== 'undefined' && obj.tasklist_name !== '') {
                    requestData['tasklist_name'] = obj.tasklist_name;
                }

                if (typeof obj.tasklist_des !== 'undefined' && obj.tasklist_des !== '') {
                    requestData['tasklist_des'] = obj.tasklist_des;
                }

                if (typeof obj.tasklist_servicetype !== 'undefined' && obj.tasklist_servicetype !== '') {
                    requestData['tasklist_servicetype'] = obj.tasklist_servicetype;
                }

                if (typeof obj.tasklist_duration !== 'undefined' && obj.tasklist_duration !== '') {
                    requestData['tasklist_duration'] = obj.tasklist_duration;
                }

                // if (typeof obj.tasklist_cost !== 'undefined' && obj.tasklist_cost !== '') {
                //     requestData['tasklist_cost'] = obj.tasklist_cost;
                // }

                if (typeof obj.tasklist_type !== 'undefined' && obj.tasklist_type !== '') {
                    requestData['tasklist_type'] = obj.tasklist_type;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END save tasklist();

        /* to get all Tasklist */
        function getTasklistClient(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'tasklistClient';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }
            
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.tasklist_id !== 'undefined' && obj.tasklist_id !== '') {
                    requestData['tasklist_id'] = parseInt(obj.tasklist_id);
                }

                if (typeof obj.tasklist_name !== 'undefined' && obj.tasklist_name !== '') {
                    requestData['tasklist_name'] = obj.tasklist_name;
                }

                if (typeof obj.tasklist_servicetype !== 'undefined' && obj.tasklist_servicetype !== '') {
                    requestData['tasklist_servicetype'] = obj.tasklist_servicetype;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getTasklistClient();

        //Get All types
        function getAllTypes(){
            var URL = APPCONFIG.APIURL + 'rating/allTypes';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllTypes();            

        /* to get single client task */
        function getSingleTasklistClient(tasklist_id) {
            var URL = APPCONFIG.APIURL + 'tasklistClient/view';
            var requestData = {};

            if (typeof tasklist_id !== undefined && tasklist_id !== '') {
                requestData['tasklist_id'] = tasklist_id;
            }

            return this.runHttp(URL, requestData);
        } //END getTasklistById();

        /* to delete a tasklist from database */
        function deleteTasklistClient(tasklist_id) {
            var URL = APPCONFIG.APIURL + 'tasklistClient/delete';
            var requestData = {};

            if (typeof tasklist_id !== undefined && tasklist_id !== '') {
                requestData['tasklist_id'] = tasklist_id;
            }
            return this.runHttp(URL, requestData);
        } //END deleteTasklist();        

        /* to change active/inactive status of tasklist */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'tasklistClient/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.tasklist_id != undefined && obj.tasklist_id != "") {
                    requestData["tasklist_id"] = obj.tasklist_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getTasklistClient: getTasklistClient,
            saveTasklist: saveTasklist,
            getAllTypes : getAllTypes,
            getSingleTasklistClient: getSingleTasklistClient,
            deleteTasklistClient: deleteTasklistClient,
            changeStatus: changeStatus
        }

    };//END TasklistService()
}());
