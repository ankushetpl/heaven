(function () {

    angular.module('tasklistClientApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var tasklistClientPath = 'app/components/tasklistClient/';
        $stateProvider
            .state('backoffice.tasklistClient', {
                url: 'tasklistClient',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistClientPath + 'views/index.html',
                        controller: 'TasklistClientController',
                        controllerAs: 'tasklistClient'
                    }
                }
            })
            .state('backoffice.tasklistClient.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistClientPath + 'views/view.html',
                        controller: 'TasklistClientController',
                        controllerAs: 'tasklistClient'
                    }
                }
            })
            .state('backoffice.tasklistClient.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tasklistClientPath + 'views/form.html',
                        controller: 'TasklistClientController',
                        controllerAs: 'tasklistClient'
                    }
                }
            });
    }

}());