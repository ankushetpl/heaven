(function() {
    "use strict";
    angular.module('suburbApp')
        .controller('SuburbController', SuburbController);

    SuburbController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SuburbService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function SuburbController($scope, $rootScope, $state, $location, SuburbService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getSuburbList = getSuburbList;
        vm.getSingleSuburb = getSingleSuburb;
        vm.saveSuburb = saveSuburb;
        vm.deleteSuburb = deleteSuburb;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;
        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getCity = getCity;
        vm.progressbar = ngProgressFactory.createInstance();
        vm.getAllCountries();
        // vm.initMap = initMap();

        $rootScope.headerTitle = 'Suburb';
        vm.totalSuburb = 0;
        vm.suburbPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.suburbForm = { suburb_id: '' };
        vm.statusList = [{ id: 0, status: 'Not Approved' }, { id: 1, status: 'Approved' }];
        


        //map.js

        //Set up some of our variables.
        var map; //Will contain map object.
        var marker = false; ////Has the user plotted their location marker? 

        //Function called to initialize / create the map.
        //This is called when the page has loaded.
        function initMap(lat, long) {
            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(lat, long);

            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 12 //The zoom value.
            };

            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);

            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                // console.log("initMap1");
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if (marker === false) {
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event) {
                        markerLocation();
                    });
                } else {
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                markerLocation(vm.suburbForm);
            });
        }
        // vm.suburbForm = {'suburb_name' : '', 'lat': '', 'long': ''};

        //This function will get the marker's current location and then add the lat/long
        //values to our textfields so that we can save the location.
        function markerLocation(obj) {
            // console.log(obj);
            //Get location.
            var suburb_id = obj.suburb_id;
            var country = obj.country;
            var state = obj.state;
            var city = obj.city;

            var currentLocation = marker.getPosition();

            //Add lat and lng values to a field that we can save.
            var lat = currentLocation.lat(); //latitude
            var lng = currentLocation.lng(); //longitude
            var latlng = new google.maps.LatLng(lat, lng);
            // console.log(lat)
            // console.log(lng)
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function(results, status) {
                
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        
                        vm.suburbForm = {'suburb_id': suburb_id,'country':country,'state':state,'city':city,'suburb_name' : results[0].formatted_address, 'lat': results[0].geometry.location.lat(), 'long': results[0].geometry.location.lng()};
                        // console.log(vm.suburbForm);
                        // alert("Location: " + results[0].formatted_address);
                        // console.log(results[0].geometry);
                        // console.log(results[0].geometry.location.lng());
                    }
                }
            });
        }


        //Load the map when the page has finished loading.
        google.maps.event.addDomListener(window, 'load', initMap);

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.suburb');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout(function() {
                    vm.getSingleSuburb(path[3]);
                }, 300);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        } //END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getSuburbList(1, '');
        } //END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getSuburbList(newPage, searchInfo);
        } //END changePage();

        /* to save Suburb after add and edit  */
        function saveSuburb() {
            // console.log(vm.suburbForm);
            SuburbService.saveSuburb(vm.suburbForm).then(function(response) {
                // console.log(response);
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Suburb');
                        $state.go('backoffice.suburb');
                    } else {
                        toastr.error(response.data.message, 'Suburb');
                    }
                } else {
                    toastr.error(response.data.message, 'Suburb');
                }
            }, function(error) {
                toastr.error('Internal server error', 'Suburb');
            });
        } //END saveSuburb();

        /* to get Suburb list */
        function getSuburbList(newPage, obj) {
            vm.progressbar.start();
            vm.totalSuburb = 0;
            vm.suburbList = [];
            vm.suburbListCheck = false;
            SuburbService.getSuburb(newPage, obj, vm.orderInfo).then(function(response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.suburbListCheck = true;
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.totalSuburb = response.data.total;
                        vm.suburbList = response.data.suburb;
                    }
                }
            }, function(error) {
                toastr.error(error.data.error, 'Error');
            });
        } //END getSuburbList();
    
        /* to get single Suburb */
        function getSingleSuburb(suburb_id) {
            $location.hash('top');
            $anchorScroll();
            SuburbService.getSingleSuburb(suburb_id).then(function(response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleSuburb = response.data.suburb;
                        vm.suburbForm = response.data.suburb;
                        var lat = response.data.suburb.lat;
                        var long = response.data.suburb.long;
                        initMap(lat,long);
                        // markerLocation(vm.suburbForm);
                        vm.getStatesByID(vm.singleSuburb);

                        if (response.data.suburb.city != 0) {
                            vm.getCityByID(vm.singleSuburb);
                        }

                        if (response.data.suburb.city == 0) {
                            vm.hideCity = true;
                            delete vm.suburbForm.city;
                        }

                        if (response.data.suburb.country != 0) {
                            var country = response.data.suburb.country;
                            vm.singleSuburb.countryName = '';
                            angular.forEach(vm.countryList, function(value, key) {
                                if (value.id == country) {
                                    vm.singleSuburb.countryName = value.name;
                                }
                            });
                        }
                    } else {
                        toastr.error(response.data.message, 'Suburb');
                        $state.go('backoffice.suburb');
                    }
                }
            }, function(error) {
                toastr.error(error.data.error, 'Suburb');
            });
        } //END getSingleSuburb();

        /** to delete a suburb **/
        function deleteSuburb(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Suburb?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function(isConfirm) {
                if (isConfirm) {
                    SuburbService.deleteSuburb(id).then(function(response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.suburbList.splice(index, 1);
                            vm.totalSuburb = vm.totalSuburb - 1;
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function(error) {
                        toastr.error(error.data.error, 'Suburb');
                    });
                }
            });
        } //END deleteSuburb();

        /* to change active/inactive status of Suburb */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { suburb_id: status.suburb_id, status: statusId };
            SuburbService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function(error) {
                toastr.error(error.data.error, 'Error');
            });
        } //END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getSuburbList(1, '');
        } //END reset();    

        /* Get All Countries */
        function getAllCountries() {
            SuburbService.getAllCountries().then(function(response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Suburb');
                }
            }, function(error) {
                toastr.error('Internal server error', 'Suburb');
            });
        } // END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj) {
            SuburbService.getStatesByID(obj).then(function(response) {
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;
                    }
                } else {
                    toastr.error(response.data.message, 'Suburb');
                }
            }, function(error) {
                toastr.error('Internal server error', 'Suburb');
            });
        } // END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj) {
            

            SuburbService.getCityByID(obj).then(function(response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                    } else {
                        vm.hideCity = true;
                        // console.log(vm.suburbForm.city);
                        delete vm.suburbForm.city;
                    }
                } else {
                    toastr.error(response.data.message, 'Suburb');
                }
            }, function(error) {
                toastr.error('Internal server error', 'Suburb');
            });
        } // END getCityByID();

        function getCity(city) {
            var city = { "id": city }
            SuburbService.getCity(city).then(function(response) {
                if (response.status == 200) {
                    var city = response.data;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'address': city.data.name }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            // console.log(results[0].geometry.location.lat());
                            // console.log(results[0].geometry.location.lng());
                            initMap(results[0].geometry.location.lat(), results[0].geometry.location.lng())
                        } else {
                            alert("Something got wrong " + status);
                        }
                    });
                } else {
                    // console.log("not okay");
                }
            }, function(error) {
                toastr.error('Internal server error', 'Suburb');
            });
        }

    }

}());