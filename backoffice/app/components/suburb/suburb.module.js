(function () {

    angular.module('suburbApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var suburbPath = 'app/components/suburb/';
        $stateProvider
            .state('backoffice.suburb', {
                url: 'suburb',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbPath + 'views/index.html',
                        controller: 'SuburbController',
                        controllerAs: 'suburb'
                    }
                }
            })
            .state('backoffice.suburb.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbPath + 'views/form.html',
                        controller: 'SuburbController',
                        controllerAs: 'suburb'
                    }
                }
            })
            .state('backoffice.suburb.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbPath + 'views/form.html',
                        controller: 'SuburbController',
                        controllerAs: 'suburb'
                    }
                }
            })
            .state('backoffice.suburb.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbPath + 'views/view.html',
                        controller: 'SuburbController',
                        controllerAs: 'suburb'
                    }
                }
            });
    }

}());