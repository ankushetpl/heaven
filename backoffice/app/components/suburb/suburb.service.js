(function () {
    "use strict";
    angular
        .module('suburbApp')
        .service('SuburbService', SuburbService);

    SuburbService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SuburbService($http, APPCONFIG, $q) {

        /* save Suburb */
        function saveSuburb(obj) {
            // console.log(obj);
            var URL = APPCONFIG.APIURL + 'suburb/add';
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.suburb_id !== 'undefined' && obj.suburb_id !== '') {
                    requestData['suburb_id'] = obj.suburb_id;
                    URL = APPCONFIG.APIURL + 'suburb/edit';
                }

                if (typeof obj.country !== 'undefined' && obj.country !== '') {
                    requestData['country'] = obj.country;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.suburb_name !== 'undefined' && obj.suburb_name !== '') {
                    requestData['suburb_name'] = obj.suburb_name;
                }

                if (typeof obj.lat !== 'undefined' && obj.lat !== '') {
                    requestData['lat'] = obj.lat;
                }

                if (typeof obj.long !== 'undefined' && obj.long !== '') {
                    requestData['long'] = obj.long;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveSuburb();

        /* to get all Suburb */
        function getSuburb(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'suburb';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.suburb_id !== 'undefined' && obj.suburb_id !== '') {
                    requestData['suburb_id'] = parseInt(obj.suburb_id);
                }

                if (typeof obj.suburb_name !== 'undefined' && obj.suburb_name !== '') {
                    requestData['suburb_name'] = obj.suburb_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburb();

        /* to get single Suburb */
        function getSingleSuburb(suburb_id) {
            var URL = APPCONFIG.APIURL + 'suburb/view';
            var requestData = {};

            if (typeof suburb_id !== undefined && suburb_id !== '') {
                requestData['suburb_id'] = suburb_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleSuburb();        

        /* to delete a suburb from database */
        function deleteSuburb(suburb_id) {
            var URL = APPCONFIG.APIURL + 'suburb/delete';
            var requestData = {};

            if (typeof suburb_id !== undefined && suburb_id !== '') {
                requestData['suburb_id'] = suburb_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteSuburb();

        /* to change active/inactive status of suburb */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'suburb/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.suburb_id != undefined && obj.suburb_id != "") {
                    requestData["suburb_id"] = obj.suburb_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';

            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get All States
        function getStatesByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allStatesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.country !== 'undefined' && obj.country !== '') {
                    requestData['ID'] = parseInt(obj.country);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get All City
        function getCityByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCitiesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['ID'] = parseInt(obj.state);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getCityByID();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

         function getCity(obj){
            console.log(obj);
            var URL = APPCONFIG.APIURL + 'suburb/getCity';
            var requestData = {};
            requestData['ID'] = parseInt(obj.id);
      
            return this.runHttp(URL, requestData);
        }

        return {
            runHttp: runHttp,
            getAllCountries: getAllCountries,
            getStatesByID: getStatesByID,
            getCityByID: getCityByID,
            getSuburb: getSuburb,
            getSingleSuburb: getSingleSuburb,
            saveSuburb: saveSuburb,
            deleteSuburb: deleteSuburb,
            changeStatus: changeStatus,
            getCity: getCity
        }

    };//END SuburbService()
}());
