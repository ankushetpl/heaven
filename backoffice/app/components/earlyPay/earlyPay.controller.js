(function () {
    "use strict";
    angular.module('earlyPayApp')
        .controller('EarlyPayController', EarlyPayController);

    EarlyPayController.$inject = ['$scope', '$rootScope', '$state', '$location', 'EarlyPayService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory'];

    function EarlyPayController($scope, $rootScope, $state, $location, EarlyPayService, toastr, SweetAlert, $timeout, ngProgressFactory) {
        var vm = this;

        vm.getList = getList;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Early Pay Request';
        vm.total = 0;
        vm.PerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};

        function isDisabled() {
            vm.isDisabled = true;
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getList(newPage, searchInfo);
        }//END changePage();

        /* to get list */
        function getList(newPage, obj) {
            vm.progressbar.start();
            vm.payList = [];
            vm.payListCheck = false;
            EarlyPayService.getAllData(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.payListCheck = true;
                    if (response.data.paylist && response.data.paylist.length > 0) {
                        vm.total = response.data.total;
                        vm.payList = response.data.paylist;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getList();

        /* to change active/inactive status of early pay */
        function changeStatus(status) {

            if (status.status == 1) {
                var statusId = 0;
                SweetAlert.swal("Error!", "Sorry, You can't change status!", "error");
                return false;
            } else {
                var statusId = 1;
            }

            var data = { request_id: status.request_id, status: statusId };
            EarlyPayService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getList(1, '');
        }//END reset();               
    }

}());
