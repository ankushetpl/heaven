(function () {

    angular.module('earlyPayApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var earlyPayPath = 'app/components/earlyPay/';
        $stateProvider
            .state('backoffice.earlyPay', {
                url: 'earlyPay',
                views: {
                    'content@backoffice': {
                        templateUrl: earlyPayPath + 'views/index.html',
                        controller: 'EarlyPayController',
                        controllerAs: 'earlyPay'
                    }
                }
            });            
    }

}());