(function () {
    "use strict";
    angular
        .module('earlyPayApp')
        .service('EarlyPayService', EarlyPayService);

    EarlyPayService.$inject = ['$http', 'APPCONFIG', '$q'];

    function EarlyPayService($http, APPCONFIG, $q) {

        /* to get all data */
        function getAllData(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'earlyPay';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.request_id !== 'undefined' && obj.request_id !== '') {
                    requestData['request_id'] = parseInt(obj.request_id);
                }

                if (typeof obj.worker_id !== 'undefined' && obj.worker_id !== '') {
                    requestData['worker_id'] = parseInt(obj.worker_id);
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getAllData();

        /* to change active/inactive status of earlyPay */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'earlyPay/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.request_id != undefined && obj.request_id != "") {
                    requestData["request_id"] = obj.request_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAllData: getAllData,
            changeStatus: changeStatus
        }

    };//END EarlyPayService()
}());
