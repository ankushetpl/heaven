(function () {
    "use strict";
    angular.module('amenitiesApp')
        .service('AmenitiesService', AmenitiesService);

    AmenitiesService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AmenitiesService($http, APPCONFIG, $q) {

        /* save Amenities */
        function saveAmenities(obj) {
            var URL = APPCONFIG.APIURL + 'amenities/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.amenities_id !== 'undefined' && obj.amenities_id !== '') {
                    requestData['amenities_id'] = obj.amenities_id;
                    URL = APPCONFIG.APIURL + 'amenities/edit';
                }

                if (typeof obj.amenities_type !== 'undefined' && obj.amenities_type !== '') {
                    requestData['amenities_type'] = obj.amenities_type;
                }

                if (typeof obj.amenities_count !== 'undefined' && obj.amenities_count !== '') {
                    requestData['amenities_count'] = obj.amenities_count;
                }

                if (typeof obj.amenities_clean_duartion !== 'undefined' && obj.amenities_clean_duartion !== '') {
                    requestData['amenities_clean_duartion'] = obj.amenities_clean_duartion;
                }

                if (typeof obj.amenities_clean_cost !== 'undefined' && obj.amenities_clean_cost !== '') {
                    requestData['amenities_clean_cost'] = obj.amenities_clean_cost;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.roleID !== 'undefined' && obj.roleID !== '') {
                    requestData['role_id'] = obj.roleID;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveAmenities();

        /* to get all Amenities */
        function getAmenities(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'amenities';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.amenities_id !== 'undefined' && obj.amenities_id !== '') {
                    requestData['amenities_id'] = parseInt(obj.amenities_id);
                }

                if (typeof obj.amenities_type !== 'undefined' && obj.amenities_type !== '') {
                    requestData['amenities_type'] = obj.amenities_type;
                }

                if (typeof obj.amenities_count !== 'undefined' && obj.amenities_count !== '') {
                    requestData['amenities_count'] = obj.amenities_count;
                }

                if (typeof obj.amenities_clean_duartion !== 'undefined' && obj.amenities_clean_duartion !== '') {
                    requestData['amenities_clean_duartion'] = obj.amenities_clean_duartion;
                }

                if (typeof obj.amenities_clean_cost !== 'undefined' && obj.amenities_clean_cost !== '') {
                    requestData['amenities_clean_cost'] = obj.amenities_clean_cost;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getAmenities();

        /* to get single Amenities */
        function getSingleAmenities(amenities_id) {
            var URL = APPCONFIG.APIURL + 'amenities/view';
            var requestData = {};

            if (typeof amenities_id !== undefined && amenities_id !== '') {
                requestData['amenities_id'] = amenities_id;
            }

            return this.runHttp(URL, requestData);
        } //END getAmenitiesById();

        /* to delete a Amenities from database */
        function deleteAmenities(amenities_id) {
            var URL = APPCONFIG.APIURL + 'amenities/delete';
            var requestData = {};

            if (typeof amenities_id !== undefined && amenities_id !== '') {
                requestData['amenities_id'] = amenities_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteAmenities();

        /* to change active/inactive status of Amenities */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'amenities/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.amenities_id != undefined && obj.amenities_id != "") {
                    requestData["amenities_id"] = obj.amenities_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to get all role */
        function getAllRoll(pageNum) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = { pageNum }; 
            requestData['orderColumn'] = 'role_name'; 
            requestData['orderBy'] = 'ASC';          
            return this.runHttp(URL, requestData);
        }//END getAllRoll();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAmenities: getAmenities,
            getSingleAmenities: getSingleAmenities,
            saveAmenities: saveAmenities,
            deleteAmenities: deleteAmenities,
            changeStatus: changeStatus,
            getAllRoll: getAllRoll
        }

    };//END AmenitiesService()
}());
