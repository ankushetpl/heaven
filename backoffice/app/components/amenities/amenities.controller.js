(function () {
    "use strict";
    angular.module('amenitiesApp')
        .controller('AmenitiesController', AmenitiesController);

    AmenitiesController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AmenitiesService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function AmenitiesController($scope, $rootScope, $state, $location, AmenitiesService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getAmenitiesList = getAmenitiesList;
        vm.getSingleAmenities = getSingleAmenities;
        vm.saveAmenities = saveAmenities;
        vm.deleteAmenities = deleteAmenities;
        vm.changeStatus = changeStatus; 
        vm.getAllRoll = getAllRoll;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.getAllRoll();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Amenities';
        vm.totalAmenities = 0;
        vm.amenitiesPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.amenitiesForm = { amenities_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];
        vm.amenitiesTypeList = [{id : 'Bedrooms', type : 'Bedrooms'}, {id : 'Bathrooms', type : 'Bathrooms'}, {id : 'Baskets', type : 'Baskets'}];
        vm.durationList = [ {id : '1', hour : '1'}, {id : '2', hour : '2'}, {id : '3', hour : '3'}, {id : '4', hour : '4'}, {id : '5', hour : '5'}, {id : '6', hour : '6'}, {id : '7', hour : '7'}, {id : '8', hour : '8'} ];

        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.amenities');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleAmenities(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAmenitiesList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAmenitiesList(newPage, searchInfo);
        }//END changePage();

        /* to save Amenities after add and edit  */
        function saveAmenities() {
            AmenitiesService.saveAmenities(vm.amenitiesForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Amenities');
                        $state.go('backoffice.amenities');
                    } else {
                        toastr.error(response.data.message, 'Amenities');
                    }
                } else {
                    toastr.error(response.data.message, 'Amenities');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Amenities');
            });
        }//END saveAmenities();

        /* to  getAmenitiesList */
        function getAmenitiesList(newPage, obj) {
            vm.progressbar.start();
            vm.totalAmenities = 0;
            vm.amenitiesList = [];
            vm.amenitiesListCheck = false;
            AmenitiesService.getAmenities(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.amenitiesListCheck = true;
                    if (response.data.amenities && response.data.amenities.length > 0) {
                        vm.totalAmenities = response.data.total;
                        vm.amenitiesList = response.data.amenities;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAmenitiesList();

        /* to get single Amenities */
        function getSingleAmenities(amenities_id) {
            $location.hash('top');
            $anchorScroll();
            AmenitiesService.getSingleAmenities(amenities_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleAmenities = response.data.amenities;
                        vm.amenitiesForm = response.data.amenities;
                        // vm.isDisabled = true;
                    } else {
                        toastr.error(response.data.message, 'Amenities');
                        $state.go('backoffice.amenities');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Amenities');
            });
        }//END getSingleAmenities();

        /** to delete a Amenities **/
        function deleteAmenities(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Amenities?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AmenitiesService.deleteAmenities(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.amenitiesList.splice(index, 1);
                            vm.totalAmenities = vm.totalAmenities - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'FAQ');
                    });
                }
            });
        }//END deleteAmenities();

        /* to change active/inactive status of Amenities */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { amenities_id: status.amenities_id, status: statusId };
            AmenitiesService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* all role type */
        function getAllRoll() {
            vm.roleList = [];
            AmenitiesService.getAllRoll('1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllRoll()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAmenitiesList(1, '');
        }//END reset();               
    }

}());
