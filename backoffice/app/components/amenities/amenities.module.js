(function () {

    angular.module('amenitiesApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var amenitiesPath = 'app/components/amenities/';
        $stateProvider
            .state('backoffice.amenities', {
                url: 'amenities',
                views: {
                    'content@backoffice': {
                        templateUrl: amenitiesPath + 'views/index.html',
                        controller: 'AmenitiesController',
                        controllerAs: 'amenities'
                    }
                }
            })
            .state('backoffice.amenities.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: amenitiesPath + 'views/form.html',
                        controller: 'AmenitiesController',
                        controllerAs: 'amenities'
                    }
                }
            })
            .state('backoffice.amenities.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: amenitiesPath + 'views/form.html',
                        controller: 'AmenitiesController',
                        controllerAs: 'amenities'
                    }
                }
            })
            .state('backoffice.amenities.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: amenitiesPath + 'views/view.html',
                        controller: 'AmenitiesController',
                        controllerAs: 'amenities'
                    }
                }
            })
    }

}());