(function () {

    angular.module('emailApp', [
        'ckeditor'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var emailPath = 'app/components/email/';
        $stateProvider
            .state('backoffice.email', {
                url: 'email',
                views: {
                    'content@backoffice': {
                        templateUrl: emailPath + 'views/index.html',
                        controller: 'EmailController',
                        controllerAs: 'email'
                    }
                }
            })
            .state('backoffice.email.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: emailPath + 'views/form.html',
                        controller: 'EmailController',
                        controllerAs: 'email'
                    }
                }
            })
            .state('backoffice.email.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: emailPath + 'views/form.html',
                        controller: 'EmailController',
                        controllerAs: 'email'
                    }
                }
            })
            .state('backoffice.email.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: emailPath + 'views/view.html',
                        controller: 'EmailController',
                        controllerAs: 'email'
                    }
                }
            })
    }

}());