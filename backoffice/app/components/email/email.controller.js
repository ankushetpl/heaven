(function () {
    "use strict";
    angular.module('emailApp')
        .controller('EmailController', EmailController);

    EmailController.$inject = ['$scope', '$rootScope', '$state', '$location', 'EmailService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function EmailController($scope, $rootScope, $state, $location, EmailService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getEmailList = getEmailList;
        vm.getSingleEmail = getSingleEmail;
        vm.saveEmail = saveEmail;
        vm.deleteEmail = deleteEmail;
        vm.changeStatus = changeStatus;
        vm.changeStatusDefault = changeStatusDefault;
        vm.formatDate = formatDate;
        vm.getEmailTypes = getEmailTypes;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;        

        vm.getEmailTypes();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Email Templetes';
        vm.totalEmail = 0;
        vm.emailPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.emailForm = { email_id: '' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
       
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.email');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleEmail(path[3]);
            }
        }

        // Editor options.
        $scope.email_content = {
            language: 'en',
            allowedContent: true,
            entities: false
        };

        vm.emailForm.email_content = "<h2>Hello World!</h2>";

        function isDisabled() {
            vm.isDisabled = true;
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getEmailList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getEmailList(newPage, searchInfo);
        }//END changePage();

        /* to save email after add and edit  */
        function saveEmail() {
            vm.progressbar.start();
            vm.isDisabledButton = true;
            EmailService.saveEmail(vm.emailForm).then(function (response) {
                vm.progressbar.complete();
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Email Templete');
                        $state.go('backoffice.email');
                    } else {
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Email Templete');
                    }
                } else {
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Email Templete');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Email Templete');
            });
        }//END saveEmail();

        /* to get email */
        function getEmailList(newPage, obj) {
            vm.progressbar.start();
            vm.totalEmail = 0;
            vm.emailList = [];
            vm.emailListCheck = false;
            EmailService.getEmails(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.emailListCheck = true;
                    if (response.data.email && response.data.email.length > 0) {
                        vm.totalEmail = response.data.total;
                        vm.emailList = response.data.email;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getEmailList();

        /* to get single email */
        function getSingleEmail(email_id) {
            $location.hash('top');
            $anchorScroll();
            EmailService.getSingleEmail(email_id).then(function (response) {                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleEmail = response.data.email;
                        vm.emailForm = response.data.email;
                        if(vm.emailForm.status){
                            vm.hideStatus = true;
                        }
                    } else {
                        toastr.error(response.data.message, 'Email Templete');
                        $state.go('backoffice.email');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSingleEmail();

        /** to delete a email **/
        function deleteEmail(id, defaults, index) {

            if (defaults == 1) {
                var message = "Sorry! Default templete cannot be deleted";
                SweetAlert.swal(message);
                return false;                
            }

            SweetAlert.swal({
                title: "Are you sure you want to delete this Email Templete?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    EmailService.deleteEmail(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.emailList.splice(index, 1);
                            vm.totalEmail = vm.totalEmail - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });
        }//END deleteEmail();

        /* to change active/inactive status of email */
        function changeStatus(status) {
            
            if (status.defaults == 1) {
                var message = "Sorry! Default templete can't be deactivated";
                SweetAlert.swal(message);
                return false;                
            }

            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { email_id: status.email_id, status: statusId };
            EmailService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to change default/not default status of email */
        function changeStatusDefault(default1) {
            
            if (default1.status == 0) {
                var message = "Please approve this email first";
                SweetAlert.swal(message);
                return false;
            }
            
            if (default1.defaults == 1) {
                var message = "Email templete already as default";
                SweetAlert.swal(message);
                return false;
                var defaultId = 0;
            } else {
                var defaultId = 1;
            }
            var data = { email_id: default1.email_id, default: defaultId, email_type : default1.email_type};
            
            EmailService.changeStatusDefault(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                    } 
                } else {
                    toastr.error(response.data.error, 'Error');
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatusDefault()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getEmailList(1, '');
        }//END reset();   

        /* get all email type */
        function getEmailTypes(){
            vm.locationList = [];            
            EmailService.getEmailTypes().then(function (response) {
                if (response.status == 200) {
                    if (response.data.emailtypes && response.data.emailtypes.length > 0) {
                        vm.locationList = response.data.emailtypes;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getEmailTypes()
    }

}());
