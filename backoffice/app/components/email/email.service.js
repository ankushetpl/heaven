(function () {
    "use strict";
    angular
        .module('emailApp')
        .service('EmailService', EmailService);

    EmailService.$inject = ['$http', 'APPCONFIG', '$q'];

    function EmailService($http, APPCONFIG, $q) {

        /* save email */
        function saveEmail(obj) {
            var URL = APPCONFIG.APIURL + 'email/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.email_id !== 'undefined' && obj.email_id !== '') {
                    requestData['email_id'] = obj.email_id;
                    URL = APPCONFIG.APIURL + 'email/edit';
                }

                if (typeof obj.email_type !== 'undefined' && obj.email_type !== '') {
                    requestData['email_type'] = obj.email_type;
                }

                if (typeof obj.email_content !== 'undefined' && obj.email_content !== '') {
                    requestData['email_content'] = obj.email_content;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveEmail();

        /* to get all emails */
        function getEmails(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'email';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.email_id !== 'undefined' && obj.email_id !== '') {
                    requestData['email_id'] = parseInt(obj.email_id);
                }

                if (typeof obj.email_type !== 'undefined' && obj.email_type !== '') {
                    requestData['email_type'] = obj.email_type;
                }

                if (typeof obj.default !== 'undefined' && obj.default !== '') {
                    requestData['default'] = obj.default;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getEmails();

        /* to get email types  */
        function getEmailTypes() {
            var URL = APPCONFIG.APIURL + 'email/emailtypes';
            var requestData = {};            
                   
            return this.runHttp(URL, requestData);
        }//END getEmailTypes();  


        /* to get single email */
        function getSingleEmail(email_id) {
            var URL = APPCONFIG.APIURL + 'email/view';
            var requestData = {};

            if (typeof email_id !== undefined && email_id !== '') {
                requestData['email_id'] = email_id;
            }

            return this.runHttp(URL, requestData);
        } //END getEmailById();

        /* to delete a email from database */
        function deleteEmail(email_id) {
            var URL = APPCONFIG.APIURL + 'email/delete';
            var requestData = {};

            if (typeof email_id !== undefined && email_id !== '') {
                requestData['email_id'] = email_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteEmail();

        /* to change active/inactive status of email */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'email/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.email_id != undefined && obj.email_id != "") {
                    requestData["email_id"] = obj.email_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to change default/not default status of email */
        function changeStatusDefault(obj) {
            var URL = APPCONFIG.APIURL + 'email/default';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.email_id != undefined && obj.email_id != "") {
                    requestData["email_id"] = obj.email_id;
                }

                if (obj.default != undefined || obj.default != "") {
                    requestData["default"] = obj.default;
                }

                if (obj.email_type != undefined || obj.email_type != "") {
                    requestData["email_type"] = obj.email_type;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatusDefault()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getEmails: getEmails,
            getEmailTypes : getEmailTypes,
            getSingleEmail: getSingleEmail,
            saveEmail: saveEmail,
            deleteEmail: deleteEmail,
            changeStatus: changeStatus,
            changeStatusDefault : changeStatusDefault
        }

    };//END FaqService()
}());
