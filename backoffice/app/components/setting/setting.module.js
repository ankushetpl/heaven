(function () {

    angular.module('settingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var settingPath = 'app/components/setting/';
        $stateProvider
            .state('backoffice.setting', {
                url: 'setting',
                views: {
                    'content@backoffice': {
                        templateUrl: settingPath + 'views/index.html',
                        controller: 'SettingController',
                        controllerAs: 'setting'
                    }
                }
            })
            .state('backoffice.setting.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: settingPath + 'views/form.html',
                        controller: 'SettingController',
                        controllerAs: 'setting'
                    }
                }
            })
            .state('backoffice.setting.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: settingPath + 'views/form.html',
                        controller: 'SettingController',
                        controllerAs: 'setting'
                    }
                }
            })
            .state('backoffice.setting.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: settingPath + 'views/view.html',
                        controller: 'SettingController',
                        controllerAs: 'setting'
                    }
                }
            })
    }

}());