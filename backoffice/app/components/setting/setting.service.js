(function () {
    "use strict";
    angular
        .module('settingApp')
        .service('SettingService', SettingService);

    SettingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SettingService($http, APPCONFIG, $q) {

        /* to get all setting */
        function getSetList() {
            var URL = APPCONFIG.APIURL + 'setting';
            var requestData = {};            

            return this.runHttp(URL, requestData);
        }//END getSetList();

        /* save setting */
        function saveSetting(obj) {
            var URL = APPCONFIG.APIURL + 'setting/edit';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.setting_id !== 'undefined' && obj.setting_id !== '') {
                    requestData['setting_id'] = obj.setting_id;
                }

                if (typeof obj.setting_value !== 'undefined' && obj.setting_value !== '') {
                    requestData['setting_value'] = obj.setting_value;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveSetting();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();
        
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getSetList: getSetList,
            saveSetting: saveSetting,
            getImage: getImage
        }

    };//END SettingService()
}());
