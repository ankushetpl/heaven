(function () {
    "use strict";
    angular.module('settingApp')
        .controller('SettingController', SettingController);

    SettingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SettingService', 'toastr', 'SweetAlert', '$timeout', 'Upload', 'APPCONFIG', 'ngProgressFactory'];

    function SettingController($scope, $rootScope, $state, $location, SettingService, toastr, SweetAlert, $timeout, Upload, APPCONFIG, ngProgressFactory) {
        var vm = this;
        
        vm.getSetList = getSetList;
        vm.close = close;
        vm.saveBonus = saveBonus;
        vm.saveReferral = saveReferral;
        vm.saveRating = saveRating;
        vm.saveContactP = saveContactP;
        vm.saveContactF = saveContactF;
        vm.saveLog = saveLog;
        vm.saveSlotTime = saveSlotTime;

        vm.progressbar = ngProgressFactory.createInstance();
            
        $rootScope.headerTitle = 'All Setting';
        vm.bonusForm = { setting_id: ''};
        vm.referralForm = { setting_id: ''};
        vm.ratingForm = { setting_id: ''};
        vm.contactPForm = { setting_id: ''};
        vm.contactFForm = { setting_id: ''};
        vm.logForm = { setting_id: ''};
        vm.assessorSlotTimeForm = { setting_id: ''};
        vm.imageFlag = false;
        vm.currentYear = 1;

        /* to close model pupup */
        function close(){
            $state.go('backoffice.setting');  
            vm.getSetList();                      
        }//END close();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status == 1) {
                    vm.logForm.setting_value = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to get setting list */
        function getSetList() {
            vm.progressbar.start();
            var total = 0;
            vm.setList = [];
            vm.bonusSet = [];   
            vm.referralSet = [];
            vm.ratingSet = []; 
            vm.contactPSet = [];
            vm.contactFSet = [];
            vm.logSet = [];
            vm.assessorSlotTime = [];
            SettingService.getSetList().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.setting && response.data.setting.length > 0) {
                        vm.setList = response.data.setting;
                        total = response.data.setting.length;
                        for(var i = 0; i < total; i++ ){
                            if(response.data.setting[i].setting_id == 1 ){
                                vm.bonusSet = vm.setList[i];
                                vm.bonusForm = vm.setList[i];
                            }
                            if(response.data.setting[i].setting_id == 2 ){
                                vm.referralSet = vm.setList[i];
                                vm.referralForm = vm.setList[i];
                            }
                            if(response.data.setting[i].setting_id == 3 ){
                                vm.contactPSet = vm.setList[i];
                                vm.contactPForm = vm.setList[i];
                            }
                            if(response.data.setting[i].setting_id == 4 ){
                                vm.contactFSet = vm.setList[i];
                                vm.contactFForm = vm.setList[i];
                            }
                            if(response.data.setting[i].setting_id == 5 ){
                                vm.logSet = vm.setList[i];
                                vm.logForm = vm.setList[i];
                                var file_id = response.data.setting[i].setting_value;
                                SettingService.getImage(file_id).then(function (responses) {
                                    if (responses.status == 200) {
                                        if (responses.data.file && responses.data.file != '') {
                                            vm.logForm.file = responses.data.file.file_base_url;
                                        }
                                    }
                                }, function (error) {
                                    toastr.error(error.data.error, 'Error');
                                });
                            }
                            if(response.data.setting[i].setting_id == 6 ){
                                vm.ratingSet = vm.setList[i];
                                vm.ratingForm = vm.setList[i];
                            }
                            if(response.data.setting[i].setting_id == 7 ){
                                vm.assessorSlotTime = vm.setList[i];
                                vm.assessorSlotTimeForm = vm.setList[i];
                            }
                        }
                    }
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSetList();        

        /* to save bonus after add and edit  */
        function saveBonus() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            SettingService.saveSetting(vm.bonusForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.success(response.data.message, 'Bonus');
                        var bonusElem = angular.element("#responsive-modal");
                        bonusElem.modal("hide");
                        $state.go('backoffice.setting');  
                        vm.getSetList();                      
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Bonus');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Bonus');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Bonus');
            });
        }//END saveBonus();

        /* to save referral after add and edit  */
        function saveReferral() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            SettingService.saveSetting(vm.referralForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.success(response.data.message, 'Referral');
                        var referralElem = angular.element("#responsive-modal-ref");
                        referralElem.modal("hide");
                        $state.go('backoffice.setting');  
                        vm.getSetList();                      
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Referral');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Referral');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Referral');
            });
        }//END saveReferral();

        /* to save rating after add and edit  */
        function saveRating() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            SettingService.saveSetting(vm.ratingForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.success(response.data.message, 'Rating');
                        var ratingElem = angular.element("#responsive-modal-rat");
                        ratingElem.modal("hide");
                        $state.go('backoffice.setting');  
                        vm.getSetList();                      
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Rating');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Rating');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Rating');
            });
        }//END saveRating();

        /* to save phone after add and edit  */
        function saveContactP() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            SettingService.saveSetting(vm.contactPForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.success(response.data.message, 'Phone Number');
                        var contPElem = angular.element("#responsive-modal-contP");
                        contPElem.modal("hide");
                        $state.go('backoffice.setting');  
                        vm.getSetList();                      
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Phone Number');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Phone Number');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Phone Number');
            });
        }//END saveContactP();

        /* to save fax after add and edit  */
        function saveContactF() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            SettingService.saveSetting(vm.contactFForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.success(response.data.message, 'Fax Number');
                        var contFElem = angular.element("#responsive-modal-contF");
                        contFElem.modal("hide");
                        $state.go('backoffice.setting');  
                        vm.getSetList();                      
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Fax Number');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Fax Number');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Fax Number');
            });
        }//END saveContactF();        

        /* to save log after add and edit  */
        function saveLog() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            if (vm.logForm.file !== '') { //check if from is valid
                
                if(vm.logForm.file.size > 0 && vm.logForm.file.name !== '' ){
                    vm.upload(vm.logForm.file); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        SettingService.saveSetting(vm.logForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.success(response.data.message, 'Logo');
                                    var logElem = angular.element("#responsive-modal-log");
                                    logElem.modal("hide");
                                    $state.go('backoffice.setting');  
                                    vm.getSetList();                      
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Logo');
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Logo');
                            }                            
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Logo');
                        });
                    }
                }, 5000 );
            }
        }//END saveLog(); 
        
        /* to save assessor slot time after add and edit  */
        function saveSlotTime() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            SettingService.saveSetting(vm.assessorSlotTimeForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.success(response.data.message, 'Slot Time');
                        var bonusElem = angular.element("#responsive-modal-slottime");
                        bonusElem.modal("hide");
                        $state.go('backoffice.setting');  
                        vm.getSetList();                      
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Slot Time');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Slot Time');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Slot Time');
            });
        }//END saveBonus();
       
    }

}());
