(function () {

    angular.module('modulesApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var modulesPath = 'app/components/modules/';
        $stateProvider
            .state('backoffice.modules', {
                url: 'modules',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/index.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })
            .state('backoffice.modules.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/form.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })
            .state('backoffice.modules.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/form.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })
            .state('backoffice.modules.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/view.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })
            .state('backoffice.modules.subview', {
                url: '/subview/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/view.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })            
            .state('backoffice.modules.subcreate', {
                url: '/subcreate',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/form.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })
            .state('backoffice.modules.subedit', {
                url: '/subedit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: modulesPath + 'views/form.html',
                        controller: 'ModulesController',
                        controllerAs: 'modules'
                    }
                }
            })         
    }

}());