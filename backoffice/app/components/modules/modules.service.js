(function () {
    "use strict";
    angular
        .module('modulesApp')
        .service('ModulesService', ModulesService);

    ModulesService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ModulesService($http, APPCONFIG, $q) {

        /* save modules */
        function saveModules(obj) {
            var URL = APPCONFIG.APIURL + 'modules/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.module_id !== 'undefined' && obj.module_id !== '') {
                    requestData['module_id'] = obj.module_id;
                    URL = APPCONFIG.APIURL + 'modules/edit';
                }

                if (typeof obj.module_name !== 'undefined' && obj.module_name !== '') {
                    requestData['module_name'] = obj.module_name;
                }
                
                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                } 
                
                if (typeof obj.parent_id !== 'undefined' && obj.parent_id !== '') {
                    requestData['parent_id'] = obj.parent_id;
                } 
            }

            return this.runHttp(URL, requestData);
        } //END saveModules();
       
        /* to get all modules */
        function getModules(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'modules';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.module_id !== 'undefined' && obj.module_id !== '') {
                    requestData['module_id'] = parseInt(obj.module_id);
                }

                if (typeof obj.module_name !== 'undefined' && obj.module_name !== '') {
                    requestData['module_name'] = obj.module_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }  
                
                if (typeof obj.parent_id !== 'undefined' && obj.parent_id !== '') {
                    requestData['parent_id'] = parseInt(obj.parent_id);
                } 
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getModules();

        /* to get single module */
        function getSingleModule(module_id) {
            var URL = APPCONFIG.APIURL + 'modules/view';
            var requestData = {};

            if (typeof module_id !== undefined && module_id !== '') {
                requestData['module_id'] = module_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleModule();

        /* to get all parent modules  */
        function getAllMain() {
            var URL = APPCONFIG.APIURL + 'modules/AllList';

            var requestData = {};             
                   
            return this.runHttp(URL, requestData);
        }//END getAllMain();        

        /* to delete a module from database */
        function deleteModule(module_id) {
            var URL = APPCONFIG.APIURL + 'modules/delete';
            var requestData = {};

            if (typeof module_id !== undefined && module_id !== '') {
                requestData['module_id'] = module_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteModule();

        /* to change active/inactive status of modules */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'modules/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.module_id != undefined && obj.module_id != "") {
                    requestData["module_id"] = obj.module_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()  

        /* to check modules */
        function checkData(obj) {
            var URL = APPCONFIG.APIURL + 'modules/check';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.module_id != undefined && obj.module_id != "") {
                    requestData["module_id"] = obj.module_id;
                }                         
            }  
            
            return this.runHttp(URL, requestData);            
        }//END checkData()  

        /* get parent name*/
        function getParent(obj){
            var URL = APPCONFIG.APIURL + 'modules/parent';
            
            var requestData = {obj};
            
            return this.runHttp(URL, requestData);
        }
        
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getModules: getModules,
            getSingleModule: getSingleModule,
            saveModules: saveModules,
            deleteModule: deleteModule,
            changeStatus: changeStatus,
            getAllMain: getAllMain,
            getParent: getParent,
            checkData: checkData
        }

    };//END ModulesService()
}());
