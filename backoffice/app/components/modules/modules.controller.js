(function () {
    "use strict";
    angular.module('modulesApp')
        .controller('ModulesController', ModulesController);

    ModulesController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ModulesService', 'toastr', 'SweetAlert'];

    function ModulesController($scope, $rootScope, $state, $location, ModulesService, toastr, SweetAlert) {
        var vm = this;

        vm.getModuleList = getModuleList;
        vm.getSingleModule = getSingleModule;
        vm.saveModules = saveModules;
        vm.deleteModule = deleteModule;
        vm.changePage = changePage;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.getAllMain = getAllMain;
               
        vm.sort = sort;
        vm.reset = reset;

        $rootScope.headerTitle = 'Modules';
        vm.totalSkills = 0;
        vm.skillsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.catFlag  = false;
        vm.catEditFlag = false;
        vm.title = 'Add New';
        vm.modulesForm = { module_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];
        
        vm.getAllMain();

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.modules');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleModule(path[3]);
            }
        } else {
           if(path[2] == "subedit" || path[2] == "subview") {
                vm.catEditFlag = true;
                vm.catFlag = true;
                vm.title = 'Edit Sub Modules';
                vm.getSingleModule(path[3]);
           } else if(path[2] == "subcreate") {
                vm.catFlag = true;
           }
        }

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getModuleList(1, '');
        }//END sort()

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()


        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getModuleList(newPage, searchInfo);
        }//END changePage();

        /* to save modules after add and edit  */
        function saveModules() {
            ModulesService.saveModules(vm.modulesForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Modules');
                        $state.go('backoffice.modules');
                    } else {
                        toastr.error(response.data.message, 'Modules');
                    }
                } else {
                    toastr.error(response.data.message, 'Modules');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Modules');
            });
        }//END saveModules();
  
        /* to get modules list */
        function getModuleList(newPage, obj) {
            vm.totalModule = 0;
            vm.moduleList = [];
            ModulesService.getModules(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.module && response.data.module.length > 0) {
                        vm.totalModule = response.data.total;
                        vm.moduleList = response.data.module;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSkillsList();

        /* to get single module  */
        function getSingleModule(module_id) {
            ModulesService.getSingleModule(module_id).then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleModule = response.data.module;
                        vm.modulesForm = response.data.module;
                         
                        if(response.data.module.parent_id != 0){
                            vm.singleModule.parent_name = '';
                            vm.modulesForm.parent_name = '';
                            var parent_id = response.data.module.parent_id;
                            ModulesService.getParent(parent_id).then(function (responses) {
                                if (responses.status == 200) {
                                    vm.singleModule.parent_name = responses.data.data.module_name;
                                    vm.modulesForm.parent_name = responses.data.data.module_name;
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }

                    } else {
                        toastr.error(response.data.message, 'SKILLS');
                        $state.go('backoffice.skills');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'SKILLS');
            });
        }//END getSingleModule();

        /** to delete a modules **/
        function deleteModule(item, index) {

            var dataCheck = { module_id: item.module_id };
            ModulesService.checkData(dataCheck).then(function(responses) {
                if (responses.status == 200) {
                    if (responses.data.status == 1 && responses.data.data > 0 ) {                    var message = "Sorry! Module cannot be deleted. Please delete sub modules first.";
                        SweetAlert.swal(message, "Error");
                        return false;                                
                    }

                    if (responses.data.status == 1 && responses.data.data == 0 ) { 

                        SweetAlert.swal({
                            title: "Are you sure you want to delete this module?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            html: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                ModulesService.deleteModule(item.module_id).then(function (response) {
                                    if (response.data.status == 1) {
                                        SweetAlert.swal("Deleted!", response.data.message, "success");
                                        vm.moduleList.splice(index, 1);
                                    } else {
                                        SweetAlert.swal("Deleted!", response.data.message, "error");
                                    }
                                }, function (error) {
                                    toastr.error(error.data.error, 'SKILLS');
                                });
                            }
                        });
                    }
                } else {
                    toastr.error(responses.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });        
        }//END deleteSkills();

        /* to change active/inactive status of module */ 
        function changeStatus(status) {

            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }

            var dataCheck = { module_id: status.module_id };
            ModulesService.checkData(dataCheck).then(function(responses) {
                if (responses.status == 200) {
                    if (responses.data.status == 1 && responses.data.data > 0 ) {                    var message = "Sorry! Module cannot be deactivated. Please deactivate sub modules first.";
                        SweetAlert.swal(message, "Error");
                        return false;                                
                    }

                    if (responses.data.status == 1 && responses.data.data == 0 ) {                        
                        var data = { module_id: status.module_id, status: statusId };
                        ModulesService.changeStatus(data).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {                        
                                    SweetAlert.swal("Success!", response.data.message, "success");
                                    $state.reload();
                                    return true;                        
                                }
                            } else {
                                toastr.error(response.data.error, 'Error');
                                return false;
                            }
                        },function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                } else {
                    toastr.error(responses.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });    
            
        }//END changeStatus()        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getModuleList(1, '');
        }//END reset();

        /* to get all mail modules listing */
        function getAllMain(){
            vm.modulesList = [];
            ModulesService.getAllMain().then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.modulesList = response.data.data;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllMain();
    }
}());
