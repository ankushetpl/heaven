(function () {
    "use strict";
    angular.module('suburbsApp')
        .controller('SuburbsController', SuburbsController);

    SuburbsController.$inject = ['$scope', '$rootScope', '$state', '$location', 'SuburbsService', 'toastr', 'SweetAlert', 'NgMap', '$anchorScroll'];

    function SuburbsController($scope, $rootScope, $state, $location, SuburbsService, toastr, SweetAlert, NgMap, $anchorScroll) {
        var vm = this;

        vm.getSuburbsList = getSuburbsList;
        vm.getSingleSuburbs = getSingleSuburbs;
        vm.saveSuburbs = saveSuburbs;
        vm.deleteSuburbs = deleteSuburbs;
        vm.changeStatus = changeStatus;
        vm.getAllcafe = getAllcafe;
        vm.getAllWorker = getAllWorker;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        $rootScope.headerTitle = 'Cab Booking';
        vm.totalSuburbs = 0;
        vm.suburbsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.suburbsForm = { suburbs_id: '' };
        vm.statusList = { '0': 'Not Approved', '1': 'Approved' };
        vm.rideStatusList = { '0': 'Pending', '1': 'Accepted', '2': 'Ongoing', '3': 'Completed', '4': 'Cancel' };


        /* to extract parameters from url */
        var path = $location.path().split("/");
        vm.getAllcafe();
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.suburbs');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleSuburbs(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getSuburbsList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getSuburbsList(newPage, searchInfo);
        }//END changePage();




        /* Map */
        // Overview path between orign and destination.
        // This does NOT exactly follow the path of a road. It is used to draw path on the map.
        var overviewPath=[];
        var overviewPathIndex=0;  // current index points of overview path

        // Detailed path between overview path points
        var detailedPathIndex=0;  // current index points of detailed path

        // When direction is changed
        // change overviewPath and reset driving directions
        vm.directionsChanged = function() {
            overviewPath = this.directions.routes[0].overview_path;

            vm.suburbsForm.suburbs_pick_lat = this.directions.routes[0].bounds.f.b;
            vm.suburbsForm.suburbs_pick_long = this.directions.routes[0].bounds.b.b;
            
            vm.suburbsForm.suburbs_drop_lat = this.directions.routes[0].bounds.f.f;
            vm.suburbsForm.suburbs_drop_long = this.directions.routes[0].bounds.b.f;

            vm.map.getStreetView().setPosition(overviewPath[0]);

            overviewPathIndex = 0; // set indexes to 0s
            detailedPathIndex = 0;
            // vm.drivingMode = false;   // stop driving
            var toContinue = null;     // reset continuing positon
        }              

        NgMap.getMap().then(function(map) {
            vm.map = map;
        });
        /* End Map */

        /* to save suburbs after add and edit  */
        function saveSuburbs() {
            SuburbsService.saveSuburbs(vm.suburbsForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Suburbs');
                        $state.go('backoffice.suburbs');
                    } else {
                        toastr.error(response.data.message, 'Suburbs');
                    }
                } else {
                    toastr.error(response.data.message, 'Suburbs');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Suburbs');
            });
        }//END saveSuburbs();

        /* to get suburbs list */
        function getSuburbsList(newPage, obj) {
            $location.hash('top');
            $anchorScroll();
            vm.totalSuburbs = 0;
            vm.suburbsList = [];
            vm.suburbsListCheck = false;
            SuburbsService.getSuburbs(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.suburbsListCheck = true;
                    if (response.data.suburbs && response.data.suburbs.length > 0) {
                        vm.totalSuburbs = response.data.total;
                        vm.suburbsList = response.data.suburbs;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSuburbsList();

        /* to get single suburbs */
        function getSingleSuburbs(suburbs_id) {
            SuburbsService.getSingleSuburbs(suburbs_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleSuburbs = response.data.suburbs;
                        vm.suburbsForm = response.data.suburbs;  
                        
                        if(response.data.suburbs.worker_user_id !=  ''){
                            var worker_id = response.data.suburbs.worker_user_id;
                            var worker_name = response.data.suburbs.worker_user_id;
                            vm.singleSuburbs.worker_name = '';

                            SuburbsService.getWorkerNameById(worker_id).then(function (responsesW) {
                                if (responsesW.status == 200) {
                                    vm.singleSuburbs.worker_name = responsesW.data.worker;
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }
                        
                        vm.getAllWorker(response.data.suburbs.cafe_user_id);  

                                 
                    } else {
                        toastr.error(response.data.message, 'Suburbs');
                        $state.go('backoffice.suburbs');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Suburbs');
            });
        }//END getSingleSuburbs();

        /** to delete a suburbs **/
        function deleteSuburbs(id, index) {
            SweetAlert.swal({
                title: "Sure you want to delete this cab request?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    SuburbsService.deleteSuburbs(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.suburbsList.splice(index, 1);
                            vm.totalSuburbs = vm.totalSuburbs - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Suburbs');
                    });
                }
            });
        }//END deleteSuburbs();

        /* to change active/inactive status of suburbs */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
                var rideId = 0;
            } else {
                var statusId = 1;
                var rideId = 1;
            }
            var data = { suburbs_id: status.suburbs_id, status: statusId, ride_status: rideId };
            SuburbsService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        if(statusId == 1) {
                            SweetAlert.swal("Success!", 'Booking request has been approved successfully.', "success");
                        } else {
                            SweetAlert.swal("Success!", 'Booking request has been disapproved.', "success");
                        }
                        
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* all cafe user list */
        function getAllcafe() {
            vm.cafeList = [];
            SuburbsService.getAllcafe('2').then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.cafeList = response.data.data;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllcafe()

        /* get all worker list */
        function getAllWorker(obj){
            if (typeof obj !== 'undefined' && obj !== '') {
                var cafeID = obj;
            }else{
                var cafeID = vm.suburbsForm.cafe_user_id;
            }
            
            vm.workerList = [];
            SuburbsService.getAllWorker(cafeID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.workerList = response.data.data;
                        console.log(vm.workerList);
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllWorker()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getSuburbsList(1, '');
        }//END reset();               
    }

}());
