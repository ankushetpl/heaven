(function () {

    angular.module('suburbsApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var suburbsPath = 'app/components/cabBook/';
        $stateProvider
            .state('backoffice.suburbs', {
                url: 'suburbs',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbsPath + 'views/index.html',
                        controller: 'SuburbsController',
                        controllerAs: 'suburbs'
                    }
                }
            })
            .state('backoffice.suburbs.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbsPath + 'views/form.html',
                        controller: 'SuburbsController',
                        controllerAs: 'suburbs'
                    }
                }
            })
            .state('backoffice.suburbs.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbsPath + 'views/form.html',
                        controller: 'SuburbsController',
                        controllerAs: 'suburbs'
                    }
                }
            })
            .state('backoffice.suburbs.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: suburbsPath + 'views/view.html',
                        controller: 'SuburbsController',
                        controllerAs: 'suburbs'
                    }
                }
            })
    }

}());