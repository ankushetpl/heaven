(function () {
    "use strict";
    angular
        .module('suburbsApp')
        .service('SuburbsService', SuburbsService);

    SuburbsService.$inject = ['$http', 'APPCONFIG', '$q'];

    function SuburbsService($http, APPCONFIG, $q) {

        /* save suburbs */
        function saveSuburbs(obj) {
            var URL = APPCONFIG.APIURL + 'suburbs/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.suburbs_id !== 'undefined' && obj.suburbs_id !== '') {
                    requestData['suburbs_id'] = obj.suburbs_id;
                    URL = APPCONFIG.APIURL + 'suburbs/edit';
                }

                if (typeof obj.cafe_user_id !== 'undefined' && obj.cafe_user_id !== '') {
                    requestData['cafe_user_id'] = obj.cafe_user_id;
                }

                if (typeof obj.worker_user_id !== 'undefined' && obj.worker_user_id !== '') {
                    requestData['worker_user_id'] = obj.worker_user_id;
                }

                if (typeof obj.suburbs_pick_name !== 'undefined' && obj.suburbs_pick_name !== '') {
                    requestData['suburbs_pick_name'] = obj.suburbs_pick_name;
                }

                if (typeof obj.suburbs_pick_address !== 'undefined' && obj.suburbs_pick_address !== '') {
                    requestData['suburbs_pick_address'] = obj.suburbs_pick_address;
                }

                if (typeof obj.suburbs_pick_lat !== 'undefined' && obj.suburbs_pick_lat !== '') {
                    requestData['suburbs_pick_lat'] = obj.suburbs_pick_lat;
                }

                if (typeof obj.suburbs_pick_long !== 'undefined' && obj.suburbs_pick_long !== '') {
                    requestData['suburbs_pick_long'] = obj.suburbs_pick_long;
                }

                if (typeof obj.suburbs_drop_name !== 'undefined' && obj.suburbs_drop_name !== '') {
                    requestData['suburbs_drop_name'] = obj.suburbs_drop_name;
                }

                if (typeof obj.suburbs_drop_address !== 'undefined' && obj.suburbs_drop_address !== '') {
                    requestData['suburbs_drop_address'] = obj.suburbs_drop_address;
                }

                if (typeof obj.suburbs_drop_lat !== 'undefined' && obj.suburbs_drop_lat !== '') {
                    requestData['suburbs_drop_lat'] = obj.suburbs_drop_lat;
                }

                if (typeof obj.suburbs_drop_long !== 'undefined' && obj.suburbs_drop_long !== '') {
                    requestData['suburbs_drop_long'] = obj.suburbs_drop_long;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.ride_status !== 'undefined' && obj.ride_status !== '') {
                    requestData['ride_status'] = obj.ride_status;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveSuburbs();

        /* to get all suburbs */
        function getSuburbs(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'suburbs';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.suburbs_id !== 'undefined' && obj.suburbs_id !== '') {
                    requestData['suburbs_id'] = parseInt(obj.suburbs_id);
                }

                if (typeof obj.suburbs_pick_name !== 'undefined' && obj.suburbs_pick_name !== '') {
                    requestData['suburbs_pick_name'] = obj.suburbs_pick_name;
                }

                if (typeof obj.suburbs_drop_name !== 'undefined' && obj.suburbs_drop_name !== '') {
                    requestData['suburbs_drop_name'] = obj.suburbs_drop_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.ride_status !== 'undefined' && obj.ride_status !== '') {
                    requestData['ride_status'] = parseInt(obj.ride_status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbs();

        /* to get single suburbs */
        function getSingleSuburbs(suburbs_id) {
            var URL = APPCONFIG.APIURL + 'suburbs/view';
            var requestData = {};

            if (typeof suburbs_id !== undefined && suburbs_id !== '') {
                requestData['suburbs_id'] = suburbs_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleSuburbs();

        /* to get single worker name */
        function getWorkerNameById(worker_id) {
            var URL = APPCONFIG.APIURL + 'user/workerName';
            var requestData = {};

            if (typeof worker_id !== undefined && worker_id !== '') {
                requestData['worker_id'] = worker_id;
            }

            return this.runHttp(URL, requestData);
        } //END getWorkerNameById();        

        /* to delete a suburbs from database */
        function deleteSuburbs(suburbs_id) {
            var URL = APPCONFIG.APIURL + 'suburbs/delete';
            var requestData = {};

            if (typeof suburbs_id !== undefined && suburbs_id !== '') {
                requestData['suburbs_id'] = suburbs_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteSuburbs();

        /* to change active/inactive status of suburbs */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'suburbs/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.suburbs_id != undefined && obj.suburbs_id != "") {
                    requestData["suburbs_id"] = obj.suburbs_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }  
                
                if (obj.ride_status != undefined || obj.ride_status != "") {
                    requestData["ride_status"] = obj.ride_status;
                }  
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to get all cafe user list */
        function getAllcafe(obj) {
            var URL = APPCONFIG.APIURL + 'user/AllList';

            var requestData = { }; 

            if (typeof obj !== 'undefined' && obj !== '') {
                requestData['usertype'] = parseInt(obj);
            }
            return this.runHttp(URL, requestData);
        }//END getAllcafe();

        /* to get all worker list */
        function getAllWorker(cafeID) {
            var URL = APPCONFIG.APIURL + 'user/AllSubList';

            var requestData = {}; 

            if (typeof cafeID !== 'undefined' && cafeID !== '') {
                requestData['cafeID'] = parseInt(cafeID);
            }

            return this.runHttp(URL, requestData);
        }//END getAllWorker();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getSuburbs: getSuburbs,
            getSingleSuburbs: getSingleSuburbs,
            saveSuburbs: saveSuburbs,
            getWorkerNameById:getWorkerNameById,
            deleteSuburbs: deleteSuburbs,
            changeStatus: changeStatus,
            getAllcafe: getAllcafe,
            getAllWorker: getAllWorker
        }

    };//END SuburbsService()
}());
