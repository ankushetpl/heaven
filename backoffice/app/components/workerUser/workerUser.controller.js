(function () {
    "use strict";
    angular.module('workerUserApp')
        .controller('WorkerUserController', WorkerUserController);

    WorkerUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'WorkerUserService', 'toastr', 'SweetAlert', 'Upload', '$timeout', 'APPCONFIG', 'ngProgressFactory', '$locale', '$anchorScroll'];

    function WorkerUserController($scope, $rootScope, $state, $location, WorkerUserService, toastr, SweetAlert, Upload, $timeout, APPCONFIG, ngProgressFactory, $locale, $anchorScroll) {
        var vm = this;

        vm.getWorkerUserList = getWorkerUserList;
        vm.getSingleWorkerUser = getSingleWorkerUser;
        vm.saveWorkerUser = saveWorkerUser;
        vm.deleteWorkerUser = deleteWorkerUser;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.suspendForms = suspendForms; 
        vm.changeSuspend = changeSuspend;
        vm.getAllCafeUser = getAllCafeUser;
        vm.sort = sort;
        vm.reset = reset;
        vm.close = close;
        vm.isDisabled = isDisabled;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;
        vm.allCitySuburbs = '';

        vm.getAllArea = getAllArea;
        vm.getAllSkill = getAllSkill;

        vm.getAllCountries();
        vm.getAllCafeUser();
        vm.getAllArea();
        vm.getAllSkill();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Worker';
        vm.totalWorkerUser = 0;
        vm.workerUserPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.usertypeFlag = false;
        vm.registertypeFlag = false;
        vm.title = 'Add New';
        vm.workerUserForm = { user_id: '', usertypeFlag: '3', registertypeFlag: '0' };
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];
        vm.imageFlag = false;

        $scope.regex = /^[@a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;

        vm.month = $locale.DATETIME_FORMATS.MONTH;
        vm.monthing= [];

        angular.forEach(vm.month, function(values, key) {
            vm.monthing.push({"id" : key + 1, "name" : values });
        });

        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.workerUser');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleWorkerUser(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting worker user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getWorkerUserList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getWorkerUserList(newPage, searchInfo);
        }//END changePage();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    vm.workerUserForm.workers_image = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to save worker user after add and edit  */
        function saveWorkerUser() {
            
            // if(typeof vm.workerUserForm.workers_city == 'undefined'){
            //     return true;
            // }else{
            //    if(typeof vm.workerUserForm.suburbA === 'undefined' && typeof vm.workerUserForm.suburbB === 'undefined' && typeof vm.workerUserForm.suburbC === 'undefined' && typeof vm.workerUserForm.suburbD === 'undefined'){
            //         toastr.error("Please select least one Suburb!", 'Worker');
            //         return false;
            //     }
            // }

            var ID = [];
            var IDNew = [];
            $(':checkbox:checked').each(function(i) {
                ID[i] = $(this).val();
            });
            
            angular.forEach(ID, function(values){
                if(values != 'on'){
                    IDNew.push(values);
                }
            });
            
            vm.workerUserForm.workers_suburb = IDNew;
                                      
            WorkerUserService.saveWorkerUser(vm.workerUserForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Worker');
                        $state.go('backoffice.workerUser');
                    } else {
                        if (response.data.status == 2) {
                            toastr.error(response.data.message, 'Worker');
                        }else{
                            if (response.data.status == 3) {
                                toastr.error(response.data.message, 'Worker');
                            }else{
                                toastr.error(response.data.message, 'Worker');
                            }
                        }                        
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                // console.log(error);
                toastr.error('Internal server error', 'Worker');
            });
                
        }//END saveWorkerUser();

        /* to get worker user list */
        function getWorkerUserList(newPage, obj) {
            vm.progressbar.start();
            vm.totalWorkerUser = 0;
            vm.workerUserList = [];
            vm.workerUserListCheck = false;
            WorkerUserService.getWorkerUser(newPage, obj, vm.orderInfo, '3').then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.workerUserListCheck = true;
                    if (response.data.users && response.data.users.length > 0) {
                        vm.totalWorkerUser = response.data.total;
                        vm.workerUserList = response.data.users;

                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWorkerUserList();

        /* to get single worker user */
        function getSingleWorkerUser(userId) { 
            $location.hash('top');
            $anchorScroll();
            var UserType = 3;           
            WorkerUserService.getSingleWorkerUser(userId, UserType).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorkerUser = response.data.data;

                        if(response.data.data.workers_phone != 0){
                            var phone = response.data.data.workers_phone;
                            var workers_phone = phone.toString().slice(2);
                        }

                        if(response.data.data.workers_mobile != 0){
                            var mobile = response.data.data.workers_mobile;
                            var workers_mobile = mobile.toString().slice(2);
                        }
                        
                        vm.workerUserForm.user_id = response.data.data.user_id;
                        vm.workerUserForm.usertype = response.data.data.user_role_id;
                        vm.workerUserForm.registertype = response.data.data.user_registertype;
                        vm.workerUserForm.user_name = response.data.data.workers_name;
                        vm.workerUserForm.user_email = response.data.data.workers_email;
                        vm.workerUserForm.user_phone = workers_phone;
                        vm.workerUserForm.user_address = response.data.data.workers_address;
                        vm.workerUserForm.status = response.data.data.status;
                        vm.workerUserForm.workers_mobile = workers_mobile;
                        vm.workerUserForm.cafeID = response.data.data.cafeuser_id;
                        vm.workerUserForm.workers_country = response.data.data.workers_country;
                        vm.workerUserForm.workers_state = response.data.data.workers_state;
                        vm.workerUserForm.workers_city = response.data.data.workers_city;

                        
                        vm.isDisabled = true;
                        vm.getStatesByID(vm.singleWorkerUser);
                        vm.getCityByID(vm.singleWorkerUser);
                        vm.getSuburbByID(vm.singleWorkerUser);

                        if(response.data.data.workers_country != 0){
                            var country = response.data.data.workers_country;
                            vm.singleWorkerUser.country = '';
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleWorkerUser.country = value.name;
                                }                               
                            }); 
                        } 
                        
                        
                        // console.log(arrWorkerSuburbs);

                        if(response.data.data.workers_city != 0){
                            var suburb = [];
                                    
                            suburb = response.data.data.workers_suburb;
                            var arrWorkerSuburbs = suburb.split(',');
                            vm.workerUserForm.workers_suburb = arrWorkerSuburbs;
                            var city_id = response.data.data.workers_city;
                            WorkerUserService.getSuburbsByCity(city_id).then(function (responsesS) {
                                
                                if (responsesS.status == 200) {
                                    if (responsesS.data.allSuburbData && responsesS.data.allSuburbData != '') {

                                        vm.allCitySuburbs = responsesS.data.allSuburbData;
                                        var workersub = vm.workerUserForm.workers_suburb;

                                        angular.forEach(vm.allCitySuburbs, function(key,values) {

                                            angular.forEach(workersub, function(values1) {

                                                if(values1 == key.suburb_id ){
                                                    key.existsFlag = '1';
                                                } 
                                            });
                                        });
                                       
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        } 
                        

                        // angular.forEach(vm.areaList, function(values) {
                        //     if(arr[0] == values.suburb_id ){
                        //         vm.singleWorkerUser.suburbA = values.suburb_name;
                        //         vm.workerUserForm.suburbA = values.suburb_id;
                        //     }

                        //     if(arr[1] == values.suburb_id ){
                        //         vm.singleWorkerUser.suburbB = values.suburb_name;
                        //         vm.workerUserForm.suburbB = values.suburb_id;
                        //     }

                        //     if(arr[2] == values.suburb_id ){
                        //         vm.singleWorkerUser.suburbC = values.suburb_name;
                        //         vm.workerUserForm.suburbC = values.suburb_id;
                        //     }

                        //     if(arr[3] == values.suburb_id ){
                        //         vm.singleWorkerUser.suburbD = values.suburb_name;
                        //         vm.workerUserForm.suburbD = values.suburb_id;
                        //     }
                        // });

                        if(vm.singleWorkerUser.workers_skills != ''){
                            
                            vm.skills = [];
                            
                            var skill = [];
                            skill = vm.singleWorkerUser.workers_skills;
                            var arrs = skill.split(',');

                            for(var i = 0; i < arrs.length; i++ ){
                            
                                angular.forEach(vm.skillList, function(values) {
                                    if(arrs[i] == values.skills_id ){
                                        vm.skills.push(values.skills_name);
                                    }
                                            
                                });
                            }
                            vm.singleWorkerUser.workers_skills = vm.skills;
                        }
                        

                        if(response.data.data.workers_image != 0){
                            var file_id = response.data.data.workers_image;
                            WorkerUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleWorkerUser.file = responses.data.file.file_base_url;
                                        vm.singleWorkerUser.workers_image = response.data.data.workers_image;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }

                        if(response.data.data.workers_codeConduct != 0){
                            var file_id = response.data.data.workers_codeConduct;
                            WorkerUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleWorkerUser.file1 = responses.data.file.file_base_url;
                                        vm.singleWorkerUser.workers_codeConduct1 = responses.data.file.file_original_name;
                                        vm.singleWorkerUser.workers_codeConduct = file_id;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }

                        if(response.data.data.workers_criminalReport != ''){
                            var file_id = response.data.data.workers_criminalReport;
                            WorkerUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleWorkerUser.file2 = responses.data.file.file_base_url;
                                        vm.singleWorkerUser.workers_criminalReport1 = responses.data.file.file_original_name;
                                        vm.singleWorkerUser.workers_criminalReport = file_id;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }

                        if(response.data.data.workers_photoIdentity != ''){
                            var file_id = response.data.data.workers_photoIdentity;
                            WorkerUserService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.singleWorkerUser.file3 = responses.data.file.file_base_url;
                                        vm.singleWorkerUser.workers_photoIdentity1 = responses.data.file.file_original_name;
                                        vm.singleWorkerUser.workers_photoIdentity = file_id;
                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }

                        if(response.data.data.cafeuser_id !=  ''){
                            var cafe_id = response.data.data.cafeuser_id;
                            var cafe_name = response.data.data.cafeuser_id;
                            vm.singleWorkerUser.cafe_name = '';
                            WorkerUserService.getCafeNameById(cafe_id).then(function (responsesCU) {
                                if (responsesCU.status == 200) {
                                    vm.singleWorkerUser.cafe_name = responsesCU.data.cafe.cafeusers_name;
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });
                        }
                        
                        vm.progressbar.complete();
                        
                    } else {
                        toastr.error(response.data.message, 'Worker');
                        $state.go('backoffice.workerUser');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Worker');
            });
        }//END getSingleWorkerUser();

        /** to delete a worker user **/
        function deleteWorkerUser(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this worker?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    WorkerUserService.deleteWorkerUser(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.workerUserList.splice(index, 1);
                            vm.totalWorkerUser = vm.totalWorkerUser - 1;
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Worker User');
                    });
                }
            });
        }//END deleteWorkerUser();

        /* to close model pupup */
        function close(){
            vm.getWorkerUserList(1, '');   
            $state.go('backoffice.workerUser');                              
        }//END close();

        vm.suspendForm = { user_id: '', is_suspend: '', user_role_id: '' };

        /* to show suspend form for Worker */ 
        function suspendForms(suspend){
            
            if(suspend.status == 0){
                SweetAlert.swal("Error!", "Sorry! please approve this worker first!", "error");
                return false;      
            }

            if(suspend.is_suspend == 1){
                SweetAlert.swal({
                    title: "Are you sure you want to unsuspend this worker?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, unsuspend it!",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    html: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        var data = { user_id: suspend.user_id, is_suspend: suspend.is_suspend, roleID : suspend.user_role_id, workers_suspendStart: '', workers_suspendEnd: '' };
                        WorkerUserService.changeSuspend(data).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {                        
                                    SweetAlert.swal("Suspend!", response.data.message, "success");
                                    $state.reload();
                                    return true;                        
                                }else {
                                    SweetAlert.swal("Suspend!", response.data.message, "error");
                                }
                            } else {
                                toastr.error(response.data.error, 'Error');
                                return false;
                            }
                        },function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                });  
            }else{
                vm.suspendForm.user_id = suspend.user_id; 
                vm.suspendForm.is_suspend = suspend.is_suspend;
                vm.suspendForm.roleID = suspend.user_role_id; 
                vm.suspendForm.workers_suspendStart = '';
                vm.suspendForm.workers_suspendEnd = '';

                var logElem = angular.element("#responsiveSuspend");
                logElem.modal("show");                
            }            
        }//END suspendForms()

        function changeSuspend(suspend) {
            SweetAlert.swal({
                title: "Are you sure you want to suspend this worker?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, suspend it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var logElem = angular.element(".modal-backdrop");
                    logElem.removeClass("show"); 
                    logElem.addClass("hide"); 

                    var logElem = angular.element("body");
                    logElem.removeClass("modal-open"); 

                    WorkerUserService.changeSuspend(vm.suspendForm).then(function(response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {                        
                                SweetAlert.swal("Suspend!", response.data.message, "success");
                                $state.reload();
                                return true;                        
                            }else {
                                SweetAlert.swal("Suspend!", response.data.message, "error");
                            }
                        } else {
                            toastr.error(response.data.error, 'Error');
                            return false;
                        }
                    },function (error) {
                        toastr.error(error.data.error, 'Error');
                    });
                }
            });            
        }//END changeSuspend()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getWorkerUserList(1, '');
        }//END reset();

        /* get cafe name */
        function getAllCafeUser(){
            vm.cafeUserList = [];
            WorkerUserService.getAllCafeUser('2').then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.cafeUserList = response.data.data;                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllCafeUser();        
        
        /* Get All Countries */
        function getAllCountries(){
            WorkerUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){
            // console.log(obj);
            WorkerUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){

            WorkerUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        // vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        delete vm.workerUserForm.workers_city;
                        // vm.hideSuburb = true;
                        // delete vm.workerUserForm.workers_suburb;
                        // delete vm.workerUserForm.suburbB;
                        // delete vm.workerUserForm.suburbC;
                        // delete vm.workerUserForm.suburbD;
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            // console.log(obj);
            WorkerUserService.getSuburbByID(obj).then(function (response) {
                console.log(response);
                if (response.status == 200) {
                    if (response.data.allSuburbData && response.data.allSuburbData.length > 0) {
                        
                        vm.allCitySuburbs = response.data.allSuburbData;
                        // vm.hideSuburb = false;

                        vm.allCitySuburbs = response.data.allSuburbData;
                        var workersubNew = obj.workers_suburb;

                        angular.forEach(vm.allCitySuburbs, function(key,values) {

                            angular.forEach(workersubNew, function(values1) {

                                if(values1 == key.suburb_id ){
                                    key.existsFlag = '1';
                                } 
                            });
                        });
                        console.log(vm.allCitySuburbs);
                    } else{
                        // vm.hideSuburb = true;
                        delete vm.allCitySuburbs;
                        // delete vm.workerUserForm.suburbB;
                        // delete vm.workerUserForm.suburbC;
                        // delete vm.workerUserForm.suburbD;
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker');
            });
        }// END getSuburbByID();

        /* to get all area [Suburb]  */
        function getAllArea(){
            WorkerUserService.getAllArea().then(function (response) {
                if (response.status == 200) {
                    if (response.data.area && response.data.area.length > 0) {
                        vm.areaList = response.data.area;
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker');
            });
        }//END getAllArea();

        /* to get all skills  */
        function getAllSkill(){
            WorkerUserService.getAllSkill().then(function (response) {
                if (response.status == 200) {
                    if (response.data.skill && response.data.skill.length > 0) {
                        vm.skillList = response.data.skill;
                    }
                } else {
                    toastr.error(response.data.message, 'Worker');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Worker');
            });
        }//END getAllSkill();

    }

}());
