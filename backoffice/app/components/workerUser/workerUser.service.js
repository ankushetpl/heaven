(function () {
    "use strict";
    angular
        .module('workerUserApp')
        .service('WorkerUserService', WorkerUserService);

    WorkerUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WorkerUserService($http, APPCONFIG, $q) {

        /* save worker user */
        function saveWorkerUser(obj) {
            var URL = APPCONFIG.APIURL + 'user/create';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.registertypeFlag !== undefined && obj.registertypeFlag !== '') {
                    requestData['registertype'] = obj.registertypeFlag;
                }

                if (typeof obj.user_address !== undefined && obj.user_address !== '') {
                    requestData['address'] = obj.user_address;
                }

                if (typeof obj.user_email !== undefined && obj.user_email !== '') {
                    requestData['email'] = obj.user_email;
                }

                if (typeof obj.user_name !== undefined && obj.user_name !== '') {
                    requestData['name'] = obj.user_name;
                }

                if (typeof obj.user_phone !== undefined && obj.user_phone !== '') {
                    requestData['phone'] = obj.user_phone;
                }

                if (typeof obj.workers_mobile !== undefined && obj.workers_mobile !== '') {
                    requestData['workers_mobile'] = obj.workers_mobile;
                }

                if (typeof obj.usertypeFlag !== undefined && obj.usertypeFlag !== '') {
                    requestData['usertype'] = obj.usertypeFlag;
                }

                if (typeof obj.cafeID !== undefined && obj.cafeID !== '') {
                    requestData['cafeID'] = obj.cafeID;
                }

                if (typeof obj.workers_country !== undefined && obj.workers_country !== '') {
                    requestData['cafeusers_country'] = obj.workers_country;
                }

                if (typeof obj.workers_state !== undefined && obj.workers_state !== '') {
                    requestData['cafeusers_state'] = obj.workers_state;
                }

                if (typeof obj.workers_city !== undefined && obj.workers_city !== '') {
                    requestData['cafeusers_city'] = obj.workers_city;
                }

                if (typeof obj.workers_suburb !== undefined && obj.workers_suburb !== '') {
                    requestData['workers_suburb'] = obj.workers_suburb;
                }

                // if (typeof obj.suburbB !== undefined && obj.suburbB !== '') {
                //     requestData['suburbB'] = obj.suburbB;
                // }

                // if (typeof obj.suburbC !== undefined && obj.suburbC !== '') {
                //     requestData['suburbC'] = obj.suburbC;
                // }

                // if (typeof obj.suburbD !== undefined && obj.suburbD !== '') {
                //     requestData['suburbD'] = obj.suburbD;
                // }
            }

            return this.runHttp(URL, requestData);
        } //END saveWorkerUser();

        /* to get all worker user  */
        function getWorkerUser(pageNum, obj, orderInfo, usertype) {
            var URL = APPCONFIG.APIURL + 'user';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof usertype !== 'undefined' && usertype !== '') {
                requestData['usertype'] = usertype;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['admin_name'] = obj.workers_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.is_suspend !== 'undefined' && obj.is_suspend !== '') {
                    requestData['is_suspend'] = parseInt(obj.is_suspend);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            // console.log(requestData);
            return this.runHttp(URL, requestData);
        }//END getWorkerUser();

        /* to get single woker */
        function getSingleWorkerUser(userId, UserType) {
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};

            if (typeof userId !== undefined && userId !== '') {
                requestData['user_id'] = userId;
            }
            if (typeof UserType !== undefined && UserType !== '') {
                requestData['UserType'] = UserType;
            }
            
            return this.runHttp(URL, requestData);
        } //END getSingleWorkerUser();

        /* to get all suburbs according to city */
        function getSuburbsByCity(city_id) {
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByCityId';
            var requestData = {};

            if (typeof city_id !== undefined && city_id !== '') {
                requestData['city_id'] = city_id;
            }
            
            return this.runHttp(URL, requestData);
        } //END getCafeNameById();

        /* to get cafe user name */
        function getCafeNameById(cafe_id) {
            var URL = APPCONFIG.APIURL + 'user/cafeName';
            var requestData = {};

            if (typeof cafe_id !== undefined && cafe_id !== '') {
                requestData['cafe_id'] = cafe_id;
            }
            
            return this.runHttp(URL, requestData);
        } //END getCafeNameById();


        /* to delete a worker from database */
        function deleteWorkerUser(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteWorkerUser();

        /* to change active/inactive suspend of worker user */
        function changeSuspend(obj) {
            var URL = APPCONFIG.APIURL + 'user/suspend';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj != undefined) {
                
                    if (obj.user_id != undefined && obj.user_id != "") {
                        requestData["user_id"] = obj.user_id;
                    }
    
                    if (obj.is_suspend != undefined || obj.is_suspend != "") {
                        requestData["is_suspend"] = obj.is_suspend;
                    }  
                    
                    if (obj.roleID != undefined || obj.roleID != "") {
                        requestData["usertype"] = obj.roleID;
                    }
    
                    if (obj.workers_suspendStart != undefined || obj.workers_suspendStart != "") {
                        requestData["suspendStart"] = obj.workers_suspendStart;
                    }
    
                    if (obj.workers_suspendEnd != undefined || obj.workers_suspendEnd != "") {
                        requestData["suspendEnd"] = obj.workers_suspendEnd;
                    }
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeSuspend()

        /* to get all cafe user  */
        function getAllCafeUser(usertype) {
            var URL = APPCONFIG.APIURL + 'user/AllList';

            var requestData = { usertype };              
                   
            return this.runHttp(URL, requestData);
        }//END getAllCafeUser();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get States by ID
        function getStatesByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allStatesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_country !== 'undefined' && obj.workers_country !== '') {
                    requestData['ID'] = parseInt(obj.workers_country);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get City by ID
        function getCityByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCitiesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_state !== 'undefined' && obj.workers_state !== '') {
                    requestData['ID'] = parseInt(obj.workers_state);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get Suburb by ID
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByIDWorker';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }                  
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        //Get all suburb 
        function getAllArea(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbs';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllArea();

        //Get all skill 
        function getAllSkill(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSkills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllSkill();

        /* to check Assessment */
        function checkAssessment(userID, workersID) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkAssessment';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['cafe_id'] = userID;
            }

            if (typeof workersID !== undefined && workersID !== '') {
                requestData['worker_id'] = workersID;
            }
                        
            return this.runHttp(URL, requestData);
        } //END checkAssessment();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            saveWorkerUser: saveWorkerUser,
            getWorkerUser: getWorkerUser,
            getSingleWorkerUser: getSingleWorkerUser,
            getSuburbsByCity:getSuburbsByCity,
            getAllCafeUser: getAllCafeUser,
            deleteWorkerUser: deleteWorkerUser,
            getCafeNameById:getCafeNameById,
            changeSuspend: changeSuspend,
            getImage: getImage,
            getAllCountries: getAllCountries,
            getStatesByID: getStatesByID,
            getCityByID: getCityByID,
            getAllArea: getAllArea,
            getAllSkill: getAllSkill,
            getSuburbByID: getSuburbByID,
            checkAssessment: checkAssessment
        }

    };//END WorkerUserService()
}());
