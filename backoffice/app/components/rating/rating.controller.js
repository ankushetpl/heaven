(function () {
    "use strict";
    angular.module('ratingApp')
        .controller('RatingController', RatingController);

    RatingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'RatingService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function RatingController($scope, $rootScope, $state, $location, RatingService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getRatingList = getRatingList;
        vm.getSingleRating = getSingleRating;
        vm.saveRating = saveRating;
        vm.getAllType = getAllType;
        vm.deleteRating = deleteRating;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.resetForm = resetForm;
        vm.isDisabled = isDisabled;

        vm.getAllType();

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Rating';
        vm.totalRating = 0;
        vm.ratingPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.ratingForm = { rating_id: ''};
        vm.statusList = [{ id : 0, status : 'Not Approved'}, { id : 1, status : 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.rating');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleRating(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRatingList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRatingList(newPage, searchInfo);
        }//END changePage();

        /* to save Rating after add and edit  */
        function saveRating() {
            RatingService.saveRating(vm.ratingForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Rating');
                        $state.go('backoffice.rating');
                    } else {
                        toastr.error(response.data.message, 'Rating');
                    }
                } else {
                    toastr.error(response.data.message, 'Rating');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Rating');
            });
        }//END saveRating();

        /* to get rating list */
        function getRatingList(newPage, obj) {
            vm.progressbar.start();
            vm.totalRating = 0;
            vm.ratingList = [];
            vm.ratingListCheck = false;
            RatingService.getRating(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.ratingListCheck = true;
                    if (response.data.rating && response.data.rating.length > 0) {
                        vm.totalRating = response.data.total;
                        vm.ratingList = response.data.rating;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRatingList();

        /* to get single Rating */
        function getSingleRating(rating_id) {
            $location.hash('top');
            $anchorScroll();
            RatingService.getSingleRating(rating_id).then(function (response) {                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleRating = response.data.rating;
                        vm.ratingForm = response.data.rating;                       
                    } else {
                        toastr.error(response.data.message, 'Rating');
                        $state.go('backoffice.rating');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Rating');
            });
        }//END getSingleRating();

        /** to delete a rating **/
        function deleteRating(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Rating?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    RatingService.deleteRating(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.ratingList.splice(index, 1);
                            vm.totalRating = vm.totalRating - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Rating');
                    });
                }
            });
        }//END deleteRating();

        /* to change active/inactive status of Rating */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { rating_id: status.rating_id, status: statusId };
            RatingService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* get all service */
        function getAllType(){
            RatingService.getAllTypes().then(function (response) {
                if (response.status == 200) {
                    if (response.data.type && response.data.type.length > 0) {
                        
                        vm.typeList = response.data.type; 
                        vm.typeList.push({ id: "0", name: "Other" });
                    }
                } else {
                    toastr.error(response.data.message, 'Rating');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Rating');
            });
        }//END getAllType();
        
        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRatingList(1, '');
        }//END reset();    

        
        /* to reset all form parameters */
        function resetForm(){
            vm.ratingForm = { rating_id: ''};
            vm.ratingForm.$setPristine();
        }//END resetForm();
    }

}());
