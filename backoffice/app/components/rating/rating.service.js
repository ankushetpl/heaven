(function () {
    "use strict";
    angular
        .module('ratingApp')
        .service('RatingService', RatingService);

    RatingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function RatingService($http, APPCONFIG, $q) {

        /* save Rating */
        function saveRating(obj) {
            var URL = APPCONFIG.APIURL + 'rating/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.rating_id !== 'undefined' && obj.rating_id !== '') {
                    requestData['rating_id'] = obj.rating_id;
                    URL = APPCONFIG.APIURL + 'rating/edit';
                }

                if (typeof obj.rating_type !== 'undefined' && obj.rating_type !== '') {
                    requestData['rating_type'] = obj.rating_type;
                }

                if (typeof obj.rating_name !== 'undefined' && obj.rating_name !== '') {
                    requestData['rating_name'] = obj.rating_name;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveRating();

        /* to get all Rating */
        function getRating(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'rating';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.rating_id !== 'undefined' && obj.rating_id !== '') {
                    requestData['rating_id'] = parseInt(obj.rating_id);
                }

                if (typeof obj.rating_type !== 'undefined' && obj.rating_type !== '') {
                    requestData['rating_type'] = obj.rating_type;
                }

                if (typeof obj.rating_name !== 'undefined' && obj.rating_name !== '') {
                    requestData['rating_name'] = obj.rating_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getRating();

        /* to get single Rating */
        function getSingleRating(rating_id) {
            var URL = APPCONFIG.APIURL + 'rating/view';
            var requestData = {};

            if (typeof rating_id !== undefined && rating_id !== '') {
                requestData['rating_id'] = rating_id;
            }

            return this.runHttp(URL, requestData);
        } //END getRatingById();

        /* to delete a Rating from database */
        function deleteRating(rating_id) {
            var URL = APPCONFIG.APIURL + 'rating/delete';
            var requestData = {};

            if (typeof rating_id !== undefined && rating_id !== '') {
                requestData['rating_id'] = rating_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteRating();

        /* to change active/inactive status of Rating */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'rating/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.rating_id != undefined && obj.rating_id != "") {
                    requestData["rating_id"] = obj.rating_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

         //Get All types
        function getAllTypes(){
            var URL = APPCONFIG.APIURL + 'rating/allTypes';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllTypes();
        
        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getRating: getRating,
            getSingleRating: getSingleRating,
            saveRating: saveRating,
            deleteRating: deleteRating,
            getAllTypes : getAllTypes,
            changeStatus: changeStatus,
        }

    };//END RatingService()
}());
