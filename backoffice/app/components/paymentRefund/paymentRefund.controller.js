(function () {
    "use strict";
    angular.module('paymentRefundApp')
        .controller('paymentRefundController', paymentRefundController);

    paymentRefundController.$inject = ['$scope', '$rootScope', '$state', '$location', 'PaymentRefundService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function paymentRefundController($scope, $rootScope, $state, $location, PaymentRefundService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getRefundList = getRefundList;
        vm.getSingleRefund = getSingleRefund;
        vm.openModal = openModal;
        vm.saveRefund = saveRefund;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Payment Refund';
        vm.totalClients = 0;
        vm.clientsPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        
        /* to extract parameters from url */
        var path = $location.path().split("/");       
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.paymentRefund');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleRefund(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getRefundList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getRefundList(newPage, searchInfo);
        }//END changePage();

        vm.refundForm = {'client_id' : '','booking_id' : '','serviceflag' : '','refundAmt' : ''};
        function openModal(client_id,id,serviceflag,refundAmt) {

            $('#responsive-modalTop').modal('show');
            vm.refundForm = {'client_id': client_id, 'booking_id' : id,'serviceflag' : serviceflag,'refundAmt' : refundAmt};
        }

        /* to save refund data */
        function saveRefund() {
            // console.log(vm.refundForm);
            PaymentRefundService.refundPayment(vm.refundForm).then(function (response) {
                console.log(response);
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $('#responsive-modalTop').modal('hide');
                        toastr.success(response.data.message, 'paymentRefund');
                        $state.go('backoffice.paymentRefund');
                        getRefundList(1, '');
                    } else {
                        toastr.error(response.data.message, 'paymentRefund');
                    }
                } else {
                    toastr.error(response.data.message, 'paymentRefund');
                }
            }, function (error) {
                console.log(error);
                toastr.error('Internal server error', 'paymentRefund');
            });
        }//END saveRefund();

        /* to get clients refund list */
        function getRefundList(newPage, obj) {
            vm.progressbar.start();
            vm.totalRefundData = 0;
            vm.refundList = [];
            vm.clientsListCheck = false;
            // vm.walletForm = {user_id : ''};
            PaymentRefundService.getRefundList(newPage, obj, vm.orderInfo).then(function (response) {
                // console.log(response);
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.clientsListCheck = true;

                    if (response.data.onceData && response.data.onceData.length > 0) {

                        var totalOnce = response.data.total;
                        var refundDataOnce = response.data.onceData;
                        
                        PaymentRefundService.getRefundListWeek(newPage, obj, vm.orderInfo).then(function (responseW) {
                            
                            if (responseW.status == 200) {
                                if (responseW.data.weekData && responseW.data.weekData.length > 0) {
                                    var totalWeek = responseW.data.total;
                                    var refundDataWeek = responseW.data.weekData;

                                    vm.totalRefundData = totalOnce + totalWeek;
                                    vm.refundList = refundDataOnce.concat(refundDataWeek);
                                    
                                }
                            }

                        }, function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                       
                    } else {
                        PaymentRefundService.getRefundListWeek(newPage, obj, vm.orderInfo).then(function (responseW) {
                            // console.log(responseW);
                            if (responseW.status == 200) {
                                vm.progressbar.complete();
                                vm.clientsListCheck = true;
                                if (responseW.data.weekData && responseW.data.weekData.length > 0) {
                                    console.log(responseW.data.weekData);
                                    vm.totalRefundData = responseW.data.total;
                                    vm.refundList = responseW.data.weekData;
                                   
                                }
                            }

                        }, function (error) {
                            toastr.error(error.data.error, 'Error');
                        });
                    }
                }   
                
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getRefundList();

        /* to get single refund data */
        function getSingleRefund(booking_id) {
            $location.hash('top');
            $anchorScroll();
            PaymentRefundService.getSingleRefund(booking_id).then(function (response) {
                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleRefund = response.data.refund;
                        // vm.buyCreditForm = response.data.buyCredit;
                    } else {
                        toastr.error(response.data.message, 'paymentRefund');
                        $state.go('backoffice.paymentRefund');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'paymentRefund');
            });
        }//END getSingleRefund();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getRefundList(1, '');
        }//END reset();               
    }

}());
