(function () {
    "use strict";
    angular
        .module('paymentRefundApp')
        .service('PaymentRefundService', PaymentRefundService);

    PaymentRefundService.$inject = ['$http', 'APPCONFIG', '$q'];

    function PaymentRefundService($http, APPCONFIG, $q) {

        /* refund booking payment into clients wallet  */
        function refundPayment(obj) {
            // console.log(obj);
            var URL = APPCONFIG.APIURL + 'paymentRefund/refund';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.client_id !== 'undefined' && obj.client_id !== '') {
                    requestData['client_id'] = obj.client_id;
                }

                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = obj.booking_id;
                }

                if (typeof obj.serviceflag !== 'undefined' && obj.serviceflag !== '') {
                    requestData['serviceflag'] = obj.serviceflag;
                }

                if (typeof obj.refundAmt !== 'undefined' && obj.refundAmt !== '') {
                    requestData['refundAmt'] = obj.refundAmt;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END refundPayment();

        /* to get all once off */
        function getRefundList(pageNum, obj, orderInfo) {
            // console.log(obj);
            // return false;
            var URL = APPCONFIG.APIURL + 'paymentRefund';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = (obj.booking_id);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getRefundList();


        /* to get all week data */
        function getRefundListWeek(pageNum, obj, orderInfo) {
            // console.log(obj);
            // return false;
            var URL = APPCONFIG.APIURL + 'paymentRefund/week';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = (obj.booking_id);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getRefundListWeek();        

        /* to get single refund data */
        function getSingleRefund(user_id) {
            var URL = APPCONFIG.APIURL + 'paymentRefund/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleRefund();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            refundPayment: refundPayment,
            getRefundList: getRefundList,
            getRefundListWeek:getRefundListWeek,
            getSingleRefund:getSingleRefund
        }

    };//END PaymentRefundService()
}());
