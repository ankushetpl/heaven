(function () {

    angular.module('paymentRefundApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var paymentRefundPath = 'app/components/paymentRefund/';
        $stateProvider
            .state('backoffice.paymentRefund', {
                url: 'paymentRefund',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentRefundPath + 'views/index.html',
                        controller: 'paymentRefundController',
                        controllerAs: 'paymentRefund'
                    }
                }
            })
            // .state('backoffice.walletTopUp.view', {
            //     url: '/view/:id',
            //     views: {
            //         'content@backoffice': {
            //             templateUrl: walletTopUpPath + 'views/view.html',
            //             controller: 'WalletTopUpController',
            //             controllerAs: 'walletTopUp'
            //         }
            //     }
            // })
    }

}());