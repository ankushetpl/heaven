(function () {
    "use strict";
    angular
        .module('tutorialApp')
        .service('TutorialService', TutorialService);

    TutorialService.$inject = ['$http', 'APPCONFIG', '$q'];

    function TutorialService($http, APPCONFIG, $q) {

        /* save tutorial */
        function saveTutorial(obj) {
            
            var URL = APPCONFIG.APIURL + 'tutorial/add';
            
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.tutorial_id !== 'undefined' && obj.tutorial_id !== '') {
                    requestData['tutorial_id'] = obj.tutorial_id;
                    URL = APPCONFIG.APIURL + 'tutorial/edit';
                }

                if (typeof obj.tutorial_name !== 'undefined' && obj.tutorial_name !== '') {
                    requestData['tutorial_name'] = obj.tutorial_name;
                }

                if (typeof obj.tutorial_des !== 'undefined' && obj.tutorial_des !== '') {
                    requestData['tutorial_des'] = obj.tutorial_des;
                }

                if (typeof obj.tutorial_type !== 'undefined' && obj.tutorial_type !== '') {
                    requestData['tutorial_type'] = obj.tutorial_type;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = obj.role_id;
                }               
            }

            return this.runHttp(URL, requestData);
        } //END saveTutorial();

        /* to get all tutorial */
        function getTutorial(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'tutorial';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.tutorial_id !== 'undefined' && obj.tutorial_id !== '') {
                    requestData['tutorial_id'] = parseInt(obj.tutorial_id);
                }

                if (typeof obj.tutorial_name !== 'undefined' && obj.tutorial_name !== '') {
                    requestData['tutorial_name'] = obj.tutorial_name;
                }
                
                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.role_id !== 'undefined' && obj.role_id !== '') {
                    requestData['role_id'] = parseInt(obj.role_id);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getTutorial();

        /* to get single tutorial */
        function getSingleTutorial(tutorial_id) {
            var URL = APPCONFIG.APIURL + 'tutorial/view';
            var requestData = {};

            if (typeof tutorial_id !== undefined && tutorial_id !== '') {
                requestData['tutorial_id'] = tutorial_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleTutorial();


        /* to get image */
        function getImage(fileId) {
            var URL = APPCONFIG.APIURL + 'tutorial/getImageData';
            var requestData = {};

            if (typeof fileId !== undefined && fileId !== '') {
                requestData['fileId'] = fileId;
            }

            return this.runHttp(URL, requestData);
        } //END getImage();


        /* to delete a tutorial from database */
        function deleteTutorial(tutorial_id) {
            var URL = APPCONFIG.APIURL + 'tutorial/delete';
            var requestData = {};

            if (typeof tutorial_id !== undefined && tutorial_id !== '') {
                requestData['tutorial_id'] = tutorial_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteTutorial();

        /* to change active/inactive status of tutorial */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'tutorial/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.tutorial_id != undefined && obj.tutorial_id != "") {
                    requestData["tutorial_id"] = obj.tutorial_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to get all role */
        function getAllRoll(pageNum) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = { pageNum }; 
            requestData['orderColumn'] = 'role_name'; 
            requestData['orderBy'] = 'ASC';          
            return this.runHttp(URL, requestData);
        }//END getAllRoll();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getTutorial: getTutorial,
            getSingleTutorial: getSingleTutorial,
            getImage:getImage,
            saveTutorial: saveTutorial,
            deleteTutorial: deleteTutorial,
            changeStatus: changeStatus,
            getAllRoll: getAllRoll
        }

    };//END TutorialService()
}());
