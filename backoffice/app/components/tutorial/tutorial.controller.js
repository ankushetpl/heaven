(function () {
    "use strict";
    angular.module('tutorialApp')
        .controller('TutorialController', TutorialController);

    TutorialController.$inject = ['$scope', '$rootScope', '$state', '$location', 'TutorialService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', 'Upload', 'APPCONFIG', '$anchorScroll'];

    function TutorialController($scope, $rootScope, $state, $location, TutorialService, toastr, SweetAlert, $timeout, ngProgressFactory, Upload, APPCONFIG, $anchorScroll) {
        var vm = this;

        vm.getTutorialList = getTutorialList;
        vm.getSingleTutorial = getSingleTutorial;
        vm.saveTutorial = saveTutorial;
        vm.saveImageTutorial = saveImageTutorial;
        vm.saveTextTutorial = saveTextTutorial;
        vm.deleteTutorial = deleteTutorial;
        vm.changeStatus = changeStatus;
        vm.getAllRoll = getAllRoll;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.resetForm = resetForm;
        vm.isDisabled = isDisabled;

        vm.getAllRoll(); 

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Tutorial';
        vm.totalTutorial = 0;
        vm.tutorialPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.tutorialTextForm = { tutorial_id: '', tutorial_type: 0 };
        vm.tutorialImageForm = { tutorial_id: '', tutorial_type: 1 };
        vm.tutorialForm = { tutorial_id: '', tutorial_type: 2 };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];
        vm.IsVisible = false;
        vm.imageFlag = false;
        
        /* to extract parameters from url */   
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "editImage" || path[2] == "editText" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.tutorial');
                return false;
            } else {
                vm.editFlag = true;
                vm.title = 'Edit';
                $timeout( function(){
                    vm.getSingleTutorial(path[3]);
                }, 100 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getTutorialList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getTutorialList(newPage, searchInfo);
        }//END changePage();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status == 1) {
                    
                    vm.tutorialImageForm.tutorial_des = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to save tutorial after add and edit  */
        function saveTutorial() {
            vm.tutorialForm.tutorial_type = '2';
            TutorialService.saveTutorial(vm.tutorialForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Tutorial');
                        $state.go('backoffice.tutorial');
                    } else {
                        toastr.error(response.data.message, 'Tutorial');
                    }
                } else {
                    toastr.error(response.data.message, 'Tutorial');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Tutorial');
            });
        }//END saveTutorial();

         /* to save tutorial after add and edit  */
        function saveImageTutorial() {
            
            if (typeof vm.tutorialImageForm.tutorial_des !== 'undefined' && vm.tutorialImageForm.tutorial_des !== '') { //check if from is valid
                
                if(vm.tutorialImageForm.tutorial_des.size > 0 && vm.tutorialImageForm.tutorial_des.name !== '' ){
                    vm.upload(vm.tutorialImageForm.tutorial_des); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){  
                    
                    vm.tutorialImageForm.tutorial_type = '1';                    
                        TutorialService.saveTutorial(vm.tutorialImageForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    toastr.success(response.data.message, 'Tutorial');
                                    $state.go('backoffice.tutorial');
                                } else {
                                    toastr.error(response.data.message, 'Tutorial');
                                }
                            } else {
                                toastr.error(response.data.message, 'Tutorial');
                            }
                        }, function (error) {
                            toastr.error('Internal server error', 'Tutorial');
                        });
                    }
                }, 5000 );
            }
            
        }//END saveImageTutorial();

        /* to save tutorial after add and edit  */
        function saveTextTutorial() {
            
            vm.tutorialTextForm.tutorial_type = '0';
            TutorialService.saveTutorial(vm.tutorialTextForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Tutorial');
                        $state.go('backoffice.tutorial');
                    } else {
                        toastr.error(response.data.message, 'Tutorial');
                    }
                } else {
                    toastr.error(response.data.message, 'Tutorial');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Tutorial');
            });
        }//END saveTextTutorial();

        /* to get tutorial list */
        function getTutorialList(newPage, obj) {
            vm.progressbar.start();
            vm.totalTutorial = 0;
            vm.tutorialList = [];
            vm.tutorialListCheck = false;
            TutorialService.getTutorial(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.tutorialListCheck = true;
                    if (response.data.tutorial && response.data.tutorial.length > 0) {
                        vm.totalTutorial = response.data.total;
                        vm.tutorialList = response.data.tutorial;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getTutorialList();

        /* to get single tutorial */
        function getSingleTutorial(tutorial_id) {
            // $location.hash('top');
            $anchorScroll();
            TutorialService.getSingleTutorial(tutorial_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {

                        if(response.data.tutorial.tutorial_type == 1) {

                            var fileId = response.data.tutorial.tutorial_des;

                            TutorialService.getImage(fileId).then(function (responseI) {
                                
                                vm.tutorialImageForm.tutorial_des = responseI.data.imageUrl;

                            }, function (error) {
                                toastr.error(error.data.error, 'Tutorial');
                            });
                        }
                        vm.singleTutorial = response.data.tutorial;
                        vm.tutorialForm = response.data.tutorial;
                        vm.tutorialImageForm = response.data.tutorial;
                        vm.tutorialTextForm = response.data.tutorial;

                        $scope.videoURL = response.data.tutorial.tutorial_des;
                    } else {
                        toastr.error(response.data.message, 'Tutorial');
                        $state.go('backoffice.tutorial');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Tutorial');
            });
        }//END getSingleTutorial();

        /** to delete a tutorial **/
        function deleteTutorial(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this tutorial?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    TutorialService.deleteTutorial(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.tutorialList.splice(index, 1);
                            vm.totalTutorial = vm.totalTutorial - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Tutorial');
                    });
                }
            });
        }//END deleteTutorial();

        /* to change active/inactive status of tutorial */ 
        function changeStatus(status) {
            if(status.status == 1){
                var statusId = 0;  
            }else{
                var statusId = 1;  
            }
            var data = { tutorial_id: status.tutorial_id, status: statusId };
            TutorialService.changeStatus(data).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* all role type */ 
        function getAllRoll(){
            vm.roleList = [];
            TutorialService.getAllRoll('1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.roleList = response.data.roles;                                                
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllRoll()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getTutorialList(1, '');
        }//END reset();

        /* to reset all parameters  */
        function resetForm() {
            vm.tutorialForm = { tutorial_id: '' };
            vm.tutorialImageForm = { tutorial_id: '' };
            vm.tutorialTextForm = { tutorial_id: '' };
        }//END resetForm();

    }

}());
