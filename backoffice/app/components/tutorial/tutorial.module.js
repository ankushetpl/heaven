(function () {

    angular.module('tutorialApp', [
        'ngYoutubeEmbed'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var tutorialPath = 'app/components/tutorial/';
        $stateProvider
            .state('backoffice.tutorial', {
                url: 'tutorial',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/index.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/form.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.createImage', {
                url: '/createImage',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/imageform.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.createText', {
                url: '/createText',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/textform.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/form.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.editImage', {
                url: '/editImage/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/imageform.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.editText', {
                url: '/editText/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/textform.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
            .state('backoffice.tutorial.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: tutorialPath + 'views/view.html',
                        controller: 'TutorialController',
                        controllerAs: 'tutorial'
                    }
                }
            })
    }

}());