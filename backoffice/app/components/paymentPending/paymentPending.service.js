(function () {
    "use strict";
    angular
        .module('paymentPendingApp')
        .service('PaymentPendingService', PaymentPendingService);

    PaymentPendingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function PaymentPendingService($http, APPCONFIG, $q) {
       
        function getPendingPayments(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'paymentPending';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = parseInt(obj.booking_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }

            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }

        /* to get single payment */
        function getSinglePending(booking_id) {
            var URL = APPCONFIG.APIURL + 'paymentPending/view';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSinglePending();

        /* to delete a payment from database */
        function deletePending(booking_id) {
            var URL = APPCONFIG.APIURL + 'paymentPending/delete';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        } //END deletePending();


        function getPendingPaymentsWeek(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'paymentPending/weekly';

            var requestData = { pageNum };
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.bookweekly_id !== 'undefined' && obj.bookweekly_id !== '') {
                    requestData['bookweekly_id'] = parseInt(obj.bookweekly_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }

            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }

        /* to get single payment */
        function getSinglePendingWeek(bookweekly_id) {
            var URL = APPCONFIG.APIURL + 'paymentPending/weeklyView';
            var requestData = {};

            if (typeof bookweekly_id !== undefined && bookweekly_id !== '') {
                requestData['bookweekly_id'] = bookweekly_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSinglePendingWeek();

        /* to delete a payment from database */
        function deletePendingWeek(bookweekly_id) {
            var URL = APPCONFIG.APIURL + 'paymentPending/weeklyDelete';
            var requestData = {};

            if (typeof bookweekly_id !== undefined && bookweekly_id !== '') {
                requestData['bookweekly_id'] = bookweekly_id;
            }

            return this.runHttp(URL, requestData);
        } //END deletePendingWeek();        

        /* to get all data booking for CSV */
        function getOncePendingPayDataCSV() {
            var URL = APPCONFIG.APIURL + 'paymentPending/pendingOnceCSV';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getOnceBookingDataCSV();        


        /* to get all data booking for CSV */
        function getWeekPendingPayDataCSV() {
            var URL = APPCONFIG.APIURL + 'paymentPending/pendingWeeklyCSV';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getWeekBookingDataCSV();            

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getPendingPayments: getPendingPayments,
            getSinglePending: getSinglePending,
            deletePending: deletePending,
            getPendingPaymentsWeek : getPendingPaymentsWeek,
            getSinglePendingWeek : getSinglePendingWeek,
            deletePendingWeek : deletePendingWeek,
            getOncePendingPayDataCSV : getOncePendingPayDataCSV,
            getWeekPendingPayDataCSV : getWeekPendingPayDataCSV
        }

    };
}());
