(function () {

    angular.module('paymentPendingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var paymentPendingPath = 'app/components/paymentPending/';
        $stateProvider
            .state('backoffice.paymentPending', {
                url: 'payPending',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentPendingPath + 'views/index.html',
                        controller: 'PaymentPendingController',
                        controllerAs: 'paymentPending'
                    }
                }
            })
            .state('backoffice.paymentPending.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentPendingPath + 'views/view.html',
                        controller: 'PaymentPendingController',
                        controllerAs: 'paymentPending'
                    }
                }
            })
            .state('backoffice.paymentPending.weekly', {
                url: '/weekly',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentPendingPath + 'views/weekly.html',
                        controller: 'PaymentPendingWeekController',
                        controllerAs: 'paymentPendingWeek'
                    }
                }
            })
            .state('backoffice.paymentPending.weeklyView', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: paymentPendingPath + 'views/weeklyView.html',
                        controller: 'PaymentPendingWeekController',
                        controllerAs: 'paymentPendingWeek'
                    }
                }
            });
    }

}());