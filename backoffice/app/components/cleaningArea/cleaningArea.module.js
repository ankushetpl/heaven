(function () {

    angular.module('cleaningAreaApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var cleaningAreaPath = 'app/components/cleaningArea/';
        $stateProvider
            .state('backoffice.cleaningArea', {
                url: 'cleaningArea',
                views: {
                    'content@backoffice': {
                        templateUrl: cleaningAreaPath + 'views/index.html',
                        controller: 'CleaningAreaController',
                        controllerAs: 'cleaningArea'
                    }
                }
            })
            .state('backoffice.cleaningArea.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: cleaningAreaPath + 'views/form.html',
                        controller: 'CleaningAreaController',
                        controllerAs: 'cleaningArea'
                    }
                }
            })
            .state('backoffice.cleaningArea.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: cleaningAreaPath + 'views/form.html',
                        controller: 'CleaningAreaController',
                        controllerAs: 'cleaningArea'
                    }
                }
            })
            .state('backoffice.cleaningArea.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: cleaningAreaPath + 'views/view.html',
                        controller: 'CleaningAreaController',
                        controllerAs: 'cleaningArea'
                    }
                }
            });
    }

}());