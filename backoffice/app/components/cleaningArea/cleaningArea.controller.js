(function () {
    "use strict";
    angular.module('cleaningAreaApp')
        .controller('CleaningAreaController', CleaningAreaController);

    CleaningAreaController.$inject = ['$scope', '$rootScope', '$state', '$location', 'CleaningAreaService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function CleaningAreaController($scope, $rootScope, $state, $location, CleaningAreaService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getCleaningArea = getCleaningArea;
        vm.getSingleCleaningArea = getSingleCleaningArea;
        vm.saveCleaningArea = saveCleaningArea;
        vm.deleteCleaningArea = deleteCleaningArea;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Additional Cleaning Area';
        vm.totalCleaningArea = 0;
        vm.cleaningAreaPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.cleaningAreaForm = { area_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];
        vm.durationList = [ {id : '1', hour : '1'}, {id : '2', hour : '2'}, {id : '3', hour : '3'}, {id : '4', hour : '4'}, {id : '5', hour : '5'}, {id : '6', hour : '6'}, {id : '7', hour : '7'}, {id : '8', hour : '8'} ];

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.cleaningArea');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleCleaningArea(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getCleaningArea(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getCleaningArea(newPage, searchInfo);
        }//END changePage();

        /* to save CleaningArea after add and edit  */
        function saveCleaningArea() {
            CleaningAreaService.saveCleaningArea(vm.cleaningAreaForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Cleaning Area');
                        $state.go('backoffice.cleaningArea');
                    } else {
                        toastr.error(response.data.message, 'Cleaning Area');
                    }
                } else {
                    toastr.error(response.data.message, 'Cleaning Area');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Cleaning Area');
            });
        }//END saveCleaningArea();

        /* to get CleaningArea list */
        function getCleaningArea(newPage, obj) {
            vm.progressbar.start();
            vm.totalCleaningArea = 0;
            vm.cleaningArea = [];
            vm.cleaningAreaCheck = false;
            CleaningAreaService.getCleaningArea(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.cleaningAreaCheck = true;
                    if (response.data.cleaningArea && response.data.cleaningArea.length > 0) {
                        vm.totalCleaningArea = response.data.total;
                        vm.cleaningArea = response.data.cleaningArea;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getCleaningArea();

        /* to get single CleaningArea */
        function getSingleCleaningArea(area_id) {
            $location.hash('top');
            $anchorScroll();
            CleaningAreaService.getSingleCleaningArea(area_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleCleaningArea = response.data.cleaningArea;
                        vm.cleaningAreaForm = response.data.cleaningArea;
                    } else {
                        toastr.error(response.data.message, 'Cleaning Area');
                        $state.go('backoffice.cleaningArea');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Cleaning Area');
            });
        }//END getSingleCleaningArea();

        /** to delete a CleaningArea **/
        function deleteCleaningArea(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this cleaning area?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CleaningAreaService.deleteCleaningArea(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.cleaningArea.splice(index, 1);
                            vm.totalCleaningArea = vm.totalCleaningArea - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Cleaning Area');
                    });
                }
            });
        }//END deleteCleaningArea();

        /* to change active/inactive status of Cleaning Area */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { area_id: status.area_id, status: statusId };
            CleaningAreaService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getCleaningArea(1, '');
        }//END reset();               
    }

}());
