(function () {
    "use strict";
    angular
        .module('cleaningAreaApp')
        .service('CleaningAreaService', CleaningAreaService);

    CleaningAreaService.$inject = ['$http', 'APPCONFIG', '$q'];

    function CleaningAreaService($http, APPCONFIG, $q) {

        /* save CleaningArea */
        function saveCleaningArea(obj) {
            var URL = APPCONFIG.APIURL + 'cleaningArea/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.area_id !== 'undefined' && obj.area_id !== '') {
                    requestData['area_id'] = obj.area_id;
                    URL = APPCONFIG.APIURL + 'cleaningArea/edit';
                }

                if (typeof obj.area_name !== 'undefined' && obj.area_name !== '') {
                    requestData['area_name'] = obj.area_name;
                }

                if (typeof obj.area_clean_duration !== 'undefined' && obj.area_clean_duration !== '') {
                    requestData['area_clean_duration'] = obj.area_clean_duration;
                }

                // if (typeof obj.area_clean_cost !== 'undefined' && obj.area_clean_cost !== '') {
                //     requestData['area_clean_cost'] = obj.area_clean_cost;
                // }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END save CleaningArea();

        /* to get all CleaningArea */
        function getCleaningArea(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'cleaningArea';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.area_id !== 'undefined' && obj.area_id !== '') {
                    requestData['area_id'] = parseInt(obj.area_id);
                }

                if (typeof obj.area_name !== 'undefined' && obj.area_name !== '') {
                    requestData['area_name'] = obj.area_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getCleaningArea();

        /* to get single CleaningArea */
        function getSingleCleaningArea(area_id) {
            var URL = APPCONFIG.APIURL + 'cleaningArea/view';
            var requestData = {};

            if (typeof area_id !== undefined && area_id !== '') {
                requestData['area_id'] = area_id;
            }

            return this.runHttp(URL, requestData);
        } //END getCleaningAreaById();

        /* to delete a CleaningArea from database */
        function deleteCleaningArea(area_id) {
            var URL = APPCONFIG.APIURL + 'cleaningArea/delete';
            var requestData = {};

            if (typeof area_id !== undefined && area_id !== '') {
                requestData['area_id'] = area_id;
            }
            return this.runHttp(URL, requestData);
        } //END deleteCleaningArea();

        /* to change active/inactive status of CleaningArea */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'cleaningArea/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.area_id != undefined && obj.area_id != "") {
                    requestData["area_id"] = obj.area_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            saveCleaningArea: saveCleaningArea,
            getCleaningArea: getCleaningArea,
            getSingleCleaningArea: getSingleCleaningArea,
            deleteCleaningArea: deleteCleaningArea,
            changeStatus: changeStatus
        }

    };//END CleaningAreaService()
}());
