(function () {
    "use strict";
    angular.module('allBookingApp')
        .controller('AllBookingController', AllBookingController);

    AllBookingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AllBookingService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function AllBookingController($scope, $rootScope, $state, $location, AllBookingService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getList = getList;
        vm.getSingle = getSingle;
        vm.getOnceCSV   = getOnceCSV;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'All Bookings';
        vm.total = 0;
        vm.PerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        vm.getList();
        if (path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.allBooking');
                return false;
            } else {
                vm.progressbar.start();
                vm.getSingle(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getList(newPage, searchInfo);
        }//END changePage();

        /* to get all list */
        function getList(newPage, obj) {
            vm.progressbar.start();
            vm.total = 0;
            vm.list = [];
            vm.listCheck = false;
            AllBookingService.getAllBooking(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.listCheck = true;
                    if (response.data.allBooking && response.data.allBooking.length > 0) {
                        vm.total = response.data.total;
                        vm.list = response.data.allBooking;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getList();

        /* to get single booking */
        function getSingle(booking_id) {
            $location.hash();
            $anchorScroll();

            AllBookingService.getSingleBooking(booking_id).then(function (response) {                
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.single = response.data.allBooking;
                    } else {
                        toastr.error(response.data.message, 'AllBooking');
                        $state.go('backoffice.allBooking');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'AllBooking');
            });
        }//END getSingle();

        /* to get all data in CSV format */
        function getOnceCSV() {
            vm.progressbar.start();
            AllBookingService.getAllBookingDataCSV().then(function (response) {
                if (response.status == 200) {
                    if (response.data.onceCSV && response.data.onceCSV.length > 0) {
                        vm.list = response.data.onceCSV;
                        // delete vm.list[0].booking_service_type;
                        var csv = Object.keys(vm.list[0]);
 						var headings = csv.toString();
                    }
                }

                var csv = headings;
                csv += "\n";
                angular.forEach(vm.list, function(value, key) {
                	var array = [];	
                	array.push([value.booking_id, value.booking_date, value.booking_time_from, value.booking_time_to, value.booking_apartment, value.booking_street, value.booking_city, value.tasktype_name, value.booking_bedrooms, value.booking_bathrooms, value.booking_baskets, value.booking_other_work, value.booking_start_time, value.booking_end_time, value.booking_cost, value.workers_name, value.workers_phone, value.workers_mobile, value.workers_address, value.clients_name, value.clients_phone, value.clients_mobile, value.clients_address]);
                	csv += array;
                	csv += "\n";
                });
               	var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'All OnceOff Bookings.csv';
                hiddenElement.click();
                vm.progressbar.complete();

            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSingle();        

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getList(1, '');
        }//END reset();               
    }

}());
