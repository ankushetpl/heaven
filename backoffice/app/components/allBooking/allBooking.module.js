(function () {

    angular.module('allBookingApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var allBookingPath = 'app/components/allBooking/';
        $stateProvider
            .state('backoffice.allBooking', {
                url: 'allBooking',
                views: {
                    'content@backoffice': {
                        templateUrl: allBookingPath + 'views/index.html',
                        controller: 'AllBookingController',
                        controllerAs: 'allBooking'
                    }
                }
            })
            .state('backoffice.allBooking.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: allBookingPath + 'views/view.html',
                        controller: 'AllBookingController',
                        controllerAs: 'allBooking'
                    }
                }
            })
            .state('backoffice.allBooking.weekly', {
                url: '/weekly',
                views: {
                    'content@backoffice': {
                        templateUrl: allBookingPath + 'views/weekly.html',
                        controller: 'WeeklyBookingController',
                        controllerAs: 'weeklyBooking'
                    }
                }
            })
            .state('backoffice.allBooking.weeklyView', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: allBookingPath + 'views/weeklyview.html',
                        controller: 'WeeklyBookingController',
                        controllerAs: 'weeklyBooking'
                    }
                }
            })

    }

}());