(function () {
    "use strict";
    angular
        .module('allBookingApp')
        .service('AllBookingService', AllBookingService);

    AllBookingService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AllBookingService($http, APPCONFIG, $q) {

        /* to get all  */
        function getAllBooking(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'allBooking';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.booking_id !== 'undefined' && obj.booking_id !== '') {
                    requestData['booking_id'] = parseInt(obj.booking_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }
                
                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }

                
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getAllBooking();

        /* to get single booking */
        function getSingleBooking(booking_id) {
            var URL = APPCONFIG.APIURL + 'allBooking/view';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleBooking();


        /* to get all data booking for CSV */
        function getAllBookingDataCSV() {
            var URL = APPCONFIG.APIURL + 'allBooking/onceCSV';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getAllBookingDataCSV();        


        /* to get all data booking for CSV */
        function getWeekBookingDataCSV() {
            var URL = APPCONFIG.APIURL + 'allBooking/weeklyCSV';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getAllBookingWeek();        

        /* to get all week booking */
        function getAllBookingWeek(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'allBooking/weekly';

            var requestData = {};
            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.bookweekly_id !== 'undefined' && obj.bookweekly_id !== '') {
                    requestData['bookweekly_id'] = parseInt(obj.bookweekly_id);
                }

                if (typeof obj.clients_name !== 'undefined' && obj.clients_name !== '') {
                    requestData['clients_name'] = obj.clients_name;
                }

                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }
                
                if (typeof obj.booking_date !== 'undefined' && obj.booking_date !== '') {
                    requestData['booking_date'] = moment(obj.booking_date).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            console.log(requestData);

            return this.runHttp(URL, requestData);
        }//END getAllBooking(); 


        /* to get single booking of week type */
        function getSingleBookingWeek(bookweekly_id) {
            var URL = APPCONFIG.APIURL + 'allBooking/weeklyview';
            var requestData = {};

            if (typeof bookweekly_id !== undefined && bookweekly_id !== '') {
                requestData['bookweekly_id'] = bookweekly_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleBookingWeek();               


        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAllBooking: getAllBooking,
            getSingleBooking: getSingleBooking,
            getAllBookingDataCSV : getAllBookingDataCSV,
            getAllBookingWeek : getAllBookingWeek,
            getSingleBookingWeek : getSingleBookingWeek,
            getWeekBookingDataCSV : getWeekBookingDataCSV
        }

    };//END AllBookingService()
}());
