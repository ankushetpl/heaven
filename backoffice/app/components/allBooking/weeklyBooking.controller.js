(function () {
    "use strict";
    angular.module('allBookingApp')
        .controller('WeeklyBookingController', WeeklyBookingController);

    WeeklyBookingController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AllBookingService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function WeeklyBookingController($scope, $rootScope, $state, $location, AllBookingService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getListWeek = getListWeek;
        vm.getSingleWeek = getSingleWeek;
        vm.getWeeklyCSV = getWeeklyCSV;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'All Bookings';
        vm.total = 0;
        vm.PerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        vm.getListWeek();
        if (path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.allBooking.weekly');
                return false;
            } else {
                vm.progressbar.start();
                vm.getSingleWeek(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getListWeek(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getListWeek(newPage, searchInfo);
        }//END changePage();

        /* to get all list */
        function getListWeek(newPage, obj) {
            vm.progressbar.start();
            vm.total = 0;
            vm.listWeek = [];
            vm.listWeekCheck = false;
            AllBookingService.getAllBookingWeek(newPage, obj, vm.orderInfo).then(function (response) {                 
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.listWeekCheck = true;
                    if (response.data.allBookingWeek && response.data.allBookingWeek.length > 0) {
                        vm.total = response.data.total;
                        vm.listWeek = response.data.allBookingWeek;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getList();

        /* to get single booking */
        function getSingleWeek(bookweekly_id) {   
            $location.hash('top');
            $anchorScroll();
         
            AllBookingService.getSingleBookingWeek(bookweekly_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleWeek = response.data.allBookingWeek;

                    } else {
                        toastr.error(response.data.message, 'WeeklyBooking');
                        $state.go('backoffice.allBooking.weekly');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'WeeklyBooking');
            });
        }//END getSingle();

        /* to get all data in CSV format */
        function getWeeklyCSV() {
            vm.progressbar.start();
            AllBookingService.getWeekBookingDataCSV().then(function (response) {

                if (response.status == 200) {
                    if (response.data.weeklyCSV && response.data.weeklyCSV.length > 0) {
                        vm.list = response.data.weeklyCSV;
                        // delete vm.list[0].booking_service_type;
                        var csv = Object.keys(vm.list[0]);
                        var headings = csv.toString();
                    }
                }

                var csv = headings;
                csv += "\n";
                angular.forEach(vm.list, function(value, key) {
                    var array = []; 
                    array.push([value.bookweekly_id, value.booking_date, value.booking_time_from, value.booking_time_to, value.booking_apartment, value.booking_street, value.booking_city, value.tasktype_name, value.booking_bedrooms, value.booking_bathrooms, value.booking_baskets, value.booking_other_work, value.booking_start_time, value.booking_end_time, value.booking_cost, value.workers_name, value.workers_phone, value.workers_mobile, value.workers_address, value.clients_name, value.clients_phone, value.clients_mobile, value.clients_address]);
                    csv += array;
                    csv += "\n";
                });
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'All Weekly Bookings.csv';
                hiddenElement.click();
                vm.progressbar.complete();

            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getSingle();           

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getListWeek(1, '');
        }//END reset();               
    }

}());
