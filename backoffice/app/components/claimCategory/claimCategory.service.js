(function () {
    "use strict";
    angular
        .module('claimCategoryApp')
        .service('ClaimCategoryService', ClaimCategoryService);

    ClaimCategoryService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ClaimCategoryService($http, APPCONFIG, $q) {

        /* save claim category */
        function saveClaimCategory(obj) {
            var URL = APPCONFIG.APIURL + 'claimCategory/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.category_id !== 'undefined' && obj.category_id !== '') {
                    requestData['category_id'] = obj.category_id;
                    URL = APPCONFIG.APIURL + 'claimCategory/edit';
                }

                if (typeof obj.category_name !== 'undefined' && obj.category_name !== '') {
                    requestData['category_name'] = obj.category_name;
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }                
            }
            
            return this.runHttp(URL, requestData);
        } //END saveClaimCategory();

        /* to get all claim category */
        function getClaimCategory(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'claimCategory';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.category_id !== 'undefined' && obj.category_id !== '') {
                    requestData['category_id'] = parseInt(obj.category_id);
                }

                if (typeof obj.category_name !== 'undefined' && obj.category_name !== '') {
                    requestData['category_name'] = obj.category_name;
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getClaimCategory();

        /* to get single claim category */
        function getSingleClaimCategory(category_id) {
            var URL = APPCONFIG.APIURL + 'claimCategory/view';
            var requestData = {};

            if (typeof category_id !== undefined && category_id !== '') {
                requestData['category_id'] = category_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleClaimCategory();

        /* to delete a claim category from database */
        function deleteClaimCategory(category_id) {
            var URL = APPCONFIG.APIURL + 'claimCategory/delete';
            var requestData = {};

            if (typeof category_id !== undefined && category_id !== '') {
                requestData['category_id'] = category_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteClaimCategory();

        /* to change active/inactive status of claim category */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'claimCategory/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.category_id != undefined && obj.category_id != "") {
                    requestData["category_id"] = obj.category_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getClaimCategory: getClaimCategory,
            getSingleClaimCategory: getSingleClaimCategory,
            saveClaimCategory: saveClaimCategory,
            deleteClaimCategory: deleteClaimCategory,
            changeStatus: changeStatus
        }

    };//END ClaimCategoryService()
}());
