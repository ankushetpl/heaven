(function () {
    "use strict";
    angular.module('claimCategoryApp')
        .controller('ClaimCategoryController', ClaimCategoryController);

    ClaimCategoryController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ClaimCategoryService', 'toastr', 'SweetAlert', 'ngProgressFactory', '$anchorScroll'];

    function ClaimCategoryController($scope, $rootScope, $state, $location, ClaimCategoryService, toastr, SweetAlert, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getClaimCategoryList = getClaimCategoryList;
        vm.getSingleClaimCategory = getSingleClaimCategory;
        vm.saveClaimCategory = saveClaimCategory;
        vm.deleteClaimCategory = deleteClaimCategory;
        vm.changeStatus = changeStatus;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Claim Category';
        vm.totalClaimCategory = 0;
        vm.claimCategoryPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.editFlag = false;
        vm.title = 'Add New';
        vm.claimCategoryForm = { category_id: '' };
        vm.statusList = [{ id : 0, status: 'Not Approved'}, { id : 1, status: 'Approved'}];

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.claimCategory');
                return false;
            } else {
                vm.progressbar.start();
                vm.editFlag = true;
                vm.title = 'Edit';
                vm.getSingleClaimCategory(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getClaimCategoryList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getClaimCategoryList(newPage, searchInfo);
        }//END changePage();

        /* to save claim category after add and edit  */
        function saveClaimCategory() {
            ClaimCategoryService.saveClaimCategory(vm.claimCategoryForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Claim Category');
                        $state.go('backoffice.claimCategory');
                    } else {
                        toastr.error(response.data.message, 'Claim Category');
                    }
                } else {
                    toastr.error(response.data.message, 'Claim Category');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Claim Category');
            });
        }//END saveClaimCategory();

        /* to get claim category list */
        function getClaimCategoryList(newPage, obj) {
            vm.progressbar.start();
            vm.totalClaimCategory = 0;
            vm.claimCategoryList = [];
            vm.claimCategoryListCheck = false;
            ClaimCategoryService.getClaimCategory(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.claimCategoryListCheck = true;
                    if (response.data.claimCategory && response.data.claimCategory.length > 0) {
                        vm.totalClaimCategory = response.data.total;
                        vm.claimCategoryList = response.data.claimCategory;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getClaimCategoryList();

        /* to get single claim category */
        function getSingleClaimCategory(category_id) {
            $location.hash('top');
            $anchorScroll();
            ClaimCategoryService.getSingleClaimCategory(category_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.singleClaimCategory = response.data.claimCategory;
                        vm.claimCategoryForm = response.data.claimCategory;
                    } else {
                        toastr.error(response.data.message, 'Claim Category');
                        $state.go('backoffice.claimCategory');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Claim Category');
            });
        }//END getSingleClaimCategory();

        /** to delete a claim category **/
        function deleteClaimCategory(id, index) {
            SweetAlert.swal({
                title: "Are you sure you want to delete this Claim Category?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ClaimCategoryService.deleteClaimCategory(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.claimCategoryList.splice(index, 1);
                            vm.totalClaimCategory = vm.totalClaimCategory - 1; 
                        } else {
                            SweetAlert.swal("Error!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Claim Category');
                    });
                }
            });
        }//END deleteClaimCategory();

        /* to change active/inactive status of claim category */
        function changeStatus(status) {
            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { category_id: status.category_id, status: statusId };
            ClaimCategoryService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }else{
                        SweetAlert.swal("Error!", response.data.message, "error");
                        return false;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getClaimCategoryList(1, '');
        }//END reset();               
    }

}());
