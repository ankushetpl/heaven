(function () {

    angular.module('claimCategoryApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var claimCategoryPath = 'app/components/claimCategory/';
        $stateProvider
            .state('backoffice.claimCategory', {
                url: 'claimCategory',
                views: {
                    'content@backoffice': {
                        templateUrl: claimCategoryPath + 'views/index.html',
                        controller: 'ClaimCategoryController',
                        controllerAs: 'claimCategory'
                    }
                }
            })
            .state('backoffice.claimCategory.create', {
                url: '/create',
                views: {
                    'content@backoffice': {
                        templateUrl: claimCategoryPath + 'views/form.html',
                        controller: 'ClaimCategoryController',
                        controllerAs: 'claimCategory'
                    }
                }
            })
            .state('backoffice.claimCategory.edit', {
                url: '/edit/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: claimCategoryPath + 'views/form.html',
                        controller: 'ClaimCategoryController',
                        controllerAs: 'claimCategory'
                    }
                }
            })
            .state('backoffice.claimCategory.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: claimCategoryPath + 'views/view.html',
                        controller: 'ClaimCategoryController',
                        controllerAs: 'claimCategory'
                    }
                }
            });
    }

}());