(function () {

    angular.module('claimApp', [
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var claimPath = 'app/components/claim/';
        $stateProvider
            .state('backoffice.claim', {
                url: 'claim',
                views: {
                    'content@backoffice': {
                        templateUrl: claimPath + 'views/index.html',
                        controller: 'ClaimController',
                        controllerAs: 'claim'
                    }
                }
            })
            .state('backoffice.claim.view', {
                url: '/view/:id',
                views: {
                    'content@backoffice': {
                        templateUrl: claimPath + 'views/view.html',
                        controller: 'ClaimController',
                        controllerAs: 'claim'
                    }
                }
            });
    }

}());