(function () {
    "use strict";
    angular
        .module('claimApp')
        .service('ClaimService', ClaimService);

    ClaimService.$inject = ['$http', 'APPCONFIG', '$q'];

    function ClaimService($http, APPCONFIG, $q) {

        /* to get all claim */
        function getClaim(pageNum, obj, orderInfo) {
            var URL = APPCONFIG.APIURL + 'claim';

            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.claim_id !== 'undefined' && obj.claim_id !== '') {
                    requestData['claim_id'] = parseInt(obj.claim_id);
                }

                if (typeof obj.claim_category_id !== 'undefined' && obj.claim_category_id !== '') {
                    requestData['claim_category_id'] = obj.claim_category_id;
                }

                if (typeof obj.claim_booking_id !== 'undefined' && obj.claim_booking_id !== '') {
                    requestData['claim_booking_id'] = parseInt(obj.claim_booking_id);
                }

                if (typeof obj.created_at !== 'undefined' && obj.created_at !== '') {
                    requestData['created_at'] = moment(obj.created_at).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.action_ats !== 'undefined' && obj.action_ats !== '') {
                    requestData['action_ats'] = parseInt(obj.action_ats);
                }

            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        }//END getClaim();

        /* to get single claim */
        function getSingleClaim(claim_id) {
            var URL = APPCONFIG.APIURL + 'claim/view';
            var requestData = {};

            if (typeof claim_id !== undefined && claim_id !== '') {
                requestData['claim_id'] = claim_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleClaim();

        /* to delete a claim from database */
        function deleteClaim(claim_id) {
            var URL = APPCONFIG.APIURL + 'claim/delete';
            var requestData = {};

            if (typeof claim_id !== undefined && claim_id !== '') {
                requestData['claim_id'] = claim_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteClaim();

        /* to change active/inactive status of claim */
        function changeStatus(obj) {
            var URL = APPCONFIG.APIURL + 'claim/status';
            
            var requestData = {};
            if (obj != undefined) {
                
                if (obj.claim_id != undefined && obj.claim_id != "") {
                    requestData["claim_id"] = obj.claim_id;
                }

                if (obj.status != undefined || obj.status != "") {
                    requestData["status"] = obj.status;
                }                
            }  
            
            return this.runHttp(URL, requestData);            
        }//END changeStatus()

        /* to get all claim category */
        function getAllClaimCategory() {
            var URL = APPCONFIG.APIURL + 'claim/allCategory';

            var requestData = {}; 
            return this.runHttp(URL, requestData);
        }//END getAllClaimCategory();

        /* get booking data */
        function getBooking(booking_id){
            var URL = APPCONFIG.APIURL + 'allBooking/view';
            var requestData = {};

            if (typeof booking_id !== undefined && booking_id !== '') {
                requestData['booking_id'] = booking_id;
            }

            return this.runHttp(URL, requestData);
        }//END getBooking();

        /* resolved claim */
        function resolvedClaim(obj) {
            var URL = APPCONFIG.APIURL + 'claim/edit';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.claim_id !== 'undefined' && obj.claim_id !== '') {
                    requestData['claim_id'] = obj.claim_id;
                }

                if (typeof obj.refund_amount !== 'undefined' && obj.refund_amount !== '') {
                    requestData['refund_amount'] = obj.refund_amount;
                }                
            }
            
            return this.runHttp(URL, requestData);
        } //END resolvedClaim();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getClaim: getClaim,
            getSingleClaim: getSingleClaim,
            getImage: getImage,
            deleteClaim: deleteClaim,
            changeStatus: changeStatus,
            getAllClaimCategory: getAllClaimCategory,
            getBooking: getBooking,
            resolvedClaim: resolvedClaim
        }

    };//END ClaimService()
}());
