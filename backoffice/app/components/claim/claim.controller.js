(function () {
    "use strict";
    angular.module('claimApp')
        .controller('ClaimController', ClaimController);

    ClaimController.$inject = ['$scope', '$rootScope', '$state', '$location', 'ClaimService', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function ClaimController($scope, $rootScope, $state, $location, ClaimService, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.getClaimList = getClaimList;
        vm.getSingleClaim = getSingleClaim;
        vm.getImage = getImage;
        vm.deleteClaim = deleteClaim;
        vm.changeStatus = changeStatus;
        vm.resolvedForms = resolvedForms;
        vm.resolvedClaim = resolvedClaim;
        vm.close = close;
        vm.getAllClaimCategory = getAllClaimCategory;
        vm.formatDate = formatDate;
        vm.changePage = changePage;
        vm.sort = sort;
        vm.reset = reset;
        vm.isDisabled = isDisabled;

        vm.progressbar = ngProgressFactory.createInstance();

        $rootScope.headerTitle = 'Claims';
        vm.totalClaim = 0;
        vm.claimPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        
        vm.getAllClaimCategory();

        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('backoffice.claim');
                return false;
            } else {
                // vm.editFlag = true;
                // vm.title = 'Edit';
                vm.progressbar.start();
                $timeout( function(){
                    vm.getSingleClaim(path[3]);
                }, 500 );
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }

        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getClaimList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getClaimList(newPage, searchInfo);
        }//END changePage();

        /* to get claim list */
        function getClaimList(newPage, obj) {
            vm.progressbar.start();
            vm.claimList = [];
            vm.claimListCheck = false;
            ClaimService.getClaim(newPage, obj, vm.orderInfo).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    vm.claimListCheck = true;
                    if (response.data.claim && response.data.claim.length > 0) {
                        vm.totalClaim = response.data.total;
                        vm.claimList = response.data.claim;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getClaimList();

        /* get images */
        function getImage(id, num){
            ClaimService.getImage(id).then(function (responses) {
                if (responses.status == 200) {
                    if (responses.data.file && responses.data.file != '') {
                        if(num == 1){
                            vm.singleClaim.file1 = responses.data.file.file_base_url;
                        }
                        if(num == 2){
                            vm.singleClaim.file2 = responses.data.file.file_base_url;
                        }
                        if(num == 3){
                            vm.singleClaim.file3 = responses.data.file.file_base_url;
                        }
                        if(num == 4){
                            vm.singleClaim.file4 = responses.data.file.file_base_url;
                        }
                        if(num == 5){
                            vm.singleClaim.file5 = responses.data.file.file_base_url;
                        }
                        
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getImage();

        /* to get single claim */
        function getSingleClaim(claim_id) {
            $location.hash('top');
            $anchorScroll();
            ClaimService.getSingleClaim(claim_id).then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleClaim = response.data.claim;
                        
                        if(vm.singleClaim.claim_image1 != 0){
                            vm.singleClaim.file1 = '';
                            vm.getImage(vm.singleClaim.claim_image1, 1);
                        }

                        if(vm.singleClaim.claim_image2 != 0){
                            vm.singleClaim.file2 = '';
                            vm.getImage(vm.singleClaim.claim_image2, 2);
                        }

                        if(vm.singleClaim.claim_image3 != 0){
                            vm.singleClaim.file3 = '';
                            vm.getImage(vm.singleClaim.claim_image3, 3);
                        }

                        if(vm.singleClaim.claim_image4 != 0){
                            vm.singleClaim.file4 = '';
                            vm.getImage(vm.singleClaim.claim_image4, 4);
                        }

                        if(vm.singleClaim.claim_image5 != 0){
                            vm.singleClaim.file5 = '';
                            vm.getImage(vm.singleClaim.claim_image5, 5);
                        }
                        
                        var bookingID = response.data.claim.claim_booking_id;
                        ClaimService.getBooking(bookingID).then(function(response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.singlesClaim = response.data.allBooking;                                    
                                }
                            }
                        });

                    } else {
                        toastr.error(response.data.message, 'Claim');
                        $state.go('backoffice.claim');
                    }
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Claim');
            });
        }//END getSingleClaim();

        /** to delete a claim **/
        function deleteClaim(id, index, item) {
            
            if(item.action_ats == 0){
                SweetAlert.swal("Error!", "Sorry, You can't delete this claim!", "error");
                return false;
            }

            if(item.status == 0){
                SweetAlert.swal("Error!", "Sorry, You can't delete this claim. Please change status first!", "error");
                return false;
            }

            SweetAlert.swal({
                title: "Are you sure you want to delete this Claim?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    ClaimService.deleteClaim(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.claimList.splice(index, 1);
                            vm.totalClaim = vm.totalClaim - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Claim');
                    });
                }
            });
        }//END deleteClaim();

        /* to change active/inactive status of claim */
        function changeStatus(status) {

            if(status.action_ats == 1){
                SweetAlert.swal("Error!", "Sorry, You can't change status!", "error");
                return false;
            }

            if (status.status == 1) {
                var statusId = 0;
            } else {
                var statusId = 1;
            }
            var data = { claim_id: status.claim_id, status: statusId };
            ClaimService.changeStatus(data).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.reload();
                        return true;
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeStatus()

        vm.resolvedForm = { claim_id: '' };

        /* to show resolved form for claim */ 
        function resolvedForms(items){

            if(items.status == 0){
                SweetAlert.swal("Resolved!", "Please approve this claim first!", "error");
                $state.reload();
                return false;       
            }

            if(items.action_ats == 1){
                SweetAlert.swal("Resolved!", "You already resolved this claim!", "error");
                $state.reload();
                return false;       
            }else{
                vm.resolvedForm.claim_id = items.claim_id; 

                ClaimService.getBooking(items.claim_booking_id).then(function(response) {
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            vm.resolvedForm.booking_cost = response.data.allBooking.booking_cost;
                        }
                    }
                });

                $timeout( function(){
                    var logElem = angular.element("#responsiveResolved");
                    logElem.modal("show");                
                }, 300 );
                
            }            
        }//END resolvedForms()

        /* to resolved claim */ 
        function resolvedClaim() {
            
            var logElem = angular.element(".modal-backdrop");
            logElem.removeClass("show"); 
            logElem.addClass("hide"); 

            var logElem = angular.element("body");
            logElem.removeClass("modal-open"); 
            
            delete vm.resolvedForm.booking_cost;

            ClaimService.resolvedClaim(vm.resolvedForm).then(function(response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {                        
                        SweetAlert.swal("Resolved!", response.data.message, "success");
                        $state.reload();
                        return true;                        
                    }else {
                        SweetAlert.swal("Resolve!", response.data.message, "error");
                    }
                } else {
                    toastr.error(response.data.error, 'Error');
                    return false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
            });            
        }//END resolvedClaim()

        /* to close model pupup */
        function close(){
            vm.getClaimList(1, '');   
            $state.go('backoffice.claim');                              
        }//END close();

        /* all claim category  */
        function getAllClaimCategory() {
            ClaimService.getAllClaimCategory().then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1 && response.data.category.length > 0) {
                        vm.claimCategoryList = response.data.category;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllClaimCategory()

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getClaimList(1, '');
        }//END reset();               
    }

}());
