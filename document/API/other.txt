1. FAQs All
http://192.168.100.44:3000/faq/allFAQs
Parameters:-    
Method    : Post
Parameter : role_id	
role_id : Client : 5
    	  Worker : 3	   		
Key - Content-Type : application/x-www-form-urlencoded

2. FAQs Add
http://192.168.100.44:3000/faq/add
Parameters:-    
Method    : Post
Parameter : faq_question
	    faq_answer
            role_id
	    user_id
	    status	
role_id : Client : 5
    	  Worker : 3
status : 0 	   	
faq_answer : ''	
Key - Content-Type : application/x-www-form-urlencoded

3. Feedback Add
http://18.221.244.173:9000/feedback/add
Parameters:-    
Method    : Post
Parameter : note
	    user_id
            role_id	    
Key - Content-Type : application/x-www-form-urlencoded

4. Feedback All List
http://18.221.244.173:9000/feedback/allfeedback
Parameters:-    
Method    : Post
Parameter : user_id	    
Key - Content-Type : application/x-www-form-urlencoded

5. Tutorial
http://18.221.244.173:9000/tutorial/alltutorial
Parameters:-    
Method    : Post
Parameter : role_id	
role_id : Client : 5
    	  Worker : 3	    
Key - Content-Type : application/x-www-form-urlencoded

6. Weekly Service
http://18.221.244.173:9000/weekService/allWeekService
Parameters:-    
Method    : Post
Key - Content-Type : application/x-www-form-urlencoded

7. Social Link
http://18.221.244.173:9000/social/allSocial
Parameters:-    
Method    : Post
Key - Content-Type : application/x-www-form-urlencoded

8. Page Data
http://18.221.244.173:9000/page/view
Parameters:- page_id   
Method    : Post
Key - Content-Type : application/x-www-form-urlencoded
page_id : 1 [About]
page_id : 2 [Contact]
page_id : 3 [Terms & Condition]
page_id : 4 [Privacy Policy]
page_id : 5 [Code Of Conduct]
page_id : 6 [Payment Agreement]
page_id : 7 [Request Early Payment]
page_id : 8 [ Payments & Bonus]

9. Mail List
http://18.221.244.173:9000/mail/allMail
Parameters:-    
Method    : Post
Key - Content-Type : application/x-www-form-urlencoded




