APIs For Worker

Worker Notification 
1) Rating - ratings given by client
2) Substitute - another worker Substitute you
3) Substitute Accept - other worker accept your Substitute request
4) Substitute Reject - other worker reject your Substitute request
5) New Job           - New Job Assignment
6) Job Cancel        - Job Cancelled by client

1) Worker profile

http://18.221.244.173:9000/worker/view
Method - Post
Parameter - worker_id
Key - Content-Type : application/x-www-form-urlencoded

2) Todays Job & Upcoming Jobs 

http://18.221.244.173:9000/worker/jobs
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id
	    flag  = 0 (todays job)
	    flag  = 1 (upcoming job) 

3) Myjobs

http://18.221.244.173:9000/worker/myjobs
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id


4) History 

http://18.221.244.173:9000/worker/history
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id
	    flag = 0 (completed jobs)
	    flag = 1 (upcoming jobs)
	    flag = 2 (sustitute jobs)


5) Send Sustitute Job 

http://18.221.244.173:9000/worker/substituteSend
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - booking_id
	    sender_id
	    receiver_id

			
6) Get Substitute Jobs	

http://18.221.244.173:9000/worker/substituteReq
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id

7) Workers List
http://18.221.244.173:9000/worker/workersList
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id

8) Update Profile 
http://18.221.244.173:9000/worker/update
Method - Post
Parameter - worker_id
		name
		phone
		address
		file

9) Leave Reason list added by admin 
http://18.221.244.173:9000/worker/leaveReasonList
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id

10) Apply for leave
http://18.221.244.173:9000/worker/leaveApply
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id
	    leave_from
	    leave_to
	    reason_id
	    leave_note
	    leave_terms

11) Worker leaves
http://18.221.244.173:9000/worker/workerLeaves
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id

12) Worker Calender
http://18.221.244.173:9000/worker/calender
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id

13)Start and End job
http://18.221.244.173:9000/worker/jobTime
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    time    
	    booking_id
  	    flag = 0 (start job)
	    flag = 1 (end job)

14) Job Details
http://18.221.244.173:9000/worker/jobDetails
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    booking_id

15) cabRequest
http://18.221.244.173:9000/worker/cabRequest
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    booking_id
	    pickup_location
	    pickup_address
	    pick_lat
	    pick_long

16) mySuburb
http://18.221.244.173:9000/worker/mySuburb
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    booking_id	

17) updateSuburb
http://18.221.244.173:9000/worker/updateSuburb
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    workers_suburb1
	    workers_suburb2
	    workers_suburb3
	    workers_suburb4

18) Delete leave
http://18.221.244.173:9000/worker/leaveDelete
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    leave_id

19) City list
http://18.221.244.173:9000/worker/allCities
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id

20) Rating list
http://18.221.244.173:9000/worker/ratingList
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
			

21) Rate Client
http://18.221.244.173:9000/worker/rateClient
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    booking_id
	    rating_id
	    ratings

22) Accept or Reject Substitute Request
http://18.221.244.173:9000/worker/substituteAcceptReject
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    substitute
	    flag   [0 = Accept, 1 = Reject]

23) Completed jobs with and without rating
http://18.221.244.173:9000/worker/completeRateJobs
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	flag   [0 = completed jobs without rating , 1 = completed jobs with rating]

24) Completed jobs with and without rating
http://18.221.244.173:9000/worker/completeRateJobs
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    flag   [0 = completed jobs without rating , 1 = completed jobs with rating]	


25) Notification list
http://18.221.244.173:9000/worker/notList
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id


26) Read Notification
http://18.221.244.173:9000/worker/readNot
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id	
	    notification_id

27) Delete OR clear Notification data For client & worker
http://18.221.244.173:9000/worker/deleteNot
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - user_id	
	    notification
	    flag


28) Update device token for client and worker
http://18.221.244.173:9000/client/updateDevice 
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - client_id
	    device_token
	    device_type


29) Rate Worker Weekly jobs
http://18.221.244.173:9000/client/rateWorkerWeek
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - client_id	
	booking_id
	rating_id
	ratings	
	jobCount

30) forget password
http://18.219.129.228:9000/worker/forgetPass
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - user_id	

31) check for OTP
http://18.219.129.228:9000/worker/checkotp
Method - Post
Key - Content-Type : application/x-www-form-urlencoded
Parameter - worker_id
			otp

