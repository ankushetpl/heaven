var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify');

gulp.task('backoffice_vendor_css', function () {
    var backoffice_css_files = [
        './assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css',
        './assets/admin/css/colors/red.css',
        './vendor/angular-toastr/dist/angular-toastr.css',
        './vendor/angucomplete-alt/angucomplete-alt.css',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.css',
        './vendor/angular-material/angular-material.css',
        './vendor/oi.select/dist/select.min.css',
        './assets/common/css/sweetalert.css',
        './assets/admin/css/custom.css',
        './assets/common/css/custom.css'
    ]

    return gulp.src(backoffice_css_files)
        .pipe(gp_concat('backoffice_vendor.css'))
        .pipe(gulp.dest('backoffice/dist'));
});

gulp.task('backoffice_vendor_js', function () {
    var backoffice_js_files = [
        './vendor/jquery/dist/jquery.min.js',
        './assets/admin/assets/plugins/bootstrap/js/popper.min.js',
        './assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js',
        './assets/admin/js/jquery.slimscroll.js',
        './assets/admin/js/waves.js',
        './vendor/angular/angular.min.js',
        './vendor/angular-ui-router/release/angular-ui-router.min.js',
        './vendor/angular-sanitize/angular-sanitize.min.js',
        './vendor/angular-toastr/dist/angular-toastr.tpls.js',
        './vendor/tg-angular-validator/dist/angular-validator.min.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/ngSweetAlert/SweetAlert.min.js',
        './assets/common/js/sweetalert.min.js',
        './vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/ng-file-upload/ng-file-upload.min.js',
        './vendor/moment/min/moment-with-locales.js',
        './vendor/lodash/lodash.js',
        './vendor/angucomplete-alt/dist/angucomplete-alt.min.js',
        './vendor/angular-aria/angular-aria.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
        './vendor/angular-material/angular-material.js',
        './vendor/oi.select/dist/select-tpls.min.js',
        './vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        './assets/common/js/custom.js',
        './vendor/ngmap/build/scripts/ng-map.min.js'
    ]

    return gulp.src(backoffice_js_files)
        .pipe(gp_concat('backoffice_vendor.js'))
        .pipe(gulp.dest('backoffice/dist'));
});


var backOfficePath = './backoffice/app/components/'
gulp.task('backoffice_app_js', function () {
    var backoffice_app_files = [

        backOfficePath + 'auth/auth.module.js',
        backOfficePath + 'auth/auth.service.js',
        backOfficePath + 'auth/auth.controller.js',

        backOfficePath + 'dashboard/dashboard.controller.js',
        backOfficePath + 'dashboard/dashboard.service.js',

        backOfficePath + 'rating/rating.module.js',
        backOfficePath + 'rating/rating.service.js',
        backOfficePath + 'rating/rating.controller.js',

        backOfficePath + 'role/role.module.js',
        backOfficePath + 'role/role.service.js',
        backOfficePath + 'role/role.controller.js',

        backOfficePath + 'faq/faq.module.js',
        backOfficePath + 'faq/faq.service.js',
        backOfficePath + 'faq/faq.controller.js',

        backOfficePath + 'fileUpload/fileUpload.module.js',        
        backOfficePath + 'fileUpload/fileUpload.controller.js',

        backOfficePath + 'leave/leave.module.js',
        backOfficePath + 'leave/leave.service.js',
        backOfficePath + 'leave/leave.controller.js',

        backOfficePath + 'tutorial/tutorial.module.js',
        backOfficePath + 'tutorial/tutorial.service.js',
        backOfficePath + 'tutorial/tutorial.controller.js',

        backOfficePath + 'checklist/checklist.module.js',
        backOfficePath + 'checklist/checklist.service.js',
        backOfficePath + 'checklist/checklist.controller.js', 
        
        backOfficePath + 'skills/skills.module.js',
        backOfficePath + 'skills/skills.service.js',
        backOfficePath + 'skills/skills.controller.js',
        
        backOfficePath + 'suburbs/suburbs.module.js',
        backOfficePath + 'suburbs/suburbs.service.js',
        backOfficePath + 'suburbs/suburbs.controller.js',

        backOfficePath + 'editors/editors.module.js',
        backOfficePath + 'editors/editors.service.js',
        backOfficePath + 'editors/editors.controller.js',

        backOfficePath + 'cafeUser/cafeUser.module.js',
        backOfficePath + 'cafeUser/cafeUser.service.js',
        backOfficePath + 'cafeUser/cafeUser.controller.js',

        backOfficePath + 'workerUser/workerUser.module.js',
        backOfficePath + 'workerUser/workerUser.service.js',
        backOfficePath + 'workerUser/workerUser.controller.js',

        backOfficePath + 'assessorsUser/assessorsUser.module.js',
        backOfficePath + 'assessorsUser/assessorsUser.service.js',
        backOfficePath + 'assessorsUser/assessorsUser.controller.js',

        backOfficePath + 'clientUser/clientUser.module.js',
        backOfficePath + 'clientUser/clientUser.service.js',
        backOfficePath + 'clientUser/clientUser.controller.js',

        backOfficePath + 'adminUser/adminUser.module.js',
        backOfficePath + 'adminUser/adminUser.service.js',
        backOfficePath + 'adminUser/adminUser.controller.js',

        backOfficePath + 'backoffice.module.js',
        backOfficePath + 'backoffice.controller.js',
        'backoffice/app.js'
    ]

    return gulp.src(backoffice_app_files)
        .pipe(gp_concat('backoffice_app.js'))
        .pipe(gulp.dest('backoffice/dist'));
});


//front gulp files start here

// gulp.task('front_vendor_css', function () {
//     var front_css_files = [
//         'vendor/angular-toastr/dist/angular-toastr.css',
//         'vendor/angular-loading-bar/build/loading-bar.min.css',
//         'vendor/angular-moment-picker/dist/angular-moment-picker.min',
//         'vendor/adm-dtp/dist/ADM-dateTimePicker.css',
//         'assets/front/css/after-before.css',
//         'assets/front/css/animate.css',
//         'assets/front/css/bootstrap.css',
//         'assets/front/css/owl.carousel.css',
//         'assets/front/css/style.css',
//         'assets/front/css/responsive.css',
//         'assets/front/css/font-awesome.min.css',
//         'assets/front/css/client_responsive.css',
//         //'assets/front/css/auth-user-style.css',
//         //'assets/front/css/client_responsive.css',
//         './vendor/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css',
//         './vendor/angular-bootstrap-colorpicker/css/colorpicker.min.css',
//         'assets/common/sweetalert.css',
//         'assets/front/css/custom.css'
//     ]

//     return gulp.src(front_css_files)
//         .pipe(gp_concat('front_vendor_css.css'))
//         .pipe(gulp.dest('dist'));
// });

// gulp.task('front_vendor_js', function () {
//     var front_js_files = [
//         './assets/front/js/jquery-2.2.1.js',
//         './assets/backoffice/js/bootstrap.min.js',
//         './vendor/angular/angular.min.js',
//         './vendor/angular-ui-router/release/angular-ui-router.min.js',
//         './vendor/angular-cookies/angular-cookies.min.js',
//         './vendor/angular-aria/angular-aria.js',
//         './vendor/angular-sanitize/angular-sanitize.min.js',
//         './vendor/angular-toastr/dist/angular-toastr.tpls.js',
//         './vendor/moment/min/moment-with-locales.js',
//         './vendor/tg-angular-validator/dist/angular-validator.min.js',
//         './vendor/angular-animate/angular-animate.min.js',
//         './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
//         './vendor/ngSweetAlert/SweetAlert.min.js',
//         './assets/common/sweetalert.min.js',
//         './vendor/lodash/lodash.js',
//         './vendor/angular-material/angular-material.js',
//         './vendor/adm-dtp/dist/ADM-dateTimePicker.min.js',
//         './vendor/angular-breadcrumb/dist/angular-breadcrumb.js',
//         './assets/common/timepickerpop.js',
//         './assets/front/js/ui-bootstraps-tpls.min.js',
//         './vendor/ng-file-upload/ng-file-upload.min.js',
//         './assets/front/js/after-before.js',
//         './assets/front/js/wow.min.js',
//         './vendor/vsGoogleAutocomplete/dist/vs-google-autocomplete.js',
//         './vendor/angularUtils-pagination/dirPagination.js',
//         './assets/front/js/custom.js',
//         './vendor/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js',
//         './vendor/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js',
//         './assets/common/custom.js'
//     ]

//     return gulp.src(front_js_files)
//         .pipe(gp_concat('front_vendor_js.js'))
//         .pipe(gulp.dest('dist'));
// });


// var frontPath = 'app/components/'
// gulp.task('front_app_js', function () {
//     var front_app_files = [

//         frontPath + 'auth/auth.module.js',
//         frontPath + 'auth/auth.service.js',
//         frontPath + 'auth/auth.controller.js',

//         frontPath + 'shared/shared.module.js',
//         frontPath + 'shared/shared.service.js',

//         frontPath + 'staticpages/staticpages.module.js',
//         frontPath + 'staticpages/staticpages.service.js',
//         frontPath + 'staticpages/staticpages.controller.js',

//         frontPath + 'home/home.module.js',
//         frontPath + 'home/home.controller.js',
//         frontPath + 'home/home.service.js',

//         frontPath + 'dashboard/dashboard.module.js',
//         frontPath + 'dashboard/dashboard.service.js',
//         frontPath + 'dashboard/dashboard.controller.js',

//         // frontPath + 'orders/order.module.js',
//         // frontPath + 'orders/order.controller.js',
//         // frontPath + 'orders/order.service.js',

//         frontPath + 'placeOrder/placeOrder.module.js',
//         frontPath + 'placeOrder/placeOrder.controller.js',
//         frontPath + 'placeOrder/placeOrder.service.js',

//         frontPath + 'calendar/calendar.module.js',
//         frontPath + 'calendar/calendar.controller.js',
//         frontPath + 'calendar/calendar.service.js',

//         frontPath + 'frontend.module.js',
//         frontPath + 'frontend.controller.js',
//         'app/services/http_interceptor.js',
//         'app.js'
//     ]

//     return gulp.src(front_app_files)
//         .pipe(gp_concat('front_app.js'))
//         .pipe(gulp.dest('dist'));
// });



// gulp.task('default', ['backoffice_vendor_css', 'backoffice_vendor_js', 'backoffice_app_js', 'front_vendor_css', 'front_vendor_js', 'front_app_js'], function () {
//     gulp.watch('backoffice/**/*.*', function () {
//         gulp.run('backoffice_app_js');
//     });
//     gulp.watch('assets/**/*.*', function () {
//         gulp.run('backoffice_vendor_css');
//         gulp.run('backoffice_vendor_js');
//         gulp.run('front_vendor_css');
//         gulp.run('front_vendor_js');
//     });

//     gulp.watch('app/**/*.*', function () {
//         gulp.run('front_app_js');
//     });
// });


gulp.task('default', ['backoffice_vendor_css', 'backoffice_vendor_js', 'backoffice_app_js'], function () {
    gulp.watch('backoffice/**/*.*', function () {
        gulp.run('backoffice_app_js');
    });
    gulp.watch('assets/**/*.*', function () {
        gulp.run('backoffice_vendor_css');
        gulp.run('backoffice_vendor_js');
    });
});
