"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',    
    'frontApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://localhost/heaven/api/'    
});

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/');    
});