-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2018 at 03:12 PM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heaven`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suburbs`
--

CREATE TABLE IF NOT EXISTS `tbl_suburbs` (
  `suburbs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cafe_user_id` int(11) NOT NULL,
  `worker_user_id` int(11) NOT NULL,
  `suburbs_pick_name` varchar(255) NOT NULL,
  `suburbs_pick_address` varchar(255) NOT NULL,
  `suburbs_pick_lat` varchar(255) NOT NULL,
  `suburbs_pick_long` varchar(255) NOT NULL,
  `suburbs_drop_name` varchar(255) NOT NULL,
  `suburbs_drop_address` varchar(255) NOT NULL,
  `suburbs_drop_lat` varchar(255) NOT NULL,
  `suburbs_drop_long` varchar(255) NOT NULL,
  `ride_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Pending, 1 = Accepted, 2 = Ongoing, 3 = Completed, 4 = Cancel',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1= Approved ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`suburbs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_suburbs`
--

INSERT INTO `tbl_suburbs` (`suburbs_id`, `cafe_user_id`, `worker_user_id`, `suburbs_pick_name`, `suburbs_pick_address`, `suburbs_pick_lat`, `suburbs_pick_long`, `suburbs_drop_name`, `suburbs_drop_address`, `suburbs_drop_lat`, `suburbs_drop_long`, `ride_status`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 17, 3, 'Baner', 'Baner, Pune, Maharashtra, India', '18.5596581', '73.7799374', 'FC Road', 'FC Road, Revenue Colony, Shivajinagar, Pune, Maharashtra, India', '18.5292481', '73.8435111', 1, 1, '2018-02-12 18:44:38', '2018-02-11 08:36:57', 0),
(2, 32, 8, 'Baner', 'Baner, Pune, Maharashtra, India', '18.447650000000003', '73.76888000000001', 'Katraj', 'Katraj, Pune, Maharashtra, India', '18.55993', '73.86947', 1, 1, '2018-02-13 09:12:27', '2018-02-13 09:12:27', 0),
(3, 32, 8, 'Exceptionaire', 'Exceptionaire Technologies Pvt Ltd - Top Web & Mobile App Development Company in Pune, India, Baner, Pune, Maharashtra, India', '18.56596', '73.70776000000001', 'Hinjewadi', 'Hinjewadi Rajiv Gandhi Infotech Park, Hinjawadi, Pune, Maharashtra, India', '18.596860000000003', '73.77529000000001', 1, 1, '2018-02-13 09:40:24', '2018-02-13 09:40:24', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
