-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2018 at 05:34 PM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heaven`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_workers`
--

CREATE TABLE IF NOT EXISTS `tbl_workers` (
  `workers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cafeuser_id` int(11) DEFAULT NULL,
  `workers_name` varchar(255) NOT NULL,
  `workers_email` varchar(255) NOT NULL,
  `workers_phone` varchar(255) NOT NULL,
  `workers_address` varchar(255) NOT NULL,
  `workers_state` int(11) NOT NULL,
  `workers_country` int(11) NOT NULL,
  `workers_zip` varchar(255) NOT NULL,
  `workers_image` int(11) NOT NULL,
  `workers_lat` varchar(255) NOT NULL,
  `workers_long` varchar(255) NOT NULL,
  `workers_suspend_start` varchar(255) NOT NULL,
  `workers_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`workers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_workers`
--

INSERT INTO `tbl_workers` (`workers_id`, `user_id`, `cafeuser_id`, `workers_name`, `workers_email`, `workers_phone`, `workers_address`, `workers_state`, `workers_country`, `workers_zip`, `workers_image`, `workers_lat`, `workers_long`, `workers_suspend_start`, `workers_suspend_end`) VALUES
(1, 8, 32, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', 0, '', '', '', ''),
(2, 7, 34, 'Ankush', 'priya@email.com', '9876543210', '', 0, 0, '', 0, '', '', '', ''),
(3, 3, 32, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', 0, '', '', '', ''),
(4, 43, 34, 'Worker 23', 'woek22@hn.com', '123548970', '12356', 0, 0, '', 0, '', '', '', ''),
(5, 44, 32, 'ankush', 'ertyu@cvbn', 'sdfsdfsdf', 'sdfsdf', 0, 0, '', 0, '', '', '', ''),
(6, 53, 32, 'Pooja', 'pooja@gmail.com', '1234567890', 'Test Home', 0, 0, '', 0, '', '', '', ''),
(7, 54, 32, 'Pooja Test', 'poojatest@gmail.com', '1234567890', 'Test', 0, 0, '', 0, '', '', '', ''),
(8, 91, 32, 'Test123', 'a123@sds.com', '1234567890', 'asdfghjkl', 0, 0, '', 0, '', '', '', ''),
(9, 94, 32, 'Test36523', 'ank3654@asdsad.com', '1234567890', 'sadsa sadsad', 0, 0, '', 28, '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
