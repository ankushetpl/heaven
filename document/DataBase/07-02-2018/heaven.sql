-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 07, 2018 at 09:46 PM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heaven`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_address` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `user_id`, `admin_name`, `admin_email`, `admin_phone`, `admin_address`) VALUES
(1, 1, 'admin', 'ankush@exceptionaire.co', '1234567891', ''),
(2, 52, 'Ankush', 'ankushAdmin@exceptionaire.co', '1234567890', 'asdfghjkl');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assessors`
--

CREATE TABLE IF NOT EXISTS `tbl_assessors` (
  `assessors_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `assessors_name` varchar(255) NOT NULL,
  `assessors_email` varchar(255) NOT NULL,
  `assessors_phone` varchar(255) NOT NULL,
  `assessors_address` varchar(255) NOT NULL,
  `assessors_state` int(11) NOT NULL,
  `assessors_country` int(11) NOT NULL,
  `assessors_zip` varchar(255) NOT NULL,
  `assessors_lag` varchar(255) NOT NULL,
  `assessors_long` varchar(255) NOT NULL,
  `assessors_suspend_start` varchar(255) NOT NULL,
  `assessors_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`assessors_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_assessors`
--

INSERT INTO `tbl_assessors` (`assessors_id`, `user_id`, `assessors_name`, `assessors_email`, `assessors_phone`, `assessors_address`, `assessors_state`, `assessors_country`, `assessors_zip`, `assessors_lag`, `assessors_long`, `assessors_suspend_start`, `assessors_suspend_end`) VALUES
(1, 16, 'priya', 'priya@email.com', '', '', 0, 0, '', '', '', '', ''),
(2, 45, 'sdsad', 'asdasd@sdsdasd', 'asdasd', 'asdasd', 0, 0, '', '', '', '', ''),
(3, 46, 'Ankush', 'ankush@gmail.com', '1234567890', 'qwertyuiop', 0, 0, '', '', '', '', ''),
(4, 47, 'sddfsdf', 'sdf@dsfdsf', 'sddfsdf', 'sddfsdf', 0, 0, '', '', '', '', ''),
(5, 48, 'Ankush', 'sdfsdf@dsfsdfsdf', 'dsfsdfsdfsdf', 'sddfsdfsdf', 0, 0, '', '', '', '', ''),
(6, 49, 'Ankush5', 'ankush5@gmail.com', '1234567890', 'ankush5', 0, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE IF NOT EXISTS `tbl_booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_client_id` int(11) NOT NULL,
  `booking_worker_id` int(11) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` int(11) NOT NULL,
  `booking_servicetypeestimate_id` int(11) NOT NULL,
  `booking_task_id` int(11) NOT NULL,
  `booking_substitute` int(11) NOT NULL,
  `booking_start_time` varchar(255) NOT NULL,
  `booking_end_time` varchar(255) NOT NULL,
  `booking_payment_status` int(11) NOT NULL,
  `booking_status` tinyint(4) NOT NULL DEFAULT '0',
  `booking_client_status` tinyint(4) NOT NULL DEFAULT '0',
  `booking_worker_status` tinyint(4) NOT NULL DEFAULT '0',
  `booking_created_at` varchar(255) NOT NULL,
  `booking_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cafeusers`
--

CREATE TABLE IF NOT EXISTS `tbl_cafeusers` (
  `cafeusers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cafeusers_name` varchar(255) NOT NULL,
  `cafeusers_email` varchar(255) NOT NULL,
  `cafeusers_phone` varchar(255) NOT NULL,
  `cafeusers_address` varchar(255) NOT NULL,
  `cafeusers_state` int(11) DEFAULT NULL,
  `cafeusers_country` int(11) DEFAULT NULL,
  `cafeusers_zip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cafeusers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_cafeusers`
--

INSERT INTO `tbl_cafeusers` (`cafeusers_id`, `user_id`, `cafeusers_name`, `cafeusers_email`, `cafeusers_phone`, `cafeusers_address`, `cafeusers_state`, `cafeusers_country`, `cafeusers_zip`) VALUES
(1, 5, 'Ankush', 'ankush@exceptionaire.co', '1234567890', 'Hno 1564', 8, 1, '45632'),
(2, 6, 'Priya', 'priya@exceptionaire.co', '1234567890', 'Hno 1564', 8, 1, '45632'),
(3, 22, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(4, 23, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(5, 24, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(6, 25, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(7, 26, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(8, 27, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(9, 28, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(10, 29, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(11, 30, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(12, 31, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(13, 32, 'Priya', 'priya@email.com', '1234567890', 'dbhadhabsd', 1, 2, '123456'),
(14, 33, 'dsasdsad', 'asd@dsdsd', 'asdasd', 'asdasd', 0, 0, ''),
(15, 34, 'asdasd', 'asd@ssdsds', '123567878', 'asdasd', 0, 0, ''),
(16, 35, 'Ankush', 'ankush.test@exceptionaire.co', '1234567890', '123456', 0, 0, ''),
(17, 36, 'Priya', 'priya@gmail.com', '1234567890', '132456', 0, 0, ''),
(18, 37, 'Priya First', 'priyatest@gmail.com', '12346987', '213465', 0, 0, ''),
(19, 38, 'Ankush1232', 'ankush@asdj.com', '12356879', 'asabskjkjkj', 0, 0, ''),
(20, 39, 'Aman Test', 'aman@eck.com', '123456789', 'test', 0, 0, ''),
(21, 88, 'Ankush Test 32', 'ankush32@gmail.com', '1234567890', 'Test test 32', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_checklist`
--

CREATE TABLE IF NOT EXISTS `tbl_checklist` (
  `checklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `checklist_name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approve 1 = Approve',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT ' 0 = Not Deleted, 1= Deleted',
  PRIMARY KEY (`checklist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_checklist`
--

INSERT INTO `tbl_checklist` (`checklist_id`, `checklist_name`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Friendliness', 1, '2018-02-07 04:25:12', '2018-02-07 04:18:15', 0),
(2, 'Treatment', 0, '2018-02-07 16:04:28', '2018-02-07 04:18:28', 0),
(3, 'Recommend to Others', 1, '2018-02-07 04:25:32', '2018-02-07 04:19:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_claim`
--

CREATE TABLE IF NOT EXISTS `tbl_claim` (
  `claim_id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_claimcategory_id` int(11) NOT NULL,
  `claim_client_id` int(11) NOT NULL,
  `claim_worker_id` int(11) NOT NULL,
  `claim_status` tinyint(4) NOT NULL DEFAULT '0',
  `claim_created_at` varchar(255) NOT NULL,
  `claim_updated_id` varchar(255) NOT NULL,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_claimcategory`
--

CREATE TABLE IF NOT EXISTS `tbl_claimcategory` (
  `claimcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `claimcategory_name` varchar(255) NOT NULL,
  `claimcategory_des` text NOT NULL,
  `claimcategory_status` tinyint(4) NOT NULL DEFAULT '0',
  `claimcategory_created_at` varchar(255) NOT NULL,
  `claimcategory_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`claimcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clients`
--

CREATE TABLE IF NOT EXISTS `tbl_clients` (
  `clients_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `clients_name` varchar(255) NOT NULL,
  `clients_email` varchar(255) NOT NULL,
  `clients_phone` varchar(255) NOT NULL,
  `clients_address` varchar(255) NOT NULL,
  `clients_state` int(11) NOT NULL,
  `clients_country` int(11) NOT NULL,
  `clients_zip` varchar(255) NOT NULL,
  `clients_image` text NOT NULL,
  `clients_lat` varchar(255) NOT NULL,
  `clients_long` varchar(255) NOT NULL,
  `clients_suspend_start` varchar(255) NOT NULL,
  `clients_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`clients_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `tbl_clients`
--

INSERT INTO `tbl_clients` (`clients_id`, `user_id`, `clients_name`, `clients_email`, `clients_phone`, `clients_address`, `clients_state`, `clients_country`, `clients_zip`, `clients_image`, `clients_lat`, `clients_long`, `clients_suspend_start`, `clients_suspend_end`) VALUES
(3, 12, 'Ankush', 'priya@email.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(4, 14, 'Ankush', 'admin1dd2d311@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(5, 50, 'Ankush', 'ankush2324@exceptionaire.co', '1234567890', 'qwertyuiop', 0, 0, '', '', '', '', '', ''),
(6, 51, 'Priya1', 'priya@test.com', '7894561230', 'qwertyuiop', 0, 0, '', '', '', '', '', ''),
(7, 55, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(8, 0, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(9, 57, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(10, 58, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(11, 59, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(12, 60, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(13, 61, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(14, 62, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(15, 63, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(16, 64, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(17, 65, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(18, 66, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(19, 67, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(20, 68, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(21, 69, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(22, 70, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(23, 71, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(24, 72, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(25, 73, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(26, 74, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(27, 75, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(28, 76, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(29, 77, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(30, 78, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(31, 79, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(32, 80, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(33, 81, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(34, 82, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(35, 83, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(36, 84, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(37, 85, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(38, 86, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(39, 87, 'Ankush', 'ankushTest@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE IF NOT EXISTS `tbl_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `faq_question` varchar(255) NOT NULL,
  `faq_answer` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1 = Approved',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`faq_id`, `role_id`, `faq_question`, `faq_answer`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 0, 'What is A for ?', 'A for Apple.', 1, '2018-02-06 09:09:38', '2018-01-23 19:05:24', 0),
(2, 0, 'What is Z for ?', 'Z for Zebra.', 1, '2018-02-05 17:39:59', '2018-01-23 19:06:19', 0),
(3, 0, 'What is K for ?', 'K for King.', 1, '2018-02-05 17:40:01', '2018-01-23 19:02:51', 0),
(4, 0, 'What is S for ?', 'S for Sugar.', 1, '2018-01-23 19:03:08', '2018-01-23 19:03:08', 0),
(5, 0, 'How is delete working ?', 'Delete is working fine.', 1, '2018-01-23 19:05:53', '2018-01-23 19:06:02', 1),
(6, 1, 'Test', 'Test', 1, '2018-02-06 09:09:34', '2018-02-06 09:09:32', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_favorite`
--

CREATE TABLE IF NOT EXISTS `tbl_favorite` (
  `favorite_id` int(11) NOT NULL AUTO_INCREMENT,
  `favorite_client_id` int(11) NOT NULL,
  `favorite_worker_id` int(11) NOT NULL,
  `favorite_created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`favorite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leaves`
--

CREATE TABLE IF NOT EXISTS `tbl_leaves` (
  `leaves_id` int(11) NOT NULL AUTO_INCREMENT,
  `leaves_name` varchar(255) NOT NULL,
  `leaves_des` longtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approve, 1 = Approve',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) DEFAULT '0' COMMENT '0 = Not Deleted, 1 = deleted',
  PRIMARY KEY (`leaves_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_leaves`
--

INSERT INTO `tbl_leaves` (`leaves_id`, `leaves_name`, `leaves_des`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Sick', 'Ironing level ', 0, '2018-02-05 17:05:23', '0000-00-00 00:00:00', 0),
(2, 'Accident', 'Speed level', 0, '2018-02-05 17:03:29', '2018-02-05 15:15:44', 0),
(3, 'Police', 'Cleaning level', 0, '2018-02-05 17:03:35', '2018-02-05 16:53:23', 0),
(4, 'Death', 'Death', 0, '2018-02-05 17:03:48', '0000-00-00 00:00:00', 0),
(5, 'Deaths', 'Death', 0, '2018-02-05 17:03:48', '0000-00-00 00:00:00', 0),
(6, 'Deaths', 'Death', 0, '2018-02-05 17:03:48', '0000-00-00 00:00:00', 0),
(7, 'Deaths', 'Death', 0, '2018-02-05 17:03:48', '0000-00-00 00:00:00', 0),
(8, 'Deaths', 'Death', 1, '2018-02-05 17:17:55', '0000-00-00 00:00:00', 0),
(9, 'Deaths', 'Death', 0, '2018-02-05 17:03:48', '0000-00-00 00:00:00', 0),
(10, 'Deaths', 'Death', 0, '2018-02-05 17:03:48', '0000-00-00 00:00:00', 0),
(11, 'Deaths', 'Death', 1, '2018-02-05 17:17:36', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE IF NOT EXISTS `tbl_rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1 = Approved',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`rating_id`, `rating_name`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Cleaning', 1, '2018-01-08 19:23:28', '2018-01-16 20:36:35', 0),
(2, 'Ironing', 1, '2018-01-23 19:23:28', '2018-01-16 20:36:35', 0),
(3, 'Cleaning8', 0, '2018-01-23 20:39:07', '2018-01-16 21:08:58', 0),
(4, 'adasdassdas', 1, '2018-01-23 21:34:40', '2018-01-23 14:51:58', 0),
(5, 'Ankush', 1, '2018-01-23 20:39:04', '2018-01-23 17:47:45', 0),
(6, 'Test 2', 1, '2018-01-23 19:23:28', '2018-01-23 18:03:36', 0),
(7, 'sfdsfsdf', 1, '2018-01-23 21:34:40', '2018-01-23 14:53:45', 0),
(8, 'sdfdsfsfa', 1, '2018-01-23 19:23:28', '2018-01-23 14:54:13', 0),
(9, 'Test', 1, '2018-01-23 20:18:04', '2018-01-23 20:18:04', 0),
(10, 'Test23', 1, '2018-01-24 05:22:01', '2018-01-23 21:05:38', 0),
(11, 'Test32', 0, '2018-01-24 05:20:36', '2018-01-23 21:06:00', 0),
(12, 'Ankush2', 1, '2018-01-24 05:32:52', '2018-01-24 05:32:33', 0),
(13, 'Ankush 2', 1, '2018-01-24 05:34:41', '2018-01-24 05:34:38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1 = Approved',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `role_name`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Administrator', 1, '2018-01-23 20:17:57', '2018-01-23 20:17:57', 0),
(2, 'Cafe User ', 1, '2018-02-05 18:21:43', '2018-01-14 09:44:44', 0),
(3, 'Workers', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0),
(4, 'Assessors', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0),
(5, 'Clients', 1, '2018-01-14 09:44:44', '2018-01-14 09:44:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_servicetype`
--

CREATE TABLE IF NOT EXISTS `tbl_servicetype` (
  `servicetype_id` int(11) NOT NULL AUTO_INCREMENT,
  `servicetype_name` varchar(255) NOT NULL,
  `servicetype_status` tinyint(4) NOT NULL DEFAULT '0',
  `servicetype_created_at` varchar(255) NOT NULL,
  `servicetype_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`servicetype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_servicetypecategory`
--

CREATE TABLE IF NOT EXISTS `tbl_servicetypecategory` (
  `servicetypecategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `servicetypecategory_servicetype_id` int(11) NOT NULL,
  `servicetypecategory_name` varchar(255) NOT NULL,
  `servicetypecategory_status` tinyint(4) NOT NULL DEFAULT '0',
  `servicetypecategory_created_at` varchar(255) NOT NULL,
  `servicetypecategory_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`servicetypecategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_servicetypeestimate`
--

CREATE TABLE IF NOT EXISTS `tbl_servicetypeestimate` (
  `servicetypeestimate_id` int(11) NOT NULL AUTO_INCREMENT,
  `servicetypeestimate_servicetype_id` int(11) NOT NULL,
  `servicetypeestimate_servicetypecategory_id` int(11) NOT NULL,
  `servicetypeestimate_name` varchar(255) NOT NULL,
  `servicetypeestimate_hour` varchar(255) NOT NULL,
  `servicetypeestimate_status` tinyint(4) NOT NULL DEFAULT '0',
  `servicetypeestimate_created_at` varchar(255) NOT NULL,
  `servicetypeestimate_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`servicetypeestimate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_skillcategory`
--

CREATE TABLE IF NOT EXISTS `tbl_skillcategory` (
  `skillcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `skillcategory_skill_id` int(11) DEFAULT NULL,
  `skillcategory_name` varchar(255) NOT NULL,
  `skillcategory_des` text NOT NULL,
  `skillcategory_status` tinyint(4) NOT NULL DEFAULT '0',
  `skillcategory_created_at` varchar(255) NOT NULL,
  `skillcategory_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`skillcategory_id`),
  KEY `skillcategory_skill_ID` (`skillcategory_skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_skills`
--

CREATE TABLE IF NOT EXISTS `tbl_skills` (
  `skills_id` int(11) NOT NULL AUTO_INCREMENT,
  `skills_name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approve 1 = Approve',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = Deleted',
  PRIMARY KEY (`skills_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_skills`
--

INSERT INTO `tbl_skills` (`skills_id`, `skills_name`, `parent`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Java123', 0, 0, '2018-02-07 16:06:22', '2018-02-07 08:02:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_substitute`
--

CREATE TABLE IF NOT EXISTS `tbl_substitute` (
  `substitute_id` int(11) NOT NULL AUTO_INCREMENT,
  `substitute_send_worker_id` int(11) NOT NULL,
  `substitute_receive_worker_id` int(11) NOT NULL,
  `substitute_status` tinyint(4) NOT NULL DEFAULT '0',
  `substitute_created_at` varchar(255) NOT NULL,
  `substitute_updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`substitute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tutorial`
--

CREATE TABLE IF NOT EXISTS `tbl_tutorial` (
  `tutorial_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `tutorial_type` int(11) NOT NULL COMMENT '0 = Text, 1 = Image, 2 =  Video',
  `tutorial_name` varchar(255) NOT NULL,
  `tutorial_des` longtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approved, 1= Approved ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Deleted, 1 = deleted',
  PRIMARY KEY (`tutorial_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_tutorial`
--

INSERT INTO `tbl_tutorial` (`tutorial_id`, `role_id`, `tutorial_type`, `tutorial_name`, `tutorial_des`, `status`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 1, 0, 'Ankush', 'Test Test', 1, '2018-02-07 15:35:53', '2018-02-07 15:35:53', 0),
(2, 5, 0, 'AnkushTest1', 'TestAnkush1', 1, '2018-02-07 15:46:31', '2018-02-07 15:45:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `user_registertype` int(11) NOT NULL,
  `device_token` text,
  `device_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1= Web, 2 = Android, 3 = iPhone',
  `social_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Approve, 1 = Approve',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Suspend, 1 = Suspend',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Delete, 1 = Delete',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_username`, `user_password`, `user_role_id`, `user_registertype`, `device_token`, `device_type`, `social_id`, `status`, `created_at`, `updated_at`, `is_suspend`, `is_deleted`) VALUES
(1, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '1', 1, 0, 1, '2018-01-19 10:13:59', '0000-00-00 00:00:00', 0, 0),
(2, 'aman', 'ccda1683d8c97f8f2dff2ea7d649b42c', 4, 0, '', 0, 0, 1, '2018-01-20 10:47:06', '2018-01-14 09:53:42', 0, 0),
(3, 'priya', '0b1c8bc395a9588a79cd3c191c22a6b4', 3, 0, 'sadasdasd', 3, 0, 1, '2018-01-19 19:07:54', '2018-02-01 05:57:40', 0, 0),
(4, 'ankush', '3a0135f9157447e16da5c17863f1531c', 5, 0, 'asdgasgdjgsahd', 2, 0, 1, '2018-01-19 19:17:36', '2018-01-19 19:17:36', 0, 0),
(5, 'amit', '0cb1eb413b8f7cee17701a37a1d74dc3', 5, 0, 'sadasdasd', 3, 0, 1, '2018-01-19 10:13:59', '2018-02-01 05:59:17', 0, 0),
(7, 'adminq@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 3, 0, '213213sdfdnanjkngkj', 127, 21323132, 1, '2018-01-19 10:13:59', '2018-01-15 19:29:44', 0, 0),
(8, 'adminq@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 3, 0, '213213sdfdnanjkngkj', 127, 21323132, 1, '2018-01-19 10:13:59', '2018-01-15 19:30:03', 0, 0),
(12, 'asdasd', 'a8f5f167f44f4964e6c998dee827110c', 5, 0, NULL, 1, NULL, 0, '2018-01-28 14:52:42', '2018-01-28 14:52:42', 0, 0),
(15, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:03:50', 0, 0),
(16, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 4, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:05:36', 0, 0),
(17, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:10:34', 0, 0),
(18, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:11:25', 0, 0),
(19, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:12:23', 0, 0),
(20, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:13:10', 0, 0),
(21, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:14:37', 0, 0),
(22, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:15:05', 0, 0),
(23, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:17:49', 0, 0),
(24, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:17:54', 0, 0),
(25, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:17:57', 0, 0),
(26, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:00', 0, 0),
(27, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:02', 0, 0),
(28, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:03', 0, 0),
(29, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-19 10:13:59', '2018-01-17 06:18:04', 0, 0),
(30, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-28 08:57:16', '2018-01-17 06:20:54', 1, 0),
(31, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 0, '2018-01-28 09:58:47', '2018-01-17 06:22:04', 1, 0),
(32, 'priya@email.com', '0b1c8bc395a9588a79cd3c191c22a6b4', 2, 0, '', 0, 0, 1, '2018-01-27 09:29:07', '2018-01-17 06:22:47', 0, 0),
(33, 'qwerr', '7085ed00bd92dc3160e1c1e9841a0f35', 2, 0, '', 1, 0, 1, '2018-01-28 08:57:04', '2018-01-26 11:28:12', 0, 0),
(34, 'ankush1', '8675898b0c43a3744e9e35243b18f407', 2, 0, '', 1, 0, 1, '2018-01-28 08:57:06', '2018-01-26 11:31:29', 0, 0),
(35, 'ankushTest', 'eb667b75017efc9c435fa4e2a46088b7', 2, 0, '', 1, 0, 0, '2018-01-28 08:57:12', '2018-01-26 12:07:53', 1, 0),
(36, 'priya1', '1ae57c1ec8411cb5596615a9675b9c29', 2, 0, '', 1, 0, 1, '2018-01-28 08:56:56', '2018-01-26 12:08:55', 0, 0),
(37, 'priyaTest', 'a53505763a27b06bd9473e7ce9b81e2c', 2, 0, '', 1, 0, 1, '2018-01-28 09:58:55', '2018-01-27 09:03:03', 1, 0),
(38, 'ankush123456', '6ecf3423f46df4fc0fc2e8a433081be5', 2, 0, '', 1, 0, 0, '2018-01-28 09:56:41', '2018-01-28 09:56:41', 1, 0),
(39, 'amanTest', '2ec79c1a489559d4b07f05c1a06d1d87', 2, 0, NULL, 1, NULL, 0, '2018-01-28 10:28:29', '2018-01-28 10:28:50', 1, 1),
(40, 'Work 1', '7dc462585f94f40430e0ade4254bcad3', 3, 0, NULL, 1, NULL, 0, '2018-01-28 13:37:21', '2018-01-28 13:37:21', 0, 1),
(41, 'Work 11', 'bf2e0c1932f3f2c983c507fbc4958d37', 3, 0, NULL, 1, NULL, 1, '2018-01-28 13:39:17', '2018-01-28 13:39:17', 0, 0),
(42, 'Work 21', 'fb94f9a5c686c21267b289cfa6a9cbc8', 3, 0, NULL, 1, NULL, 0, '2018-01-28 13:40:30', '2018-01-28 13:40:30', 1, 0),
(43, 'Work 22', '3d905149847d4fe5dfef0c23bb37d725', 3, 0, NULL, 1, NULL, 1, '2018-01-28 13:42:25', '2018-01-28 13:50:29', 0, 1),
(44, 'sddfsd', '0e27211006e6e46734163b015565f382', 3, 0, NULL, 1, NULL, 0, '2018-01-28 14:44:05', '2018-01-28 14:44:05', 0, 0),
(46, 'ankush2', 'f5543650a16a92a966fb01ef7232033d', 4, 0, NULL, 1, NULL, 1, '2018-01-28 14:55:02', '2018-01-28 14:55:02', 1, 0),
(47, 'sdfsdf', 'd58e3582afa99040e27b92b13c8f2280', 4, 0, NULL, 1, NULL, 0, '2018-01-28 14:56:39', '2018-01-28 14:56:39', 0, 1),
(48, 'sdfsdfsdf', '73a90acaae2b1ccc0e969709665bc62f', 4, 0, NULL, 1, NULL, 0, '2018-01-28 14:56:53', '2018-01-28 15:06:50', 0, 1),
(49, 'ankush5', '5341d721dd3a5205fd3fac94929f4b09', 4, 0, NULL, 1, NULL, 1, '2018-01-28 15:10:25', '2018-01-28 15:10:38', 0, 0),
(50, 'ankush1234', '7610fae5057e47565070266c9ba12649', 5, 0, NULL, 1, NULL, 1, '2018-01-29 06:21:31', '2018-01-29 06:21:31', 1, 0),
(51, 'testpriya', 'cc489e7d34ddc9ed29d2743316b68660', 5, 0, NULL, 1, NULL, 1, '2018-01-29 06:23:13', '2018-01-29 06:50:28', 0, 0),
(52, 'ankushAdmin', '711cd584dcd1b254551e57a4e19c4514', 1, 0, NULL, 1, NULL, 1, '2018-01-29 16:10:42', '2018-01-29 16:15:02', 1, 0),
(53, 'pooja', '9cbb6aebcf5ae14a9248b4c08165212e', 3, 0, NULL, 1, NULL, 1, '2018-01-31 05:54:53', '2018-01-31 05:54:53', 1, 0),
(54, 'poojatest', '0183a4ad81910ea32028e2410951a133', 3, 0, NULL, 1, NULL, 1, '2018-01-31 05:58:17', '2018-01-31 05:58:17', 0, 0),
(55, 'ankushTest1', '2ce688a46a7403ae270ea063ffe5e158', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 0, '2018-02-01 06:06:39', '2018-02-01 06:06:39', 0, 0),
(56, 'ankushTest2', '65f47dcc4048fc8aab67368857a74f52', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 0, '2018-02-01 06:08:51', '2018-02-01 06:08:51', 0, 0),
(57, 'ankushTest21', '67342bd05c478a597f5312bebb0b2ce7', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 0, '2018-02-01 06:16:13', '2018-02-01 06:16:13', 0, 0),
(58, 'ankushTest211', 'a5eb0ed0fa6746fc1f6a175958a9766d', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:29:50', '2018-02-01 06:29:50', 0, 0),
(59, 'ankushTest2111', '20f89fb54c8aebce03bdf9fb9d6ac27e', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:30:20', '2018-02-01 06:30:20', 0, 0),
(60, 'ankushTest21111', '690464cd2ef28ee9f7530aa99de6a822', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:42:03', '2018-02-01 06:42:03', 0, 0),
(61, 'ankushTest211111', '05315771ded0b771c3eddf3c7154627a', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:43:06', '2018-02-01 06:43:06', 0, 0),
(62, 'ankushTest2111112', 'b3731bc2976f0f2040f16e9e46571dc7', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:44:00', '2018-02-01 06:44:00', 0, 0),
(63, 'ankushTest21111122', '264e1da0df48f328745041f8fee24776', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:44:39', '2018-02-01 06:44:39', 0, 0),
(64, 'ankushTest211111223', 'f2a17bb8ab79f758b172129b3cfde0cb', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:45:13', '2018-02-01 06:45:13', 0, 0),
(65, 'ankushTest2111112234', 'd4268be4ec9f29ca6e161e68e6b5ca23', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:46:17', '2018-02-01 06:46:17', 0, 0),
(66, 'ankushTest21111122345', '3f6667940d0f6c3ed4eb24f0e9d11f33', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:48:37', '2018-02-01 06:48:37', 0, 0),
(67, 'ankushTest211111223456', '054a5cbdd298bb978a2a3879b526b0d2', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:50:04', '2018-02-01 06:50:04', 0, 0),
(68, 'ankushTest3', 'fe0e6f3853b0a19771a999dcca81903c', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:50:46', '2018-02-01 06:50:46', 0, 0),
(69, 'ankushTest31', '82a90caa99983371962c90367698bf53', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:51:31', '2018-02-01 06:51:31', 0, 0),
(70, 'ankushTest32', '2e11587024c1c325a85d2bd36598cf24', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:52:03', '2018-02-01 06:52:03', 0, 0),
(71, 'ankushTest322', '80203476544705e1256a419c7f59f321', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:57:17', '2018-02-01 06:57:17', 0, 0),
(72, 'ankushTest3222', '202b4500345f3a9efd78a7670a278e67', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:57:54', '2018-02-01 06:57:54', 0, 0),
(73, 'ankushTest32222', 'f5049ceb3ca36aa68c3ff1b51828a984', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 06:58:22', '2018-02-01 06:58:22', 0, 0),
(74, 'ankushTest322224', '9103fdf3c98895b71bbe832a3802695b', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:00:08', '2018-02-01 07:00:08', 0, 0),
(75, 'ankushTest322225', '1969a5eb444b6de94490ed5291cbcd15', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:11:48', '2018-02-01 07:11:48', 0, 0),
(76, 'ankushTest4', 'c90f5f5d3e461e0a42b4b6c1bd9f7c51', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:12:35', '2018-02-01 07:12:35', 0, 0),
(77, 'ankushTest41', 'd426f557c2a8aa6d5db85dd01063b120', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:16:10', '2018-02-01 07:16:10', 0, 0),
(78, 'ankushTest42', 'fe86d823dc8546cc508261d90bb23ce2', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:16:41', '2018-02-01 07:16:41', 0, 0),
(79, 'ankushTest423', '2c5a63bd666cf9ece731950c1185014b', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:19:17', '2018-02-01 07:19:17', 0, 0),
(80, 'ankushTest426', '1b08efbada5228f3b646fe96837bbd2f', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:20:05', '2018-02-01 07:20:05', 0, 0),
(81, 'ankushTest425', '0d29f14e777c726adef5cf210cc5b6f7', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:21:00', '2018-02-01 07:21:00', 0, 0),
(82, 'ankushTest5', '0d5ca974a554e7e61911b3f98987f18c', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:29:01', '2018-02-01 07:29:01', 0, 0),
(83, 'ankushTest6', 'bd113d16e00d0d9c35bde353182c8805', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:29:16', '2018-02-01 07:29:16', 0, 0),
(84, 'ankushTest7', '7461e4ae5cd0580594deeba42c8368f5', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:29:49', '2018-02-01 07:29:49', 0, 0),
(85, 'ankushTest8', '258d67ccf417fe7bcd425b2725c99939', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:31:01', '2018-02-01 07:31:01', 0, 0),
(86, 'ankushTest9', '031e5ad1a5b2b6817f90b70b543eb405', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:31:24', '2018-02-01 07:31:24', 0, 0),
(87, 'ankushTest91', 'c21046e620c15e5a7199ac1ba318cb91', 5, 0, 'gjkhgkjhgjgjk', 127, 1212, 1, '2018-02-01 07:32:26', '2018-02-01 07:32:26', 0, 0),
(88, 'ankush32', '61b9f5c05753e09baf61698a707ac06e', 2, 0, NULL, 1, NULL, 1, '2018-02-02 08:14:43', '2018-02-02 08:16:28', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_workers`
--

CREATE TABLE IF NOT EXISTS `tbl_workers` (
  `workers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cafeuser_id` int(11) DEFAULT NULL,
  `workers_name` varchar(255) NOT NULL,
  `workers_email` varchar(255) NOT NULL,
  `workers_phone` varchar(255) NOT NULL,
  `workers_address` varchar(255) NOT NULL,
  `workers_state` int(11) NOT NULL,
  `workers_country` int(11) NOT NULL,
  `workers_zip` varchar(255) NOT NULL,
  `workers_image` text NOT NULL,
  `workers_lat` varchar(255) NOT NULL,
  `workers_long` varchar(255) NOT NULL,
  `workers_suspend_start` varchar(255) NOT NULL,
  `workers_suspend_end` varchar(255) NOT NULL,
  PRIMARY KEY (`workers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_workers`
--

INSERT INTO `tbl_workers` (`workers_id`, `user_id`, `cafeuser_id`, `workers_name`, `workers_email`, `workers_phone`, `workers_address`, `workers_state`, `workers_country`, `workers_zip`, `workers_image`, `workers_lat`, `workers_long`, `workers_suspend_start`, `workers_suspend_end`) VALUES
(1, 8, NULL, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(2, 7, NULL, 'Ankush', 'priya@email.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(3, 3, NULL, 'Ankush', 'adminq@gmail.com', '9876543210', '', 0, 0, '', '', '', '', '', ''),
(4, 43, NULL, 'Worker 23', 'woek22@hn.com', '123548970', '12356', 0, 0, '', '', '', '', '', ''),
(5, 44, NULL, 'ankush', 'ertyu@cvbn', 'sdfsdfsdf', 'sdfsdf', 0, 0, '', '', '', '', '', ''),
(6, 53, NULL, 'Pooja', 'pooja@gmail.com', '1234567890', 'Test Home', 0, 0, '', '', '', '', '', ''),
(7, 54, 32, 'Pooja Test', 'poojatest@gmail.com', '1234567890', 'Test', 0, 0, '', '', '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
