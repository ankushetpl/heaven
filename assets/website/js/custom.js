// scroll remove class js 
    jQuery(window).scroll(function() {
      	if (jQuery(document).scrollTop() > 150) {
        	jQuery('.header_section').addClass('header_set');
        	jQuery('.navbar_menu_link').addClass('navbar_menu_link_set');		
      	} else {
        	jQuery('.header_section').removeClass('header_set');
			jQuery('.navbar_menu_link').removeClass('navbar_menu_link_set');
      	}
    });
	
// scroll section show js 
	jQuery("#nav ul li a[href^='#']").on('click', function(e) {
	   // prevent default anchor click behavior
	   e.preventDefault();
	   // store hash
	   var hash = this.hash;
	   // animate
	   	jQuery('html, body').animate({
		   	scrollTop: jQuery(hash).offset().top
	 	}, 2000, function(){
		   // when done, add hash to url
		   // (default click behaviour)
		   window.location.hash = hash;
	 	});
	});

// //responsive tab js here
// 	jQuery('#horizontalTab').easyResponsiveTabs({
// 		type: 'default', //Types: default, vertical, accordion           
// 		width: 'auto', //auto or any width like 600px
// 		fit: true,   // 100% fit in a container
// 		closed: 'accordion', // Start closed if in accordion view
// 		activate: function(event) { // Callback function if tab is switched
// 			var $tab = jQuery(this);
// 			var $info = jQuery('#tabInfo');
// 			var $name = jQuery('span', $info);
// 			$name.text($tab.text());
// 			$info.show();
// 		}
// 	});

// 	jQuery('#verticalTab').easyResponsiveTabs({
// 		type: 'vertical',
// 		width: 'auto',
// 		fit: true
// 	});
