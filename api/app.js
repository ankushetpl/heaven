var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var cron = require('node-cron');
var dateFormat = require('dateformat');
var async = require('async');

// Test
var routes = require('./routes/index');
var users = require('./routes/users');
var categories = require('./routes/categories');

// Live 
var login = require('./routes/login');
var user = require('./routes/backend/user');
var rating = require('./routes/backend/rating');
var role = require('./routes/backend/role');
var faq = require('./routes/backend/faq');
var files = require('./routes/backend/fileupload');
var leave = require('./routes/backend/leave');
var tutorial = require('./routes/backend/tutorial');
var checklist = require('./routes/backend/checklist');
var skills = require('./routes/backend/skills');
var suburbs = require('./routes/backend/suburbs'); //Cab Booking
var page = require('./routes/backend/page');
var tasklist = require('./routes/backend/tasklist');
var tasklistClient = require('./routes/backend/tasklistClient');
var feedback = require('./routes/backend/feedback');
var claimCategory = require('./routes/backend/claimCategory');
var claim = require('./routes/backend/claim');
var serviceType = require('./routes/backend/serviceType');
var weekService = require('./routes/backend/weekService');
var cleaningArea = require('./routes/backend/cleaningArea');
var amenities = require('./routes/backend/amenities');
var promocode = require('./routes/backend/promocode');
var suburb = require('./routes/backend/suburb');
var email = require('./routes/backend/email');
var paymentSuccess = require('./routes/backend/paymentSuccess');
var paymentUnsuccess = require('./routes/backend/paymentUnsuccess');
var paymentCancel = require('./routes/backend/paymentCancel');
var paymentPending = require('./routes/backend/paymentPending');
var todayBooking = require('./routes/backend/todayBooking');
var allBooking = require('./routes/backend/allBooking');
var socialSetting = require('./routes/backend/socialSetting');
var mailSetting = require('./routes/backend/mailSetting');
var setting = require('./routes/backend/setting');
var banner = require('./routes/backend/banner');
var modules = require('./routes/backend/modules');
var leaveManage = require('./routes/backend/leaveManage');
var notification = require('./routes/backend/notification');
var suspension = require('./routes/backend/suspension');
var earlyPay = require('./routes/backend/earlyPay');
var support = require('./routes/backend/support');
var buyCredit = require('./routes/backend/buyCredit');
var dashboard = require('./routes/backend/dashboard');
var transReport = require('./routes/backend/transReport');
var walletTopUp = require('./routes/backend/walletTopUp');
var paymentRefund = require('./routes/backend/paymentRefund');

// Frontend
var cafeUser = require('./routes/frontend/cafeUser');
var assessor = require('./routes/frontend/assessor');

// Website
var website = require('./routes/website/website');

// API
var worker = require('./routes/api/worker');
var client = require('./routes/api/client');
var Worker = require('./models/Worker');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cors());

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Enable CORS from client-side
 */

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials, total_roles");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

//Test
app.use('/', routes);
app.use('/users', users);
app.use('/categories', categories);

// Live
app.use('/login', login);
app.use('/user', user);
app.use('/rating', rating);
app.use('/role', role);
app.use('/faq', faq);
app.use('/files', files);
app.use('/leave', leave);
app.use('/tutorial', tutorial);
app.use('/checklist', checklist);
app.use('/skills', skills);
app.use('/suburbs', suburbs); //Cab Booking
app.use('/page', page);
app.use('/tasklist', tasklist);
app.use('/tasklistClient', tasklistClient);
app.use('/feedback', feedback);
app.use('/claimCategory', claimCategory);
app.use('/claim', claim);
app.use('/serviceType', serviceType);
app.use('/weekService', weekService);
app.use('/cleaningArea', cleaningArea);
app.use('/amenities', amenities);
app.use('/promocode', promocode);
app.use('/suburb', suburb);
app.use('/email', email);
app.use('/paymentSuccess', paymentSuccess);
app.use('/paymentUnsuccess', paymentUnsuccess);
app.use('/paymentCancel', paymentCancel);
app.use('/paymentPending', paymentPending);
app.use('/todayBooking', todayBooking);
app.use('/allBooking', allBooking);
app.use('/social', socialSetting);
app.use('/mail', mailSetting);
app.use('/setting', setting);
app.use('/banner', banner);
app.use('/modules', modules);
app.use('/leaveManage', leaveManage);
app.use('/notification', notification);
app.use('/suspension', suspension);
app.use('/earlyPay', earlyPay);
app.use('/support', support);
app.use('/buyCredit', buyCredit);
app.use('/dashboard', dashboard);
app.use('/transReport', transReport);
app.use('/walletTopUp', walletTopUp);
app.use('/paymentRefund', paymentRefund);

// Frontend
app.use('/cafeUser', cafeUser);
app.use('/assessor', assessor);

// Website
app.use('/website', website);

// API
app.use('/worker', worker);
app.use('/client', client);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}


app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


var mysql = require('mysql');
var db = require('./config/database.js');
var Q = require("q");
var commonHelper = require('./helper/common_helper.js');

cron.schedule('* * * * *', function() {
    console.log('running a task every minute');

    var deferred = Q.defer();
    var total = 0;
    var currentDateTime = commonHelper.getCurrentDateTime();
    var currentDate = commonHelper.getCurrentDate();

    var query = "SELECT COUNT(*) as total FROM `tbl_user` WHERE `status`= 1 AND `is_deleted` = 0 AND `is_suspend` = 1;";

    query = mysql.format(query);
    db.connection.query(query, function(err, result, fields) {
        if (err) {
            deferred.reject(new Error(err));
        } else {
            total = result[0].total;
            if (total > 0) {
                var querySuspend = "SELECT * FROM `tbl_user` WHERE `status`= 1 AND `is_deleted` = 0 AND `is_suspend` = 1 AND DATE_FORMAT(suspendEnd, '%Y-%m-%d') = '" + currentDate + "' ";

                querySuspend = mysql.format(querySuspend);
                db.connection.query(querySuspend, function(errSuspend, resultSuspend, fields) {
                    if (errSuspend) {
                        deferred.reject(new Error(errSuspend));
                    } else {
                        resultSuspend.forEachAsync(function(element, index, arr) {

                            var Usertype = element.user_role_id;
                            var UserID = element.user_id;

                            var table = '';
                            if (Usertype == 1 || Usertype == 6) {
                                table = 'tbl_admin';
                                var ModuleData = {};
                            } else {
                                if (Usertype == 2) {
                                    table = 'tbl_cafeusers';
                                    var ModuleData = {
                                        cafeusers_suspendStart: '',
                                        cafeusers_suspendEnd: ''
                                    };
                                } else {
                                    if (Usertype == 3) {
                                        table = 'tbl_workers';
                                        var ModuleData = {
                                            workers_suspendStart: '',
                                            workers_suspendEnd: ''
                                        };
                                    } else {
                                        if (Usertype == 4) {
                                            table = 'tbl_assessors';
                                            var ModuleData = {
                                                assessors_suspendStart: '',
                                                assessors_suspendEnd: ''
                                            };
                                        } else {
                                            table = 'tbl_clients';
                                            var ModuleData = {
                                                clients_suspendStart: '',
                                                clients_suspendEnd: ''
                                            };
                                        }
                                    }
                                }
                            }

                            var UserData = {
                                suspendStart: '',
                                suspendEnd: '',
                                status: 1,
                                is_suspend: 0
                            };

                            var UpSuspend = 'UPDATE tbl_user SET ? WHERE ?';
                            db.connection.query(UpSuspend, [UserData, { user_id: UserID }], function(errorUpSuspend, resultUpSuspend, fieldsUpSuspend) {
                                if (errorUpSuspend) {
                                    deferred.reject(new Error(errorUpSuspend));
                                } else {

                                    var ModuleSuspend = "UPDATE " + table + " SET ? WHERE ?";
                                    db.connection.query(ModuleSuspend, [ModuleData, { user_id: UserID }], function(errModuleSuspend, rowsModuleSuspend) {
                                        if (errModuleSuspend) {
                                            deferred.reject(new Error(errModuleSuspend));
                                        } else {
                                            console.log(1);
                                        }
                                        // deferred.resolve(resultModuleSuspend);
                                    });

                                }
                            });
                        });
                    }

                });
            }
        }

        // deferred.resolve(rows);
    });
    return deferred.promise;
});


/*by komal*/
cron.schedule('* * * * *', function() {
    console.log('running a task every minute');
    var deferred = Q.defer();
    if ((new Date().getHours() == 15) && (new Date().getMinutes() == 50)) {

        var currentDateTime = commonHelper.getCurrentDateTime();
        var currentDate = commonHelper.getCurrentDate();
        var tommorrowDate = new Date();
        tommorrowDate.setDate(tommorrowDate.getDate() + 1);
        tommorrowDate = dateFormat(tommorrowDate, "yyyy-mm-dd")
        var query = "select client_id,booking_date,booking_cost from tbl_booking where booking_date='" + tommorrowDate+ "' AND booking_payment_status = 0 AND booking_cost != 0 AND is_deleted = 0 "
        db.connection.query(query, function(err, rows) {
            if (err) {
                console.log(err);
                deferred.reject(new Error(err));
            } else {
                var query = "select client_id, booking_date,booking_cost from tbl_bookweekly where booking_date='" + tommorrowDate + "' AND booking_payment_status = 0 AND booking_cost != 0 AND is_deleted = 0 "
                db.connection.query(query, function(err, rows1) {
                    if (err) {
                        console.log(err);
                        deferred.reject(new Error(err));
                    } else {
                        async.forEach(rows, function(values, callback) {
                            rows1.push(values);
                            callback();
                        }, function(err) {
                            if (err) {
                                throw err;
                            } else {
                                async.forEach(rows1, function(values, callback) {
                                    db.connection.query("select credit_points from tbl_clients where user_id='" + values.client_id + "'", function(err_, rows_) {
                                        if (err_) {
                                            console.log(err_);
                                            deferred.reject(new Error(err_));
                                            callback();
                                        } else {
                                            //  console.log(rows_);
                                            if (rows_[0].credit_points < values.booking_cost) {
                                                //   console.log("1");
                                                Worker.getDeviceToken(values.client_id).then(function(resultDT) {
                                                    console.log(resultDT)
                                                    var deviceToken = resultDT[0].device_token;
                                                    var deviceType = resultDT[0].device_type;
                                                    var msgtitle = 'Payment';
                                                    var msgbody = 'Please add money into wallet';
                                                    var notSendData = { user_id: values.client_id, notification_type: msgtitle, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };
                                                    Worker.addNotificationData(notSendData).then(function(resultAN) {
                                                        Worker.getNotResponse(resultAN).then(function(resultNR) {
                                                            Worker.notifyBadgeCount(values.client_id).then(function(resultBC) {
                                                                var badge = resultBC.length;
                                                                console.log("Hello")
                                                                if (deviceType == 2) {
                                                                    console.log("Android")
                                                                    Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {
                                                                        var response = { "status": 1, "message": "Payment successful" };
                                                                        res.json(response);
                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "Payment successful" };
                                                                        res.json(response);
                                                                    });
                                                                } else if (deviceType == 3) {
                                                                    console.log("IOS")
                                                                    Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {
                                                                        var response = { "status": 1, "message": "Payment successful" };
                                                                        res.json(response);
                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "Payment successful", "notify": resultNR, "badgeCount": badge };
                                                                        res.json(response);
                                                                    });
                                                                } else {
                                                                    var response = { "status": 0, "message": "client not found" };
                                                                    res.json(response);
                                                                }
                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Payment successful" };
                                                                res.json(response);
                                                            });
                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Payment successful" };
                                                            res.json(response);
                                                        });

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Payment successful" };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });
                                                callback();
                                            } else {
                                                callback();
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
                return deferred.promise;
            }
        });
        return deferred.promise;
    }
});


cron.schedule('* * * * *', function() {
    console.log('running a task every second');
    var deferred = Q.defer();
    if ((new Date().getHours() == 12) && (new Date().getMinutes() == 30)) {
        var created_at = dateFormat(new Date(), "yyyy-mm-dd hh:MM:ss");
        var currentDateTime = commonHelper.getCurrentDateTime();
        var currentDate = commonHelper.getCurrentDate();
        var tommorrowDate = new Date();
        tommorrowDate.setDate(tommorrowDate.getDate() + 1);
        tommorrowDate = dateFormat(tommorrowDate, "yyyy-mm-dd")
        var query = "select client_id,booking_date,booking_cost,booking_id,booking_once_weekly from tbl_booking where booking_date='" + tommorrowDate + "' AND booking_payment_status = 0 AND booking_cost != 0 AND is_deleted = 0 "
        db.connection.query(query, function(err, rows) {
            if (err) {
                console.log(err);
                deferred.reject(new Error(err));
            } else {
                var query = "select client_id, booking_date, booking_cost, bookweekly_id as booking_id, booking_once_weekly from tbl_bookweekly where booking_date='" + tommorrowDate + "' AND booking_payment_status = 0 AND booking_cost != 0 AND is_deleted = 0 "
                db.connection.query(query, function(err, rows1) {
                    if (err) {
                        console.log(err);
                        deferred.reject(new Error(err));
                    } else {
                        async.forEach(rows, function(values, callback) {
                            rows1.push(values);
                            callback();
                        }, function(err) {
                            if (err) {
                                throw err;
                            } else {
                                async.forEach(rows1, function(values, callback) {
                                    db.connection.query("select credit_points from tbl_clients where user_id='" + values.client_id + "'", function(err_, rows_) {
                                        if (err_) {
                                            console.log(err_);
                                            deferred.reject(new Error(err_));
                                            callback();
                                        } else {
                                            if (rows_[0].credit_points < values.booking_cost) {
                                                console.log("clients wallet have less balance.");
                                                callback();
                                            } else {
                                                console.log("get data")
                                                var wallet_cost = rows_[0].credit_points - values.booking_cost;
                                                db.connection.query("update tbl_clients set credit_points = '" + wallet_cost + "' where user_id='" + values.client_id + "'", function(err, rows) {
                                                    if (err) {
                                                        deferred.reject(new Error(err));
                                                    }
                                                    deferred.resolve(rows);
                                                    console.log(values);

                                                    db.connection.query(
                                                        // "insert into tbl_wallet (client_id, booking_id,booking_serviceflag,booking_cost,wallet_amount,created_at) values('" + values.client_id + "','" + values.booking_id + "','" + values.booking_once_weekly + "','" + values.booking_cost + "','" + wallet_cost + "','" + created_at + "')"
                                                        "INSERT into tbl_credituse (client_id, booking_id,booking_once_weekly,credituse_creditpoints,created_at,updated_at) values('" + values.client_id + "','" + values.booking_id + "','" + values.booking_once_weekly + "','" + values.booking_cost + "','" + created_at + "','" + created_at + "')"
                                                        , function(err, rows) {
                                                        if (err) {
                                                            deferred.reject(new Error(err));
                                                        }
                                                        deferred.resolve(rows);
                                                        if (values.booking_once_weekly == 0) {
                                                            var query = "update tbl_booking set booking_payment_status=1 where booking_id = '" +values.booking_id + "'"
                                                        } else {
                                                            var query = "update tbl_bookweekly set booking_payment_status=1 where booking_id = '" +values.booking_id + "'"
                                                        }
                                                        db.connection.query(query, function(err, rows) {
                                                            if (err) {
                                                                deferred.reject(new Error(err));
                                                            }
                                                            deferred.resolve(rows);
                                                            console.log("Okay");
                                                            Worker.getDeviceToken(values.client_id).then(function(resultDT) {
                                                                console.log(resultDT)
                                                                var deviceToken = resultDT[0].device_token;
                                                                var deviceType = resultDT[0].device_type;
                                                                var msgtitle = 'Payment';
                                                                var msgbody = 'Please add money into wallet';
                                                                var notSendData = { user_id: values.client_id, notification_type: msgtitle, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };
                                                                Worker.addNotificationData(notSendData).then(function(resultAN) {
                                                                    Worker.getNotResponse(resultAN).then(function(resultNR) {
                                                                        Worker.notifyBadgeCount(values.client_id).then(function(resultBC) {
                                                                            var badge = resultBC.length;
                                                                            console.log("Hello")
                                                                            if (deviceType == 2) {
                                                                                console.log("Android")
                                                                                Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {
                                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                                    res.json(response);
                                                                                }).catch(function(error) {
                                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                                    res.json(response);
                                                                                });
                                                                            } else if (deviceType == 3) {
                                                                                console.log("IOS")
                                                                                Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {
                                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                                    res.json(response);
                                                                                }).catch(function(error) {
                                                                                    var response = { "status": 1, "message": "Payment successful", "notify": resultNR, "badgeCount": badge };
                                                                                    res.json(response);
                                                                                });
                                                                            } else {
                                                                                var response = { "status": 0, "message": "client not found" };
                                                                                res.json(response);
                                                                            }
                                                                        }).catch(function(error) {
                                                                            var response = { "status": 1, "message": "Payment successful" };
                                                                            res.json(response);
                                                                        });
                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "Payment successful" };
                                                                        res.json(response);
                                                                    });

                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                    res.json(response);
                                                                });

                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Payment successful" };
                                                                res.json(response);
                                                            });

                                                        });

                                                    });
                                                    return deferred.promise;
                                                });
                                                return deferred.promise;
                                                callback();
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
                return deferred.promise;
            }

        });
        return deferred.promise;
    }
});

//BY Priya
// Auto reject worker request if assessor does not accept or reject request and date is in past
cron.schedule('* * * * *', function() {
    console.log('auto reject cron running a task every minute');

    var deferred = Q.defer();
    var total = 0;
    var currentDateTime = commonHelper.getCurrentDateTime();
    var currentDate = commonHelper.getCurrentDate();

    var query = "SELECT * FROM `tbl_assessorRequest` as a LEFT JOIN `tbl_workers` as w ON a.worker_id = w.user_id WHERE a.`request_date` < '"+ currentDate +"' AND a.`status` = 0";

    query = mysql.format(query);
    db.connection.query(query, function(err, result, fields) {
        if (err) {
            deferred.reject(new Error(err));
        } else {
            total = result.length;
            if (total > 0) {
                async.forEach(result, function(values, callback) {
                    db.connection.query("Update tbl_assessorRequest SET status = '2' where request_id='" + values.request_id + "'", function(err_, rows_) {
                        
                        if (err_) {
                            // console.log(err_);
                            deferred.reject(new Error(err_));
                            callback();
                        } else {
                            //  console.log(rows_);
                            if (rows_) {
                                
                                var msgtitle = 'Auto Reject';
                                var msgbody = 'The Assessment request for angel "'+values.workers_name+'" is auto rejected.';

                                var notSendData = { user_id: values.cafe_id, notification_type: msgtitle, notification_msg: msgbody, notification_detail_id:values.request_id, created_at: currentDateTime, updated_at: currentDateTime };
                                Worker.addNotificationData(notSendData).then(function(resultAN) {
                                            
                                    var response = { "status": 1, "message": "Request auto rejected" };
                                    res.json(response);
                                        
                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Request auto rejected" };
                                    res.json(response);
                                });

                                callback();
                            } else {
                                callback();
                            }
                        }
                    });
                });
            }
        }

        // deferred.resolve(rows);
    });
    return deferred.promise;
});
/****************************/

module.exports = app;