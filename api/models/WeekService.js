var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var WeekService = {

    addWeekService: function (weekData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_weeklyservice SET ?";
        weekData.week_name = striptags(weekData.week_name);
        
        db.connection.query(query, weekData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getWeekService: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'week_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_weeklyservice WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleWeekService: function (week_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_weeklyservice WHERE ? AND is_deleted = 0";
        db.connection.query(query, { week_id: week_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateWeekService: function (weekData, week_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_weeklyservice SET ? WHERE ?';
        weekData.week_name = striptags(weekData.week_name);
        
        db.connection.query(query, [weekData, { week_id: week_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusWeekService: function(Status, week_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_weeklyservice SET ? WHERE ?';

        db.connection.query(query, [Status, { week_id: week_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteWeekService: function (week_id) {
        var deferred = Q.defer();
        var week_id = week_id;
        var query = "UPDATE tbl_weeklyservice SET is_deleted = 1 WHERE week_id = " + week_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countWeekService: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(week_id) as total FROM tbl_weeklyservice WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Weekly Service found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.week_id !== 'undefined' && searchParams.week_id !== '') {
            where += " AND week_id = " + searchParams.week_id;
        }

        if (typeof searchParams.week_name !== 'undefined' && searchParams.week_name !== '') {
            where += " AND week_name LIKE '%" + striptags(searchParams.week_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    //API
    getAllWeekService: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_weeklyservice WHERE ";
        
        var where = "";

        where += " status = 1 ";
        where += " AND is_deleted = 0 ;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }

};

module.exports = WeekService;