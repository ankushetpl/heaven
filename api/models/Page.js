var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Page = {

    addPage: function (pageData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_pages SET ?";
        pageData.page_name = striptags(pageData.page_name);
        pageData.page_slug = striptags(pageData.page_slug);

        db.connection.query(query, pageData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getPages: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'page_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_pages WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singlePage: function (page_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_pages WHERE ? AND is_deleted = 0";
        db.connection.query(query, { page_id: page_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updatePage: function (pageData, page_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_pages SET ? WHERE ?';
        pageData.page_name = striptags(pageData.page_name);        

        db.connection.query(query, [pageData, { page_id: page_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusPage: function(Status, page_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_pages SET ? WHERE ?';

        db.connection.query(query, [Status, { page_id: page_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deletePage: function (page_id) {
        var deferred = Q.defer();
        var page_id = page_id;
        var query = "UPDATE tbl_pages SET is_deleted = 1 WHERE page_id = " + page_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countPage: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(page_id) as total FROM tbl_pages WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Pages found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.page_id !== 'undefined' && searchParams.page_id !== '') {
            where += " AND page_id = " + searchParams.page_id;
        }

        if (typeof searchParams.page_name !== 'undefined' && searchParams.page_name !== '') {
            where += " AND page_name LIKE '%" + striptags(searchParams.page_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    allPage: function () {
        var deferred = Q.defer();
        var query = "SELECT page_id, page_name, page_slug FROM tbl_pages WHERE page_location = 2 AND status = 1 AND is_deleted = 0";
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }

};

module.exports = Page;