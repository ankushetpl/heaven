var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var CleaningArea = {

    addCleaningArea: function (cleaningAreaData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_cleaningarea SET ?";
        cleaningAreaData.area_name = striptags(cleaningAreaData.area_name);
        
        db.connection.query(query, cleaningAreaData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getCleaningArea: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'area_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT area_id, area_name, area_clean_duration, area_clean_cost, status, created_at FROM tbl_cleaningarea WHERE is_deleted = 0 ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleCleaningArea: function (area_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_cleaningarea WHERE ? AND is_deleted = 0";
        db.connection.query(query, { area_id: area_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateCleaningArea: function (cleaningAreaData, area_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_cleaningarea SET ? WHERE ?';
        cleaningAreaData.area_name = striptags(cleaningAreaData.area_name);
        
        db.connection.query(query, [cleaningAreaData, { area_id: area_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusCleaningArea: function(Status, area_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_cleaningarea SET ? WHERE ?';

        db.connection.query(query, [Status, { area_id: area_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteCleaningArea: function (area_id) {
        var deferred = Q.defer();
        var area_id = area_id;
        var query = "UPDATE tbl_cleaningarea SET is_deleted = 1 WHERE area_id = " + area_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countCleaningArea: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(area_id) as total FROM tbl_cleaningarea WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No cleaning area data found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.area_id !== 'undefined' && searchParams.area_id !== '') {
            where += " AND area_id = " + searchParams.area_id;
        }

        if (typeof searchParams.area_name !== 'undefined' && searchParams.area_name !== '') {
            where += " AND area_name LIKE '%" + striptags(searchParams.area_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = CleaningArea;