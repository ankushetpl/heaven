var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Claim = {

    getClaim: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'claim_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claim WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleClaim: function (claim_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claim WHERE ? AND is_deleted = 0";
        db.connection.query(query, { claim_id: claim_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateClaim: function (resolvedData, claim_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_claim SET ? WHERE ?';
        
        db.connection.query(query, [resolvedData, { claim_id: claim_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusClaim: function(Status, claim_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_claim SET ? WHERE ?';

        db.connection.query(query, [Status, { claim_id: claim_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteClaim: function (claim_id) {
        var deferred = Q.defer();
        var claim_id = claim_id;
        var query = "UPDATE tbl_claim SET is_deleted = 1 WHERE claim_id = " + claim_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countClaim: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(claim_id) as total FROM tbl_claim WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Claim found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.claim_id !== 'undefined' && searchParams.claim_id !== '') {
            where += " AND claim_id = " + searchParams.claim_id;
        }

        if (typeof searchParams.claim_category_id !== 'undefined' && searchParams.claim_category_id !== '') {
            where += " AND claim_category_id = " + searchParams.claim_category_id;
        }

        if (typeof searchParams.claim_booking_id !== 'undefined' && searchParams.claim_booking_id !== '') {
            where += " AND claim_booking_id = " + searchParams.claim_booking_id;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.action_ats !== 'undefined' && searchParams.action_ats !== '') {
            where += " AND action_ats = " + searchParams.action_ats;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    getAllCategory: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claimcategory WHERE is_deleted = 0";
        var where = ''
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }

};

module.exports = Claim;