var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Checklist = {

    addChecklist: function (checklistData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_checklist SET ?";
        db.connection.query(query, checklistData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getChecklist: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'checklist_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_checklist WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleChecklist: function (checklist_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_checklist WHERE ? AND is_deleted = 0";
        db.connection.query(query, { checklist_id: checklist_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateChecklist: function (checklistData, checklist_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_checklist SET ? WHERE ?';
        checklistData.checklist_name = striptags(checklistData.checklist_name);
        
        db.connection.query(query, [checklistData, { checklist_id: checklist_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Status Checklist
    statusChecklist: function(Status, ChecklistId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_checklist SET ? WHERE ?';

        db.connection.query(query, [Status, { checklist_id: ChecklistId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },
    
    deleteChecklist: function (checklist_id) {
        var deferred = Q.defer();
        var checklist_id = checklist_id;
       
        var query = "UPDATE tbl_checklist SET is_deleted = 1 WHERE checklist_id = " + checklist_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countChecklist: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(checklist_id) as total FROM tbl_checklist WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Checklist found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.checklist_id !== 'undefined' && searchParams.checklist_id !== '') {
            where += " AND checklist_id = " + searchParams.checklist_id;
        }

        if (typeof searchParams.checklist_name !== 'undefined' && searchParams.checklist_name !== '') {
            where += " AND checklist_name LIKE '%" + striptags(searchParams.checklist_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }        
 
        return where;
    }

};

module.exports = Checklist;