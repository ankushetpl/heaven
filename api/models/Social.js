var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Social = {

    addSocial: function (socialData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_socialSetting SET ?";
        
        db.connection.query(query, socialData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getSocial: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'social_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_socialSetting WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllSocial: function () {
        var deferred = Q.defer();
        var query = "Select social_id, social_type, social_url from tbl_socialSetting WHERE is_deleted = 0 AND defaults = 1 ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleSocial: function (social_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_socialSetting WHERE is_deleted = 0 AND social_id = "+ social_id +" ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateSocial: function (socialData, social_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_socialSetting SET ? WHERE ?';
        
        db.connection.query(query, [socialData, { social_id: social_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusSocial: function(Status, social_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_socialSetting SET ? WHERE ?';

        db.connection.query(query, [Status, { social_id: social_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    defaultSocial: function(socialData, social_id, socialType) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_socialSetting SET defaults = 0 WHERE social_type = "'+ socialType +'" ';
        
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            
            var querys = "UPDATE tbl_socialSetting SET ? WHERE ?";
                
            db.connection.query(querys, [socialData, { social_id: social_id } ], function(errs, rows) {

                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows);                
            });
        });
        return deferred.promise;
    },    

    deleteSocial: function (social_id) {
        var deferred = Q.defer();
        var social_id = social_id;
        var query = "UPDATE tbl_socialSetting SET is_deleted = 1 WHERE social_id = " + social_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countSocial: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(social_id) as total FROM tbl_socialSetting WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Email content found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.social_id !== 'undefined' && searchParams.social_id !== '') {
            where += " AND social_id = " + searchParams.social_id;
        }

        if (typeof searchParams.social_type !== 'undefined' && searchParams.social_type !== '') {
            where += " AND social_type LIKE '%" + striptags(searchParams.social_type) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        if (typeof searchParams.defaults !== 'undefined' && searchParams.defaults !== '') {
            where += " AND defaults = " + searchParams.defaults;
        }

        return where;
    }

};

module.exports = Social;