var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Feedback = {

    addFeedback: function (feedbackData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_feedback SET ?";
        feedbackData.feedback_note = striptags(feedbackData.feedback_note);
        
        db.connection.query(query, feedbackData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getFeedback: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'feedback_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_feedback WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleFeedback: function (feedback_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_feedback WHERE ? AND is_deleted = 0";
        db.connection.query(query, { feedback_id: feedback_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    deleteFeedback: function (feedback_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_feedback SET is_deleted = 1 WHERE feedback_id = " + feedback_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    deleteFeedbackM: function (IDNew) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_feedback SET is_deleted = 1 WHERE feedback_id IN ("+IDNew+")";
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countFeedback: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(feedback_id) as total FROM tbl_feedback WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Feedback found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.feedback_id !== 'undefined' && searchParams.feedback_id !== '') {
            where += " AND feedback_id = " + searchParams.feedback_id;
        }

        if (typeof searchParams.role_id !== 'undefined' && searchParams.role_id !== '') {
            where += " AND role_id = " + searchParams.role_id;
        }

        if (typeof searchParams.feedback_note !== 'undefined' && searchParams.feedback_note !== '') {
            where += " AND feedback_note LIKE '%" + striptags(searchParams.feedback_note) + "%' ";
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    //API
    getAllFeedback: function (user_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_feedback WHERE ";
        
        var where = "";

        if (typeof user_id !== 'undefined' && user_id !== '') {
            where += " user_id = " + user_id;
        }

        // where += " AND status = 1 ";
        where += " AND is_deleted = 0 ;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

};

module.exports = Feedback;