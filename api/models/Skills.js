var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Skills = {

    addSkills: function (skillsData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_skills SET ?";
        db.connection.query(query, skillsData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getSkills: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'skills_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleSkill: function (skills_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE ? AND is_deleted = 0";
        db.connection.query(query, { skills_id: skills_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateSkills: function (skillsData, skills_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_skills SET ? WHERE ?';
        skillsData.skills_name = striptags(skillsData.skills_name);
        
        db.connection.query(query, [skillsData, { skills_id: skills_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Status Skills
    statusSkills: function(Status, SkillsId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_skills SET ? WHERE ?';

        db.connection.query(query, [Status, { skills_id: SkillsId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteSkills: function (skills_id) {
        var deferred = Q.defer();
        var skills_id = skills_id;
       
        var query = "UPDATE tbl_skills SET is_deleted = 1 WHERE skills_id = " + skills_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countSkills: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(skills_id) as total FROM tbl_skills WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Skills found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.skills_id !== 'undefined' && searchParams.skills_id !== '') {
            where += " AND skills_id = " + searchParams.skills_id;
        }

        if (typeof searchParams.skills_name !== 'undefined' && searchParams.skills_name !== '') {
            where += " AND skills_name LIKE '%" + striptags(searchParams.skills_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        } 

        if (typeof searchParams.parent !== 'undefined' && searchParams.parent !== '') {
            if(searchParams.parent == 0) {
                 where += " AND parent = " + searchParams.parent;
             } else {
                 where += " AND parent != 0";
             }
           
        }       
 
        return where;
    },

    getAllCatList: function (parent) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE ? AND is_deleted = 0";
        db.connection.query(query, { parent: parent }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getParent: function (parent) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE ? AND is_deleted = 0";
        db.connection.query(query, { skills_id: parent }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getCheckData: function (parent) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE ? AND status = 1 AND is_deleted = 0";
        db.connection.query(query, { parent: parent }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }    

};

module.exports = Skills;