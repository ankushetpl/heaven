var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Suburb = {

    addSuburb: function (suburbData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_suburb SET ?";
        // suburbData.suburb_name = striptags(suburbData.suburb_name);
        
        var test = db.connection.query(query, suburbData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getSuburb: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'suburb_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT suburb_id, suburb_name, status, created_at FROM tbl_suburb WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleSuburb: function (suburb_id) {
        var deferred = Q.defer();
        var suburb_id = suburb_id;
        var query = "SELECT * FROM tbl_suburb WHERE ? AND is_deleted = 0";
        db.connection.query(query, { suburb_id: suburb_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateSuburb: function (suburbData, suburb_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_suburb SET ? WHERE ?';
        suburbData.suburb_name = striptags(suburbData.suburb_name);
        
        db.connection.query(query, [suburbData, { suburb_id: suburb_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusSuburb: function(Status, suburb_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_suburb SET ? WHERE ?';

        db.connection.query(query, [Status, { suburb_id: suburb_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    checkExistsSuburb: function (suburb_id) {
        var deferred = Q.defer();
        var suburb_id = suburb_id;
        var query = "SELECT * FROM `tbl_workers` WHERE workers_suburb LIKE '%"+ suburb_id +"%'  ";
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    deleteSuburb: function (suburb_id) {
        var deferred = Q.defer();
        var suburb_id = suburb_id;
        var query = "UPDATE tbl_suburb SET is_deleted = 1 WHERE suburb_id = " + suburb_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },    
    getCity:function(city){
         var deferred = Q.defer();
        var id = city.ID;
        var query = "select * from cities where id="+id+"";

        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
   
            deferred.resolve(results);
        });
        return deferred.promise;

    },

    countSuburb: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(suburb_id) as total FROM tbl_suburb WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Suburb found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.suburb_id !== 'undefined' && searchParams.suburb_id !== '') {
            where += " AND suburb_id = " + searchParams.suburb_id;
        }

        if (typeof searchParams.suburb_name !== 'undefined' && searchParams.suburb_name !== '') {
            where += " AND suburb_name LIKE '%" + striptags(searchParams.suburb_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    checkSuburb: function (checkSuburb){
        var deferred = Q.defer();
        var query = "SELECT COUNT(*) as total, suburb_id FROM tbl_suburb WHERE is_deleted = 0";

        var where = "";

        if (typeof checkSuburb.country !== 'undefined' && checkSuburb.country !== '') {
            where += " AND country = " + checkSuburb.country;
        }

        if (typeof checkSuburb.suburb_name !== 'undefined' && checkSuburb.suburb_name !== '') {
            where += " AND suburb_name LIKE '%" + striptags(checkSuburb.suburb_name) + "%' ";
        }

        if (typeof checkSuburb.state !== 'undefined' && checkSuburb.state !== '') {
            where += " AND state = " + checkSuburb.state;
        }

        if (typeof checkSuburb.city !== 'undefined' && checkSuburb.city !== '') {
            where += " AND city = " + checkSuburb.city;
        }

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    }

};

module.exports = Suburb;