var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Leave = {

    addLeave: function (leaveData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_leaves SET ?";
        leaveData.leaves_name = striptags(leaveData.leaves_name);
        leaveData.leaves_des = striptags(leaveData.leaves_des);
        leaveData.status = striptags(leaveData.status);

        db.connection.query(query, leaveData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getLeave: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'leaves_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT leaves_id, leaves_name, status, created_at FROM tbl_leaves WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleLeave: function (leaves_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_leaves WHERE ? AND is_deleted = 0";
        db.connection.query(query, { leaves_id: leaves_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateLeave: function (leaveData, leaves_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_leaves SET ? WHERE ?';
        leaveData.leaves_name = striptags(leaveData.leaves_name);
        leaveData.leaves_des = striptags(leaveData.leaves_des);

        db.connection.query(query, [leaveData, { leaves_id: leaves_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusLeave: function(Status, leaves_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_leaves SET ? WHERE ?';

        db.connection.query(query, [Status, { leaves_id: leaves_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteLeave: function (leaves_id) {
        var deferred = Q.defer();
        var leaves_id = leaves_id;
        var query = "UPDATE tbl_leaves SET is_deleted = 1 WHERE leaves_id = " + leaves_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countLeave: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(leaves_id) as total FROM tbl_leaves WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Leaves found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.leaves_id !== 'undefined' && searchParams.leaves_id !== '') {
            where += " AND leaves_id = " + searchParams.leaves_id;
        }

        if (typeof searchParams.leaves_name !== 'undefined' && searchParams.leaves_name !== '') {
            where += " AND leaves_name LIKE '%" + striptags(searchParams.leaves_name) + "%' ";
        }

        // if (typeof searchParams.faq_answer !== 'undefined' && searchParams.faq_answer !== '') {
        //     where += " AND faq_answer LIKE '%" + striptags(searchParams.faq_answer) + "%' ";
        // }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Leave;