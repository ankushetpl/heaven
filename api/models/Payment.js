var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");
var commonHelper = require('../helper/common_helper.js');

var Payment = {


    getPayments: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, w.workers_name, c.clients_name FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id WHERE b.is_deleted = 0 AND b.booking_payment_status = '1' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        console.log(query);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    getPaymentsUnsuccess: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, w.workers_name, c.clients_name FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id WHERE b.is_deleted = 0 AND b.booking_payment_status = '1' AND b.job_status = '0' AND b.booking_date < '"+ today +"' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getPaymentsCancel: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, w.workers_name, c.clients_name FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id WHERE  b.booking_payment_status = '1' AND b.is_deleted = '1' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getPaymentsPending: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, w.workers_name, c.clients_name FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id WHERE b.is_deleted = 0 AND b.booking_payment_status = '0' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countPayment: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(booking_id) as total FROM tbl_booking WHERE is_deleted = 0 AND booking_payment_status = '1' ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },


    countPaymentUnsuccess: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(booking_id) as total FROM tbl_booking WHERE is_deleted = 0 AND booking_payment_status = '1' AND  job_status = '0' ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },


    countPaymentCancel: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(booking_id) as total FROM tbl_booking WHERE booking_payment_status = '1' AND   is_deleted = '1' ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },    


    countPaymentPending: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(booking_id) as total FROM tbl_booking WHERE is_deleted = 0 AND booking_payment_status = '0' ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },        


    singlePayment: function (booking_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE ? ";
        db.connection.query(query, { booking_id: booking_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    // ALL FILES DATA
    getImages: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    

    deletePayment: function (booking_id) {
        var deferred = Q.defer();
        var booking_id = booking_id;
        var query = "UPDATE tbl_booking SET is_deleted = 1 WHERE booking_id = " + booking_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },




    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.booking_id !== 'undefined' && searchParams.booking_id !== '') {
            where += " AND booking_id = " + searchParams.booking_id;
        }

        if (typeof searchParams.role_id !== 'undefined' && searchParams.role_id !== '') {
            where += " AND role_id = " + searchParams.role_id;
        }

        if (typeof searchParams.faq_question !== 'undefined' && searchParams.faq_question !== '') {
            where += " AND faq_question LIKE '%" + striptags(searchParams.faq_question) + "%' ";
        }

        if (typeof searchParams.faq_answer !== 'undefined' && searchParams.faq_answer !== '') {
            where += " AND faq_answer LIKE '%" + striptags(searchParams.faq_answer) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }


};

module.exports = Payment;