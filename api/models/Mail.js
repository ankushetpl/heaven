var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Mail = {

    addMail: function (mailData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_emailSetting SET ?";
        
        db.connection.query(query, mailData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getMail: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'email_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_emailSetting WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllMail: function () {
        var deferred = Q.defer();
        var query = "Select email_id, email_type, email_text from tbl_emailSetting WHERE is_deleted = 0 AND defaults = 1 ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleMail: function (email_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_emailSetting WHERE is_deleted = 0 AND email_id = "+ email_id +" ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateMail: function (mailData, email_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_emailSetting SET ? WHERE ?';
        
        db.connection.query(query, [mailData, { email_id: email_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusMail: function(Status, emall_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_emailSetting SET ? WHERE ?';

        db.connection.query(query, [Status, { email_id: emall_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    defaultMail: function(mailData, email_id, emailType) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_emailSetting SET defaults = 0 WHERE email_type = "'+ emailType +'" ';
        
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            
            var querys = "UPDATE tbl_emailSetting SET ? WHERE ?";
                
            db.connection.query(querys, [mailData, { email_id: email_id } ], function(errs, rows) {

                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows);                
            });
        });
        return deferred.promise;
    },    

    deleteMail: function (email_id) {
        var deferred = Q.defer();
        var email_id = email_id;
        var query = "UPDATE tbl_emailSetting SET is_deleted = 1 WHERE email_id = " + email_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countMail: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(email_id) as total FROM tbl_emailSetting WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Email content found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.email_id !== 'undefined' && searchParams.email_id !== '') {
            where += " AND email_id = " + searchParams.email_id;
        }

        if (typeof searchParams.email_type !== 'undefined' && searchParams.email_type !== '') {
            where += " AND email_type LIKE '%" + striptags(searchParams.email_type) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        if (typeof searchParams.defaults !== 'undefined' && searchParams.defaults !== '') {
            where += " AND defaults = " + searchParams.defaults;
        }

        return where;
    }

};

module.exports = Mail;