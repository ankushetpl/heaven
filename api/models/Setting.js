var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Setting = {

    getSetting: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_setting WHERE ";
        var where = "";

        where += " status = 1 ";
        where += " AND is_deleted = 0 ;";
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    updateSetting: function (settingData, setting_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_setting SET ? WHERE ?';
        settingData.setting_value = settingData.setting_value;        

        db.connection.query(query, [settingData, { setting_id: setting_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }

};

module.exports = Setting;