var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Modules = {

    addModules: function (modulesData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_modules SET ?";
        var tect = db.connection.query(query, modulesData, function (error, result, fields) {
            
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getModules: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'module_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_modules WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleModule: function (module_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_modules WHERE ? AND is_deleted = 0";
        db.connection.query(query, { module_id: module_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateModules: function (modulesData, module_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_modules SET ? WHERE ?';
        modulesData.module_name = striptags(modulesData.module_name);
        
        db.connection.query(query, [modulesData, { module_id: module_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusModules: function(data, module_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_modules SET ? WHERE ?';

        db.connection.query(query, [data, { module_id: module_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },
    
    deleteModules: function (module_id) {
        var deferred = Q.defer();
        var module_id = module_id;
       
        var query = "UPDATE tbl_modules SET is_deleted = 1 WHERE module_id = " + module_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countModules: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(module_id) as total FROM tbl_modules WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Skills found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.module_id !== 'undefined' && searchParams.module_id !== '') {
            where += " AND module_id = " + searchParams.module_id;
        }

        if (typeof searchParams.module_name !== 'undefined' && searchParams.module_name !== '') {
            where += " AND module_name LIKE '%" + striptags(searchParams.module_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        } 

        if (typeof searchParams.parent_id !== 'undefined' && searchParams.parent_id !== '') {
            if(searchParams.parent_id == 0) {
                where += " AND parent_id = " + searchParams.parent_id;
            } else {
                where += " AND parent_id != 0";
            }           
        }       
 
        return where;
    },

    getAllParent: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_modules WHERE parent_id = 0 AND is_deleted = 0; ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAll: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_modules WHERE status = 1 AND is_deleted = 0; ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getParent: function (parent_id) {
        var deferred = Q.defer();
        var query = "SELECT module_name FROM tbl_modules WHERE ? AND is_deleted = 0";
        db.connection.query(query, { module_id: parent_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getCheck: function (module_id) {
        var deferred = Q.defer();
        
        var query = "SELECT COUNT(module_id) as total FROM tbl_modules WHERE parent_id = "+ module_id +" AND status = 1 AND is_deleted = 0";    
    
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    }    

};

module.exports = Modules;