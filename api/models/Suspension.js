var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Suspension = {

    addCategory: function (addData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_suspensionCategory SET ?";
        
        db.connection.query(query, addData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getData: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'category_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_suspensionCategory WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleData: function (category_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_suspensionCategory WHERE ? AND is_deleted = 0";
        db.connection.query(query, { category_id: category_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateCategory: function (addData, category_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_suspensionCategory SET ? WHERE ?';
        
        db.connection.query(query, [addData, { category_id: category_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusUpdate: function(Status, category_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_suspensionCategory SET ? WHERE ?';

        db.connection.query(query, [Status, { category_id: category_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteCategory: function (category_id) {
        var deferred = Q.defer();
        var category_id = category_id;
        var query = "UPDATE tbl_suspensionCategory SET is_deleted = 1 WHERE category_id = " + category_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countData: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(category_id) as total FROM tbl_suspensionCategory WHERE is_deleted = 0";
        
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No category data found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.category_id !== 'undefined' && searchParams.category_id !== '') {
            where += " AND category_id = " + searchParams.category_id;
        }

        if (typeof searchParams.category_type !== 'undefined' && searchParams.category_type !== '') {
            where += " AND category_type LIKE '%" + striptags(searchParams.category_type) + "%' ";
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        return where;
    }

};

module.exports = Suspension;