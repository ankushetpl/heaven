var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");
var commonHelper = require('../helper/common_helper.js');

var AllBooking = {

    getBookings: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, w.workers_name, c.clients_name FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id WHERE is_deleted = 0  AND booking_date < '"+ today +"' AND job_status = '1' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getBookingsWeek: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'bookweekly_id', orderBy: 'DESC' }) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT bw.bookweekly_id, bw.booking_date, bw.booking_time_from, w.workers_name, c.clients_name FROM tbl_bookweekly as bw JOIN tbl_workers as w ON bw.worker_id = w.user_id JOIN tbl_clients as c ON bw.client_id = c.user_id WHERE bw.is_deleted = 0  AND bw.booking_date < '"+ today +"' AND bw.job_status = '1' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    singleBooking: function (booking_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE booking_id = '"+ booking_id +"' ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getBookingsOnceOffCSV: function () {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_apartment, b.booking_street, b.booking_city, t.tasktype_name, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets, b.booking_other_work, b.booking_start_time, b.booking_end_time, b.booking_cost, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address  FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id  JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.is_deleted = 0  AND b.booking_date < '"+ today +"' AND b.job_status = '1' ";
        query = mysql.format(query);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getBookingsWeeklyCSV: function () {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT b.bookweekly_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_apartment, b.booking_street, b.booking_city, t.tasktype_name, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets, b.booking_other_work, b.booking_start_time, b.booking_end_time, b.booking_cost, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address  FROM tbl_bookweekly as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id  JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.is_deleted = 0 AND b.booking_date < '"+ today +"' AND b.job_status = '1' ";
        query = mysql.format(query);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    singleBookingWeek: function (bookweekly_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_bookweekly as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE bookweekly_id = '"+ bookweekly_id +"' ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },    

    countBookings: function (searchParams = {}) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT COUNT(booking_id) as total FROM tbl_booking WHERE is_deleted = 0 AND booking_date < '"+ today +"' AND job_status = '1' ";
        // var where = this.returnWhere(searchParams);
        // query += where;
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },

    countBookingsWeek: function (searchParams = {}) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT COUNT(bookweekly_id) as total FROM tbl_bookweekly WHERE is_deleted = 0 AND booking_date < '"+ today +"' AND job_status = '1' ";
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },


    getAmenities: function () {
        var deferred = Q.defer();
        var query = "SELECT amenities_id,amenities_count FROM tbl_amenities WHERE is_deleted = 0 AND status = '1' ";
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No amenities found!"));
            }
        });
        return deferred.promise;
    },


    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.booking_id !== 'undefined' && searchParams.booking_id !== '') {
            where += " AND b.booking_id = " + searchParams.booking_id;
        }

        if (typeof searchParams.bookweekly_id !== 'undefined' && searchParams.bookweekly_id !== '') {
            where += " AND bw.bookweekly_id = " + searchParams.bookweekly_id;
        }

        if (typeof searchParams.clients_name !== 'undefined' && searchParams.clients_name !== '') {
            where += " AND c.clients_name  LIKE '%" + searchParams.clients_name + "%' ";
        }

        if (typeof searchParams.workers_name !== 'undefined' && searchParams.workers_name !== '') {
            where += " AND w.workers_name  LIKE '%" + searchParams.workers_name + "%' ";
        }

        if (typeof searchParams.booking_date !== 'undefined' && searchParams.booking_date !== '') {
            where += " AND DATE_FORMAT(booking_date, '%Y-%m-%d') = '" + searchParams.booking_date + "'";
        }

        return where;
    }


};

module.exports = AllBooking;