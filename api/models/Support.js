var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Support = {

    addContact: function (mailData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_contactUs SET ?";
        
        db.connection.query(query, mailData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getRecords: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'contactUs_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_contactUs WHERE is_deleted = 0 ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleRecord: function (contactUs_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_contactUs WHERE ? ";
        db.connection.query(query, { contactUs_id: contactUs_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateRecord: function (mailData, contactUs_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_contactUs SET ? WHERE ?';
        mailData.contactUs_replyMsg = striptags(mailData.contactUs_replyMsg);

        var test = db.connection.query(query, [mailData, { contactUs_id: contactUs_id }], function (error, result, fields) {
            console.log(test.sql);   
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    countData: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(contactUs_id) as total FROM tbl_contactUs WHERE is_deleted = 0";
        
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Mail data found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.contactUs_id !== 'undefined' && searchParams.contactUs_id !== '') {
            where += " AND contactUs_id = " + searchParams.contactUs_id;
        }

        if (typeof searchParams.contactUs_from !== 'undefined' && searchParams.contactUs_from !== '') {
            where += " AND contactUs_from LIKE '%" + striptags(searchParams.contactUs_from) + "%' ";
        }

        if (typeof searchParams.contactUs_subject !== 'undefined' && searchParams.contactUs_subject !== '') {
            where += " AND contactUs_subject LIKE '%" + striptags(searchParams.contactUs_subject) + "%' ";
        }

        if (typeof searchParams.contactUs_type !== 'undefined' && searchParams.contactUs_type !== '') {
            where += " AND contactUs_type = " + searchParams.contactUs_type;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Support;