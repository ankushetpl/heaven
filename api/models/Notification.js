var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Notification = {

    allWorkers: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE is_suspend = 0 AND is_deleted = 0 AND status = 1 AND user_role_id = 3";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    allClients: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE is_suspend = 0 AND is_deleted = 0 AND status = 1 AND user_role_id = 5";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL Image DATA
    getImages: function(id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE file_id = '"+ id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }    

};

module.exports = Notification;