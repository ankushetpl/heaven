var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var LeaveManage = {

   getLeavesData: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'workerleaves_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_workerleaves as l JOIN tbl_workers as w ON l.worker_id = w.user_id WHERE l.is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleLeave: function (workerleaves_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_workerleaves as l JOIN tbl_workers as w ON l.worker_id = w.user_id LEFT JOIN tbl_leaves ON l.workerleaves_leaves_id = tbl_leaves.leaves_id WHERE l.is_deleted = 0 AND l.workerleaves_id = '"+ workerleaves_id +"' ";
        db.connection.query(query, function (err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getFileUrl: function (file_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE file_id = '"+ file_id +"' ";
        db.connection.query(query, function (err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },    

    countLeaves: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(l.workerleaves_id) as total FROM tbl_workerleaves as l JOIN tbl_workers as w ON l.worker_id = w.user_id WHERE l.is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Leaves found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.workerleaves_id !== 'undefined' && searchParams.workerleaves_id !== '') {
            where += " AND workerleaves_id = " + searchParams.workerleaves_id;
        }

        if (typeof searchParams.worker_id !== 'undefined' && searchParams.worker_id !== '') {
            where += " AND worker_id = " + searchParams.worker_id;
        }

        if (typeof searchParams.workers_name !== 'undefined' && searchParams.workers_name !== '') {
            where += " AND w.workers_name  LIKE '%" + searchParams.workers_name + "%' ";
        }

        if (typeof searchParams.workerleaves_from !== 'undefined' && searchParams.workerleaves_from !== '') {
            where += " AND DATE_FORMAT(workerleaves_from, '%Y-%m-%d') = '" + searchParams.workerleaves_from + "'";
        }

        if (typeof searchParams.workerleaves_leaves_id !== 'undefined' && searchParams.workerleaves_leaves_id !== '') {
            where += " AND workerleaves_leaves_id = " + searchParams.workerleaves_leaves_id;
        }
        
        return where;
    },

    // Leave Reasons
    allLeaveType: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_leaves WHERE status = 1 AND is_deleted = 0 ";        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }

};

module.exports = LeaveManage;