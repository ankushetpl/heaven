var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Tutorial = {

    addTutorial: function (tutorialData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_tutorial SET ?";
        tutorialData.tutorial_name = striptags(tutorialData.tutorial_name);
        
        var test = db.connection.query(query, tutorialData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getTutorial: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'tutorial_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tutorial WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleTutorial: function (tutorial_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tutorial WHERE ? AND is_deleted = 0";
        db.connection.query(query, { tutorial_id: tutorial_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateTutorial: function (tutorialData, tutorial_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tutorial SET ? WHERE ?';
        tutorialData.tutorial_name = striptags(tutorialData.tutorial_name);
        
        db.connection.query(query, [tutorialData, { tutorial_id: tutorial_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusTutorial: function(Status, tutorial_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tutorial SET ? WHERE ?';

        db.connection.query(query, [Status, { tutorial_id: tutorial_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteTutorial: function (tutorial_id) {
        var deferred = Q.defer();
        var tutorial_id = tutorial_id;
        var query = "UPDATE tbl_tutorial SET is_deleted = 1 WHERE tutorial_id = " + tutorial_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countTutorial: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(tutorial_id) as total FROM tbl_tutorial WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Tutorial Found!"));
            }
        });
        return deferred.promise;
    },

    getImageUrl: function (id) {
        var deferred = Q.defer();
        var query = "SELECT file_base_url FROM tbl_files WHERE file_id =  "+id+" ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.tutorial_id !== 'undefined' && searchParams.tutorial_id !== '') {
            where += " AND tutorial_id = " + searchParams.tutorial_id;
        }

        if (typeof searchParams.tutorial_name !== 'undefined' && searchParams.tutorial_name !== '') {
            where += " AND tutorial_name LIKE '%" + striptags(searchParams.tutorial_name) + "%' ";
        }

        if (typeof searchParams.role_id !== 'undefined' && searchParams.role_id !== '') {
            where += " AND role_id = " + searchParams.role_id;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    //API
    getAllTutorial: function (role_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tutorial WHERE ";
        
        var where = "";

        if (typeof role_id !== 'undefined' && role_id !== '') {
            where += " role_id = " + role_id;
        }

        where += " AND status = 1 ";
        where += " AND is_deleted = 0 ;";

        query += where;
        query = mysql.format(query, where);
        // console.log(query);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }

};

module.exports = Tutorial;