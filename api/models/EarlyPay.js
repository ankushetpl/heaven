var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var EarlyPay = {

    getData: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'request_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_earlypayRequest as e JOIN tbl_workers as w ON e.worker_id = w.user_id WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    statusEarlyPay: function(Status, request_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_earlypayRequest SET ? WHERE ?';

        db.connection.query(query, [Status, { request_id: request_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    countData: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(request_id) as total FROM tbl_earlypayRequest as e JOIN tbl_workers as w ON e.worker_id = w.user_id WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";
        
        if (typeof searchParams.request_id !== 'undefined' && searchParams.request_id !== '') {
            where += " AND e.request_id = " + searchParams.request_id;
        }

        if (typeof searchParams.worker_id !== 'undefined' && searchParams.worker_id !== '') {
            where += " AND e.worker_id = " + searchParams.worker_id;
        }

        if (typeof searchParams.workers_name !== 'undefined' && searchParams.workers_name !== '') {
            where += " AND w.workers_name  LIKE '%" + searchParams.workers_name + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND e.status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }
        
        return where;
    },

    getNotAcceptData: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_earlypayRequest as e JOIN tbl_workers as w ON e.worker_id = w.user_id WHERE e.is_deleted = 0 AND e.status = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }, 

    countNotAcceptData: function () {
        var deferred = Q.defer();
        var query = "SELECT COUNT(request_id) as total FROM tbl_earlypayRequest as e JOIN tbl_workers as w ON e.worker_id = w.user_id WHERE e.is_deleted = 0 AND e.status = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No found!"));
            }
        });
        return deferred.promise;
    }

};

module.exports = EarlyPay;