var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var ClaimCategory = {

    addClaimCategory: function (claimCategoryData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_claimcategory SET ?";
        claimCategoryData.category_name = striptags(claimCategoryData.category_name);
        
        db.connection.query(query, claimCategoryData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getClaimCategory: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'category_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claimcategory WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleClaimCategory: function (category_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claimcategory WHERE ? AND is_deleted = 0";
        db.connection.query(query, { category_id: category_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateClaimCategory: function (claimCategoryData, category_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_claimcategory SET ? WHERE ?';
        claimCategoryData.category_name = striptags(claimCategoryData.category_name);

        db.connection.query(query, [claimCategoryData, { category_id: category_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusClaimCategory: function(Status, category_id) {
        var deferred = Q.defer();
        var total = 0;
        var query = "SELECT * FROM tbl_claim WHERE ? AND is_deleted = 0";
        db.connection.query(query, { claim_category_id: category_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            total = total + rows.length;
            if(total == 0){
                var querys = 'UPDATE tbl_claimcategory SET ? WHERE ?';
                db.connection.query(querys, [Status, { category_id: category_id }], function(error, result, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }
                    deferred.resolve(1);
                });
            }else{
                deferred.resolve(0);
            }
        });

        return deferred.promise;
    },

    deleteClaimCategory: function (category_id) {
        var deferred = Q.defer();
        
        var total = 0;
        var query = "SELECT * FROM tbl_claim WHERE ? AND is_deleted = 0";
        db.connection.query(query, { claim_category_id: category_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            total = total + rows.length;
            if(total == 0){
                var querys = "UPDATE tbl_claimcategory SET is_deleted = 1 WHERE category_id = " + category_id;
                db.connection.query(querys, function (error, results, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }
                    deferred.resolve(1);
                });
            }else{
                deferred.resolve(0);
            }
        });
        
        return deferred.promise;
    },

    countClaimCategory: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(category_id) as total FROM tbl_claimcategory WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Claim Category found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.category_id !== 'undefined' && searchParams.category_id !== '') {
            where += " AND category_id = " + searchParams.category_id;
        }

        if (typeof searchParams.category_name !== 'undefined' && searchParams.category_name !== '') {
            where += " AND category_name LIKE '%" + striptags(searchParams.category_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = ClaimCategory;