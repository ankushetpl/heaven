var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");
var dateFormat = require('dateformat');
var nodeUnique = require('node-unique-array');
var async = require('async');
var NodeGeocoder = require('node-geocoder');
var Client = {

    // CHECK USERNAME EXISTS OR NOT
    checkClientExists: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user as u JOIN tbl_clients as c ON u.user_id = c.user_id LEFT JOIN tbl_files as f ON c.clients_image = f.file_id WHERE ";
        var where = "";

        if (typeof client_id != 'undefined' && client_id != '') {
            where += " u.user_id = '" + client_id + "'";
        }

        where += " AND u.status = 1";
        where += " AND u.is_suspend = 0";
        where += " AND u.is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // UPDATE CLIENTS DATA
    updateClient: function(client_id, clientData) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_clients SET ? WHERE ?";

        db.connection.query(query, [clientData, { user_id: striptags(client_id) }], function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET CLIENTS DATA
    getClient: function(client_id) {
        var table = 'tbl_clients';

        var deferred = Q.defer();
        var query = "SELECT u.user_id, u.user_username, u.user_password, u.user_role_id, u.user_registertype, u.device_token, u.device_type, u.social_id, c.clients_name, c.clients_email, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.credit_points,  c.clients_rating as clientRating FROM tbl_user as u  JOIN " + table + " as c ON u.user_id = c.user_id WHERE";
        var where = "";

        if (typeof client_id != 'undefined' && client_id != '') {
            where += " u.user_id = '" + (client_id) + "'";
        }

        where += " AND u.status = 1";
        where += " AND u.is_suspend = 0";
        where += " AND u.is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET CLIENTS AVERAGE RATINGS
    getAvgRating: function(client_id) {

        var deferred = Q.defer();
        var query = "Select sum(booking_client_rating) as totalRating, count(booking_id) as count FROM `tbl_booking` WHERE booking_client_rating != '0' AND client_id = '" + client_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL CLIENTS DATA
    getAllWorkers: function() {
        var table = 'tbl_workers';

        var deferred = Q.defer();
        var query = "SELECT u.user_id, w.workers_name, w.workers_address, w.workers_phone, w.workers_mobile, w.workers_state, w.workers_city, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating  FROM tbl_user as u JOIN " + table + " as w ON u.user_id = w.user_id WHERE u.status = 1 AND u.is_suspend = 0 AND u.is_deleted = 0";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD BOOKING   
    addBooking: function(register, weektype, days, preferred_days, nowdate,finalCostArray) {
        var deferred = Q.defer();
        var count = 0;

        if (weektype == 0) {

            var query = "INSERT INTO tbl_booking SET ?";
            db.connection.query(query, register, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                
                if (result) {
                    deferred.resolve(result.insertId);
                }
            });
            return deferred.promise;

        } else {
            register.booking_date = nowdate;
            var query = "INSERT INTO tbl_booking SET ?";
            db.connection.query(query, register, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                // console.log(test.sql);
                if (result) {

                    arraydays = days.split(',');
                    for (var i = 0; i < arraydays.length; i++) {

                        var querys = "INSERT INTO tbl_bookweekly SET ?";
                        register.bookweekly_prefered_days = preferred_days,
                            register.booking_id = result.insertId;
                        register.booking_date = arraydays[i];
                            if(register.booking_date == finalCostArray[i].date) {
                                register.booking_cost = finalCostArray[i].cost;
                            } 
                            
                        db.connection.query(querys, register, function(err, rows) {
                            if (err) {
                                deferred.reject(new Error(err));
                            }
                            // console.log(week.sql);
                            count = count + 1;
                            if (count == i) {
                                deferred.resolve(result.insertId);
                            }

                        });
                    }
                    return deferred.promise;
                }
            });
            return deferred.promise;

        }

    },


    // UPDATE WORKER FOR BOOKING
    updateBookWorker: function(data, booking_id, client_id, serviceflag) {
        var deferred = Q.defer();

        if (serviceflag == 0) {
            var query = "UPDATE tbl_booking SET ? WHERE ?";

            db.connection.query(query, [data, { booking_id: booking_id }, { client_id: client_id }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
        } else {
            var query = "UPDATE tbl_bookweekly SET ? WHERE ?";

            db.connection.query(query, [data, { bookweekly_id: booking_id }, { client_id: client_id }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
        }

        return deferred.promise;
    },


    // ADD FAVOURITE WORKER 
    addFavourite: function(addFav, flag, client_id, worker_id) {
        var deferred = Q.defer();
        var status = 1;
        var statusZero = 0;
        var client_id = client_id;
        var worker_id = worker_id;
        var favData = { status: status };
        if (flag == 0) {

            var query = "SELECT * FROM tbl_favorite WHERE favorite_client_id = " + client_id + " AND favorite_worker_id = " + worker_id + " ";
            db.connection.query(query, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }

                if (rows == 0) {
                    var query = "INSERT INTO tbl_favorite SET ?";
                    db.connection.query(query, addFav, function(error, result, fields) {
                        if (error) {
                            deferred.reject(new Error(error));
                        }
                        deferred.resolve(result);
                    });
                    return deferred.promise;
                } else {

                    var query = "UPDATE tbl_favorite SET status = " + statusZero + " WHERE favorite_client_id = " + client_id + " AND favorite_worker_id = " + worker_id + " ";
                    db.connection.query(query, function(error, result, fields) {
                        if (error) {
                            deferred.reject(new Error(error));
                        }
                        deferred.resolve(result);
                    });
                    return deferred.promise;

                }

                deferred.resolve(rows);
            });
            return deferred.promise;


        } else {

            var query = "UPDATE tbl_favorite SET status = " + status + " WHERE favorite_client_id = " + client_id + " AND favorite_worker_id = " + worker_id + " ";
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;
        }

    },

    // ALL FAVOURITE WORKERS
    favouriteWorkers: function(client_id) {
        var table = 'tbl_workers';
        var table1 = 'tbl_user';

        var deferred = Q.defer();
        var query = "SELECT f.favorite_worker_id as workers_id, w.workers_name, w.workers_email, w.workers_phone, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, u.user_username, u.user_password, u.user_role_id, u.user_registertype, u.device_token, u.device_type, u.social_id FROM tbl_favorite as f JOIN " + table + " as w ON f.favorite_worker_id = w.user_id  JOIN " + table1 + " as u ON f.favorite_worker_id = u.user_id WHERE u.status = 1 AND u.is_suspend = 0 AND u.is_deleted = 0 AND f.favorite_client_id = " + client_id + " AND f.status = 0 ";

        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET TODAYS AND PAST JOB LIST
    getHistoryJobs: function(client_id, flag, today) {
        var table = 'tbl_files';

        var deferred = Q.defer();
        var query = '';
        if (flag == 0) { // Past jobs

            var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from , b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker, 0 as rateFlag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.client_id = '" + (client_id) + "' AND b.booking_date <  '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND job_status = 1 AND booking_once_weekly = 0 ";

        } else { // 1 => current jobs             
            var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from , b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker, 0 as startJobFlag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.client_id = '" + (client_id) + "' AND b.booking_date = '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND booking_payment_status = 1 AND booking_once_weekly = 0 ";
        }

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET WEEKLY CURRENT JOB LIST
    getHistoryJobsWeekly: function(client_id, today) {

        var deferred = Q.defer();
        var query = '';

        var query = "SELECT b.bookweekly_id, b.bookweekly_prefered_days, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker, 0 as startJobFlag FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.client_id = '" + (client_id) + "' AND b.booking_date = '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND booking_payment_status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET PAST JOB LIST
    getHistoryJobsPastWeekly: function(client_id, today) {

        var deferred = Q.defer();
        var query = '';

        var query = "SELECT b.bookweekly_id, b.bookweekly_prefered_days, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating,  w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker, 0 as startJobFlag FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.client_id = '" + (client_id) + "' AND b.booking_date < '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND booking_payment_status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET PAST JOB LIST WEEKLY
    getHistoryWeeklyDone: function(client_id, booking_id, today) {

        var deferred = Q.defer();
        var query = '';

        var query = "SELECT b.bookweekly_id, b.bookweekly_prefered_days, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating,  w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker, 0 as rateFlag FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.client_id = '" + (client_id) + "' AND b.booking_date <  '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND job_status = 1 AND booking_id = '" + booking_id + "'  ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL FAVOURITE WORKERS
    favouriteWorkerClients: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_favorite WHERE status = 0 AND favorite_client_id = " + client_id + " ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ADD TASKLIST BY CLIENT   
    addTasklist: function(addTask) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_tasklist SET ?";
        db.connection.query(query, addTask, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // DELETE TASK LIST
    deleteTasklist: function(deleteTask, tasklist_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_tasklist SET ? WHERE ?";

        db.connection.query(query, [deleteTask, { tasklist_id: striptags(tasklist_id) }], function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET MY ADDED TASKLIST
    getMyTasklist: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tasklist WHERE tasklist_client_id = '" + (client_id) + "' AND tbl_tasklist.is_deleted = 0 order by created_at DESC ";

        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET ADMIN ADDED & CLINET'S APPROVED TASKLIST
    getTasklist: function() {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_tasklist WHERE is_deleted = 0 AND status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET ADMIN ADDED STANDARD TASKLIST
    getStandardTasklist: function(service_type) {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_tasklist WHERE is_deleted = 0 AND status = 1 AND tasklist_servicetype = '" + service_type + "' AND tasklist_type = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET UPCOMING JOBS
    getUpcomingJobs: function(client_id, today) {

        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating,  w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.client_id = '" + (client_id) + "' AND b.booking_date > '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND booking_once_weekly = 0 ";

        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);

        });
        return deferred.promise;
    },

    // GET UPCOMING JOBS WEEKLY
    getUpcomingJobsWeekly: function(client_id, today) {

        var deferred = Q.defer();
        var query = "SELECT b.bookweekly_id, b.bookweekly_prefered_days, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, w.workers_booking_rating, t.tasktype_name, 0 as flagFavWorker FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.client_id = '" + (client_id) + "' AND b.booking_date > '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 ";

        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);

        });
        return deferred.promise;
    },


    // DELETE BOOKING
/*
    deleteBooking: function(deleteBooking,booking_id,serviceflag) {

        if(serviceflag == 0) {
            var query = "UPDATE tbl_booking SET ? WHERE ?"; 
            db.connection.query(query, [deleteBooking, { booking_id: striptags(booking_id) }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;

        } else {
            var query = "UPDATE tbl_bookweekly SET ? WHERE ?"; 
            db.connection.query(query, [deleteBooking, { bookweekly_id: striptags(booking_id) }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        }*/
      
      

    deleteBooking: function(deleteBooking, booking_id, serviceflag) {
        var deferred = Q.defer();
        
        if(serviceflag == 0) {

        	var query = "select booking_cost,booking_date from tbl_booking where booking_id = '" + booking_id + "'";
           
        } else {
        	var query = "select booking_cost,booking_date from tbl_bookweekly where bookweekly_id = '" + booking_id + "'";
        }
        query = mysql.format(query);
        db.connection.query(query, function(error, result) {
        	if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
            	// console.log(result);
                var today = new Date();
                var tommorrowDate = new Date();
                tommorrowDate.setDate(tommorrowDate.getDate() + 1);
                tommorrowDate = dateFormat(tommorrowDate, "yyyy-mm-dd");
                var currentHour = new Date().getHours();
                var currentMin = new Date().getMinutes();
                var bookDate = dateFormat(result[0].booking_date, "yyyy-mm-dd");

                if(tommorrowDate == bookDate || today == bookDate) {

                    if(tommorrowDate == bookDate && (currentHour > 16 || currentHour <= 24) ) {
                        var deductedCost = result[0].booking_cost * 0.4;
                    } else if(today == bookDate && (currentHour >= 1 || (currentHour <= 5 && currentMin <= 30) ) ) {
                        var deductedCost = result[0].booking_cost * 0.4;

                    } else if(today == bookDate && (currentHour > 5 && currentMin > 30) || (currentHour <= 9) ) {
                        var deductedCost = result[0].booking_cost * 0.6;
                    } else {
                        var deductedCost = 0;
                    }

                    if (serviceflag == 0) {
                        var query = "update tbl_booking set booking_deduct='" + deductedCost + "', is_deleted = 1 where booking_id = '" + booking_id + "' ";
                        db.connection.query(query, function(err, rows) {
                            if (err) {
                                deferred.reject(new Error(err));
                            }
                            deferred.resolve(rows);
                        });
                        return deferred.promise;
                    } else {
                        var query = "update tbl_bookweekly set booking_deduct='" + deductedCost + "', is_deleted = 1 where bookweekly_id = '" + booking_id + "'";
                        db.connection.query(query, function(err, rows) {
                            if (err) {
                                deferred.reject(new Error(err));
                            }
                            deferred.resolve(rows);
                        });
                        return deferred.promise;
                    }
                    

                } else {
                    if (serviceflag == 0) {
                        var query = "UPDATE tbl_booking SET ? WHERE ?";
                        db.connection.query(query, [deleteBooking, { booking_id: striptags(booking_id) }], function(err, rows) {
                            if (err) {
                                deferred.reject(new Error(err));
                            }
                            deferred.resolve(rows);
                        });
                        return deferred.promise;
                    } else {
                        var query = "UPDATE tbl_bookweekly SET ? WHERE ?";
                        db.connection.query(query, [deleteBooking, { bookweekly_id: striptags(booking_id) }], function(err, rows) {
                            if (err) {
                                deferred.reject(new Error(err));
                            }
                            deferred.resolve(rows);
                        });
                        return deferred.promise;
                    }
                }

                return deferred.promise;
            }
            deferred.resolve(result);
        });
        return deferred.promise;
       
    },

    // GET CLAIM CATEGORY ADDED BY ADMIN
    getClaimCategory: function() {

        var deferred = Q.defer();
        var query = "SELECT category_id, category_name FROM tbl_claimcategory WHERE  status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // CHECK CLAIM ALREADY EXISTS OR NOT
    checkClaimExists: function(client_id, booking_id, serviceflag) {

        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claim WHERE claim_client_id = '" + client_id + "' AND claim_booking_id = '" + booking_id + "' AND claim_servicetype = '" + serviceflag + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD CLAIM  
    addClaim: function(addClaim) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_claim SET ?";
        db.connection.query(query, addClaim, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET ALL CITIES LIST
    getAllCitySouthAfrica: function() {

        var deferred = Q.defer();
        var query = "SELECT name, id FROM cities WHERE state_id IN (SELECT id from states WHERE country_id = '202') ORDER BY cities.name";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET TASK TYPE ADDED BY ADMIN
    getTasktype: function() {

        var deferred = Q.defer();
        var query = "SELECT tasktype_id, tasktype_name FROM tbl_tasktype WHERE  status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL AMENITIES DATA
    getAmenitiesTypes: function() {

        var deferred = Q.defer();
        var query = "SELECT amenities_type FROM tbl_amenities WHERE status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL AMENITIES DATA
    getAmenities: function() {

        var deferred = Q.defer();
        var query = "SELECT amenities_id, amenities_type, amenities_count, amenities_clean_duartion, amenities_clean_cost FROM tbl_amenities WHERE status = 1 AND is_deleted = 0  order by amenities_count ASC ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL AMENITIES DATA
    getAmenitiesBaskets: function() {

        var deferred = Q.defer();
        var query = "SELECT amenities_id, amenities_type, amenities_count, amenities_clean_duartion, amenities_clean_cost FROM tbl_amenities WHERE amenities_type = 'Baskets' AND status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL AMENITIES DATA
    getCleaningArea: function() {

        var deferred = Q.defer();
        var query = "SELECT area_id, area_name, area_clean_duration, area_clean_cost FROM tbl_cleaningarea WHERE  status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET ALL JOB LIST
    getAllBooking: function(client_id, today) {
        var deferred = Q.defer();
        var query = '';

        var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating,  w.workers_booking_rating, t.tasktype_name, 0 as claimflag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.client_id = '" + (client_id) + "' AND b.booking_date < '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND job_status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET ALL WEEKLY JOB LIST
    getAllWeeklyBooking: function(client_id, today) {
        var deferred = Q.defer();
        var query = '';

        var query = "SELECT b.bookweekly_id as booking_id, b.bookweekly_prefered_days, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as claimflag FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.client_id = '" + (client_id) + "' AND b.booking_date < '" + (today) + "' AND b.status = 1 AND b.is_deleted = 0 AND job_status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL CLAIMED BOOKING
    claimClientBooking: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_claim WHERE is_deleted = 0 AND claim_client_id = " + client_id + " ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL SKILLS LIST
    getMainSkills: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL SKILLS LIST
    getWorkerSkillsAndAge: function(worker_id) {
        var deferred = Q.defer();

        var query = "SELECT workers_age, workers_skills FROM tbl_workers WHERE user_id = '" + worker_id + "' ";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL SKILLS LIST OF WORKER
    getWorkerSkillsForFavWorker: function(skillsArray) {
        var deferred = Q.defer();
        var unique_array = new nodeUnique();
        var total = 0;

        var arraySkills = skillsArray.split(',');

        for (var i = 0; i < arraySkills.length; i++) {

            var query = "SELECT * FROM tbl_skills WHERE skills_id = '" + arraySkills[i] + "' ";
            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }

                total = total + 1;
                unique_array.add(rows);
                if (total == arraySkills.length) {
                    deferred.resolve(unique_array.get());
                }

            });
        }

        return deferred.promise;
    },

    // ALL SKILLS LIST
    getChildSkills: function(mainskill) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE status = 1 AND is_deleted = 0 AND parent = " + mainskill + " ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD INSTRUCTIONS  
    instruction: function(addInstru, flag) {
        var deferred = Q.defer();
        var instru = { instruction1: addInstru.instruction1, instruction2: addInstru.instruction2, instruction3: addInstru.instruction3 };
        var client_id = addInstru.client_id;

        if (flag == 0) {
            var query = "INSERT INTO tbl_instruction SET ?";
            db.connection.query(query, addInstru, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        } else {
            var query = "UPDATE tbl_instruction SET ? WHERE ?";

            db.connection.query(query, [instru, { client_id: striptags(client_id) }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        }

    },


    // GET INSTRUCTIONS DATA
    getInstruction: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT instruction_id, instruction1, instruction2, instruction3 FROM tbl_instruction WHERE  client_id = " + client_id + " ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET PROMOS DATA
    getpromos: function(currentDate, weektype) {
        if(weektype == '0') {
            var search = '1';
        } else {
            var search = '0';
        } 
        var deferred = Q.defer();
        var query = "SELECT p.promo_id, p.promo_name, p.promo_des, p.promo_code, p.promo_discount, p.promo_banner, p.promo_startdate, p.promo_enddate FROM tbl_promocode as p WHERE p.promo_enddate > '" + currentDate + "' AND p.is_deleted = 0  AND p.status = 1 AND promocode_weektype != '" + search + "' order by p.promo_id DESC  ";

        query = mysql.format(query);
        console.log(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET ALL PROMOS DATA
    getAllPromos: function(currentDate) {
        var deferred = Q.defer();
        var query = "SELECT p.promo_id, p.promo_name, p.promo_des, p.promo_code, p.promo_discount, p.promo_banner, p.promo_startdate, p.promo_enddate FROM tbl_promocode as p WHERE p.promo_enddate > '" + currentDate + "' AND p.is_deleted = 0  AND p.status = 1 order by p.promo_id DESC  ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET USED OFFERS BY CLIENT
    getOffersUsed: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT credituse_promodiscount FROM tbl_credituse WHERE client_id = '" + client_id + "' AND credituse_promodiscount != 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET JOB DETAILS
    getJobDetails: function(booking_id, serviceflag) {
        var deferred = Q.defer();
        if (serviceflag == 0) {

            var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_cost, b.booking_work_time, b. booking_suburb, b.booking_age_min, b.booking_age_max, c.clients_name, c.clients_phone, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, t.tasktype_id FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.is_deleted = 0 AND b.booking_id = '" + booking_id + "' ";

        } else {

            var query = "SELECT b.bookweekly_id, b.bookweekly_prefered_days, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status,  b.booking_once_weekly, b.booking_cost, b.booking_work_time, b. booking_suburb, b.booking_age_min, b.booking_age_max, c.clients_name, c.clients_phone, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, t.tasktype_id FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.is_deleted = 0 AND b.bookweekly_id = '" + booking_id + "' ";

        }

        query = mysql.format(query);
        // console.log(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL AMENITIES
    getAmenitiesData: function() {
        var deferred = Q.defer();
        var query = "SELECT amenities_id, amenities_count, amenities_clean_duartion FROM tbl_amenities WHERE is_deleted = 0 AND status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL CLEANING AREA DATA
    getCleaningData: function() {
        var deferred = Q.defer();
        var query = "SELECT area_id, area_name, area_clean_duration FROM tbl_cleaningarea WHERE is_deleted = 0 AND status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL SKILLS DATA
    getSkillsData: function() {
        var deferred = Q.defer();
        var query = "SELECT skills_id, skills_name FROM tbl_skills WHERE is_deleted = 0 AND status = 1 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL CUSTOM TASK DATA
    getCustomTask: function() {
        var deferred = Q.defer();
        var query = "SELECT tasklist_id, tasklist_name, tasklist_des, tasklist_duration FROM tbl_tasklist WHERE is_deleted = 0 AND status = 1 AND tasklist_type = 1";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL STANDARD TASK DATA
    getStandardTask: function() {
        var deferred = Q.defer();
        var query = "SELECT tasklist_id, tasklist_name, tasklist_des, tasklist_duration FROM tbl_tasklist WHERE is_deleted = 0 AND status = 1 AND tasklist_type = 0";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL FILES DATA
    getImages: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL BOOKING LIST DATA FROM PAST
    getHistoryJobsRate: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, b.booking_cost, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, w.workers_booking_rating, f.file_base_url, t.tasktype_name, 0 as flagFavWorker, 0 as rateFlag, 1 as renewFlag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_files as f ON w.workers_image = f.file_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.client_id = '" + (client_id) + "' AND b.status = 1 AND b.is_deleted = 0 AND b.job_status = '1' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL BOOKING LIST WEEKLY DATA FROM PAST
    getHistoryJobsWeeklyRate: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from, b.booking_time_to, b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, b.booking_cost, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, w.workers_booking_rating, f.file_base_url, t.tasktype_name, 0 as flagFavWorker, 0 as rateFlag, 0 as renewFlag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_files as f ON w.workers_image = f.file_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.client_id = '" + (client_id) + "' AND b.status = 1 AND b.is_deleted = 0 AND b.booking_id IN (SELECT booking_id from tbl_bookweekly as bw WHERE bw.job_status = '1' AND bw.client_id = '" + (client_id) + "' AND bw.is_deleted = 0 ) ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL CREDIT POINTS AND REFERRAL CODE
    getReferralData: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT credit_points, clients_referID FROM tbl_clients WHERE user_id = " + client_id + " ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL REFERRAL COUNT
    getTotalReferrals: function(refer_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_clients WHERE refer_by_client = '" + refer_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL REFERRAL COUNT
    getCreditHistory: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT referralcredit_id as credit_history, referralcredit_type as credit_type, referralcredit_points as credit_points, created_at FROM tbl_referralcredit WHERE client_id = '" + client_id + "' order by created_at DESC ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL CREDIT USERD HISTORY
    getCreditUsedByClient: function(client_id) {
        var deferred = Q.defer();
        var query = "SELECT credituse_id as credit_history, booking_id as credit_type, credituse_creditpoints as credit_points, created_at FROM tbl_credituse WHERE client_id = '" + client_id + "'  order by created_at DESC ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // BOOKING DATA
    getBookingInfo: function(booking_id, client_id, serviceflag) {
        var deferred = Q.defer();

        if (serviceflag == 0) {

            var query = "SELECT * FROM tbl_booking WHERE client_id = '" + client_id + "' AND booking_id = '" + booking_id + "' ";

        } else {
            var query = "SELECT * FROM tbl_bookweekly WHERE client_id = '" + client_id + "' AND bookweekly_id = '" + booking_id + "' ";
        }

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ANGEL LIST SKILLS FILTER DATA
    getAngelList: function(agemin, agemax, suburb, skills) {
        var deferred = Q.defer();
        var query = "SELECT w.user_id, w.workers_name, w.workers_phone, w.workers_address, w.workers_mobile, w.workers_image, w.workers_lat, w.workers_long, w.workers_suburb, w.workers_friendlyDogs, w.workers_friendlyCats, w.worker_type, w.worker_type, w.workers_rating, w.workers_booking_rating, 0 as favWorker FROM tbl_workers as w JOIN tbl_user as u ON u.user_id = w.user_id  WHERE u.status =1 AND u.is_suspend = 0 AND u.is_deleted = 0 AND w.workers_age  BETWEEN '" + agemin + "' AND '" + agemax + "' AND FIND_IN_SET('" + suburb + "', w.workers_suburb ) AND w.workers_skills IN(" + skills + " )  order by workers_rating DESC ";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);

        });
        return deferred.promise;
    },


    // GET LEAVES DATA
    checkLeaveDate: function(dateBook, worker_id) {
        var deferred = Q.defer();
        var query = "SELECT worker_id, workerleaves_from FROM tbl_workerleaves WHERE is_deleted = 0 AND workerleaves_from = '" + dateBook + "' AND worker_id = '" + worker_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET BOOKING DATA
    checkBookDate: function(dateBook, booking_time_from, booking_time_to) {
        var deferred = Q.defer();
        var query = "SELECT worker_id FROM tbl_booking WHERE booking_date = '" + dateBook + "' AND `booking_time_from` IN(SELECT booking_time_from from tbl_booking where booking_time_from >= '" + booking_time_from + "' OR booking_time_from <= '" + booking_time_to + "') and `booking_time_to` IN (SELECT booking_time_to from tbl_booking where booking_time_to >= '" + booking_time_from + "' OR booking_time_to <= '" + booking_time_to + "') ";


        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET RATING LIST
    getRatinglist: function(service_type) {
        var deferred = Q.defer();
        var query = "SELECT rating_id, rating_name FROM tbl_rating WHERE is_deleted = 0 AND status = 1 AND rating_type = '" + service_type + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET DEFAULT RATING LIST
    getDefaultRatinglist: function() {
        var deferred = Q.defer();
        var query = "SELECT rating_id, rating_name FROM tbl_rating WHERE is_deleted = 0 AND status = 1 AND rating_type = '0' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET WORKER 
    getWorker: function(booking_id, serviceflag) {
        var deferred = Q.defer();
        if (serviceflag == 0) {

            var query = "SELECT * FROM tbl_booking b JOIN tbl_workers w ON b.worker_id = w.user_id WHERE b.is_deleted = 0 AND b.booking_id = '" + booking_id + "' ";
        } else {
            var query = "SELECT * FROM tbl_bookweekly b JOIN tbl_workers w ON b.worker_id = w.user_id WHERE b.is_deleted = 0 AND b.bookweekly_id = '" + booking_id + "' ";
        }
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;

    },


    // ADD RATING TO WORKER
    giveWorkersRating: function(booking_id, client_id, worker_id, arrayId, arrayRatings, currentDate) {
        var deferred = Q.defer();

        for (var i = 0; i < arrayId.length; i++) {

            var query = "INSERT INTO tbl_workerrating SET ?";
            var ratingData = { booking_id: booking_id, client_id: client_id, worker_id: worker_id, rating_id: arrayId[i], ratings: arrayRatings[i], created_at: currentDate, updated_at: currentDate };

            db.connection.query(query, ratingData, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }

                deferred.resolve(rows);
            });

        }
        return deferred.promise;
    },


    // UPDATE AVERAGE RATING OF WORKER IN BOOKING
    averageRating: function(booking_id, client_id, worker_id, avg, serviceflag) {
        var deferred = Q.defer();
        if (serviceflag == 0) {
            var query = "UPDATE tbl_booking SET ? WHERE ?";

            var avgRate = { booking_worker_rating: avg };
            db.connection.query(query, [avgRate, { booking_id: booking_id }, { client_id: client_id }, { worker_id: worker_id }], function(err, rows) {
                // console.log(test.sql);
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        } else {
            var query = "UPDATE tbl_bookweekly SET ? WHERE ?";

            var avgRate = { booking_worker_rating: avg };
            db.connection.query(query, [avgRate, { bookweekly_id: booking_id }, { client_id: client_id }, { worker_id: worker_id }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        }
    },


    // GET ALL DONE BOOKING JOB
    getAllDoneJobsBooking: function(worker_id) {

        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking WHERE worker_id = '" + worker_id + "' AND booking_worker_rating != '' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET ALL DONE BOOKING WEEKLY
    getAllDoneJobsWeekly: function(worker_id) {

        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_bookweekly WHERE worker_id = '" + worker_id + "' AND booking_worker_rating != '' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // UPDATE CLIENTS RATING
    loginAvgRating: function(worker_id, totalAvg) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_workers SET workers_booking_rating = '" + totalAvg + "' WHERE  user_id = '" + worker_id + "' ";
        db.connection.query(query, function(err, rows) {
            if (err) {

                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ADD COUPONS   
    addPromoCoupon: function(couponData, payBook, booking_id, client_id, serviceflag) {
        var deferred = Q.defer();

        var query = "INSERT INTO tbl_credituse SET ?";
        db.connection.query(query, couponData, function(error, result, fields) {

            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {

                if (serviceflag == 0) {

                    var querys = "UPDATE tbl_booking SET ? WHERE ? ";
                    db.connection.query(querys, [payBook, { booking_id: booking_id }, { client_id: client_id }], function(err, rows) {

                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(rows);
                    });
                } else {
                    var querys = "UPDATE tbl_bookweekly SET ? WHERE ? ";
                    db.connection.query(querys, [payBook, { bookweekly_id: booking_id }, { client_id: client_id }], function(err, rows) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(rows);
                    });
                }

            }
        });
        return deferred.promise;

    },


    // UPDATE CLIENTS CREDIT AFTER USERD
    updateClientCredit: function(remainCredit, client_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_clients SET credit_points = '" + remainCredit + "' WHERE user_id = '" + client_id + "' ";
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // CHECK PAYMENT STATUS 
    checkPaymentStatus: function(client_id, reference_id) {

        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_payment WHERE client_id = '" + client_id + "' AND payment_reference = '" + reference_id + "' AND payment_status = '1' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // SEND NOTIFICATION  CLIENT ANDROID
    sendNotificationClient: function(deviceToken, msgtitle, msgbody, badge, data) {

        var deferred = Q.defer();
        var FCM = require('fcm-push');
        var serverKey = 'AIzaSyDZvhkAtY0OFV_Lu96Zif9j0uWEVzlmDlw'; // server key
        var fcm = new FCM(serverKey);
        var message = {
            to: deviceToken,
            notification: {
                badge: badge,
                user_id: data.user_id,
                notification_id: data.notification_id,
                notification_type: data.notification_type,
                notification_detail_id: data.notification_detail_id,
                notification_detail_serviceflag: data.notification_detail_serviceflag,
                notification_msg: data.notification_msg,
                notification_date: dateFormat(data.created_at, "yyyy-mm-dd H:i:s")
            }
        };

        fcm.send(message, function(err, response) {
            if (err) {
                deferred.reject(new Error(err));
            }

            deferred.resolve(response);
        });
        return deferred.promise;
    },


    // SEND NOTIFICATION  CLIENT IOS
    sendIOSNotification: function(deviceTokenC, msgtitleC, msgbodyC, badge, data) {

        var deferred = Q.defer();
        var apn = require("apn");
        var options = {
            cert: 'notification/cer.pem',
            key: 'notification/key.pem',
            passphrase: '1234HeavenSent'
        };
        var note = new apn.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600;
        note.badge = badge;
        note.sound = "ping.aiff";
        note.alert = {
            title: msgtitleC,
            body: msgbodyC,
            user_id: data.user_id,
            notification_id: data.notification_id,
            notification_type: data.notification_type,
            notification_detail_id: data.notification_detail_id,
            notification_detail_serviceflag: data.notification_detail_serviceflag,
            notification_msg: data.notification_msg,
            notification_date: dateFormat(data.created_at, "yyyy-mm-dd H:m:s")
        };
        // note.data = data;
        // note.data = { "attachment-url": "https://pusher.com/static_logos/320x320.png" }
        // note.payload = { 'messageFrom': 'Priya' };

        var deviceToken = deviceTokenC;
        var apnProvider = new apn.Provider(options);
        apnProvider.send(note, deviceToken).then((result) => {
            deferred.resolve(result);
        });
        return deferred.promise;
    },


    getAllWeeklyJobs: function(booking_id) {
        var deferred = Q.defer();
        var query = "SELECT count(bookweekly_id) as total FROM tbl_bookweekly WHERE booking_id = '" + booking_id + "' AND is_deleted = '0' ";

        query = mysql.format(query);
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(err));
            }
            if (result) {

                var query = "SELECT count(bookweekly_id) as totalS,bookweekly_prefered_days FROM tbl_bookweekly WHERE booking_id = '" + booking_id + "' AND is_deleted = '0' AND job_status = 1 ";
                query = mysql.format(query);
                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    if (rows) {
                        if (result[0].total == rows[0].totalS) {
                            var renewFlag = 1;
                        } else {
                            var renewFlag = 0;
                        }
                        var data = { "renewFlag": renewFlag, "prefered_days": rows[0].bookweekly_prefered_days }
                    }
                    deferred.resolve(data);
                });
            }

        });
        return deferred.promise;
    },


    getWeekDays: function(booking_id) {
        var deferred = Q.defer();
        var query = "SELECT bookweekly_prefered_days FROM tbl_bookweekly WHERE booking_id = '" + booking_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(err));
            }
            if (result) {
                deferred.resolve(result[0]);
            }
        });
        return deferred.promise;
    },


    getDatesRate: function(startdate, enddate) {
        const datesBetween = require('dates-between');
        var deferred = Q.defer();

        var allDates = [];
        const startDate = new Date(startdate);
        const endDate = new Date(enddate);

        for (const date of datesBetween(startDate, endDate)) {
            if (date.getDay() === 0) {
                allDates.push(date);
            }
            deferred.resolve(allDates);
        }

        return deferred.promise;
    },


    getDatesWeek: function(client_id, booking_id) {
        var deferred = Q.defer();
        var query = "SELECT booking_date FROM tbl_bookweekly WHERE client_id = '" + client_id + "' AND booking_id = '" + booking_id + "' AND is_deleted = '0' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    getBulkDoneJobRate: function(startdate, dates, client_id, booking_id) {
        var deferred = Q.defer();
        var unique_array = new nodeUnique();
        var finalArray = [];
        var total = 0;

        for (var i = 0; i < dates.length; i++) {

            if (i == 0) {

                var query = "SELECT " + (i + 1) + " AS week ,b.booking_id, b.bookweekly_id, b.booking_date, b.booking_start_time, b.booking_end_time,  b.booking_once_weekly, b.booking_worker_rating, b.booking_client_rating, b.worker_id, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, 0 as rateFlag FROM tbl_bookweekly as b JOIN tbl_workers as w ON w.user_id = b.worker_id WHERE b.client_id = '" + client_id + "' AND b.booking_id = '" + booking_id + "' AND b.job_status = '1' AND b.booking_date BETWEEN '" + startdate + "' AND '" + dateFormat(dates[i], "yyyy-mm-dd") + "' AND b.is_deleted = '0' ";

            } else {

                var query = "SELECT " + (i + 1) + " AS week ,b.booking_id, b.bookweekly_id, b.booking_date, b.booking_start_time, b.booking_end_time,  b.booking_once_weekly, b.booking_worker_rating, b.booking_client_rating, b.worker_id, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, 0 as rateFlag FROM tbl_bookweekly as b JOIN tbl_workers as w ON w.user_id = b.worker_id  WHERE b.client_id = '" + client_id + "' AND b.booking_id = '" + booking_id + "' AND b.job_status = '1' AND b.booking_date BETWEEN '" + dateFormat(dates[i - 1], "yyyy-mm-dd") + "' AND '" + dateFormat(dates[i], "yyyy-mm-dd") + "' AND b.is_deleted = '0' ";
            }

            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }

                total = total + 1;
                if (rows.length > 0) {
                    unique_array.add(rows);
                }

                if (total == dates.length) {
                    deferred.resolve(unique_array.get());
                }

            });

        }
        return deferred.promise;
    },



    getBulkDoneJobRateWithoutSat: function(startdate, dates, client_id, booking_id) {
        var deferred = Q.defer();

        var query = "SELECT 0 AS week ,b.booking_id, b.bookweekly_id, b.booking_date, b.booking_start_time, b.booking_end_time,  b.booking_once_weekly, b.booking_worker_rating, b.booking_client_rating, b.worker_id,  w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, 0 as rateFlag FROM tbl_bookweekly as b JOIN tbl_workers as w ON w.user_id = b.worker_id WHERE b.client_id = '" + client_id + "' AND b.booking_id = '" + booking_id + "' AND b.job_status = '1' AND b.booking_date BETWEEN '" + startdate + "' AND '" + dateFormat(dates, "yyyy-mm-dd") + "' AND b.is_deleted = '0' ";

        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    getPaymentData: function(client_id) {
        var deferred = Q.defer();

        var query = "SELECT *, 0 as booking_date, 0 as service_type FROM tbl_payment WHERE client_id = '" + client_id + "' order by created_at DESC";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    getBookDataPaymentOnce: function(client_id) {
        var deferred = Q.defer();

        var query = "SELECT b.booking_id, b.booking_date, t.tasktype_name FROM tbl_booking as b JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.is_deleted = '0' AND b.client_id = '" + client_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    getBookDataPaymentWeekly: function(client_id) {
        var deferred = Q.defer();

        var query = "SELECT b.bookweekly_id, b.booking_date, t.tasktype_name FROM tbl_bookweekly as b JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE b.is_deleted = '0' AND b.client_id = '" + client_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // UPDATE DEVICE TOKEN
    updateDeviceToken: function(user_id, device_token, device_type) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_user SET device_token = '" + device_token + "' WHERE user_id = '" + user_id + "' AND device_type = '" + device_type + "' ";
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET BOOKING DATA FOR WORKER
    checkWorkerAvailableBook: function(date, from_time, to_time, worker_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking WHERE booking_date = '" + date + "' AND booking_payment_status = 1 AND `booking_time_from` IN(SELECT booking_time_from from tbl_booking where booking_time_from >= '" + from_time + "' OR booking_time_from <= '" + to_time + "') and `booking_time_to` IN (SELECT booking_time_to from tbl_booking where booking_time_to >= '" + from_time + "' OR booking_time_to <= '" + to_time + "') AND worker_id = '" + worker_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

                                                                                             
    // GET BOOKING DATA FOR WORKER
    checkWorkerAvailableBookWeekly: function(date, from_time, to_time, worker_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_bookweekly WHERE booking_date = '" + date + "' AND booking_payment_status = 1 AND `booking_time_from` IN(SELECT booking_time_from from tbl_bookweekly where booking_time_from >= '" + from_time + "' OR booking_time_from <= '" + to_time + "') and `booking_time_to` IN (SELECT booking_time_to from tbl_bookweekly where booking_time_to >= '" + from_time + "' OR booking_time_to <= '" + to_time + "') AND worker_id = '" + worker_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET DEVICE TOKEN OF ALL USERS FOR NOTIFICATION
    getDeviceTokenAllUsers: function() {
        var deferred = Q.defer();
        var query = "Select * FROM tbl_user WHERE status= '1' AND is_suspend = '0' AND is_deleted = '0' AND user_role_id = 5 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // SEND MAIL TO ADMIN
    sendSupportMail: function(mailData) {
        var deferred = Q.defer();

        var query = "INSERT INTO tbl_contactUs SET ?";
        db.connection.query(query, mailData, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;

    },


    // CHECK WORKER BOOKING DATA
    CheckWorkerAvailabilityDates: function(worker_id, bookingDate, serviceflag, booking_id) {
        var deferred = Q.defer();

        if (serviceflag == 0) {

            var query = "Select * FROM tbl_booking WHERE booking_date= '" + bookingDate + "' AND worker_id = '" + worker_id + "' AND booking_id != '" + booking_id + "' ";

            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {

                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;

        } else {

            var query = "Select * FROM tbl_bookweekly WHERE booking_date= '" + bookingDate + "' AND worker_id = '" + worker_id + "' AND bookweekly_id != '" + booking_id + "' ";

            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {
                // console.log(query);
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        }
    },



    // CHECK WORKER BOOKING DATA ONCE OFF BOOKINGS
    checkWorkerServiceOnce: function(worker_id, booking_date) {
        var deferred = Q.defer();

        var query = "Select * FROM tbl_booking WHERE booking_date= '" + booking_date + "' AND worker_id = '" + worker_id + "' AND booking_once_weekly = '0' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // CHECK WORKER BOOKING DATA WEEKLY OFF BOOKINGS
    checkWorkerServiceWeekly: function(worker_id, booking_date) {
        var deferred = Q.defer();

        var query = "Select * FROM tbl_bookweekly WHERE booking_date= '" + booking_date + "' AND worker_id = '" + worker_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;

    },


    // GET ONCE OR WEEKLY BOOKING DATA
    getBookingOnceWeek: function(worker_id, client_id, booking_id, serviceflag) {
        var deferred = Q.defer();

        if (serviceflag == 0) {

            var query = "Select * FROM tbl_booking WHERE client_id= '" + client_id + "' AND worker_id = '" + worker_id + "' AND booking_id = '" + booking_id + "' ";

            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {

                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;

        } else {

            var query = "Select * FROM tbl_bookweekly WHERE client_id= '" + client_id + "' AND worker_id = '" + worker_id + "' AND bookweekly_id = '" + booking_id + "' ";

            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        }

    },


    // UPDATE RESCHEDULE DATA
    updateRescheduleData: function(updateRe, booking_id, serviceflag) {
        var deferred = Q.defer();

        if (serviceflag == 0) {

            var query = "UPDATE tbl_booking SET ? WHERE ? ";

            query = mysql.format(query);
            db.connection.query(query, [updateRe, { booking_id: booking_id }], function(err, rows) {

                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;

        } else {

            var query = "UPDATE tbl_bookweekly SET ? WHERE ? ";

            query = mysql.format(query);
            db.connection.query(query, [updateRe, { bookweekly_id: booking_id }], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;
        }
    },


    // GET ALL DATA OF SERVICE
    getServiceAllData: function(booking_id, serviceflag, client_id) {
        var deferred = Q.defer();

        var query = "Select * FROM tbl_booking WHERE booking_id= '" + booking_id + "' AND client_id = '" + client_id + "' And booking_once_weekly = '" + serviceflag + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            // console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;

    },


    // GET PREFERD DAYS DATA
    getPreferdDaysData: function(booking_id) {
        var deferred = Q.defer();

        var query = "Select bookweekly_prefered_days FROM tbl_bookweekly WHERE booking_id= '" + booking_id + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;

    },


    // GET REFERRAL AND RATING POINTS
    getRefAndRatePoints: function(parameter) {
        var deferred = Q.defer();
        var query = "Select setting_value FROM tbl_setting WHERE setting_parameter= '" + parameter + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            // console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },


    // GET FIRST WEEKLY ID FOR PAYMENT
    getWeekBookId: function(mainBookId) {
        var deferred = Q.defer();
        var query = "Select bookweekly_id FROM tbl_bookweekly WHERE booking_id = '" + mainBookId + "' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },


    // GET CREDIT POINTS LIST
    getBuyCreditList: function() {
        var deferred = Q.defer();
        var query = "Select * FROM tbl_buyCredit WHERE status = 1 AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },



    // GET POINT PER RAND
    getPointPerRand: function() {
        var deferred = Q.defer();
        var query = "Select setting_value FROM tbl_setting WHERE setting_id = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },     

    /*by komal*/

     getNormalEFT: function() {
        var deferred = Q.defer();
        var query = "Select * FROM tbl_pages WHERE page_id = 10";
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }
    /**********************/

};

module.exports = Client;