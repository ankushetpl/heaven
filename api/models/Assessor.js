var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");
var nodeUnique = require('node-unique-array');
var moment = require('moment');
var dateFormat = require('dateformat');

var Assessor = {

    // GET ALL CHECKLIST
    getAllChecklist: function() {
        var deferred = Q.defer();
        var query = "SELECT checklist_id, checklist_name FROM tbl_checklist WHERE is_deleted = 0 and status = 1 ";
        
        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET ALL NOTIFICATIONS DATA
    getAllNotification: function(assesor_id, currentDate, lastDate) {
        var deferred = Q.defer();
        var query = "SELECT *, n.created_at as notification_date FROM tbl_notification as n JOIN tbl_assessorRequest as r ON n.notification_detail_id = r.request_id WHERE n.is_deleted = 0 AND n.user_id = '"+ assesor_id +"' AND n.created_at <= '"+currentDate+"' AND n.created_at >= '"+lastDate+"' ORDER BY notification_id DESC ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET ALL NOTIFICATIONS COUNT
    countNot: function(assesor_id, currentDate, lastDate) {
        var deferred = Q.defer();
        var query = "SELECT count(notification_id) as total FROM tbl_notification as n JOIN tbl_assessorRequest as r ON n.notification_detail_id = r.request_id WHERE n.is_deleted = 0 AND n.notification_read = 0 AND n.user_id = '"+ assesor_id +"' AND n.created_at <= '"+currentDate+"' AND n.created_at >= '"+lastDate+"'  ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // CLEAR ALL NOTIFICATIONS 
    deleteAll: function(user_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_notification SET is_deleted = 1 WHERE user_id = '"+ user_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    

    // GET ALL REQUEST DATA
    countAllRequests: function(searchParams, user_id) {
        var deferred = Q.defer();
        var query = "SELECT count(*) as total  FROM tbl_assessorRequest as a JOIN tbl_workers as w ON a.worker_id = w.user_id JOIN tbl_cafeusers as f ON w.cafeuser_id = f.user_id JOIN cities as c ON w.workers_city = c.id JOIN states as s ON w.workers_state = s.id WHERE a.is_deleted = 0 AND (a.status = 0 OR a.status = 1) AND a.assessor_id = '"+user_id+"' ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET ALL REQUEST DATA
    getAllRequests: function(limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'request_id', orderBy: 'DESC' }, user_id) {
        var deferred = Q.defer();
        var query = "SELECT *, c.name as cityName, s.name as stateName, a.worker_id as workerId  FROM tbl_assessorRequest as a JOIN tbl_workers as w ON a.worker_id = w.user_id JOIN tbl_cafeusers as f ON w.cafeuser_id = f.user_id JOIN cities as c ON w.workers_city = c.id JOIN states as s ON w.workers_state = s.id WHERE a.is_deleted = 0 AND (a.status = 0 OR a.status = 1) AND a.assessor_id = '"+user_id+"' ";
        
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}, user_id) {
        var where = "";

        if (typeof searchParams.name !== 'undefined' && searchParams.name !== '') {
            where += " AND ( w.workers_phone = '" + searchParams.name + "' ";
            where += " OR  w.workers_mobile = '" + searchParams.name + "' ";
            where += " OR w.workers_name LIKE '%" + striptags(searchParams.name) + "%' ";
            where += " OR f.cafeusers_name LIKE '%" + striptags(searchParams.name) + "%' )";
        }

        if (typeof searchParams.city !== 'undefined' && searchParams.city !== '') {
            where += " AND c.name LIKE '%" + striptags(searchParams.city) + "%' ";
        }
        
        if (typeof searchParams.state !== 'undefined' && searchParams.state !== '') {
            where += " AND s.name LIKE '%" + striptags(searchParams.state) + "%' ";
        }

        if (typeof searchParams.startDate !== 'undefined' && searchParams.startDate !== '' && typeof searchParams.endDate !== 'undefined' && searchParams.endDate !== '') {
            
            where += " AND ( DATE_FORMAT(request_date, '%Y-%m-%d') >= '" + searchParams.startDate + "' AND DATE_FORMAT(request_date, '%Y-%m-%d') <= '" + searchParams.endDate + "') ";
        

        }else{
            if (typeof searchParams.startDate !== 'undefined' && searchParams.startDate !== '') {
                where += " AND DATE_FORMAT(request_date, '%Y-%m-%d') >= '" + searchParams.startDate + "'";
            }

            if (typeof searchParams.endDate !== 'undefined' && searchParams.endDate !== '') {
                where += " AND DATE_FORMAT(request_date, '%Y-%m-%d') <= '" + searchParams.endDate + "'";
            }
        }     

        return where;
    },

    //REJECT WORKER REQUEST FOR ASSESSMENT
    rejectWorkerRequest: function(request_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_assessorRequest SET status = 2 WHERE request_id = '"+ request_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            if(rows) { 
                var query = "SELECT w.workers_name FROM tbl_assessorRequest as a JOIN tbl_workers as w ON a.worker_id = w.user_id WHERE request_id = '"+ request_id +"' ";
        
                query = mysql.format(query);
                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows[0]);
                });
            }
        });
        return deferred.promise;
    },

    //ACCEPT WORKER REQUEST FOR ASSESSMENT
    acceptWorkerRequest: function(request_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_assessorRequest SET status = 1 WHERE request_id = '"+ request_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            if(rows) { 
                var query = "SELECT a.request_date, a.request_timeFrom, a.request_timeTo, w.workers_name FROM tbl_assessorRequest as a JOIN tbl_workers as w ON a.worker_id = w.user_id WHERE request_id = '"+ request_id +"' ";
        
                query = mysql.format(query);
                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows[0]);
                });
            }
           
        });
        return deferred.promise;
    }, 
    
    //GET OTHER WORKER REQUEST FOR SAME SLOT
    GetOtherRequest: function(request_id,reDate,reFromTime,reToTime,assId) {
        var deferred = Q.defer();
        var query = "SELECT a.* , w.workers_name FROM tbl_assessorRequest as a LEFT JOIN tbl_workers as w ON a.worker_id = w.user_id WHERE a.request_id != '"+ request_id +"' AND a.assessor_id = '"+ assId +"' AND a.request_date = '"+ reDate +"' AND a.request_timeFrom = '"+ reFromTime +"' AND a.request_timeTo = '"+ reToTime +"' ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            if(rows) { 
                deferred.resolve(rows);
            }
        });
        return deferred.promise;

    },    

    //AUTO REJECT OTHER WORKER REQUEST FOR SAME SLOT
    AutoReject: function(request_id, notData) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_assessorRequest SET status = 2 WHERE request_id = '"+ request_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            if(rows) { 

                var querys = "INSERT INTO tbl_notification SET ?";
                querys = mysql.format(querys);
                var test = db.connection.query(querys, notData, function(error, result, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }
                    if (result) {
                        deferred.resolve(result.insertId);
                    }
                });
            }
        });
        return deferred.promise;
    },

    //READ NOTIFICATION
    readNotification: function(request_id) {
        var deferred = Q.defer();
 
        var query = "UPDATE tbl_notification SET notification_read = 1 WHERE notification_type = 'Internet Cafe Assessment Request' AND notification_detail_id = '"+ request_id +"' ";
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    //GET REQUEST DATA 
    getRequestData: function(request_id) {
        var deferred = Q.defer();
        var query = "SELECT r.cafe_id, r.assessor_id, r.request_assessLevel, r.status, r.request_id as requestId, r.request_date, r.request_timeFrom, r.request_timeTo, w.workers_name, a.assessors_name FROM tbl_assessorRequest as r JOIN tbl_workers as w ON r.worker_id = w.user_id JOIN tbl_assessors as a ON r.assessor_id = a.user_id WHERE request_id = "+request_id+" ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },   

    //CHECK BOOKING DATA PRESENT ON SAME TIME OR NOT WITH ID
    CheckRequestBookedExists: function(reDate, reFromTime, reToTime, request_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_assessorRequest WHERE request_date = '"+ reDate +"' AND request_timeFrom = '"+ reFromTime +"' AND request_timeTo ='"+ reToTime +"' AND request_id != "+request_id+" AND status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //CHECK LEAVE DATA ON THE DATE
    CheckLeaveExists: function(reDate, assId) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_assessorLeave WHERE assessorLeave_date = '"+ reDate +"' AND assessor_id = '"+ assId +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },            

    // ADD NOTIFICATION DATA
    addNotificationData: function(notData) {
        var deferred = Q.defer();
        
        var query = "INSERT INTO tbl_notification SET ?";
        query = mysql.format(query);
        var test = db.connection.query(query, notData, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
          
        return deferred.promise;
    },           

    //GET WORKER'S PROFILE DATA
    getWorkerProfileData: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user JOIN tbl_workers on tbl_user.user_id = tbl_workers.user_id JOIN tbl_assessorRequest as a ON a.worker_id = tbl_workers.user_id WHERE tbl_user.user_id = "+worker_id+" AND tbl_user.is_deleted = 0 AND a.skills = '' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },


    //GET SKILLS NAME
    getSkillsName: function(skill) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE skills_id = "+skill+" ";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);    
        });
            
        return deferred.promise;
    }, 

    //GET ALL MY ADDED WORKER'S
    getAllMyWorker: function(limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'a.request_id', orderBy: 'DESC' }, assessor_id) {
        var deferred = Q.defer();
        var query = "SELECT *, c.name as cityName, s.name as stateName, a.worker_id as workerId FROM tbl_user JOIN tbl_workers on tbl_user.user_id = tbl_workers.user_id JOIN tbl_cafeusers as f ON tbl_workers.cafeuser_id = f.user_id JOIN tbl_assessorRequest as a ON a.worker_id = tbl_workers.user_id JOIN cities as c ON tbl_workers.workers_city = c.id JOIN states as s ON tbl_workers.workers_state = s.id WHERE a.assessor_id = "+assessor_id+" AND ( a.status != 2 AND a.status != 0 )  ";

        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhereW(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //GET COUNT ALL MY ADDED WORKER'S
    countAllMyWorker: function(searchParams, assessor_id) {
        var deferred = Q.defer();
        var query = "SELECT count(*) as total FROM tbl_user JOIN tbl_workers on tbl_user.user_id = tbl_workers.user_id JOIN tbl_cafeusers as f ON tbl_workers.cafeuser_id = f.user_id JOIN tbl_assessorRequest as a ON a.worker_id = tbl_workers.user_id JOIN cities as c ON tbl_workers.workers_city = c.id JOIN states as s ON tbl_workers.workers_state = s.id WHERE a.assessor_id = "+assessor_id+" AND ( a.status != 2 AND a.status != 0 )  ";


        var where = this.returnWhereW(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    


    returnWhereW: function (searchParams = {}, assessor_id) {
        var where = "";

        if (typeof searchParams.name !== 'undefined' && searchParams.name !== '') {
            where += " AND ( tbl_workers.workers_phone = '" + searchParams.name + "' ";
            where += " OR  tbl_workers.workers_mobile = '" + searchParams.name + "' ";
            where += " OR tbl_workers.workers_name LIKE '%" + striptags(searchParams.name) + "%' ";
            where += " OR f.cafeusers_name LIKE '%" + striptags(searchParams.name) + "%' )";
        }

        if (typeof searchParams.city !== 'undefined' && searchParams.city !== '') {
            where += " AND c.name LIKE '%" + striptags(searchParams.city) + "%' ";
        }
        
        if (typeof searchParams.state !== 'undefined' && searchParams.state !== '') {
            where += " AND s.name LIKE '%" + striptags(searchParams.state) + "%' ";
        }

        if (typeof searchParams.startDate !== 'undefined' && searchParams.startDate !== '' && typeof searchParams.endDate !== 'undefined' && searchParams.endDate !== '') {
            
            where += " AND ( DATE_FORMAT(request_date, '%Y-%m-%d') >= '" + searchParams.startDate + "' AND DATE_FORMAT(request_date, '%Y-%m-%d') <= '" + searchParams.endDate + "') ";
        

        }else{
            if (typeof searchParams.startDate !== 'undefined' && searchParams.startDate !== '') {
                where += " AND DATE_FORMAT(request_date, '%Y-%m-%d') >= '" + searchParams.startDate + "'";
            }

            if (typeof searchParams.endDate !== 'undefined' && searchParams.endDate !== '') {
                where += " AND DATE_FORMAT(request_date, '%Y-%m-%d') <= '" + searchParams.endDate + "'";
            }
        }  

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND a.status = " + searchParams.status;
        }

        if (typeof searchParams.level !== 'undefined' && searchParams.level !== '') {
            where += " AND a.request_assessLevel = " + searchParams.level;
        }   

        return where;
    },


    //GET ALL ASSESSORS
    getAllAssessors: function(limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'assessors_id', orderBy: 'DESC' }, userID, country) {
        var deferred = Q.defer();
        var query = "SELECT *, c.name as cityName, s.name as stateName, u.user_id as assessorId FROM tbl_user as u JOIN tbl_assessors as a on u.user_id = a.user_id JOIN cities as c ON a.assessors_city = c.id JOIN states as s ON a.assessors_state = s.id WHERE u.is_deleted = 0 AND u.is_suspend = 0 AND u.status = 1 AND u.user_passCount = 0 AND u.user_id != '"+userID+"' AND assessors_country = "+ country +" ";
        // console.log(query);
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhereA(searchParams);
        query += where;
        // query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //GET COUNT ALL ASSESSORS
    countAllAssessors: function(searchParams, userID, country) {
        var deferred = Q.defer();
        var query = "SELECT count(*) as total FROM tbl_user as u JOIN tbl_assessors as a on u.user_id = a.user_id JOIN cities as c ON a.assessors_city = c.id JOIN states as s ON a.assessors_state = s.id WHERE u.is_deleted = 0 AND u.is_suspend = 0 AND u.status = 1 AND u.user_passCount = 0 AND u.user_id != '"+userID+"' AND assessors_country = "+ country +" ";
        
        var where = this.returnWhereA(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    returnWhereA: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.city !== 'undefined' && searchParams.city !== '') {
            where += " AND c.id  = " + searchParams.city + " ";
        }
        
        if (typeof searchParams.state !== 'undefined' && searchParams.state !== '') {
            where += " AND s.id  = " + searchParams.state + " ";
        }

        // if (typeof searchParams.startDate !== 'undefined' && searchParams.startDate !== '') {
        //     where += " AND ( DATE_FORMAT(a.request_date, '%Y-%m-%d') != '" + searchParams.startDate + "') ";
        // }    
               
        return where;
    },

    //GET ASSESSORS CALENDER 
    getBookingRequestData: function(user_id) {
        var deferred = Q.defer();
        var unique_array = new nodeUnique();
        var query = "SELECT request_date as start FROM tbl_assessorRequest WHERE assessor_id = "+user_id +" AND is_deleted = 0 AND status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            unique_array.add(rows);
            deferred.resolve(unique_array.get());
        });
        return deferred.promise;
    },

    //GET ASSESSORS LEAVES CALENDER 
    getLeavesData: function(user_id) {
        var deferred = Q.defer();
        var query = "SELECT assessorLeave_date as start FROM tbl_assessorLeave WHERE assessor_id = "+user_id +" AND is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    


     //ALL ADMIN ADDED SKILLS LIST
    getSkillSet: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE is_deleted = 0 AND status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

     //CHECK NEW SKILLS EXISTS
    checkSkillExists: function(skillName) {
        var deferred = Q.defer();
            var query = "SELECT * FROM tbl_skills WHERE skills_name = '"+skillName+"' ";
            
            query = mysql.format(query);
            db.connection.query(query, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            }); 
        return deferred.promise;
    },  

     //ADD NEW SKILLS LIST
    saveSkill: function(addNew) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_skills SET ?";
        query = mysql.format(query);
        db.connection.query(query, addNew, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if(result) {
                var query = "SELECT * FROM tbl_skills WHERE skills_id = '"+result.insertId+"' ";
            
                query = mysql.format(query);
                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows);
                }); 
            }
        });
        return deferred.promise;
    },  

    // ADD RATING TO WORKER
    giveWorkersRating: function(assessor_id,assessmentLevel,worker_id,arrayId,arrayRatings,currentDate) {
        var deferred = Q.defer();
 
        for(var i=0; i < arrayId.length; i++) {
  
        var query = "INSERT INTO tbl_workerrating SET ?";
        var ratingData = { assessor_id : assessor_id, assessmentLevel : assessmentLevel, worker_id : worker_id, rating_id : arrayId[i], ratings : arrayRatings[i], created_at : currentDate, updated_at : currentDate};
            
            db.connection.query(query, ratingData, function(err, rows) {
                if (err) {
                        deferred.reject(new Error(err));
                    }
                    
                    deferred.resolve(rows);
                });
            
        }    
        return deferred.promise;
    },    


    // ADD RATING TO WORKER
    AverageRatingAssessment: function(assessor_id,assessmentLevel,worker_id,rating_id,avg,currentDate,todayDate) {
        var deferred = Q.defer();

            if(avg >= 8) {

                var query = "UPDATE tbl_assessorRequest SET status = 3, ratings = '"+ avg +"', skills = '"+ rating_id +"', updated_at = '"+ currentDate +"' WHERE assessor_id = '"+ assessor_id +"' AND request_assessLevel = '"+ assessmentLevel +"' AND worker_id = '"+ worker_id +"' ";
        
                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows);
                });
                return deferred.promise;

            } else {

                var query = "UPDATE tbl_assessorRequest SET status = 4, ratings = '"+ avg +"', skills = '"+ rating_id +"', updated_at = '"+ currentDate +"' WHERE assessor_id = '"+ assessor_id +"' AND request_assessLevel = '"+ assessmentLevel +"' AND worker_id = '"+ worker_id +"' ";
            
                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    if(rows) {
                            var end = moment(todayDate).add(6, 'M');
                            var suspendEnd = dateFormat(end, "yyyy-mm-dd");
                        var querys = "UPDATE  tbl_user SET suspendStart = '"+ todayDate +"', suspendEnd = '"+ suspendEnd +"', is_suspend = 1, updated_at = '"+ currentDate +"' WHERE user_id = '"+ worker_id +"' ";
            
                        db.connection.query(querys, function(err, rows) {
                            if (err) {
                                deferred.reject(new Error(err));
                            }
                            deferred.resolve(rows);
                        });
                    }
                });
                return deferred.promise;
            }
 
        
    },

    //ACTIVE WORKER ACCOUNT AT FINAL ASSESSMENT RESULT
    ActiveWorkerProfile: function(worker_id, avg, rating_id, currentDate) {
        var deferred = Q.defer();

        var query = "UPDATE tbl_user SET status = 1, updated_at = '"+ currentDate +"' WHERE user_id = '"+ worker_id +"' ";
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            } 
            if(rows) {
                var query = "UPDATE tbl_workers SET workers_rating = '"+ avg +"', workers_skills = '"+ rating_id +"' WHERE user_id = '"+ worker_id +"' ";

                db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    } 
                    deferred.resolve(rows);
                });
            }
        });
        return deferred.promise;
    },  


    //GET REQUEST DATA BY WOKRER
    getRequestDataByWorker: function(assessor_id,worker_id,assessmentLevel) {
        var deferred = Q.defer();
        var query = "SELECT r.cafe_id, r.request_id, w.workers_name FROM tbl_assessorRequest as r JOIN tbl_workers as w ON r.worker_id = w.user_id WHERE r.assessor_id = "+assessor_id+" AND r.worker_id = "+worker_id+" AND r.request_assessLevel = "+assessmentLevel+" ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },  


     //GET CAFE USER BY WORKER
    getCafeByWorker: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT cafeuser_id, workers_name FROM tbl_workers WHERE user_id = '"+ worker_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

     //GET ASSESSOR DATA BY ID
    getAssesssorName: function(assessorId) {
        var deferred = Q.defer();
        var query = "SELECT assessors_id, assessors_name FROM tbl_assessors WHERE user_id = '"+ assessorId +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

     //CHECK REQUEST ALREADY PERSENT OR NOT
    CheckRequestBooked: function(assessor_id, request_date, start_time, end_time) {
        var deferred = Q.defer();

        var query = "SELECT count(*) as total from tbl_assessorRequest WHERE assessor_id = '"+ assessor_id +"' AND request_date = '"+ request_date +"' AND request_timeFrom = '"+ start_time +"' AND request_timeTo = '"+ end_time +"' AND status = 1  ";
        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result[0]);
            }
        });
        return deferred.promise;   
    },

     //GET CAFE USER BY WORKER
    finalAssessmentRequest: function(finalData, worker_id) {
        var deferred = Q.defer();

        var query = "INSERT INTO tbl_assessorRequest SET ?";
        db.connection.query(query, finalData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {

                var query = "UPDATE tbl_assessorRequest SET assessorRequest_next = 1 WHERE worker_id = '"+ worker_id +"' AND request_assessLevel = 0 ";
                var test = db.connection.query(query, function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(result.insertId);
                });
            }
        });
        return deferred.promise;   
    },    

    //GET ASSESSOR'S ALL REQUEST FOR PARTICULAR DATE
    getAllRequestForDate: function(user_id, searchDate) {
        var deferred = Q.defer();
        var query = "SELECT a.request_id, a.worker_id, a.request_assessLevel, a.request_date, a.request_timeFrom, a.request_timeTo, w.workers_name, w.workers_mobile, w.workers_phone, f.file_base_url FROM tbl_assessorRequest as a JOIN tbl_workers as w ON a.worker_id = w.user_id JOIN tbl_files as f ON w.workers_image = f.file_id WHERE a.assessor_id = '"+ user_id +"' AND a.request_date = '"+ searchDate +"' AND a.status = 1 AND a.is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //CHECK ASSESSOR UNAVAILABILITY DATE
    checkUnavailability: function(user_id, searchDate) {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_assessorLeave WHERE assessor_id = '"+ user_id +"' AND assessorLeave_date = '"+ searchDate +"' AND is_deleted = 0 ";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
        
    },    

    //UPDATE ASSESSOR'S AVAILABILITY
    UpdateAvailability: function(leaveData, user_id, type, selectedDate) {
        var deferred = Q.defer();
        
            if(type == 1) {

                var query = "INSERT INTO tbl_assessorLeave SET ?";
                db.connection.query(query, leaveData, function (error, result, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }
                    if (result) {
                        deferred.resolve(result.insertId);
                    }
                });
                return deferred.promise; 

            } else {
                var query = "UPDATE tbl_assessorLeave SET ? WHERE assessor_id = '"+ user_id +"' AND assessorLeave_date = '"+ selectedDate +"' ";
                var test = db.connection.query(query, [leaveData], function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows);
                });
                return deferred.promise;
            }
        
    },

    //RESCHEDULE REQUEST 
    rescheduleRequest: function(rescheduleData, request_id) {
        var deferred = Q.defer();
 
        var query = "UPDATE tbl_assessorRequest SET ? WHERE ? ";
        db.connection.query(query, [rescheduleData, { request_id: request_id }], function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    //GET ALL PROVINCE FOR SOUTH AFRICE
    getAllProvince: function(userID, userRoleID) {
        var deferred = Q.defer();        
         
        if(userRoleID == 2){
            var querys = "SELECT cafeusers_country FROM tbl_cafeusers WHERE user_id = "+ userID +"";

            db.connection.query(querys, function (error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                if (result) {
                    
                    var query = "SELECT id as stateId, name as stateName FROM states WHERE country_id = "+result[0].cafeusers_country+" order by stateName ASC ";
                    query = mysql.format(query);
                    db.connection.query(query, function(err, rows) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(rows);
                    });
                }
            });
        }else{
            var querys = "SELECT assessors_country FROM tbl_assessors WHERE user_id = "+ userID +"";

            db.connection.query(querys, function (error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                if (result) {
                    
                    var query = "SELECT id as stateId, name as stateName FROM states WHERE country_id = "+result[0].assessors_country+" order by stateName ASC ";
                    query = mysql.format(query);
                    db.connection.query(query, function(err, rows) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(rows);
                    });
                }
            });
        }
        return deferred.promise; 
    },

    //GET ALL CITIES BY STATE ID
    getAllCitiesById: function(stateId) {
        var deferred = Q.defer();
        var query = "SELECT id as cityId, name as cityName FROM cities WHERE state_id = "+ stateId +" order by cityName ASC ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //GET SLOT PERIOD SET BY ADMIN
    getSlotPeriod: function() {
        var deferred = Q.defer();
        var query = "SELECT setting_value FROM tbl_setting WHERE setting_id = 7 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },                                            


    //GET ASSESSOR'S ALL REQUEST FOR PARTICULAR DATE
    getAllRequestCalenderDate: function(user_id, searchDate) {
        var deferred = Q.defer();
        var query = "SELECT r.request_timeFrom, r.request_timeTo FROM tbl_assessorRequest as r JOIN tbl_assessors as a ON r.assessor_id = a.user_id WHERE r.assessor_id = '"+ user_id +"' AND r.request_date = '"+ searchDate +"' AND r.is_deleted = 0 AND r.status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //GET WORKER'S PROFILE by id
    getWorkerData: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user JOIN tbl_workers on tbl_user.user_id = tbl_workers.user_id WHERE tbl_user.user_id = "+worker_id+" AND tbl_user.is_deleted = 0";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }



};

module.exports = Assessor;