var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Banner = {

    addBanner: function (bannerData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_bannerSetting SET ?";
        bannerData.banner_title = striptags(bannerData.banner_title);
        bannerData.banner_description = striptags(bannerData.banner_description);

        db.connection.query(query, bannerData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getBanner: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'banner_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_bannerSetting WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    

    singleBanner: function (banner_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_bannerSetting WHERE is_deleted = 0 AND banner_id = "+ banner_id +" ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateBanner: function (bannerData, banner_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_bannerSetting SET ? WHERE ?';
        bannerData.banner_title = striptags(bannerData.banner_title);
        bannerData.banner_description = striptags(bannerData.banner_description);

        db.connection.query(query, [bannerData, { banner_id: banner_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusBanner: function(Status, banner_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_bannerSetting SET ? WHERE ?';

        db.connection.query(query, [Status, { banner_id: banner_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteBanner: function (banner_id) {
        var deferred = Q.defer();
        var banner_id = banner_id;
        var query = "UPDATE tbl_bannerSetting SET is_deleted = 1 WHERE banner_id = " + banner_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countBanner: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(banner_id) as total FROM tbl_bannerSetting WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No banner content found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.banner_id !== 'undefined' && searchParams.banner_id !== '') {
            where += " AND banner_id = " + searchParams.banner_id;
        }

        if (typeof searchParams.banner_title !== 'undefined' && searchParams.banner_title !== '') {
            where += " AND banner_title LIKE '%" + striptags(searchParams.banner_title) + "%' ";
        }

        if (typeof searchParams.banner_location !== 'undefined' && searchParams.banner_location !== '') {
            where += " AND banner_location LIKE '%" + striptags(searchParams.banner_location) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }
    
};

module.exports = Banner;