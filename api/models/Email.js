var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Email = {

    addEmail: function (emailData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_email SET ?";
        emailData.email_content = striptags(emailData.email_content);

        db.connection.query(query, emailData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getEmails: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'email_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_email WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getEmailtypes: function () {
        var deferred = Q.defer();
        var query = "Select * from tbl_emailtypes WHERE emailtypes_id NOT IN ( Select email_type from tbl_email WHERE is_deleted = 0) ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    singleEmail: function (email_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_email WHERE is_deleted = 0 AND email_id = "+ email_id +" ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateEmail: function (emailData, email_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_email SET ? WHERE ?';
        emailData.email_type = striptags(emailData.email_type);
        emailData.email_content = emailData.email_content;

        db.connection.query(query, [emailData, { email_id: email_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusEmail: function(Status, email_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_email SET ? WHERE ?';

        db.connection.query(query, [Status, { email_id: email_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    defaultEmail: function(emailData, email_id, emailType) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_email SET defaults = 0 WHERE email_type = "'+ emailType +'" ';
        
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            
            var querys = "UPDATE tbl_email SET ? WHERE ?";
                
            db.connection.query(querys, [emailData, { email_id: email_id } ], function(errs, rows) {

                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows);                
            });
        });
        return deferred.promise;
    }, 

    checkTypeExistsDefault: function(emailType) {
        var deferred = Q.defer();
        var query = 'SELECT * FROM tbl_email WHERE is_deleted = 0 AND email_type = "'+ emailType +'" AND defaults = 1 ';

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }, 


    deleteEmail: function (email_id) {
        var deferred = Q.defer();
        var email_id = email_id;
        var query = "UPDATE tbl_email SET is_deleted = 1 WHERE email_id = " + email_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countEmail: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(email_id) as total FROM tbl_email WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Email content found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.email_id !== 'undefined' && searchParams.email_id !== '') {
            where += " AND email_id = " + searchParams.email_id;
        }

        if (typeof searchParams.email_type !== 'undefined' && searchParams.email_type !== '') {
            where += " AND email_type LIKE '%" + striptags(searchParams.email_type) + "%' ";
        }

        if (typeof searchParams.default !== 'undefined' && searchParams.default !== '') {
            where += " AND defaults = " + searchParams.default;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }
    
};

module.exports = Email;