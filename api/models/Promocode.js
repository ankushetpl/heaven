var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Promocode = {

    addPromocode: function (promocodeData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_promocode SET ?";
        promocodeData.promo_name = striptags(promocodeData.promo_name);
        promocodeData.promo_des = striptags(promocodeData.promo_des);

        db.connection.query(query, promocodeData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getPromocode: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'promo_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_promocode WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singlePromocode: function (promo_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_promocode WHERE  promo_id = '"+ promo_id +"' ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getPromoImage: function (file_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE file_id = "+ file_id +" AND is_deleted = 0";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    updatePromocode: function (promocodeData, promo_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_promocode SET ? WHERE ?';
        promocodeData.promo_name = striptags(promocodeData.promo_name);
        promocodeData.promo_des = striptags(promocodeData.promo_des);

        db.connection.query(query, [promocodeData, { promo_id: promo_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusPromocode: function(Status, promo_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_promocode SET ? WHERE ?';

        db.connection.query(query, [Status, { promo_id: promo_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deletePromocode: function (promo_id) {
        var deferred = Q.defer();
        var promo_id = promo_id;
        var query = "UPDATE tbl_promocode SET is_deleted = 1 WHERE promo_id = " + promo_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countPromocode: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(promo_id) as total FROM tbl_promocode WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No promocode found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.promo_id !== 'undefined' && searchParams.promo_id !== '') {
            where += " AND promo_id = " + searchParams.promo_id;
        }

        if (typeof searchParams.promocode_weektype !== 'undefined' && searchParams.promocode_weektype !== '') {
            where += " AND promocode_weektype = " + searchParams.promocode_weektype;
        }

        if (typeof searchParams.promocode_servicetype !== 'undefined' && searchParams.promocode_servicetype !== '') {
            where += " AND promocode_servicetype = " + searchParams.promocode_servicetype;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Promocode;