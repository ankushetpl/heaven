var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Amenities = {

    checkAmenities: function (checkData) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_amenities WHERE is_deleted = 0";
        checkData.amenities_type = striptags(checkData.amenities_type);        

        if (typeof checkData.amenities_type !== 'undefined' && checkData.amenities_type !== '') {
            query += " AND amenities_type LIKE '%" + striptags(checkData.amenities_type) + "%' ";
        }

        if (typeof checkData.amenities_count !== 'undefined' && checkData.amenities_count !== '') {
            query += " AND amenities_count = " + checkData.amenities_count + " ";
        }
        
        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);            
        });
        return deferred.promise;
    },

    addAmenities: function (amenitiesData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_amenities SET ?";
        amenitiesData.amenities_type = striptags(amenitiesData.amenities_type);
        amenitiesData.amenities_count = striptags(amenitiesData.amenities_count);
        amenitiesData.amenities_clean_duartion = striptags(amenitiesData.amenities_clean_duartion);
        amenitiesData.amenities_clean_cost = striptags(amenitiesData.amenities_clean_cost);

        db.connection.query(query, amenitiesData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getAmenities: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'amenities_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_amenities WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleAmenities: function (amenities_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_amenities WHERE ? AND is_deleted = 0";
        db.connection.query(query, { amenities_id: amenities_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateAmenities: function (amenitiesData, amenities_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_amenities SET ? WHERE ?';
        amenitiesData.amenities_type = striptags(amenitiesData.amenities_type);
        // amenitiesData.amenities_clean_duartion = amenitiesData.amenities_clean_duartion;
        // amenitiesData.amenities_clean_cost = amenitiesData.amenities_clean_cost;

        db.connection.query(query, [amenitiesData, { amenities_id: amenities_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusAmenities: function(Status, amenities_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_amenities SET ? WHERE ?';

        db.connection.query(query, [Status, { amenities_id: amenities_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteAmenities: function (amenities_id) {
        var deferred = Q.defer();
        var amenities_id = amenities_id;
        var query = "UPDATE tbl_amenities SET is_deleted = 1 WHERE amenities_id = " + amenities_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countAmenities: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(amenities_id) as total FROM tbl_amenities WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Amenities found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.amenities_id !== 'undefined' && searchParams.amenities_id !== '') {
            where += " AND amenities_id = " + searchParams.amenities_id;
        }

        if (typeof searchParams.amenities_type !== 'undefined' && searchParams.amenities_type !== '') {
            where += " AND amenities_type LIKE '%" + striptags(searchParams.amenities_type) + "%' ";
        }

        if (typeof searchParams.amenities_count !== 'undefined' && searchParams.amenities_count !== '') {
            where += " AND amenities_count LIKE '%" + striptags(searchParams.amenities_count) + "%' ";
        }

        if (typeof searchParams.amenities_clean_duartion !== 'undefined' && searchParams.amenities_clean_duartion !== '') {
            where += " AND amenities_clean_duartion LIKE '%" + striptags(searchParams.amenities_clean_duartion) + "%' ";
        }

        if (typeof searchParams.amenities_clean_cost !== 'undefined' && searchParams.amenities_clean_cost !== '') {
            where += " AND amenities_clean_cost LIKE '%" + striptags(searchParams.amenities_clean_cost) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Amenities;