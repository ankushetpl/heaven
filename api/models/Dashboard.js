var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");
var commonHelper = require('../helper/common_helper.js');

var Dashboard = {

    countDATA: function () {
        var deferred = Q.defer();
        
        var query = "SELECT COUNT(tbl_user.user_id) as Worker FROM tbl_user JOIN tbl_workers on tbl_user.user_id = tbl_workers.user_id WHERE is_deleted = 0 AND user_role_id = 3; SELECT COUNT(tbl_user.user_id) as Client FROM tbl_user JOIN tbl_clients on tbl_user.user_id = tbl_clients.user_id WHERE is_deleted = 0 AND user_role_id = 5; SELECT COUNT(booking_id) as Booking FROM tbl_booking WHERE job_status = '1'; SELECT COUNT(bookweekly_id) as WeeklyBooking FROM tbl_bookweekly WHERE job_status = '1'; SELECT COUNT(request_id) as EarlyPay FROM tbl_earlypayRequest WHERE status = '1'; SELECT COUNT(tbl_user.user_id) as Cafe FROM tbl_user JOIN tbl_cafeusers on tbl_user.user_id = tbl_cafeusers.user_id WHERE is_deleted = 0 AND user_role_id = 2; SELECT COUNT(tbl_user.user_id) as Assessor FROM tbl_user JOIN tbl_assessors on tbl_user.user_id = tbl_assessors.user_id WHERE is_deleted = 0 AND user_role_id = 4; SELECT setting_value as logPic FROM `tbl_setting` WHERE `setting_id` = 5; ";
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    getNotificationData: function () {
        var deferred = Q.defer();
        
        var query = "SELECT notification_id, notification_msg, created_at FROM tbl_notification WHERE notification_read = 0 AND user_id = '1' ";
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    }   

};

module.exports = Dashboard;