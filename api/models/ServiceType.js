var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var ClaimCategory = {

    addServiceType: function (serviceTypeData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_tasktype SET ?";
        serviceTypeData.tasktype_name = striptags(serviceTypeData.tasktype_name);
        
        db.connection.query(query, serviceTypeData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getServiceType: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'tasktype_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tasktype WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleServiceType: function (tasktype_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tasktype WHERE ? AND is_deleted = 0";
        db.connection.query(query, { tasktype_id: tasktype_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateServiceType: function (serviceTypeData, tasktype_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tasktype SET ? WHERE ?';
        serviceTypeData.tasktype_name = striptags(serviceTypeData.tasktype_name);

        db.connection.query(query, [serviceTypeData, { tasktype_id: tasktype_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusServiceType: function(Status, tasktype_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tasktype SET ? WHERE ?';

        db.connection.query(query, [Status, { tasktype_id: tasktype_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteServiceType: function (tasktype_id) {
        var deferred = Q.defer();
        var tasktype_id = tasktype_id;
        var query = "UPDATE tbl_tasktype SET is_deleted = 1 WHERE tasktype_id = " + tasktype_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countServiceType: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(tasktype_id) as total FROM tbl_tasktype WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Service Type found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.tasktype_id !== 'undefined' && searchParams.tasktype_id !== '') {
            where += " AND tasktype_id = " + searchParams.tasktype_id;
        }

        if (typeof searchParams.tasktype_name !== 'undefined' && searchParams.tasktype_name !== '') {
            where += " AND tasktype_name LIKE '%" + striptags(searchParams.tasktype_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    getCheckBooking: function (tasktype_id) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(*) as total FROM `tbl_booking` WHERE ? AND `job_status` = 0 AND `booking_payment_status` = 0 AND `is_deleted` = 0";
        db.connection.query(query, { booking_service_type: tasktype_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    }

};

module.exports = ClaimCategory;