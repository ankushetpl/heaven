var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Worker = {

    // CHECK USERNAME EXISTS OR NOT
    checkWokerExists: function(workers_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof workers_id != 'undefined' && workers_id != '') {
            where += " user_id = '" + striptags(workers_id) + "'";
        }

        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // UPDATE WORKERS DATA
    updateWorkers: function(workers_id, workersData) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_workers SET ? WHERE ?";

        var test = db.connection.query(query, [workersData, { user_id: striptags(workers_id) }], function(err, rows) {
            if (err) {
                
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET WORKERS DATA
    getWorker: function(worker_id) {
        var table  = 'tbl_workers';
        
        var deferred = Q.defer();
        var query = "SELECT u.user_id, u.user_username,u.user_password,u.user_role_id, u.device_token, u.device_type, u.social_id, u.is_suspend, u.is_deleted, w.workers_name, w.workers_image, w.workers_phone,  w.workers_mobile, w.workers_address, w.workers_email, w.workers_state, w.workers_country, w.workers_zip, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating FROM tbl_user as u JOIN "+ table +" as w ON u.user_id = w.user_id WHERE";
        var where = "";

        if (typeof worker_id != 'undefined' && worker_id != '') {
            where += " u.user_id = '" + (worker_id) + "'";
        }

        where += " AND u.status = 1";
        where += " AND u.is_suspend = 0";
        where += " AND u.is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    // ALL WORKERS DATA
    getAllWorkers: function(worker_id,booking_id) {
        var deferred = Q.defer();

        var table = 'tbl_workers';

        var query = "SELECT w.user_id, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_email, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, 0 as workerBookingFlag FROM tbl_user as u JOIN tbl_workers as w ON u.user_id = w.user_id WHERE u.status = 1 AND u.is_suspend = 0 AND u.is_deleted = 0 AND  w.user_id != "+ worker_id +"";

        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;  
    },

    // ALL Substitute By worker
    getAllSubstitute:function(worker_id, booking_id){
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_substitute WHERE substitute_sender_id = '"+ worker_id +"' AND substitute_booking_id = '"+ booking_id +"' ";

        query = mysql.format(query);
        db.connection.query(query, function(errs, result) {
            if (errs) {
                deferred.reject(new Error(errs));
            }

            deferred.resolve(result);                        
            
        }); 
        return deferred.promise;
    },

    // ALL CLIENTS DATA
    substituteWorkers: function(worker_id,booking_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_substitute WHERE substitute_sender_id = '"+ worker_id +"' AND substitute_booking_id = '"+ booking_id +"' ";
        
        query = mysql.format(query);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },                                              

    // GET TODAYS AND UPCOMING JOB LIST
    getTodaysJob: function(worker_id,flag,today) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time,  c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE ";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " AND b.is_deleted =  0 ";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date = '" + (today) + "' ";
                }
              
            } else {
                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long,  c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date > '" + (today) + "' ";
                }
            } 

            where += " AND b.job_status = 0";
            where += " AND b.booking_payment_status = 1";
            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            
            query += where;
            query = mysql.format(query, where);
            
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },


    // GET TODAYS AND UPCOMING WEEKLY JOB LIST
    getTodaysJobWeekly: function(worker_id,flag,today) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {

                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE ";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " AND b.is_deleted =  0 ";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date = '" + (today) + "' ";
                }
              
            } else {
                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date > '" + (today) + "' ";
                }
            } 

            where += " AND b.job_status = 0";
            where += " AND b.booking_payment_status = 1";
            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0 ";
            // where += " AND b.worker_status = 0";
           
            query += where;
            query = mysql.format(query, where);
            
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },    


    // GET MY JOB LIST
    getMyJobs: function(worker_id) {
        var deferred = Q.defer(); 
            var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating,  w.workers_booking_rating, t.tasktype_name, 0 as startJobFlag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
            var where = "";       //0 as clientRating

            if (typeof worker_id != 'undefined' && worker_id != '') {
                where += " b.worker_id = '" + (worker_id) + "'";
            }
            
            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            where += " AND b.booking_payment_status = 1";
            
            query += where;
            query = mysql.format(query, where);
            
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;
    }, 


    // GET MY WEEKLY JOB LIST
    getMyJobsWeekly: function(worker_id) {
        var deferred = Q.defer();
            var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating,t.tasktype_name, 0 as startJobFlag FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
            var where = "";

            if (typeof worker_id != 'undefined' && worker_id != '') {
                where += " b.worker_id = '" + (worker_id) + "'";
            }
            
            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            where += " AND b.booking_payment_status = 1";
            
            query += where;
            query = mysql.format(query, where);
            
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;
    },      


    // SUBSTITUTE REQUEST LIST
    getHistory: function(worker_id,flag,today) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {    // completed jobs

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as rateFlag FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date < '" + (today) + "' ";
                }
                where += " AND b.job_status = 1";
                where += " AND b.booking_payment_status = 1";
               
            } else if(flag == 1) {   // upcoming jobs

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date > '" + (today) + "' ";
                }
                where += " AND b.booking_payment_status = 1";

            } else if(flag == 4) {   // pending jobs

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE ";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date < '" + (today) + "' ";
                }
                where += " AND b.job_status = 0";
                where += " AND b.booking_payment_status = 1";
                where += " AND b.booking_substitute = 0";
            }   

            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            
            query += where;
            query = mysql.format(query, where);
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },


    // SUBSTITUTE REQUEST LIST WEEKLY SERVICE
    getHistoryWeekly: function(worker_id,flag,today) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {    // completed jobs

                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as rateFlag FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date < '" + (today) + "' ";
                }
                where += " AND b.job_status = 1";
                where += " AND b.booking_payment_status = 1";
               
            } else if(flag == 1) {   // upcoming jobs

                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date > '" + (today) + "' ";
                }
                where += " AND b.booking_payment_status = 1";

            } else if(flag == 4) {   // pending jobs

                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE ";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date < '" + (today) + "' ";
                }
                where += " AND b.job_status = 0";
                where += " AND b.booking_payment_status = 1";
                where += " AND b.booking_substitute = 0";
            }   

            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            
            query += where;
            query = mysql.format(query, where);
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },



  // SUBSTITUTE REQUEST LIST
    getSubstituteRequest: function(worker_id,today) {
        var deferred = Q.defer();
        var query = '';
            
                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, s.substitute_id FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type JOIN tbl_substitute as s ON b.booking_id = s.substitute_booking_id  WHERE";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " s.substitute_receiver_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date >= '" + (today) + "' ";
                }

            // where += " OR b.booking_date = '" + (today) + "' )";
            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            where += " AND s.is_deleted = 0";
            where += " AND b.worker_status = 0 ";
           
            query += where;
            query = mysql.format(query, where);
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },

   // SUBSTITUTE REQUEST LIST WEEKLY BOOKING
    getSubstituteRequestWeekly: function(worker_id,today) {
        var deferred = Q.defer();
        var query = '';
            
                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, s.substitute_id FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type JOIN tbl_substitute as s ON b.bookweekly_id = s.substitute_bookweekly_id  WHERE";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " s.substitute_receiver_id = '" + (worker_id) + "'";
                }
                if (typeof today != 'undefined' && today != '') {
                    where += " AND b.booking_date >= '" + (today) + "' ";
                }

            // where += " OR b.booking_date = '" + (today) + "' )";
            where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            where += " AND s.is_deleted = 0";
            where += " AND b.worker_status = 0 ";
           
            query += where;
            query = mysql.format(query, where);
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },


   // CHECK SUBSTITUTE JOB ALREADY SEND OR NOT
    CheckSendReq: function(booking_id,worker_id) {
        var deferred = Q.defer();
        var query = "SELECT INTO tbl_substitute WHERE substitute_booking_id = '"+ booking_id +"' AND substitute_sender_id = '"+ worker_id +"' ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    // Check Substitute request already exists or not
    CheckSubstituteExists: function(booking_id, serviceflag, worker_id, receiver_id) {
        var deferred = Q.defer();
            if(serviceflag == 0) {

                var query = "SELECT * FROM tbl_substitute WHERE substitute_booking_id = '"+ booking_id +"' AND substitute_sender_id = '"+ worker_id +"' AND substitute_receiver_id = '"+ receiver_id +"' ";
            } else {
                var query = "SELECT * FROM tbl_substitute WHERE substitute_bookweekly_id = '"+ booking_id +"' AND substitute_sender_id = '"+ worker_id +"' AND substitute_receiver_id = '"+ receiver_id +"' ";
            }
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

   // SUBSTITUTE SEND
    getSubstituteSend: function(addSubstitute, booking_id, serviceflag) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_substitute SET ?";

        db.connection.query(query, addSubstitute, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if(result) {

                if(serviceflag == 0) {

                    var querys = "UPDATE tbl_booking SET ? WHERE ?";
                    var userData = {booking_substitute : '1'};
                    db.connection.query(querys, [userData, { booking_id: booking_id }], function(errs, rows1) {
                        if (errs) {
                            deferred.reject(new Error(errs));
                        }
                        deferred.resolve(rows1);
                    });

                } else {
                    var querys = "UPDATE tbl_bookweekly SET ? WHERE ?";
                    var userData = {booking_substitute : '1'};
                    db.connection.query(querys, [userData, { bookweekly_id: booking_id }], function(errs, rows1) {
                        if (errs) {
                            deferred.reject(new Error(errs));
                        }
                        deferred.resolve(rows1);
                    });
                }
                
            }
            deferred.resolve(result.insertId);
        });
        return deferred.promise;
    },

    // Leave Reasons
    leavesList: function() {
        var deferred = Q.defer();
        var query = "SELECT leaves_id,leaves_name FROM tbl_leaves WHERE status = 1 AND is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


   // Apply For Leave Request
    applyLeaveReq: function(leaveApply,dateData) {
        var deferred = Q.defer();
        var total = 0;

        for(i=0; i< dateData.length; i++) {
            leaveApply.workerleaves_from = dateData[i];
            
            var query = "INSERT INTO tbl_workerleaves SET ?";
            db.connection.query(query, leaveApply, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                if(result) {
                    total = total+1;
                }
                if(total == dateData.length) {
            deferred.resolve(result);
        }
            });
        }
        return deferred.promise;
    },

    // Delete leave
    deleteWorkerLeave: function(datadelete, worker_id,leave_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_workerleaves SET is_deleted = 1 WHERE worker_id = '"+ worker_id +"' AND workerleaves_id = '"+ leave_id +"' ";
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // Worker All Leaves
    getWorkerLeaves: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT workerleaves_id, workerleaves_from, workerleaves_note, workerleaves_terms, workerleaves_emergency, workerleaves_file, workerleaves_leaves_id FROM tbl_workerleaves WHERE is_deleted = 0 AND worker_id = '"+ worker_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // WORKER CALENDER
    getWorkerCalender: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT workerleaves_from as date FROM tbl_workerleaves WHERE is_deleted = 0 AND worker_id = '"+ worker_id +"' order by workerleaves_from DESC ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
        
    },

    // WORKER ALL BOOKING
    getWorkerBookingCalender: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT booking_date as date FROM tbl_booking WHERE is_deleted = 0 AND worker_id = '"+ worker_id +"' AND booking_once_weekly = 0 AND booking_payment_status = 1 ";
        
        query = mysql.format(query);
       
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // WORKER ALL BOOKING
    getWorkerBookingWeekCalender: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT booking_date as date FROM tbl_bookweekly WHERE is_deleted = '0' AND worker_id = '"+ worker_id +"'  AND booking_once_weekly = 1 AND booking_payment_status = 1 ";
        
        query = mysql.format(query);
       
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    


    // START AND END JOB
    workerJobTime: function(worker_id,time,booking_id,flag,updated_at,serviceflag) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {    // start job

                if(serviceflag == 0) {

                    var querys = "UPDATE tbl_booking SET ? WHERE ?";
                    var jobData = {booking_start_time : time, worker_status : '1', updated_at : updated_at};
                    db.connection.query(querys, [jobData, { booking_id: booking_id }, { worker_id: worker_id }], function(errs, rows1) {
                        if (errs) {
                            deferred.reject(new Error(errs));
                        }
                        deferred.resolve(rows1);
                    });

                } else {

                    var querys = "UPDATE tbl_bookweekly SET ? WHERE ?";
                    var jobData = {booking_start_time : time, worker_status : '1', updated_at : updated_at};
                    db.connection.query(querys, [jobData, { bookweekly_id: booking_id }, { worker_id: worker_id }], function(errs, rows1) {
                        if (errs) {
                            deferred.reject(new Error(errs));
                        }
                        deferred.resolve(rows1);
                    });
                }

            } else {   // end job

                if(serviceflag == 0) {

                    var querys = "UPDATE tbl_booking SET ? WHERE ?";
                    var jobData = {booking_end_time : time, job_status : '1', updated_at : updated_at};
                    db.connection.query(querys, [jobData, { booking_id: booking_id }, { worker_id: worker_id }], function(errs, rows1) {
                        if (errs) {
                            deferred.reject(new Error(errs));
                        }
                        deferred.resolve(rows1);
                    });

                } else {

                    var querys = "UPDATE tbl_bookweekly SET ? WHERE ?";
                    var jobData = {booking_end_time : time, job_status : '1', updated_at : updated_at};
                    db.connection.query(querys, [jobData, { bookweekly_id: booking_id }, { worker_id: worker_id }], function(errs, rows1) {
                        if (errs) {
                            deferred.reject(new Error(errs));
                        }
                        deferred.resolve(rows1);
                    });
                }
   
            } 
           
            return deferred.promise;
    },

    // GET JOB DETAILS
    getJobDetails: function(booking_id, serviceflag) {
        var deferred = Q.defer();
        
            if(serviceflag == 0) {

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_substitute, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.is_deleted = 0 AND b.booking_id = '"+ booking_id +"' ";

            } else {

                var query = "SELECT b.bookweekly_id, b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_substitute, b.booking_start_time, b.booking_end_time, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_bookweekly as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE b.is_deleted = 0 AND b.bookweekly_id = '"+ booking_id +"' ";

            }
        
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // GET SUBSTITITUTE ID
    getSubstituteID: function(booking_id, serviceflag, worker_id) {
        var deferred = Q.defer();

            if(serviceflag == 0) {
                var query = "SELECT substitute_id FROM tbl_substitute WHERE substitute_booking_id = '"+ booking_id +"' AND substitute_receiver_id = '"+ worker_id +"' ";

            } else {
                var query = "SELECT substitute_id FROM tbl_substitute WHERE substitute_bookweekly_id = '"+ booking_id +"' AND substitute_receiver_id = '"+ worker_id +"' ";
            }
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    // ALL AMENITIES
    getAmenitiesData: function() {
        var deferred = Q.defer();
        var query = "SELECT amenities_id, amenities_count, amenities_clean_duartion FROM tbl_amenities WHERE is_deleted = 0 AND status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL CLEANING AREA DATA
    getCleaningData: function() {
        var deferred = Q.defer();
        var query = "SELECT area_id, area_name, area_clean_duration FROM tbl_cleaningarea WHERE is_deleted = 0 AND status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL SKILLS DATA
    getSkillsData: function() {
        var deferred = Q.defer();
        var query = "SELECT skills_id, skills_name FROM tbl_skills WHERE is_deleted = 0 AND status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL CUSTOM TASK DATA
    getCustomTask: function() {
        var deferred = Q.defer();
        var query = "SELECT tasklist_id, tasklist_name, tasklist_des, tasklist_duration FROM tbl_tasklist WHERE is_deleted = 0 AND status = 1 AND tasklist_type = 1";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ALL STANDARD TASK DATA
    getStandardTask: function() {
        var deferred = Q.defer();
        var query = "SELECT tasklist_id, tasklist_name, tasklist_des, tasklist_duration FROM tbl_tasklist WHERE is_deleted = 0 AND status = 1 AND tasklist_type = 0";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }, 


    // ALL FILES DATA
    getImages: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ALL BOOKING DATA
    getAddressBooking: function(booking_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking WHERE booking_id =  "+ booking_id +" ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    


    // CAB REQUEST TO ADMIN
    cabRequest: function(cabBook) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_suburbs SET ?";

        db.connection.query(query, cabBook, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }, 

    // GET CAFE USER 
    getCafeUser: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_workers WHERE user_id = "+ worker_id +" ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },  

    // CHECK CAB REQUEST ALREADY SENT OR NOT
    cabRequestExists: function(worker_id,booking_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_suburbs WHERE worker_user_id = "+ worker_id +" AND booking_id = "+ booking_id +" ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },  


    // MY ALL SUBURBS
    myAllSuburbs: function(worker_id) {
        var deferred = Q.defer();
        var query = "SELECT workers_suburb FROM tbl_workers WHERE user_id = "+ worker_id +" ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //  GET VALUE OF ONCE OFF SERVICE PRICE
    getOnceOffPrice: function() {
        var deferred = Q.defer();
        var query = "SELECT setting_value FROM tbl_setting WHERE setting_id = 8 ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },
    

    //  ALL SUBURBS
    getAllSuburbs: function() {
        var deferred = Q.defer();
        var query = "SELECT suburb_id, suburb_name FROM tbl_suburb WHERE is_deleted = 0 ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },


    // MY ALL SUBURBS
    updateSuburbs: function(suburbs, worker_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_workers SET ? WHERE ?";

        db.connection.query(query, [suburbs, { user_id: worker_id }], function(errs, rows1) {
            if (errs) {
                deferred.reject(new Error(errs));
            }
            console.log(query);
            deferred.resolve(rows1);
        });  
        return deferred.promise;
    },


    // GET ALL CITIES LIST
    getAllCitySouthAfrica: function() {
                
        var deferred = Q.defer();
        var query = "SELECT name,id FROM cities WHERE state_id IN (SELECT id from states WHERE country_id = '202')";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ADD RATING TO CLIENT
    giveClientsRating: function(booking_id,client_id,worker_id,arrayId,arrayRatings,currentDate) {
        var deferred = Q.defer();
 
        for(var i=0; i < arrayId.length; i++) {

                
            var query = "INSERT INTO tbl_clientrating SET ?";
            var ratingData = { booking_id : booking_id, client_id : client_id, worker_id : worker_id, rating_id : arrayId[i], ratings : arrayRatings[i], created_at : currentDate, updated_at : currentDate};
            
            db.connection.query(query, ratingData, function(err, rows) {
                if (err) {
                        deferred.reject(new Error(err));
                    }
                    
                    deferred.resolve(rows);
                });
            
        }    
        return deferred.promise;
    },


    // UPDATE AVERAGE RATING OF CLIENT IN BOOKING
    averageRating: function(booking_id,client_id,worker_id,avg,serviceflag) {
        var deferred = Q.defer();
        if(serviceflag == 0) {
            var query = "UPDATE tbl_booking SET ? WHERE ?";

            var avgRate = {booking_client_rating : avg};
                db.connection.query(query, [avgRate, { booking_id: booking_id }, {client_id : client_id}, {worker_id : worker_id}], function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows);
                });     
            return deferred.promise;
        } else {
            var query = "UPDATE tbl_bookweekly SET ? WHERE ?";
            
            var avgRate = {booking_client_rating : avg};
                db.connection.query(query, [avgRate, { bookweekly_id: booking_id }, {client_id : client_id}, {worker_id : worker_id}], function(err, rows) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                    deferred.resolve(rows);
                });
            return deferred.promise;
        }
    },


        // GET ALL DONE BOOKING JOB
    getAllDoneJobsBooking: function(client_id) {
                
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking WHERE client_id = '"+ client_id +"' AND booking_client_rating != '' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    }, 

        // GET ALL DONE BOOKING WEEKLY
    getAllDoneJobsWeekly: function(client_id) {
                
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_bookweekly WHERE client_id = '"+ client_id +"' AND booking_client_rating != '' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    },      


     // UPDATE CLIENTS RATING
    loginAvgRating: function(client_id,totalAvg) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_clients SET clients_rating = '"+ totalAvg +"' WHERE  user_id = '"+ client_id +"' ";
        db.connection.query(query, function(err, rows) {
            if (err) {
                
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET BOOKING ID
    getBookId: function(substitute_id) {
                
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_substitute WHERE substitute_id = '"+ substitute_id +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    


    // SUBSTITUTE ACCEPT OR REJECT
    subAcceptReject: function(substitute_id,flag,currentDate,bookingId,worker_id,serviceflag) {
        var deferred = Q.defer();
        
            if(flag == 0) {    // accept request

                var query = "UPDATE tbl_substitute SET ? WHERE ?";
                var sub = {substitute_status : '1', updated_at : currentDate};
                db.connection.query(query, [sub, { substitute_id: substitute_id }], function(error, result, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }
                    if(result) {

                        if(serviceflag == 0) {

                            var querys = "UPDATE tbl_booking SET ? WHERE ?";
                            var userData = {worker_status : '1', worker_id : worker_id};
                            db.connection.query(querys, [userData, { booking_id: bookingId }], function(errs, rows1) {
                                if (errs) {
                                    deferred.reject(new Error(errs));
                                }
                                deferred.resolve(rows1);
                            });

                        } else {

                            var queryss = "UPDATE tbl_bookweekly SET ? WHERE ?";
                            var userDatas = {worker_status : '1', worker_id : worker_id};
                            db.connection.query(queryss, [userDatas, { bookweekly_id: bookingId }], function(errs, rows1) {
                                if (errs) {
                                    deferred.reject(new Error(errs));
                                }
                                deferred.resolve(rows1);
                            });

                        }
                        
                    }
                    deferred.resolve(result);
                });
        
            } else {   // reject request

                var querys = "UPDATE tbl_substitute SET ? WHERE ?";
                var sub = {substitute_status : '2', updated_at : currentDate};
                db.connection.query(querys, [sub, { substitute_id: substitute_id }], function(error, result, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }
                   deferred.resolve(result);
                });
            } 
           
            return deferred.promise;
    },


    // GET CLIENTS AVERAGE RATINGS
    getAvgRating: function() {
                
        var deferred = Q.defer();
        var query = "Select client_id,sum(booking_client_rating) as totalRating, count(booking_id) as count FROM `tbl_booking` WHERE booking_client_rating != '0' group by client_id ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // GET COMPLETED JOB LIST WITH AND WITHOUT RATING
    getCompletedJobs: function(worker_id,flag) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_email, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE ";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }

                where += " AND b.booking_client_rating = 0.00 ";
                
            } else {
                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_email, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                where += " AND b.booking_client_rating != 0.00 ";
            }    

            // where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            where += " AND b.job_status = 1";
           
            query += where;
            query = mysql.format(query, where);
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },

    // GET COMPLETED WEEKLY JOB LIST WITH AND WITHOUT RATING
    getCompletedJobsWeekly: function(worker_id,flag) {
        var deferred = Q.defer();
        var query = '';
            if(flag == 0) {

                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_email, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE ";
                
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }

                where += " AND b.booking_client_rating = 0.0 ";
                
            } else {
                var query = "SELECT b.booking_id, b.client_id, b.worker_id, b.booking_date, b.booking_time_from ,b.booking_lat, b.booking_long, b.booking_apartment, b.booking_street, b.booking_city, b.booking_bedrooms, b.booking_bathrooms, b.booking_baskets ,b.booking_cleaning_area, b.booking_other_work, b.booking_standard, b.booking_custom, b.booking_inst1, b.booking_inst2, b.booking_inst3, b.booking_image1, b.booking_image2, b.booking_image3, b.booking_image4, b.booking_image5, b.booking_skills, b.booking_start_time, b.booking_end_time, b.booking_worker_rating, b.booking_client_rating, b.booking_payment_status, b.booking_once_weekly, b.booking_work_time, c.clients_name, c.clients_email, c.clients_phone, c.clients_mobile, c.clients_address, c.clients_state, c.clients_country, c.clients_zip, c.clients_image, c.clients_lat, c.clients_long, c.clients_rating, w.workers_name, w.workers_phone, w.workers_mobile, w.workers_address, w.workers_state, w.workers_country, w.workers_zip, w.workers_image, w.workers_lat, w.workers_long, w.workers_rating, w.workers_booking_rating, t.tasktype_name, 0 as clientRating FROM tbl_booking as b JOIN tbl_clients as c ON c.user_id = b.client_id JOIN tbl_workers as w ON w.user_id = b.worker_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type  WHERE";
                var where = "";

                if (typeof worker_id != 'undefined' && worker_id != '') {
                    where += " b.worker_id = '" + (worker_id) + "'";
                }
                where += " AND b.booking_client_rating != 0.0 ";
            }    

            // where += " AND b.status = 1";
            where += " AND b.is_deleted = 0";
            where += " AND b.job_status = 1";
           
            query += where;
            query = mysql.format(query, where);
            db.connection.query(query, function(error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                deferred.resolve(result);
            });
            return deferred.promise;

    },


    // GET DEVICE TOKEN OF USERS FOR NOTIFICATION
    getDeviceToken: function(user_id) {
        var deferred = Q.defer();
        var query = "Select * FROM tbl_user WHERE user_id = '"+ user_id +"' AND status= '1' AND is_suspend = '0' AND is_deleted = '0' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // ADD NOTIFICATION DATA
    addNotificationData: function(notSendData) {
        var deferred = Q.defer();
        
        var query = "INSERT INTO tbl_notification SET ?";
        query = mysql.format(query);
        db.connection.query(query, notSendData, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
          
        return deferred.promise;
    },


    // GET BADGE COUNT 
    notifyBadgeCount: function(user_id) {
        var deferred = Q.defer();
        
        var querys = "Select * FROM tbl_notification WHERE user_id = '"+ user_id +"' AND notification_read = '0' ";
            db.connection.query(querys, function(errs, rows1) {
                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows1);
            });
        return deferred.promise;
    },    

    // TEST SEND NOTIFICATION 
    sendNotificationTest: function(deviceToken,msgtitle,msgbody) {

        var deferred = Q.defer();       
        var FCM = require('fcm-push');
        // var serverKey =  'AIzaSyDTlleQYk5yS4eOSG256qD4Awa4BST7whM';  // server key for worker
        var serverKey =  ' AIzaSyDZvhkAtY0OFV_Lu96Zif9j0uWEVzlmDlw';  // server key for client
        var fcm = new FCM(serverKey);
        var message = {  
            to : deviceToken,
            notification : {
                user_id : 102,
                notification_id : 2,
                notification_type : 'Test',
                notification_detail_id : 0,
                notification_detail_serviceflag : 0,
                notification_msg : 'Test Data',
                notification_date : '2018-06-06'
            }
        };
        console.log(message);
        fcm.send(message, function(err,response){  
            console.log(response);
            deferred.resolve(response);
        });
        return deferred.promise;
    },


    // SEND NOTIFICATION 
    sendNotification: function(deviceToken,msgtitle,msgbody,badge,data) {

        var deferred = Q.defer();       
        var FCM = require('fcm-push');
        var serverKey =  'AIzaSyDTlleQYk5yS4eOSG256qD4Awa4BST7whM';  // server key
        var fcm = new FCM(serverKey);
        var message = {  
            to : deviceToken,
            notification : {
                badge : badge,
                user_id : data.user_id,
                notification_id : data.notification_id,
                notification_type : data.notification_type,
                notification_detail_id : data.notification_detail_id,
                notification_detail_serviceflag : data.notification_detail_serviceflag,
                notification_msg : data.notification_msg,
                notification_date : dateFormat(data.created_at, "yyyy-mm-dd H:i:s")
            }
        };
        
        fcm.send(message, function(err,response){ 
            console.log(err); 
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(response);
        });
        return deferred.promise;
    },


    // GET NOTIFICATION DATA
    getNotResponse: function(resultAN) {
        var deferred = Q.defer();
        var query = "Select * FROM tbl_notification WHERE notification_id = '"+ resultAN +"' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },



    // GET NOTIFICATION LIST
    getAllNotList: function(user_id, device_type) {
        var deferred = Q.defer();
        var query = "Select * FROM tbl_notification WHERE user_id = '"+ user_id +"' AND is_deleted = '0' ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // UPDATE READ STATUS OF NOTIFICATION
    readStatus: function(worker_id,notification_id) {
        var deferred = Q.defer();
        var query = "UPDATE tbl_notification SET ? WHERE ?";
        var statusNot = {notification_read : 1};
        db.connection.query(query, [ statusNot, { notification_id : notification_id }, { user_id : worker_id } ], function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // DELETE OR CLEAR ALL NOTIFICATION
    deleteNotification: function(worker_id,notification_id,flag) {
        var deferred = Q.defer();

        if(flag == 0) {

            var query = "UPDATE tbl_notification SET ? WHERE ?";
            var statusNot = {is_deleted : 1};
            db.connection.query(query, [ statusNot, { notification_id : notification_id }, { user_id : worker_id } ], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;

        } else {

            var query = "UPDATE tbl_notification SET ? WHERE ?";
            var statusNot = {is_deleted : 1};
            db.connection.query(query, [ statusNot, { user_id : worker_id } ], function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
            return deferred.promise;

        }
        
    },


    // NOTIFICATION COUNT
    getNotCount: function(user_id) {
        var deferred = Q.defer();
        var query = "Select count(notification_id) as notifications FROM tbl_notification WHERE user_id = '"+ user_id +"' AND is_deleted = '0' AND notification_read = '0' ";
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // BOOKING DATA
    getBookingDataAll: function(booking_id,serviceflag) {
        var deferred = Q.defer();

        if(serviceflag == 0) {

            var query = "SELECT * FROM tbl_booking WHERE booking_id = '"+ booking_id +"' ";

        } else {
            var query = "SELECT * FROM tbl_bookweekly WHERE bookweekly_id = '"+ booking_id +"' ";
        }
       
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },    


    // add early pay request
    sendRequest: function(data) {
        var deferred = Q.defer();
        
        var query = "INSERT INTO tbl_earlypayRequest SET ?";
        query = mysql.format(query);
        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
          
        return deferred.promise;
    },

    // get users number to send otp
    getUsersNumber: function(mobileNumber) {
        var deferred = Q.defer();
        
        var query = "SELECT * from tbl_user WHERE user_username = '"+ mobileNumber +"' AND is_suspend = 0 AND is_deleted = 0 ";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },  

    //save users OTP for forget pass
    saveUsersOTP: function(otp,user_id) {
        var deferred = Q.defer();
        
        var query = "UPDATE tbl_user SET user_otp = '"+ otp +"' WHERE user_id = '"+ user_id +"' ";
        query = mysql.format(query);
        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result);
            }
        });
    },          

    // reset password
    resetPassword: function(password,worker_id) {
        var deferred = Q.defer();
        
        var query = "UPDATE tbl_user SET user_password = '"+ password +"' WHERE user_id = '"+ worker_id +"' ";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ANGEL LIST SKILLS FILTER DATA FOR SUBSTITUTE
    getAngelList: function(agemin, agemax, suburb, skills,worker_id) {
        var deferred = Q.defer();
        var query = "SELECT w.user_id, w.workers_name, w.workers_phone, w.workers_address, w.workers_mobile, w.workers_image, w.workers_lat, w.workers_long, w.workers_suburb, w.workers_friendlyDogs, w.workers_friendlyCats, w.worker_type, w.worker_type, w.workers_rating, w.workers_booking_rating, 0 as favWorker FROM tbl_workers as w JOIN tbl_user as u ON u.user_id = w.user_id  WHERE w.user_id != '" + worker_id + "' AND u.status =1 AND u.is_suspend = 0 AND u.is_deleted = 0 AND w.workers_age  BETWEEN '" + agemin + "' AND '" + agemax + "' AND FIND_IN_SET('" + suburb + "', w.workers_suburb ) AND w.workers_skills IN(" + skills + " )  order by workers_rating DESC ";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);

        });
        return deferred.promise;
    },    

    //  ALL TAKTYPES(SERVICES LIST)
    getAllTasktypeListing: function() {
        var deferred = Q.defer();
        var query = "SELECT tasktype_id, tasktype_name FROM tbl_tasktype WHERE status = 1 AND is_deleted = 0 ";

        db.connection.query(query, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }               


};

module.exports = Worker;