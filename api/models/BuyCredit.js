var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var BuyCredit = {

    addBuyCredit: function (buyCreditData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_buyCredit SET ?";
        
        db.connection.query(query, buyCreditData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getBuyCredit: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'buyCredit_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_buyCredit WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleBuyCredit: function (buyCredit_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_buyCredit WHERE ? AND is_deleted = 0";
        db.connection.query(query, { buyCredit_id: buyCredit_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateBuyCredit: function (buyCreditData, buyCredit_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_buyCredit SET ? WHERE ?';
        
        db.connection.query(query, [buyCreditData, { buyCredit_id: buyCredit_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusBuyCredit: function(Status, buyCredit_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_buyCredit SET ? WHERE ?';

        db.connection.query(query, [Status, { buyCredit_id: buyCredit_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteBuyCredit: function (buyCredit_id) {
        var deferred = Q.defer();
        var buyCredit_id = buyCredit_id;
        var query = "UPDATE tbl_buyCredit SET is_deleted = 1 WHERE buyCredit_id = " + buyCredit_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countBuyCredit: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(buyCredit_id) as total FROM tbl_buyCredit WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No data found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.buyCredit_id !== 'undefined' && searchParams.buyCredit_id !== '') {
            where += " AND buyCredit_id = " + searchParams.buyCredit_id;
        }

        if (typeof searchParams.buyCredit_points !== 'undefined' && searchParams.buyCredit_points !== '') {
            where += " AND buyCredit_points = " + searchParams.buyCredit_points;
        }

        if (typeof searchParams.buyCredit_amount !== 'undefined' && searchParams.buyCredit_amount !== '') {
            where += " AND buyCredit_amount = " + searchParams.buyCredit_amount;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = BuyCredit;