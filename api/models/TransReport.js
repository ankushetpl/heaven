var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var TransReport = {

    getTranx: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'payment_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_payment WHERE payment_status = 1 ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleTranx: function (payment_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_payment as p JOIN tbl_clients as c ON p.client_id = c.user_id WHERE p.payment_id = '"+ payment_id +"' ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    countTranx: function () {
        var deferred = Q.defer();
        var query = "SELECT COUNT(payment_id) as total FROM tbl_payment WHERE payment_status = 1 ";
        
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Transaction record found!"));
            }
        });
        return deferred.promise;
    },

    getAllTransactionData: function () {
        var deferred = Q.defer();
        var query =  "SELECT p.payment_id, p.payment_type, p.payment_reference, p.payment_credit, p.payment_coupon_discount, p.payment_cost, p.payment_currency, p.payment_country, p.payment_method, c.clients_name as payment_by, p.payment_trans_date FROM tbl_payment as p JOIN tbl_clients as c ON p.client_id = c.user_id WHERE p.payment_status = '1' ";
        query = mysql.format(query);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.payment_id !== 'undefined' && searchParams.payment_id !== '') {
            where += " AND payment_id = " + searchParams.payment_id;
        }

        if (typeof searchParams.payment_reference !== 'undefined' && searchParams.payment_reference !== '') {
            where += " AND payment_reference = " + searchParams.payment_reference;
        }

        if (typeof searchParams.payment_type !== 'undefined' && searchParams.payment_type !== '') {
            where += " AND payment_type = " + searchParams.payment_type;
        }
        
        if (typeof searchParams.payment_trans_date !== 'undefined' && searchParams.payment_trans_date !== '') {
            where += " AND DATE_FORMAT(payment_trans_date, '%Y-%m-%d') = '" + searchParams.payment_trans_date + "'";
        }

        return where;
    }

};

module.exports = TransReport;