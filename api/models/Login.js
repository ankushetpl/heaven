var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var commonHelper = require('../helper/common_helper.js');
var Q = require("q");

var Login = {

    //Login Check User
    checkLoginExists: function (username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        //where += " AND user_role_id = " + logintype;
        // where += " AND status = 1";
        // where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Login User
    getLogin: function (username, password, logintype, device_token, device_type) {
        var deferred = Q.defer();
        var total = 0;
        //var currentDate = commonHelper.getCurrentDateTime();
        //console.log(currentDate); return false;        
        var currentDate = '';

        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += "user_username = '" + username + "' ";
        }

        if (typeof password != 'undefined' && password != '') {
            where += " AND user_password = '" + password + "' ";
        }

        where += " AND user_role_id = " + logintype;
        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }

            total = result.length;
            if (total > 0) {
                var UserId = result[0].user_id;
                // var token = commonHelper.setEncodeString(UserId);
                var token = UserId;
                var querys = 'UPDATE tbl_user SET ? WHERE ?';
                if ((logintype == 3 || logintype == 5) && device_type != 1) {
                    var data = { device_token: device_token, device_type: device_type };
                    db.connection.query(querys, [data, { user_id: UserId }], function (err, results, fields) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        result[0].device_token = token;
                        deferred.resolve(result[0]);
                    });
                } else {
                    var data = { device_token: token, device_type: device_type };
                    //console.log("AAA", data);
                    db.connection.query(querys, [data, { user_id: UserId }], function (err, results, fields) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        result[0].device_token = token;
                        deferred.resolve(result[0]);
                    });
                }
            } else {
                deferred.resolve(0);
            }

        });

        return deferred.promise;
    },

    //Register User
    checkUserExists: function (username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Check mobile for Register User
    checkMobileExists: function (mobile) {
        var deferred = Q.defer();

        var query = "SELECT COUNT(u.user_id) as ClientD FROM tbl_clients as c LEFT JOIN tbl_user as u ON c.user_id = u.user_id WHERE c.clients_phone = "+ mobile +" OR c.clients_mobile = "+ mobile +" AND u.is_deleted = 0; SELECT COUNT(u.user_id) as workerD FROM tbl_workers as w LEFT JOIN tbl_user as u ON w.user_id = u.user_id WHERE w.workers_phone = "+ mobile +" OR w.workers_mobile = "+ mobile +" AND u.is_deleted = 0; SELECT COUNT(u.user_id) as cafeD FROM tbl_cafeusers as f LEFT JOIN tbl_user as u ON f.user_id = u.user_id WHERE f.cafeusers_phone = "+ mobile +" AND u.is_deleted = 0; SELECT COUNT(u.user_id) as assD FROM tbl_assessors as a LEFT JOIN tbl_user as u ON a.user_id = u.user_id WHERE a.assessors_phone = "+ mobile +" AND u.is_deleted = 0; ";
        
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    

   // CHECK REFER CODE EXISTS OR NOT
    checkReferCode: function(referID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_clients WHERE ";
        var where = "";

        if (typeof referID != 'undefined' && referID != '') {
            where += " clients_referID = '" + striptags(referID) + "'";
        }

        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    

    //Register Social
    checkUsersocial: function (email, socialID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof email != 'undefined' && email != '') {
            where += " user_username = '" + striptags(email) + "'";
        }

        if (typeof socialID != 'undefined' && socialID != '') {
            where += " AND social_id = '" + striptags(socialID) + "'";
        }

        
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        // console.log(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Register Social ID
    checkUsersocialID: function (socialID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof socialID != 'undefined' && socialID != '') {
            where += " social_id = '" + striptags(socialID) + "'";
        }
        
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);
        // console.log(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Add User
    addUser: function(data, Info, UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "INSERT INTO tbl_user SET ?";

        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            // deferred.resolve(result.insertId);
            Info.user_id = result.insertId;

            var querys = "INSERT INTO "+ table +" SET ?";
            db.connection.query(querys, Info, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(result.insertId);
            });

        });
        return deferred.promise;
    },

    //Add User
    addCredit: function(credit,user_id,credit_points) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_referralcredit SET ?";

        db.connection.query(query, credit, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {

                var querys = "UPDATE tbl_clients SET credit_points = "+ credit_points +" WHERE  user_id = "+ user_id +" ";
        
                    db.connection.query(querys, function(err, rows) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(result.insertId);
                    });
                    return deferred.promise;
            }
        });
        return deferred.promise;
    },    
    
    //Check Password
    checkPassword: function (OldPassword, UserId) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof OldPassword != 'undefined' && OldPassword != '') {
            where += " user_password = '" + striptags(OldPassword) + "'";
        }

        where += " AND user_id = " + UserId;
        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            //console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Change Password
    ChangePassword: function (OldPassword, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [OldPassword, { user_id: UserId }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Update Register Field
    socialUpdated: function (register, info, userId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [register, { user_id: userId }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if(result){
                var querys = 'UPDATE tbl_clients SET ? WHERE ?';

                db.connection.query(querys, [info, { user_id: userId }], function (error, results, fields) {
                    if (error) {
                        deferred.reject(new Error(error));
                    }

                    deferred.resolve(results);                    
                });                
            }
            
        });
        return deferred.promise;
    },

    //Forget Password
    forgetPassword: function (Email) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof Email != 'undefined' && Email != '') {
            where += " user_username = '" + striptags(Email) + "'";
        }

        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            //console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            if (rows) {
                deferred.resolve(rows);
            }
        });
        return deferred.promise;
    },    

    validateUser: function (token) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof token != 'undefined' && token != '') {
            where += "device_token = '" + token + "' ";
        }

        // if (typeof usertype != 'undefined' && usertype != '') {
        //     where += " AND user_role_id = '" + usertype + "' ";
        // }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (error, result, fields) {

            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    logoutUser: function (usertype, token) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET device_token = "" WHERE user_id=' + usertype;

        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    logoutUserApp: function (user_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET device_token = "" WHERE user_id=' + user_id;

        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    CheckForgot: function (user_id, device_token) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof user_id != 'undefined' && user_id != '') {
            where += "user_id = '" + user_id + "' ";
        }

        if (typeof device_token != 'undefined' && device_token != '') {
            where += " AND device_token = '" + device_token + "' ";
        }

        // where += " AND user_registertype = 0";
        where += " AND status = 1";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    ChangeDeviceToken: function (device_token, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET device_token = "'+device_token+'" WHERE user_id=' + UserId;

        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    activeAccount: function (user_id, device_token, clients_referID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user JOIN tbl_clients ON tbl_user.user_id = tbl_clients.user_id WHERE ";
        var where = "";

        if (typeof user_id != 'undefined' && user_id != '') {
            where += "tbl_user.user_id = '" + user_id + "' ";
        }

        if (typeof device_token != 'undefined' && device_token != '') {
            where += " AND tbl_user.device_token = '" + device_token + "' ";
        }

        if (typeof clients_referID != 'undefined' && clients_referID != '') {
            where += " AND tbl_clients.clients_referID = '" + clients_referID + "' ";
        }

        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            
            if(result[0].status == 0){
                var querys = 'UPDATE tbl_user SET status = "1" WHERE user_id=' + user_id;

                db.connection.query(querys, function (err, rows, fields) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }
                
                    deferred.resolve(1);

                });
            }else{
                deferred.resolve(0);
            }           

        });
        return deferred.promise;
    }

};

module.exports = Login;