var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");
var commonHelper = require('../helper/common_helper.js');

var TodayBooking = {

    getBookings: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.booking_date, b.booking_time_from, w.workers_name, c.clients_name FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id WHERE is_deleted = 0  AND booking_date = '"+ today +"' AND booking_payment_status = '1' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },


    // getBookingsWeekly: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'booking_id', orderBy: 'DESC' }) {
    //     var today = commonHelper.getCurrentDate();
    //     var deferred = Q.defer();
    //     var query = "SELECT * FROM tbl_bookweekly WHERE is_deleted = 0  AND booking_date = '"+ today +"' AND booking_payment_status = '1' AND worker_status = '1' ";
    //     var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
    //     var where = this.returnWhere(searchParams);
    //     query += where;
    //     query += orderBy + " LIMIT " + offset + ", " + limit;
    //     query = mysql.format(query, where);

    //     db.connection.query(query, function (err, rows) {
    //         if (err) {
    //             deferred.reject(new Error(err));
    //         }
    //         deferred.resolve(rows);
    //     });
    //     return deferred.promise;
    // },


    singleBooking: function (booking_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_booking as b JOIN tbl_workers as w ON b.worker_id = w.user_id JOIN tbl_clients as c ON b.client_id = c.user_id JOIN tbl_tasktype as t ON t.tasktype_id = b.booking_service_type WHERE booking_id = '"+ booking_id +"' ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    countBookings: function (searchParams = {}) {
        var today = commonHelper.getCurrentDate();
        var deferred = Q.defer();
        var query = "SELECT COUNT(booking_id) as total FROM tbl_booking WHERE is_deleted = 0 AND booking_date = '"+ today +"' AND booking_payment_status = '1' ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Booking found!"));
            }
        });
        return deferred.promise;
    },

    // countBookingsWeekly: function (searchParams = {}) {
    //     var today = commonHelper.getCurrentDate();
    //     var deferred = Q.defer();
    //     var query = "SELECT COUNT(bookweekly_id) as total FROM tbl_bookweekly WHERE is_deleted = 0 AND booking_date = '"+ today +"' AND booking_payment_status = '1' AND worker_status = '1' ";
    //     var where = this.returnWhere(searchParams);
    //     query += where;
    //     query = mysql.format(query, where);

    //     db.connection.query(query, function (err, rows) {
    //         if (err) {
    //             deferred.reject(new Error(err));
    //         }

    //         if (rows) {
    //             deferred.resolve(rows);
    //         } else {
    //             deferred.reject(new Error("No Booking found!"));
    //         }
    //     });
    //     return deferred.promise;
    // },


    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.booking_id !== 'undefined' && searchParams.booking_id !== '') {
            where += " AND booking_id = " + searchParams.booking_id;
        }

        if (typeof searchParams.clients_name !== 'undefined' && searchParams.clients_name !== '') {
            where += " AND clients_name = " + searchParams.clients_name;
        }

        if (typeof searchParams.workers_name !== 'undefined' && searchParams.workers_name !== '') {
            where += " AND workers_name LIKE '%" + striptags(searchParams.workers_name) + "%' ";
        }

        if (typeof searchParams.booking_date !== 'undefined' && searchParams.booking_date !== '') {
            where += " AND DATE_FORMAT(booking_date, '%Y-%m-%d') = '" + searchParams.booking_date + "'";
        }

        return where;
    }


};

module.exports = TodayBooking;