var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var WalletTopUp = {

    topUpWallet: function (topuUpData, client_id, creditData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_wallet SET ?";
        
        db.connection.query(query, topuUpData, function (err, rows) {
            if (err) {
                deferred.reject(new Error(error));
            }
            if (rows) {

                var querys = "INSERT INTO tbl_referralcredit SET ?";
            
                db.connection.query(querys, creditData, function(errs, rows1) {
                    if (errs) {
                        deferred.reject(new Error(errs));
                    }
                    if(rows1) {
                        var query = "SELECT c.credit_points, u.device_token, u.device_type FROM tbl_clients as c LEFT JOIN tbl_user as u ON u.user_id = c.user_id WHERE c.user_id = "+client_id+" ";
                        db.connection.query(query, function (error, result, fields) {
                            if (error) {
                                deferred.reject(new Error(error));
                            }
                            if (result) {
                                
                                deferred.resolve(result);
                            }
                        });
                    }
                    
                });
            }
        });
        return deferred.promise;
    },

    updateWallet: function (client_id , amount) {
        var deferred = Q.defer();
            var querys = "UPDATE tbl_clients SET credit_points = "+amount+" where user_id = "+client_id+" ";
            
            db.connection.query(querys, function(errs, rows1) {
                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows1);
            });
        return deferred.promise;
    },    

    getAllClients: function (limit = 10, offset = 0, searchParams = {}, orderParams = {}) {
        
        var deferred = Q.defer();
        var query = "SELECT c.user_id, c.clients_name, c.clients_email, c.clients_phone, c.clients_mobile FROM tbl_user as u  JOIN tbl_clients as c ON u.user_id = c.user_id WHERE u.status = 1 AND u.is_suspend = 0 AND u.is_deleted = 0 ";

        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        // query += orderBy + " LIMIT " + offset + ", " + limit;
        // console.log(query);
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getCreditHistory: function (user_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_wallet WHERE  client_id = '"+ user_id +"' order by created_at DESC ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countWalletTopUp: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(c.user_id) as total FROM tbl_user as u  JOIN tbl_clients as c ON u.user_id = c.user_id WHERE u.status = 1 AND u.is_suspend = 0 AND u.is_deleted = 0 ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No data found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.contact !== 'undefined' && searchParams.contact !== '') {
            where += " AND ( c.clients_phone like '%" + searchParams.contact + "%' " ;
            where += " OR c.clients_mobile like '%" + searchParams.contact + "%' ) " ;
        }

        return where;
    }

};

module.exports = WalletTopUp;