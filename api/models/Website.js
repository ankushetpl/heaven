var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Website = {

    singleWeb: function (page_slug) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_pages WHERE ? AND status = 1 AND is_deleted = 0";
        db.connection.query(query, { page_slug: page_slug }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },    

    allTypeData: function () {
        var deferred = Q.defer();
        
        var query = "SELECT * FROM tbl_setting WHERE status = 1 AND is_deleted = 0 ; SELECT * FROM tbl_socialSetting WHERE status = 1 AND is_deleted = 0 AND defaults = 1 ; SELECT * FROM tbl_emailSetting WHERE status = 1 AND is_deleted = 0 AND defaults = 1 ; SELECT * FROM tbl_bannerSetting WHERE status = 1 AND is_deleted = 0; ";
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

};

module.exports = Website;