var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var User = {

    countUser: function (searchParams = {}, usertype) {
        var deferred = Q.defer();

        var table = '';
        if(usertype == 1 || usertype == 6){
            table = 'tbl_admin';
        }else{
            if(usertype == 2){
                table = 'tbl_cafeusers';
            }else{
                if(usertype == 3){
                    table = 'tbl_workers';
                }else{
                    if(usertype == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "SELECT COUNT(tbl_user.user_id) as total FROM tbl_user JOIN "+table+" on tbl_user.user_id = "+table+".user_id WHERE user_role_id = "+ usertype +" AND is_deleted = 0";
        var where = this.returnWhere(searchParams, usertype);
        query += where;

        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No User found!"));
            }
        });
        return deferred.promise;
    },

    getUser: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'tbl_user.user_id', orderBy: 'DESC' }, usertype) {
        var deferred = Q.defer();

        var table = '';
        if(usertype == 1 || usertype == 6){
            table = 'tbl_admin';
        }else{
            if(usertype == 2){
                table = 'tbl_cafeusers';
            }else{
                if(usertype == 3){
                    table = 'tbl_workers';
                }else{
                    if(usertype == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        if(orderParams.orderColumn == 'user_id'){
            orderParams.orderColumn = 'tbl_user.user_id';         
        }

        if(orderParams.orderColumn == 'cafeusers_name'){
            orderParams.orderColumn = table+'.cafeusers_name';         
        }

        if(orderParams.orderColumn == 'admin_name'){
            orderParams.orderColumn = table+'.admin_name';         
        }

        if(orderParams.orderColumn == 'assessors_name'){
            orderParams.orderColumn = table+'.assessors_name';         
        }

        if(orderParams.orderColumn == 'clients_name'){
            orderParams.orderColumn = table+'.clients_name';         
        }

        if(orderParams.orderColumn == 'workers_name'){
            orderParams.orderColumn = table+'.workers_name';         
        }
     
        var query = "SELECT tbl_user.*, "+table+".* FROM tbl_user JOIN "+table+" on tbl_user.user_id = "+table+".user_id WHERE user_role_id = "+ usertype +" AND is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams, usertype);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Delete User
    deleteUser: function(Delete, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [Delete, { user_id: UserId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Status User
    statusUser: function(Status, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [Status, { user_id: UserId }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    //Suspend User
    suspendUser: function(UserData, UserId, UserType, data) {
        var deferred = Q.defer();
        
        var table = '';
        if(UserType == 1 || UserType == 6){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "UPDATE "+ table +" SET ? WHERE ?";

        db.connection.query(query, [data, { user_id: UserId }], function(err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            var querys = "UPDATE tbl_user SET ? WHERE ?";
            db.connection.query(querys, [UserData, { user_id: UserId }], function(errs, rows1) {
                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows1);
            });
        });
        return deferred.promise;
    },

    // CHECK USERNAME EXISTS OR NOT
    checkUserExists: function(username) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user WHERE ";
        var where = "";

        if (typeof username != 'undefined' && username != '') {
            where += " user_username = '" + striptags(username) + "'";
        }

        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // CHECK USER EMAIL EXISTS OR NOT
    checkUserEmailExists: function(email) {
        var deferred = Q.defer();
        // var query = "SELECT * FROM `tbl_user`, `tbl_cafeusers`, `tbl_clients`, `tbl_workers`, `tbl_assessors` WHERE ";
        var query = "SELECT * FROM `tbl_user` WHERE ";
        var where = "";

        if (typeof email != 'undefined' && email != '') {
            where += " `tbl_user`.`user_username` = '" + email + "' ";
            // where += " OR ( `tbl_cafeusers`.`cafeusers_email` = '" + email + "' )";
            // where += " OR ( `tbl_clients`.`clients_email` = '" + email + "' )";
            // where += " OR ( `tbl_workers`.`workers_email` = '" + email + "' )";
            // where += " OR ( `tbl_assessors`.`assessors_email` = '" + email + "' )";
        }

        // where += " AND status = 1";
        where += " AND is_deleted = 0;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // ADD CAFE USER INTO MAIN LOGIN TABLE
    addUser: function(data, Info, UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1 || UserType == 6){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "INSERT INTO tbl_user SET ?";

        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            // deferred.resolve(result.insertId);
            Info.user_id = result.insertId;

            var querys = "INSERT INTO "+ table +" SET ?";
            db.connection.query(querys, Info, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(result.insertId);
            });

        });
        return deferred.promise;
    },   

    // UPDATE USER DATA
    updateUser: function(user_id, UserType, Data, userData) {
        var deferred = Q.defer();
        
        var table = '';
        if(UserType == 1 || UserType == 6){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }

        var query = "UPDATE "+ table +" SET ? WHERE ?";

        db.connection.query(query, [Data, { user_id: user_id }], function(err, rows) {

            if (err) {
                deferred.reject(new Error(err));
            }
            var querys = "UPDATE tbl_user SET ? WHERE ?";
            db.connection.query(querys, [userData, { user_id: user_id }], function(errs, rows1) {
                if (errs) {
                    deferred.reject(new Error(errs));
                }
                deferred.resolve(rows1);
            });
        });
        return deferred.promise;
    },

    //Get User Details
    userDetails: function(UserId, Usertype) {
        var table = '';
        if(Usertype == 1 || Usertype == 6){
            table = 'tbl_admin';
        }else{
            if(Usertype == 2){
                table = 'tbl_cafeusers';
            }else{
                if(Usertype == 3){
                    table = 'tbl_workers';
                }else{
                    if(Usertype == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user JOIN "+ table +" ON tbl_user.user_id = "+ table +".user_id WHERE";
        var where = "";

        if (typeof UserId != 'undefined' && UserId != '') {
            where += " tbl_user.user_id = '" + (UserId) + "';";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            //console.log(query);
            if (err) {
                //console.log(err);
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },


    // ALL FILES DATA
    getImages: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },    


    returnWhere: function (searchParams = {}, usertype) {
        var where = "";

        var table = '';
        var colName = '';
        if(usertype == 1 || usertype == 6){
            table = 'tbl_admin';
            colName = 'admin_name';
        }else{
            if(usertype == 2){
                table = 'tbl_cafeusers';
                colName = 'cafeusers_name';
            }else{
                if(usertype == 3){
                    table = 'tbl_workers';
                    colName = 'workers_name';
                }else{
                    if(usertype == 4){
                        table = '   tbl_assessors';
                        colName = 'assessors_name';
                    }else{
                        table = 'tbl_clients';
                        colName = 'clients_name';
                    }
                }
            }
        }

        if (typeof searchParams.user_id !== 'undefined' && searchParams.user_id !== '') {
            where += " AND tbl_user.user_id = " + searchParams.user_id;
        }

        if (typeof searchParams.admin_name !== 'undefined' && searchParams.admin_name !== '') {
            where += " AND "+table+"."+colName+" LIKE '%" + striptags(searchParams.admin_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        if (typeof searchParams.is_suspend !== 'undefined' && searchParams.is_suspend !== '') {
            where += " AND is_suspend = " + searchParams.is_suspend;
        }

        return where;
    },

    singleUser: function (user_id, UserType) {
        var deferred = Q.defer();

        var table = '';
        
        if(UserType == 1 || UserType == 6){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        
        var query = "SELECT * FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id WHERE  ";

        var where = "";

        if (typeof user_id != 'undefined' && user_id != '') {
            where += " tbl_user.user_id = '" + user_id + "'";
        }

        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getAllUser: function (UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1 || UserType == 6){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        var query = "SELECT * FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id  WHERE  ";
        var where = "";

        if (typeof UserType != 'undefined' && UserType != '') {
            where += " tbl_user.user_role_id = '" + UserType + "'";
        }

        where += " AND status = 1";
        where += " AND user_passCount = 0";
        where += " AND is_suspend = 0";
        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllSubUser: function (cafeuser_id) {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_workers LEFT JOIN tbl_user ON tbl_workers.user_id  = tbl_user.user_id  WHERE  ";
        var where = "";

        if (typeof cafeuser_id != 'undefined' && cafeuser_id != '') {
            where +="tbl_workers.cafeuser_id = '" + cafeuser_id + "'";
        }

        where += " AND tbl_user.status = 1";
        where += " AND tbl_user.is_suspend = 0";
        where += " AND tbl_user.is_deleted = 0";

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    savePermissions: function(user_id, module, currentDate){
        
        var deferred = Q.defer();
        var total = 0;
        var query = "INSERT INTO tbl_permissions SET ? ";

        for(var i = 0; i < module.length; i++){
            
            var data = {}
            if(module[i].add == true){
                var add = 1;
            }else{
                var add = 0;
            } 

            if(module[i].edite == true){
                var edite = 1;
            }else{
                var edite = 0;
            } 

            if(module[i].delete == true){
                var deletes = 1;
            }else{
                var deletes = 0;
            } 

            data = { modules_id : module[i].module_id, user_id : user_id, added_at : add, updated_at : edite, deleted_at : deletes, created_date : currentDate };

            db.connection.query(query, data, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                total = total + 1;

                if(total == module.length ) {
                    deferred.resolve(rows);    
                }                
            });
        }  
       
        return deferred.promise;
    },

    viewPermissions: function (user_id) {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_permissions WHERE ";
        var where = "";

        if (typeof user_id !== 'undefined' && user_id !== '') {
            where += "user_id = '" + user_id + "' ;";
        }

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    checkStatus: function (user_id) {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_workers JOIN tbl_user on tbl_workers.user_id = tbl_user.user_id WHERE tbl_user.status = 1 AND ";
        var where = "";

        if (typeof user_id !== 'undefined' && user_id !== '') {
            where += "tbl_workers.cafeuser_id = '" + user_id + "' ;";
        }

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getTemplete: function (templete) {
        var deferred = Q.defer();

        var query = "SELECT * FROM tbl_email WHERE status = 1 AND defaults = 1 AND is_deleted = 0 AND ";
        var where = "";

        if (typeof templete !== 'undefined' && templete !== '') {
            where += "email_type = '" + templete + "' ;";
        }

        query += where;
        query = mysql.format(query, where);
        
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },


    getCafeName: function (id) {
        var deferred = Q.defer();

        var query = "SELECT cafeusers_name FROM tbl_cafeusers WHERE user_id = "+id+" ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },


    getWorkerName: function (id) {
        var deferred = Q.defer();

        var query = "SELECT workers_name FROM tbl_workers WHERE user_id = "+id+" ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    }



};

module.exports = User;