var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var FileUpload = {

    addFileUpload: function (fileData) {        
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_files SET ?";        
        
        db.connection.query(query, fileData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getAllFiles: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files ORDER BY `file_id` DESC ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleFile: function (file_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_files WHERE ? AND is_deleted = 0";
        db.connection.query(query, { file_id: file_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    }

};

module.exports = FileUpload;