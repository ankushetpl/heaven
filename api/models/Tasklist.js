var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Tasklist = {

    addTasklist: function (tasklistData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_tasklist SET ?";
        tasklistData.tasklist_name = striptags(tasklistData.tasklist_name);
        tasklistData.tasklist_des = striptags(tasklistData.tasklist_des);

        db.connection.query(query, tasklistData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getTasklist: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'tasklist_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT tasklist_id, tasklist_servicetype, tasklist_name, tasklist_des, status, created_at FROM tbl_tasklist WHERE is_deleted = 0 AND tasklist_client_id = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleTasklist: function (tasklist_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tasklist WHERE ? AND is_deleted = 0";
        db.connection.query(query, { tasklist_id: tasklist_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateTasklist: function (tasklistData, tasklist_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tasklist SET ? WHERE ?';
        tasklistData.tasklist_name = striptags(tasklistData.tasklist_name);
        tasklistData.tasklist_des = striptags(tasklistData.tasklist_des);

        db.connection.query(query, [tasklistData, { tasklist_id: tasklist_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusTasklist: function(Status, tasklist_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tasklist SET ? WHERE ?';

        db.connection.query(query, [Status, { tasklist_id: tasklist_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteTasklist: function (tasklist_id) {
        var deferred = Q.defer();
        var tasklist_id = tasklist_id;
        var query = "UPDATE tbl_tasklist SET is_deleted = 1 WHERE tasklist_id = " + tasklist_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countTasklist: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(tasklist_id) as total FROM tbl_tasklist WHERE is_deleted = 0 AND tasklist_client_id = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Tasklist found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.tasklist_id !== 'undefined' && searchParams.tasklist_id !== '') {
            where += " AND tasklist_id = " + searchParams.tasklist_id;
        }

        if (typeof searchParams.tasklist_servicetype !== 'undefined' && searchParams.tasklist_servicetype !== '') {
            where += " AND tasklist_servicetype = " + searchParams.tasklist_servicetype;
        }

        if (typeof searchParams.tasklist_name !== 'undefined' && searchParams.tasklist_name !== '') {
            where += " AND tasklist_name LIKE '%" + striptags(searchParams.tasklist_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Tasklist;