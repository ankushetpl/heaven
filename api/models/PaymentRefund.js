var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var WalletTopUp = {

    topUpWallet: function (topuUpData, client_id, refundData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_wallet SET ?";
        
        db.connection.query(query, refundData, function (err, rows) {
            if (err) {
                deferred.reject(new Error(error));
            }
            if (rows) {

                var querys = "INSERT INTO tbl_referralcredit SET ?";
            
                db.connection.query(querys, topuUpData, function(errs, rows1) {
                    if (errs) {
                        deferred.reject(new Error(errs));
                    }
                    if(rows1) {
                        var queryS = "SELECT c.credit_points, u.device_token, u.device_type FROM tbl_clients as c LEFT JOIN tbl_user as u ON u.user_id = c.user_id WHERE c.user_id = "+client_id+" ";
                        db.connection.query(queryS, function (error, result, fields) {
                            if (error) {
                                deferred.reject(new Error(error));
                            }
                            if (result) {
                                
                                deferred.resolve(result);
                            }
                        });
                    }
                    
                });
            }
        });
        return deferred.promise;
    },

    updateWallet: function (client_id , amount, serviceflag, bookingId) {
        var deferred = Q.defer();
            var querys = "UPDATE tbl_clients SET credit_points = "+amount+" where user_id = "+client_id+" ";
            
            db.connection.query(querys, function(errs, rows1) {
                if (errs) {
                    deferred.reject(new Error(errs));
                }
                if(rows1) {

                    if(serviceflag == 0) {
                        var query = "UPDATE tbl_booking SET refund_status = 1 WHERE booking_id = "+bookingId+" ";
                    } else {
                        var query = "UPDATE tbl_bookweekly SET refund_status = 1 WHERE bookweekly_id = "+bookingId+" ";
                    }
                   
                    db.connection.query(query, function (error, result, fields) {
                        if (error) {
                            deferred.reject(new Error(error));
                        }
                        if (result) {
                            
                            deferred.resolve(result);
                        }
                    }); 
                }
                deferred.resolve(rows1);
            });
        return deferred.promise;
    }, 

    singleRefundData: function (booking_id,serviceflag) {
        var deferred = Q.defer();
        if(serviceflag == '0') {
            var query = "SELECT booking_id, client_id, booking_date, booking_cost, booking_deduct, booking_once_weekly from tbl_booking WHERE booking_id = "+ booking_id +" ";
        } else {
            var query = "SELECT bookweekly_id, client_id, booking_date, booking_cost, booking_deduct, booking_once_weekly from tbl_bookweekly WHERE bookweekly_id = "+ booking_id +" ";
        }
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },       

    getAllRefundData: function (limit = 10, offset = 0, searchParams = {}, orderParams = {}) {
        
        var deferred = Q.defer();
        var query = "SELECT b.booking_id, b.client_id, b.booking_date, b.booking_cost, b.booking_deduct, b.booking_once_weekly, b.refund_status, c.clients_name from tbl_booking as b JOIN tbl_clients as c ON b.client_id = c.user_id where b.booking_deduct != 0 AND b.is_deleted = 1 AND b.booking_payment_status = 1 ";

        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countPaymentRefund: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(b.booking_id) as total from tbl_booking as b JOIN tbl_clients as c ON b.client_id = c.user_id  where b.booking_deduct != 0 AND b.refund_status = 0 AND b.is_deleted = 1 AND b.booking_payment_status = 1 ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No data found!"));
            }
        });
        return deferred.promise;
    },

    getAllRefundWeekData: function (limit = 10, offset = 0, searchParams = {}, orderParams = {}) {
        
        var deferred = Q.defer();
        var query = "SELECT b.bookweekly_id as booking_id, b.client_id, b.booking_date, b.booking_cost, b.booking_deduct, b.booking_once_weekly, b.refund_status, c.clients_name from tbl_bookweekly as b JOIN tbl_clients as c ON b.client_id = c.user_id where b.booking_deduct != 0 AND b.is_deleted = 1 AND b.booking_payment_status = 1 ";

        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query);
        db.connection.query(query, function (err, rows) {
            
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countPaymentRefundWeek: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(b.bookweekly_id) as total from tbl_bookweekly as b JOIN tbl_clients as c ON b.client_id = c.user_id where b.booking_deduct != 0 AND b.refund_status = 0 AND b.is_deleted = 1 AND b.booking_payment_status = 1 ";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No data found!"));
            }
        });
        return deferred.promise;
    },


    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.booking_id !== 'undefined' && searchParams.booking_id !== '') {
            where += " AND b.booking_id = " + searchParams.booking_id ;
        }

        return where;
    }

};

module.exports = WalletTopUp;