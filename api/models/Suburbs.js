var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Suburbs = {

    addSuburbs: function (suburbsData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_suburbs SET ?";
        suburbsData.suburbs_pick_name = striptags(suburbsData.suburbs_pick_name);
        suburbsData.suburbs_drop_name = striptags(suburbsData.suburbs_drop_name);

        db.connection.query(query, suburbsData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getSuburbs: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'suburbs_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_suburbs WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleSuburbs: function (suburbs_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_suburbs WHERE ? AND is_deleted = 0";
        db.connection.query(query, { suburbs_id: suburbs_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateFaq: function (faqData, faq_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_faq SET ? WHERE ?';
        faqData.faq_question = striptags(faqData.faq_question);
        faqData.faq_answer = striptags(faqData.faq_answer);

        db.connection.query(query, [faqData, { faq_id: faq_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusSuburbs: function(Status, suburbs_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_suburbs SET ? WHERE ?';

        db.connection.query(query, [Status, { suburbs_id: suburbs_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteSuburbs: function (suburbs_id) {
        var deferred = Q.defer();
        var suburbs_id = suburbs_id;
        var query = "UPDATE tbl_suburbs SET is_deleted = 1 WHERE suburbs_id = " + suburbs_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countSuburbs: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(suburbs_id) as total FROM tbl_suburbs WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No suburbs found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.suburbs_id !== 'undefined' && searchParams.suburbs_id !== '') {
            where += " AND suburbs_id = " + searchParams.suburbs_id;
        }

        if (typeof searchParams.suburbs_pick_name !== 'undefined' && searchParams.suburbs_pick_name !== '') {
            where += " AND suburbs_pick_name LIKE '%" + striptags(searchParams.suburbs_pick_name) + "%' ";
        }

        if (typeof searchParams.suburbs_drop_name !== 'undefined' && searchParams.suburbs_drop_name !== '') {
            where += " AND suburbs_drop_name LIKE '%" + striptags(searchParams.suburbs_drop_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.ride_status !== 'undefined' && searchParams.ride_status !== '') {
            where += " AND ride_status = " + searchParams.ride_status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Suburbs;