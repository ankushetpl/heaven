var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Rating = {

    addRating: function (ratingData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_rating SET ?";
        ratingData.rating_name = striptags(ratingData.rating_name);

        db.connection.query(query, ratingData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getRating: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'rating_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_rating WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleRating: function (rating_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_rating WHERE ? AND is_deleted = 0";
        db.connection.query(query, { rating_id: rating_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateRating: function (ratingData, rating_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_rating SET ? WHERE ?';
        ratingData.rating_name = striptags(ratingData.rating_name);

        db.connection.query(query, [ratingData, { rating_id: rating_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusRating: function(Status, rating_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_rating SET ? WHERE ?';

        db.connection.query(query, [Status, { rating_id: rating_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteRating: function (rating_id) {
        var deferred = Q.defer();
        var rating_id = rating_id;
        var query = "UPDATE tbl_rating SET is_deleted = 1 WHERE rating_id = " + rating_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countRating: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(rating_id) as total FROM tbl_rating WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Rating found!"));
            }
        });
        return deferred.promise;
    },

    getServices: function () {
        var deferred = Q.defer();
        var query = "SELECT tasktype_id as id, tasktype_name as name FROM tbl_tasktype WHERE status = 1 AND is_deleted = 0";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    }, 

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.rating_id !== 'undefined' && searchParams.rating_id !== '') {
            where += " AND rating_id = " + searchParams.rating_id;
        }

        if (typeof searchParams.rating_type !== 'undefined' && searchParams.rating_type !== '') {
            where += " AND rating_type = " + searchParams.rating_type;
        }

        if (typeof searchParams.rating_name !== 'undefined' && searchParams.rating_name !== '') {
            where += " AND rating_name LIKE '%" + striptags(searchParams.rating_name) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = Rating;