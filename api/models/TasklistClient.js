var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var TasklistClient = {

    getTasklistClient: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'tasklist_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tasklist WHERE is_deleted = 0 AND tasklist_client_id != 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleTasklistClient: function (tasklist_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_tasklist JOIN tbl_clients ON tbl_tasklist.tasklist_client_id = tbl_clients.user_id WHERE tbl_tasklist.tasklist_id = '"+ tasklist_id +"' AND is_deleted = 0";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    statusTasklistClient: function(Status, tasklist_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tasklist SET ? WHERE ?';

        db.connection.query(query, [Status, { tasklist_id: tasklist_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    updateTasklist: function (tasklistData, tasklist_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_tasklist SET ? WHERE ?';
        tasklistData.tasklist_name = striptags(tasklistData.tasklist_name);
        tasklistData.tasklist_des = striptags(tasklistData.tasklist_des);

        db.connection.query(query, [tasklistData, { tasklist_id: tasklist_id }], function (error, result, fields) {
            
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },    

    deleteTasklistClient: function (tasklist_id) {
        var deferred = Q.defer();
        var tasklist_id = tasklist_id;
        var query = "UPDATE tbl_tasklist SET is_deleted = 1 WHERE tasklist_id = " + tasklist_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },    

    countTasklistClient: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(tasklist_id) as total FROM tbl_tasklist WHERE is_deleted = 0 AND tasklist_client_id != 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No Client Tasklist found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.tasklist_id !== 'undefined' && searchParams.tasklist_id !== '') {
            where += " AND tasklist_id = " + searchParams.tasklist_id;
        }

        if (typeof searchParams.tasklist_name !== 'undefined' && searchParams.tasklist_name !== '') {
            where += " AND tasklist_name LIKE '%" + striptags(searchParams.tasklist_name) + "%' ";
        }

        if (typeof searchParams.tasklist_servicetype !== 'undefined' && searchParams.tasklist_servicetype !== '') {
            where += " AND tasklist_servicetype = " + searchParams.tasklist_servicetype;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    }

};

module.exports = TasklistClient;