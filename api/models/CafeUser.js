var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var commonHelper = require('../helper/common_helper.js');

var CafeUser = {

    // CHECK USER PHONE NUMBER EXISTS OR NOT
    checkUserPhoneExists: function(UserNumber) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_workers` JOIN `tbl_user` ON `tbl_workers`.`user_id` = `tbl_user`.`user_id` WHERE `workers_phone` = '" + UserNumber + "' AND is_deleted = 0; SELECT * FROM `tbl_cafeusers` JOIN `tbl_user` ON `tbl_cafeusers`.`user_id` = `tbl_user`.`user_id` WHERE `cafeusers_phone` = '" + UserNumber + "' AND is_deleted = 0; SELECT * FROM `tbl_clients` JOIN `tbl_user` ON `tbl_clients`.`user_id` = `tbl_user`.`user_id` WHERE `clients_phone` = '" + UserNumber + "' AND is_deleted = 0; SELECT * FROM `tbl_admin` JOIN `tbl_user` ON `tbl_admin`.`user_id` = `tbl_user`.`user_id` WHERE `admin_phone` = '" + UserNumber + "' AND is_deleted = 0; SELECT * FROM `tbl_assessors` JOIN `tbl_user` ON `tbl_assessors`.`user_id` = `tbl_user`.`user_id` WHERE `assessors_phone` = '" + UserNumber + "' AND is_deleted = 0;";
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // CHECK USER EMAIL EXISTS OR NOT
    checkUserEmailExists: function(email) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_user` WHERE `user_username` = '" + email + "' AND is_deleted = 0; SELECT * FROM `tbl_workers` JOIN `tbl_user` ON `tbl_workers`.`user_id` = `tbl_user`.`user_id` WHERE `workers_email` = '" + email + "' AND is_deleted = 0;";
        
        query = mysql.format(query);

        db.connection.query(query, function(err, result, fields) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    // CHECK USER ID OR PASSPORT EXISTS OR NOT
    checkIdPassportExists: function(workers_idPassport, user_id) {
        var deferred = Q.defer();
        if(user_id == 0) {
            var query = "SELECT * FROM `tbl_workers` WHERE `workers_idPassport` = '" + workers_idPassport + "' ";
        } else {
            var query = "SELECT * FROM `tbl_workers` WHERE `workers_idPassport` = '" + workers_idPassport + "' AND user_id != '"+user_id+"' ";
        }
        
        query = mysql.format(query);
        db.connection.query(query, function(err, result, fields) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },   

    // CHECK USER ASYLUM NUMBER EXISTS OR NOT
    checkAsylumExists: function(workers_asylum, user_id) {
        var deferred = Q.defer();
        if(user_id == 0) {
            var query = "SELECT * FROM `tbl_workers` WHERE `workers_asylum` = '" + workers_asylum + "' ";
        } else {
            var query = "SELECT * FROM `tbl_workers` WHERE `workers_asylum` = '" + workers_asylum + "' AND user_id != '"+user_id+"' ";
        }
        
        query = mysql.format(query);
        db.connection.query(query, function(err, result, fields) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    // GET ALL SUBURBS ACCORDING TO CITY ID
    getAllSuburbByCity: function(cityID) {
        var deferred = Q.defer();
        var query = "SELECT suburb_id, suburb_name FROM `tbl_suburb` WHERE `city` = '" + cityID + "' AND `status` = '1' AND is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, result, fields) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },            

    // ADD WORKER
    addUser: function(data, Info) {
        var deferred = Q.defer();

        var query = "INSERT INTO tbl_user SET ?";
        db.connection.query(query, data, function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            // deferred.resolve(result.insertId);
            Info.user_id = result.insertId;
            var querys = "INSERT INTO tbl_workers SET ?";
            db.connection.query(querys, Info, function(err, rows) {
                if (err) {
                    deferred.reject(new Error(err));
                }
                deferred.resolve(rows);
            });
        });
        return deferred.promise;
    },

    updateUser: function (user_id, Data, userData) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_workers SET ? WHERE ?';        

        var queryt = db.connection.query(query, [Data, { user_id: user_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }

            if(result){
                var querys = 'UPDATE tbl_user SET ? WHERE ?';      
                db.connection.query(querys, [userData, { user_id: user_id }], function (err, rows, fields) {
                    if (err) {
                        deferred.reject(new Error(err));
                    }

                    deferred.resolve(rows);
                });
            }
            
        });
        return deferred.promise;
    },

    updateImage: function (user_id, Data) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_workers SET ? WHERE ?';        

        var queryt = db.connection.query(query, [Data, { user_id: user_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }

            if(result){
                deferred.resolve(result);
            }
            
        });
        return deferred.promise;
    },

    // ALL SKILLS LIST
    getAllSkills: function() {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_skills WHERE is_deleted = 0 ";
        
        query = mysql.format(query);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },   
    
    singleWorker: function (user_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_user JOIN tbl_workers on tbl_user.user_id = tbl_workers.user_id WHERE tbl_user.user_id = "+user_id+" AND tbl_user.is_deleted = 0";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    getWorker: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'workers_id', orderBy: 'DESC' }, userID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_workers JOIN tbl_user on tbl_workers.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_workers.workers_image = tbl_files.file_id WHERE tbl_user.is_deleted = 0 AND tbl_workers.cafeuser_id = '"+userID+"'";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countWorkers: function (searchParams = {}, userID) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(workers_id) as total FROM tbl_workers JOIN tbl_user on tbl_workers.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_workers.workers_image = tbl_files.file_id WHERE tbl_user.is_deleted = 0 AND tbl_workers.cafeuser_id = '"+ userID +"' ";
        
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No worker found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.workers_name !== 'undefined' && searchParams.workers_name !== '') {
            where += " AND tbl_workers.workers_name LIKE '%" + striptags(searchParams.workers_name) + "%' ";
        }

        if (typeof searchParams.workers_phone !== 'undefined' && searchParams.workers_phone !== '') {
            where += " AND tbl_workers.workers_phone = " + searchParams.workers_phone;
        }

        if (typeof searchParams.workers_suburb !== 'undefined' && searchParams.workers_suburb !== '') {
            where += " AND FIND_IN_SET('" + searchParams.workers_suburb +"', tbl_workers.workers_suburb ) ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            if(searchParams.status == 3){
                where += " AND is_suspend = 1";
            }else{
                where += " AND status = " + searchParams.status;
            }
        }

        if (typeof searchParams.start_at !== 'undefined' && searchParams.start_at !== '' && typeof searchParams.end_at !== 'undefined' && searchParams.end_at !== '') {
            
            where += " AND ( DATE_FORMAT(created_at, '%Y-%m-%d') >= '" + searchParams.start_at + "' AND DATE_FORMAT(created_at, '%Y-%m-%d') <= '" + searchParams.end_at + "') ";
        

        }else{
            if (typeof searchParams.start_at !== 'undefined' && searchParams.start_at !== '') {
                where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') >= '" + searchParams.start_at + "'";
            }

            if (typeof searchParams.end_at !== 'undefined' && searchParams.end_at !== '') {
                where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') <= '" + searchParams.end_at + "'";
            }
        }        
        
        return where;
    },

    getAllCountries: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM countries";
        
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllStates: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM states ORDER BY  `states`.`name` ASC ";
        
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllCities: function () {
        var deferred = Q.defer();
        var query = "SELECT * FROM cities ORDER BY  `cities`.`name` ASC ";
        
        query = mysql.format(query);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllSuburbs: function (ID) {
        var deferred = Q.defer();
        var query = "SELECT suburb_id, suburb_name FROM tbl_suburb WHERE is_deleted = 0";
        
        var where = "";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllStatesByID: function (ID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM states WHERE ";
        
        var where = "";

        if (typeof ID !== 'undefined' && ID !== '') {
            where += " country_id = " + ID + ";";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllCitiesByID: function (ID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM cities WHERE ";
        
        var where = "";

        if (typeof ID !== 'undefined' && ID !== '') {
            where += " state_id = " + ID + ";";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllSuburbByID: function (ID) {
        var deferred = Q.defer();
        var query = "SELECT suburb_id, suburb_name FROM tbl_suburb WHERE is_deleted = 0 AND ";
        
        var where = "";

        if (typeof ID !== 'undefined' && ID !== '') {
            where += " city = " + ID + ";";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAllSuburbByMID: function (ID, searchSuburb) {
        var deferred = Q.defer();
        var query = "SELECT suburb_id, suburb_name FROM tbl_suburb WHERE is_deleted = 0 ";
        
        var where = "";

        if (typeof searchSuburb.suburb1ID !== 'undefined' && searchSuburb.suburb1ID !== '') {
            where += " AND suburb_id != " + searchSuburb.suburb1ID + " ";
        }

        if (typeof searchSuburb.suburb2ID !== 'undefined' && searchSuburb.suburb2ID !== '') {
            where += " AND suburb_id != " + searchSuburb.suburb2ID + " ";
        }

        if (typeof searchSuburb.suburb3ID !== 'undefined' && searchSuburb.suburb3ID !== '') {
            where += " AND suburb_id != " + searchSuburb.suburb3ID + " ";
        }

        if (typeof searchSuburb.suburb4ID !== 'undefined' && searchSuburb.suburb4ID !== '') {
            where += " AND suburb_id != " + searchSuburb.suburb4ID + " ";
        }   

        if (typeof ID !== 'undefined' && ID !== '') {
            where += " AND city = " + ID + ";";
        }     

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // Backend
    getAllWorker: function (user_id, UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1 || UserType == 6 ){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = 'tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        
        var query = "SELECT * FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id LEFT JOIN tbl_files ON " + table + ".workers_image = tbl_files.file_id WHERE ";      

        var where = "";

        if (typeof user_id != 'undefined' && user_id != '') {
            where += table+".cafeuser_id = '" + striptags(user_id) + "'";
        }

        where += " AND tbl_user.is_deleted = 0";

        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    // Backend 
    countWorker: function (user_id, UserType) {
        var deferred = Q.defer();

        var table = '';
        if(UserType == 1 || UserType == 6 ){
            table = 'tbl_admin';
        }else{
            if(UserType == 2){
                table = 'tbl_cafeusers';
            }else{
                if(UserType == 3){
                    table = 'tbl_workers';
                }else{
                    if(UserType == 4){
                        table = '   tbl_assessors';
                    }else{
                        table = 'tbl_clients';
                    }
                }
            }
        }
        
        var query = "SELECT DISTINCT(SELECT COUNT(*) FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id WHERE  " + table + ".cafeuser_id = '" + user_id + "' AND is_deleted = 0 AND status = 0) as inactive, (SELECT COUNT(*) FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id WHERE  " + table + ".cafeuser_id = '" + user_id + "' AND is_deleted = 0 AND status = 1) as active, (SELECT COUNT(*) FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id WHERE  " + table + ".cafeuser_id = '" + user_id + "' AND is_deleted = 0 AND is_suspend = 1) as suspend FROM tbl_user LEFT JOIN " + table + " ON tbl_user.user_id  = " + table + ".user_id  WHERE ";

        var where = "";

        if (typeof user_id != 'undefined' && user_id != '') {
            where += table+".cafeuser_id = '" + user_id + "'";
        }

        where += " AND is_deleted = 0";

        query += where;
        query = mysql.format(query, where);
        db.connection.query(query, function(err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    //Change Password
    ChangePassword: function (OldPassword, UserId) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_user SET ? WHERE ?';

        db.connection.query(query, [OldPassword, { user_id: UserId }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    checkAssessment: function (cafeId, workerId) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_assessorRequest` WHERE `cafe_id` = '"+ cafeId +"' AND `worker_id` = '"+ workerId +"' ";
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    checkAssessor: function (assessorID, start_at) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_assessorRequest` WHERE ";

        var where = '';        

        if (typeof assessorID !== 'undefined' && assessorID !== '') {
            where += "assessor_id = '" + assessorID + "' ";
        }

        if (typeof start_at !== 'undefined' && start_at !== '') {
            where += " AND DATE_FORMAT(request_date, '%Y-%m-%d') = '" + start_at + "'";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            // console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    checkLeave: function (assessorID, start_at) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_assessorLeave` WHERE ";

        var where = '';        

        if (typeof assessorID !== 'undefined' && assessorID !== '') {
            where += "assessor_id = '" + assessorID + "' ";
        }

        if (typeof start_at !== 'undefined' && start_at !== '') {
            where += " AND DATE_FORMAT(assessorLeave_date, '%Y-%m-%d') = '" + start_at + "'";
        }

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    getAssessment: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'assessors_id', orderBy: 'DESC' }, getAssCountry) {

        var deferred = Q.defer();
        // var query = "SELECT * FROM tbl_assessors JOIN tbl_user on tbl_assessors.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_assessors.assessors_image = tbl_files.file_id WHERE tbl_user.is_deleted = 0 AND tbl_user.user_passCount = 0 AND tbl_user.status = 1 AND tbl_assessors.assessors_country = 202 ";

        var query = "SELECT * FROM tbl_user JOIN tbl_assessors on tbl_assessors.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_assessors.assessors_image = tbl_files.file_id WHERE tbl_user.is_deleted = 0 AND tbl_user.user_passCount = 0 AND tbl_user.status = 1 AND tbl_assessors.assessors_country = "+getAssCountry+" ";

        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhereAssessment(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            // console.log(query);
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countAssessment: function (searchParams = {}, getAssCountry) {
        var deferred = Q.defer();
        // var query = "SELECT COUNT(assessors_id) as total FROM tbl_assessors JOIN tbl_user on tbl_assessors.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_assessors.assessors_image = tbl_files.file_id WHERE tbl_user.is_deleted = 0 AND tbl_user.user_passCount = 0 AND tbl_user.status = 1 AND tbl_assessors.assessors_country = 202 ";

        var query = "SELECT COUNT(assessors_id) as total FROM tbl_user JOIN tbl_assessors on tbl_assessors.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_assessors.assessors_image = tbl_files.file_id WHERE tbl_user.is_deleted = 0 AND tbl_user.user_passCount = 0 AND tbl_user.status = 1 AND tbl_assessors.assessors_country = "+getAssCountry+" ";
        
        var where = this.returnWhereAssessment(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No worker found!"));
            }
        });
        return deferred.promise;
    },

    returnWhereAssessment: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.assessors_state !== 'undefined' && searchParams.assessors_state !== '') {
            where += " AND tbl_assessors.assessors_state = " + searchParams.assessors_state;
        }

        if (typeof searchParams.assessors_city !== 'undefined' && searchParams.assessors_city !== '') {
            where += " AND tbl_assessors.assessors_city = " + searchParams.assessors_city;
        }

        if (typeof searchParams.assessors_suburb !== 'undefined' && searchParams.assessors_suburb !== '') {
            where += " AND tbl_assessors.assessors_suburb = " + searchParams.assessors_suburb;
        }

        if (typeof searchParams.start_at !== 'undefined' && searchParams.start_at !== '') {
            where += " AND tbl_user.user_id NOT IN(SELECT assessor_id FROM tbl_assessorLeave WHERE DATE_FORMAT(tbl_assessorLeave.assessorLeave_date, '%Y-%m-%d') = '" + searchParams.start_at + "')";
        }

        return where;
    },

    singleAssessor: function (userID, assessmentDate) {
        var deferred = Q.defer();
        // var query = "Select * from tbl_user JOIN tbl_assessors on tbl_user.user_id = tbl_assessors.user_id LEFT JOIN tbl_assessorRequest on tbl_assessors.user_id = tbl_assessorRequest.assessor_id LEFT JOIN tbl_files on tbl_assessors.assessors_image = tbl_files.file_id where tbl_assessorRequest.assessor_id = "+userID+" AND tbl_user.is_deleted = 0 AND tbl_assessorRequest.status = 1 AND tbl_assessorRequest.request_date = '" +assessmentDate+ "';SELECT * FROM `tbl_setting` WHERE `setting_id` = 7; ";

        var query = "SELECT * FROM tbl_user JOIN tbl_assessors on tbl_user.user_id = tbl_assessors.user_id LEFT JOIN tbl_files on tbl_assessors.assessors_image = tbl_files.file_id  WHERE tbl_assessors.user_id = "+userID+" AND tbl_user.is_deleted = 0; SELECT * FROM `tbl_assessorRequest` WHERE `assessor_id` = "+userID+" AND status = '1' AND DATE_FORMAT(request_date, '%Y-%m-%d') = '" +assessmentDate+ "'; SELECT * FROM `tbl_setting` WHERE `setting_id` = 7; ";

        // var query = "SELECT * FROM tbl_assessorRequest as a JOIN tbl_workers as w ON a.worker_id = w.user_id JOIN tbl_files as f ON w.workers_image = f.file_id WHERE a.assessor_id = '"+ userID +"' AND a.request_date = '"+ assessmentDate +"' AND a.status = 1 AND a.is_deleted = 0 ; SELECT * FROM `tbl_setting` WHERE `setting_id` = 7; ";

        db.connection.query(query, function (error, result, fields) {
            // console.log(query);
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    checkAssessmentRequest: function (cafe_id, worker_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM `tbl_assessorRequest` WHERE `cafe_id`= '"+ cafe_id +"' AND `worker_id` = '"+ worker_id +"';";
        db.connection.query(query, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }, 

     //GET CAFE USER BY WORKER
     assessmentRequest: function(Data, notData, tab) {
        var deferred = Q.defer();

        if(tab != ''){
            var query = 'UPDATE tbl_assessorRequest SET ? WHERE ?';
            db.connection.query(query, [Data, { request_id: tab }], function (error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                if (result) {
                    notData.notification_detail_id = tab;
                    var querys = "INSERT INTO `tbl_notification` SET ?";
                    db.connection.query(querys, notData, function(err, rows) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(result);
                    });
                    
                }
            }); 
            return deferred.promise;

        }else{
            var query = "INSERT INTO tbl_assessorRequest SET ?";
            db.connection.query(query, Data, function (error, result, fields) {
                if (error) {
                    deferred.reject(new Error(error));
                }
                if (result) {

                    notData.notification_detail_id = result.insertId;
                    var querys = "INSERT INTO `tbl_notification` SET ?";
                    db.connection.query(querys, notData, function(err, rows) {
                        if (err) {
                            deferred.reject(new Error(err));
                        }
                        deferred.resolve(result.insertId);
                    });
                    
                }
            }); 
            return deferred.promise;
        }        
    },



    getBooking: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'request_id', orderBy: 'DESC' }, userID) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_assessorRequest JOIN tbl_user on tbl_assessorRequest.worker_id = tbl_user.user_id JOIN tbl_workers on tbl_workers.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_workers.workers_image = tbl_files.file_id WHERE tbl_assessorRequest.cafe_id = '"+ userID +"' AND tbl_assessorRequest.assessorRequest_next = '0' ";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhereBooking(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);
        
        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    countBooking: function (searchParams = {}, userID) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(request_id) as total FROM tbl_assessorRequest JOIN tbl_user on tbl_assessorRequest.worker_id = tbl_user.user_id JOIN tbl_workers on tbl_workers.user_id = tbl_user.user_id LEFT JOIN tbl_files on tbl_workers.workers_image = tbl_files.file_id WHERE tbl_assessorRequest.cafe_id = '"+ userID +"' AND tbl_assessorRequest.assessorRequest_next = '0' ";
        
        var where = this.returnWhereBooking(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No worker found!"));
            }
        });
        return deferred.promise;
    },

    returnWhereBooking: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.stateID !== 'undefined' && searchParams.stateID !== '') {
            where += " AND tbl_workers.workers_state = " + searchParams.stateID;
        }

        if (typeof searchParams.cityID !== 'undefined' && searchParams.cityID !== '') {
            where += " AND tbl_workers.workers_city = " + searchParams.cityID;
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND tbl_assessorRequest.status = " + searchParams.status;
        }

        if (typeof searchParams.start_at !== 'undefined' && searchParams.start_at !== '' && typeof searchParams.end_at !== 'undefined' && searchParams.end_at !== '') {
            
            where += " AND ( DATE_FORMAT(tbl_assessorRequest.request_date, '%Y-%m-%d') >= '" + searchParams.start_at + "' AND DATE_FORMAT(tbl_assessorRequest.request_date, '%Y-%m-%d') <= '" + searchParams.end_at + "') ";
        

        }else{
            if (typeof searchParams.start_at !== 'undefined' && searchParams.start_at !== '') {
                where += " AND DATE_FORMAT(tbl_assessorRequest.request_date, '%Y-%m-%d') >= '" + searchParams.start_at + "'";
            }

            if (typeof searchParams.end_at !== 'undefined' && searchParams.end_at !== '') {
                where += " AND DATE_FORMAT(tbl_assessorRequest.request_date, '%Y-%m-%d') <= '" + searchParams.end_at + "'";
            }
        }        
        
        return where;
    },


};

module.exports = CafeUser;