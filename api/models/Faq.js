var striptags = require('striptags');
var mysql = require('mysql');
var db = require('../config/database.js');
var Q = require("q");

var Faq = {

    addFaq: function (faqData) {
        var deferred = Q.defer();
        var query = "INSERT INTO tbl_faq SET ?";
        faqData.faq_question = striptags(faqData.faq_question);
        faqData.faq_answer = striptags(faqData.faq_answer);

        db.connection.query(query, faqData, function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            if (result) {
                deferred.resolve(result.insertId);
            }
        });
        return deferred.promise;
    },

    getFaqs: function (limit = 10, offset = 0, searchParams = {}, orderParams = { orderColumn: 'faq_id', orderBy: 'DESC' }) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_faq WHERE is_deleted = 0";
        var orderBy = " ORDER BY " + orderParams.orderColumn + " " + orderParams.orderBy + " ";
        var where = this.returnWhere(searchParams);
        query += where;
        query += orderBy + " LIMIT " + offset + ", " + limit;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

    singleFaq: function (faq_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_faq WHERE ? AND is_deleted = 0";
        db.connection.query(query, { faq_id: faq_id }, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows[0]);
        });
        return deferred.promise;
    },

    updateFaq: function (faqData, faq_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_faq SET ? WHERE ?';
        faqData.faq_question = striptags(faqData.faq_question);
        faqData.faq_answer = striptags(faqData.faq_answer);

        db.connection.query(query, [faqData, { faq_id: faq_id }], function (error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    statusFaq: function(Status, faq_id) {
        var deferred = Q.defer();
        var query = 'UPDATE tbl_faq SET ? WHERE ?';

        db.connection.query(query, [Status, { faq_id: faq_id }], function(error, result, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    },

    deleteFaq: function (faq_id) {
        var deferred = Q.defer();
        var faq_id = faq_id;
        var query = "UPDATE tbl_faq SET is_deleted = 1 WHERE faq_id = " + faq_id;
        db.connection.query(query, function (error, results, fields) {
            if (error) {
                deferred.reject(new Error(error));
            }
            deferred.resolve(results);
        });
        return deferred.promise;
    },

    countFaq: function (searchParams = {}) {
        var deferred = Q.defer();
        var query = "SELECT COUNT(faq_id) as total FROM tbl_faq WHERE is_deleted = 0";
        var where = this.returnWhere(searchParams);
        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }

            if (rows) {
                deferred.resolve(rows);
            } else {
                deferred.reject(new Error("No FAQ found!"));
            }
        });
        return deferred.promise;
    },

    returnWhere: function (searchParams = {}) {
        var where = "";

        if (typeof searchParams.faq_id !== 'undefined' && searchParams.faq_id !== '') {
            where += " AND faq_id = " + searchParams.faq_id;
        }

        if (typeof searchParams.role_id !== 'undefined' && searchParams.role_id !== '') {
            where += " AND role_id = " + searchParams.role_id;
        }

        if (typeof searchParams.faq_question !== 'undefined' && searchParams.faq_question !== '') {
            where += " AND faq_question LIKE '%" + striptags(searchParams.faq_question) + "%' ";
        }

        if (typeof searchParams.faq_answer !== 'undefined' && searchParams.faq_answer !== '') {
            where += " AND faq_answer LIKE '%" + striptags(searchParams.faq_answer) + "%' ";
        }

        if (typeof searchParams.status !== 'undefined' && searchParams.status !== '') {
            where += " AND status = " + searchParams.status;
        }

        if (typeof searchParams.created_at !== 'undefined' && searchParams.created_at !== '') {
            where += " AND DATE_FORMAT(created_at, '%Y-%m-%d') = '" + searchParams.created_at + "'";
        }

        return where;
    },

    //API
    getAllFaqs: function (role_id) {
        var deferred = Q.defer();
        var query = "SELECT * FROM tbl_faq WHERE ";
        
        var where = "";

        if (typeof role_id !== 'undefined' && role_id !== '') {
            where += " role_id = " + role_id;
        }

        where += " AND status = 1 ";
        where += " AND is_deleted = 0 ;";

        query += where;
        query = mysql.format(query, where);

        db.connection.query(query, function (err, rows) {
            if (err) {
                deferred.reject(new Error(err));
            }
            deferred.resolve(rows);
        });
        return deferred.promise;
    },

};

module.exports = Faq;