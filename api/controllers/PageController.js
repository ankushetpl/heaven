var fs = require('fs');
var Page = require('../models/Page');
var commonHelper = require('../helper/common_helper.js');

var PageController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            page_id: req.body.page_id,
            page_name: req.body.page_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Page.countPage(searchParams).then(function (result) {
            total = result[0].total;
            Page.getPages(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "page": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var page_id = req.body.page_id;

        if (typeof page_id !== 'undefined' && page_id !== '') {
            Page.singlePage(page_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "page": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var pageName = req.body.page_name;
        var lowerCaseName = pageName.toLowerCase();
        lowerCaseName = lowerCaseName.replace(/ /g, "-");
        var pageData = {
            page_name: req.body.page_name,
            page_slug: lowerCaseName,
            page_html: req.body.page_html,
            page_location: req.body.page_location,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Page.addPage(pageData).then(function (result) {
            var response = { "status": 1, "message": "Page created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Page create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var page_id = req.body.page_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var pageData = {
            page_name: req.body.page_name,
            page_html: req.body.page_html,
            page_location: req.body.page_location,
            status: req.body.status,
            updated_at: currentDate
        };

        Page.updatePage(pageData, page_id).then(function (result) {
            var response = { "status": 1, "message": "Page updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Page update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Page!" };
        var page_id = req.body.page_id;

        Page.deletePage(page_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Page record has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var page_id = req.body.page_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Page.statusPage(data, page_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Page Actived" };                
            }else{
                var response = { "status": 1, "message": "Page Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionAllPage: function(req, res, next) {
        Page.allPage().then(function(result) {
            var response = { "status": 1, "message": "Page Found", "data": result };                
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = PageController;