var fs = require('fs');
var Support = require('../models/Support');
var User = require('../models/User');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: { 
        user: 'etpl18node@gmail.com', 
        pass: 'nodetest123' 
    }
});

var SupportController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            contactUs_id: req.body.contactUs_id,
            contactUs_from: req.body.contactUs_from,
            contactUs_subject: req.body.contactUs_subject,
            contactUs_type: req.body.contactUs_type,
            created_at: req.body.created_at
        };
        
        Support.countData(searchParams).then(function (result) {
            total = result[0].total;
            Support.getRecords(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "supportMail": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    // Send conatct us mail to admin
    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var contactUs_name     = req.body.contactUs_name;
        var contactUs_from     = req.body.contactUs_from;
        var contactUs_subject  = req.body.contactUs_subject;
        var contactUs_msg   = req.body.contactUs_msg;
        
        var mailData = { 
            contactUs_name : contactUs_name, 
            contactUs_from : contactUs_from,
            contactUs_subject : contactUs_subject, 
            contactUs_msg : contactUs_msg, 
            contactUs_type : 1, 
            created_at : currentDate, 
            updated_at : currentDate 
        };

        Support.addContact(mailData).then(function(resultM) {
            if(resultM > 0) {
                User.getTemplete('Contact Us').then(function(resultM) { 
                    var URL = '';
                    var html = resultM.email_content;
                   
                    var mailOptions = { 
                        to: contactUs_from, 
                        from: "Heavenly Sent <etpl18node@gmail.com>",
                        subject: 'Thank you for contact Us - '+contactUs_subject,  
                        html: html 
                    };

                    transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                            var response = {
                                "status": 1, 
                                "message": "Thank You, Your request has been sent successfully."
                            };
                            res.json(response);
                        } else {
                            var response = {
                                "status": 1, 
                                "message": "Thank You, Your request has been sent successfully."
                            };
                            res.json(response);                               
                        }                             
                    });

                }).catch(function(error) {
                    var response = {
                        "status": 1, 
                        "message": "Thank You, Your request has been sent successfully."
                    };
                    res.json(response);
                }); 
            } else {
                var response = { "status": 0, "message": "Failed to sent request, Please try again later" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Failed to sent request, Please try again later." };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var contactUs_id = req.body.contactUs_id;

        if (typeof contactUs_id !== 'undefined' && contactUs_id !== '') {
            Support.singleRecord(contactUs_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "supportMail": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionUpdate: function (req, res, next) {
        var contactUs_id = req.body.contactUs_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var mailData = {
            contactUs_replyMsg: req.body.contactUs_replyMsg,
            contactUs_replyStatus: '1',
            updated_at: currentDate
        };

        Support.updateRecord(mailData, contactUs_id).then(function (result) {

            Support.singleRecord(contactUs_id).then(function (resultSR) {

                if(resultSR.contactUs_type == 0) { // support

                    User.getTemplete('Contact Us').then(function(resultM) { 
                    var URL = '';
                    var html = resultM.email_content;
                    html = html.replace('{{TempMessage}}', mailData.contactUs_replyMsg);

                        var mailOptions = { 
                            to: resultSR.contactUs_from, 
                            from: "Heavenly Sent <etpl18node@gmail.com>",
                            subject: resultSR.contactUs_replySubject,  
                            html: html 
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                var response = { "status": 1, "message": "Reply send successfully" };
                                res.json(response);
                            } else {
                                var response = { "status": 1, "message": "Reply send successfully" };
                                res.json(response);                              
                            }                             
                        });

                    }).catch(function(error) {
                        var response = { "status": 1, "message": "Reply send successfully" };
                        res.json(response);
                    }); 

                } else {

                    User.getTemplete('Contact Us').then(function(resultM) { 
                    var URL = '';
                    var html = resultM.email_content;
                    html = html.replace('{{TempMessage}}', mailData.contactUs_replyMsg);

                        var mailOptions = { 
                            to: resultSR.contactUs_from, 
                            from: "Heavenly Sent <etpl18node@gmail.com>",
                            subject: resultSR.contactUs_replySubject,  
                            html: html 
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                var response = { "status": 1, "message": "Reply send successfully" };
                                res.json(response);
                            } else {
                                var response = { "status": 1, "message": "Reply send successfully" };
                                res.json(response);                              
                            }                             
                        });

                    }).catch(function(error) {
                        var response = { "status": 1, "message": "Reply send successfully" };
                        res.json(response);
                    }); 
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });                

        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to send reply" };
            res.json(response);
        });
    }
}

module.exports = SupportController;