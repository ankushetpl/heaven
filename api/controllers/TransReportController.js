var fs = require('fs');
var TransReport = require('../models/TransReport');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var TransReportController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            payment_id: req.body.payment_id,
            payment_reference: req.body.payment_reference,
            payment_type: req.body.payment_type,
            payment_trans_date: req.body.payment_trans_date
        };

        TransReport.countTranx().then(function (result) {
            total = result[0].total;
            TransReport.getTranx(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "tranx": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var payment_id = req.body.payment_id;

        if (typeof payment_id !== 'undefined' && payment_id !== '') {
            TransReport.singleTranx(payment_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "tranx": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionAllTranxData: function (req, res, next) {
        
        TransReport.getAllTransactionData().then(function (result) {

            if(result.length > 0) {

                result.forEachAsync(function(element, index, arr) {
                    element.payment_trans_date = dateFormat(element.payment_trans_date, "yyyy-mm-dd hh:MM:ss");

                    if(element.payment_type = 0) {
                        element.payment_type = 'Booking';
                    } else {
                        element.payment_type = 'Credit Purchase';
                    }
                    
                }, function() {
                    var response = { "status": 1, "message": "Record Found", "tranxData": result};
                    res.json(response);
                });

                    

            } else {
                var response = { "status": 0, "message": "No data found"};
                res.json(response);
            }

        }).catch(function (error) {

            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
       
    }

}

module.exports = TransReportController;