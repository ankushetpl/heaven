var fs = require('fs');
var LeaveManage = require('../models/LeaveManage');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');
var site_url = commonHelper.getSiteURL();

var LeaveManageController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            workerleaves_id: req.body.workerleaves_id,
            worker_id: req.body.worker_id,
            workers_name: req.body.workers_name,
            workerleaves_from: req.body.workerleaves_from,
            workerleaves_leaves_id: req.body.workerleaves_leaves_id
        };
        
        LeaveManage.countLeaves(searchParams).then(function (result) {
            total = result[0].total;
            LeaveManage.getLeavesData(limit, offset, searchParams, orderParams).then(function (result) {

                var response = { "status": 1, "message": "Record Found", "workerLeaves": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var workerleaves_id = req.body.workerleaves_id;

        if (typeof workerleaves_id !== 'undefined' && workerleaves_id !== '') {

            LeaveManage.singleLeave(workerleaves_id).then(function (result) {

                if (typeof result != 'undefined' && result != '') {

                    if(result.workerleaves_file != '') {

                        LeaveManage.getFileUrl(result.workerleaves_file).then(function (resultImage) {

                            result.workerleaves_file = site_url + resultImage.file_base_url;

                            var response = { "status": 1, "message": "Record Found", "workerLeaves": result };
                            res.json(response);

                        }).catch(function (error) {
                            var response = { "status": 0, "message": "No Record Exist" };
                            res.json(response);
                        });

                    } else {

                        var response = { "status": 1, "message": "Record Found", "workerLeaves": result };
                        res.json(response);
                    }
                    
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    // Leave Reasons list
    actionAllLeaveType: function(req, res, next) {
        
        LeaveManage.allLeaveType().then(function(results) {
            if(results.length > 0) {
                var response = { "status": 1, "message": "leaves list", "reasons" : results };
                res.json(response);
            } else {
                var response = { "status": 1, "message": "No data found" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not Found" };
            res.json(response);
        });            
    }

}

module.exports = LeaveManageController;