var fs = require('fs');
var multer = require('multer');
var FileUpload = require('../models/FileUpload');
var commonHelper = require('../helper/common_helper.js');
var site_url = commonHelper.getSiteURL();

var FileUploadController = {

    actionIndex: function (req, res, next) {
        FileUpload.getAllFiles().then(function (result) {
            var response = { "status": 1, "message": "Record Found", "files": result };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },    

    actionCreate: function (req, res, next) {
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, __dirname + '/../../assets/uploads/');
            },
            filename: function (req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
            }
        });
        
        var upload = multer({ storage: storage }).single('file');

        upload(req, res, function (err) {
            var response = {};
            if (err) {
                response = {status: 1, message: err};
            }
    
            var fileData = {
                file_original_name: req.file.originalname,
                file_name: req.file.filename,                
                file_type: req.file.mimetype,
                file_size: req.file.size,
                file_base_path: req.file.path,
                file_base_url: site_url+'/assets/uploads/' + req.file.filename
            };

            FileUpload.addFileUpload(fileData).then(function (result) {
                var response = { status: 1, message: "File added successfully!", data: result };
                res.json(response);
            }).catch(function (error) {
                var response = { status: 0, message: "File add failed!" };
                res.json(response);
            });
        });
    },

    actionView: function (req, res, next) {
        var file_id = req.body.file_id;

        if (typeof file_id !== 'undefined' && file_id !== '') {
            FileUpload.singleFile(file_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "file": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }  

};

module.exports = FileUploadController;