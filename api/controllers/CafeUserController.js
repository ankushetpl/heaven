var fs = require('fs');
var md5 = require('md5');
var randomstring = require("randomstring");
var nodemailer = require('nodemailer');
var Array = require('node-array');
var dateFormat = require('dateformat');

var CafeUser = require('../models/CafeUser');
var User = require('../models/User');
var Client = require('../models/Client');
var commonHelper = require('../helper/common_helper.js');
var site_url = commonHelper.getSiteURL();

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: { 
        user: 'etpl18node@gmail.com', 
        pass: 'nodetest123' 
    }
});

var CafeUserController = {

    actionAllCountries: function (req, res, next) {
        var total = 0;

        CafeUser.getAllCountries().then(function (result) {
            total = result.length;            
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "countries": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "countries": '', "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllStates: function (req, res, next) {
        var total = 0;

        CafeUser.getAllStates().then(function (result) {
            total = result.length;            
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "states": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "states": '', "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllCities: function (req, res, next) {
        var total = 0;

        CafeUser.getAllCities().then(function (result) {
            total = result.length;            
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "citys": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "citys": '', "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllSuburs: function (req, res, next) {
        var total = 0;
        
        CafeUser.getAllSuburbs().then(function (result) {
            total = result.length;
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "area": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "area": [], "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllSkills: function (req, res, next) {
        var total = 0;
        
        CafeUser.getAllSkills().then(function (result) {
            total = result.length;
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "skill": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "skill": [], "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllStatesByID: function (req, res, next) {
        var total = 0;
        var ID = req.body.ID;

        CafeUser.getAllStatesByID(ID).then(function (result) {
            total = result.length;
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "states": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "states": '', "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllCitiesByID: function (req, res, next) {
        var total = 0;
        var ID = req.body.ID;

        CafeUser.getAllCitiesByID(ID).then(function (result) {
            total = result.length;
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "citys": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "citys": '', "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllSuburbByID: function (req, res, next) {
        var total = 0;
        var ID = req.body.ID;

        CafeUser.getAllSuburbByID(ID).then(function (result) {
            total = result.length;
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "suburb": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "suburb": [], "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllSuburbByMID: function (req, res, next) {
        var total = 0;
        var ID = req.body.ID;

        var suburb1 = req.body.suburb1;
        if (typeof suburb1 !== 'undefined' && suburb1 !== '') {
            var suburb1ID = suburb1; 
        }else{
            var suburb1ID = ''; 
        }

        var suburb2 = req.body.suburb2;
        if (typeof suburb2 !== 'undefined' && suburb2 !== '') {
            var suburb2ID = suburb2; 
        }else{
            var suburb2ID = ''; 
        }

        var suburb3 = req.body.suburb3;
        if (typeof suburb3 !== 'undefined' && suburb3 !== '') {
            var suburb3ID = suburb3; 
        }else{
            var suburb3ID = ''; 
        }

        var suburb4 = req.body.suburb4;
        if (typeof suburb4 !== 'undefined' && suburb4 !== '') {
            var suburb4ID = suburb4; 
        }else{
            var suburb4ID = ''; 
        }

        var searchSuburb = {
            suburb1ID: suburb1ID,
            suburb2ID: suburb2ID,
            suburb3ID: suburb3ID,
            suburb4ID: suburb4ID
        };
        
        CafeUser.getAllSuburbByMID(ID, searchSuburb).then(function (result) {
            total = result.length;
            if(total > 0){
                var response = { "status": 1, "message": "Record Found", "suburb": result, "total": total };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "No Record Found", "suburb": [], "total": 0 };
                res.json(response);
            }                
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },


    actionAllSuburbByIDWorker: function (req, res, next) {
        var total = 0;
        var ID = req.body.ID;

        CafeUser.getAllSuburbByCity(ID).then(function (result) {
            if (typeof result != 'undefined' && result != '') {

                result.forEachAsync(function(element, index, arr) {
                    element.existsFlag = '0';
                }, function() {

                    var response = { "status": 1, "message": "Record Found", "allSuburbData": result };
                    res.json(response);        
                });
                
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },



    actionAllSuburbByCityId: function (req, res, next) {
        var city_id = req.body.city_id;
        
        if (typeof city_id !== 'undefined' && city_id !== '') {
            CafeUser.getAllSuburbByCity(city_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {

                    result.forEachAsync(function(element, index, arr) {
                        element.existsFlag = '0';
                    }, function() {

                        var response = { "status": 1, "message": "Record Found", "allSuburbData": result };
                        res.json(response);        
                    });
                    
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    // Backend
    actionWorkerCount : function(req, res, next){
        var user_id = req.body.user_id;
        var UserType = req.body.UserType;
        var inactive = '';
        var active = '';
        var suspend = '';

        if (typeof user_id !== 'undefined' && user_id !== '') {
            CafeUser.countWorker(user_id, UserType).then(function (results) {
                inactive = results[0].inactive;
                active = results[0].active;
                suspend = results[0].suspend;
                CafeUser.getAllWorker(user_id, UserType).then(function (result) {
                    if (typeof result != 'undefined' && result != '') {
                        var response = { "status": 1, "message": "Record Found", "data": result, "inactive": inactive, "active": active, "suspend": suspend };
                        res.json(response);
                    } else {
                        var response = { "status": 0, "message": "No Record Exist" };
                        res.json(response);
                    }
                }).catch(function (error) {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                });
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionSavePassword: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        
        var UserId = req.body.user_id;
        var NewPassword = md5(req.body.user_password);
        var Username = req.body.user_username;

        var password = {
            user_password: NewPassword,
            updated_at: currentDate,
            user_passCount: 0
        };

        CafeUser.ChangePassword(password, UserId).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                    
                User.getTemplete('Change Password').then(function(resultM) {
                    var html = resultM.email_content;
                    html = html.replace('{{TempName}}', 'Dear');
                    html = html.replace('{{TempEmailID}}', Username);
                    html = html.replace('{{TempPassword}}', req.body.user_password);
                    
                    var mailOptions = { 
                        to: Username, 
                        from: "Heavenly Sent <etpl18node@gmail.com>",
                        subject: 'Change Password',  
                        html: html 
                    };

                    transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                            var response = {
                                "status": 1,
                                "message": "Password Changed, But Mail Not Send"
                            };
                            res.json(response);
                        } else {
                            var response = {
                                "status": 1,
                                "message": "Password Changed, Please Check Your Mail."
                            };
                            res.json(response);                               
                        }                             
                    });
                }).catch(function (error) {
                    var response = {
                        "status": 1,
                        "message": "Password Changed, But Mail Not Send"
                    };
                    res.json(response);
                });

            } else {
                var response = {
                    "status": 0,
                    "message": "Token don't match."
                };
                res.json(response);
            }

        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "Password change to failed"
            };
            res.json(response);
        });        
    },

    actionIndex: function (req, res, next) {

        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }
        
        var userID = req.body.userID;
        var searchParams = {
            workers_name: req.body.workers_name,
            workers_phone: req.body.workers_phone,
            workers_suburb: req.body.workers_suburb,
            status: req.body.status,
            start_at: req.body.start_at,
            end_at: req.body.end_at,
        };

        CafeUser.countWorkers(searchParams, userID).then(function (result) {
            total = result[0].total;
            CafeUser.getWorker(limit, offset, searchParams, orderParams, userID).then(function (results) {

        		if(results.length > 0) {
                    var response = { "status": 1, "message": "Record Found", "workers": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
        			res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                	res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var user_id = req.body.user_id;
       
        if (typeof user_id !== 'undefined' && user_id !== '') {
            CafeUser.singleWorker(user_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "worker": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var total = 0;

        var totalNumber = 0;
        var totalNumber1 = 0;
        var totalNumber2 = 0;
        var totalNumber3 = 0;
        var totalNumber4 = 0;
        var totalNumber5 = 0;

        var totalEmail = 0;
        var totalEmail1 = 0;
        var totalEmail2 = 0;

        // var suburb = [];
        var Password = randomstring.generate(8);
        var currentDate = commonHelper.getCurrentDateTime();
        
        var country = req.body.workers_country;
        if (typeof country !== 'undefined' && country !== '') {
            var countryID = country; 
        }else{
            var countryID = ''; 
        }

        var state = req.body.workers_state;
        if (typeof state !== 'undefined' && state !== '') {
            var stateID = state; 
        }else{
            var stateID = ''; 
        }

        var city = req.body.workers_city;
        if (typeof city !== 'undefined' && city !== '') {
            var cityID = city; 
        }else{
            var cityID = ''; 
        }

        // var suburb1 = req.body.suburb1;
        // if (typeof suburb1 !== 'undefined' && suburb1 !== '') {
        //     var selectedSuburb = suburb1.toString();
        // }

        // var suburb2 = req.body.suburb2;
        // if (typeof suburb2 !== 'undefined' && suburb2 !== '') {
        //     suburb.push(suburb2); 
        // }

        // var suburb3 = req.body.suburb3;
        // if (typeof suburb3 !== 'undefined' && suburb3 !== '') {
        //     suburb.push(suburb3); 
        // }

        // var suburb4 = req.body.suburb4;
        // if (typeof suburb4 !== 'undefined' && suburb4 !== '') {
        //     suburb.push(suburb4); 
        // }

        var imageID = req.body.workers_image;
        if (typeof imageID !== 'undefined' && imageID !== '') {
            var imagesID = imageID; 
        }else{
            var imagesID = ''; 
        }

        var mobileID = req.body.workers_mobile;
        if (typeof mobileID !== 'undefined' && mobileID !== '') {
            var mobileID = mobileID; 
        }else{
            var mobileID = ''; 
        }

        var messageID = req.body.worker_message;
        if (typeof messageID !== 'undefined' && messageID !== '') {
            var messageID = messageID; 
        }else{
            var messageID = ''; 
        }

        var Username = req.body.workers_phone;
        var UserNumber = req.body.workers_phone;  
        var Email = req.body.workers_email; 
        if (typeof Email !== 'undefined' && Email !== '') {
            Email = Email; 
        }else{
            Email = ''; 
        }
        var workers_idPassport = req.body.workers_idPassport; 
        var workers_asylum = req.body.workers_asylum; 

        var userData = {
            user_username: Username,
            user_password: md5(Password),
            user_role_id: 3,
            user_registertype: 0,
            device_type: 1,
            status: 0,
            created_at: currentDate,
            updated_at: currentDate
        };

        var addData = { 
            cafeuser_id: req.body.cafeuser_id,
            workers_name: req.body.workers_name, 
            workers_age: req.body.workers_age, 
            workers_birthDate:dateFormat(req.body.workers_birthDate, "yyyy-mm-dd"), 
            workers_email: Email, 
            workers_phone: UserNumber, 
            workers_mobile: mobileID, 
            workers_address: req.body.workers_address, 
            workers_city: cityID,
            workers_state: stateID,
            workers_country: countryID,
            workers_image: imagesID,
            workers_digitalAgreement: req.body.workers_digitalAgreement,
            workers_photoIdentity: req.body.workers_photoIdentity,
            workers_criminalReport: req.body.workers_criminalReport, 
            workers_codeConduct: req.body.workers_codeConduct,
            workers_expiryDateDay: req.body.workers_expiryDateDay,
            workers_expiryDateMonth: req.body.workers_expiryDateMonth,
            workers_expiryDateYear: req.body.workers_expiryDateYear,
            workers_suburb:'',
            workers_friendlyDogs: req.body.workers_friendlyDogs,
            workers_friendlyCats: req.body.workers_friendlyCats,
            worker_type: req.body.worker_type,
            worker_message: messageID,
            workers_idPassport:workers_idPassport,
            workers_asylum:workers_asylum,
            workers_uniform:req.body.workers_uniform
        }; 
        var suburbLength = 0;
        
        User.checkUserExists(Username).then(function(result) {
            total = result.length;
            if (total == 0) {
                CafeUser.checkUserPhoneExists(UserNumber).then(function(resultS) {
                    
                    totalNumber1 = resultS[0].length;
                    totalNumber2 = resultS[1].length;
                    totalNumber3 = resultS[2].length;
                    totalNumber4 = resultS[3].length;
                    totalNumber5 = resultS[4].length;
                    totalNumber  = totalNumber1 + totalNumber2 + totalNumber3 + totalNumber4 + totalNumber5;
                    
                    if (totalNumber == 0) {
                        var user_id = 0;
                        CafeUser.checkIdPassportExists(workers_idPassport,user_id).then(function(resultPass) {
                            if(resultPass.length == 0) {

                                CafeUser.checkAsylumExists(workers_asylum,user_id).then(function(resultAs) {
                                    if(resultAs.length == 0) {

                                        CafeUser.checkUserEmailExists(Email).then(function(results) {
                                        
                                            totalEmail1 = results[0].length;
                                            totalEmail2 = results[1].length;
                                            totalEmail = totalEmail1 + totalEmail2;

                                            CafeUser.getAllSuburbByCity(cityID).then(function(resultAllS) {
                                                suburbs = [];

                                                for(var i=0; i < resultAllS.length; i++) {
                                                     suburbs.push(resultAllS[i].suburb_id);
                                                }

                                                addData.workers_suburb = suburbs.toString();
                                               
                                                if (totalEmail == 0) {                                
                                                    CafeUser.addUser(userData, addData).then(function(result) {      
                                                        var response = { "status": 1, "message": "Worker Added Successfully" };
                                                        res.json(response);
                                                    }).catch(function(error) {
                                                        var response = { "status": 0, "message": "Failed to add worker." };
                                                        res.json(response);
                                                    });
                                                }else{
                                                    if(Email == '') {

                                                            CafeUser.addUser(userData, addData).then(function(result) {      
                                                            var response = { "status": 1, "message": "Worker added successfully" };
                                                            res.json(response);

                                                            }).catch(function(error) {
                                                                var response = { "status": 0, "message": "Failed to add worker." };
                                                                res.json(response);
                                                            });

                                                    } else {
                                                        var response = { "status": 0, "message": "Failed to add worker." };
                                                        res.json(response);
                                                    }
                                                    
                                                }

                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Failed to add worker." };
                                                res.json(response);
                                            }); 

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Try Again." };
                                            res.json(response);
                                        });
                                    } else {
                                        var response = { "status": 2, "message": "Asylum number already exists." };
                                        res.json(response);
                                    }
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Try Again." };
                                    res.json(response);
                                });

                            } else {
                                var response = { "status": 2, "message": "Passport/Id already exists." };
                                res.json(response);
                            }
                            
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Try Again." };
                            res.json(response);
                        });
                    }else{
                        var response = { "status": 2, "message": "Contact number already exist." };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Try Again." };
                    res.json(response);
                });
            } else {                
                var response = { "status": 2, "message": "Contact Number Already Exist." };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {

        var total = 0;
        var flag = 0;
        var flags = 0;

        var totalNumber1 = 0;
        var totalNumber2 = 0;
        var totalNumber3 = 0;
        var totalNumber4 = 0;
        var totalNumber5 = 0;

        var totalEmail1 = 0;
        var totalEmail2 = 0;

        // var suburb = [];
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id = req.body.user_id;
        
        var country = req.body.workers_country;
        if (typeof country !== 'undefined' && country !== '') {
            var countryID = country; 
        }else{
            var countryID = ''; 
        }

        var state = req.body.workers_state;
        if (typeof state !== 'undefined' && state !== '') {
            var stateID = state; 
        }else{
            var stateID = ''; 
        }

        var city = req.body.workers_city;
        if (typeof city !== 'undefined' && city !== '') {
            var cityID = city; 
        }else{
            var cityID = ''; 
        }

        var suburb1 = req.body.suburb1;
        if (typeof suburb1 !== 'undefined' && suburb1 !== '') {
            var selectedSuburb = suburb1.toString();
        }

        // var suburb2 = req.body.suburb2;
        // if (typeof suburb2 !== 'undefined' && suburb2 !== '') {
        //     suburb.push(suburb2); 
        // }

        // var suburb3 = req.body.suburb3;
        // if (typeof suburb3 !== 'undefined' && suburb3 !== '') {
        //     suburb.push(suburb3); 
        // }

        // var suburb4 = req.body.suburb4;
        // if (typeof suburb4 !== 'undefined' && suburb4 !== '') {
        //     suburb.push(suburb4); 
        // }

        var imageID = req.body.workers_image;
        if (typeof imageID !== 'undefined' && imageID !== '') {
            var imagesID = imageID; 
        }else{
            var imagesID = ''; 
        }

        var mobileID = req.body.workers_mobile;
        if (typeof mobileID !== 'undefined' && mobileID !== '') {
            var mobileID = mobileID; 
        }else{
            var mobileID = ''; 
        }

        var messageID = req.body.worker_message;
        if (typeof messageID !== 'undefined' && messageID !== '') {
            var messageID = messageID; 
        }else{
            var messageID = ''; 
        }

        var Username = req.body.workers_phone;
        var UserNumber = req.body.workers_phone;  
        var Email = req.body.workers_email; 

        var workers_idPassport = req.body.workers_idPassport; 
        var workers_asylum = req.body.workers_asylum; 

        var Data = { 
            cafeuser_id: req.body.cafeuser_id,
            workers_name: req.body.workers_name, 
            workers_age: req.body.workers_age, 
            workers_birthDate:dateFormat(req.body.workers_birthDate, "yyyy-mm-dd"), 
            workers_email: Email, 
            workers_phone: UserNumber, 
            workers_mobile: mobileID, 
            workers_address: req.body.workers_address, 
            workers_city: cityID,
            workers_state: stateID,
            workers_country: countryID,
            workers_image: imagesID,
            workers_digitalAgreement: req.body.workers_digitalAgreement,
            workers_photoIdentity: req.body.workers_photoIdentity,
            workers_criminalReport: req.body.workers_criminalReport, 
            workers_codeConduct: req.body.workers_codeConduct,
            workers_expiryDateDay: req.body.workers_expiryDateDay,
            workers_expiryDateMonth: req.body.workers_expiryDateMonth,
            workers_expiryDateYear: req.body.workers_expiryDateYear,
            workers_suburb: selectedSuburb,
            workers_friendlyDogs: req.body.workers_friendlyDogs,
            workers_friendlyCats: req.body.workers_friendlyCats,
            worker_type: req.body.worker_type,
            worker_message: messageID,
            workers_idPassport:workers_idPassport,
            workers_asylum:workers_asylum,
            workers_uniform:req.body.workers_uniform 
        };
        
        var userData = { 
            updated_at: currentDate 
        };

        User.checkUserExists(Username).then(function(result) {
            total = result.length;
            if (total == 1 && result[0].user_id == user_id) {

                CafeUser.checkIdPassportExists(workers_idPassport,user_id).then(function(resultPass) {
                    if(resultPass.length == 0) {

                        CafeUser.checkAsylumExists(workers_asylum,user_id).then(function(resultAs) {
                            if(resultAs.length == 0) {

                                CafeUser.checkUserPhoneExists(UserNumber).then(function(resultS) {
                                    totalNumber1 = resultS[0].length;
                                    totalNumber2 = resultS[1].length;
                                    totalNumber3 = resultS[2].length;
                                    totalNumber4 = resultS[3].length;
                                    totalNumber5 = resultS[4].length;

                                    if(totalNumber1 == 0){
                                        flag = 1;
                                    }

                                    if(totalNumber2 == 0){
                                        flag = 1;
                                    }

                                    if(totalNumber3 == 0){
                                        flag = 1;
                                    }

                                    if(totalNumber4 == 0){
                                        flag = 1;
                                    }

                                    if(totalNumber5 == 0){
                                        flag = 1;
                                    }


                                    if (totalNumber1 == 1 && resultS[0][0].user_id == user_id) {
                                        flag = 1;
                                    }else{
                                        if(totalNumber1 > 1){
                                            flag = 0;
                                        }
                                    }

                                    if (totalNumber2 == 1 && resultS[1][0].user_id == user_id) {
                                        flag = 1;
                                    }else{
                                        if(totalNumber2 > 1){
                                            flag = 0;
                                        }
                                    }

                                    if (totalNumber3 == 1 && resultS[2][0].user_id == user_id) {
                                        flag = 1;
                                    }else{
                                        if(totalNumber3 > 1){
                                            flag = 0;
                                        }
                                    }

                                    if (totalNumber4 == 1 && resultS[3][0].user_id == user_id) {
                                        flag = 1;
                                    }else{
                                        if(totalNumber4 > 1){
                                            flag = 0;
                                        }
                                    }

                                    if (totalNumber5 == 1 && resultS[4][0].user_id == user_id) {
                                        flag = 1;
                                    }else{
                                        if(totalNumber5 > 1){
                                            flag = 0;
                                        }
                                    }
                                    
                                    if (flag == 1) {
                                        CafeUser.checkUserEmailExists(Email).then(function(results) {
                                            totalEmail1 = results[0].length;
                                            totalEmail2 = results[1].length;

                                            if(totalEmail1 == 0){
                                                flags = 1;
                                            }

                                            if(totalEmail2 == 0){
                                                flags = 1;
                                            }

                                            if (totalEmail1 == 1 && results[0][0].user_id == user_id) {
                                                flags = 1;
                                            }else{
                                                if(totalEmail1 > 1){
                                                    flags = 0;
                                                }
                                            }

                                            if (totalEmail2 == 1 && results[1][0].user_id == user_id) {
                                                flags = 1;
                                            }else{
                                                if(totalEmail2 > 1){
                                                    flags = 0;
                                                }
                                            }

                                            if (flags == 1) {                                
                                                CafeUser.updateUser(user_id, Data, userData).then(function(result) {
                                                    var response = { "status": 1, "message": "Worker Updated Successfully" };
                                                    res.json(response);
                                                }).catch(function(error) {
                                                    var response = { "status": 0, "message": "Worker Updated failed" };
                                                    res.json(response);
                                                });
                                            }else{
                                                var response = { "status": 2, "message": "Email ID Already Exist." };
                                                res.json(response);
                                            }
                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Try Again." };
                                            res.json(response);
                                        });
                                    }else{
                                        var response = { "status": 2, "message": "Contact Number Already Exist." };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Try Again." };
                                    res.json(response);
                                });
                            
                            } else {
                                var response = { "status": 2, "message": "Asylum number already exists." };
                                res.json(response);
                            }
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Try Again." };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 2, "message": "Passport/Id already exists." };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Try Again." };
                    res.json(response);
                });

            } else {                
                var response = { "status": 2, "message": "Contact Number Already Exist." };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });
    },

    actionUpdateImage: function (req, res, next) {

        var user_id = req.body.user_id;        
        var imageType = req.body.imageType;
        if (typeof imageType !== 'undefined' && imageType !== '') {
            if(imageType == 1){
                var Data = { workers_image: req.body.fileImage };
            }

            if(imageType == 2){
                var Data = { workers_photoIdentity: req.body.fileImage };
            }

            if(imageType == 3){
                var Data = { workers_criminalReport: req.body.fileImage };
            }

            if(imageType == 4){
                var Data = { workers_codeConduct: req.body.fileImage };
            }
        }

        CafeUser.updateImage(user_id, Data).then(function(result) {
            if(imageType == 1 || imageType == 2){
                var response = { "status": 1, "message": "Image Updated Successfully" };
            }else{
                var response = { "status": 1, "message": "File Updated Successfully" };
            }
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Updated failed" };
            res.json(response);
        });
                            
    },

    actionCheckAssessment: function (req, res, next) {
        var cafeId = req.body.cafe_id;
        var workerId = req.body.worker_id;

        CafeUser.checkAssessment(cafeId, workerId).then(function (result) {
            // console.log(result);
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found", "assessment": result };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },

    actionCheckAssessor: function (req, res, next) {
        var assessorID = req.body.assessorID;
        var start_at = req.body.start_at;

        CafeUser.checkAssessor(assessorID, start_at).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found", "assessment": result };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },

    actionCheckLeave: function (req, res, next) {
        var assessorID = req.body.assessorID;
        var start_at = req.body.start_at;
        var total = 0;

        CafeUser.checkLeave(assessorID, start_at).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                total = result.length;
                var response = { "status": 1, "message": "Record Found", "dataCheck": total };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },

    actionAssessmentList: function (req, res, next) {

        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var startDate = commonHelper.getCurrentDatePlusOne();
        if(typeof req.body.start_at != 'undefined' && req.body.start_at != '' ){
            startDate = req.body.start_at;
        }
        
        var searchParams = {
            assessors_state: req.body.stateID,
            assessors_city: req.body.cityID,
            assessors_suburb: req.body.suburb,
            start_at: startDate
        };

        var getAssCountry = req.body.getAssCountry;

        CafeUser.countAssessment(searchParams, getAssCountry).then(function (result) {
            total = result[0].total;
            CafeUser.getAssessment(limit, offset, searchParams, orderParams, getAssCountry).then(function (results) {

                if(results.length > 0) {
                    var response = { "status": 1, "message": "Record Found", "assessors": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionViewAssessor: function (req, res, next) {
        var userID = req.body.user_id;
        var assessmentDate = req.body.start_at; 
       
        if (typeof userID !== 'undefined' && userID !== '') {
            CafeUser.singleAssessor(userID, assessmentDate).then(function (result) {
                console.log(result[0][0]);
                console.log(result);
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "assessor": result[0][0], "timeSlot": result[1], "defaultSlot": result[2][0] };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionBookAssessor: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var total = 0;
        var assessor_id   = req.body.assessor_id;
        var cafe_id   = req.body.cafe_id;
        var cafe_name   = req.body.cafe_name;
        var worker_id   = req.body.worker_id;
        var request_date   = req.body.request_date;
        var request_timeFrom   = req.body.request_timeFrom;
        var request_timeTo   = req.body.request_timeTo;
        
        CafeUser.checkAssessmentRequest(cafe_id, worker_id).then(function(resultC) {
            total = resultC.length;
            
            if(total == 0){
                var Data = {
                    "cafe_id": cafe_id, 
                    "worker_id" : worker_id, 
                    "assessor_id" : assessor_id, 
                    "request_assessLevel" : 0, 
                    "request_date" : request_date, 
                    "request_timeFrom" : request_timeFrom, 
                    "request_timeTo" : request_timeTo, 
                    "created_at" : currentDate, 
                    "updated_at" : currentDate 
                };

                var notData = { 
                    'user_id' : assessor_id, 
                    'notification_type' : 'Internet Cafe Assessment Request', 
                    'notification_detail_id' : '', 
                    'notification_msg' : 'You have new assessment request from '+ cafe_name +' Cafe.', 
                    'created_at': currentDate, 
                    'updated_at': currentDate 
                };

                var tab = '';

                CafeUser.assessmentRequest(Data, notData, tab).then(function(result) {
                    if(typeof result != 'undefined' && result != '') {
                        var response = { "status": 1, "message" : "Your request has been sent successfully." };
                        res.json(response);
                    } else {
                        var response = { "status": 0, "message" : "Failed to send request" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to send request" };
                    res.json(response);
                });
            }else{

                if(total == 1){
                    var Data = {
                        "assessor_id" : assessor_id, 
                        "request_date" : request_date, 
                        "request_timeFrom" : request_timeFrom, 
                        "request_timeTo" : request_timeTo,
                        "status" : 0, 
                        "updated_at" : currentDate 
                    };

                    var notData = { 
                        'user_id' : assessor_id, 
                        'notification_type' : 'Internet Cafe Assessment Request', 
                        'notification_detail_id' : '', 
                        'notification_msg' : 'You have new assessment request from '+ cafe_name +' Cafe.', 
                        'created_at': currentDate, 
                        'updated_at': currentDate 
                    };

                    var tab = resultC[0].request_id;
                    
                    CafeUser.assessmentRequest(Data, notData, tab).then(function(result) {
                        
                        if(typeof result != 'undefined' && result != '') {
                            var response = { "status": 1, "message" : "Send request successfully for reschedule." };
                            res.json(response);
                        } else {
                            var response = { "status": 0, "message" : "Failed to send request" };
                            res.json(response);
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Failed to send request" };
                        res.json(response);
                    });
                }else{
                    var Data = {
                        "assessor_id" : assessor_id, 
                        "request_date" : request_date, 
                        "request_timeFrom" : request_timeFrom, 
                        "request_timeTo" : request_timeTo,
                        "status" : 0, 
                        "updated_at" : currentDate 
                    };

                    var notData = { 
                        'user_id' : assessor_id, 
                        'notification_type' : 'Internet Cafe Assessment Request', 
                        'notification_detail_id' : '', 
                        'notification_msg' : 'You have new assessment request from '+ cafe_name +' Cafe.', 
                        'created_at': currentDate, 
                        'updated_at': currentDate 
                    };

                    var tab = resultC[1].request_id;
                    
                    CafeUser.assessmentRequest(Data, notData, tab).then(function(result) {
                        
                        if(typeof result != 'undefined' && result != '') {
                            var response = { "status": 1, "message" : "Send request successfully for reschedule." };
                            res.json(response);
                        } else {
                            var response = { "status": 0, "message" : "Failed to send request" };
                            res.json(response);
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Failed to send request" };
                        res.json(response);
                    });
                }
            }
            

        }).catch(function(error) {
            var response = { "status": 0, "message": "Failed to send request" };
            res.json(response);
        });
    },

    actionBooking: function (req, res, next) {

        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }
        
        var userID = req.body.userID;
        var searchParams = {
            stateID: req.body.stateID,
            cityID: req.body.cityID,
            status: req.body.status,
            start_at: req.body.start_at,
            end_at: req.body.end_at,
        };

        CafeUser.countBooking(searchParams, userID).then(function (result) {
            total = result[0].total;
            CafeUser.getBooking(limit, offset, searchParams, orderParams, userID).then(function (results) {

                if(results.length > 0) {
                    var response = { "status": 1, "message": "Record Found", "booking": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }

}

module.exports = CafeUserController;