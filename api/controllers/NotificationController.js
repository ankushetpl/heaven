var fs = require('fs');
var Worker = require('../models/Worker');
var Client = require('../models/Client');
var Notification = require('../models/Notification');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');
// var site_url     = 'http://18.219.129.228/heaven';
var site_url = commonHelper.getSiteURL();

var NotificationController = {

    actionCreate: function (req, res, next) {

        var currentDate = commonHelper.getCurrentDateTime();

        var data = {
            user: req.body.user,
            message: req.body.message,
            notification_img : req.body.notification_img,
        };


        if(data.notification_img != 0) {

            Notification.getImages(data.notification_img).then(function (resultImage) {

                var image = site_url + resultImage[0].file_base_url;

                if(data.user == 0) { // send notification to all workers

                    Notification.allWorkers().then(function (result) {

                        result.forEachAsync(function(element, index, arr) {
                               
                            var msgtitle  = 'Normal';
                            var msgbody   = data.message;

                            var notSendData = { user_id : element.user_id, notification_type : msgtitle, notification_msg : msgbody, notification_img : image, created_at: currentDate, updated_at: currentDate };
                                
                            Worker.addNotificationData(notSendData).then(function(resultAN) {
                                
                                Worker.getNotResponse(resultAN).then(function(resultNR) {

                                    Worker.notifyBadgeCount(element.user_id).then(function(resultBC) {
                                        if (typeof resultBC != 'undefined' && resultBC != '') {

                                            var badge = resultBC.length;
                                        } else {
                                            var badge = 0;
                                        }

                                            Worker.sendNotification(element.device_token,msgtitle,msgbody,badge,resultNR).then(
                                                        function(resultCSN) {
                                                
                                                var response = { "status": 1, "message": "Send notification successfully" };
                                                res.json(response);                                                
                                                          
                                            }).catch(function (error) {
                                                var response = { "status": 1, "message": "Failed to send notification" };
                                                res.json(response);
                                            });
                                        
                                       
                                    }).catch(function (error) {
                                        var response = { "status": 1, "message": "Send notification successfully" };
                                        res.json(response);
                                    });

                                }).catch(function (error) {
                                    var response = { "status": 1, "message": "Send notification successfully" };
                                    res.json(response);
                                });


                            }).catch(function (error) {
                                var response = { "status": 0, "message": "Failed to add notification data" };
                                res.json(response);
                            });

                        });

                    }).catch(function (error) {

                        var response = { "status": 0, "message": "No worker found" };
                        res.json(response);

                    });

                } else { // client

                    Notification.allClients().then(function (result) {

                        result.forEachAsync(function(element, index, arr) {
                               
                            var msgtitle  = 'Normal';
                            var msgbody   = data.message;

                            var notSendData = { user_id : element.user_id, notification_type : msgtitle, notification_msg : msgbody, notification_img : image, created_at: currentDate, updated_at: currentDate };
                                
                            Worker.addNotificationData(notSendData).then(function(resultAN) {
                                
                                Worker.getNotResponse(resultAN).then(function(resultNR) {

                                    Worker.notifyBadgeCount(element.user_id).then(function(resultBC) {

                                        if (typeof resultBC != 'undefined' && resultBC != '') {
                                            var badge = resultBC.length;
                                        } else {
                                            var badge = 0;
                                        }    

                                            if(element.device_type == 2) {
                                                
                                                Client.sendNotificationClient(element.device_token,msgtitle,msgbody,badge,resultNR).then(
                                                    function(resultCSN) {
                                                      
                                                }).catch(function (error) {
                                                    var response = { "status": 0, "message": "Failed to send notification" };
                                                    res.json(response);
                                                });

                                            } else {
                                               
                                                Client.sendIOSNotification(element.device_token,msgtitle,msgbody,badge,resultNR).then(function(resultCSNI) {
                                                  
                                                    
                                                }).catch(function (error) {
                                                    var response = { "status": 0, "message": "Failed to send notification" };
                                                    res.json(response);
                                                });
                                            } 

                                            var response = { "status": 1, "message": "Send notification successfully" };
                                            res.json(response);
                                        
                                       
                                    }).catch(function (error) {
                                        var response = { "status": 1, "message": "Send notification successfully" };
                                        res.json(response);
                                    });

                                }).catch(function (error) {
                                    var response = { "status": 1, "message": "Send notification successfully" };
                                    res.json(response);
                                });


                            }).catch(function (error) {
                                var response = { "status": 0, "message": "Failed to add notification data" };
                                res.json(response);
                            });

                        });

                    }).catch(function (error) {

                        var response = { "status": 0, "message": "No worker found" };
                        res.json(response);

                    });
                } 

            }).catch(function (error) {
                var response = { "status": 0, "message": "Failed to get badge count" };
                res.json(response);
            });    

        } else {

            if(data.user == 0) { // send notification to all workers

                Notification.allWorkers().then(function (result) {

                    result.forEachAsync(function(element, index, arr) {
                           
                        var msgtitle  = 'Normal';
                        var msgbody   = data.message;

                        var notSendData = { user_id : element.user_id, notification_type : msgtitle, notification_msg : msgbody, created_at: currentDate, updated_at: currentDate };
                            
                        Worker.addNotificationData(notSendData).then(function(resultAN) {
                            
                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                Worker.notifyBadgeCount(element.user_id).then(function(resultBC) {
                                    
                                    if (typeof resultBC != 'undefined' && resultBC != '') {
                                        var badge = resultBC.length;
                                    } else {
                                        var badge = 0;
                                    }

                                        Worker.sendNotification(element.device_token,msgtitle,msgbody,badge,resultNR).then(
                                                    function(resultCSN) {
                                            
                                            var response = { "status": 1, "message": "Send notification successfully" };
                                            res.json(response);                                                
                                                      
                                        }).catch(function (error) {
                                            var response = { "status": 0, "message": "Failed to send notification" };
                                            res.json(response);
                                        });
                                    
                                   
                                }).catch(function (error) {
                                    var response = { "status": 1, "message": "Send notification successfully" };
                                    res.json(response);
                                });

                            }).catch(function (error) {
                                var response = { "status": 1, "message": "Send notification successfully" };
                                res.json(response);
                            });


                        }).catch(function (error) {
                            var response = { "status": 0, "message": "Failed to add notification data" };
                            res.json(response);
                        });

                    });

                }).catch(function (error) {

                    var response = { "status": 0, "message": "No worker found" };
                    res.json(response);

                });

            } else { // client

                Notification.allClients().then(function (result) {

                    result.forEachAsync(function(element, index, arr) {
                           
                        var msgtitle  = 'Normal';
                        var msgbody   = data.message;

                        var notSendData = { user_id : element.user_id, notification_type : msgtitle, notification_msg : msgbody, created_at: currentDate, updated_at: currentDate };
                            
                        Worker.addNotificationData(notSendData).then(function(resultAN) {
                            
                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                Worker.notifyBadgeCount(element.user_id).then(function(resultBC) {

                                    if (typeof resultBC != 'undefined' && resultBC != '') {
                                        var badge = resultBC.length;
                                    } else {
                                        var badge = 0;
                                    }
                                        if(element.device_type == 2) {
                                            
                                            Client.sendNotificationClient(element.device_token,msgtitle,msgbody,badge,resultNR).then(
                                                function(resultCSN) {
                                                  
                                            }).catch(function (error) {
                                                var response = { "status": 0, "message": "Failed to send notification" };
                                                res.json(response);
                                            });

                                        } else {
                                           
                                            Client.sendIOSNotification(element.device_token,msgtitle,msgbody,badge,resultNR).then(function(resultCSNI) {
                                              
                                                
                                            }).catch(function (error) {
                                                var response = { "status": 0, "message": "Failed to send notification" };
                                                res.json(response);
                                            });
                                        } 

                                        var response = { "status": 1, "message": "Send notification successfully" };
                                        res.json(response);
                                    
                                }).catch(function (error) {
                                    var response = { "status": 1, "message": "Send notification successfully" };
                                    res.json(response);
                                });

                            }).catch(function (error) {
                                var response = { "status": 1, "message": "Send notification successfully" };
                                res.json(response);
                            });


                        }).catch(function (error) {
                            var response = { "status": 0, "message": "Failed to add notification data" };
                            res.json(response);
                        });

                    });

                }).catch(function (error) {

                    var response = { "status": 0, "message": "No worker found" };
                    res.json(response);

                });
            } 

        }            
        
    }

}

module.exports = NotificationController;