var fs = require('fs');
var Skills = require('../models/Skills');
var commonHelper = require('../helper/common_helper.js');

var SkillsController = {

    actionIndex: function (req, res, next) {
        
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            skills_id  : req.body.skills_id,
            skills_name: req.body.skills_name,
            parent: req.body.parent,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Skills.countSkills(searchParams).then(function (result) {
            total = result[0].total;
            Skills.getSkills(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "skills": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    // view Skills data
    actionView: function (req, res, next) {
        var skills_id = req.body.skills_id;
        if (typeof skills_id !== 'undefined' && skills_id !== '') {
            Skills.singleSkill(skills_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "skills": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    // add skills data
    actionCreate: function (req, res, next) {
        
        var currentDate = commonHelper.getCurrentDateTime();
        var parent = req.body.parent;
         if (typeof parent !== 'undefined' && parent !== '') {        
            var skillsData = { 
                skills_name: req.body.skills_name, 
                status: req.body.status, 
                parent: parent, 
                created_at: currentDate, 
                updated_at : currentDate 
            };
        } else {
            var skillsData = { 
                skills_name: req.body.skills_name, 
                status: req.body.status,  
                created_at: currentDate, 
                updated_at : currentDate
            };
        }
          
        Skills.addSkills(skillsData).then(function (result) {
            var response = { "status": 1, "message": "Skill created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Skill create failed!" };
            res.json(response);
        });
    },

    //update  Skills data
    actionUpdate: function (req, res, next) {
        var skills_id = req.body.skills_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var parent = req.body.parent;
         if (typeof parent !== 'undefined' && parent !== '') {        
            var skillsData = { 
                skills_name: req.body.skills_name, 
                status: req.body.status, 
                parent: parent, 
                updated_at : currentDate 
            };
        } else {
            var skillsData = { 
                skills_name: req.body.skills_name,
                status: req.body.status, 
                updated_at: currentDate
            };
        }        
        
        Skills.updateSkills(skillsData, skills_id).then(function (result) {
            var response = { "status": 1, "message": "Skill updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Skill update failed!" };
            res.json(response);
        });
    },

    // delete Skills data
    actionDelete: function (req, res, next) {
        
        var response = { status: 0, message: "Unable to delete this skill!" };
        var skills_id = req.body.skills_id;

        Skills.deleteSkills(skills_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your skill has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    //Status skills
    actionStatus: function(req, res, next) {
        var SkillsId = req.body.skills_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Skills.statusSkills(data, SkillsId).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Skill Approved" };                
            }else{
                var response = { "status": 1, "message": "Skill Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionAllList: function (req, res, next) {
        var parent = req.body.parent;
        if (typeof parent !== 'undefined' && parent !== '') {
            Skills.getAllCatList(parent).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },      

    actionParent: function (req, res, next) {
        var parent = req.body.obj;
        if (typeof parent !== 'undefined' && parent !== '') {
            Skills.getParent(parent).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCheckData: function (req,res, next) {
        var parent = req.body.obj;
        if (typeof parent !== 'undefined' && parent !== '') {
            Skills.getCheckData(parent).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }

}

module.exports = SkillsController;