var fs = require('fs');
var Mail = require('../models/Mail');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var MailController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            email_id: req.body.email_id,
            email_type: req.body.email_type,
            status: req.body.status,
            created_at: req.body.created_at,
            defaults: req.body.defaults
        };

        Mail.countMail(searchParams).then(function (result) {
            total = result[0].total;
            Mail.getMail(limit, offset, searchParams, orderParams).then(function (results) {
                var response = { "status": 1, "message": "Record Found", "email": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllMail: function (req, res, next) {
        
        Mail.getAllMail().then(function (result) {
            var response = { "status": 1, "message": "All Email List", "email" : result };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
    }, 

    actionView: function (req, res, next) {
        var email_id = req.body.email_id;

        if (typeof email_id !== 'undefined' && email_id !== '') {
            Mail.singleMail(email_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "email": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var mailData = {
            email_type: req.body.email_type,
            email_text: req.body.email_text,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };
        
        Mail.addMail(mailData).then(function (result) {
            var response = { "status": 1, "message": "Email created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to create" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var email_id = req.body.email_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var mailData = {
            email_type: req.body.email_type,
            email_text: req.body.email_text,
            status: req.body.status,
            updated_at: currentDate
        };
        
        Mail.updateMail(mailData, email_id).then(function (result) {
            var response = { "status": 1, "message": "Email updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to update" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this email!" };
        var email_id = req.body.email_id;

        Mail.deleteMail(email_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your email has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var email_id = req.body.email_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Mail.statusMail(data, email_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Email approved" };                
            }else{
                var response = { "status": 1, "message": "Email disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionDefault: function(req, res, next) {
        var email_id = req.body.email_id;
        var Default = req.body.default;
        var emailType = req.body.email_type;
        var currentDate = commonHelper.getCurrentDateTime();
        var mailData = { defaults: req.body.default };
        Mail.defaultMail(mailData, email_id, emailType).then(function(result) {

            if(Default == 1){
                var response = { "status": 1, "message": "Email set as default" };                
            }else{
                var response = { "status": 1, "message": "Remove email from default" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
           
    }

}

module.exports = MailController;