var fs = require('fs');
var EarlyPay = require('../models/EarlyPay');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var EarlyPayController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            request_id: req.body.request_id,
            worker_id: req.body.worker_id,
            workers_name: req.body.workers_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        EarlyPay.countData(searchParams).then(function (result) {
            total = result[0].total;
            EarlyPay.getData(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "paylist": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var request_id = req.body.request_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        EarlyPay.statusEarlyPay(data, request_id).then(function(result) {            
            if(Status == 1){
                var response = { "status": 1, "message": "Request Accepted" };                
            }else{
                var response = { "status": 1, "message": "Request Not Accepted" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionNot: function (req, res, next) {
        var total = 0;

        EarlyPay.countNotAcceptData().then(function (result) {
            total = result[0].total;
            EarlyPay.getNotAcceptData().then(function (result) {

                var response = { "status": 1, "message": "Record Found", "paylist": result, "total": total };
                res.json(response);

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }

}

module.exports = EarlyPayController;