var fs = require('fs');
var Leave = require('../models/Leave');
var commonHelper = require('../helper/common_helper.js');

var LeaveController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            leaves_id: req.body.leaves_id,
            leaves_name: req.body.leaves_name,
            // faq_answer: req.body.faq_answer,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Leave.countLeave(searchParams).then(function (result) {
            total = result[0].total;
            Leave.getLeave(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "leave": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var leaves_id = req.body.leaves_id;

        if (typeof leaves_id !== 'undefined' && leaves_id !== '') {
            Leave.singleLeave(leaves_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "leave": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var leaveData = {
            leaves_name: req.body.leaves_name,
            leaves_des: req.body.leaves_des,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };
        
        Leave.addLeave(leaveData).then(function (result) {
            var response = { "status": 1, "message": "Leave has been added successfully." };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to add leave!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var leaves_id = req.body.leaves_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var leaveData = {
            leaves_name: req.body.leaves_name,
            leaves_des: req.body.leaves_des,
            status: req.body.status,
            updated_at: currentDate
        };

        Leave.updateLeave(leaveData, leaves_id).then(function (result) {
            var response = { "status": 1, "message": "Leave has been updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to update leave data!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this leave data!" };
        var leaves_id = req.body.leaves_id;

        Leave.deleteLeave(leaves_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Leave has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var leaves_id = req.body.leaves_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Leave.statusLeave(data, leaves_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Leave reason has been approved" };                
            }else{
                var response = { "status": 1, "message": "Leave reason has been disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = LeaveController;