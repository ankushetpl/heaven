var fs = require('fs');
var Category = require('../models/Category');
var commonHelper = require('../helper/common_helper.js');

var CategoryController = {

    actionIndex: function(req, res, next) {
        var limit = 2;
        var total = 0;
        var page = parseInt(req.body.page) || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            id: req.body.id,
            name: req.body.name,
            status: req.body.status
        };

        Category.countCategory(searchParams).then(function(result) {
            total = result[0].total;

            Category.getCategories(limit, offset, searchParams, orderParams).then(function(result) {
                var response = { "status": 1, "message": "Record Found", "categories": result, "current": page, "pages": Math.ceil(total / limit) };
                res.json(response);
            }).catch(function(error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function(error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionShow: function(req, res, next) {
        var category_id = req.body.category_id;

        if (typeof category_id != 'undefined' && category_id != '') {
            Category.singleCategory(category_id).then(function(result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "category": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function(error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var category = { name: req.body.name, created_at: currentDate, updated_at: currentDate };

        Category.addCategory(category).then(function(result) {
            var response = { "status": 1, "message": "Add User" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Add User failed" };
            res.json(response);
        });
    },


    actionUpdate: function(req, res, next) {
        var category_id = req.body.category_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var category = { name: req.body.name, updated_at: currentDate };

        Category.updateCategory(category, category_id).then(function(result) {
            var response = { "status": 1, "message": "User Updated" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "User Update failed" };
            res.json(response);
        });

    },

    actionDelete: function(req, res, next) {
        var response = { success: false, msg: "Unable to delete this category!" };
        var category_id = req.body.category_id;

        Category.deleteCategory(category_id).then(function(result) {
            if (result) {
                response = { success: true, msg: "Your category record has been deleted." };
            }
            res.json(response);
        }).catch(function(error) {
            res.json(response);
        });
    }

}

module.exports = CategoryController;