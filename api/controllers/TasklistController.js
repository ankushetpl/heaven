var fs = require('fs');
var Tasklist = require('../models/Tasklist');
var commonHelper = require('../helper/common_helper.js');

var TasklistController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            tasklist_id: req.body.tasklist_id,
            tasklist_servicetype : req.body.tasklist_servicetype,
            tasklist_name: req.body.tasklist_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Tasklist.countTasklist(searchParams).then(function (result) {
            total = result[0].total;
            Tasklist.getTasklist(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "tasklist": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var tasklist_id = req.body.tasklist_id;

        if (typeof tasklist_id !== 'undefined' && tasklist_id !== '') {
            Tasklist.singleTasklist(tasklist_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "tasklist": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var tasklistData = {
            tasklist_servicetype : req.body.tasklist_servicetype,
            tasklist_name: req.body.tasklist_name,
            tasklist_des: req.body.tasklist_des,
            tasklist_duration: req.body.tasklist_duration,
            // tasklist_cost: req.body.tasklist_cost,
            tasklist_type: req.body.tasklist_type,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Tasklist.addTasklist(tasklistData).then(function (result) {
            var response = { "status": 1, "message": "Task created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Task create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var tasklist_id = req.body.tasklist_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var tasklistData = {
            tasklist_servicetype : req.body.tasklist_servicetype,
            tasklist_name: req.body.tasklist_name,
            tasklist_des: req.body.tasklist_des,
            tasklist_duration: req.body.tasklist_duration,
            // tasklist_cost: req.body.tasklist_cost,
            tasklist_type: req.body.tasklist_type,
            status: req.body.status,
            updated_at: currentDate
        };

        Tasklist.updateTasklist(tasklistData, tasklist_id).then(function (result) {
            var response = { "status": 1, "message": "Task updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Task update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Task!" };
        var tasklist_id = req.body.tasklist_id;
        
        Tasklist.deleteTasklist(tasklist_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your task has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var tasklist_id = req.body.tasklist_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Tasklist.statusTasklist(data, tasklist_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Task Approved" };                
            }else{
                var response = { "status": 1, "message": "Task Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = TasklistController;