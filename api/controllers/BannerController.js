var fs = require('fs');
var Banner = require('../models/Banner');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var BannerController = {

    actionIndex: function (req, res, next) {
        
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            banner_id: req.body.banner_id,
            banner_title: req.body.banner_title,
            banner_location: req.body.banner_location,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Banner.countBanner(searchParams).then(function (result) {
            total = result[0].total;
            Banner.getBanner(limit, offset, searchParams, orderParams).then(function (results) {
                var response = { "status": 1, "message": "Record Found", "banner": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },
   
    actionView: function (req, res, next) {
        var banner_id = req.body.banner_id;

        if (typeof banner_id !== 'undefined' && banner_id !== '') {
            Banner.singleBanner(banner_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "banner": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var bannerData = {
            banner_title: req.body.banner_title,
            banner_description: req.body.banner_description,
            banner_image: req.body.banner_image,
            banner_location: req.body.banner_location,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };
        
        Banner.addBanner(bannerData).then(function (result) {
            var response = { "status": 1, "message": "Banner created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to create" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var banner_id = req.body.banner_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var bannerData = {
            banner_title: req.body.banner_title,
            banner_description: req.body.banner_description,
            banner_image: req.body.banner_image,
            banner_location: req.body.banner_location,
            status: req.body.status,
            updated_at: currentDate
        };
        
        Banner.updateBanner(bannerData, banner_id).then(function (result) {
            var response = { "status": 1, "message": "Banner updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to update" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this banner content!" };
        var banner_id = req.body.banner_id;

        Banner.deleteBanner(banner_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your banner content has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var banner_id = req.body.banner_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Banner.statusBanner(data, banner_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Banner approved" };                
            }else{
                var response = { "status": 1, "message": "Banner disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }
    
}

module.exports = BannerController;