var fs = require('fs');
var Worker = require('../models/Worker');
var Client = require('../models/Client');
var Array = require('node-array');
var dateFormat = require('dateformat');
var multer  = require('multer');
var split = require('split-string');
var FileUpload = require('../models/FileUpload');
var commonHelper = require('../helper/common_helper.js');
var arraySort = require('array-sort');
var sortBy = require('array-sort-by');
var md5 = require('md5');

var site_url = commonHelper.getSiteURL();

var WorkerController = {

    //UPDATE WORKERS
    actionUpdate: function(req, res, next) {

        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, __dirname + '/../../assets/uploads/');
            },
            filename: function (req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
            }
        });
        
        var upload = multer({ storage: storage }).single('file');

        upload(req, res, function (err) {
            var response = {};
            if (err) {
                response = {status: 1, message: err};
            }

            var flag = req.body.flag;
                if(flag == 0) {

                    var workers_id = req.body.worker_id;
                    var workersData = { workers_name: req.body.name, workers_phone: req.body.phone, workers_mobile: req.body.mobile, workers_address: req.body.address };
                    
                    Worker.updateWorkers(workers_id, workersData).then(function(result) {
                        var response = { "status": 1, "message": "Your profile has been updated successfully." };
                        res.json(response);
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Failed to update your profile." };
                        res.json(response);
                    });

                } else {
                    var fileData = {
                        file_original_name: req.file.originalname,
                        file_name: req.file.filename,                
                        file_type: req.file.mimetype,
                        file_size: req.file.size,
                        file_base_path: req.file.path,
                        file_base_url: site_url + '/assets/uploads/' + req.file.filename
                    };

                    FileUpload.addFileUpload(fileData).then(function (result) {
                        if(result){
                            var workers_id = req.body.worker_id;
                            var workersData = { workers_name: req.body.name, workers_phone: req.body.phone, workers_mobile: req.body.mobile, workers_address: req.body.address, workers_image: result };
                            
                            Worker.updateWorkers(workers_id, workersData).then(function(result) {
                                var response = { "status": 1, "message": "Your profile picture has been updated successfully." };
                                res.json(response);
                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Failed to update your profile." };
                                res.json(response);
                            });
                        }
                    }).catch(function (error) {
                        var response = { status: 0, message: "File add failed!" };
                        res.json(response);
                    });
                }    
        });
    },

    //VIEW WORKERS
    actionView: function(req, res, next) {
        var worker_id = req.body.worker_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {
                Worker.getWorker(worker_id).then(function(results) {

                    if(results.length > 0) {

                        Worker.getImages().then(function(resultImage) {
                            
                            if(resultImage.length > 0) {
                                
                                for (var i = 0; i < results.length; i++) {
                                        
                                    for (var k = 0; k < resultImage.length; k++) {
                                        if(results[i].workers_image == resultImage[k].file_id) {
                                                results[i].workers_image = resultImage[k].file_base_url;
                                        }
                                    }
                                }
                                        
                                var response = { "status": 1, "message": "Worker Data", "workerDetails" : results };
                                res.json(response);
                                        
                            } else {
                                var response = { "status": 0, "message": "No data found" };
                                res.json(response);
                            }   
                   
                                    
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        }); 

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }
                  
                    
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    //TODAYS AND UPCOMING JOB
    actionJobs: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        
        var worker_id   = req.body.worker_id;
        var flag        = req.body.flag;

        Worker.checkWokerExists(worker_id).then(function(result) {
            
            total = result.length;
            
            if (total == 1) {
                Worker.getTodaysJob(worker_id,flag,today).then(function(resultsO) {

                    if(resultsO.length > 0) {

                        Worker.getTodaysJobWeekly(worker_id,flag,today).then(function(resultsW) {

                            if(resultsW.length > 0) {

                                var results = resultsO.concat(resultsW);

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {

                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "Job List", "jobList" : final };
                                        res.json(response);            
                                         
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                }); 
                                
                            } else {
                                var results = resultsO;

                               if(results.length > 0) {

                                    Worker.getImages().then(function(resultImage) {
                                        
                                        if(resultImage.length > 0) {
                                            
                                            for (var i = 0; i < results.length; i++) {
  
                                                for (var k = 0; k < resultImage.length; k++) {
                                                    if(results[i].clients_image == resultImage[k].file_id) {
                                                        results[i].clients_image = resultImage[k].file_base_url;
                                                    }
                                                    if(results[i].workers_image == resultImage[k].file_id) {
                                                        results[i].workers_image = resultImage[k].file_base_url;
                                                    }
                                                }
                                            }

                                            for(var j=0; j < results.length; j++) {
                                                results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                            }

                                            var final = arraySort(results, 'booking_date','booking_time_from');
                                            var response = { "status": 1, "message": "Job List", "jobList" : final };
                                            res.json(response); 

                                                 
                                        } else {
                                            var response = { "status": 0, "message": "No data found" };
                                            res.json(response);
                                        }   
                               
                                                
                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    }); 
                                
                                    
                                } else {
                                    var response = { "status": 0, "message": "No data found"};
                                    res.json(response);
                                }
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {

                        Worker.getTodaysJobWeekly(worker_id,flag,today).then(function(results) {

                            if(results.length > 0) {

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {

                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "Job List", "jobList" : final };
                                        res.json(response); 
                                            
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                }); 
                            
                                
                            } else {
                                var response = { "status": 0, "message": "No data found"};
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }

                   
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    //MY JOB
    actionMyJobs: function(req, res, next) {
        var time  = commonHelper.getCurrentTime();
        var today = commonHelper.getCurrentDate();
        var worker_id   = req.body.worker_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            
            total = result.length;
            
            if (total == 1) {

                Worker.getMyJobs(worker_id).then(function(resultsO) {

                    if(resultsO.length > 0) {

                        Worker.getMyJobsWeekly(worker_id).then(function(resultsW) {   

                            if(resultsW.length > 0) {

                                var results = resultsO.concat(resultsW);

                                results.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                });

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
                                            
                                            if(results[i].booking_date == today && results[i].booking_start_time <= time) {
                                               results[i].startJobFlag = 1;
                                            }

                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }   
                                        }
                                    }    

                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "My Job List", "myJobs" : final };
                                        res.json(response);          
                                        
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });
         
                            } else {
                                
                                var results = resultsO;

                                results.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                });

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {

                                            if(results[i].booking_date == today && results[i].booking_start_time <= time) {
                                               results[i].startJobFlag = 1;
                                            }
                                                
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }   
                                        }

                                    }    
                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "My Job List", "myJobs" : final };
                                        res.json(response);          
                                        
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });
                            }  

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found for weekly" };
                            res.json(response);
                        });

                    } else {

                         Worker.getMyJobsWeekly(worker_id).then(function(results) {   

                            if(results.length > 0) {

                                results.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                });

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {

                                            if(results[i].booking_date == today && results[i].booking_start_time <= time) {
                                               results[i].startJobFlag = 1;
                                            }
                                                
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }   
                                        }

                                    }    
                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "My Job List", "myJobs" : final };
                                        res.json(response);          
                                       
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });
         
                            } else {
                                var response = { "status": 0, "message": "No data found"};
                                res.json(response);
                            }  

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found for weekly" };
                            res.json(response);
                        });

                    }
                 
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not Found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    }, 


    //HISTORY
    actionHistory: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        var worker_id   = req.body.worker_id;
        var flag        = req.body.flag;
       
        Worker.checkWokerExists(worker_id).then(function(result) {
           
            if (result.length == 1) {
                Worker.getHistory(worker_id,flag,today).then(function(resultsO) {

                    if(resultsO.length > 0) {

                        Worker.getHistoryWeekly(worker_id,flag,today).then(function(resultsW) {

                            if(resultsW.length > 0) {
                                var results = resultsO.concat(resultsW);

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
         
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].booking_client_rating != '0') {
                                                    results[i].rateFlag = '1';
                                                }
                                            }
                                        }

                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "History List", "history" :final };
                                        res.json(response);         
                                             
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {
                                var results = resultsO;

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
         
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].booking_client_rating != '0') {
                                                    results[i].rateFlag = '1';
                                                }
                                            }
                                        }
                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "History List", "history" : final };
                                        res.json(response);         
                                             
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });
   
                    } else {
                        Worker.getHistoryWeekly(worker_id,flag,today).then(function(results) {

                            if(results.length > 0) {
                                
                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
         
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].booking_client_rating != '0') {
                                                    results[i].rateFlag = '1';
                                                }
                                            }
                                        }
                                        for(var j=0; j < results.length; j++) {
                                            results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                        }

                                        var final = arraySort(results, 'booking_date','booking_time_from');
                                        var response = { "status": 1, "message": "History List", "history" : final };
                                        res.json(response);         
                                            
                                             
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {
                                var response = { "status": 0, "message": "No data found" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });
                    } 

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // SUBSTITUTE REQUEST
    actionSubstituteRequest: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        var worker_id   = req.body.worker_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            
            if (result.length == 1) {
                
                Worker.getSubstituteRequest(worker_id,today).then(function(resultsO) {
                    
                    if(resultsO.length > 0) {

                        Worker.getSubstituteRequestWeekly(worker_id,today).then(function(resultsW) {

                            if(resultsW.length > 0) {

                                var results = resultsO.concat(resultsW);

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
                                                
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                            results.forEachAsync(function(element, index, arr) {
                                                element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                            }, function() {

                                                var final = arraySort(results, 'booking_date','booking_time_from');

                                                var response = { "status": 1, "message": "Sustitute Request List", "substituteRequest" : results };
                                                res.json(response);        
                                            });
                                             
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {
                                var results = resultsO;
                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
                                                
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                            results.forEachAsync(function(element, index, arr) {
                                                element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                            }, function() {

                                                var final = arraySort(results, 'booking_date','booking_time_from');

                                                var response = { "status": 1, "message": "Sustitute Request List", "substituteRequest" : results };
                                                res.json(response);        
                                            });
                                             
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {

                        Worker.getSubstituteRequestWeekly(worker_id,today).then(function(results) {

                            if(results.length > 0) {

                                Worker.getImages().then(function(resultImage) {
                                    
                                    if(resultImage.length > 0) {
                                        
                                        for (var i = 0; i < results.length; i++) {
                                                
                                            for (var k = 0; k < resultImage.length; k++) {
                                                if(results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if(results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                            results.forEachAsync(function(element, index, arr) {
                                                element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                            }, function() {

                                                var final = arraySort(results, 'booking_date','booking_time_from');

                                                var response = { "status": 1, "message": "Sustitute Request List", "substituteRequest" : results };
                                                res.json(response);        
                                            });
                                             
                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }   
                           
                                            
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {
                                var response = { "status": 0, "message": "No any substitute request" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }
                    
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },



    // SUBSTITUTE SEND
    actionSubstituteSend: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id   = req.body.sender_id;
        var booking_id  = req.body.booking_id;
        var serviceflag  = req.body.serviceflag;
        var receiver_id = req.body.receiver_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            
            total = result.length;
            
            if (total == 1) {

                if(serviceflag == 0) {

                    var addSubstitute = { substitute_booking_id: booking_id, substitute_sender_id: worker_id, substitute_receiver_id:receiver_id, created_at: currentDate, updated_at: currentDate };
                } else {
                    var addSubstitute = { substitute_bookweekly_id: booking_id, substitute_sender_id: worker_id, substitute_receiver_id:receiver_id, created_at: currentDate, updated_at: currentDate };
                }

                Worker.CheckSubstituteExists(booking_id, serviceflag, worker_id, receiver_id).then(function(resultCE) {

                    if(resultCE.length == 1) {

                        var response = { "status": 1, "message": "Substitute request already sent to this angel." };
                        res.json(response);

                    } else {

                         Worker.getSubstituteSend(addSubstitute, booking_id, serviceflag).then(function(results) {

                            Worker.getWorker(worker_id).then(function(resultW) {  // get sender name

                                Worker.getDeviceToken(receiver_id).then(function(resultDT) {

                                    var sender = resultW[0].workers_name;
                                    var deviceToken = resultDT[0].device_token;
                                    var msgtitle    = 'Substitute Request';
                                    var msgbody     = 'You have just received a substitute request from '+ sender +' ';
                                    
                                    var notSendData = { user_id : receiver_id, notification_type : 'Substitute', notification_detail_id : results,  notification_detail_serviceflag : serviceflag, notification_msg : msgbody, created_at: currentDate, updated_at: currentDate };

                                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                Worker.notifyBadgeCount(receiver_id).then(function(resultBC) {
                                                    var badge = resultBC.length;

                                                    Worker.sendNotification(deviceToken,msgtitle,msgbody,badge,resultNR).then(function(resultSN) {

                                                        var response = { "status": 1, "message": "Your request has been sent successfully" };
                                                        res.json(response);

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Your request has been sent successfully" };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Your request has been sent successfully" };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Your request has been sent successfully" };
                                                res.json(response);
                                            });

                                        
                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Your request has been sent successfully" };
                                            res.json(response);
                                        });

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Your request has been sent successfully" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 1, "message": "Your request has been sent successfully" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Failed to send substitute request" };
                            res.json(response);
                        });
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to get substitute request data" };
                    res.json(response);
                });    
                
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },

    // Get Worker List
    actionWorkersList: function(req, res, next) {
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        var total = 0;
        var totals = 1;

        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {

                Worker.getAllWorkers(worker_id,booking_id).then(function(results) {

                    Worker.getAllSubstitute(worker_id,booking_id).then(function(resultS) {
                        
                        Worker.getImages().then(function(resultImage) {
                            
                            if(resultImage.length > 0) {

                                
                                for (var i = 0; i < results.length; i++) {

                                    for (var j = 0; j < resultS.length; j++) {
                                        if(results[i].user_id == resultS[j].substitute_receiver_id){
                                            results[i].workerBookingFlag = 1;
                                        }
                                    }
                                        
                                    for (var k = 0; k < resultImage.length; k++) {
                                        if(results[i].clients_image == resultImage[k].file_id) {
                                            results[i].clients_image = resultImage[k].file_base_url;
                                        }
                                        if(results[i].workers_image == resultImage[k].file_id) {
                                            results[i].workers_image = resultImage[k].file_base_url;
                                        }
                                    }
                                }

                                var final = arraySort(results, 'workers_rating');
                                var final1 = sortBy(final, item => `DESC:${item.workers_rating}`);
                                        
                                var response = { "status": 1, "message": "Workers Data", "workersList" : final1 };
                                res.json(response);
                                        
                            } else {
                                var response = { "status": 0, "message": "No data found" };
                                res.json(response);
                            }   
                   
                                    
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                        
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });            
            
    },

    // Leave Reasons list
    actionleaveReasonList: function(req, res, next) {
        var worker_id = req.body.worker_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {

            Worker.leavesList().then(function(results) {
                if(results.length > 0) {
                    var response = { "status": 1, "message": "leaves list", "reasons" : results };
                    res.json(response);
                } else {
                    var response = { "status": 1, "message": "No data found" };
                    res.json(response);
                }
            }).catch(function(error) {
                var response = { "status": 0, "message": "Not found" };
                res.json(response);
            });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });            
            
    },

    // Apply for Leave Request
    actionleaveApply: function(req, res, next) {
    
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, __dirname + '/../../assets/uploads/');
            },
            filename: function (req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
            }
        });
        
        var upload = multer({ storage: storage }).single('file');

        upload(req, res, function (err) {
            var response = {};
            if (err) {
                response = {status: 1, message: err};
            }
            var flag = req.body.flag;

                if(flag == 0){

                    var currentDate = commonHelper.getCurrentDateTime();
                    var worker_id   = req.body.worker_id;
                    var leave_from   = req.body.leave_from;
                    var leaveApply  = { worker_id: worker_id, workerleaves_from: req.body.leave_from, workerleaves_leaves_id : req.body.reason_id, workerleaves_note : req.body.leave_note, workerleaves_emergency : req.body.emergency, workerleaves_terms : req.body.leave_terms, created_at: currentDate, updated_at: currentDate };

                    var dateData = leave_from.split(",");

                    
                    Worker.checkWokerExists(worker_id).then(function(result) {
                        total = result.length;
                        
                        if (total == 1) {

                        Worker.applyLeaveReq(leaveApply,dateData).then(function(results) {
                            
                                var response = { "status": 1, "message": "Leave applied successfully" };
                                res.json(response);
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Failed to apply" };
                            res.json(response);
                        });
                        } else {
                            var response = { "status": 0, "message": "Worker does not exit" };
                            res.json(response);
                        }
                    }).catch(function(error) {
                        //console.log(error);
                        var response = { "status": 0, "message": "Try again." };
                        res.json(response);
                    }); 

                }else{

                    // console.log(req.file);
                    var fileData = {
                        file_original_name: req.file.originalname,
                        file_name: req.file.filename,                
                        file_type: req.file.mimetype,
                        file_size: req.file.size,
                        file_base_path: req.file.path,
                        file_base_url: site_url + '/assets/uploads/' + req.file.filename
                    };

                    FileUpload.addFileUpload(fileData).then(function (result) {
                        if(result){
                            var currentDate = commonHelper.getCurrentDateTime();
                            var worker_id   = req.body.worker_id;
                            var leave_from   = req.body.leave_from;
                            var leaveApply  = { worker_id: worker_id, workerleaves_from: req.body.leave_from, workerleaves_leaves_id : req.body.reason_id, workerleaves_note : req.body.leave_note, workerleaves_emergency : req.body.emergency, workerleaves_file : result, workerleaves_terms : req.body.leave_terms, created_at: currentDate, updated_at: currentDate };

                            var dateData = leave_from.split(",");

                            
                            Worker.checkWokerExists(worker_id).then(function(result) {
                                total = result.length;
                                
                                if (total == 1) {

                                Worker.applyLeaveReq(leaveApply,dateData).then(function(results) {
                                    
                                        var response = { "status": 1, "message": "Leave applied successfully" };
                                        res.json(response);
                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Failed to apply" };
                                    res.json(response);
                                });
                                } else {
                                    var response = { "status": 0, "message": "Worker does not exit" };
                                    res.json(response);
                                }
                            }).catch(function(error) {
                                //console.log(error);
                                var response = { "status": 0, "message": "Try again." };
                                res.json(response);
                            }); 
                        }
                    }).catch(function (error) {
                        var response = { status: 0, message: "File add failed!" };
                        res.json(response);
                    });
                }
        });
  
    },


    // DELETE WORKER LEAVE
    actionleaveDelete: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id = req.body.worker_id;
        var leave_id = req.body.leave_id;
        var datadelete = {is_deleted : 1, updated_at : currentDate};
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {
                Worker.deleteWorkerLeave(datadelete, worker_id,leave_id).then(function(results) {
                    
                        var response = { "status": 1, "message": "Leave deleted successfully",};
                        res.json(response);
                                      
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },    


    // WORKERS ALL LEAVES
    actionworkerLeaves: function(req, res, next) {
        var worker_id = req.body.worker_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {
                Worker.getWorkerLeaves(worker_id).then(function(results) {
                    if(results.length > 0) {

                        Worker.leavesList().then(function(resultLL) {

                            Worker.getImages().then(function(resultImage) {
                                
                                if(resultImage.length > 0) {

                                    
                                    for (var i = 0; i < results.length; i++) {
      
                                        for (var k = 0; k < resultImage.length; k++) {
                                            if(results[i].workerleaves_file == resultImage[k].file_id) {
                                                results[i].workerleaves_file = resultImage[k].file_base_url;
                                            }
                                        }

                                        for (var m = 0; m < resultLL.length; m++) {
                                            if(results[i].workerleaves_leaves_id == resultLL[m].leaves_id) {
                                                results[i].workerleaves_leaves_id = resultLL[m].leaves_id;
                                                results[i].workerleaves_leaves_name = resultLL[m].leaves_name;
                                            }
                                        }
                                    }

                                    for(var j=0; j < results.length; j++) {
                                        results[j].workerleaves_from = dateFormat(results[j].workerleaves_from, "yyyy-mm-dd");
                                    }
                                    
                                    var final = arraySort(results, 'workerleaves_from');
                                    // var final1 = sortBy(final, item => `DESC:${item.workerleaves_from}`);

                                    var response = { "status": 1, "message": "leaves Data", "workerLeaves" : final };
                                    res.json(response);
                                    
                                            
                                } else {
                                    var response = { "status": 0, "message": "No data found" };
                                    res.json(response);
                                }   
                       
                                        
                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 1, "message": "No data found" };
                        res.json(response);
                    }                    
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // WORKER CALENDER
    actioncalender: function(req, res, next) {
        var worker_id = req.body.worker_id;
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            
            if (result.length == 1) {

                Worker.getWorkerCalender(worker_id).then(function(results) {

                    if(results.length > 0) {

                        for(var i=0; i < results.length; i++) {
                            if(results[i].date != '0000-00-00') {
                                results[i].date = dateFormat(results[i].date, "yyyy-mm-dd");
                            }
                        }

                        Worker.getWorkerBookingCalender(worker_id).then(function(resultB) {

                            if(resultB.length > 0) {

                                Worker.getWorkerBookingWeekCalender(worker_id).then(function(resultBW) {

                                    if(resultBW.length > 0) {

                                        var resultAB = resultB.concat(resultBW);

                                        for(var j=0; j < resultAB.length; j++) {
                                            if(resultAB[j].date != '0000-00-00') {
                                                resultAB[j].date = dateFormat(resultAB[j].date, "yyyy-mm-dd");
                                            }
                                        }

                                        var final = arraySort(resultAB, 'date');
                                        var final1 = sortBy(final, item => `DESC:${item.date}`);
                                        
                                        var response = { "status": 1, "message": "calender Data", "leave" : results, "booking" : final1 };
                                        res.json(response);

                                    } else {

                                        for(var k=0; k < resultB.length; k++) {
                                            if(resultB[k].date != '0000-00-00') {
                                                resultB[k].date = dateFormat(resultB[k].date, "yyyy-mm-dd");
                                            }
                                        }

                                        var final = arraySort(resultB, 'date');
                                        var final1 = sortBy(final, item => `DESC:${item.date}`);

                                        var response = { "status": 1, "message": "calender Data", "leave" : results, "booking" : final1 };
                                        res.json(response);
                                        
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });    

                            } else {

                                Worker.getWorkerBookingWeekCalender(worker_id).then(function(resultBW) {

                                    if(resultBW.length > 0) {

                                        for(var j=0; j < resultBW.length; j++) {
                                            if(resultBW[j].date != '0000-00-00') {
                                                resultBW[j].date = dateFormat(resultBW[j].date, "yyyy-mm-dd");
                                            }
                                        }

                                        var final = arraySort(resultBW, 'date');
                                        var final1 = sortBy(final, item => `DESC:${item.date}`);
                                        
                                        var response = { "status": 1, "message": "calender Data", "leave" : results, "booking" : resultBW };
                                        res.json(response);

                                    } else {

                                        var response = { "status": 1, "message": "calender Data", "leave" : results, "booking" : [] };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                }); 
                            } 

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });  

                    } else {

                        Worker.getWorkerBookingCalender(worker_id).then(function(resultB) {

                            if(resultB.length > 0) {

                                Worker.getWorkerBookingWeekCalender(worker_id).then(function(resultBW) {

                                    if(resultBW.length > 0) {

                                        var resultAB = resultB.concat(resultBW);

                                        for(var j=0; j < resultAB.length; j++) {
                                            if(resultAB[j].date != '0000-00-00') {
                                                resultAB[j].date = dateFormat(resultAB[j].date, "yyyy-mm-dd");
                                            }
                                        }

                                        var final = arraySort(resultAB, 'date');
                                        var final1 = sortBy(final, item => `DESC:${item.date}`);
                                        
                                        var response = { "status": 1, "message": "calender Data", "leave" : [], "booking" : resultAB };
                                        res.json(response);

                                    } else {

                                        for(var l=0; l < resultB.length; l++) {
                                            if(resultB[l].date != '0000-00-00') {
                                                resultB[l].date = dateFormat(resultB[l].date, "yyyy-mm-dd");
                                            }
                                        }

                                        var final = arraySort(resultB, 'date');
                                        var final1 = sortBy(final, item => `DESC:${item.date}`);

                                        resultB.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                            
                                        }, function() {
                                            var response = { "status": 1, "message": "calender Data", "leave" : [], "booking" : final1 };
                                            res.json(response);
                                        });

                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });    

                            } else {

                                Worker.getWorkerBookingWeekCalender(worker_id).then(function(resultBW) {

                                    if(resultBW.length > 0) {

                                        for(var j=0; j < resultBW.length; j++) {
                                            if(resultBW[j].date != '0000-00-00') {
                                                resultBW[j].date = dateFormat(resultBW[j].date, "yyyy-mm-dd");
                                            }
                                        }
                                        var final = arraySort(resultBW, 'date');
                                        var final1 = sortBy(final, item => `DESC:${item.date}`);
                                        
                                        var response = { "status": 1, "message": "calender Data", "leave" : [], "booking" : final1 };
                                        res.json(response);

                                    } else {

                                        var response = { "status": 1, "message": "calender Data", "leave" : [], "booking" : [] };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                }); 
                            } 

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });
                    }

                   
                   
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // START AND END JOB
    actionjobTime: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var time  = commonHelper.getCurrentTime();
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var flag = req.body.flag;
        var updated_at = currentDate;

            if(flag == 0) { 
                var messageJob     = 'A worker has started the job.';
            } else {
                var messageJob     = 'A worker has ended the job.';
            }
        Worker.checkWokerExists(worker_id).then(function(result) {
            
            if (result.length == 1) {

                Worker.workerJobTime(worker_id,time,booking_id,flag,updated_at,serviceflag).then(function(results) {
                    
                    Worker.getJobDetails(booking_id,serviceflag).then(function(resultGC) {
                        
                        var notReceiver = resultGC[0].client_id;

                        Worker.getDeviceToken(notReceiver).then(function(resultDT) {

                            var deviceToken = resultDT[0].device_token;
                            var deviceType = resultDT[0].device_type;
                            
                            if(flag == 0) { 
                                var msgtitle    = 'Start Job';
                                var msgbody     = 'A worker has started the job.';
                                var type        = 'Start Job'; 

                            } else {
                                var msgtitle    = 'End Job';
                                var msgbody     = 'A worker has ended the job.';
                                var type        = 'End Job'; 
                            }
                            
                    
                            var notSendData = { user_id : notReceiver, notification_type : type, notification_detail_id : booking_id, notification_detail_serviceflag : serviceflag, notification_msg : msgbody, created_at: currentDate, updated_at: currentDate };

                                Worker.addNotificationData(notSendData).then(function(resultAN) {

                                    Worker.notifyBadgeCount(notReceiver).then(function(resultBC) {
                                        var badge = resultBC.length;

                                        Worker.getNotResponse(resultAN).then(function(resultNR) {

                                            if(deviceType == 2) {       // Android
                                                
                                                Client.sendNotificationClient(deviceToken,msgtitle,msgbody,badge,resultNR).then(function(resultSN) {
                                                    
                                                    var response = { "status": 1, "message": messageJob };
                                                    res.json(response); 

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": messageJob };
                                                    res.json(response);
                                                });

                                            } else if(deviceType == 3) {
                                                
                                                Client.sendIOSNotification(deviceToken,msgtitle,msgbody,badge,resultNR).then(function(resultISN) {
                                                    
                                                    var response = { "status": 1, "message": messageJob };
                                                    res.json(response);

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": messageJob };
                                                    res.json(response);
                                                });
                                            } else {    
                                                var response = { "status": 0, "message": "No device found for client" };
                                                res.json(response);
                                            }

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": messageJob };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": messageJob };
                                        res.json(response);
                                    });
                               
                                }).catch(function(error) {
                                    var response = { "status": 1, "message": messageJob };
                                    res.json(response);
                                });

                        }).catch(function(error) {
                            var response = { "status": 1, "message": messageJob };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found data for client" };
                        res.json(response);
                    });
                                
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not Found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // JOB DETAILS
    actionjobDetails: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var total = 0;
        var countData = 0;
        var bedroom = [];
        var bathroom = [];
        var basket = [];
        var cleaningArea = [];
        var skills = [];
        var CustomTask = [];
        var StandardTask = [];
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {

                Worker.getJobDetails(booking_id, serviceflag).then(function(results) {

                    results.forEachAsync(function(element, index, arr) {
                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                            if(element.booking_substitute == 1) {
                                
                                Worker.getSubstituteID(booking_id, serviceflag, worker_id).then(function(resultSID) {
                                    element.booking_substitute = resultSID.substitute_id;
                                });
                            }
                    });

                      
                    Worker.getAvgRating().then(function(resultAR) { 

                        for (var m = 0; m < resultAR.length; m++) {
                            if(results[0].client_id == resultAR[m].client_id) {
                                results[0].clientRating = (resultAR[m].totalRating / resultAR[m].count);
                            }
                        }

                        if(results.length > 0) {

                            Worker.getImages().then(function(resultImage) {
                                
                                if(resultImage.length > 0) {

                                    for(var p =0; p < resultImage.length; p++) {

                                        if(results[0].booking_image1 == resultImage[p].file_id) {
                                            results[0].booking_image1 = resultImage[p].file_base_url;
                                        }
                                        if(results[0].booking_image2 == resultImage[p].file_id) {
                                            results[0].booking_image2 = resultImage[p].file_base_url;
                                        }
                                        if(results[0].booking_image3 == resultImage[p].file_id) {
                                            results[0].booking_image3 = resultImage[p].file_base_url;
                                        }
                                        if(results[0].booking_image4 == resultImage[p].file_id) {
                                            results[0].booking_image4 = resultImage[p].file_base_url;
                                        }
                                        if(results[0].booking_image5 == resultImage[p].file_id) {
                                            results[0].booking_image5 = resultImage[p].file_base_url;
                                        }
                                        if(results[0].clients_image == resultImage[p].file_id) {
                                            results[0].clients_image = resultImage[p].file_base_url;
                                        }
                                        if(results[0].workers_image == resultImage[p].file_id) {
                                            results[0].workers_image = resultImage[p].file_base_url;
                                        }
                                     
                                    }

                                    Worker.getStandardTask().then(function(resultSTask) {

                                        if(resultSTask.length > 0) {
                                            
                                            Worker.getSkillsData().then(function(resultSD) { 

                                                if(resultSD.length > 0 && results[0].booking_skills != '') {

                                                    var arrayskill = results[0].booking_skills.split(',');
                                                    for (var k = 0; k < resultSD.length; k++) {
                                                
                                                        if(arrayskill[k] == resultSD[k].skills_id) {

                                                            skills.push({skills_name: resultSD[k].skills_name }); 
                                                        }
                                                    }
                                                    
                                                    Worker.getAmenitiesData().then(function(resultA) {

                                                        if(resultA.length > 0) {

                                                            for (var i = 0; i < resultA.length; i++) {
                                                                if(results[0].booking_bedrooms != 0) {
                                                                    if(results[0].booking_bedrooms == resultA[i].amenities_id) {
                                                                        bedroom.push({bedroom_count: resultA[i].amenities_count, bedroom_clean_duartion: resultA[i].amenities_clean_duartion }); 
                                                                    }   
                                                                }
                                                                if(results[0].booking_bathrooms != 0) {
                                                                    if(results[0].booking_bathrooms == resultA[i].amenities_id) {
                                                                        bathroom.push({bathroom_count: resultA[i].amenities_count, bathroom_clean_duartion: resultA[i].amenities_clean_duartion }); 
                                                                    }     
                                                                }
                                                                if(results[0].booking_baskets != 0) {
                                                                    if(results[0].booking_baskets == resultA[i].amenities_id) {
                                                                        basket.push({basket_count: resultA[i].amenities_count, basket_clean_duartion: resultA[i].amenities_clean_duartion }); 
                                                                    }
                                                                }
                                                            }

                                                            Worker.getCleaningData().then(function(resultC) { 


                                                                if(resultC.length > 0 && results[0].booking_cleaning_area != '') {

                                                                    var array = results[0].booking_cleaning_area.split(',');
                                                                    for (var j = 0; j < resultC.length; j++) {
                                                                
                                                                        if(array[j] == resultC[j].area_id) {

                                                                            cleaningArea.push({cleaning_area_name: resultC[j].area_name, cleaning_area_duration: resultC[j].area_clean_duration }); 
                                                                        }
                                                                    }
                                                                    
                                                                    Worker.getCustomTask().then(function(resultCT) { 
                                                                        if(resultCT.length > 0 && results[0].booking_custom != '') {

                                                                            var arrayCustom = results[0].booking_custom.split(',');
                                                                            for (var l = 0; l < resultCT.length; l++) {
                                                                        
                                                                                if(arrayCustom[l] == resultCT[l].tasklist_id) {

                                                                                    CustomTask.push({tasklist_name: resultCT[l].tasklist_name, tasklist_des: resultCT[l].tasklist_des, tasklist_duration : resultCT[l].tasklist_duration }); 
                                                                                }
                                                                            }
                                                                            
                                                                            var response = { "status": 1, "message": "Job details", 'jobDetails' : results[0], 'standardTask' : resultSTask, 'workerskills' : skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket' : basket, 'cleaningArea' : cleaningArea, 'customTask' :CustomTask };
                                                                            res.json(response);
                                                                            
                                                                        } else {
                                                                            var response = { "status": 1, "message": "Job details", 'jobDetails' : results[0], 'standardTask' : resultSTask, 'workerskills' : skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket' : basket, 'cleaningArea' : cleaningArea, 'customTask' :[] };
                                                                            res.json(response);
                                                                        }  

                                                                        

                                                                    }).catch(function(error) {
                                                                        var response = { "status": 0, "message": "Not Found" };
                                                                        res.json(response);
                                                                    });

                                                                } else {
                                                                    var response = { "status": 1, "message": "Job details", 'jobDetails' : results[0], 'standardTask' : resultSTask, 'workerskills' : skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket' : basket, 'cleaningArea' : [], 'customTask' :[] };
                                                                    res.json(response);
                                                                }  

                                                                

                                                            }).catch(function(error) {
                                                                var response = { "status": 0, "message": "Not Found" };
                                                                res.json(response);
                                                            });                              

                                                        } else {
                                                            var response = { "status": 1, "message": "Job details", 'jobDetails' : results[0], 'standardTask' : resultSTask, 'workerskills' : skills, 'bedroom': [], 'bathroom': [], 'basket' : [], 'cleaningArea' : [], 'customTask' :[] };
                                                            res.json(response);
                                                        }    
                                                        
                                                    }).catch(function(error) {
                                                        var response = { "status": 0, "message": "Not found" };
                                                        res.json(response);
                                                    }); 


                                                } else {
                                                    var response = { "status": 1, "message": "Job details", 'jobDetails' : results[0], 'standardTask' : resultSTask, 'workerskills' : [], 'bedroom': [], 'bathroom': [], 'basket' : [], 'cleaningArea' : [], 'customTask' :[] };
                                                    res.json(response);
                                                }    

                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not found" };
                                                res.json(response);
                                            }); 

                                        } else {
                                            var response = { "status": 1, "message": "Job details", 'jobDetails' : results[0], 'standardTask' : [], 'workerskills' : [], 'bedroom': [], 'bathroom': [], 'basket' : [], 'cleaningArea' : [], 'customTask' :[] };
                                            res.json(response);
                                        }   
                               
                                                
                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    }); 

                                } else {
                                    var response = { "status": 0, "message": "No data found" };
                                    res.json(response);
                                }   
                       
                                        
                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });                                       

                        } else {
                            var response = { "status": 0, "message": "No data found" };
                            res.json(response);
                        }   

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });               
                                        
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // CAB REQUEST FROM WORKER
    actioncabRequest: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        
       
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {

                Worker.getAddressBooking(booking_id).then(function(resultB) {

                    if(resultB.length > 0) {

                        Worker.getCafeUser(worker_id).then(function(resultCU) {

                            if(resultCU.length > 0) {

                                var cafeUser = resultCU[0].cafeuser_id;
                                var dropAdd = resultB[0].booking_apartment +","+ resultB[0].booking_street+","+ resultB[0].booking_city;
                                
                                var cabBook = { worker_user_id : worker_id,
                                                cafe_user_id : cafeUser,
                                                booking_id : booking_id,
                                                suburbs_pick_name : req.body.pickup_location,
                                                suburbs_pick_address : req.body.pickup_address,
                                                suburbs_pick_lat : req.body.pick_lat,
                                                suburbs_pick_long : req.body.pick_long,
                                                suburbs_drop_name : resultB[0].booking_apartment,
                                                suburbs_drop_address : dropAdd,
                                                suburbs_drop_lat : resultB[0].booking_lat,
                                                suburbs_drop_long : resultB[0].booking_long,
                                                created_at : currentDate,
                                                updated_at : currentDate
                                              };

                                Worker.cabRequestExists(worker_id, booking_id).then(function(resultCE) { 

                                    if(resultCE.length == 0) {

                                        Worker.cabRequest(cabBook).then(function(results) {
                                        
                                            var response = { "status": 1, "message": "Cab request send successfully" };
                                            res.json(response);
                                                          
                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });

                                    } else {
                                        var response = { "status": 1, "message": "Your request has been sent already" };
                                        res.json(response);
                                    }                    

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });        

                            } else {
                                var response = { "status": 0, "message": "Cafe user not found" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });        


                    } else {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    }
                                      
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

                
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // MY ALL SUBURBS
    actionmySuburb: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id = req.body.worker_id;
        var totals = 0 ;
        var workers_suburb = [];
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {
                Worker.myAllSuburbs(worker_id).then(function(results) {
                    
                    if(results.length > 0) {

                        var array = results[0].workers_suburb.split(',');
                        
                        Worker.getAllSuburbs().then(function(resultS) {

                            for(var i=0; i < array.length; i++) {
 
                                for(var j=0; j < resultS.length; j++) {

                                    if(array[i] == resultS[j].suburb_id) {
                                        workers_suburb[i] = {'suburb_id' : resultS[j].suburb_id, 'suburb_name' : resultS[j].suburb_name};
                                    }
                                }
                                totals = totals +1 ;
                            }
                                if(totals == array.length) {
                                     var response = { "status": 1, "message": "All Suburbs", "suburbs" : workers_suburb };
                                    res.json(response);
                                }
                           

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                        
                    } else {
                        var response = { "status": 0, "message": "No data found"};
                        res.json(response);
                    }
                   
                                      
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // UPDATE SUBURBS
    actionupdateSuburb: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id   = req.body.worker_id;
        var suburbs     = {workers_suburb : req.body.workers_suburbs};
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {
               
                Worker.updateSuburbs(suburbs, worker_id).then(function(results) {

                        var response = { "status": 1, "message": "Update suburbs successfully" };
                        res.json(response);
                                     
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    //ALL CITIES LIST
    actionallCities: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id   = req.body.worker_id;
       
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {
               
                Worker.getAllCitySouthAfrica().then(function(results) {

                    if(results.length > 0) {

                        var response = { "status": 1, "message": "city list", "cities" : results };
                        res.json(response);
                    } else {
                        var response = { "status": 1, "message": "No data found" };
                        res.json(response);
                    }
                      
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // get rating list to rate worker
    actionratingList: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total       = 0;
        var worker_id   = req.body.worker_id;

        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            if (total == 1) { 

                Client.getDefaultRatinglist().then(function(resultDR) {   

                    var response = { "status": 1, "message": "Rating list", 'ratelist' : resultDR };
                    res.json(response);

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });             
     
    },


    // rate client
    actionrateClient: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var total       = 0;
        var worker_id   = req.body.worker_id;
        var booking_id  = req.body.booking_id;
        var serviceflag = req.body.serviceflag; // once or weekly service
        var rating_id   = req.body.rating_id;
        var ratings     = req.body.ratings;
        var arrayId     = [];
        var arrayRatings= [];
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getWorker(booking_id,serviceflag).then(function(resultW) {

                    var arrayId   = rating_id.split(',');
                    var arrayRatings   = ratings.split(',');
                    var sum = 0;
                        for( var i = 0; i < arrayRatings.length; i++ ){
                            sum += parseInt( arrayRatings[i], 10 ); 
                        }

                    var avg = sum/arrayId.length;
                    var client_id = resultW[0].client_id;

                    Worker.giveClientsRating(booking_id,client_id,worker_id,arrayId,arrayRatings,currentDate).then(function(resultWR) {

                        Worker.averageRating(booking_id,client_id,worker_id,avg,serviceflag).then(function(resultA) {

                            Worker.getAllDoneJobsBooking(client_id).then(function(resultADJ) {  // get all booking 

                                Worker.getAllDoneJobsWeekly(client_id).then(function(resultADJW) {  // get all booking 

                                    if(resultADJ.length > 0 && resultADJW.length > 0) {

                                        var countBooking = resultADJ.length; 
                                        var totalBookingRate = 0;
                                        var totalAvg  = 0;
                                            for( var j = 0; j < resultADJ.length; j++ ){
                                                totalBookingRate += parseFloat( resultADJ[j].booking_client_rating,10); 
                                            }
                                        var countWeekly = resultADJW.length;
                                        var totalWeeklyRate = 0;
                                        var totalAvg  = 0;
                                            for( var k = 0; k < resultADJW.length; k++ ){
                                                totalWeeklyRate += parseFloat(resultADJW[k].booking_client_rating); 
                                            }

                                        var totalRateData = parseFloat(totalBookingRate + totalWeeklyRate);
                                        var totalCountRate = parseFloat(countBooking + countWeekly);
                                        var totalAvg = totalRateData / totalCountRate;
                                            
                                    } else if(resultADJ.length > 0) {

                                        var countWeekly = resultADJ.length;
                                        var totalWeeklyRate = 0;
                                        var totalAvg  = 0;
                                            for( var l = 0; l < resultADJ.length; l++ ){
                                                totalWeeklyRate += parseFloat(resultADJ[l].booking_client_rating); 
                                            }
                                        var totalAvg = totalWeeklyRate / countWeekly;
                                            
                                    } else if(resultADJW.length > 0) {

                                        var countWeekly = resultADJW.length;
                                        var totalWeeklyRate = 0;
                                        var totalAvg  = 0;
                                            for( var m = 0; m< resultADJW.length; m++ ){
                                                totalWeeklyRate += parseFloat(resultADJW[m].booking_client_rating); 
                                            }
                                        var totalAvg = totalWeeklyRate / countWeekly;
                                    } 
                                
                                        Worker.loginAvgRating(client_id,totalAvg).then(function(resultTAR) {

                                            Worker.getDeviceToken(client_id).then(function(resultCDT) {

                                                var deviceType = resultCDT[0].device_type;    
                                                var senderC = resultW[0]['workers_name'];
                                                var deviceTokenC = resultCDT[0].device_token;
                                                var msgtitleC    = 'Rating';
                                                var msgbodyC   = "You have been rated by "+ senderC +" ";
                                                
                                                var notSendData1 = { user_id : client_id, notification_type : msgtitleC, notification_detail_id : booking_id, notification_detail_serviceflag : serviceflag, notification_msg : msgbodyC, created_at: currentDate, updated_at: currentDate };

                                                    Worker.addNotificationData(notSendData1).then(function(resultCAN) {

                                                        Worker.getNotResponse(resultCAN).then(function(resultNR) {

                                                            Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                                                var badge = resultBC.length;

                                                                if(deviceType == 2) {     // Android 

                                                                    Client.sendNotificationClient(deviceTokenC,msgtitleC,msgbodyC,badge,resultNR).then(function(resultCSN) {
                                                                    
                                                                        var response = { "status": 1, "message": "Rate client successfully" };
                                                                        res.json(response);
                                                                       
                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "rate client successfully" };
                                                                        res.json(response);
                                                                    });

                                                                } else if(deviceType == 3) {     // IOS 

                                                                    Client.sendIOSNotification(deviceTokenC,msgtitleC,msgbodyC,badge,resultNR).then(function(resultCSN) {
                                                                    
                                                                        var response = { "status": 1, "message": "Rate client successfully", 'notify' : resultNR, badgeCount : resultBC.length};
                                                                        res.json(response);
                                                                       
                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "rate client successfully" };
                                                                        res.json(response);
                                                                    });


                                                                } else {

                                                                    var response = { "status": 0, "message": "No found client device type" };
                                                                    res.json(response);
                                                                }

                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "rate client successfully" };
                                                                res.json(response);
                                                            });

                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "rate client successfully" };
                                                            res.json(response);
                                                        });

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "rate client successfully" };
                                                        res.json(response);
                                                    });

                                               
                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "rate client successfully" };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No rating added for client" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No data found for worker" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });             
     
    }, 


    // SUBSTITUTE ACCEPT OR REJECT
    actionsubstituteAcceptReject: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var worker_id = req.body.worker_id;
        var substitute_id = req.body.substitute;
        var flag = req.body.flag;
        if(flag == 0) {
            var messageSub     = 'Substitute request accepted';

        } else {
            var messageSub     = 'Substitute request rejected';
        }
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {

                Worker.getBookId(substitute_id).then(function(resultB) {

                    if(resultB[0].substitute_bookweekly_id == 0) {
                        var bookingId = resultB[0].substitute_booking_id;
                        var serviceflag = 0;
                    } else {
                        var bookingId = resultB[0].substitute_bookweekly_id;
                        var serviceflag = 1;
                    }

                    var Old = resultB[0].substitute_sender_id;
                    var New = resultB[0].substitute_receiver_id;

                        Worker.subAcceptReject(substitute_id,flag,currentDate,bookingId,worker_id,serviceflag).then(function(results) {

                            Worker.getWorker(Old).then(function(resultOld) {  //Get old worker
                                
                                Worker.getWorker(New).then(function(resultNew) {  //Get old worker
                                    
                                    Client.getWorker(bookingId,serviceflag).then(function(resultCID) {    
                                        var client_id = resultCID[0].client_id;
                                        resultCID.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                        });

                                        Client.getClient(client_id).then(function(resultC) {   
                                            
                                            Worker.getDeviceToken(Old).then(function(resultDT) {
                                                
                                                var clientName = resultC[0].clients_name;
                                                var OldWorkerName = resultOld[0].workers_name;
                                                var NewWorkerName = resultNew[0].workers_name;

                                                var bookingDate = resultCID[0].booking_date;
                                                var deviceToken = resultDT[0].device_token;

                                                if(flag == 0) {
                                                    var msgtitle    = 'Substitute Accept';
                                                    var msgbody     = 'Your substitute request for the job on '+ bookingDate +' has been accepted by '+ NewWorkerName +' ';

                                                } else {
                                                    var msgtitle    = 'Substitute Reject';
                                                    var msgbody     = 'Your substitute request for the job on '+ bookingDate +' has been rejected by '+ NewWorkerName +' ';
                                                }
                                                
                                                var notSendData = { user_id : Old, notification_type : msgtitle, notification_detail_id : bookingId,  notification_detail_serviceflag : serviceflag, notification_msg : msgbody, created_at: currentDate, updated_at: currentDate };

                                                    Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                        Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                            Worker.notifyBadgeCount(Old).then(function(resultBC) {
                                                                var badgeW = resultBC.length;

                                                                Worker.sendNotification(deviceToken,msgtitle,msgbody,badgeW,resultNR).then(function(resultSN) {

                                                                    if(flag == 0) {   //Accept request

                                                                        Worker.getDeviceToken(client_id).then(function(resultCDT) {
                                                                            
                                                                            var deviceType = resultCDT[0].device_type; 

                                                                            var deviceTokenC = resultCDT[0].device_token;
                                                                            var msgtitleC    = 'Substitute';
                                                                            var msgbodyC   = "The domestic angel "+ OldWorkerName +" has been substituted with "+ NewWorkerName +" on "+ bookingDate +" ";
                                                                            
                                                                            var notSendData1 = { user_id : client_id, notification_type : msgtitleC, notification_msg : msgbodyC, created_at: currentDate, updated_at: currentDate };    

                                                                            Worker.addNotificationData(notSendData1).then(function(resultCAN) {

                                                                                Worker.getNotResponse(resultCAN).then(function(resultCNR) {

                                                                                    Worker.notifyBadgeCount(client_id).then(function(resultCBC) {
                                                                                        var badge = resultCBC.length;

                                                                                        if(deviceType == 2) {     // Android 
                                                                                    
                                                                                            Client.sendNotificationClient(deviceTokenC,msgtitleC,msgbodyC,badge,resultCNR).then(function(resultCSN) {
                                                                                            
                                                                                                var response = { "status": 1, "message": "Substitute request accepted"};
                                                                                                res.json(response);
                                                                                                

                                                                                            }).catch(function(error) {
                                                                                                var response = { "status": 0, "message": messageSub };
                                                                                                res.json(response);
                                                                                            });

                                                                                        } else if(deviceType == 3) {       // IOS

                                                                                            Client.sendIOSNotification(deviceTokenC,msgtitleC,msgbodyC,badge,resultCNR).then(function(resultCSNI) {
                                                                                            
                                                                                                var response = { "status": 1, "message": "Substitute request accepted"};
                                                                                                res.json(response);
                                                                                                

                                                                                            }).catch(function(error) {
                                                                                                var response = { "status": 1, "message": messageSub };
                                                                                                res.json(response);
                                                                                            });

                                                                                        } else {
                                                                                            var response = { "status": 0, "message": "client not found" };
                                                                                            res.json(response);
                                                                                        }

                                                                                    }).catch(function(error) {
                                                                                        var response = { "status": 1, "message": messageSub };
                                                                                        res.json(response);
                                                                                    });

                                                                                }).catch(function(error) {
                                                                                    var response = { "status": 1, "message": messageSub };
                                                                                    res.json(response);
                                                                                });

                                                                            }).catch(function(error) {
                                                                                var response = { "status": 1, "message": messageSub };
                                                                                res.json(response);
                                                                            });

                                                                        }).catch(function(error) {
                                                                            var response = { "status": 1, "message": messageSub };
                                                                            res.json(response);
                                                                        });

                                                                    } else {          // Reject Request
                                                                        var response = { "status": 1, "message": "Substitute request rejected" };
                                                                        res.json(response);
                                                                    }

                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": messageSub };
                                                                    res.json(response);
                                                                });


                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": messageSub };
                                                                res.json(response);
                                                            });


                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": messageSub };
                                                            res.json(response);
                                                        });


                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": messageSub };
                                                        res.json(response);
                                                    });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": messageSub };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found for client data" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found for booking" };
                                        res.json(response);
                                    });                                  

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found for new worker" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found for old worker" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    // JOBS GIVEN RATING AND NOT RATED
    actioncompleteRateJobs: function(req, res, next) {
        var worker_id = req.body.worker_id;
        var flag = req.body.flag;
            if(flag == 0) {
                var displayMsg = 'Completed jobs without rating';
            } else {
                var displayMsg = 'Completed jobs with rating';
            }
        
        Worker.checkWokerExists(worker_id).then(function(result) {
            total = result.length;
            
            if (total == 1) {

                Worker.getCompletedJobs(worker_id,flag).then(function(resultsO) {

                    Worker.getCompletedJobsWeekly(worker_id,flag).then(function(resultsW) {

                        if(resultsO.length > 0 && resultsW.length < 0) {

                            var results = resultsO.concat(resultsW);

                        } else if(resultsO.length < 0) {
                            var results = resultsO;
                        } else {
                            var results = resultsW;
                        }

                            Worker.getImages().then(function(resultImage) {

                                for (var i = 0; i < results.length; i++) {

                                    for (var k = 0; k < resultImage.length; k++) {
                                        if(results[i].clients_image == resultImage[k].file_id) {
                                            results[i].clients_image = resultImage[k].file_base_url;
                                        }
                                    }
                                }

                                for(var j=0; j < results.length; j++) {
                                    results[j].booking_date = dateFormat(results[j].booking_date, "yyyy-mm-dd");
                                }
                                var final = arraySort(results, 'booking_date','booking_time_from');
                                var response = { "status": 1, "message": displayMsg, "jobWithRating" : final };
                                res.json(response);
                                    
                                
                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not Found" };
                                res.json(response);
                            });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });
                       
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },


    actionnotification: function(req, res, next) {
        var token = req.body.token;
        var deviceToken = token;
        var msgtitle = 'Test Notification';
        var msgbody  = 'Heaven Sent test notification';

            Worker.sendNotificationTest(deviceToken,msgtitle,msgbody).then(function(results) {
                
                var response = { "status": 1, "message": "Notification send successfully"};
                res.json(response);
                                 
            }).catch(function(error) {
                var response = { "status": 0, "message": "Not found" };
                res.json(response);
            });

    },    


    // NOTIFICATION LIST
    actionnotList: function(req, res, next) {
        var worker_id = req.body.user_id;
        

        Worker.getAllNotList(worker_id).then(function(results) {
            if(results.length > 0) {

                for(var j=0; j < results.length; j++) {
                    results[j].notification_date = dateFormat(results[j].created_at, "yyyy-mm-dd HH:MM:ss");
                }
                var final = arraySort(results, 'created_at');
                var final1 = sortBy(final, item => `DESC:${item.notification_date}`);
                
                var response = { "status": 1, "message": "notification list", "notlist" : final1 };
                res.json(response);

                
            } else {
                var response = { "status": 0, "message": "No data found" };
                res.json(response);
            }
                                
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
    
    }, 


    // READ NOTIFICATION
    actionreadNot: function(req, res, next) {
        var worker_id = req.body.user_id;
        var notification_id = req.body.notification;
        
            Worker.readStatus(worker_id,notification_id).then(function(results) {

                Worker.getNotCount(worker_id).then(function(resultC) {
                
                    var response = { "status": 1, "message": "read notification", "count" : resultC };
                    res.json(response);

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
                                  
            }).catch(function(error) {
                var response = { "status": 0, "message": "Not found" };
                res.json(response);
            });

            
    },    


    // DELETE OR CLEAR ALL NOTIFICATION
    actionDeleteNot: function(req, res, next) {
        var worker_id = req.body.user_id;
        var notification_id = req.body.notification;
        var flag = req.body.flag;
        
        
            Worker.deleteNotification(worker_id,notification_id,flag).then(function(results) {
                
                var response = { "status": 1, "message": "delete notification successfully" };
                res.json(response);
                                  
            }).catch(function(error) {
                var response = { "status": 0, "message": "Not found" };
                res.json(response);
            });
          
    },


    // NOTIFICATION COUNT
    actionnotCount: function(req, res, next) {
        var user_id = req.body.user_id;
        
        Worker.getNotCount(user_id).then(function(results) {
            
            var response = { "status": 1, "message": "notification count", "count" : results };
            res.json(response);
                              
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
      
    },


    // GET WORKER LIST FOR SUBSTITUTE WORKER
    actionSubstituteWorker: function(req, res, next) {
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;

        Worker.checkWokerExists(worker_id).then(function(result) {

            if(result.length > 0) {

                Worker.getBookingDataAll(booking_id,serviceflag).then(function(resultBD) {

                    var agemin = resultBD.booking_age_min;
                    var agemax = resultBD.booking_age_max;
                    var suburb = resultBD.booking_suburb;
                    var skills = resultBD.booking_skills;
                    var dateBook = resultBD.booking_date;
                    var booking_time_from = resultBD.booking_time_from;
                    var booking_time_to = resultBD.booking_time_to;

                    Worker.getAngelList(agemin,agemax,suburb,skills).then(function(resultA) {
                        
                        if(resultA.length > 0) {

                            Client.getImages().then(function(resultImage) {
          
                                    for(var i=0; i < resultA.length; i++) {

                                        for(var j=0; j < resultImage.length; j++) {

                                            if(resultA[i].workers_image == resultImage[j].file_id) {
                                                resultA[i].workers_image = resultImage[j].file_base_url;
                                            }
                                        }
                                    }

                                        if(serviceflag == 1) {

                                            var response = { "status": 1, "message": "Worker list", "workerlist" : resultA };
                                            res.json(response); 

                                        } else {

                                            Client.checkLeaveDate(dateBook).then(function(resultCL) {
                                        
                                                Client.checkBookDate(dateBook, booking_time_from, booking_time_to).then(function(resultCB) {

                                                    for(var l=0; l<resultA.length; l++) {

                                                        for(var m=0; m < resultCL.length; m++) {

                                                            if(resultA[l].user_id == resultCL[m].worker_id) {
                                                                resultA.splice(l, 1);
                                                            }
                                                        }

                                                        for(var n=0; n < resultCB.length; n++) {

                                                            if(resultA[l].user_id == resultCB[n].worker_id) {
                                                                resultA.splice(l, 1);
                                                            }
                                                        }

                                                        if(resultA[l].user_id == worker_id) {
                                                            resultA.splice(l, 1);
                                                        }
                                                     }

                                                    var response = { "status": 1, "message": "Worker list", "workerlist" : resultA };
                                                    res.json(response); 

                                                }).catch(function(error) {
                                                    var response = { "status": 0, "message": "Not found for book date" };
                                                    res.json(response);
                                                });


                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "No result found for leave" };
                                                res.json(response);
                                            }); 

                                        }


                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        } else {
                            var response = { "status": 0, "message": "No list found", "workerlist" : [] };
                            res.json(response); 
                        }    

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    }); 

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });    

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
      
    },


    // EARLY PAYMENT REQUEST
    actionEarlyPayRequest: function(req, res, next) {
        var worker_id = req.body.worker_id;

        Worker.checkWokerExists(worker_id).then(function(result) {

            if(result.length > 0) {
                    var data = {'worker_id' : worker_id};
                Worker.sendRequest(data).then(function(results) {
                    
                    var response = { "status": 1, "message": "Early payment request has been sent successfully." };
                    res.json(response);
                                      
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
      
    },


    // FORGET PASSWORD
    actionForgetPass: function(req, res, next) {
        var mobileNumber = req.body.mobileNumber;
            // console.log(mobileNumber);
            Worker.getUsersNumber(mobileNumber).then(function(results) { 
                // console.log(results);
                if(results.length == 1) {   
                    var user_id = results[0].user_id;

                    var otp =  Math.floor(1000 + Math.random() * 9000);

                    // Worker.saveUsersOTP(otp,user_id).then(function(resultsOTP) {
                        
                        // var request = require('request')
                        // var message       = 'Hello, your OTP to reset you password is "'+ otp +'". Do not share it with anyone. ';
                       
                        // var sendRequest = {
                        //    'messages': [ {'content': message, 'destination': mobileNumber} ]
                        // };

                        // request.post({
                        //    headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbklkIjoiMjA3NzM0IiwiaXNzIjoiU21zUG9ydGFsU2VjdXJpdHlBcGkiLCJhdWQiOiJBbGwiLCJleHAiOjE1MzA5NDU3MzMsIm5iZiI6MTUzMDg1OTMzM30.gtQAT-37_uBiTXGhgXtfV0Us6f6NY4KBwUGXRBtCjlU'},
                        //    url: 'https://rest.smsportal.com/v1/bulkmessages',
                        //    json: true,
                        //    body: sendRequest
                           
                        // }, function (error, response, body) {
                        //     if(error) throw new Error(error);
                        //     console.log(error);
                        //     if(response) {

                        //         var response = { "status": 1, "message": "Message send successfully",'user' : user_id, 'otp' : otp  };
                        //         res.json(response);
                        //     } else {
                        //         var response = { "status": 1, "message": "Message send successfully",'user' : user_id, 'otp' : otp  };
                        //         res.json(response);
                        //     }
                        // });

                        var request = require('request')
          
                        var sendRequest = {
                           'messages': [ {'content': "Hello, your OTP to reset you password is '"+ otp +"'. Do not share it with anyone.", 'destination': mobileNumber} ]
                        };

                        request.post({
                           headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbklkIjoiMjkyMTM2IiwiaXNzIjoiU21zUG9ydGFsU2VjdXJpdHlBcGkiLCJhdWQiOiJBbGwiLCJleHAiOjE1MzI2OTE1MzEsIm5iZiI6MTUzMjYwNTEzMX0.3POziqafdScAFMbB2NaJv0T8pCa1PKwPaCEkiMJz5Qc'},
                           url: 'https://rest.smsportal.com/v1/bulkmessages',
                           json: true,
                           body: sendRequest
                        }, function (error, response, body) {
                           if(response) {

                                var response = { "status": 1, "message": "Message send successfully",'user' : user_id, 'otp' : otp  };
                                res.json(response);
                            } else {
                                var response = { "status": 1, "message": "Message send successfully",'user' : user_id, 'otp' : otp  };
                                res.json(response);
                            }
                        });

                    // }).catch(function(error) {
                    //     var response = { "status": 0, "message": "Failed to send OTP" };
                    //     res.json(response);
                    // });

                } else {
                    var response = { "status": 0, "message": "Number is not registered." };
                    res.json(response);
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "Not found" };
                res.json(response);
            });

    },

    // RESET PASSWORD
    actionResetPass: function(req, res, next) {
        var worker_id = req.body.worker_id;
        var password = md5(req.body.password);

        Worker.checkWokerExists(worker_id).then(function(result) {

            if(result.length == 1) {

                Worker.resetPassword(password,worker_id).then(function(results) {
                    
                    var response = { "status": 1, "message": "Password reset successfully" };
                    res.json(response);
                                  
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
    },


    // SUBURNS LIST 
    actionserviceSuburbList: function(req, res, next) {
        var worker_id = req.body.worker_id;
        
        Client.checkClientExists(worker_id).then(function(result) {

            if(result.length == 1) {

                Worker.getOnceOffPrice().then(function(resultOP) {

                    Worker.getAllSuburbs().then(function(resultS) {

                        if(resultS.length > 0) {

                            Worker.getAllTasktypeListing().then(function(resultTS) {

                                if(resultTS.length > 0) {

                                    resultTS.forEachAsync(function(element, index, arr) {
                                        element.price = resultOP[0].setting_value;
                                    }, function() {

                                        var response = { "status": 1, "message": "Suburbs and service list", "suburb": resultS, "service": resultTS };
                                        res.json(response);     
                                    });

                                } else {
                                    var response = { "status": 1, "message": "Suburbs and service list", "suburb": resultS, "service": [] };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        } else {

                            Worker.getAllTasktypeListing().then(function(resultTS) {

                                if(resultTS.length > 0) {

                                    resultTS.forEachAsync(function(element, index, arr) {
                                        element.price = resultOP[0].setting_value;
                                    }, function() {

                                        var response = { "status": 1, "message": "Suburbs and service list", "suburb": resultS, "service": resultTS };
                                        res.json(response);     
                                    });

                                } else {
                                    var response = { "status": 1, "message": "Suburbs and service list", "suburb": [], "service": [] };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });
                        }
                        
                                    
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
      
    }             


}

module.exports = WorkerController;