var fs = require('fs');
var Claim = require('../models/Claim');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var ClaimController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            claim_id: req.body.claim_id,
            claim_category_id: req.body.claim_category_id,
            claim_booking_id: req.body.claim_booking_id,
            status: req.body.status,
            action_ats: req.body.action_ats,
            created_at: req.body.created_at
        };

        Claim.countClaim(searchParams).then(function (result) {
            total = result[0].total;
            Claim.getClaim(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "claim": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var claim_id = req.body.claim_id;

        if (typeof claim_id !== 'undefined' && claim_id !== '') {
            Claim.singleClaim(claim_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "claim": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionUpdate: function (req, res, next) {
        var claim_id = req.body.claim_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var resolvedData = {
            action_ats: 1,
            refund_amount: req.body.refund_amount,
            updated_at: currentDate
        };

        Claim.updateClaim(resolvedData, claim_id).then(function (result) {
            var response = { "status": 1, "message": "Claim resolved successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to resolve claim" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Claim!" };
        var claim_id = req.body.claim_id;

        Claim.deleteClaim(claim_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Claim has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var claim_id = req.body.claim_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Claim.statusClaim(data, claim_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Claim approved successfully." };                
            }else{
                var response = { "status": 1, "message": "Claim disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionAllCategory: function(req, res, next) {
        
        Claim.getAllCategory().then(function(result) {
            var response = { "status": 1, "message": "Record Found", "category": result };
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = ClaimController;