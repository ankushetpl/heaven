var fs = require('fs');
var randomstring = require("randomstring");
var Promocode = require('../models/Promocode');
var Client = require('../models/Client');
var Worker = require('../models/Worker');
var commonHelper = require('../helper/common_helper.js');
var dateFormat = require('dateformat');

var PromocodeController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            promo_id: req.body.promo_id,
            promocode_servicetype : req.body.promocode_servicetype,
            promocode_weektype : req.body.promocode_weektype,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Promocode.countPromocode(searchParams).then(function (result) {
            total = result[0].total;
            Promocode.getPromocode(limit, offset, searchParams, orderParams).then(function (result) {

                var response = { "status": 1, "message": "Record Found", "promocode": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var promo_id = req.body.promo_id;
        
        Promocode.singlePromocode(promo_id).then(function (result) {
            if (typeof result !== 'undefined' && result !== '') {
                result.promo_startdate =  dateFormat(result.promo_startdate, "yyyy-mm-dd");
                result.promo_enddate =  dateFormat(result.promo_enddate, "yyyy-mm-dd");
                var response = { "status": 1, "message": "Record Found", "promocode": result };
                res.json(response);
            }                
        }).catch(function (error) {
            var response = { "status": 1, "message": "Record Found", "promocode": result };
            res.json(response);
        });    
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var promo_code  = randomstring.generate(6);
        
        if (typeof req.body.promo_banner !== 'undefined' && req.body.promo_banner !== '') {
            var banner = req.body.promo_banner;
        }else{
            var banner = 0;
        }

        var promocodeData = {
            promocode_servicetype : req.body.promocode_servicetype,
            promocode_weektype : req.body.promocode_weektype,
            promo_name: req.body.promo_name,
            promo_des: req.body.promo_des,
            promo_code : promo_code,
            promo_minamount: req.body.promo_minamount,
            promo_discount: req.body.promo_discount,
            promo_banner: banner,
            promo_startdate: req.body.promo_startdate,
            promo_enddate: req.body.promo_enddate,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        var total = 1;

        Promocode.addPromocode(promocodeData).then(function (result) {
           
            if(promocodeData.status == 1) {

                Client.getDeviceTokenAllUsers().then(function(resultAU) {

                    resultAU.forEachAsync(function(element, index, arr) {
                       
                        var msgtitle    = 'Promocode';
                        var msgbody   = 'Use "'+ promocodeData.promo_code +'" to get "'+ promocodeData.promo_discount +'" discount on onceoff/weekly service.';

                        var notSendData = { 
                            user_id : element.user_id, 
                            notification_type : msgtitle, 
                            notification_detail_id : result, 
                            notification_msg : msgbody, 
                            created_at: currentDate, 
                            updated_at: currentDate 
                        };
                        
                        Worker.addNotificationData(notSendData).then(function(resultAN) {
                            
                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                Worker.notifyBadgeCount(element.user_id).then(function(resultBC) {
                                    var badge = resultBC.length;
                                   
                                    if(element.device_type == 2) {
                                        
                                        Client.sendNotificationClient(element.device_token,msgtitle,msgbody,badge,resultNR).then(
                                            function(resultCSN) {
                                              
                                        }).catch(function (error) {
                                            var response = { "status": 1, "message": "Promo created successfully!" };
                                            res.json(response);
                                        });

                                    } else {
                                       
                                        Client.sendIOSNotification(element.device_token,msgtitle,msgbody,badge,resultNR).then(function(resultCSNI) {
                                          
                                            
                                        }).catch(function (error) {
                                            var response = { "status": 1, "message": "Promo code created successfully!" };
                                            res.json(response);
                                        });
                                    } 

                                    var response = { "status": 1, "message": "Promo code created successfully!" };
                                    res.json(response);
                                    
                                }).catch(function (error) {
                                    var response = { "status": 1, "message": "Promo code created successfully!" };
                                    res.json(response);
                                });

                            }).catch(function (error) {
                                var response = { "status": 1, "message": "Promo code created successfully!" };
                                res.json(response);
                            });

                        }).catch(function (error) {
                            var response = { "status": 1, "message": "Promo code created successfully!" };
                            res.json(response);
                        });

                    });
 
                }).catch(function (error) {
                    var response = { "status": 1, "message": "Promo code created successfully!" };
                    res.json(response);
                });

            } else {
                var response = { "status": 1, "message": "Promo code created successfully!" };
                res.json(response);
            }
         
        }).catch(function (error) {
            var response = { "status": 0, "message": "Promo code create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {

        var promo_id = req.body.promo_id;
        var currentDate = commonHelper.getCurrentDateTime();
        
        var promocodeData = {
            promocode_servicetype : req.body.promocode_servicetype,
            promocode_weektype : req.body.promocode_weektype,
            promo_name: req.body.promo_name,
            promo_des: req.body.promo_des,
            promo_minamount: req.body.promo_minamount,
            promo_discount: req.body.promo_discount,
            promo_banner: req.body.promo_banner,
            promo_startdate: req.body.promo_startdate,
            promo_enddate: req.body.promo_enddate,
            status: req.body.status,
            updated_at: currentDate
        };

        Promocode.updatePromocode(promocodeData, promo_id).then(function (result) {
            var response = { "status": 1, "message": "Promo code updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Promo code update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this promo code!" };
        var promo_id = req.body.promo_id;

        Promocode.deletePromocode(promo_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Promo code has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var promo_id = req.body.promo_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Promocode.statusPromocode(data, promo_id).then(function(result) {
            
            if(Status == 1){

                Client.getDeviceTokenAllUsers().then(function(resultAU) {

                    resultAU.forEachAsync(function(element, index, arr) {
                       
                        var msgtitle    = 'Promocode';
                        var msgbody   = 'Use "'+ promocodeData.promo_code +'" to get "'+ promocodeData.promo_discount +'" discount on onceoff/weekly service.';

                        var notSendData = { 
                            user_id : element.user_id, 
                            notification_type : msgtitle, 
                            notification_detail_id : promo_id, 
                            notification_msg : msgbody, 
                            created_at: currentDate, 
                            updated_at: currentDate 
                        };
                        
                        Worker.addNotificationData(notSendData).then(function(resultAN) {
                            
                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                Worker.notifyBadgeCount(element.user_id).then(function(resultBC) {
                                   
                                    if(element.device_type == 2) {
                                        
                                        Client.sendNotificationClient(element.device_token,msgtitle,msgbody).then(
                                            function(resultCSN) {
                                                
                                        });

                                    } else {
                                       
                                        Client.sendIOSNotification(element.device_token,msgtitle,msgbody,resultBC.length,resultNR).then(function(resultCSNI) {
                                          
                                        });

                                    } 

                                    var response = { "status": 1, "message": "Promo Code Actived", "notify" : resultNR, "badgeCount" : badge };
                                    res.json(response);
                                   
                                }).catch(function (error) {
                                    var response = { "status": 1, "message": "Promo Code Actived" };
                                    res.json(response);
                                });

                            }).catch(function (error) {
                                var response = { "status": 1, "message": "Promo Code Actived" };
                                res.json(response);
                            });

                        }).catch(function (error) {
                            var response = { "status": 1, "message": "Promo Code Actived" };
                            res.json(response);
                        });
                    }); 
                }).catch(function (error) {
                    var response = { "status": 0, "message": "Promo code create failed!" };
                    res.json(response);
                });
                           
            }else{
                var response = { "status": 1, "message": "Promo Code Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = PromocodeController;