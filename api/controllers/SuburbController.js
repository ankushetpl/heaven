var fs = require('fs');
var Suburb = require('../models/Suburb');
var commonHelper = require('../helper/common_helper.js');

var SuburbController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            suburb_id: req.body.suburb_id,
            suburb_name: req.body.suburb_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Suburb.countSuburb(searchParams).then(function (result) {
            total = result[0].total;
            Suburb.getSuburb(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "suburb": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },  

    actionView: function (req, res, next) {
        var suburb_id = req.body.suburb_id;

        if (typeof suburb_id !== 'undefined' && suburb_id !== '') {
            Suburb.singleSuburb(suburb_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "suburb": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var total = 0;
        var currentDate = commonHelper.getCurrentDateTime();

        var country = req.body.country;
        if (typeof country !== 'undefined' && country !== '') {
            var countryID = country; 
        }else{
            var countryID = ''; 
        }

        var state = req.body.state;
        if (typeof state !== 'undefined' && state !== '') {
            var stateID = state; 
        }else{
            var stateID = ''; 
        }

        var city = req.body.city;
        if (typeof city !== 'undefined' && city !== '') {
            var cityID = city; 
        }else{
            var cityID = ''; 
        }

        var suburbData = {
            country: countryID,
            state: stateID,
            city: cityID,
            suburb_name: req.body.suburb_name,
            lat: req.body.lat,
            long: req.body.long,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        var checkSuburb = {
            country: countryID,
            state: stateID,
            city: cityID,
            suburb_name: req.body.suburb_name            
        };

        Suburb.checkSuburb(checkSuburb).then(function (results) {
            total = results.total;
            if(total > 0){
                var response = { "status": 0, "message": "Suburb already exist!" };
                res.json(response);
            }else{
                Suburb.addSuburb(suburbData).then(function (result) {
                    var response = { "status": 1, "message": "Suburb created successfully!" };
                    res.json(response);
                }).catch(function (error) {
                    var response = { "status": 0, "message": "Suburb create failed!" };
                    res.json(response);
                });
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "Suburb create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var total = 0;
        var suburb_id = req.body.suburb_id;
        var currentDate = commonHelper.getCurrentDateTime();
        
        var country = req.body.country;
        if (typeof country !== 'undefined' && country !== '') {
            var countryID = country; 
        }else{
            var countryID = ''; 
        }

        var state = req.body.state;
        if (typeof state !== 'undefined' && state !== '') {
            var stateID = state; 
        }else{
            var stateID = ''; 
        }

        var city = req.body.city;
        if (typeof city !== 'undefined' && city !== '') {
            var cityID = city; 
        }else{
            var cityID = ''; 
        }

        var suburbData = {
            country: countryID,
            state: stateID,
            city: cityID,
            suburb_name: req.body.suburb_name,
            lat: req.body.lat,
            long: req.body.long,
            status: req.body.status,
            updated_at: currentDate
        };

        var checkSuburb = {
            country: countryID,
            state: stateID,
            city: cityID,
            suburb_name: req.body.suburb_name            
        };

        Suburb.checkSuburb(checkSuburb).then(function (results) {
            if(results.suburb_id == suburb_id){
                Suburb.updateSuburb(suburbData, suburb_id).then(function (result) {
                    var response = { "status": 1, "message": "Suburb updated successfully!" };
                    res.json(response);
                }).catch(function (error) {
                    var response = { "status": 0, "message": "Suburb update failed!" };
                    res.json(response);
                });
            }else{
                total = results.total;
                if(total > 0){
                    var response = { "status": 0, "message": "Suburb already exist!" };
                    res.json(response);
                }else{
                    Suburb.updateSuburb(suburbData, suburb_id).then(function (result) {
                        var response = { "status": 1, "message": "Suburb updated successfully!" };
                        res.json(response);
                    }).catch(function (error) {
                        var response = { "status": 0, "message": "Suburb update failed!" };
                        res.json(response);
                    });
                }
            }            
        }).catch(function (error) {
            var response = { "status": 0, "message": "Suburb create failed!" };
            res.json(response);
        }); 
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Suburb!" };
        var suburb_id = req.body.suburb_id;

        Suburb.checkExistsSuburb(suburb_id).then(function (resultCE) {

            if(resultCE.length > 0) {

                response = { status: 0, message: "You can not delete this suburb. This present in angels suburbs list." };
                res.json(response);
            } else {

                Suburb.deleteSuburb(suburb_id).then(function (result) {
                    if (result) {
                        response = { status: 1, message: "Your suburb has been deleted." };
                    }
                    res.json(response);
                }).catch(function (error) {
                    res.json(response);
                });
            }
           
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var suburb_id = req.body.suburb_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Suburb.statusSuburb(data, suburb_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Suburb Approved" };                
            }else{
                var response = { "status": 1, "message": "Suburb Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },
    getCity:function(req,res,next){
        // console.log(req.body);
         Suburb.getCity(req.body).then(function(result) {
            // console.log(result)        
                var response = { "status": 1, data: result[0] };                             
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });

    }

}

module.exports = SuburbController;