var fs = require('fs');
var Feedback = require('../models/Feedback');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var FeedbackController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }
      
        var searchParams = {
            feedback_id: req.body.feedback_id,
            role_id: req.body.role_id,
            feedback_note: req.body.feedback_note,
            created_at: req.body.created_at
        };

        Feedback.countFeedback(searchParams).then(function (result) {
            total = result[0].total;
            Feedback.getFeedback(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "feedback": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var feedback_id = req.body.feedback_id;

        if (typeof feedback_id !== 'undefined' && feedback_id !== '') {
            Feedback.singleFeedback(feedback_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "feedback": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var role_id = req.body.role_id;
        var feedbackData = {
            feedback_note: req.body.note,
            user_id: req.body.user_id,
            role_id: req.body.role_id,
            created_at: currentDate,
            updated_at: currentDate
        };

        Feedback.addFeedback(feedbackData).then(function (result) {
            if(role_id == 5){
                var response = { "status": 1, "message": "Your feedback has been successfully submitted" };
                res.json(response);
            }else{
                var response = { "status": 1, "message": "Feedback created successfully!" };
                res.json(response);
            }            
        }).catch(function (error) {
            var response = { "status": 0, "message": "Feedback create failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Feed Back!" };
        var feedback_id = req.body.feedback_id;

        Feedback.deleteFeedback(feedback_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Feedback has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionDeleteM: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Feed Back!" };
        var ID = req.body.ID;
        
        var IDNew = ID.join(',');
        
        Feedback.deleteFeedbackM(IDNew).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Feedback has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    //API
    actionAllFeedBack: function (req, res, next) {
        var user_id = req.body.user_id; 
        var total = 0;

        Feedback.getAllFeedback(user_id).then(function (result) {
            total = result.length;
            
            result.forEachAsync(function(element, index, arr) {
                element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                element.updated_at = dateFormat(element.updated_at, "yyyy-mm-dd hh:MM:ss");
               
            }, function() {
                if(total > 0){
                    var response = { "status": 1, "message": "Record Found", "feedback": result, "total": total };
                    res.json(response);
                }else{
                    var response = { "status": 1, "message": "No Record Found", "feedback": '', "total": 0 };
                    res.json(response);
                }                
            });
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }

}

module.exports = FeedbackController;