var fs = require('fs');
var PaymentRefund = require('../models/PaymentRefund');
var Client = require('../models/Client');
var Worker = require('../models/Worker');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var paymentRefundController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            booking_id: req.body.booking_id
        };

        PaymentRefund.countPaymentRefund(searchParams).then(function (result) {
            if(result.length > 0) {

                total = result[0].total;

                PaymentRefund.getAllRefundData(limit, offset, searchParams, orderParams).then(function (resultO) {

                    resultO.forEachAsync(function(element, index, arr) {
                        element.refundAmt = Number(element.booking_cost) - Number(element.booking_deduct);
                        
                    }, function() {
                        var response = { "status": 1, "message": "Record Found", "onceData": resultO, "current": page, "pages": Math.ceil(total / limit), "total": total };
                        res.json(response);
                    });
                   
                }).catch(function (error) {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                });

            } else {
                
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
            

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionIndexWeek: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            booking_id: req.body.booking_id
        };

            PaymentRefund.countPaymentRefundWeek(searchParams).then(function (resultCW) {

                if(resultCW.length > 0) {

                    total = resultCW[0].total;

                    PaymentRefund.getAllRefundWeekData(limit, offset, searchParams, orderParams).then(function (resultW) {

                        resultW.forEachAsync(function(element, index, arr) {
                            element.refundAmt = Number(element.booking_cost) - Number(element.booking_deduct);

                        }, function() {
                            var response = { "status": 1, "message": "Record Found", "weekData": resultW, "current": page, "pages": Math.ceil(total / limit), "total": total };
                            res.json(response);
                        });

                    }).catch(function (error) {
                        var response = { "status": 0, "message": "No Record Exist" };
                        res.json(response);
                    });

                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
     },    
    
    actionView: function (req, res, next) {
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;

        if (typeof booking_id !== 'undefined' && booking_id !== '') {
            PaymentRefund.singleRefundData(booking_id,serviceflag).then(function (result) {

                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "clientData": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionRefund: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();

        var clientId = req.body.client_id;
        var bookingId = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var refundAmt = req.body.refundAmt;

        var topUpData = {
            client_id: clientId,
            booking_id:bookingId,
            serviceflag:serviceflag,
            referralcredit_points: refundAmt,
            referralcredit_type: '4',
            created_at: currentDate,
            updated_at: currentDate
        };

        var refundData = {
            client_id: clientId,
            booking_id:bookingId,
            booking_serviceflag:serviceflag,
            wallet_amount: refundAmt,
            wallet_type: '2',
            created_at: currentDate
        };

        PaymentRefund.topUpWallet(topUpData, clientId, refundData).then(function (result) {

            if(result.length > 0) {

                var finalAmt = Number(result[0].credit_points) + Number(refundAmt);

                    PaymentRefund.updateWallet(clientId, finalAmt, serviceflag, bookingId).then(function (resultU) {

                        var deviceToken = result[0].device_token;
                        var deviceType = result[0].device_type;
                        var msgtitle = 'Refund Wallet';
                        var msgbody = 'Your wallet has been refunded with amount ' + refundAmt + ' by Heaven Sent admin.';

                        var notSendData = { user_id: clientId, notification_type: msgtitle, notification_detail_id : bookingId, notification_detail_serviceflag:serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                    var badge = resultBC.length;

                                    if (deviceType == 2) {

                                        Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {

                                            var response = { "status":1, "message": "Payment refunded successfully." };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status":1, "message": "Payment refunded successfully." };
                                            res.json(response);
                                        });

                                    } else if (deviceType == 3) {

                                        Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                            var response = { "status":1, "message": "Payment refunded successfully." };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Payment refunded successfully.", "notify": resultNR, "badgeCount": badge };
                                            res.json(response);
                                        });

                                    } else {
                                        var response = { "status":1, "message": "Payment refunded successfully." };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status":1, "message": "Payment refunded successfully." };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status":1, "message": "Payment refunded successfully." };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status":1, "message": "Payment refunded successfully." };
                            res.json(response);
                        });
                    
                    }).catch(function (error) {
                        var response = { "status": 0, "message": "Failed to refund!" };
                        res.json(response);
                    });

            } else {
                var response = { "status":0, "message": "No data found for client." };
                res.json(response);
            }
           
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to refund!" };
            res.json(response);
        });
    }

}

module.exports = paymentRefundController;