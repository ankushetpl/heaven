var fs = require('fs');
var TodayBooking = require('../models/TodayBooking');
var Payment = require('../models/Payment');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');
// var site_url     = 'http://ec2-18-221-244-173.us-east-2.compute.amazonaws.com/heaven';
var site_url = 'http://localhost/heaven';

var TodayBookingController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            booking_id: req.body.booking_id,
            clients_name: req.body.clients_name,
            workers_name: req.body.workers_name,
            booking_date: req.body.booking_date
         };

        TodayBooking.countBookings(searchParams).then(function (resultO) {

            // AllBooking.countBookingsWeekly(searchParams).then(function (resultW) {

                // total = parseInt(resultO[0].total) + parseInt(resultW[0].total);
                total = resultO[0].total;
                
                TodayBooking.getBookings(limit, offset, searchParams, orderParams).then(function (result) {

                    // if(resultR.length > 1) {

                    //     AllBooking.getBookingsWeekly(limit, offset, searchParams, orderParams).then(function (resultWR) {

                    //         if(resultWR.length > 1) {

                    //             var result = resultR.concat(resultWR);

                    //             var response = { "status": 1, "message": "Record Found", "todayBook": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                    //             res.json(response);

                    //         } else {

                    //             var result = resultR;

                    //             var response = { "status": 1, "message": "Record Found", "todayBook": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                    //             res.json(response);
                    //         } 

                    //     }).catch(function (error) {

                    //         var response = { "status": 0, "message": "No Record Exist" };
                    //         res.json(response);
                    //     });

                    // } else {

                    //     TodayBookings.getBookingsWeekly(limit, offset, searchParams, orderParams).then(function (resultWR) {
                           
                    //         var result = resultWR;

                            var response = { "status": 1, "message": "Record Found", "todayBooking": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                            res.json(response);

                    //     }).catch(function (error) {

                    //         var response = { "status": 0, "message": "No Record Exist for weekly" };
                    //         res.json(response);
                    //     });

                    // }

                }).catch(function (error) {

                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                });

            // }).catch(function (error) {

            //     var response = { "status": 0, "message": "No Record Exist" };
            //     res.json(response);
            // });

        }).catch(function (error) {

            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var booking_id = req.body.booking_id;

        if (typeof booking_id !== 'undefined' && booking_id !== '') {

            TodayBooking.singleBooking(booking_id).then(function (result) {

                if (typeof result != 'undefined' && result != '') {

                    Payment.getImages().then(function(resultImage) {
                                
                        if(resultImage.length > 0) {
                            
                            for (var k = 0; k < resultImage.length; k++) {

                                if(result.clients_image == resultImage[k].file_id) {
                                    result.clients_image = site_url + resultImage[k].file_base_url;
                                }
                                if(result.workers_image == resultImage[k].file_id) {
                                    result.workers_image = site_url + resultImage[k].file_base_url;
                                }
                            }
                                   
                                var response = { "status": 1, "message": "Record Found", "todayBooking": result };
                                res.json(response);
                                    
                        } else {
                            var response = { "status": 1, "message": "Record Found", "todayBooking": result };
                            res.json(response);
                        }   
                                       
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not Found" };
                        res.json(response);
                    });

                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }


}

module.exports = TodayBookingController;