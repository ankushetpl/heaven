var fs = require('fs');
var ServiceType = require('../models/ServiceType');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var ServiceTypeController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            tasktype_id: req.body.tasktype_id,
            tasktype_name: req.body.tasktype_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        ServiceType.countServiceType(searchParams).then(function (result) {
            total = result[0].total;
            ServiceType.getServiceType(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "serviceType": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var tasktype_id = req.body.tasktype_id;

        if (typeof tasktype_id !== 'undefined' && tasktype_id !== '') {
            ServiceType.singleServiceType(tasktype_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "serviceType": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var serviceTypeData = {
            tasktype_name: req.body.tasktype_name,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        ServiceType.addServiceType(serviceTypeData).then(function (result) {
            var response = { "status": 1, "message": "Service created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Service create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var tasktype_id = req.body.tasktype_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var serviceTypeData = {
            tasktype_name: req.body.tasktype_name,
            status: req.body.status,
            updated_at: currentDate
        };

        ServiceType.updateServiceType(serviceTypeData, tasktype_id).then(function (result) {
            var response = { "status": 1, "message": "Service updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Service update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Service Type!" };
        var tasktype_id = req.body.tasktype_id;

        ServiceType.deleteServiceType(tasktype_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Service has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var tasktype_id = req.body.tasktype_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        ServiceType.statusServiceType(data, tasktype_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Service Approved" };                
            }else{
                var response = { "status": 1, "message": "Service Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionCheckBooking: function (req, res, next) {
        var tasktype_id = req.body.tasktype_id;
        var total = 0;
        ServiceType.getCheckBooking(tasktype_id).then(function(result) {
            if (typeof result != 'undefined' && result != '') {
                total = result.total;
                if(total > 0){
                    var response = { "status": 1 };                
                    res.json(response);           
                }else{
                    var response = { "status": 2 };                
                    res.json(response);           
                }                
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = ServiceTypeController;