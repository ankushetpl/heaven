var fs = require('fs');
var Setting = require('../models/Setting');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var SettingController = {

    actionIndex: function (req, res, next) {        

        Setting.getSetting().then(function (result) {
            var response = { "status": 1, "message": "Record Found", "setting": result };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
        
    },

    actionUpdate: function (req, res, next) {
        var setting_id = req.body.setting_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var settingData = {
            setting_value: req.body.setting_value,
            updated_at: currentDate
        };

        Setting.updateSetting(settingData, setting_id).then(function (result) {
            var response = { "status": 1, "message": "Setting updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Setting update failed!" };
            res.json(response);
        });
    }

}

module.exports = SettingController;