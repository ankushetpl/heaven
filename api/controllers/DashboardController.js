var fs = require('fs');
var Dashboard = require('../models/Dashboard');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var DashboardController = {

    actionCount: function (req, res, next) {
        
        Dashboard.countDATA().then(function (result) {
            var doneBookings = result[2][0].Booking + result[3][0].WeeklyBooking;
            var response = { 
                "status": 1, 
                "worker": result[0][0].Worker, 
                "client": result[1][0].Client, 
                "booking": doneBookings, 
                "earlyPay": result[4][0].EarlyPay, 
                "cafe": result[5][0].Cafe,
                "assessor": result[6][0].Assessor,
                "logPic": result[7][0].logPic
            };
            res.json(response);

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });

    },


        //VIEW client
    actionNotification: function(req, res, next) {
       
        Dashboard.getNotificationData().then(function(results) {
           
            var response = { "status": 1, "message": "not data", "not" : results };
            res.json(response);
                      
        }).catch(function(error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });

    }

}

module.exports = DashboardController;