var fs = require('fs');
var WalletTopUp = require('../models/WalletTopUp');
var Client = require('../models/Client');
var Worker = require('../models/Worker');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var WalletTopUpController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            contact: req.body.contact
        };

        WalletTopUp.countWalletTopUp(searchParams).then(function (result) {
            total = result[0].total;

            WalletTopUp.getAllClients(limit, offset, searchParams, orderParams).then(function (result) {

                var response = { "status": 1, "message": "Record Found", "clients": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var client_id = req.body.client_id;

        WalletTopUp.getCreditHistory(client_id).then(function (result) {

            if (result.length > 0) {
                var response = { "status": 1, "message": "Record Found", "creditHistory": result };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist for credit history" };
                res.json(response);
            }

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });

    },

    actionTopUp: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();

        var clientId = req.body.user_id;
        var topUpAmt = req.body.amount;

        var topuUpData = {
            client_id: clientId,
            wallet_amount: topUpAmt,
            wallet_type: '1',
            created_at: currentDate
        };

        var creditData = {
            client_id: clientId,
            referralcredit_points: topUpAmt,
            referralcredit_type: '5',
            created_at: currentDate
        };

        WalletTopUp.topUpWallet(topuUpData, clientId, creditData).then(function (result) {

            if(result.length > 0) {

                var finalAmt = Number(result[0].credit_points) + Number(topUpAmt);

                WalletTopUp.updateWallet(clientId, finalAmt).then(function (resultU) {

                    var deviceToken = result[0].device_token;
                    var deviceType = result[0].device_type;
                    var msgtitle = 'Top Up Wallet';
                    var msgbody = 'Your wallet has been top up with amount ' + topUpAmt + ' by Heaven Sent admin.';

                    var notSendData = { user_id: clientId, notification_type: msgtitle, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                    Worker.addNotificationData(notSendData).then(function(resultAN) {

                        Worker.getNotResponse(resultAN).then(function(resultNR) {

                            Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                var badge = resultBC.length;

                                if (deviceType == 2) {

                                    Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {

                                        var response = { "status": 1, "message": "Wallet top up successfully." };
                                        res.json(response);


                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Wallet top up successfully." };
                                        res.json(response);
                                    });

                                } else if (deviceType == 3) {

                                    Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                        var response = { "status": 1, "message": "Wallet top up successfully." };
                                        res.json(response);


                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Wallet top up successfully.", "notify": resultNR, "badgeCount": badge };
                                        res.json(response);
                                    });

                                } else {
                                    var response = { "status":1, "message": "Wallet top up successfully." };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status":1, "message": "Wallet top up successfully." };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status":1, "message": "Wallet top up successfully." };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status":1, "message": "Wallet top up successfully." };
                        res.json(response);
                    });

                }).catch(function (error) {
                    var response = { "status": 0, "message": "Failed to top up!" };
                    res.json(response);
                });

            } else {
                var response = { "status":0, "message": "No data found for client." };
                res.json(response);
            }
           
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to top up!" };
            res.json(response);
        });
    }

}

module.exports = WalletTopUpController;