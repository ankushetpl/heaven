var fs = require('fs');
var Checklist = require('../models/Checklist');
var commonHelper = require('../helper/common_helper.js');

var ChecklistController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            checklist_id  : req.body.checklist_id,
            checklist_name: req.body.checklist_name,
            status        : req.body.status,
            created_at    : req.body.created_at
        };

        Checklist.countChecklist(searchParams).then(function (result) {
            total = result[0].total;
            Checklist.getChecklist(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "checklist": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    // view checklist data
    actionView: function (req, res, next) {
        var checklist_id = req.body.checklist_id;

        if (typeof checklist_id !== 'undefined' && checklist_id !== '') {
            Checklist.singleChecklist(checklist_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "checklist": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    // add checklist data
    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var checklistData = { 
            checklist_name: req.body.checklist_name,
            status: req.body.status, 
            created_at: currentDate,
            updated_at : currentDate
        };
             
        Checklist.addChecklist(checklistData).then(function (result) {
            var response = { "status": 1, "message": "Checklist created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Checklist create failed!" };
            res.json(response);
        });
    },

    //update  checklist data
    actionUpdate: function (req, res, next) {
        var checklist_id = req.body.checklist_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var checklistData = { 
            checklist_name: req.body.checklist_name,
            status: req.body.status, 
            updated_at: currentDate
        };

        Checklist.updateChecklist(checklistData, checklist_id).then(function (result) {
            var response = { "status": 1, "message": "Checklist updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Checklist update failed!" };
            res.json(response);
        });
    },

    // delete checklist data
    actionDelete: function (req, res, next) {
        
        var response = { status: 0, message: "Unable to delete this Checklist!" };
        var checklist_id = req.body.checklist_id;

        Checklist.deleteChecklist(checklist_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Checklist has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    //Status checklist
    actionStatus: function(req, res, next) {
        var ChecklistId = req.body.checklist_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Checklist.statusChecklist(data, ChecklistId).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Checklist Approved" };                
            }else{
                var response = { "status": 1, "message": "Checklist Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }   

}

module.exports = ChecklistController;