var fs = require('fs');
var md5 = require('md5');
var randomstring = require("randomstring");
var dateFormat = require('dateformat');
var nodemailer = require('nodemailer');
var User = require('../models/User');
var commonHelper = require('../helper/common_helper.js');
var site_url = commonHelper.getSiteURL();

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: { 
        user: 'etpl18node@gmail.com', 
        pass: 'nodetest123' 
    }
});

var UserController = {

    // All User List
    actionIndex: function(req, res, next) {
    	var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;
        var usertype = req.body.usertype;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            user_id: req.body.user_id,
            admin_name: req.body.admin_name,
            status: req.body.status,
            created_at: req.body.created_at,
            is_suspend: req.body.is_suspend
        };
        
        User.countUser(searchParams, usertype).then(function (result) {
        	total = result[0].total;
            User.getUser(limit, offset, searchParams, orderParams, usertype).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "users": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },

    //Delete User
    actionDelete: function(req, res, next) {
        var UserId = req.body.user_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { is_deleted: 1 };

        var response = { "status": 0, "message": "Unable to delete this User!" };
        
        User.deleteUser(data, UserId).then(function(result) {
            if (result) {
                var response = { "status": 1, "message": "Deleted Successfully" };
            }
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //Status User
    actionStatus: function(req, res, next) {
        var UserId = req.body.user_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        User.statusUser(data, UserId).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Account Approved" };                
            }else{
                var response = { "status": 1, "message": "Account Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //Suspend User
    actionSuspend: function(req, res, next) {
        var UserId = req.body.user_id;
        var Suspend = req.body.is_suspend;
        var UserType = req.body.usertype;

        var currentDate = commonHelper.getCurrentDateTime();

        var suspendStart = req.body.suspendStart;
        if (typeof suspendStart !== 'undefined' && suspendStart !== '') {
            var suspendStartID = suspendStart; 
        }else{
            var suspendStartID = ''; 
        }

        var suspendEnd = req.body.suspendEnd;
        if (typeof suspendEnd !== 'undefined' && suspendEnd !== '') {
            var suspendEndID = suspendEnd; 
        }else{
            var suspendEndID = ''; 
        }

        if(UserType == 1 || UserType == 6){
            var data = {};            
        }else{
            if(UserType == 2){
                var data = { 
                    cafeusers_suspendStart: suspendStartID,
                    cafeusers_suspendEnd: suspendEndID
                };                
            }else{
                if(UserType == 3){
                    var data = { 
                        workers_suspendStart: suspendStartID,
                        workers_suspendEnd: suspendEndID
                    };                     
                }else{
                    if(UserType == 4){
                        var data = { 
                            assessors_suspendStart: suspendStartID,
                            assessors_suspendEnd: suspendEndID
                        };                        
                    }else{
                        var data = { 
                            clients_suspendStart: suspendStartID,
                            clients_suspendEnd: suspendEndID
                        };                           
                    }
                }
            }
        }      

        if(Suspend == 1){
            var UserData = { 
                suspendStart: suspendStartID,
                suspendEnd: suspendEndID,
                is_suspend: 0
            };                  
        }else{
            var UserData = { 
                suspendStart: suspendStartID,
                suspendEnd: suspendEndID,
                is_suspend: 1
            };
        } 

        User.suspendUser(UserData, UserId, UserType, data).then(function(result) {
            if(Suspend == 1){
                var response = { "status": 1, "message": "Account Deactived For Suspend" };                
            }else{
                var response = { "status": 1, "message": "Account Actived For Suspend" };                
            }
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //ADD USER INTERNATE CAFE
    actionCreate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var Password = randomstring.generate(8);

        var status = req.body.status;
        if (typeof status !== 'undefined' && status !== '') {
            var statusID = status; 
        }else{
            var statusID = ''; 
        }

        var country = req.body.cafeusers_country;
        if (typeof country !== 'undefined' && country !== '') {
            var countryID = country; 
        }else{
            var countryID = ''; 
        }

        var state = req.body.cafeusers_state;
        if (typeof state !== 'undefined' && state !== '') {
            var stateID = state; 
        }else{
            var stateID = ''; 
        }

        var city = req.body.cafeusers_city;
        if (typeof city !== 'undefined' && city !== '') {
            var cityID = city; 
        }else{
            var cityID = ''; 
        }

        var suburb = req.body.cafeusers_suburb;
        if (typeof suburb !== 'undefined' && suburb !== '') {
            var suburbID = suburb; 
        }else{
            var suburbID = ''; 
        }

        var imageID = req.body.assessors_image;
        if (typeof imageID !== 'undefined' && imageID !== '') {
            var imagesID = imageID; 
        }else{
            var imagesID = ''; 
        }

        var addUsers = { 
            user_username: req.body.email, 
            user_password: md5(Password),
            created_at: currentDate, 
            updated_at: currentDate, 
            user_role_id: req.body.usertype, 
            user_registertype: req.body.registertype,
            status: statusID
        };

        var Username = req.body.email;
        var Email = req.body.email;   
        var UserType = req.body.usertype;     
        var total = 0;
        var totalEmail = 0;
        
        User.checkUserExists(Username).then(function(result) {
            total = result.length;
            if (total == 0) {
            	User.checkUserEmailExists(Email).then(function(results) {
            		totalEmail = results.length;
            		if (totalEmail == 0) {
                        if(UserType == 1 || UserType == 6){
                            var Info = { 
                                admin_name: req.body.name, 
                                admin_email: req.body.email, 
                                admin_phone: req.body.phone 
                            };
                            var URL = site_url+'/backoffice';
                        }else{
                            if(UserType == 2){
                                var Info = { 
                                    cafeusers_name: req.body.name, 
                                    cafeusers_email: req.body.email, 
                                    cafeusers_phone: req.body.phone, 
                                    cafeusers_address: req.body.address,
                                    cafeusers_country: countryID, 
                                    cafeusers_state: stateID, 
                                    cafeusers_city: cityID,
                                    cafeusers_suburb: suburbID
                                };
                                var URL = site_url+'/frontoffice';
                            }else{
                                if(UserType == 3){
                                    var Info = { workers_name: req.body.name, workers_email: req.body.email, workers_phone: req.body.phone, workers_address: req.body.address, cafeuser_id: req.body.cafeID, workers_image: req.body.image };
                                    var URL = '';
                                }else{
                                    if(UserType == 4){
                                        var Info = { 
                                            assessors_name: req.body.name, 
                                            assessors_email: req.body.email, 
                                            assessors_phone: req.body.phone, 
                                            assessors_address: req.body.address,
                                            assessors_country: countryID, 
                                            assessors_state: stateID, 
                                            assessors_city: cityID,
                                            assessors_suburb: suburbID,
                                            assessors_image: imagesID
                                        };
                                        var URL = site_url+'/frontoffice';
                                    }else{

                                        var mobileID = req.body.clients_mobile;
                                        if (typeof mobileID !== 'undefined' && mobileID !== '') {
                                            var mobilesID = mobileID; 
                                        }else{
                                            var mobilesID = ''; 
                                        }

                                        var Info = { 
                                            clients_name: req.body.name, 
                                            clients_email: req.body.email, 
                                            clients_phone: req.body.phone,
                                            clients_mobile: mobilesID,
                                            clients_address: req.body.address,
                                            clients_country: countryID, 
                                            clients_state: stateID, 
                                            clients_city: cityID,
                                            clients_suburb: suburbID,
                                            clients_image: imagesID,
                                            clients_referID: randomstring.generate(6)
                                        };
                                        var URL = '';
                                    }
                                }
                            }
                        }

		                User.addUser(addUsers, Info, UserType).then(function(result) {	
                            if (typeof result !== 'undefined' && result !== '') {
                                User.getTemplete('Registration').then(function(resultM) { 
                                    var html = resultM.email_content;
                                    html = html.replace('{{TempName}}', req.body.name);
                                    html = html.replace('{{TempEmailID}}', Username);
                                    html = html.replace('{{TempPassword}}', Password);

                                    if(statusID == 0){
                                        var Note = 'Note : Account not active yet? Please connect with our team.'; 
                                        html = html.replace('{{TempNoteContent}}', Note);
                                    }else{
                                        html = html.replace('{{TempNoteContent}}', '');
                                    }                                           

                                    html = html.replace('{{TempURL}}', URL);

                                    var mailOptions = { 
                                        to: Username, 
                                        from: "Heavenly Sent <etpl18node@gmail.com>",
                                        subject: 'Login Details',  
                                        html: html 
                                    };

                                    transporter.sendMail(mailOptions, function(error, info){
                                        if (error) {
                                            var response = {
                                                "status": 1,
                                                "message": "Added Successfully, But Mail Not Send",
                                                "data": result 
                                            };
                                            res.json(response);
                                        } else {
                                            var response = { 
                                                "status": 1, 
                                                "message": "Added Successfully. Please Check Your Mail.", 
                                                "data": result 
                                            };
                                            res.json(response);                               
                                        }                             
                                    });

                                }).catch(function(error) {
                                    var response = {
                                        "status": 1,
                                        "message": "Added Successfully, But Mail Not Send",
                                        "data": result 
                                    };
                                    res.json(response);
                                });
                            }
		                }).catch(function(error) {
		                    var response = { "status": 0, "message": "Added failed" };
		                    res.json(response);
		                });
		            }else{
		            	var response = { "status": 3, "message": "Email ID Already Exist." };
                		res.json(response);
		            }
                });
            } else {            	
                var response = { "status": 2, "message": "Email ID Already Exist." };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });
    },

    //EDIT USER INTERNATE CAFE
    actionUpdate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id = req.body.user_id;
        var usertype = req.body.usertype;

        var status = req.body.status;
        if (typeof status !== 'undefined' && status !== '') {
            var statusID = status; 
        }else{
            var statusID = ''; 
        }

        var country = req.body.cafeusers_country;
        if (typeof country !== 'undefined' && country !== '') {
            var countryID = country; 
        }else{
            var countryID = ''; 
        }

        var state = req.body.cafeusers_state;
        if (typeof state !== 'undefined' && state !== '') {
            var stateID = state; 
        }else{
            var stateID = ''; 
        }

        var city = req.body.cafeusers_city;
        if (typeof city !== 'undefined' && city !== '') {
            var cityID = city; 
        }else{
            var cityID = ''; 
        }

        var suburb = req.body.cafeusers_suburb;
        if (typeof suburb !== 'undefined' && suburb !== '') {
            var suburbID = suburb; 
        }else{
            var suburbID = ''; 
        }

        var imageID = req.body.assessors_image;
        if (typeof imageID !== 'undefined' && imageID !== '') {
            var imagesID = imageID; 
        }else{
            var imagesID = ''; 
        }

        if(usertype == 1 || usertype == 6){
            
            var AddressID = req.body.admin_address;
            if (typeof AddressID !== 'undefined' && AddressID !== '') {
                var AddrID = AddressID; 
            }else{
                var AddrID = ''; 
            }

            var Data = { 
                admin_name: req.body.name, 
                admin_phone: req.body.phone,
                admin_address: AddrID,
                admin_image: imagesID
            };
            var userData = { 
                status: statusID, 
                updated_at: currentDate 
            };
        }else{
            if(usertype == 2){
            	var Data = { 
                    cafeusers_name: req.body.name, 
                    cafeusers_phone: req.body.phone, 
                    cafeusers_address: req.body.address,
                    cafeusers_country: countryID, 
                    cafeusers_state: stateID, 
                    cafeusers_city: cityID,
                    cafeusers_suburb: suburbID,
                    cafeusers_image: imagesID
                };
            	var userData = { 
                    status: statusID, 
                    updated_at: currentDate 
                };
            }else{
                if(usertype == 3){
                    // var suburbTab = [];

                    var mobileID = req.body.workers_mobile;
                    if (typeof mobileID !== 'undefined' && mobileID !== '') {
                        var mobilesID = mobileID; 
                    }else{
                        var mobilesID = ''; 
                    }

                    // var suburb1 = req.body.suburbA;
                    // if (typeof suburb1 !== 'undefined' && suburb1 !== '') {
                    //     suburbTab.push(suburb1); 
                    // }

                    // var suburb2 = req.body.suburbB;
                    // if (typeof suburb2 !== 'undefined' && suburb2 !== '') {
                    //     suburbTab.push(suburb2); 
                    // }

                    // var suburb3 = req.body.suburbC;
                    // if (typeof suburb3 !== 'undefined' && suburb3 !== '') {
                    //     suburbTab.push(suburb3); 
                    // }

                    // var suburb4 = req.body.suburbD;
                    // if (typeof suburb4 !== 'undefined' && suburb4 !== '') {
                    //     suburbTab.push(suburb4); 
                    // }
                    var suburbs  = req.body.workers_suburb;
                    var Data = { 
                        cafeuser_id: req.body.cafeID,
                        workers_name: req.body.name, 
                        workers_phone: 27+req.body.phone,
                        workers_mobile: 27+mobilesID,
                        workers_address: req.body.address,
                        workers_country: countryID, 
                        workers_state: stateID, 
                        workers_city: cityID,
                        workers_suburb: suburbs.toString()

                    };
                    var userData = { 
                        updated_at: currentDate 
                    };
                }else{
                    if(usertype == 4){
                        var Data = { 
                            assessors_name: req.body.name, 
                            assessors_phone: req.body.phone, 
                            assessors_address: req.body.address,
                            assessors_country: countryID, 
                            assessors_state: stateID, 
                            assessors_city: cityID,
                            assessors_suburb: suburbID,
                            assessors_image: imagesID
                        };
                        var userData = { 
                            status: req.body.status, 
                            updated_at: currentDate 
                        };
                    }else{
                        
                        var mobileID = req.body.clients_mobile;
                        if (typeof mobileID !== 'undefined' && mobileID !== '') {
                            var mobilesID = mobileID; 
                        }else{
                            var mobilesID = ''; 
                        }

                    	var Data = { 
                            clients_name: req.body.name, 
                            clients_phone: req.body.phone,
                            clients_mobile: mobilesID,
                            clients_address: req.body.address,
                            clients_country: countryID, 
                            clients_state: stateID, 
                            clients_city: cityID,
                            clients_suburb: suburbID,
                            clients_image: imagesID
                        };
                        var userData = { 
                            status: req.body.status, 
                            updated_at: currentDate 
                        };
                    }
                }
            } 
        }       

        User.updateUser(user_id, usertype, Data, userData).then(function(result) {
        	var response = { "status": 1, "message": "Profile updated successfully" };
            res.json(response);
        }).catch(function(error) {
            var response = { "status": 0, "message": "Updated failed" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
    	var user_id = req.body.user_id;
        var UserType = req.body.UserType;
        
        if (typeof user_id !== 'undefined' && user_id !== '') {
            User.singleUser(user_id, UserType).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionAllList: function (req, res, next) {
        var user_id = req.body.usertype;
        if (typeof user_id !== 'undefined' && user_id !== '') {
            User.getAllUser(user_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionAllSubList: function (req, res, next) {
        var cafeuser_id = req.body.cafeID;
        User.getAllSubUser(cafeuser_id).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found", "data": result };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
        
    },

    actionPermissions: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id = req.body.user_id;
        var module = req.body.modules;
        User.savePermissions(user_id, module, currentDate).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found" };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
        
    },

    actionViewPermissions: function (req, res, next) {
        var user_id = req.body.user_id;
        
        if (typeof user_id !== 'undefined' && user_id !== '') {
            User.viewPermissions(user_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCheckStatus: function (req, res, next) {
        var user_id = req.body.user_id;
        
        if (typeof user_id !== 'undefined' && user_id !== '') {
            User.checkStatus(user_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCafeName: function (req, res, next) {
        var cafe_id = req.body.cafe_id;
        
        if (typeof cafe_id !== 'undefined' && cafe_id !== '') {
            User.getCafeName(cafe_id).then(function (result) {

                var response = { "status": 1, "message": "cafe name", "cafe": result.cafeusers_name };
                res.json(response);
                
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },


    actionWorkerName: function (req, res, next) {
        var worker_id = req.body.worker_id;
        
        if (typeof worker_id !== 'undefined' && worker_id !== '') {
            User.getWorkerName(worker_id).then(function (result) {

                var response = { "status": 1, "message": "worker name", "worker": result.workers_name };
                res.json(response);
                
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }



}

module.exports = UserController;