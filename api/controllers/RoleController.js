var fs = require('fs');
var Role = require('../models/Role');
var commonHelper = require('../helper/common_helper.js');

var RoleController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            role_id: req.body.role_id,
            role_name: req.body.role_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Role.countRole(searchParams).then(function (result) {
            total = result[0].total;
            Role.getRoles(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "roles": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var role_id = req.body.role_id;

        if (typeof role_id !== 'undefined' && role_id !== '') {
            Role.singleRole(role_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "role": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var roleData = {
            role_name: req.body.role_name,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Role.addRole(roleData).then(function (result) {
            var response = { "status": 1, "message": "Role created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Role create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var role_id = req.body.role_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var roleData = {
            role_name: req.body.role_name,
            status: req.body.status,
            updated_at: currentDate
        };

        Role.updateRole(roleData, role_id).then(function (result) {
            var response = { "status": 1, "message": "Role updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Role update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Role!" };
        var role_id = req.body.role_id;

        Role.deleteRole(role_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Role has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    }

}

module.exports = RoleController;