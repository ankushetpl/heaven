var fs = require('fs');
var Modules = require('../models/Modules');
var commonHelper = require('../helper/common_helper.js');

var ModulesController = {

    actionIndex: function (req, res, next) {
        
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            module_id : req.body.module_id,
            module_name : req.body.module_name,
            parent_id : req.body.parent_id,
            status : req.body.status,
            created_at : req.body.created_at
        };

        Modules.countModules(searchParams).then(function (result) {
            total = result[0].total;
            Modules.getModules(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "module": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    // view Modules data
    actionView: function (req, res, next) {
        var module_id = req.body.module_id;
        if (typeof module_id !== 'undefined' && module_id !== '') {
            Modules.singleModule(module_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "module": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    // add Modules data
    actionCreate: function (req, res, next) {

        var moduleName = req.body.module_name;
        var lowerCaseName = moduleName.toLowerCase();
        lowerCaseName = lowerCaseName.replace(/ /g, "-");   
        
        var currentDate = commonHelper.getCurrentDateTime();
        var parent_id = req.body.parent_id;
         if (typeof parent_id !== 'undefined' && parent_id !== '') {        
            var modulesData = { module_name: req.body.module_name, module_alias: lowerCaseName, status: req.body.status, parent_id: parent_id, created_at: currentDate, updated_at : currentDate };
        } else {
            var modulesData = { module_name: req.body.module_name, module_alias: lowerCaseName, status: req.body.status, created_at: currentDate, updated_at : currentDate};
        }
          
        Modules.addModules(modulesData).then(function (result) {
            var response = { "status": 1, "message": "Modules created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Modules create failed!" };
            res.json(response);
        });
    },

    //update  Modules data
    actionUpdate: function (req, res, next) {
        var module_id = req.body.module_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var parent_id = req.body.parent_id;

        var moduleName = req.body.module_name;
        var lowerCaseName = moduleName.toLowerCase();
        lowerCaseName = lowerCaseName.replace(/ /g, "-");

         if (typeof parent_id !== 'undefined' && parent_id !== '') {        
            var modulesData = { module_name: req.body.module_name, module_alias: lowerCaseName, status: req.body.status, parent_id: parent_id, updated_at : currentDate };
        } else {
            var modulesData = { module_name: req.body.module_name, module_alias: lowerCaseName, status: req.body.status,  updated_at: currentDate};
        }                

        Modules.updateModules(modulesData, module_id).then(function (result) {
            var response = { "status": 1, "message": "Module updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Module update failed!" };
            res.json(response);
        });
    },

    // delete Modules data
    actionDelete: function (req, res, next) {
        
        var response = { status: 0, message: "Unable to delete this Module!" };
        var module_id = req.body.module_id;

        Modules.deleteModules(module_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Module has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    //Status Modules
    actionStatus: function(req, res, next) {
        var module_id = req.body.module_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Modules.statusModules(data, module_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Module Actived" };                
            }else{
                var response = { "status": 1, "message": "Module Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    // All Parent Modules List
    actionAllList: function (req, res, next) {
        Modules.getAllParent().then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found", "data": result };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },    

    // All Modules List
    actionAll: function (req, res, next) {
        Modules.getAll().then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found", "data": result };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });        
    },      

    //Get single Parent
    actionParent: function (req, res, next) {
        var parent_id = req.body.obj;
        if (typeof parent_id !== 'undefined' && parent_id !== '') {
            Modules.getParent(parent_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "data": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    //Check Parent
    actionCheck: function (req, res, next) {
        var total = 0;
        var module_id = req.body.module_id;
        if (typeof module_id !== 'undefined' && module_id !== '') {
            Modules.getCheck(module_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    total = result.total;
                    var response = { "status": 1, "message": "Record Found", "data": total };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }

}

module.exports = ModulesController;