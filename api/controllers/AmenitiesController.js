var fs = require('fs');
var Amenities = require('../models/Amenities');
var commonHelper = require('../helper/common_helper.js');

var AmenitiesController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            amenities_id: req.body.amenities_id,
            amenities_type: req.body.amenities_type,
            amenities_count: req.body.amenities_count,
            amenities_clean_duartion: req.body.amenities_clean_duartion,
            amenities_clean_cost: req.body.amenities_clean_cost,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Amenities.countAmenities(searchParams).then(function (result) {
            total = result[0].total;
            Amenities.getAmenities(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "amenities": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var amenities_id = req.body.amenities_id;

        if (typeof amenities_id !== 'undefined' && amenities_id !== '') {
            Amenities.singleAmenities(amenities_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "amenities": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var total = 0;
        var currentDate = commonHelper.getCurrentDateTime();
        var amenitiesData = {
            amenities_type: req.body.amenities_type,
            amenities_count: req.body.amenities_count,
            amenities_clean_duartion: req.body.amenities_clean_duartion,
            // amenities_clean_cost: req.body.amenities_clean_cost,
            amenities_clean_cost: '0',
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        var checkData = {
            amenities_type: req.body.amenities_type,
            amenities_count: req.body.amenities_count
        };

        Amenities.checkAmenities(checkData).then(function (results) {
            total = results.length;
            if(total <= 0){
                Amenities.addAmenities(amenitiesData).then(function (result) {
                    var response = { "status": 1, "message": "Amenities created successfully!" };
                    res.json(response);
                }).catch(function (error) {
                    var response = { "status": 0, "message": "Amenities create failed!" };
                    res.json(response);
                });
            }else{
                var response = { "status": 0, "message": "Amenities count already exist.!" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "Amenities create failed!" };
            res.json(response);
        });

    },

    actionUpdate: function (req, res, next) {
        var total = 0;
        var amenities_id = req.body.amenities_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var amenitiesData = {
            amenities_type: req.body.amenities_type,
            amenities_count: req.body.amenities_count,
            amenities_clean_duartion: req.body.amenities_clean_duartion,
            // amenities_clean_cost: req.body.amenities_clean_cost,
            amenities_clean_cost: '0',
            status: req.body.status,
            updated_at: currentDate
        };

        var checkData = {
            amenities_type: req.body.amenities_type,
            amenities_count: req.body.amenities_count
        };

        Amenities.checkAmenities(checkData).then(function (results) {
            total = results.length;
            if(total == 0){
                Amenities.updateAmenities(amenitiesData, amenities_id).then(function (result) {
                    var response = { "status": 1, "message": "Amenities updated successfully!" };
                    res.json(response);
                }).catch(function (error) {
                    var response = { "status": 0, "message": "Amenities update failed!" };
                    res.json(response);
                });
            }else{
               if(amenities_id == results[0].amenities_id){
                    Amenities.updateAmenities(amenitiesData, amenities_id).then(function (result) {
                        var response = { "status": 1, "message": "Amenities updated successfully!" };
                        res.json(response);
                    }).catch(function (error) {
                        var response = { "status": 0, "message": "Amenities update failed!" };
                        res.json(response);
                    });  
                }else{
                    if(total <= 0){
                        Amenities.updateAmenities(amenitiesData, amenities_id).then(function (result) {
                            var response = { "status": 1, "message": "Amenities updated successfully!" };
                            res.json(response);
                        }).catch(function (error) {
                            var response = { "status": 0, "message": "Amenities update failed!" };
                            res.json(response);
                        });
                    }else{
                        var response = { "status": 0, "message": "Amenities count already exist.!" };
                        res.json(response);
                    }    
                } 
            }           
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "Amenities update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Amenities!" };
        var amenities_id = req.body.amenities_id;

        Amenities.deleteAmenities(amenities_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Amenities has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var amenities_id = req.body.amenities_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Amenities.statusAmenities(data, amenities_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Amenities Approved" };                
            }else{
                var response = { "status": 1, "message": "Amenities Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = AmenitiesController;