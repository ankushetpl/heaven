var fs = require('fs');
var Email = require('../models/Email');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var EmailController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            email_id: req.body.email_id,
            email_type: req.body.email_type,
            default: req.body.default,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Email.countEmail(searchParams).then(function (result) {
            total = result[0].total;
            Email.getEmails(limit, offset, searchParams, orderParams).then(function (results) {
                var response = { "status": 1, "message": "Record Found", "email": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionEmailtypes: function (req, res, next) {
        
        Email.getEmailtypes().then(function (result) {
            var response = { "status": 1, "message": "email types", "emailtypes" : result };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
    }, 

    actionView: function (req, res, next) {
        var email_id = req.body.email_id;

        if (typeof email_id !== 'undefined' && email_id !== '') {
            Email.singleEmail(email_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "email": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var emailData = {
            email_type: req.body.email_type,
            email_content: req.body.email_content,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };
        
        Email.addEmail(emailData).then(function (result) {
            var response = { "status": 1, "message": "Email content created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to create" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var email_id = req.body.email_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var emailData = {
            email_type: req.body.email_type,
            email_content: req.body.email_content,
            status: req.body.status,
            updated_at: currentDate
        };
        
        Email.updateEmail(emailData, email_id).then(function (result) {
            var response = { "status": 1, "message": "Email content updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to update" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Email content!" };
        var email_id = req.body.email_id;

        Email.deleteEmail(email_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Email content record has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var email_id = req.body.email_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Email.statusEmail(data, email_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Email approved" };                
            }else{
                var response = { "status": 1, "message": "Email disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionDefault: function(req, res, next) {
        var email_id = req.body.email_id;
        var Default = req.body.default;
        var emailType = req.body.email_type;
        var currentDate = commonHelper.getCurrentDateTime();
        var emailData = { defaults: req.body.default };
        Email.defaultEmail(emailData, email_id, emailType).then(function(result) {

            if(Default == 1){
                var response = { "status": 1, "message": "Email set as default" };                
            }else{
                var response = { "status": 1, "message": "Remove email from default" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
           
    }
    
}

module.exports = EmailController;