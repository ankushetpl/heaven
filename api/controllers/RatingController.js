var fs = require('fs');
var Rating = require('../models/Rating');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var RatingController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            rating_id: req.body.rating_id,
            rating_type: req.body.rating_type,
            rating_name: req.body.rating_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        Rating.countRating(searchParams).then(function (result) {
            total = result[0].total;
            Rating.getRating(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "rating": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var rating_id = req.body.rating_id;

        if (typeof rating_id !== 'undefined' && rating_id !== '') {
            Rating.singleRating(rating_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "rating": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var ratingData = {
            rating_type: req.body.rating_type,
            rating_name: req.body.rating_name,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Rating.addRating(ratingData).then(function (result) {
            var response = { "status": 1, "message": "Rating created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Rating create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var rating_id = req.body.rating_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var ratingData = {
            rating_type: req.body.rating_type,
            rating_name: req.body.rating_name,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };
        
        Rating.updateRating(ratingData, rating_id).then(function (result) {
            var response = { "status": 1, "message": "Rating updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Rating update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Rating!" };
        var rating_id = req.body.rating_id;

        Rating.deleteRating(rating_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Rating has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var rating_id = req.body.rating_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Rating.statusRating(data, rating_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Rating Approved" };                
            }else{
                var response = { "status": 1, "message": "Rating Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionAllTypes: function (req, res, next) {
        
        Rating.getServices().then(function (result) {
            var response = { "status": 1, "message": "Record Found", "type": result };
            res.json(response);                
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }
}

module.exports = RatingController;