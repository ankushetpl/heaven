var fs = require('fs');
var BuyCredit = require('../models/BuyCredit');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var BuyCreditController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            buyCredit_id: req.body.buyCredit_id,
            buyCredit_points: req.body.buyCredit_points,
            buyCredit_amount: req.body.buyCredit_amount,
            status: req.body.status,
            created_at: req.body.created_at
        };

        BuyCredit.countBuyCredit(searchParams).then(function (result) {
            total = result[0].total;

            BuyCredit.getBuyCredit(limit, offset, searchParams, orderParams).then(function (result) {

                var response = { "status": 1, "message": "Record Found", "buyCredit": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var buyCredit_id = req.body.buyCredit_id;

        if (typeof buyCredit_id !== 'undefined' && buyCredit_id !== '') {
            BuyCredit.singleBuyCredit(buyCredit_id).then(function (result) {

                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "buyCredit": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var buyCreditData = {
            buyCredit_points: req.body.buyCredit_points,
            buyCredit_amount: req.body.buyCredit_amount,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        BuyCredit.addBuyCredit(buyCreditData).then(function (result) {
            var response = { "status": 1, "message": "Credit created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to create!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var buyCredit_id = req.body.buyCredit_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var buyCreditData = {
            buyCredit_points: req.body.buyCredit_points,
            buyCredit_amount: req.body.buyCredit_amount,
            status: req.body.status,
            updated_at: currentDate
        };

        BuyCredit.updateBuyCredit(buyCreditData, buyCredit_id).then(function (result) {
            var response = { "status": 1, "message": "Credit updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to update!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Credit!" };
        var buyCredit_id = req.body.buyCredit_id;

        BuyCredit.deleteBuyCredit(buyCredit_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your credit has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var buyCredit_id = req.body.buyCredit_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        BuyCredit.statusBuyCredit(data, buyCredit_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Credit Approved" };                
            }else{
                var response = { "status": 1, "message": "Credit Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }


}

module.exports = BuyCreditController;