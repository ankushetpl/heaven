var fs = require('fs');
var dateFormat = require('dateformat');
var multer = require('multer');
var WeekService = require('../models/WeekService');
var FileUpload = require('../models/FileUpload');
var Client = require('../models/Client');
var Worker = require('../models/Worker');
var Login = require('../models/Login');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var arraySort = require('array-sort');
var sortBy = require('array-sort-by');
var NodeGeocoder = require('node-geocoder');
var async = require("async");

var site_url = commonHelper.getSiteURL();
var options = {
    provider: 'google',

    // Optional depending on the providers
    httpAdapter: 'https', // Default
    apiKey: 'AIzaSyDTSC3_RAunDwX3gpTAhHaNx-xIuhtFuFE&libraries=placeses,visualization,drawing,geometry,places', // for Mapquest, OpenCage, Google Premier
    formatter: null // 'gpx', 'string', ...
};

var geocoder = NodeGeocoder(options);
var ClientController = {


    //UPDATE WORKERS
    actionUpdate: function(req, res, next) {

        var storage = multer.diskStorage({
            destination: function(req, file, cb) {
                cb(null, __dirname + '/../../assets/uploads/');
            },
            filename: function(req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
            }
        });

        var upload = multer({ storage: storage }).single('file');

        upload(req, res, function(err) {
            var response = {};
            if (err) {
                response = { status: 1, message: err };
            }
            var flag = req.body.flag;
            if (flag == 0) {

                var client_id = req.body.client_id;
                var clientData = { clients_name: req.body.name, clients_phone: req.body.phone, clients_mobile: req.body.mobile, clients_address: req.body.address };

                Client.updateClient(client_id, clientData).then(function(result) {
                    var response = { "status": 1, "message": "Your profile has been updated successfully." };
                    res.json(response);
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to update your profile." };
                    res.json(response);
                });

            } else {

                var fileData = {
                    file_original_name: req.file.originalname,
                    file_name: req.file.filename,
                    file_type: req.file.mimetype,
                    file_size: req.file.size,
                    file_base_path: req.file.path,
                    file_base_url: site_url + '/assets/uploads/' + req.file.filename
                };

                FileUpload.addFileUpload(fileData).then(function(result) {
                    if (result) {
                        var client_id = req.body.client_id;
                        var clientData = { clients_name: req.body.name, clients_phone: req.body.phone, clients_mobile: req.body.mobile, clients_address: req.body.address, clients_image: result };

                        Client.updateClient(client_id, clientData).then(function(result) {
                            var response = { "status": 1, "message": "Your profile has been updated successfully." };
                            res.json(response);
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Failed to update your profile." };
                            res.json(response);
                        });
                    }
                }).catch(function(error) {
                    var response = { status: 0, message: "File add failed!" };
                    res.json(response);
                });

            }


        });
    },

    //VIEW client
    actionView: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {
                Client.getClient(client_id).then(function(results) {

                    if (results.length > 0) {

                        Client.getImages().then(function(resultImage) {

                            if (resultImage.length > 0) {

                                for (var k = 0; k < resultImage.length; k++) {
                                    if (results[0].clients_image == resultImage[k].file_id) {
                                        results[0].clients_image = resultImage[k].file_base_url;
                                    }     
                                }

                                var response = { "status": 1, "message": "Client Data", "clientDetails": results };
                                res.json(response);

                            } else {
                                var response = { "status": 0, "message": "No data found" };
                                res.json(response);
                            }


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },

    // Get Worker List
    actionWorkerList: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.getAllWorkers().then(function(results) {

                    if (results.length > 0) {

                        Client.getImages().then(function(resultImage) {

                            if (resultImage.length > 0) {

                                for (var i = 0; i < results.length; i++) {

                                    for (var k = 0; k < resultImage.length; k++) {
                                        if (results[i].workers_image == resultImage[k].file_id) {
                                            results[i].workers_image = resultImage[k].file_base_url;
                                        }
                                    }
                                }
                                var final = arraySort(results, 'workers_rating');
                                var final1 = sortBy(final, item => `DESC:${item.workers_rating}`);
                                var response = { "status": 1, "message": "Workers Data", "workersDetails": final1 };
                                res.json(response);

                            } else {
                                var response = { "status": 0, "message": "No data found" };
                                res.json(response);
                            }


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // Worker Booking
    actionWorkerBooking: function(req, res, next) {
        req.setMaxListeners(0);
        var currentDate = commonHelper.getCurrentDateTime();
        var booking_image1 = '';
        var booking_image2 = '';
        var booking_image3 = '';
        var booking_image4 = '';
        var booking_image5 = '';
        var imageArray = '';
        var arr2 = [];
        var total = 0;
        var storage = multer.diskStorage({
            destination: function(req, file, cb) {
                cb(null, __dirname + '/../../assets/uploads/');
            },
            filename: function(req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
            }
        });

        var upload = multer({ storage: storage }).array('file', 5);

        upload(req, res, function(err) {
            var response = {};
            if (err) {
                response = { status: 1, message: err };
            }

            var flag = req.body.flag;
            var client_id = req.body.client_id;
            var bookingWeekType = req.body.weektype;
            var worker_id = req.body.worker_id;

            var totalWeek = req.body.totalWeek;
            var dateDataA = req.body.dateData

            // var dateDataA =  '[[ {"date" : "2018-11-19"},{"date" : "2018-11-20"},{"date" : "2018-11-21"} ],[ {"date" : "2018-11-26"},{"date" : "2018-11-27"},{"date" : "2018-11-28"} ]]';
            
            var dateData = JSON.parse(dateDataA);

            async.forEach(dateData,function(value,callback){
                async.forEachOf(value,function(value1,key,callback1){
                    
                    if(key || key == 0) {
                        
                        if(key == 0) {
                            value1.cost = '275';
                            callback1();
                        } else if(key == 1) {
                            value1.cost = '265';
                            callback1();
                        } else
                        if(key == 2) {
                            value1.cost = '255';
                            callback1();
                        } else
                        if(key == 3) {
                            value1.cost = '225';
                            callback1();
                        } else
                        if(key == 4) {
                            value1.cost = '230';
                            callback1();
                        } else
                        if(key == 5) {
                            value1.cost = '250';
                            callback1();
                        } else {
                            callback1();
                        }
                    } else {
                        callback1();
                    }
                   
                },function(err){
                    callback();
                    
                })
            },function(err){
                // var response = { "status": 1, 'data': dateData };
                // res.json(response);
            })
                
                var finalCostArray = [];
                async.forEach(dateData,function(value2,callback2) {

                    async.forEach(value2,function(value3,callback3){

                        finalCostArray.push({'date': value3.date , 'cost': value3.cost});
                        callback3();

                    });

                });
            

            if (flag == 0) {

                if (bookingWeekType == 0) {

                    var register = {
                        client_id: req.body.client_id,
                        worker_id: req.body.worker_id,
                        booking_date: req.body.booking_date,
                        booking_time_from: req.body.booking_time_from,
                        booking_time_to: req.body.booking_time_to,
                        // booking_time_to: req.body.booking_time_to,
                        booking_lat: req.body.lat,
                        booking_long: req.body.long,
                        booking_apartment: req.body.apartment,
                        booking_street: req.body.street,
                        booking_city: req.body.city,
                        booking_suburb: req.body.booking_suburb,
                        booking_work_time: req.body.work_time,
                        booking_service_type: req.body.service_type,
                        booking_bedrooms: req.body.bedrooms,
                        booking_bathrooms: req.body.bathrooms,
                        booking_baskets: req.body.baskets,
                        booking_cleaning_area: req.body.cleaning_area,
                        booking_other_work: req.body.other_work,
                        booking_cost: req.body.cost,
                        booking_custom: req.body.customtask,
                        booking_inst1: req.body.instruction1,
                        booking_inst2: req.body.instruction2,
                        booking_inst3: req.body.instruction3,
                        booking_skills: req.body.skills,
                        booking_age_min: req.body.agemin,
                        booking_age_max: req.body.agemax,
                        booking_once_weekly: req.body.weektype,
                        created_at: currentDate,
                        updated_at: currentDate
                    };

                    var days = req.body.booking_date;
                    var preferred_days = req.body.preferred_days;
                    var nowdate = req.body.nowdate;

                } else {
                    var register = {
                        client_id: req.body.client_id,
                        worker_id: req.body.worker_id,
                        booking_date: req.body.booking_date,
                        booking_time_from: req.body.booking_time_from,
                        booking_time_to: req.body.booking_time_to,
                        // booking_time_to: req.body.booking_time_to,
                        booking_lat: req.body.lat,
                        booking_long: req.body.long,
                        booking_apartment: req.body.apartment,
                        booking_street: req.body.street,
                        booking_city: req.body.city,
                        booking_suburb: req.body.booking_suburb,
                        booking_work_time: req.body.work_time,
                        booking_service_type: req.body.service_type,
                        booking_bedrooms: req.body.bedrooms,
                        booking_bathrooms: req.body.bathrooms,
                        booking_baskets: req.body.baskets,
                        booking_cleaning_area: req.body.cleaning_area,
                        booking_other_work: req.body.other_work,
                        booking_cost: req.body.cost,
                        booking_custom: req.body.customtask,
                        booking_inst1: req.body.instruction1,
                        booking_inst2: req.body.instruction2,
                        booking_inst3: req.body.instruction3,
                        booking_skills: req.body.skills,
                        booking_age_min: req.body.agemin,
                        booking_age_max: req.body.agemax,
                        booking_once_weekly: req.body.weektype,
                        created_at: currentDate,
                        updated_at: currentDate
                    };

                    var days = req.body.booking_date;
                    var preferred_days = req.body.preferred_days;
                    var nowdate = req.body.nowdate;
                }

                Client.checkClientExists(client_id).then(function(resultE) {

                    if (resultE.length == 1) {

                        Client.addBooking(register, bookingWeekType, days, preferred_days, nowdate,finalCostArray).then(function(resultB) {

                            if (bookingWeekType == 0) {

                                Worker.getDeviceToken(worker_id).then(function(resultDT) {

                                    var booking_date = req.body.booking_date;
                                    var bookDate = dateFormat(booking_date, "dd/mm/yyyy");
                                    var deviceToken = resultDT[0].device_token;
                                    var msgtitle = 'New Job';
                                    var msgbody = 'A new job on ' + bookDate + ' has been assigned to you!';

                                    var notSendData = { user_id: worker_id, notification_type: 'New Job', notification_detail_id: resultB, notification_detail_serviceflag: bookingWeekType, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                    Worker.addNotificationData(notSendData).then(function(resultAN) {

                                        Worker.getNotResponse(resultAN).then(function(resultNR) {

                                            Worker.notifyBadgeCount(worker_id).then(function(resultBC) {
                                                var badge = resultBC.length;

                                                Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                    res.json(response);

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                            res.json(response);
                                        });


                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                    res.json(response);
                                });

                            } else {

                                Client.getWeekBookId(resultB).then(function(resultWBI) {
                                    var returnId = resultWBI.bookweekly_id;

                                    Worker.getDeviceToken(worker_id).then(function(resultDT) {

                                        var booking_date = req.body.booking_date;
                                        var bookingdateS = booking_date.split(',');
                                        var startdate = dateFormat(bookingdateS[0], "dd/mm/yyyy");
                                        var enddate = dateFormat(bookingdateS[bookingdateS.length - 1], "dd/mm/yyyy");
                                        var deviceToken = resultDT[0].device_token;
                                        var msgtitle = 'New Job';
                                        var msgbody = 'A new job from ' + startdate + ' to ' + enddate + ' has been assigned to you!';

                                        var notSendData = { user_id: worker_id, notification_type: 'New Job', notification_detail_id: resultB, notification_detail_serviceflag: bookingWeekType, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                Worker.notifyBadgeCount(worker_id).then(function(resultBC) {
                                                    var badge = resultBC.length;

                                                    Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                                        var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                        res.json(response);

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                res.json(response);
                                            });


                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                        res.json(response);
                                    });


                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Your Booking Successful" };
                                    res.json(response);
                                });
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "failed to book" };
                            res.json(response);
                        });
                    } else {
                        var response = { "status": 0, "message": "Client does not exit" };
                        res.json(response);
                    }
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Try again." };
                    res.json(response);
                });

            } else {



                for (var i = 0; i < req.files.length; i++) {
                    var fileData = {
                        file_original_name: req.files[i].originalname,
                        file_name: req.files[i].filename,
                        file_type: req.files[i].mimetype,
                        file_size: req.files[i].size,
                        file_base_path: req.files[i].path,
                        file_base_url: site_url + '/assets/uploads/' + req.files[i].filename
                    };

                    FileUpload.addFileUpload(fileData).then(function(result) {
                        arr2.push(result);
                        total = total + 1;
                        if (total == req.files.length) {

                            if (arr2.length == 1) {
                                booking_image1 = arr2[0];
                            } else if (arr2.length == 2) {
                                booking_image1 = arr2[0];
                                booking_image2 = arr2[1];
                            } else if (arr2.length == 3) {
                                booking_image1 = arr2[0];
                                booking_image2 = arr2[1];
                                booking_image3 = arr2[2];
                            } else if (arr2.length == 4) {
                                booking_image1 = arr2[0];
                                booking_image2 = arr2[1];
                                booking_image3 = arr2[2];
                                booking_image4 = arr2[3];
                            } else if (arr2.length == 5) {
                                booking_image1 = arr2[0];
                                booking_image2 = arr2[1];
                                booking_image3 = arr2[2];
                                booking_image4 = arr2[3];
                                booking_image5 = arr2[4];
                            }

                            if (bookingWeekType == 0) {

                                var register = {
                                    client_id: req.body.client_id,
                                    worker_id: req.body.worker_id,
                                    booking_date: req.body.booking_date,
                                    booking_time_from: req.body.booking_time_from,
                                    booking_time_to: req.body.booking_time_to,
                                    booking_lat: req.body.lat,
                                    booking_long: req.body.long,
                                    booking_apartment: req.body.apartment,
                                    booking_street: req.body.street,
                                    booking_city: req.body.city,
                                    booking_suburb: req.body.booking_suburb,
                                    booking_work_time: req.body.work_time,
                                    booking_service_type: req.body.service_type,
                                    booking_bedrooms: req.body.bedrooms,
                                    booking_bathrooms: req.body.bathrooms,
                                    booking_baskets: req.body.baskets,
                                    booking_cleaning_area: req.body.cleaning_area,
                                    booking_other_work: req.body.other_work,
                                    booking_cost: req.body.cost,
                                    booking_custom: req.body.customtask,
                                    booking_inst1: req.body.instruction1,
                                    booking_inst2: req.body.instruction2,
                                    booking_inst3: req.body.instruction3,
                                    booking_image1: booking_image1,
                                    booking_image2: booking_image2,
                                    booking_image3: booking_image3,
                                    booking_image4: booking_image4,
                                    booking_image5: booking_image5,
                                    booking_skills: req.body.skills,
                                    booking_age_min: req.body.agemin,
                                    booking_age_max: req.body.agemax,
                                    booking_once_weekly: req.body.weektype,
                                    created_at: currentDate,
                                    updated_at: currentDate
                                };

                                var days = 0;
                                var preferred_days = req.body.preferred_days;
                                var nowdate = req.body.nowdate;

                            } else {
                                var register = {
                                    client_id: req.body.client_id,
                                    worker_id: req.body.worker_id,
                                    booking_date: req.body.booking_date,
                                    booking_time_from: req.body.booking_time_from,
                                    booking_time_to: req.body.booking_time_to,
                                    booking_lat: req.body.lat,
                                    booking_long: req.body.long,
                                    booking_apartment: req.body.apartment,
                                    booking_street: req.body.street,
                                    booking_city: req.body.city,
                                    booking_suburb: req.body.booking_suburb,
                                    booking_work_time: req.body.work_time,
                                    booking_service_type: req.body.service_type,
                                    booking_bedrooms: req.body.bedrooms,
                                    booking_bathrooms: req.body.bathrooms,
                                    booking_baskets: req.body.baskets,
                                    booking_cleaning_area: req.body.cleaning_area,
                                    booking_other_work: req.body.other_work,
                                    booking_cost: req.body.cost,
                                    booking_custom: req.body.customtask,
                                    booking_inst1: req.body.instruction1,
                                    booking_inst2: req.body.instruction2,
                                    booking_inst3: req.body.instruction3,
                                    booking_image1: booking_image1,
                                    booking_image2: booking_image2,
                                    booking_image3: booking_image3,
                                    booking_image4: booking_image4,
                                    booking_image5: booking_image5,
                                    booking_skills: req.body.skills,
                                    booking_age_min: req.body.agemin,
                                    booking_age_max: req.body.agemax,
                                    booking_once_weekly: req.body.weektype,
                                    created_at: currentDate,
                                    updated_at: currentDate
                                };

                                var days = req.body.booking_date;
                                var preferred_days = req.body.preferred_days;
                                var nowdate = req.body.nowdate;
                            }

                            Client.checkClientExists(client_id).then(function(resultE) {

                                if (resultE.length == 1) {

                                    Client.addBooking(register, bookingWeekType, days, preferred_days, nowdate,finalCostArray).then(function(resultB) {

                                        if (bookingWeekType == 0) {

                                            Worker.getDeviceToken(worker_id).then(function(resultDT) {

                                                var booking_date = req.body.booking_date;
                                                var bookDate = dateFormat(booking_date, "dd/mm/yyyy");
                                                var deviceToken = resultDT[0].device_token;
                                                var msgtitle = 'New Job';
                                                var msgbody = 'A new job on ' + bookDate + ' has been assigned to you!';

                                                var notSendData = { user_id: worker_id, notification_type: 'New Job', notification_detail_id: resultB, notification_detail_serviceflag: bookingWeekType, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                                Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                    Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                        Worker.notifyBadgeCount(worker_id).then(function(resultBC) {
                                                            var badge = resultBC.length;

                                                            Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                                                var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                                res.json(response);

                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                                res.json(response);
                                                            });

                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                            res.json(response);
                                                        });

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                        res.json(response);
                                                    });


                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': resultB };
                                                res.json(response);
                                            });

                                        } else {

                                            Client.getWeekBookId(resultB).then(function(resultWBI) {
                                                var returnId = resultWBI.bookweekly_id;

                                                Worker.getDeviceToken(worker_id).then(function(resultDT) {

                                                    var booking_date = req.body.booking_date;
                                                    var bookingdateS = booking_date.split(',');
                                                    var startdate = dateFormat(bookingdateS[0], "dd/mm/yyyy");
                                                    var enddate = dateFormat(bookingdateS[bookingdateS.length - 1], "dd/mm/yyyy");
                                                    var deviceToken = resultDT[0].device_token;
                                                    var msgtitle = 'New Job';
                                                    var msgbody = 'A new job from ' + startdate + ' to ' + enddate + ' has been assigned to you!';

                                                    var notSendData = { user_id: worker_id, notification_type: 'New Job', notification_detail_id: resultB, notification_detail_serviceflag: bookingWeekType, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                                    Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                        Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                            Worker.notifyBadgeCount(worker_id).then(function(resultBC) {
                                                                var badge = resultBC.length;

                                                                Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                                    res.json(response);

                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                                    res.json(response);
                                                                });

                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                                res.json(response);
                                                            });

                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                            res.json(response);
                                                        });


                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Your Booking Successful", 'bookingId': returnId };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Your Booking Successful" };
                                                res.json(response);
                                            });
                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "failed to book" };
                                        res.json(response);
                                    });

                                } else {
                                    var response = { "status": 0, "message": "Client does not exit" };
                                    res.json(response);
                                }
                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Try again." };
                                res.json(response);
                            });
                        }

                    }).catch(function(error) {
                        var response = { status: 0, message: "File add failed!" };
                        res.json(response);
                    });
                }

            }

        });

    },


    // Make payment for Booking
    actionbookingPayment: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var coupon_id = req.body.coupon_id;
        var credit = req.body.credit;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.getBookingInfo(booking_id, client_id, serviceflag).then(function(resultBI) {

                    var payBook = { booking_coupon_discount: req.body.coupon_discount, booking_credit: credit, booking_cost: req.body.totalCost, booking_payment_status: '1', client_status: '1' };

                    if (coupon_id != 0 && credit != 0) {

                        var couponData = { client_id: client_id, booking_id: req.body.booking_id, booking_once_weekly: serviceflag, credituse_promodiscount: coupon_id, credituse_creditpoints: credit, created_at: currentDate, updated_at: currentDate };

                        Client.getReferralData(client_id).then(function(resultCP) {
                            var clientWallet = resultCP[0].credit_points;

                            if(clientWallet > credit) {

                                Client.addPromoCoupon(couponData, payBook, booking_id, client_id, serviceflag).then(function(resultP) {
                                
                                    var remainCredit = resultCP[0].credit_points - credit;

                                    Client.updateClientCredit(remainCredit, client_id).then(function(resultCU) {

                                        Worker.getDeviceToken(client_id).then(function(resultDT) {

                                            resultBI.forEachAsync(function(element, index, arr) {
                                                element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                            });

                                            var bookDate = resultBI[0].booking_date;
                                            var deviceToken = resultDT[0].device_token;
                                            var deviceType = resultDT[0].device_type;
                                            var msgtitle = 'Payment';
                                            var msgbody = 'Your booking on ' + bookDate + ' has been successful with credit account';

                                            var notSendData = { user_id: client_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                            Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                    Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                                        var badge = resultBC.length;

                                                        if (deviceType == 2) {

                                                            Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {

                                                                var response = { "status": 1, "message": "Payment successful" };
                                                                res.json(response);


                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Payment successful" };
                                                                res.json(response);
                                                            });

                                                        } else if (deviceType == 3) {

                                                            Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                                                var response = { "status": 1, "message": "Payment successful" };
                                                                res.json(response);


                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Payment successful", "notify": resultNR, "badgeCount": badge };
                                                                res.json(response);
                                                            });

                                                        } else {

                                                            var response = { "status": 0, "message": "client not found" };
                                                            res.json(response);
                                                        }

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Payment successful" };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Payment successful" };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Payment successful" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "failed to update credit" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "failed to add coupon" };
                                    res.json(response);
                                });
                            
                            } else {
                                var response = { "status": 0, "message": "You don't have enough balance for booking, top up your wallet." };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "failed to get credit points" };
                            res.json(response);
                        });

                    } else if (coupon_id != 0) {

                        var couponData = { client_id: client_id, booking_id: req.body.booking_id, booking_once_weekly: serviceflag, credituse_promodiscount: coupon_id, created_at: currentDate, updated_at: currentDate };

                        Client.addPromoCoupon(couponData, payBook, booking_id, client_id, serviceflag).then(function(resultP) {

                            Worker.getDeviceToken(client_id).then(function(resultDT) {

                                resultBI.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                });

                                var bookDate = resultBI[0].booking_date;
                                var deviceToken = resultDT[0].device_token;
                                var deviceType = resultDT[0].device_type;
                                var msgtitle = 'Payment';
                                var msgbody = 'Your booking on ' + bookDate + ' has been successful with credit account';

                                var notSendData = { user_id: client_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                Worker.addNotificationData(notSendData).then(function(resultAN) {

                                    Worker.getNotResponse(resultAN).then(function(resultNR) {

                                        Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                            var badge = resultBC.length;

                                            if (deviceType == 2) {

                                                Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {

                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);


                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });

                                            } else if (deviceType == 3) {

                                                Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);


                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful", "notify": resultNR, "badgeCount": badge };
                                                    res.json(response);
                                                });

                                            } else {

                                                var response = { "status": 0, "message": "client not found" };
                                                res.json(response);
                                            }

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Payment successful" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Payment successful" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Payment successful" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 1, "message": "Payment successful" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "failed to add coupon" };
                            res.json(response);
                        });

                    } else {

                        var couponData = { client_id: client_id, booking_id: req.body.booking_id, booking_once_weekly: serviceflag, credituse_creditpoints: credit, created_at: currentDate, updated_at: currentDate };

                        Client.getReferralData(client_id).then(function(resultCP) {
                            var clientWallet = resultCP[0].credit_points;

                                if(clientWallet > credit) {

                                    Client.addPromoCoupon(couponData, payBook, booking_id, client_id, serviceflag).then(function(resultP) {

                                        var remainCredit = resultCP[0].credit_points - credit;

                                        Client.updateClientCredit(remainCredit, client_id).then(function(resultCU) {

                                            Worker.getDeviceToken(client_id).then(function(resultDT) {

                                                resultBI.forEachAsync(function(element, index, arr) {
                                                    element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                                });

                                                var bookDate = resultBI[0].booking_date;
                                                var deviceToken = resultDT[0].device_token;
                                                var deviceType = resultDT[0].device_type;
                                                var msgtitle = 'Payment';
                                                var msgbody = 'Your booking on ' + bookDate + ' has been successful with credit account';

                                                var notSendData = { user_id: client_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                                Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                    Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                        Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                                            var badge = resultBC.length;

                                                            if (deviceType == 2) {

                                                                Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {

                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                    res.json(response);


                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                    res.json(response);
                                                                });

                                                            } else if (deviceType == 3) {

                                                                Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                                                    var response = { "status": 1, "message": "Payment successful" };
                                                                    res.json(response);


                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": "Payment successful", "notify": resultNR, "badgeCount": badge };
                                                                    res.json(response);
                                                                });

                                                            } else {

                                                                var response = { "status": 0, "message": "client not found" };
                                                                res.json(response);
                                                            }

                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Payment successful" };
                                                            res.json(response);
                                                        });

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Payment successful" };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Payment successful" };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "failed to update credit" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "failed to add coupon" };
                                        res.json(response);
                                    });

                                } else {
                                    var response = { "status": 0, "message": "You don't have enough balance for booking, top up your wallet." };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "failed to get credit points" };
                                res.json(response);
                            });

                    }


                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to get booking" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Check payment for Booking after Paygate payment
    actioncheckPaygate: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var reference_id = req.body.reference_id;


        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.checkPaymentStatus(client_id, reference_id).then(function(resultCP) {

                    if (resultCP.length > 0) {

                        if (resultCP[0].payment_type == 0) {

                            var booking_id = resultCP[0].booking_id;
                            var serviceflag = resultCP[0].booking_serviceflag;

                            Client.getBookingInfo(booking_id, client_id, serviceflag).then(function(resultBI) {

                                if (resultBI[0].booking_payment_status == 1) {

                                    Worker.getDeviceToken(client_id).then(function(resultDT) {

                                        resultBI.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                        });

                                        var bookDate = resultBI[0].booking_date;
                                        var deviceToken = resultDT[0].device_token;
                                        var deviceType = resultDT[0].device_type;
                                        var msgtitle = 'Payment';
                                        var msgbody = 'Your booking on ' + bookDate + ' has been successful';

                                        var notSendData = { user_id: client_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                                    var badge = resultBC.length;

                                                    if (deviceType == 2) {

                                                        Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {
                                                            // console.log(resultCSN);                          
                                                            var response = { "status": 1, "message": "Payment successful" };
                                                            res.json(response);


                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Payment successful" };
                                                            res.json(response);
                                                        });

                                                    } else if (deviceType == 3) {

                                                        Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                                            var response = { "status": 1, "message": "Payment successful" };
                                                            res.json(response);


                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Payment successful" };
                                                            res.json(response);
                                                        });

                                                    } else {

                                                        var response = { "status": 0, "message": "client not found" };
                                                        res.json(response);
                                                    }

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Payment successful" };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Payment successful" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Payment successful" };
                                        res.json(response);
                                    });

                                } else {

                                    var response = { "status": 0, "message": "Payment is pending" };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Failed to get booking" };
                                res.json(response);
                            });

                        } else {

                            Worker.getDeviceToken(client_id).then(function(resultDT) {

                                resultBI.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                });

                                var deviceToken = resultDT[0].device_token;
                                var deviceType = resultDT[0].device_type;
                                var msgtitle = 'Payment';
                                var msgbody = 'Your payment for credit purchase has been successful';

                                var notSendData = { user_id: client_id, notification_type: msgtitle, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                Worker.addNotificationData(notSendData).then(function(resultAN) {

                                    Worker.getNotResponse(resultAN).then(function(resultNR) {

                                        Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                            var badge = resultBC.length;

                                            if (deviceType == 2) {

                                                Client.sendNotificationClient(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSN) {
                                                    // console.log(resultCSN);                          
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);


                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });

                                            } else if (deviceType == 3) {

                                                Client.sendIOSNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultCSNI) {

                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);


                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Payment successful" };
                                                    res.json(response);
                                                });

                                            } else {

                                                var response = { "status": 0, "message": "client not found" };
                                                res.json(response);
                                            }

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Payment successful" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Payment successful" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Payment successful" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 1, "message": "Payment successful" };
                                res.json(response);
                            });

                        }

                    } else {

                        var response = { "status": 0, "message": "Transaction failed" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to get booking" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Worker Booking Update worker id after booking data submitted
    actionbookServiceWorker: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var data = { worker_id: req.body.worker_id };

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.updateBookWorker(data, booking_id, client_id, serviceflag).then(function(result) {
                    var response = { "status": 1, "message": "update worker for service successfully" };
                    res.json(response);
                }).catch(function(error) {
                    var response = { "status": 0, "message": "failed to update" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },



    // update worker for booking
    bookServiceWorker: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var data = { worker_id: req.body.worker_id };


        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {
                Client.updateBookWorker(data, booking_id, client_id).then(function(results) {
                    var response = { "status": 1, "message": "Worker assign for service" };
                    res.json(response);
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to assign" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Add Worker as Favourite
    actionAddFavourite: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var worker_id = req.body.worker_id;
        var flag = req.body.flag;
        var addFav = { favorite_client_id: client_id, favorite_worker_id: worker_id, status: flag, created_at: currentDate, updated_at: currentDate };

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {
                Client.addFavourite(addFav, flag, client_id, worker_id).then(function(results) {
                    if (flag == 0) {
                        var response = { "status": 1, "message": "Worker added in favourite list successfully" };
                        res.json(response);
                    } else {
                        var response = { "status": 1, "message": "Remove worker from favourite list" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to add" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // Show all Favourite Workers 
    actionFavWorkers: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {
                Client.favouriteWorkers(client_id).then(function(results) {

                    if (results.length > 0) {

                        Client.getImages().then(function(resultImage) {

                            if (resultImage.length > 0) {

                                for (var i = 0; i < results.length; i++) {

                                    for (var k = 0; k < resultImage.length; k++) {
                                        if (results[i].workers_image == resultImage[k].file_id) {
                                            results[i].workers_image = resultImage[k].file_base_url;
                                        }
                                    }
                                }
                                var final = arraySort(results, 'workers_rating');
                                var final1 = sortBy(final, item => `DESC:${item.workers_rating}`);

                                var response = { "status": 1, "message": "Favourite Workers", "favWorkers": final1 };
                                res.json(response);

                            } else {
                                var response = { "status": 0, "message": "No data found" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not Found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not Found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // History Jobs 
    actionHistory: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        var time = commonHelper.getCurrentTime();
        var client_id = req.body.client_id;
        var flag = req.body.flag;
        var total = 0;
        var totals = 0;
        var totalS = 0;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                if (flag == 0) { // past jobs

                    Client.getHistoryJobs(client_id, flag, today).then(function(resultsOP) {

                        if (resultsOP.length > 0) {

                            Client.getHistoryJobsPastWeekly(client_id, today).then(function(resultsWP) {

                                if (resultsWP.length > 0) {

                                    var results = resultsOP.concat(resultsWP);

                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        if (element.booking_date == today && element.booking_start_time != '00:00:00' && element.booking_end_time == '00:00:00') {
                                            element.startJobFlag = 1;
                                        }
                                    });

                                    Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                        if (resultS.length > 0) {

                                            Client.getImages().then(function(resultImage) {

                                                if (resultImage.length > 0) {

                                                    for (var i = 0; i < results.length; i++) {

                                                        for (var j = 0; j < resultS.length; j++) {
                                                            if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                                results[i].flagFavWorker = 1;
                                                            }
                                                            if (results[i].booking_worker_rating != '0') {
                                                                results[i].rateFlag = '1';
                                                            }
                                                        }

                                                        for (var k = 0; k < resultImage.length; k++) {
                                                            if (results[i].clients_image == resultImage[k].file_id) {
                                                                results[i].clients_image = resultImage[k].file_base_url;
                                                            }
                                                            if (results[i].workers_image == resultImage[k].file_id) {
                                                                results[i].workers_image = resultImage[k].file_base_url;
                                                            }
                                                        }
                                                    }

                                                    var final = arraySort(results, 'booking_date', 'booking_time_from');

                                                    var response = { "status": 1, "message": "History Jobs", "history": final };
                                                    res.json(response);

                                                } else {
                                                    var response = { "status": 0, "message": "No data found" };
                                                    res.json(response);
                                                }


                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not Found" };
                                                res.json(response);
                                            });


                                        } else {
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "History Jobs", "history": final };
                                            res.json(response);
                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not Found" };
                                        res.json(response);
                                    });

                                } else {
                                    var results = resultsOP;

                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        if (element.booking_date == today && element.booking_start_time != '00:00:00' && element.booking_end_time == '00:00:00') {
                                            element.startJobFlag = 1;
                                        }
                                    });

                                    Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                        if (resultS.length > 0) {

                                            Client.getImages().then(function(resultImage) {

                                                if (resultImage.length > 0) {

                                                    for (var i = 0; i < results.length; i++) {

                                                        for (var j = 0; j < resultS.length; j++) {
                                                            if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                                results[i].flagFavWorker = 1;
                                                            }
                                                            if (results[i].booking_worker_rating != '0') {
                                                                results[i].rateFlag = '1';
                                                            }
                                                        }

                                                        for (var k = 0; k < resultImage.length; k++) {
                                                            if (results[i].clients_image == resultImage[k].file_id) {
                                                                results[i].clients_image = resultImage[k].file_base_url;
                                                            }
                                                            if (results[i].workers_image == resultImage[k].file_id) {
                                                                results[i].workers_image = resultImage[k].file_base_url;
                                                            }
                                                        }
                                                    }
                                                    var final = arraySort(results, 'booking_date', 'booking_time_from');
                                                    var response = { "status": 1, "message": "History Jobs", "history": final };
                                                    res.json(response);

                                                } else {
                                                    var response = { "status": 0, "message": "No data found" };
                                                    res.json(response);
                                                }


                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not Found" };
                                                res.json(response);
                                            });


                                        } else {
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "History Jobs", "history": final };
                                            res.json(response);
                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not Found" };
                                        res.json(response);
                                    });
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not Found" };
                                res.json(response);
                            });

                        } else {

                            Client.getHistoryJobsPastWeekly(client_id, today).then(function(results) {

                                if (results.length > 0) {

                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        if (element.booking_date == today && element.booking_start_time != '00:00:00' && element.booking_end_time == '00:00:00') {
                                            element.startJobFlag = 1;
                                        }
                                    });

                                    Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                        if (resultS.length > 0) {

                                            Client.getImages().then(function(resultImage) {

                                                if (resultImage.length > 0) {

                                                    for (var i = 0; i < results.length; i++) {

                                                        for (var j = 0; j < resultS.length; j++) {
                                                            if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                                results[i].flagFavWorker = 1;
                                                            }
                                                            if (results[i].booking_worker_rating != '0') {
                                                                results[i].rateFlag = '1';
                                                            }
                                                        }

                                                        for (var k = 0; k < resultImage.length; k++) {
                                                            if (results[i].clients_image == resultImage[k].file_id) {
                                                                results[i].clients_image = resultImage[k].file_base_url;
                                                            }
                                                            if (results[i].workers_image == resultImage[k].file_id) {
                                                                results[i].workers_image = resultImage[k].file_base_url;
                                                            }
                                                        }
                                                    }
                                                    var final = arraySort(results, 'booking_date', 'booking_time_from');
                                                    var response = { "status": 1, "message": "History Jobs", "history": final };
                                                    res.json(response);

                                                } else {
                                                    var response = { "status": 0, "message": "No data found" };
                                                    res.json(response);
                                                }


                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not Found" };
                                                res.json(response);
                                            });


                                        } else {
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "History Jobs", "history": final };
                                            res.json(response);
                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not Found" };
                                        res.json(response);
                                    });

                                } else {

                                    var response = { "status": 0, "message": "No data found" };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not Found" };
                                res.json(response);
                            });
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not Found" };
                        res.json(response);
                    });

                } else { // current jobs

                    Client.getHistoryJobs(client_id, flag, today).then(function(resultsO) {

                        if (resultsO.length > 0) {

                            Client.getHistoryJobsWeekly(client_id, today).then(function(resultsW) {

                                if (resultsW.length > 0) {

                                    var results = resultsO.concat(resultsW);

                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        if (element.booking_date == today && element.booking_start_time != '00:00:00' && element.booking_end_time == '00:00:00') {
                                            element.startJobFlag = 1;
                                        }
                                    });

                                    if (results.length > 0) {

                                        Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                            if (resultS.length > 0) {

                                                Client.getImages().then(function(resultImage) {

                                                    if (resultImage.length > 0) {

                                                        for (var i = 0; i < results.length; i++) {

                                                            for (var j = 0; j < resultS.length; j++) {
                                                                if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                                    results[i].flagFavWorker = 1;
                                                                }
                                                            }

                                                            for (var k = 0; k < resultImage.length; k++) {
                                                                if (results[i].clients_image == resultImage[k].file_id) {
                                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                                }
                                                                if (results[i].workers_image == resultImage[k].file_id) {
                                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                                }
                                                            }
                                                        }
                                                        var final = arraySort(results, 'booking_date', 'booking_time_from');
                                                        var response = { "status": 1, "message": "History Jobs", "history": final };
                                                        res.json(response);

                                                    } else {
                                                        var response = { "status": 0, "message": "No data found" };
                                                        res.json(response);
                                                    }


                                                }).catch(function(error) {
                                                    var response = { "status": 0, "message": "Not Found" };
                                                    res.json(response);
                                                });


                                            } else {
                                                var final = arraySort(results, 'booking_date', 'booking_time_from');
                                                var response = { "status": 1, "message": "History Jobs", "history": final };
                                                res.json(response);
                                            }

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });

                                    } else {
                                        var response = { "status": 0, "message": "Not data found" };
                                        res.json(response);
                                    }

                                } else {

                                    var results = resultsO;
                                    // console.log(results);
                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        if (element.booking_date == today && element.booking_start_time != '00:00:00' && element.booking_end_time == '00:00:00') {
                                            element.startJobFlag = 1;
                                        }
                                    });

                                    Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                        if (resultS.length > 0) {

                                            Client.getImages().then(function(resultImage) {

                                                if (resultImage.length > 0) {

                                                    for (var i = 0; i < results.length; i++) {

                                                        for (var j = 0; j < resultS.length; j++) {
                                                            if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                                results[i].flagFavWorker = 1;
                                                            }
                                                        }

                                                        for (var k = 0; k < resultImage.length; k++) {
                                                            if (results[i].clients_image == resultImage[k].file_id) {
                                                                results[i].clients_image = resultImage[k].file_base_url;
                                                            }
                                                            if (results[i].workers_image == resultImage[k].file_id) {
                                                                results[i].workers_image = resultImage[k].file_base_url;
                                                            }
                                                        }
                                                    }
                                                    var final = arraySort(results, 'booking_date', 'booking_time_from');
                                                    var response = { "status": 1, "message": "History Jobs", "history": final };
                                                    res.json(response);

                                                } else {
                                                    var response = { "status": 0, "message": "No data found" };
                                                    res.json(response);
                                                }


                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not found" };
                                                res.json(response);
                                            });


                                        } else {
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "History Jobs", "history": final };
                                            res.json(response);
                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });
                                }



                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        } else {

                            Client.getHistoryJobsWeekly(client_id, today).then(function(results) {

                                if (results.length > 0) {

                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        if (element.booking_date == today && element.booking_start_time != '00:00:00' && element.booking_end_time == '00:00:00') {
                                            element.startJobFlag = 1;
                                        }
                                    });

                                    Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                        if (resultS.length > 0) {

                                            Client.getImages().then(function(resultImage) {

                                                if (resultImage.length > 0) {

                                                    for (var i = 0; i < results.length; i++) {

                                                        for (var j = 0; j < resultS.length; j++) {
                                                            if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                                results[i].flagFavWorker = 1;
                                                            }
                                                        }

                                                        for (var k = 0; k < resultImage.length; k++) {
                                                            if (results[i].clients_image == resultImage[k].file_id) {
                                                                results[i].clients_image = resultImage[k].file_base_url;
                                                            }
                                                            if (results[i].workers_image == resultImage[k].file_id) {
                                                                results[i].workers_image = resultImage[k].file_base_url;
                                                            }
                                                        }
                                                    }
                                                    var final = arraySort(results, 'booking_date', 'booking_time_from');
                                                    var response = { "status": 1, "message": "History Jobs", "history": final };
                                                    res.json(response);


                                                } else {
                                                    var response = { "status": 0, "message": "No data found" };
                                                    res.json(response);
                                                }


                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not found" };
                                                res.json(response);
                                            });


                                        } else {
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "History Jobs", "history": final };
                                            res.json(response);
                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });

                                } else {
                                    var response = { "status": 0, "message": "Not data found" };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }


                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                }

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // History Jobs list from weekly bookings
    actionHistoryWeekly: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var total = 0;


        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.getHistoryWeeklyDone(client_id, booking_id, today).then(function(results) {

                    if (results.length > 0) {

                        Client.favouriteWorkerClients(client_id).then(function(resultS) {

                            if (resultS.length > 0) {

                                Client.getImages().then(function(resultImage) {

                                    if (resultImage.length > 0) {

                                        for (var i = 0; i < results.length; i++) {

                                            for (var j = 0; j < resultS.length; j++) {
                                                if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                    results[i].flagFavWorker = 1;
                                                }
                                                if (results[i].booking_worker_rating != '0') {
                                                    results[i].rateFlag = '1';
                                                }
                                            }

                                            for (var k = 0; k < resultImage.length; k++) {
                                                if (results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if (results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                        results.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "Weekly Jobs", "weeklylist": results };
                                            res.json(response);
                                        });

                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });


                            } else {
                                results.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                }, function() {
                                    var response = { "status": 1, "message": "History Jobs", "history": results };
                                    res.json(response);
                                });
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });


            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Add Custome Tasklist
    actionAddTasklist: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var addTask = { tasklist_name: req.body.name, tasklist_des: req.body.description, tasklist_client_id: client_id, tasklist_type: 2, created_at: currentDate, updated_at: currentDate };

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {
                Client.addTasklist(addTask).then(function(results) {
                    var response = { "status": 1, "message": "Task added successfully" };
                    res.json(response);
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to add task" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Delete Custome Tasklist by id
    actionDeleteTasklist: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var tasklist_id = req.body.tasklist_id;
        var deleteTask = { is_deleted: 1, updated_at: currentDate };

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {
                Client.deleteTasklist(deleteTask, tasklist_id).then(function(results) {
                    var response = { "status": 1, "message": "Task deleted successfully" };
                    res.json(response);
                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to delete task" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // MY ADDED TASKLIST
    actionMyTasklist: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {
                Client.getMyTasklist(client_id).then(function(results) {

                    if (results.length > 0) {

                        results.forEachAsync(function(element, index, arr) {
                            element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                            element.updated_at = dateFormat(element.updated_at, "yyyy-mm-dd hh:MM:ss");

                        }, function() {
                            var response = { "status": 1, "message": "My Tasklist", "tasklist": results };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // ADMIN ADDED & CLIENT APPROVED TASKLIST
    actionTasklist: function(req, res, next) {
        var client_id = req.body.client_id;
        var service_type = req.body.service_type;
        var total = 0;
        var standardTasklist = [];
        var customeTasklist = [];
        var myTasklist = [];

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getStandardTasklist(service_type).then(function(resultST) {
                    for (var k = 0; k < resultST.length; k++) {

                        standardTasklist.push({ id: resultST[k].tasklist_id, name: resultST[k].tasklist_name, description: resultST[k].tasklist_des, tasklist_duration: resultST[k].tasklist_duration, tasklist_cost: resultST[k].tasklist_cost });
                    }

                    Client.getTasklist().then(function(resultS) {
                        if (resultS.length > 0) {

                            for (var i = 0; i < resultS.length; i++) {

                                if (resultS[i].tasklist_type == '1' && resultS[i].tasklist_client_id == '0') {

                                    customeTasklist.push({ id: resultS[i].tasklist_id, name: resultS[i].tasklist_name, description: resultS[i].tasklist_des, tasklist_duration: resultS[i].tasklist_duration, tasklist_cost: resultS[i].tasklist_cost });
                                }
                                if (resultS[i].tasklist_client_id == client_id && resultS[i].tasklist_type == '2') {

                                    myTasklist.push({ id: resultS[i].tasklist_id, name: resultS[i].tasklist_name, description: resultS[i].tasklist_des, tasklist_duration: resultS[i].tasklist_duration, tasklist_cost: resultS[i].tasklist_cost });
                                }

                            }

                            var response = { "status": 1, "message": "Tasklist data", 'tasklists': { 'Standard': standardTasklist, 'Custome': customeTasklist, 'mytasklist': myTasklist } };
                            res.json(response);

                        } else {
                            var response = { "status": 1, "message": "Tasklist data", 'tasklists': { 'Standard': standardTasklist, 'Custome': [], 'mytasklist': [] } };
                            res.json(response);
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Try again." };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Try again." };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // // Get Upcoming Jobs
    actionupcomingJobs: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        var client_id = req.body.client_id;
        var total = 0;
        var totals = 0;
        var totalS = 0;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.getUpcomingJobs(client_id, today).then(function(resultsO) {

                    if (resultsO.length > 0) {

                        Client.getUpcomingJobsWeekly(client_id, today).then(function(resultsW) {

                            if (resultsW.length > 0) {

                                var results = resultsO.concat(resultsW);

                                Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                    Client.getImages().then(function(resultImage) {

                                        if (resultImage.length > 0) {

                                            for (var i = 0; i < results.length; i++) {

                                                for (var j = 0; j < resultS.length; j++) {
                                                    if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                        results[i].flagFavWorker = 1;
                                                    }
                                                }

                                                for (var k = 0; k < resultImage.length; k++) {
                                                    if (results[i].clients_image == resultImage[k].file_id) {
                                                        results[i].clients_image = resultImage[k].file_base_url;
                                                    }
                                                    if (results[i].workers_image == resultImage[k].file_id) {
                                                        results[i].workers_image = resultImage[k].file_base_url;
                                                    }
                                                }
                                            }
                                            for (var x = 0; x < results.length; x++) {
                                                results[x].booking_date = dateFormat(results[x].booking_date, "yyyy-mm-dd");
                                            }
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "Upcoming Jobs", "history": final };
                                            res.json(response);

                                        } else {
                                            var response = { "status": 0, "message": "No data found" };
                                            res.json(response);
                                        }


                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {

                                var results = resultsO;

                                Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                    Client.getImages().then(function(resultImage) {

                                        if (resultImage.length > 0) {

                                            for (var i = 0; i < results.length; i++) {

                                                for (var j = 0; j < resultS.length; j++) {
                                                    if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                        results[i].flagFavWorker = 1;
                                                    }
                                                }

                                                for (var k = 0; k < resultImage.length; k++) {
                                                    if (results[i].clients_image == resultImage[k].file_id) {
                                                        results[i].clients_image = resultImage[k].file_base_url;
                                                    }
                                                    if (results[i].workers_image == resultImage[k].file_id) {
                                                        results[i].workers_image = resultImage[k].file_base_url;
                                                    }
                                                }
                                            }

                                            for (var x = 0; x < results.length; x++) {
                                                results[x].booking_date = dateFormat(results[x].booking_date, "yyyy-mm-dd");
                                            }
                                            var final = arraySort(results, 'booking_date', 'booking_time_from');
                                            var response = { "status": 1, "message": "Upcoming Jobs", "history": final };
                                            res.json(response);

                                        } else {
                                            var response = { "status": 0, "message": "No data found" };
                                            res.json(response);
                                        }


                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {

                        Client.getUpcomingJobsWeekly(client_id, today).then(function(results) {

                            Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                Client.getImages().then(function(resultImage) {

                                    if (resultImage.length > 0) {

                                        for (var i = 0; i < results.length; i++) {

                                            for (var j = 0; j < resultS.length; j++) {
                                                if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                    results[i].flagFavWorker = 1;
                                                }
                                            }

                                            for (var k = 0; k < resultImage.length; k++) {
                                                if (results[i].clients_image == resultImage[k].file_id) {
                                                    results[i].clients_image = resultImage[k].file_base_url;
                                                }
                                                if (results[i].workers_image == resultImage[k].file_id) {
                                                    results[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                        for (var x = 0; x < results.length; x++) {
                                            results[x].booking_date = dateFormat(results[x].booking_date, "yyyy-mm-dd");
                                        }
                                        var final = arraySort(results, 'booking_date', 'booking_time_from');
                                        var response = { "status": 1, "message": "Upcoming Jobs", "history": final };
                                        res.json(response);

                                    } else {
                                        var response = { "status": 0, "message": "No data found" };
                                        res.json(response);
                                    }


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });


                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Delete Upcoming Job
    actiondeleteUpcoming: function(req, res, next) {

        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var deleteBooking = { is_deleted: 1, updated_at: currentDate };

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {
                Client.deleteBooking(deleteBooking, booking_id, serviceflag).then(function(results) {

                    Client.getBookingInfo(booking_id, client_id, serviceflag).then(function(resultJBD) {
                        var receiver = resultJBD[0].worker_id;

                        Worker.getDeviceToken(receiver).then(function(resultDT) {

                            resultJBD.forEachAsync(function(element, index, arr) {
                                element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                            });

                            var jobDate = resultJBD[0].booking_date;
                            var deviceToken = resultDT[0].device_token;
                            var msgtitle = 'Job Cancel';
                            var msgbody = 'The job on ' + jobDate + ' has been cancelled!';

                            var notSendData = { user_id: receiver, notification_type: 'Job Cancel', notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                            Worker.addNotificationData(notSendData).then(function(resultAN) {

                                Worker.getNotResponse(resultAN).then(function(resultNR) {

                                    Worker.notifyBadgeCount(receiver).then(function(resultBC) {
                                        var badge = resultBC.length;

                                        Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                            var response = { "status": 1, "message": "Booking deleted successfully", 'notify': resultNR, badgeCount: resultBC.length };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Booking deleted successfully" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Booking deleted successfully" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Booking deleted successfully" };
                                    res.json(response);
                                });


                            }).catch(function(error) {
                                var response = { "status": 1, "message": "Booking deleted successfully"  };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 1, "message": "Booking deleted successfully"};
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 1, "message": "Booking deleted successfully"};
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to delete booking" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // CLAIM CATEGORY ADDED BY ADMIN
    actionclaimCategory: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {
                Client.getClaimCategory().then(function(results) {

                    if (results.length > 0) {

                        var response = { "status": 1, "message": "Claim Category List", "claimCategory": results };
                        res.json(response);

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // Worker Booking
    actionclaimWorker: function(req, res, next) {
        req.setMaxListeners(0);
        var currentDate = commonHelper.getCurrentDateTime();
        var claim_image1 = '';
        var claim_image2 = '';
        var claim_image3 = '';
        var claim_image4 = '';
        var claim_image5 = '';
        var imageArray = '';
        var arr2 = [];
        var total = 0;
        var storage = multer.diskStorage({
            destination: function(req, file, cb) {
                cb(null, __dirname + '/../../assets/uploads/');
            },
            filename: function(req, file, cb) {
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
            }
        });

        var upload = multer({ storage: storage }).array('file', 5);

        upload(req, res, function(err) {
            var response = {};
            if (err) {
                response = { status: 1, message: err };
            }

            var flag = req.body.flag;
            var client_id = req.body.client_id;

            if (flag == 0) {

                var addClaimData = { claim_category_id: req.body.category_id, claim_des: req.body.description, claim_client_id: client_id, claim_servicetype: req.body.serviceflag, claim_booking_id: req.body.booking_id, created_at: currentDate, updated_at: currentDate };

                Client.checkClientExists(client_id).then(function(result) {
                    total = result.length;
                    if (total == 1) {

                        Client.checkClaimExists(client_id, req.body.booking_id, req.body.serviceflag).then(function(resultCCE) {

                            if (resultCCE.length == 1) {

                                var response = { "status": 1, "message": "Your claim report already submitted." };
                                res.json(response);

                            } else {

                                Client.addClaim(addClaimData).then(function(results) {
                                    var response = { "status": 1, "message": "Your claim report has been submitted successfully. We will get back to you soon" };
                                    res.json(response);

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Failed to submit claim report" };
                                    res.json(response);
                                });

                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Failed to receive claim report" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "Client does not exit" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    //console.log(error);
                    var response = { "status": 0, "message": "Try again." };
                    res.json(response);
                });

            } else {

                for (var i = 0; i < req.files.length; i++) {
                    // console.log(i);
                    var fileData = {
                        file_original_name: req.files[i].originalname,
                        file_name: req.files[i].filename,
                        file_type: req.files[i].mimetype,
                        file_size: req.files[i].size,
                        file_base_path: req.files[i].path,
                        file_base_url: site_url + '/assets/uploads/' + req.files[i].filename
                    };

                    FileUpload.addFileUpload(fileData).then(function(result) {
                        arr2.push(result);
                        total = total + 1;
                        if (total == req.files.length) {

                            if (arr2.length == 1) {
                                claim_image1 = arr2[0];
                                claim_image2 = '';
                                claim_image3 = '';
                                claim_image4 = '';
                                claim_image5 = '';
                            } else if (arr2.length == 2) {
                                claim_image1 = arr2[0];
                                claim_image2 = arr2[1];
                                claim_image3 = '';
                                claim_image4 = '';
                                claim_image5 = '';
                            } else if (arr2.length == 3) {
                                claim_image1 = arr2[0];
                                claim_image2 = arr2[1];
                                claim_image3 = arr2[2];
                                claim_image4 = '';
                                claim_image5 = '';
                            } else if (arr2.length == 4) {
                                claim_image1 = arr2[0];
                                claim_image2 = arr2[1];
                                claim_image3 = arr2[2];
                                claim_image4 = arr2[3];
                                claim_image5 = '';
                            } else if (arr2.length == 5) {
                                claim_image1 = arr2[0];
                                claim_image2 = arr2[1];
                                claim_image3 = arr2[2];
                                claim_image4 = arr2[3];
                                claim_image5 = arr2[4];
                            }

                            var addClaimData = { claim_category_id: req.body.category_id, claim_des: req.body.description, claim_client_id: client_id, claim_servicetype: req.body.serviceflag, claim_booking_id: req.body.booking_id, claim_image1: claim_image1, claim_image2: claim_image2, claim_image3: claim_image3, claim_image4: claim_image4, claim_image5: claim_image5, created_at: currentDate, updated_at: currentDate };

                            Client.checkClientExists(client_id).then(function(result) {
                                total = result.length;
                                if (total == 1) {

                                    Client.checkClaimExists(client_id, req.body.booking_id, req.body.serviceflag).then(function(resultCCE) {

                                        if (resultCCE.length == 1) {

                                            var response = { "status": 1, "message": "Your claim report already submitted." };
                                            res.json(response);

                                        } else {

                                            Client.addClaim(addClaimData).then(function(results) {
                                                var response = { "status": 1, "message": "Your claim report has been submitted successfully.we will get back to you soon" };
                                                res.json(response);

                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Failed to submit claim report" };
                                                res.json(response);
                                            });

                                        }

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Failed to receive claim report" };
                                        res.json(response);
                                    });

                                } else {
                                    var response = { "status": 0, "message": "Client does not exit" };
                                    res.json(response);
                                }
                            }).catch(function(error) {
                                //console.log(error);
                                var response = { "status": 0, "message": "Try again." };
                                res.json(response);
                            });
                        }

                    }).catch(function(error) {
                        var response = { status: 0, message: "File add failed!" };
                        res.json(response);
                    });
                }

            }
        });

    },

    // TASK TYPE ADDED BY ADMIN
    actiontasktype: function(req, res, next) {
        var client_id = req.body.client_id;
        var total = 0;
        // var countries = require ('countries-cities').getCountries();         
        // var cities = require ('countries-cities').getCities('South Africa'); 

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getAllCitySouthAfrica().then(function(resultA) {

                    Client.getTasktype().then(function(results) {

                        if (results.length > 0) {


                            var response = { "status": 1, "message": "Task Types", "tasktype": results, 'city': resultA };
                            res.json(response);

                        } else {
                            var response = { "status": 0, "message": "No data found" };
                            res.json(response);
                        }
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Try again." };
                    res.json(response);
                });


            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // ALL AMENITIES DATA
    actionamenities: function(req, res, next) {
        var client_id = req.body.client_id;
        var total = 0;
        var Bedrooms = [];
        var Bathrooms = [];
        var Baskets = [];

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {
                Client.getCleaningArea().then(function(results) {

                    if (results.length > 0) {

                        Client.getAmenities().then(function(resultS) {

                            for (var i = 0; i < resultS.length; i++) {

                                if (resultS[i].amenities_type == 'Bedrooms') {

                                    Bedrooms.push({ id: resultS[i].amenities_id, type: resultS[i].amenities_type, count: resultS[i].amenities_count, duration: resultS[i].amenities_clean_duartion, cost: resultS[i].amenities_clean_cost });
                                }
                                if (resultS[i].amenities_type == 'Bathrooms') {

                                    Bathrooms.push({ id: resultS[i].amenities_id, type: resultS[i].amenities_type, count: resultS[i].amenities_count, duration: resultS[i].amenities_clean_duartion, cost: resultS[i].amenities_clean_cost });
                                }
                                if (resultS[i].amenities_type == 'Baskets') {

                                    Baskets.push({ id: resultS[i].amenities_id, type: resultS[i].amenities_type, count: resultS[i].amenities_count, duration: resultS[i].amenities_clean_duartion, cost: resultS[i].amenities_clean_cost });
                                }

                            }
                            // var Bedrooms = arraySort(Bedrooms, 'count');
                            var totals = 0;

                            WeekService.getAllWeekService().then(function(resultW) {
                                totals = resultW.length;

                                resultW.forEachAsync(function(element, index, arr) {
                                    element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                                    element.updated_at = dateFormat(element.updated_at, "yyyy-mm-dd hh:MM:ss");

                                }, function() {
                                    if (totals > 0) {
                                        var response = { "status": 1, "message": "Amenities data", 'cleaningArea': results, 'amenities': { 'Bedrooms': Bedrooms, 'Bathrooms': Bathrooms, 'Baskets': Baskets }, "weekService": resultW };
                                        res.json(response);
                                    } else {
                                        var response = { "status": 1, "message": "Amenities data", 'cleaningArea': '', 'amenities': { 'Bedrooms': '', 'Bathrooms': '', 'Baskets': '' }, "weekService": '' };
                                        res.json(response);

                                    }
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "No record exist" };
                                res.json(response);
                            });


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });


                    } else {

                        Client.getAmenities().then(function(resultS) {

                            for (var i = 0; i < resultS.length; i++) {

                                if (resultS[i].amenities_type == 'Bedrooms') {

                                    Bedrooms.push({ type: resultS[i].amenities_type, count: resultS[i].amenities_count, duration: resultS[i].amenities_clean_duartion, cost: resultS[i].amenities_clean_cost });
                                }
                                if (resultS[i].amenities_type == 'Bathrooms') {

                                    Bathrooms.push({ type: resultS[i].amenities_type, count: resultS[i].amenities_count, duration: resultS[i].amenities_clean_duartion, cost: resultS[i].amenities_clean_cost });
                                }
                                if (resultS[i].amenities_type == 'Baskets') {

                                    Baskets.push({ type: resultS[i].amenities_type, count: resultS[i].amenities_count, duration: resultS[i].amenities_clean_duartion, cost: resultS[i].amenities_clean_cost });
                                }

                            }
                            var response = { "status": 1, "message": "Amenities data", 'cleaningArea': [], 'amenities': { 'Bedrooms': Bedrooms, 'Bathrooms': Bathrooms, 'Baskets': Baskets } };
                            res.json(response);


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // GET ALL BOOKING WITH CLAIMED ALREADY OR NOT
    actionclaimBooking: function(req, res, next) {
        var today = commonHelper.getCurrentDate();
        var client_id = req.body.client_id;
        var total = 0;
        var totals = 0;
        var totalS = 0;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.getAllBooking(client_id, today).then(function(resultsO) {

                    if (resultsO.length > 0) {

                        Client.getAllWeeklyBooking(client_id, today).then(function(resultsW) {

                            if (resultsW.length > 0) {

                                var results = resultsO.concat(resultsW);

                                Client.claimClientBooking(client_id).then(function(resultS) {

                                    if (resultS.length > 0) {

                                        Client.getImages().then(function(resultImage) {

                                            if (resultImage.length > 0) {

                                                for (var i = 0; i < results.length; i++) {

                                                    for (var j = 0; j < resultS.length; j++) {
                                                        if (results[i].booking_id == resultS[j].claim_booking_id) {
                                                            results[i].claimflag = 1;
                                                        }
                                                    }

                                                    for (var k = 0; k < resultImage.length; k++) {
                                                        if (results[i].clients_image == resultImage[k].file_id) {
                                                            results[i].clients_image = resultImage[k].file_base_url;
                                                        }
                                                        if (results[i].workers_image == resultImage[k].file_id) {
                                                            results[i].workers_image = resultImage[k].file_base_url;
                                                        }
                                                    }
                                                }

                                                results.forEachAsync(function(element, index, arr) {
                                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                                }, function() {
                                                    var response = { "status": 1, "message": "Booking List", "bookingList": results };
                                                    res.json(response);
                                                });


                                            } else {
                                                var response = { "status": 0, "message": "No data found" };
                                                res.json(response);
                                            }


                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });
                                    } else {

                                        var response = { "status": 1, "message": "Booking List", "bookingList": results };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {

                                var results = resultsO;

                                Client.claimClientBooking(client_id).then(function(resultS) {

                                    if (resultS.length > 0) {

                                        Client.getImages().then(function(resultImage) {

                                            if (resultImage.length > 0) {

                                                for (var i = 0; i < results.length; i++) {

                                                    for (var j = 0; j < resultS.length; j++) {
                                                        if (results[i].booking_id == resultS[j].claim_booking_id) {
                                                            results[i].claimflag = 1;
                                                        }
                                                    }

                                                    for (var k = 0; k < resultImage.length; k++) {
                                                        if (results[i].clients_image == resultImage[k].file_id) {
                                                            results[i].clients_image = resultImage[k].file_base_url;
                                                        }
                                                        if (results[i].workers_image == resultImage[k].file_id) {
                                                            results[i].workers_image = resultImage[k].file_base_url;
                                                        }
                                                    }
                                                }

                                                results.forEachAsync(function(element, index, arr) {
                                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                                }, function() {
                                                    var response = { "status": 1, "message": "Booking List", "bookingList": results };
                                                    res.json(response);
                                                });


                                            } else {
                                                var response = { "status": 0, "message": "No data found" };
                                                res.json(response);
                                            }


                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });
                                    } else {

                                        var response = { "status": 1, "message": "Booking List", "bookingList": results };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found " };
                            res.json(response);
                        });

                    } else {

                        Client.getAllWeeklyBooking(client_id, today).then(function(results) {

                            Client.claimClientBooking(client_id).then(function(resultS) {

                                if (resultS.length > 0) {

                                    Client.getImages().then(function(resultImage) {

                                        if (resultImage.length > 0) {

                                            for (var i = 0; i < results.length; i++) {

                                                for (var j = 0; j < resultS.length; j++) {
                                                    if (results[i].booking_id == resultS[j].claim_booking_id) {
                                                        results[i].claimflag = 1;
                                                    }
                                                }

                                                for (var k = 0; k < resultImage.length; k++) {
                                                    if (results[i].clients_image == resultImage[k].file_id) {
                                                        results[i].clients_image = resultImage[k].file_base_url;
                                                    }
                                                    if (results[i].workers_image == resultImage[k].file_id) {
                                                        results[i].workers_image = resultImage[k].file_base_url;
                                                    }
                                                }
                                            }

                                            results.forEachAsync(function(element, index, arr) {
                                                element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                            }, function() {
                                                var response = { "status": 1, "message": "Booking List", "bookingList": results };
                                                res.json(response);
                                            });


                                        } else {
                                            var response = { "status": 0, "message": "No data found" };
                                            res.json(response);
                                        }


                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });
                                } else {

                                    var response = { "status": 1, "message": "Booking List", "bookingList": results };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found " };
                            res.json(response);
                        });

                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found " };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Worker does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // GET ALL SKILLS LIST
    actionworkerSkills: function(req, res, next) {
        var client_id = req.body.client_id;
        var total = 0;
        var childskills = [];
        var totals = 0;

        Client.checkClientExists(client_id).then(function(result) {
            if (result.length == 1) {

                Client.getMainSkills().then(function(resultS) {
                    if (resultS.length > 0) {

                        var response = { "status": 1, "message": "Skills list", 'mainskills': resultS };
                        res.json(response);

                    } else {
                        var response = { "status": 1, "message": "No list found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // GET ALL SKILLS LIST
    actionworkerSkillsNew: function(req, res, next) {
        var client_id = req.body.client_id;
        var worker_id = req.body.worker_id;
        var total = 0;
        var childskills = [];
        var totals = 0;

        Client.checkClientExists(client_id).then(function(result) {
            if (result.length == 1) {

                if (worker_id == 0) {

                    Client.getMainSkills().then(function(resultS) {
                        if (resultS.length > 0) {

                            var response = { "status": 1, "message": "Skills list", 'mainskills': resultS };
                            res.json(response);

                        } else {
                            var response = { "status": 1, "message": "No list found" };
                            res.json(response);
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                } else {

                    Client.getMainSkills().then(function(resultS) {

                        Client.getWorkerSkillsAndAge(worker_id).then(function(resultWS) {

                            if (resultWS.length > 0) {

                                Client.getWorkerSkillsForFavWorker(resultWS[0].workers_skills).then(function(resultSDATA) {

                                    var response = { "status": 1, "message": "Skills list", 'mainskills': resultS, 'workerSkills': resultSDATA, 'age': resultWS[0].workers_age };
                                    res.json(response);

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {
                                var response = { "status": 1, "message": "No list found" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                }

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // add and update instructions
    actioninstruction: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var flag = req.body.flag; // 0 = add instruction 1 = update instruction
        var addInstru = { client_id: client_id, instruction1: req.body.instruction1, instruction2: req.body.instruction2, instruction3: req.body.instruction3 };

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.instruction(addInstru, flag).then(function(result) {
                    if (flag == 0) {
                        var response = { "status": 1, "message": "Add instructions successfully" };
                        res.json(response);
                    } else {
                        var response = { "status": 1, "message": "Update instructions successfully" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "failed to update" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // get instructions data
    actiongetInstruction: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var total = 0;
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {

                Client.getInstruction(client_id).then(function(result) {

                    var response = { "status": 1, "message": "Instructions data", 'instruction': result };
                    res.json(response);

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No data found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // get promo data
    actionpromos: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;

        Client.checkClientExists(client_id).then(function(results) {

            if (results.length == 1) {

                if (booking_id == 0) {

                    Client.getAllPromos(currentDate).then(function(resultALL) {
                        

                        if (resultALL.length > 0) {

                            Client.getOffersUsed(client_id).then(function(resultFD) {

                                Client.getImages().then(function(resultImage) {

                                    for (var i = 0; i < resultALL.length; i++) {

                                        for (var j = 0; j < resultImage.length; j++) {

                                            if (resultALL[i].promo_banner == resultImage[j].file_id) {
                                                resultALL[i].promo_banner = resultImage[j].file_base_url;
                                            }
                                        }
                                        for (var m = 0; m < resultFD.length; m++) {

                                            if (resultALL[i].promo_id == resultFD[m].credituse_promodiscount) {
                                                resultALL.splice(i, 1);
                                            }
                                        }
                                    }

                                    resultALL.forEachAsync(function(element, index, arr) {
                                        element.promo_startdate = dateFormat(element.promo_startdate, "yyyy-mm-dd");
                                        element.promo_enddate = dateFormat(element.promo_enddate, "yyyy-mm-dd");

                                    }, function() {
                                        var response = { "status": 1, "message": "Promo data", 'promo': resultALL };
                                        res.json(response);
                                    });

                                }).catch(function(error) {

                                    resultALL.forEachAsync(function(element, index, arr) {
                                        element.promo_startdate = dateFormat(element.promo_startdate, "yyyy-mm-dd");
                                        element.promo_enddate = dateFormat(element.promo_enddate, "yyyy-mm-dd");

                                    }, function() {
                                        var response = { "status": 1, "message": "Promo data", 'promo': resultALL };
                                        res.json(response);
                                    });
                                });

                            }).catch(function(error) {

                                resultALL.forEachAsync(function(element, index, arr) {
                                    element.promo_startdate = dateFormat(element.promo_startdate, "yyyy-mm-dd");
                                    element.promo_enddate = dateFormat(element.promo_enddate, "yyyy-mm-dd");

                                }, function() {
                                    var response = { "status": 1, "message": "Promo data", 'promo': resultALL };
                                    res.json(response);
                                });
                            });

                        } else {
                            var response = { "status": 0, "message": "No offers available for you" };
                            res.json(response);
                        }


                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No offers available for you" };
                        res.json(response);
                    });

                } else {

                    Client.getBookingInfo(booking_id, client_id).then(function(resultBI) {

                        var weektype = resultBI[0].booking_once_weekly;

                        Client.getpromos(currentDate, weektype).then(function(result) {
                            
                            if (result.length > 0) {

                                Client.getOffersUsed(client_id).then(function(resultFD) {

                                    Client.getImages().then(function(resultImage) {

                                        for (var i = 0; i < result.length; i++) {

                                            for (var j = 0; j < resultImage.length; j++) {

                                                if (result[i].promo_banner == resultImage[j].file_id) {
                                                    result[i].promo_banner = resultImage[j].file_base_url;
                                                }
                                            }
                                            for (var m = 0; m < resultFD.length; m++) {

                                                if (result[i].promo_id == resultFD[m].credituse_promodiscount) {
                                                    result.splice(i, 1);
                                                }
                                            }
                                        }

                                        result.forEachAsync(function(element, index, arr) {
                                            element.promo_startdate = dateFormat(element.promo_startdate, "yyyy-mm-dd");
                                            element.promo_enddate = dateFormat(element.promo_enddate, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "Promo data", 'promo': result };
                                            res.json(response);
                                        });


                                    }).catch(function(error) {

                                        result.forEachAsync(function(element, index, arr) {
                                            element.promo_startdate = dateFormat(element.promo_startdate, "yyyy-mm-dd");
                                            element.promo_enddate = dateFormat(element.promo_enddate, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "Promo data", 'promo': result };
                                            res.json(response);
                                        });
                                    });

                                }).catch(function(error) {

                                    result.forEachAsync(function(element, index, arr) {
                                        element.promo_startdate = dateFormat(element.promo_startdate, "yyyy-mm-dd");
                                        element.promo_enddate = dateFormat(element.promo_enddate, "yyyy-mm-dd");

                                    }, function() {
                                        var response = { "status": 1, "message": "Promo data", 'promo': result };
                                        res.json(response);
                                    });
                                });

                            } else {
                                var response = { "status": 0, "message": "No offers available for you" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "No offers available for you" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                }

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // JOB DETAILS
    actionjobDetails: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var total = 0;
        var countData = 0;
        var bedroom = [];
        var bathroom = [];
        var basket = [];
        var cleaningArea = [];
        var skills = [];
        var CustomTask = [];
        var StandardTask = [];

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (total == 1) {
                Client.getJobDetails(booking_id, serviceflag).then(function(results) {

                    results.forEachAsync(function(element, index, arr) {
                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                    });

                    if (results.length > 0) {

                        Client.getImages().then(function(resultImage) {

                            if (resultImage.length > 0) {

                                for (var p = 0; p < resultImage.length; p++) {

                                    if (results[0].booking_image1 == resultImage[p].file_id) {
                                        results[0].booking_image1 = resultImage[p].file_base_url;
                                    }
                                    if (results[0].booking_image2 == resultImage[p].file_id) {
                                        results[0].booking_image2 = resultImage[p].file_base_url;
                                    }
                                    if (results[0].booking_image3 == resultImage[p].file_id) {
                                        results[0].booking_image3 = resultImage[p].file_base_url;
                                    }
                                    if (results[0].booking_image4 == resultImage[p].file_id) {
                                        results[0].booking_image4 = resultImage[p].file_base_url;
                                    }
                                    if (results[0].booking_image5 == resultImage[p].file_id) {
                                        results[0].booking_image5 = resultImage[p].file_base_url;
                                    }
                                    if (results[0].clients_image == resultImage[p].file_id) {
                                        results[0].clients_image = resultImage[p].file_base_url;
                                    }
                                    if (results[0].workers_image == resultImage[p].file_id) {
                                        results[0].workers_image = resultImage[p].file_base_url;
                                    }

                                }

                                Client.getStandardTask().then(function(resultSTask) {

                                    if (resultSTask.length > 0) {

                                        Client.getSkillsData().then(function(resultSD) {

                                            if (resultSD.length > 0 && results[0].booking_skills != '') {

                                                var arrayskill = results[0].booking_skills.split(',');

                                                for (var k = 0; k < resultSD.length; k++) {

                                                    if (arrayskill[k] == resultSD[k].skills_id) {

                                                        skills.push({ skills_name: resultSD[k].skills_name });
                                                    }
                                                }

                                                Client.getAmenitiesData().then(function(resultA) {

                                                    if (resultA.length > 0) {

                                                        for (var i = 0; i < resultA.length; i++) {
                                                            if (results[0].booking_bedrooms != 0) {
                                                                if (results[0].booking_bedrooms == resultA[i].amenities_id) {
                                                                    bedroom.push({ bedroom_count: resultA[i].amenities_count, bedroom_clean_duartion: resultA[i].amenities_clean_duartion });
                                                                }
                                                            }
                                                            if (results[0].booking_bathrooms != 0) {
                                                                if (results[0].booking_bathrooms == resultA[i].amenities_id) {
                                                                    bathroom.push({ bathroom_count: resultA[i].amenities_count, bathroom_clean_duartion: resultA[i].amenities_clean_duartion });
                                                                }
                                                            }
                                                            if (results[0].booking_baskets != 0) {
                                                                if (results[0].booking_baskets == resultA[i].amenities_id) {
                                                                    basket.push({ basket_count: resultA[i].amenities_count, basket_clean_duartion: resultA[i].amenities_clean_duartion });
                                                                }
                                                            }
                                                        }

                                                        Client.getCleaningData().then(function(resultC) {


                                                            if (resultC.length > 0 && results[0].booking_cleaning_area != '') {

                                                                var array = results[0].booking_cleaning_area.split(',');
                                                                for (var j = 0; j < resultC.length; j++) {

                                                                    if (array[j] == resultC[j].area_id) {

                                                                        cleaningArea.push({ cleaning_area_name: resultC[j].area_name, cleaning_area_duration: resultC[j].area_clean_duration });
                                                                    }
                                                                }

                                                                Client.getCustomTask().then(function(resultCT) {
                                                                    if (resultCT.length > 0 && results[0].booking_custom != '') {

                                                                        var arrayCustom = results[0].booking_custom.split(',');
                                                                        for (var l = 0; l < resultCT.length; l++) {

                                                                            if (arrayCustom[l] == resultCT[l].tasklist_id) {

                                                                                CustomTask.push({ tasklist_name: resultCT[l].tasklist_name, tasklist_des: resultCT[l].tasklist_des, tasklist_duration: resultCT[l].tasklist_duration });
                                                                            }
                                                                        }

                                                                        var response = { "status": 1, "message": "Job details", 'jobDetails': results[0], 'standardTask': resultSTask, 'workerskills': skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket': basket, 'cleaningArea': cleaningArea, 'customTask': CustomTask };
                                                                        res.json(response);

                                                                    } else {
                                                                        var response = { "status": 1, "message": "Job details", 'jobDetails': results[0], 'standardTask': resultSTask, 'workerskills': skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket': basket, 'cleaningArea': cleaningArea, 'customTask': [] };
                                                                        res.json(response);
                                                                    }



                                                                }).catch(function(error) {
                                                                    var response = { "status": 0, "message": "Not Found" };
                                                                    res.json(response);
                                                                });

                                                            } else {
                                                                var response = { "status": 1, "message": "Job details", 'jobDetails': results[0], 'standardTask': resultSTask, 'workerskills': skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket': basket, 'cleaningArea': [], 'customTask': [] };
                                                                res.json(response);
                                                            }



                                                        }).catch(function(error) {
                                                            var response = { "status": 0, "message": "Not found" };
                                                            res.json(response);
                                                        });

                                                    } else {
                                                        var response = { "status": 1, "message": "Job details", 'jobDetails': results[0], 'standardTask': resultSTask, 'workerskills': skills, 'bedroom': bedroom, 'bathroom': bathroom, 'basket': basket, 'cleaningArea': [], 'customTask': [] };
                                                        res.json(response);
                                                    }

                                                }).catch(function(error) {
                                                    var response = { "status": 0, "message": "Not found" };
                                                    res.json(response);
                                                });


                                            } else {
                                                var response = { "status": 1, "message": "Job details", 'jobDetails': results[0], 'standardTask': resultSTask, 'workerskills': [], 'bedroom': bedroom, 'bathroom': bathroom, 'basket': basket, 'cleaningArea': [], 'customTask': [] };
                                                res.json(response);
                                            }

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found worker skills" };
                                            res.json(response);
                                        });

                                    } else {
                                        var response = { "status": 1, "message": "Job details", 'jobDetails': results[0], 'standardTask': [], 'workerskills': [], 'bedroom': bedroom, 'bathroom': bathroom, 'basket': basket, 'cleaningArea': [], 'customTask': [] };
                                        res.json(response);
                                    }


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found standard tasklist" };
                                    res.json(response);
                                });

                            } else {
                                var response = { "status": 1, "message": "Job details", 'jobDetails': results[0] };
                                res.json(response);
                            }


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found image data" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found for Job" };
                        res.json(response);
                    }


                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },

    // get referral data
    actionreferral: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getReferralData(client_id).then(function(resultR) {

                    if (resultR.length > 0) {

                        var refer_id = resultR[0].clients_referID;

                        Client.getTotalReferrals(refer_id).then(function(resultTR) {

                            if (resultTR.length > 0) {

                                resultR[0].referralsCount = resultTR.length;
                            } else {
                                resultR[0].referralsCount = 0;
                            }

                            var response = { "status": 1, "message": "Referral Data", 'referral': resultR };
                            res.json(response);

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // get credit history
    actioncreditHistory: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getReferralData(client_id).then(function(resultR) {

                    if (resultR.length > 0) {

                        var points = resultR[0].credit_points;

                        Client.getCreditHistory(client_id).then(function(resultTR) {

                            if (resultTR.length > 0) {

                                resultTR.forEachAsync(function(element, index, arr) {
                                    element.credit_history = 'Credit Earned';
                                    if (element.credit_type == 2) {
                                        element.credit_history = 'Credit Purchase';
                                    }
                                    if (element.credit_type == 4) {
                                        element.credit_history = 'Refund';
                                    }
                                    if (element.credit_type == 5) {
                                        element.credit_history = 'Top Up';
                                    }
                                });

                                Client.getCreditUsedByClient(client_id).then(function(resultUC) {

                                    if (resultUC.length > 0) {

                                        resultUC.forEachAsync(function(element, index, arr) {
                                            element.credit_type = 3;
                                            element.credit_history = 'Credit Used';
                                        });

                                        var finalArray = resultTR.concat(resultUC);

                                        finalArray.forEachAsync(function(element, index, arr) {
                                            element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                                        }, function() {

                                            var response = { "status": 1, "message": "Credit History", 'TotalPoints': points, 'creditlist': finalArray };
                                            res.json(response);
                                        });

                                    } else {

                                        resultTR.forEachAsync(function(element, index, arr) {
                                            element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                                        }, function() {

                                            var response = { "status": 1, "message": "Credit History", 'TotalPoints': points, 'creditlist': resultTR };
                                            res.json(response);
                                        });
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            } else {

                                Client.getCreditUsedByClient(client_id).then(function(resultUC) {

                                    if (resultUC.length > 0) {

                                        resultUC.forEachAsync(function(element, index, arr) {
                                            element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                                            element.credit_type = 3;
                                            element.credit_history = 'Credit Used';
                                        }, function() {

                                            var response = { "status": 1, "message": "Credit History", 'TotalPoints': points, 'creditlist': resultUC };
                                            res.json(response);
                                        });

                                    } else {

                                        var response = { "status": 1, "message": "Credit History", 'TotalPoints': points, 'creditlist': [] };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // get Angel list
    actionangelList: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;
        var agemin = req.body.agemin;
        var agemax = req.body.agemax;
        var suburb = req.body.suburb;
        var dateBook = req.body.booking_date;
        var booking_time_from = req.body.booking_time_from;
        var booking_time_to = req.body.booking_time_to;
        var booking_once_weekly = req.body.booking_once_weekly;
        var skills = req.body.skills;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {

                Client.getAngelList(agemin, agemax, suburb, skills).then(function(resultA) {

                    if (resultA.length > 0) {

                        Client.getImages().then(function(resultImage) {

                            Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                for (var i = 0; i < resultA.length; i++) {

                                    for (var j = 0; j < resultImage.length; j++) {

                                        if (resultA[i].workers_image == resultImage[j].file_id) {
                                            resultA[i].workers_image = resultImage[j].file_base_url;
                                        }
                                    }
                                    for (var k = 0; k < resultS.length; k++) {

                                        if (resultA[i].user_id == resultS[k].favorite_worker_id) {
                                            resultA[i].favWorker = 1;
                                        }
                                    }
                                }

                                if (booking_once_weekly == 1) {

                                    var response = { "status": 1, "message": "Angel list", "angelList": resultA };
                                    res.json(response);

                                } else {

                                    Client.checkLeaveDate(dateBook).then(function(resultCL) {

                                        Client.checkBookDate(dateBook, booking_time_from, booking_time_to).then(function(resultCB) {

                                            for (var l = 0; l < resultA.length; l++) {

                                                for (var m = 0; m < resultCL.length; m++) {

                                                    if (resultA[l].user_id == resultCL[m].worker_id) {
                                                        resultA.splice(l, 1);
                                                    }
                                                }

                                                for (var n = 0; n < resultCB.length; n++) {

                                                    if (resultA[l].user_id == resultCB[n].worker_id) {
                                                        resultA.splice(l, 1);
                                                    }
                                                }
                                            }

                                            var response = { "status": 1, "message": "Angel list", "angelList": resultA };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found for book date" };
                                            res.json(response);
                                        });


                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "No result found for leave" };
                                        res.json(response);
                                    });

                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "No found for fav worker" };
                                res.json(response);
                            });


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Image not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No list found", "angelList": [] };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found for book date" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // get rating list to rate worker
    actionratingList: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getJobDetails(booking_id, serviceflag).then(function(resultJ) {

                    if (resultJ.length > 0) {

                        var service_type = resultJ[0].tasktype_id;

                        Client.getRatinglist(service_type).then(function(resultRT) {

                            Client.getDefaultRatinglist().then(function(resultDR) {

                                var list = resultRT.concat(resultDR);

                                var response = { "status": 1, "message": "Rating list", 'ratelist': list };
                                res.json(response);


                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // rate worker
    actionrateWorker: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag; // once or weekly service
        var rating_id = req.body.rating_id;
        var ratings = req.body.ratings;
        var arrayId = [];
        var arrayRatings = [];

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getWorker(booking_id, serviceflag).then(function(resultW) {

                    Client.getClient(client_id).then(function(resultCP) {

                        var worker_id = resultW[0]['worker_id'];

                        var arrayId = rating_id.split(',');
                        var arrayRatings = ratings.split(',');
                        var sum = 0;
                        for (var i = 0; i < arrayRatings.length; i++) {
                            sum += parseInt(arrayRatings[i], 10);
                        }

                        var avg = sum / arrayId.length;

                        Client.giveWorkersRating(booking_id, client_id, worker_id, arrayId, arrayRatings, currentDate).then(function(resultWR) {

                            var points = '10';
                            var credit_points = parseInt(resultCP[0].credit_points) + parseInt(points);

                            var credit = { client_id: client_id, referralcredit_worker_id: worker_id, booking_id: booking_id, referralcredit_type: '1', referralcredit_points: points, created_at: currentDate, updated_at: currentDate };

                            Login.addCredit(credit, client_id, credit_points).then(function(resultC) {

                                Client.averageRating(booking_id, client_id, worker_id, avg, serviceflag).then(function(resultA) {

                                    Client.getAllDoneJobsBooking(worker_id).then(function(resultADJ) { // get all booking 

                                        Client.getAllDoneJobsWeekly(worker_id).then(function(resultADJW) { // get all booking

                                            if (resultADJ.length > 0 && resultADJW.length > 0) {
                                                var countBooking = resultADJ.length;
                                                var totalBookingRate = 0;
                                                var totalAvg = 0;
                                                for (var j = 0; j < resultADJ.length; j++) {
                                                    totalBookingRate += parseFloat(resultADJ[j].booking_worker_rating);
                                                }

                                                var countWeekly = resultADJW.length;
                                                var totalWeeklyRate = 0;
                                                var totalAvg = 0;
                                                for (var k = 0; k < resultADJW.length; k++) {
                                                    totalWeeklyRate += parseFloat(resultADJW[k].booking_worker_rating);
                                                }

                                                var totalRateData = parseFloat(totalBookingRate + totalWeeklyRate);
                                                var totalCountRate = parseFloat(countBooking + countWeekly);
                                                var totalAvg = totalRateData / totalCountRate;


                                            } else if (resultADJ.length > 0) {

                                                var countBooking = resultADJ.length;
                                                var totalBookingRate = 0;
                                                var totalAvg = 0;
                                                for (var l = 0; l < resultADJ.length; l++) {
                                                    totalBookingRate += parseFloat(resultADJ[l].booking_worker_rating);
                                                }
                                                var totalAvg = totalBookingRate / countBooking;

                                            } else if (resultADJW.length > 0) {

                                                var countWeekly = resultADJW.length;
                                                var totalWeeklyRate = 0;
                                                var totalAvg = 0;
                                                for (var m = 0; m < resultADJW.length; m++) {
                                                    totalWeeklyRate += parseFloat(resultADJW[m].booking_worker_rating);
                                                }
                                                var totalAvg = totalWeeklyRate / countWeekly;
                                            }

                                            Client.loginAvgRating(worker_id, totalAvg).then(function(resultTAR) {

                                                Worker.getJobDetails(booking_id, serviceflag).then(function(resultJBD) {

                                                    Worker.getDeviceToken(worker_id).then(function(resultDT) {

                                                        resultJBD.forEachAsync(function(element, index, arr) {
                                                            element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                                        });

                                                        var jobDate = resultJBD[0].booking_date;
                                                        var sender = resultCP[0].clients_name;
                                                        var deviceToken = resultDT[0].device_token;
                                                        var msgtitle = 'Rating';
                                                        var msgbody = 'Hello! ' + sender + ' has given you ratings for a ' + jobDate + ' job';

                                                        var notSendData = { user_id: worker_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                            Worker.notifyBadgeCount(worker_id).then(function(resultBC) {
                                                                var badgeW = resultBC.length;

                                                                Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                                    Worker.sendNotification(deviceToken, msgtitle, msgbody, badgeW, resultNR).then(function(resultSN) {
                                                                        // console.log(resultSN);
                                                                        Worker.getDeviceToken(client_id).then(function(resultCDT) {
                                                                            // console.log(resultCDT);
                                                                            var deviceType = resultCDT[0].device_type;
                                                                            var senderC = resultW[0]['workers_name'];
                                                                            var deviceTokenC = resultCDT[0].device_token;
                                                                            var msgtitleC = 'Get Credit';
                                                                            var msgbodyC = "You've just earned " + points + " credit points for rating " + senderC + " ";

                                                                            var notSendData1 = { user_id: client_id, notification_type: msgtitleC, notification_msg: msgbodyC, created_at: currentDate, updated_at: currentDate };

                                                                            Worker.addNotificationData(notSendData1).then(function(resultCAN) {

                                                                                Worker.getNotResponse(resultCAN).then(function(resultNRCD) {

                                                                                    Worker.notifyBadgeCount(client_id).then(function(resultBCD) {
                                                                                        var badgeC = resultBCD.length;

                                                                                        if (deviceType == 2) {

                                                                                            Client.sendNotificationClient(deviceTokenC, msgtitleC, msgbodyC, badgeC, resultNRCD).then(function(resultCSN) {

                                                                                                if (avg < 2) {
                                                                                                    var not_type = 'admin notification';
                                                                                                    var not_msg = 'The rating given by client ' + sender + ' to angel ' + senderC + ' is below than 2.';

                                                                                                    var adminNotData = { user_id: '1', notification_type: not_type, notification_msg: not_msg, created_at: currentDate, updated_at: currentDate };

                                                                                                    Worker.addNotificationData(adminNotData).then(function(resultADMIN) {

                                                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                        res.json(response);

                                                                                                    }).catch(function(error) {
                                                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                        res.json(response);
                                                                                                    });
                                                                                                } else {
                                                                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                    res.json(response);
                                                                                                }


                                                                                            }).catch(function(error) {
                                                                                                var response = { "status": 1, "message": "Failed to send client not to device" };
                                                                                                res.json(response);
                                                                                            });

                                                                                        } else if (deviceType == 3) {

                                                                                            Client.sendIOSNotification(deviceTokenC, msgtitleC, msgbodyC, badgeC, resultNRCD).then(function(resultCSN) {

                                                                                                if (avg < 2) {
                                                                                                    var not_type = 'admin notification';
                                                                                                    var not_msg = 'The rating given by client ' + sender + ' to angel ' + senderC + ' is below than 2.';

                                                                                                    var adminNotData = { user_id: '1', notification_type: not_type, notification_msg: not_msg, created_at: currentDate, updated_at: currentDate };

                                                                                                    Worker.addNotificationData(adminNotData).then(function(resultADMIN) {

                                                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                        res.json(response);

                                                                                                    }).catch(function(error) {
                                                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                        res.json(response);
                                                                                                    });
                                                                                                } else {
                                                                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                    res.json(response);
                                                                                                }

                                                                                            }).catch(function(error) {
                                                                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                res.json(response);
                                                                                            });


                                                                                        } else {

                                                                                            var response = { "status": 0, "message": "No client found" };
                                                                                            res.json(response);

                                                                                        }

                                                                                    }).catch(function(error) {
                                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                        res.json(response);
                                                                                    });

                                                                                }).catch(function(error) {
                                                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                    res.json(response);
                                                                                });

                                                                            }).catch(function(error) {
                                                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                res.json(response);
                                                                            });

                                                                        }).catch(function(error) {
                                                                            var response = { "status": 1, "message": "Rate worker successfully" };
                                                                            res.json(response);
                                                                        });

                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                        res.json(response);
                                                                    });

                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                                    res.json(response);
                                                                });

                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                                res.json(response);
                                                            });


                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Rate worker successfully" };
                                                            res.json(response);
                                                        });

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 0, "message": "Not found" };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "No credit added" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Failed to give rating" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No client data found" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No result for worker" };
                    res.json(response);
                });


            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Done Job listing to gives rating
    actionbookingListRate: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {

                Client.getHistoryJobsRate(client_id).then(function(resultsOP) {

                    if (resultsOP.length > 0) {

                        Client.getHistoryJobsWeeklyRate(client_id).then(function(resultsWP) {

                            if (resultsWP.length > 0) {

                                resultsWP.forEachAsync(function(element, index, arr) {

                                    if (element.booking_once_weekly == 1) {

                                        Client.getWeekDays(element.booking_id).then(function(resultWID) {

                                            if (typeof resultWID !== 'undefined' && resultWID != '') {
                                                element.preferred_days = resultWID.bookweekly_prefered_days;

                                                Client.getAllWeeklyJobs(element.booking_id).then(function(resultWJ) {

                                                    if (typeof resultWJ !== 'undefined' && resultWJ != '') {
                                                        element.renewFlag = resultWJ.renewFlag;
                                                    }
                                                });
                                            }
                                        });

                                    }
                                });
                            }

                            var results = resultsOP.concat(resultsWP);

                            Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                if (resultS.length > 0) {

                                    for (var i = 0; i < results.length; i++) {

                                        for (var j = 0; j < resultS.length; j++) {
                                            if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                results[i].flagFavWorker = 1;
                                            }

                                        }
                                    }
                                }
                                results.forEachAsync(function(element, index, arr) {
                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                    element.clients_image = result[0].file_base_url;
                                    element.workers_image = element.file_base_url;

                                    if (element.booking_worker_rating != '0') {
                                        element.rateFlag = '1';
                                    }

                                }, function() {
                                    var response = { "status": 1, "message": "booking list", "bookingRate": results };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not Found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not Found" };
                            res.json(response);
                        });

                    } else {

                        Client.getHistoryJobsWeeklyRate(client_id).then(function(results) {

                            if (results.length > 0) {

                                Client.favouriteWorkerClients(client_id).then(function(resultS) {

                                    if (resultS.length > 0) {

                                        for (var i = 0; i < results.length; i++) {

                                            for (var j = 0; j < resultS.length; j++) {
                                                if (results[i].worker_id == resultS[j].favorite_worker_id) {
                                                    results[i].flagFavWorker = 1;
                                                }

                                            }
                                        }
                                    }

                                    results.forEachAsync(function(element, index, arr) {
                                        element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                        element.clients_image = result[0].file_base_url;
                                        element.workers_image = element.file_base_url;

                                        if (element.booking_worker_rating != '0') {
                                            element.rateFlag = '1';
                                        }

                                        if (element.booking_once_weekly == 1) {

                                            Client.getWeekDays(element.booking_id).then(function(resultWID) {

                                                if (typeof resultWID !== 'undefined' && resultWID != '') {
                                                    element.preferred_days = resultWID.bookweekly_prefered_days;

                                                    Client.getAllWeeklyJobs(element.booking_id).then(function(resultWJ) {

                                                        if (typeof resultWJ !== 'undefined' && resultWJ != '') {
                                                            element.renewFlag = resultWJ.renewFlag;
                                                        }
                                                    });
                                                }
                                            });
                                        }

                                    }, function() {
                                        var response = { "status": 1, "message": "booking list", "bookingRate": results };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not Found" };
                                    res.json(response);
                                });

                            } else {
                                var response = { "status": 0, "message": "No record found for booking list" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not Found" };
                            res.json(response);
                        });
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not Found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client Does Not Exit" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try Again." };
            res.json(response);
        });

    },


    // get credit points
    actionmyCredit: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getReferralData(client_id).then(function(resultR) {

                    var points = resultR[0].credit_points;
                    if (resultR.length > 0) {

                        var response = { "status": 1, "message": "Credits", 'TotalPoints': points };
                        res.json(response);

                    } else {
                        var response = { "status": 0, "message": "No data found" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },



    // ios notification
    actioniosNot: function(req, res, next) {

        var apn = require("apn");
        var options = {
            cert: 'notification/cer.pem',
            key: 'notification/key.pem',
            passphrase: '1234HeavenSent'
        };
        var note = new apn.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600;
        note.badge = '1';
        note.sound = "ping.aiff";
        note.alert = {
            title: "Test Notification",
            body: "HSD"
        };
        
        var deviceToken = '25904572FE7BEAA0374B510C837D08C31A1C7BC89913D62D339C9761B5E07A2A';
        var apnProvider = new apn.Provider(options);
        apnProvider.send(note, deviceToken).then((result) => {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
        

        // Client.sendIOSNotification().then(function(resultR) {

        //     var response = { "status": 1, "message": "send notification", "data": resultR };
        //     res.json(response);


        // }).catch(function(error) {
        //     var response = { "status": 0, "message": "Not found" };
        //     res.json(response);
        // });
    },


    actionrateWeeklyList: function(req, res, next) {

        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {

                Client.getDatesWeek(client_id, booking_id).then(function(resultR) {

                    var first = resultR[0].booking_date;
                    var last = resultR.slice(-1)[0].booking_date;

                    var startdate = dateFormat(first, "yyyy-mm-dd");
                    var enddate = dateFormat(last, "yyyy-mm-dd");


                    Client.getDatesRate(startdate, enddate).then(function(resultRBD) {

                        if (resultRBD.length > 1) {

                            Client.getBulkDoneJobRate(startdate, resultRBD, client_id, booking_id).then(function(resultRBS) {

                                Client.getImages().then(function(resultImage) {

                                    if (resultImage.length > 0) {

                                        for (var i = 0; i < resultRBS.length; i++) {

                                            if (resultRBS[i].booking_worker_rating != 0) {
                                                resultRBS[i].rateFlag = 1;
                                            }

                                            for (var k = 0; k < resultImage.length; k++) {

                                                if (resultRBS[i].workers_image == resultImage[k].file_id) {
                                                    resultRBS[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                        resultRBS.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "dates", 'weeklyRecord': resultRBS };
                                            res.json(response);
                                        });


                                    } else {
                                        resultRBS.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "dates", 'weeklyRecord': resultRBS };
                                            res.json(response);
                                        });
                                    }


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });


                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        } else {

                            Client.getBulkDoneJobRateWithoutSat(startdate, enddate, client_id, booking_id).then(function(resultRB) {

                                Client.getImages().then(function(resultImage) {

                                    if (resultImage.length > 0) {

                                        for (var i = 0; i < resultRB.length; i++) {

                                            if (resultRB[i].booking_worker_rating != 0) {
                                                resultRB[i].rateFlag = 1;
                                            }

                                            for (var k = 0; k < resultImage.length; k++) {

                                                if (resultRB[i].workers_image == resultImage[k].file_id) {
                                                    resultRB[i].workers_image = resultImage[k].file_base_url;
                                                }
                                            }
                                        }

                                        resultRB.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "dates", 'weeklyRecord': resultRB };
                                            res.json(response);
                                        });


                                    } else {
                                        resultRB.forEachAsync(function(element, index, arr) {
                                            element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");

                                        }, function() {
                                            var response = { "status": 1, "message": "dates", 'weeklyRecord': resultRB };
                                            res.json(response);
                                        });
                                    }


                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });


                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });


            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    actionpaymentHistory: function(req, res, next) {

        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {

                Client.getPaymentData(client_id).then(function(resultR) {

                    Client.getBookDataPaymentOnce(client_id).then(function(resultRO) {
                                           
                        Client.getBookDataPaymentWeekly(client_id).then(function(resultRW) {
                            // console.log(resultRW);

                            for (var i = 0; i < resultR.length; i++) {

                                for (var k = 0; k < resultRO.length; k++) {

                                    if (resultR[i].booking_id == resultRO[k].booking_id && resultR[i].booking_serviceflag == 0) {
                                        resultR[i].booking_date = resultRO[k].booking_date;
                                        resultR[i].service_type = resultRO[k].tasktype_name;
                                    }
                                }
                                for (var j = 0; j < resultRW.length; j++) {

                                    if (resultR[i].booking_id == resultRW[j].bookweekly_id && resultR[i].booking_serviceflag == 1) {
                                        resultR[i].booking_date = resultRW[j].booking_date;
                                        resultR[i].service_type = resultRW[j].tasktype_name;
                                    }
                                }

                            }

                            resultR.forEachAsync(function(element, index, arr) {
                                if (element.booking_id != 0) {
                                    if (element.payment_trans_date != '0000-00-00 00:00:00') {
                                        element.payment_trans_date = dateFormat(element.payment_trans_date, "yyyy-mm-dd hh:MM:ss");
                                    }
                                    element.booking_date = dateFormat(element.booking_date, "yyyy-mm-dd");
                                } else if (element.creditPurchase_id != 0) {
                                    if (element.payment_trans_date != '0000-00-00 00:00:00') {
                                        element.payment_trans_date = dateFormat(element.payment_trans_date, "yyyy-mm-dd hh:MM:ss");
                                    }
                                    element.booking_date = "";
                                    element.service_type = "";
                                }
                                if (element.payment_type == 0) {
                                    element.payment_type = "Booking";
                                } else {
                                    element.payment_type = "Credit Purchase";
                                }
                            }, function() {
                                var response = { "status": 1, "message": "payment", 'paymentData': resultR };
                                res.json(response);
                            });


                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Not found" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not found" };
                        res.json(response);
                    });

                    // var response = { "status": 1, "message": "payment", 'paymentData' : resultR};
                    // res.json(response);                       

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },

    // Update device token
    actionupdateDevice: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id = req.body.user_id;
        var device_token = req.body.device_token;
        var device_type = req.body.device_type;

        Client.updateDeviceToken(user_id, device_token, device_type).then(function(results) {

            if (results.affectedRows == 1) {
                var response = { "status": 1, "message": "Device token updated successfully" };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "Failed to update device token" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Failed to add" };
            res.json(response);
        });

    },


    // Check worker availability
    actionworkerAvailable: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var date = req.body.date;
        var from_time = req.body.from_time;
        var to_time = req.body.to_time;
        var worker_id = req.body.worker_id;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {


                Client.checkLeaveDate(date, worker_id).then(function(resultCL) {

                    if (resultCL.length > 0) {

                        var response = { "status": 0, "message": "Worker is not available,worker is on leave" };
                        res.json(response);

                    } else {

                        Client.checkWorkerAvailableBook(date, from_time, to_time, worker_id).then(function(resultsB) {

                            if (resultsB.length > 0) {

                                var response = { "status": 0, "message": "Worker is not available,already booked" };
                                res.json(response);

                            } else {

                                Client.checkWorkerAvailableBookWeekly(date, from_time, to_time, worker_id).then(function(resultsBW) {

                                    if (resultsBW.length > 0) {

                                        var response = { "status": 0, "message": "Worker is not available,already booked" };
                                        res.json(response);

                                    } else {
                                        var response = { "status": 1, "message": "Worker is available" };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Failed to add" };
                                    res.json(response);
                                });

                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Failed to add" };
                            res.json(response);
                        });

                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No leave data found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client not exists" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Failed to add" };
            res.json(response);
        });

    },


    // rate worker for weekly jobs
    actionrateWorkerWeek: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDate();
        var total = 0;
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var worker_id = req.body.worker_id;
        var jobCount = req.body.jobCount;
        var rating_id = req.body.rating_id;
        var ratings = req.body.ratings;
        var serviceflag = 1;
        var arrayId = [];
        var arrayRatings = [];
        var ratedpoints = 10;
        if (ratedpoints != 0) {
            var points = ratedpoints / jobCount;
        } else {
            var points = 0;
        }

        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;
            if (total == 1) {

                Client.getClient(client_id).then(function(resultCP) {

                    var arrayId = rating_id.split(',');
                    var arrayRatings = ratings.split(',');
                    var sum = 0;
                    for (var i = 0; i < arrayRatings.length; i++) {
                        sum += parseInt(arrayRatings[i], 10);
                    }

                    var avg = sum / arrayId.length;

                    Client.giveWorkersRating(booking_id, client_id, worker_id, arrayId, arrayRatings, currentDate).then(function(resultWR) {

                        var credit_points = parseInt(resultCP[0].credit_points) + parseInt(points);

                        var credit = { client_id: client_id, referralcredit_worker_id: worker_id, booking_id: booking_id, referralcredit_type: '1', referralcredit_points: points, created_at: currentDate, updated_at: currentDate };

                        Login.addCredit(credit, client_id, credit_points).then(function(resultC) {

                            Client.averageRating(booking_id, client_id, worker_id, avg, serviceflag).then(function(resultA) {

                                Client.getAllDoneJobsBooking(worker_id).then(function(resultADJ) { // get all booking 

                                    Client.getAllDoneJobsWeekly(worker_id).then(function(resultADJW) { // get all booking

                                        if (resultADJ.length > 0 && resultADJW.length > 0) {
                                            var countBooking = resultADJ.length;
                                            var totalBookingRate = 0;
                                            var totalAvg = 0;
                                            for (var j = 0; j < resultADJ.length; j++) {
                                                totalBookingRate += parseFloat(resultADJ[j].booking_worker_rating);
                                            }

                                            var countWeekly = resultADJW.length;
                                            var totalWeeklyRate = 0;
                                            var totalAvg = 0;
                                            for (var k = 0; k < resultADJW.length; k++) {
                                                totalWeeklyRate += parseFloat(resultADJW[k].booking_worker_rating);
                                            }

                                            var totalRateData = parseFloat(totalBookingRate + totalWeeklyRate);
                                            var totalCountRate = parseFloat(countBooking + countWeekly);
                                            var totalAvg = totalRateData / totalCountRate;


                                        } else if (resultADJ.length > 0) {

                                            var countBooking = resultADJ.length;
                                            var totalBookingRate = 0;
                                            var totalAvg = 0;
                                            for (var l = 0; l < resultADJ.length; l++) {
                                                totalBookingRate += parseFloat(resultADJ[l].booking_worker_rating);
                                            }
                                            var totalAvg = totalBookingRate / countBooking;

                                        } else if (resultADJW.length > 0) {

                                            var countWeekly = resultADJW.length;
                                            var totalWeeklyRate = 0;
                                            var totalAvg = 0;
                                            for (var m = 0; m < resultADJW.length; m++) {
                                                totalWeeklyRate += parseFloat(resultADJW[m].booking_worker_rating);
                                            }
                                            var totalAvg = totalWeeklyRate / countWeekly;
                                        }

                                        Client.loginAvgRating(worker_id, totalAvg).then(function(resultTAR) {

                                            Worker.getJobDetails(booking_id, serviceflag).then(function(resultJBD) {

                                                if (points == 0) {

                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                    res.json(response);

                                                } else {

                                                    Worker.getDeviceToken(worker_id).then(function(resultDT) {

                                                        resultJBD.forEachAsync(function(element, index, arr) {
                                                            element.booking_date = dateFormat(element.booking_date, "dd/mm/yyyy");
                                                        });

                                                        var jobDate = resultJBD[0].booking_date;
                                                        var sender = resultCP[0].clients_name;
                                                        var deviceToken = resultDT[0].device_token;
                                                        var msgtitle = 'Rating';
                                                        var msgbody = 'Hello! ' + sender + ' has given you ratings for a ' + jobDate + ' job';

                                                        var notSendData = { user_id: worker_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                                                            Worker.notifyBadgeCount(worker_id).then(function(resultBCW) {
                                                                var badgeW = resultBCW.length;

                                                                Worker.getNotResponse(resultAN).then(function(resultNRW) {

                                                                    Worker.sendNotification(deviceToken, msgtitle, msgbody, badgeW, resultNRW).then(function(resultSN) {

                                                                        Worker.getDeviceToken(client_id).then(function(resultCDT) {

                                                                            var deviceType = resultCDT[0].device_type;
                                                                            var senderC = resultW[0]['workers_name'];
                                                                            var deviceTokenC = resultCDT[0].device_token;
                                                                            var msgtitleC = 'Get Credit';
                                                                            var msgbodyC = "You've just earned " + points + " credit points for rating " + senderC + " ";

                                                                            var notSendData1 = { user_id: client_id, notification_type: msgtitleC, notification_msg: msgbodyC, created_at: currentDate, updated_at: currentDate };

                                                                            Worker.addNotificationData(notSendData1).then(function(resultCAN) {

                                                                                Worker.getNotResponse(resultCAN).then(function(resultNR) {

                                                                                    Worker.notifyBadgeCount(client_id).then(function(resultBC) {
                                                                                        var badge = resultBC.length;

                                                                                        if (deviceType == 2) {

                                                                                            Client.sendNotificationClient(deviceTokenC, msgtitleC, msgbodyC, badge, resultNR).then(function(resultCSN) {

                                                                                                var response = { "status": 1, "message": "Rate worker successfully", 'notify': resultNR, badgeCount: resultBC.length };
                                                                                                res.json(response);

                                                                                            }).catch(function(error) {
                                                                                                var response = { "status": 1, "message": "Failed to send client not to device" };
                                                                                                res.json(response);
                                                                                            });

                                                                                        } else if (deviceType == 3) {

                                                                                            Client.sendIOSNotification(deviceTokenC, msgtitleC, msgbodyC, badge, resultNR).then(function(resultCSN) {

                                                                                                var response = { "status": 1, "message": "Rate worker successfully", 'notify': resultNR, badgeCount: resultBC.length };
                                                                                                res.json(response);

                                                                                            }).catch(function(error) {
                                                                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                                res.json(response);
                                                                                            });


                                                                                        } else {

                                                                                            var response = { "status": 0, "message": "No client found" };
                                                                                            res.json(response);

                                                                                        }

                                                                                    }).catch(function(error) {
                                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                        res.json(response);
                                                                                    });

                                                                                }).catch(function(error) {
                                                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                    res.json(response);
                                                                                });

                                                                            }).catch(function(error) {
                                                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                                                res.json(response);
                                                                            });

                                                                        }).catch(function(error) {
                                                                            var response = { "status": 1, "message": "Rate worker successfully" };
                                                                            res.json(response);
                                                                        });

                                                                    }).catch(function(error) {
                                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                                        res.json(response);
                                                                    });

                                                                }).catch(function(error) {
                                                                    var response = { "status": 1, "message": "Rate worker successfully" };
                                                                    res.json(response);
                                                                });

                                                            }).catch(function(error) {
                                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                                res.json(response);
                                                            });


                                                        }).catch(function(error) {
                                                            var response = { "status": 1, "message": "Rate worker successfully" };
                                                            res.json(response);
                                                        });

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Rate worker successfully" };
                                                        res.json(response);
                                                    });

                                                }


                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Rate worker successfully" };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Not found" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "Not found" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Not found" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "No credit added" };
                            res.json(response);
                        });

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Failed to give rating" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No client data found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client does not exit" };
                res.json(response);
            }
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });

    },


    // Send support mail to admin
    actionSupportMail: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var name = req.body.name;
        var subject = req.body.subject;
        var message = req.body.message;
        var mail_from = req.body.mail_from;

        var mailData = { 'contactUs_from': mail_from, 'contactUs_name': name, 'contactUs_subject': subject, 'contactUs_msg': message, 'created_at': currentDate, 'updated_at': currentDate };

        Client.sendSupportMail(mailData).then(function(resultM) {

            if (resultM > 0) {

                // var transporter = nodemailer.createTransport({
                //     service: 'gmail',
                //     auth: {
                //         user: 'etpl18node@gmail.com',   
                //         pass: 'nodetest123'
                //     }
                // });

                // var mailOptions = {
                //     to: 'etpl18node@gmail.com',
                //     subject: 'User Cafe Credentials',
                //     text: 'Username : priya@gmail.com and password : priya'
                // };

                // transporter.sendMail(mailOptions, function(error, info){
                //     if (error) {
                //         var response = { "status": 0, "message": "E-mail Not Send" };
                //         res.json(response);
                //     } else {                                         
                var response = { "status": 1, "message": "Mail send successfully" };
                res.json(response);
                //     }
                // });

            } else {
                var response = { "status": 0, "message": "Falied to send mail" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Falied to add mail data" };
            res.json(response);
        });

    },



    // Check worker availability before payment
    actionCheckAvailWorkerPayment: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;

        Client.checkClientExists(client_id).then(function(result) {

            Client.getBookingInfo(booking_id, client_id, serviceflag).then(function(resultBI) {

                var bookingDate = dateFormat(resultBI[0].booking_date, "yyyy-mm-dd");

                Client.CheckWorkerAvailabilityDates(worker_id, bookingDate, serviceflag, booking_id).then(function(resultBD) {

                    if (resultBD.length > 0) {
                        var response = { "status": 0, "message": "Worker is not available" };
                        res.json(response);

                    } else {

                        Client.checkLeaveDate(bookingDate, worker_id).then(function(resultCL) {

                            if (resultCL.length > 0) {

                                var response = { "status": 0, "message": "Worker is not available" };
                                res.json(response);
                            } else {

                                var response = { "status": 1, "message": "Worker is available" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Falied to add mail data" };
                            res.json(response);
                        });
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Falied to add mail data" };
                    res.json(response);
                });

            }).catch(function(error) {
                var response = { "status": 0, "message": "Falied to get booking info" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Client does not exists" };
            res.json(response);
        });

    },

    // Check worker availability for favourite workers
    actionCheckFavAvailWorker: function(req, res, next) {
        var client_id = req.body.client_id;
        var worker_id = req.body.worker_id;
        var booking_date = req.body.booking_date;

        Client.checkClientExists(client_id).then(function(result) {

            Client.checkWorkerServiceOnce(worker_id, booking_date).then(function(resultBD) {

                if (resultBD.length > 0) {
                    var response = { "status": 0, "message": "Worker is not available" };
                    res.json(response);

                } else {

                    Client.checkWorkerServiceWeekly(worker_id, booking_date).then(function(resultBDW) {

                        if (resultBDW.length > 0) {

                            var response = { "status": 0, "message": "Worker is not available" };
                            res.json(response);
                        } else {

                            Client.checkLeaveDate(booking_date, worker_id).then(function(resultCL) {

                                if (resultCL.length > 0) {

                                    var response = { "status": 0, "message": "Worker is not available" };
                                    res.json(response);
                                } else {

                                    var response = { "status": 1, "message": "Worker is available" };
                                    res.json(response);
                                }

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Falied to add mail data" };
                                res.json(response);
                            });

                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Data not found" };
                        res.json(response);
                    });
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "Data not found" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Client does not exists" };
            res.json(response);
        });

    },

    // Reschedule bookings
    actionRescheduleBooking: function(req, res, next) {
        var client_id = req.body.client_id;
        var worker_id = req.body.worker_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var booking_date = req.body.booking_date;
        var booking_time_from = req.body.booking_time_from;
        var booking_time_to = req.body.booking_time_to;

        Client.checkClientExists(client_id).then(function(result) {

            Client.getBookingOnceWeek(worker_id, client_id, booking_id, serviceflag).then(function(resultData) {

                if (resultData.length == 1) {
                    var oldDate = dateFormat(resultData[0].booking_date, "dd/mm/yyyy");
                    var newDate = dateFormat(booking_date, "dd/mm/yyyy");

                    var updateRe = { booking_date: booking_date, booking_time_from: booking_time_from, booking_time_to: booking_time_to };

                    Client.updateRescheduleData(updateRe, booking_id, serviceflag).then(function(resultBDW) {

                        Worker.getDeviceToken(worker_id).then(function(resultDT) {

                            var booking_date = req.body.booking_date;
                            var deviceToken = resultDT[0].device_token;
                            var msgtitle = 'Reschedule Job';
                            var msgbody = 'Job on ' + oldDate + ' has been reschedule on ' + newDate + ' ';

                            var notSendData = { user_id: worker_id, notification_type: msgtitle, notification_detail_id: booking_id, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                            Worker.addNotificationData(notSendData).then(function(resultAN) {

                                Worker.getNotResponse(resultAN).then(function(resultNR) {

                                    Worker.notifyBadgeCount(worker_id).then(function(resultBC) {
                                        var badge = resultBC.length;

                                        Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                            var response = { "status": 1, "message": "Reschedule booking successfully" };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Reschedule booking successfully" };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Reschedule booking successfully" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Reschedule booking successfully" };
                                    res.json(response);
                                });


                            }).catch(function(error) {
                                var response = { "status": 1, "message": "Reschedule booking successfully" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 1, "message": "Reschedule booking successfully" };
                            res.json(response);
                        });


                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Failed to reschedule" };
                        res.json(response);
                    });

                } else {
                    var response = { "status": 1, "message": "Booking is not exists" };
                    res.json(response);
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "Data not found" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Client does not exists" };
            res.json(response);
        });

    },


    // Renew bookings
    actionRenewBooking: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var client_id = req.body.client_id;
        var booking_id = req.body.booking_id;
        var serviceflag = req.body.serviceflag;
        var booking_date = req.body.booking_date;
        var nowdate = req.body.nowdate;
        var booking_time_from = req.body.booking_time_from;
        var booking_time_to = req.body.booking_time_to;


        // var totalWeek = '2'; //req.body.totalWeek;
        // var dateData =  { 
        //         '1' :   [ {'date' : '2018-11-19'},
        //                   {'date' : '2018-11-20'},
        //                   {'date' : '2018-11-21'} ],
        //         '2' :   [
        //                     {'date' : '2018-11-26'},
        //                     {'date' : '2018-11-27'},
        //                     {'date' : '2018-11-28'}
        //                 ]
        //     }

        var totalWeek = req.body.totalWeek;
        var dateData = req.body.dateData;

            Object.entries(dateData).forEach(([key, val]) => {

                for(var i=1; i<= totalWeek; i++ ) {

                    if(i == key) {

                        if( typeof val[0] != 'undefined') {
                            val[0].cost = '275';
                        }
                        if( typeof val[1] != 'undefined') {
                            val[1].cost = '265';    
                        }
                        if( typeof val[2] != 'undefined') {
                            val[2].cost = '255';    
                        }
                        if( typeof val[3] != 'undefined') {
                            val[3].cost = '225';    
                        }
                        if( typeof val[4] != 'undefined') {
                            val[4].cost = '230';    
                        }
                        if( typeof val[5] != 'undefined') {
                            val[5].cost = '250';    
                        }
                    }
                }
            });

            var finalCostArray = [];
            Object.entries(dateData).forEach(([key1, val1]) => {

                for(var l=0; l<val1.length; ++l) {

                    finalCostArray.push({'date': val1[l].date , 'cost': val1[l].cost});
                }
            });

        Client.checkClientExists(client_id).then(function(result) {

            Client.getServiceAllData(booking_id, serviceflag, client_id).then(function(resultData) {

                if (resultData.length > 0) {

                    if (serviceflag == 0) {

                        var register = {
                            client_id: client_id,
                            worker_id: resultData[0].worker_id,
                            booking_date: booking_date,
                            booking_time_from: booking_time_from,
                            booking_time_to: booking_time_to,
                            booking_lat: resultData[0].booking_lat,
                            booking_long: resultData[0].booking_long,
                            booking_apartment: resultData[0].booking_apartment,
                            booking_street: resultData[0].booking_street,
                            booking_city: resultData[0].booking_city,
                            booking_suburb: resultData[0].booking_suburb,
                            booking_work_time: resultData[0].booking_work_time,
                            booking_service_type: resultData[0].booking_service_type,
                            booking_bedrooms: resultData[0].booking_bedrooms,
                            booking_bathrooms: resultData[0].booking_bathrooms,
                            booking_baskets: resultData[0].booking_baskets,
                            booking_cleaning_area: resultData[0].booking_cleaning_area,
                            booking_other_work: resultData[0].booking_other_work,
                            booking_cost: resultData[0].booking_cost,
                            booking_custom: resultData[0].booking_custom,
                            booking_inst1: resultData[0].booking_inst1,
                            booking_inst2: resultData[0].booking_inst2,
                            booking_inst3: resultData[0].booking_inst3,
                            booking_image1: resultData[0].booking_image1,
                            booking_image2: resultData[0].booking_image2,
                            booking_image3: resultData[0].booking_image3,
                            booking_image4: resultData[0].booking_image4,
                            booking_image5: resultData[0].booking_image5,
                            booking_skills: resultData[0].booking_skills,
                            booking_age_min: resultData[0].booking_age_min,
                            booking_age_max: resultData[0].booking_age_max,
                            booking_once_weekly: serviceflag,
                            created_at: currentDate,
                            updated_at: currentDate
                        };
                        var days = booking_date;
                        var preferred_days = booking_date;

                        Client.addBooking(register, serviceflag, days, preferred_days, nowdate, finalCostArray).then(function(resultB) {

                            Worker.getDeviceToken(resultData[0].worker_id).then(function(resultDT) {

                                var booking_date = req.body.booking_date;
                                var bookDate = dateFormat(booking_date, "dd/mm/yyyy");
                                var deviceToken = resultDT[0].device_token;
                                var msgtitle = 'New Job';
                                var msgbody = 'A new job on ' + bookDate + ' has been assigned to you!';

                                var notSendData = { user_id: resultData[0].worker_id, notification_type: 'New Job', notification_detail_id: resultB, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                Worker.addNotificationData(notSendData).then(function(resultAN) {

                                    Worker.getNotResponse(resultAN).then(function(resultNR) {

                                        Worker.notifyBadgeCount(resultData[0].worker_id).then(function(resultBC) {
                                            var badge = resultBC.length;

                                            Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                                var response = { "status": 1, "message": "Booking successfully", 'bookingId': resultB };
                                                res.json(response);

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Booking successfully", 'bookingId': resultB };
                                                res.json(response);
                                            });

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Booking successfully", 'bookingId': resultB };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Booking successfully", 'bookingId': resultB };
                                        res.json(response);
                                    });


                                }).catch(function(error) {
                                    var response = { "status": 1, "message": "Booking successfully", 'bookingId': resultB };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 1, "message": "Booking successfully", 'bookingId': resultB };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Data not found" };
                            res.json(response);
                        });

                    } else {

                        Client.getPreferdDaysData(booking_id).then(function(resultWPD) {

                            var register = {
                                client_id: client_id,
                                worker_id: resultData[0].worker_id,
                                booking_date: booking_date,
                                booking_time_from: booking_time_from,
                                booking_time_to: booking_time_to,
                                booking_lat: resultData[0].booking_lat,
                                booking_long: resultData[0].booking_long,
                                booking_apartment: resultData[0].booking_apartment,
                                booking_street: resultData[0].booking_street,
                                booking_city: resultData[0].booking_city,
                                booking_suburb: resultData[0].booking_suburb,
                                booking_work_time: resultData[0].booking_work_time,
                                booking_service_type: resultData[0].booking_service_type,
                                booking_bedrooms: resultData[0].booking_bedrooms,
                                booking_bathrooms: resultData[0].booking_bathrooms,
                                booking_baskets: resultData[0].booking_baskets,
                                booking_cleaning_area: resultData[0].booking_cleaning_area,
                                booking_other_work: resultData[0].booking_other_work,
                                booking_cost: resultData[0].booking_cost,
                                booking_custom: resultData[0].booking_custom,
                                booking_inst1: resultData[0].booking_inst1,
                                booking_inst2: resultData[0].booking_inst2,
                                booking_inst3: resultData[0].booking_inst3,
                                booking_image1: resultData[0].booking_image1,
                                booking_image2: resultData[0].booking_image2,
                                booking_image3: resultData[0].booking_image3,
                                booking_image4: resultData[0].booking_image4,
                                booking_image5: resultData[0].booking_image5,
                                booking_skills: resultData[0].booking_skills,
                                booking_age_min: resultData[0].booking_age_min,
                                booking_age_max: resultData[0].booking_age_max,
                                booking_once_weekly: serviceflag,
                                created_at: currentDate,
                                updated_at: currentDate
                            };
                            var days = booking_date;
                            var preferred_days = resultWPD.bookweekly_prefered_days;

                            Client.addBooking(register, serviceflag, days, preferred_days, nowdate, finalCostArray).then(function(resultB) {

                                Client.getWeekBookId(resultB).then(function(resultWBI) {
                                    var returnId = resultWBI.bookweekly_id;

                                    Worker.getDeviceToken(resultData[0].worker_id).then(function(resultDT) {

                                        var bookingdateS = booking_date.split(',');
                                        var startdate = dateFormat(bookingdateS[0], "dd/mm/yyyy");
                                        var enddate = dateFormat(bookingdateS[bookingdateS.length - 1], "dd/mm/yyyy");
                                        var deviceToken = resultDT[0].device_token;
                                        var msgtitle = 'New Job';
                                        var msgbody = 'A new job from ' + startdate + ' to ' + enddate + ' has been assigned to you!';

                                        var notSendData = { user_id: resultData[0].worker_id, notification_type: 'New Job', notification_detail_id: resultB, notification_detail_serviceflag: serviceflag, notification_msg: msgbody, created_at: currentDate, updated_at: currentDate };

                                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                                Worker.notifyBadgeCount(resultData[0].worker_id).then(function(resultBC) {
                                                    var badge = resultBC.length;

                                                    Worker.sendNotification(deviceToken, msgtitle, msgbody, badge, resultNR).then(function(resultSN) {

                                                        var response = { "status": 1, "message": "Booking successfully", 'bookingId': returnId };
                                                        res.json(response);

                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Booking successfully", 'bookingId': returnId };
                                                        res.json(response);
                                                    });

                                                }).catch(function(error) {
                                                    var response = { "status": 1, "message": "Booking successfully", 'bookingId': returnId };
                                                    res.json(response);
                                                });

                                            }).catch(function(error) {
                                                var response = { "status": 1, "message": "Booking successfully", 'bookingId': returnId };
                                                res.json(response);
                                            });


                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": "Booking successfully", 'bookingId': returnId };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 1, "message": "Booking successfully", 'bookingId': returnId };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Data not found" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "Data not found" };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Data not found for preferred days" };
                            res.json(response);
                        });
                    }


                } else {
                    var response = { "status": 1, "message": "Booking is not exists" };
                    res.json(response);
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "Data not found" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Client does not exists" };
            res.json(response);
        });

    },


    // List of buy credit points
    actionBuyCredit: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {

                Client.getBuyCreditList().then(function(resultData) {

                    if (resultData.length > 0) {

                        Client.getPointPerRand().then(function(resultRand) {

                            if(resultRand.length > 0) {

                                var response = { "status": 1, "message": "Buy credit list", "rand" : resultRand[0].setting_value, "creditList": resultData };
                                res.json(response);
                                
                            } else {
                                var response = { "status":0, "message": "No credit list available" };
                                res.json(response);
                            }

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Data not found" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 0, "message": "No credit list available" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Data not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client is not exists" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Client does not exists" };
            res.json(response);
        });

    },

    //top up point per rand
    actiontopUpPoint: function(req, res, next) {
        var client_id = req.body.client_id;

        Client.checkClientExists(client_id).then(function(result) {

            if (result.length == 1) {

                Client.getPointPerRand().then(function(resultData) {

                    if (resultData.length > 0) {

                        var response = { "status": 1, "message": "Point per rand", "point": resultData[0].setting_value };
                        res.json(response);

                    } else {
                        var response = { "status": 0, "message": "No credit list available" };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Data not found" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Client is not exists" };
                res.json(response);
            }

        }).catch(function(error) {
            var response = { "status": 0, "message": "Client does not exists" };
            res.json(response);
        });

    },

    normalEFT: function(req, res, next) {
        var client_id = req.body.client_id;
        Client.checkClientExists(client_id).then(function(result) {
            total = result.length;

            if (result.length > 0) {
                Client.getNormalEFT().then(function(result) {
                    // console.log(result.length);
                    if (result.length == 1) {
                        var response = { "status": 1, "message": "Normal EFT list", "normalEft": result };
                        res.json(response);
                    }
                }).catch(function(error) {
                    //console.log(error);
                    var response = { "status": 0, "message": "Try again." };
                    res.json(response);
                });
            } else {
                var response = { "status": 0, "message": "Client is not exists" };
                res.json(response);
            }
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
    },

    getLatLong: function(req, res, next) {
        geocoder.geocode('29 champs elysée paris', function(err, res) {
            console.log(res);
        });


    },



    checkWeekDays: function(req, res, next) {

        var once = {"first" : '275'};
        var twice = {"first" : '275', 'second': '265'};
        var thrice = {"first" : '275', 'second': '265', 'third': '255'};
        var four   = {"first" : '275', 'second': '265', 'third': '255', 'four':'225'};
        var fifth  = {"first" : '275', 'second': '265', 'third': '255', 'four':'225', 'fifth':'230'};
        var sixth = {"first":'275', 'second': '265', 'third': '255', 'four':'225', 'fifth':'230', 'six':'250'};
                                                                                                                
        var totalWeek = '3';
        var dateData =  { 
                            '1' :   [ {'date' : '09-11-2018'} ],
                            '2' :   [
                                        {'date' : '12-11-2018'},
                                        {'date' : '13-11-2018'}
                                    ],
                            '3' :   [
                                        {'date' : '12-11-2018'},
                                        {'date' : '13-11-2018'},
                                        {'date' : '16-11-2018'},
                                        {'date' : '17-11-2018'},
                                        {'date' : '18-11-2018'},
                                        {'date' : '19-11-2018'}
                                    ]
                        }

                        Object.entries(dateData).forEach(([key, val]) => {

                            for(var i=1; i<= totalWeek; i++ ) {

                                if(i == key) {

                                    if( typeof val[0] != 'undefined') {
                                        val[0].cost = '275';
                                    }
                                    if( typeof val[1] != 'undefined') {
                                        val[1].cost = '265';    
                                    }
                                    if( typeof val[2] != 'undefined') {
                                        val[2].cost = '255';    
                                    }
                                    if( typeof val[3] != 'undefined') {
                                        val[3].cost = '225';    
                                    }
                                    if( typeof val[4] != 'undefined') {
                                        val[4].cost = '230';    
                                    }
                                    if( typeof val[5] != 'undefined') {
                                        val[5].cost = '250';    
                                    }
                                }
                            }
                        });

                        var finalCostArray = [];
                        Object.entries(dateData).forEach(([key1, val1]) => {

                            for(var l=0; l<val1.length; ++l) {

                                finalCostArray.push({'date': val1[l].date , 'cost': val1[l].cost});
                            }
                        });

                        var response = { "status": 1, "weeklylist": finalCostArray};
                        res.json(response); 
    },


    checkNotData: function(req, res, next) {
        var deviceToken = 'deDbKl5J9vw:APA91bGqMPpR6jka_9Lys5nWq9Nqy4aUtvGwob2cfBS4FIz9NwmDf0w_2TLPu6ot3uJjfFYvcgW6UeJNV3ZXQnihhgW0o4LnycBbksqA9pBLMyu7uzZMv7MjenNX3IJ6J40UXPq5RAZj';
        var msgtitle = 'Test';
        var msgbody = 'HSD';
         Worker.sendNotificationTest(deviceToken,msgtitle,msgbody).then(function(result) {
           
                var response = { "status": 1, "data": result };
                res.json(response);
         
        }).catch(function(error) {
            //console.log(error);
            var response = { "status": 0, "message": "Try again." };
            res.json(response);
        });
       
    }


}



module.exports = ClientController;