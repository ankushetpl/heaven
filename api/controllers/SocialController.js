var fs = require('fs');
var Social = require('../models/Social');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var SocialController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            social_id: req.body.social_id,
            social_type: req.body.social_type,
            status: req.body.status,
            created_at: req.body.created_at,
            defaults: req.body.defaults
        };

        Social.countSocial(searchParams).then(function (result) {
            total = result[0].total;
            Social.getSocial(limit, offset, searchParams, orderParams).then(function (results) {
                var response = { "status": 1, "message": "Record Found", "social": results, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionAllSocial: function (req, res, next) {
        
        Social.getAllSocial().then(function (result) {
            var response = { "status": 1, "message": "All social types", "socialtypes" : result };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Not found" };
            res.json(response);
        });
    }, 

    actionView: function (req, res, next) {
        var social_id = req.body.social_id;

        if (typeof social_id !== 'undefined' && social_id !== '') {
            Social.singleSocial(social_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "social": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var socialData = {
            social_type: req.body.social_type,
            social_url: req.body.social_url,
            status: req.body.status,
            defaults: req.body.defaults,
            created_at: currentDate,
            updated_at: currentDate
        };
        // console.log(emailData);
        var defaults = req.body.defaults;
        var status = req.body.status;

        if(defaults == 1) {

            if(status == 1) {

                Social.addSocial(socialData).then(function (result) {
            
                    if(result > 0) {

                        var socialType = req.body.social_type;
                        var socialData = {defaults:defaults  };

                        Social.defaultSocial(socialData, result, socialType).then(function(result) {

                            var response = { "status": 1, "message": "Social created successfully!" };
                            res.json(response);        

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "Try Again" };
                            res.json(response);
                        });

                    } else {
                        var response = { "status": 1, "message": "Social created successfully!" };
                        res.json(response);    
                    }
                    
                }).catch(function (error) {
                    var response = { "status": 0, "message": "Failed to create" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Please first approve link to set as default." };
                res.json(response);    
            }

        } else {
            Social.addSocial(socialData).then(function (result) {

                var response = { "status": 1, "message": "Social created successfully!" };
                res.json(response);    

            }).catch(function (error) {
                var response = { "status": 0, "message": "Failed to create" };
                res.json(response);
            });

        }
        
    },

    actionUpdate: function (req, res, next) {
        var social_id = req.body.social_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var socialData = {
            social_type: req.body.social_type,
            social_url: req.body.social_url,
            status: req.body.status,
            defaults: req.body.defaults,
            updated_at: currentDate
        };
        var defaults = req.body.defaults;
        var status = req.body.status;

        if(defaults == 1) {

            if(status == 1) {

                Social.updateSocial(socialData, social_id).then(function (result) {
            
                    var socialType = req.body.social_type;
                    var socialDataDe = {defaults:defaults  };

                    Social.defaultSocial(socialDataDe, social_id, socialType).then(function(result) {

                        var response = { "status": 1, "message": "Social updated successfully!" };
                        res.json(response);        

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Try Again" };
                        res.json(response);
                    });

                }).catch(function (error) {
                    var response = { "status": 0, "message": "Failed to update" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "Please first approve link to set as default." };
                res.json(response);    
            }

        } else {
            Social.updateSocial(socialData, social_id).then(function (result) {

                var response = { "status": 1, "message": "Social updated successfully!" };
                res.json(response);

            }).catch(function (error) {
                var response = { "status": 0, "message": "Failed to update" };
                res.json(response);
            });

        }
        
        
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this social!" };
        var social_id = req.body.social_id;

        Social.deleteSocial(social_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your social has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var social_id = req.body.social_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Social.statusSocial(data, social_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Social approved" };                
            }else{
                var response = { "status": 1, "message": "Social disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    actionDefault: function(req, res, next) {
        var social_id = req.body.social_id;
        var Default = req.body.default;
        var socialType = req.body.social_type;
        var currentDate = commonHelper.getCurrentDateTime();
        var socialData = { defaults: req.body.default };
        Social.defaultSocial(socialData, social_id, socialType).then(function(result) {

            if(Default == 1){
                var response = { "status": 1, "message": "Social set as default" };                
            }else{
                var response = { "status": 1, "message": "Remove social from default" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
           
    }

}

module.exports = SocialController;