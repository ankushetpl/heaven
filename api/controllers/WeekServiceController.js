var fs = require('fs');
var WeekService = require('../models/WeekService');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var WeekServiceController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            week_name: req.body.week_name,
            week_id: req.body.week_id,
            status: req.body.status,
            created_at: req.body.created_at
        };

        WeekService.countWeekService(searchParams).then(function (result) {
            total = result[0].total;
            WeekService.getWeekService(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "weekService": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var week_id = req.body.week_id;

        if (typeof week_id !== 'undefined' && week_id !== '') {
            WeekService.singleWeekService(week_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "weekService": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var weekData = {
            week_name: req.body.week_name,
            week_price: req.body.week_price,
            week_day: req.body.week_day,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        WeekService.addWeekService(weekData).then(function (result) {
            var response = { "status": 1, "message": "Weekly Service created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Weekly Service create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var week_id = req.body.week_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var weekData = {
            week_name: req.body.week_name,
            week_price: req.body.week_price,
            week_day: req.body.week_day,
            status: req.body.status,
            updated_at: currentDate
        };

        WeekService.updateWeekService(weekData, week_id).then(function (result) {
            var response = { "status": 1, "message": "Weekly Service updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Weekly Service update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Weekly Service!" };
        var week_id = req.body.week_id;

        WeekService.deleteWeekService(week_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Weekly Service has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var week_id = req.body.week_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        WeekService.statusWeekService(data, week_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Weekly Service Approved" };                
            }else{
                var response = { "status": 1, "message": "Weekly Service Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },

    //API
    actionAllWeekService: function (req, res, next) {
        var total = 0;

        WeekService.getAllWeekService().then(function (result) {
            total = result.length;
            
            result.forEachAsync(function(element, index, arr) {
                element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                element.updated_at = dateFormat(element.updated_at, "yyyy-mm-dd hh:MM:ss");
               
            }, function() {
                if(total > 0){
                    var response = { "status": 1, "message": "Record Found", "weekService": result, "total": total };
                    res.json(response);
                }else{
                    var response = { "status": 1, "message": "No Record Found", "weekService": '', "total": 0 };
                    res.json(response);
                }                
            });            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }

}

module.exports = WeekServiceController;