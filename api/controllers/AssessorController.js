var fs = require('fs');
var Assessor = require('../models/Assessor');
var dateFormat = require('dateformat');
var commonHelper = require('../helper/common_helper.js');
var moment = require('moment');
var nodeUnique = require('node-unique-array');
var async = require('async');

var AssessorController = {


    //GET ALL CHECKLIST
    actionChecklist: function(req, res, next) {
        
        Assessor.getAllChecklist().then(function(result) {
           
            var response = { "status": 1, "checklist": result };
            res.json(response);
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for checklist" };
            res.json(response);
        });
      
    },

    //GET ALL NOTIFICATIONS
    actionNotification: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id = req.body.user_id;
        var lastDate = moment().subtract(30, 'days').format('YYYY-MM-DD hh:MM:ss');
        
        Assessor.countNot(user_id, currentDate, lastDate).then(function(resultNC) {
        
            Assessor.getAllNotification(user_id, currentDate, lastDate).then(function(resultN) {

                var response = { "status": 1, "notifications": resultN, 'count': resultNC[0].total };
                res.json(response);
                
            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for notifications" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for notifications" };
            res.json(response);
        });
      
    },

    // DELETE ALL NOTIFICATIONS
    actionDeleteNot: function(req, res, next) {
        var user_id = req.body.user_id;

        Assessor.deleteAll(user_id).then(function(resultN) {

            var response = { "status": 1, "message": "Clear all notifications successfully" };
            res.json(response);
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "Failed to clear notifications" };
            res.json(response);
        });

    },

    //GET ALL REQUEST LIST
    actionRequests: function(req, res, next) {

        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            name: req.body.name,
            city: req.body.city,
            state: req.body.state,
            startDate: req.body.startDate,
            endDate: req.body.endDate
        };

        var user_id = req.body.user_id;

        Assessor.countAllRequests(searchParams, user_id).then(function(result) {
            total = result[0].total;
            Assessor.getAllRequests(limit, offset, searchParams, orderParams, user_id).then(function(resultN) {
                var response = { "status": 1, "message" : "request list", "request": resultN, "requestCount" : total };
                res.json(response);
                
            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for requests" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for requests" };
            res.json(response);
        });


    },

    //REJECT WORKER REQUEST FOR ASSESSMENT
    actionRejectWorker: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var request_id = req.body.request_id;

        Assessor.rejectWorkerRequest(request_id).then(function(result) {

            Assessor.readNotification(request_id).then(function(resultNR) { 

                Assessor.getRequestData(request_id).then(function(resultRD) {
                        
                        if(resultRD[0].request_assessLevel == 0) {
                            var level = 'First level';
                        } else {
                            var level = 'Final level';
                        }

                    var msg = 'The Assessor '+ resultRD[0].assessors_name+' has rejected '+ level +' assessment booking request for the angel '+ resultRD[0].workers_name+' for '+ moment(resultRD[0].request_date).format("MMMM Do YYYY") +' for time slot '+ moment(resultRD[0].request_timeFrom, "HH:mm:ss").format("hh:mm A") +' - '+ moment(resultRD[0].request_timeTo, "HH:mm:ss").format("hh:mm A") +' ';
                    
                    var notData = { 'user_id' : resultRD[0].cafe_id, 'notification_type' : 'Assessment Request Reject', 'notification_detail_id' : request_id, 'notification_msg' : msg, 'created_at': currentDate, 'updated_at': currentDate };

                    Assessor.addNotificationData(notData).then(function(resultAN) {

                        var response = { "status": 1, "message" : "Worker request rejected", 'requestData':result };
                        res.json(response);

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No record found for requests" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No record found for requests" };
                    res.json(response);
                });

            }).catch(function(error) {
                var response = { "status": 0, "message": "Failed to read notification" };
                res.json(response);
            }); 
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for requests" };
            res.json(response);
        });

    },  

    //ACCEPT WORKER REQUEST FOR ASSESSMENT
    actionAcceptWorker: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var request_id = req.body.request_id;

        Assessor.getRequestData(request_id).then(function(resultRD) {
            var reDate = dateFormat(resultRD[0].request_date, "yyyy-mm-dd");
            var reFromTime = resultRD[0].request_timeFrom;
            var reToTime = resultRD[0].request_timeTo;
            var assId = resultRD[0].assessor_id;

            Assessor.CheckRequestBookedExists(reDate, reFromTime, reToTime, request_id).then(function(resultCB) {
               
                if(resultCB.length == 1) {

                    var response = { "status": 0, "message" : "You already have booking on this time slot." };
                    res.json(response);

                } else {

                    Assessor.CheckLeaveExists(reDate, assId).then(function(resultCL) {

                        if(resultCL.length == 1) {

                            var response = { "status": 0, "message" : "You are on leave this date" };
                            res.json(response);
                        } else {

                            Assessor.acceptWorkerRequest(request_id).then(function(result) {

                                Assessor.readNotification(request_id).then(function(resultNR) { 

                                    if(resultRD[0].request_assessLevel == 0) {
                                        var level = 'First level';
                                    } else {
                                        var level = 'Final level';
                                    }
                                    
                                    var msg = 'The Assessor '+ resultRD[0].assessors_name+' has accepted '+ level +' assessment booking request for the angel '+ resultRD[0].workers_name+' for '+ moment(resultRD[0].request_date).format("MMMM Do YYYY") +' for time slot '+ moment(resultRD[0].request_timeFrom, "HH:mm:ss").format("hh:mm A") +' - '+ moment(resultRD[0].request_timeTo, "HH:mm:ss").format("hh:mm A") +' ';
                                    
                                    var notData = { 'user_id' : resultRD[0].cafe_id, 'notification_type' : 'Assessment Request Accept', 'notification_detail_id' : request_id, 'notification_msg' : msg, 'created_at': currentDate, 'updated_at': currentDate };

                                    Assessor.addNotificationData(notData).then(function(resultAN) {

                                        Assessor.GetOtherRequest(request_id,reDate,reFromTime,reToTime,assId).then(function(resultOther) {
                                            
                                            if(resultOther.length > 0) {
                                                
                                                resultOther.forEachAsync(function(element, index, arr) {
                                                    // console.log(element.workers_name);
                                                    // console.log(resultOther);
                                                    var msgtitle = 'Auto Reject';
                                                    var msgbody = 'The Assessment request for angel '+element.workers_name+' is auto rejected.';
                    
                                                    var notSendData = { user_id: element.cafe_id,notification_type: msgtitle,notification_msg: msgbody,notification_detail_id:element.request_id,created_at: currentDate, updated_at: currentDate};
                                                    
                                                    Assessor.AutoReject(element.request_id,notSendData).then(function(resultAuto) {
                                                                
                                                        
                                                        // Assessor.addNotificationData(notSendData).then(function(resultAN) {
                                                        //     var response = { "status": 1, "message": "Request auto rejected" };
                                                        //     res.json(response);
                                                                
                                                        // }).catch(function(error) {
                                                        //     var response = { "status": 1, "message": "Request auto rejected" };
                                                        //     res.json(response);
                                                        // });
                        
                                                    }).catch(function(error) {
                                                        var response = { "status": 1, "message": "Request auto rejected" };
                                                        res.json(response);
                                                    });
        
                                                }, function() {
                                                    var response = { "status": 1, "message" : "Worker request accepted", 'requestData':result };
                                                    res.json(response);
                                                });

                                            } else {
                                                var response = { "status": 1, "message" : "Worker request accepted", 'requestData':result };
                                                res.json(response);
                                            }

                                        }).catch(function(error) {
                                            var response = { "status": 0, "message": "Failed to get other request." };
                                            res.json(response);
                                        });

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "No record found for requests" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "Failed to read notification" };
                                    res.json(response);
                                });
                                 
                            }).catch(function(error) {
                                var response = { "status": 0, "message": "No record found for requests" };
                                res.json(response);
                            });
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No record found for requests exists" };
                        res.json(response);
                    });
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for requests exists" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for requests" };
            res.json(response);
        });

    },        

    //GET WORKER'S PROFILE DATA
    actionWorkerProfile: function(req, res, next) {
        var worker_id = req.body.user_id;
        var request_id = req.body.request_id;

        Assessor.getWorkerProfileData(worker_id).then(function(resultN) {

            Assessor.getRequestData(request_id).then(function(resultNR) {

            var response = { "status": 1, "message" : "worker profile", "worker": resultN, "request" : resultNR };
            res.json(response);
            
            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for request" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for worker" };
            res.json(response);
        });

    },

    //GET ALL MY ADDEDD WORKER'S
    actionMyWorker: function(req, res, next) {

        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            name: req.body.name,
            city: req.body.city,
            state: req.body.state,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            status: req.body.status,
            level: req.body.level
        };
        
        var assessor_id = req.body.assessor_id;

        Assessor.countAllMyWorker(searchParams, assessor_id).then(function(resultC) {
            total = resultC[0].total;
            Assessor.getAllMyWorker(limit, offset, searchParams, orderParams, assessor_id).then(function(result) {
                    
                var response = { "status": 1, "message" : "all added workers", "allWorkers": result, "workerCount" : total };
                res.json(response);
                           
            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for worker for final level" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No count for worker for first level" };
            res.json(response);
        });

    },

    //GET ALL ASSESSORS
    actionAllAssessors: function(req, res, next) {

        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            city: req.body.city,
            state: req.body.state,
            startDate: req.body.startDate
        };

        var userID = req.body.user_id;
        var workerId = req.body.workerId;
        
        Assessor.getWorkerData(workerId).then(function(resultWU) {
            // console.log(resultWU);
            var country = resultWU[0].workers_country;
            Assessor.countAllAssessors(searchParams, userID, country).then(function(resultC) {
                total = resultC[0].total;
                Assessor.getAllAssessors(limit, offset, searchParams, orderParams, userID, country).then(function(result) {

                    var response = { "status": 1, "message" : "assessors list", "assessors": result, "assessorCount": total };
                    res.json(response);
                
                }).catch(function(error) {
                    var response = { "status": 0, "message": "No record found for assessors" };
                    res.json(response);
                });

            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for assessors" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for assessors" };
            res.json(response);
        });

    },

    //GET ASSESSORS CALENDER 
    actionCalender: function(req, res, next) {
        var user_id = req.body.user_id;

        Assessor.getBookingRequestData(user_id).then(function(resultB) {

            if(resultB.length > 0) {

                resultB.forEachAsync(function(element, index, arr) {
                    element.title = 'Booking';
                    element.start = dateFormat(element.start, "yyyy-mm-dd");
                    element.color = '#40c353';
                }, function(){
                    Assessor.getLeavesData(user_id).then(function(resultL) {

                        if(resultL.length > 0) {

                            resultL.forEachAsync(function(element, index, arr) {
                                element.title = 'Unavailable';
                                element.start = dateFormat(element.start, "yyyy-mm-dd");
                                element.color = '#a83333';
                            }, function(){
                                var result = resultB.concat(resultL);
                                var response = { "status": 1, "message" : "Assessor's calender", "calender": result };
                                res.json(response);
                            });                        

                        } else {
                            var response = { "status": 1, "message" : "Assessor's calender", "calender": resultB };
                            res.json(response);
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No record found for leaves" };
                        res.json(response);
                    }); 

                });

            } else {

                Assessor.getLeavesData(user_id).then(function(resultL) {

                    resultL.forEachAsync(function(element, index, arr) {
                        element.title = 'Unavailable';
                        element.start = dateFormat(element.start, "yyyy-mm-dd");
                        element.color = '#a83333';

                    }, function() {
                        var response = { "status": 1, "message" : "Assessor's calender", "calender": resultL };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No record found for leaves" };
                    res.json(response);
                });
            }
           
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for Booking requests" };
            res.json(response);
        });

    },

    //ALL ADMIN ADDED SKILLS LIST
    actionSkills: function(req, res, next) {
        
        Assessor.getSkillSet().then(function(result) {

            result.forEachAsync(function(element, index, arr) {
                element.ratingPoints = 0;
               
            }, function() {
                var response = { "status": 1, "message" : "Skill set", "skillList": result };
                res.json(response);
            });
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });
    },

    //SKILLS BY ID
    actionSkillById: function(req, res, next) {
        var total = 0;
        var id   = req.body.id;
        var arr = id.split(',');
        var angelSkill = [];
        arr.forEachAsync(function(element, index, arr) {

            Assessor.getSkillsName(element).then(function(result) {
                angelSkill.push(result[0].skills_name);
                total = total + 1;
                if(total == arr.length) {
                   var response = { "status": 1, "message" : "skill names", "skillname": angelSkill };
                   res.json(response);
                }
                
            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for skill" };
                res.json(response);
            });
           
        }, function(){
                
        });
        
    },    


    //SAVE EXTRA SKILL IN SKILLS LIST
    actionAddskill: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var skillsData   = req.body.skillName;
        var unique_array = new nodeUnique();
        unique_array.add(skillsData);
        var skills = unique_array.get();
        
        var arrayExtra = [];
        var total = 0;

            skills.forEachAsync(function(element, index, arr) {
                
                var skillName = element.skillName;
                
                Assessor.checkSkillExists(skillName).then(function(resultE) {
                    
                    if(resultE.length == 1) {
                        arrayExtra.push({'skills_id' : resultE[0].skills_id, 'skills_name' : resultE[0].skills_name, 'ratingPoints' : 0});
                        
                        total = total + 1;
                        if(total == skills.length) {
                           var response = { "status": 1, "message" : "Add extra skill successfully", "newSkill": arrayExtra };
                           res.json(response);
                        }
                    } else {

                        var addNew = {'skills_name' : skillName, 'parent': 0, 'skills_type': 1, 'created_at': currentDate, 'updated_at' : currentDate};

                        Assessor.saveSkill(addNew).then(function(result) {

                            if(result.length > 0) {
                                arrayExtra.push({'skills_id' : result[0].skills_id, 'skills_name' : result[0].skills_name, 'ratingPoints' : 0});
                                
                                total = total + 1;
                                if(total == skills.length) {
                                   var response = { "status": 1, "message" : "Add extra skill successfully", "newSkill": arrayExtra };
                                   res.json(response);
                                }

                            } 

                        }).catch(function(error) {
                            var response = { "status": 0, "message": "No record found for skill exists" };
                            res.json(response);
                        });
                    }
                   
                });                
            }, function(){
                
            });
    },

    //GIVE WORKERS RATING
    actionWorkerRatings: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var todayDate = commonHelper.getCurrentDate();
        var total       = 0;
        var totalData = 0;
        
        var worker_id   = req.body.worker;
        var request_id   = req.body.request;
        var rating_id   = req.body.rateId;
        var ratings     = req.body.ratePoint;
        var assessor_id   = req.body.user_id;
        
        var sum = 0;
            for( var i = 0; i < ratings.length; i++ ) {
                sum += parseInt( ratings[i], 10 ); 
            }

        var avg = Math.round(sum/rating_id.length);
        
        Assessor.getRequestData(request_id).then(function(resultRD) {
            var assessmentLevel = resultRD[0].request_assessLevel;
            if(assessmentLevel == 0) {
                var level = 'First level assessment';
            } else {
                var level = 'Final level assessment';
            }
            
            Assessor.giveWorkersRating(assessor_id,assessmentLevel,worker_id,rating_id,ratings,currentDate).then(function(result) {

                Assessor.AverageRatingAssessment(assessor_id,assessmentLevel,worker_id,rating_id,avg,currentDate,todayDate).then(function(resultR) {

                        if(avg >= 8) {

                            var msg = 'The Angel '+ resultRD[0].workers_name+' qualifies in '+ level +' ';
                            var type = 'Qualify Angel';
                        } else {
                            var msg = 'The Angel '+ resultRD[0].workers_name+' disqualifies in '+ level +' ';
                            var type = 'Disqualify Angel';
                        }
                        
                        var notData = { 'user_id' : resultRD[0].cafe_id, 'notification_type' : type, 'notification_detail_id' : request_id, 'notification_msg' : msg, 'created_at': currentDate, 'updated_at': currentDate };
                        
                        Assessor.addNotificationData(notData).then(function(resultAN) {

                            if(assessmentLevel == 1 && avg >= 8) {
                            
                                Assessor.ActiveWorkerProfile(worker_id, avg, rating_id, currentDate).then(function(resultACW) {


                                    Assessor.getWorkerData(worker_id).then(function(resultData) {

                                        totalData = resultData[0].length;

                                        if(totalData > 0){

                                            var request = require('request')
          
                                            var sendRequest = {
                                               'messages': [ {'content': "Hello, your OTP to reset you password is '"+ otp +"'. Do not share it with anyone.", 'destination': resultData[0].user_username} ]
                                            };

                                            request.post({
                                               headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbklkIjoiMjkyMTM2IiwiaXNzIjoiU21zUG9ydGFsU2VjdXJpdHlBcGkiLCJhdWQiOiJBbGwiLCJleHAiOjE1MzI2OTE1MzEsIm5iZiI6MTUzMjYwNTEzMX0.3POziqafdScAFMbB2NaJv0T8pCa1PKwPaCEkiMJz5Qc'},
                                               url: 'https://rest.smsportal.com/v1/bulkmessages',
                                               json: true,
                                               body: sendRequest
                                            }, function (error, response, body) {
                                               if(response) {

                                                    var response = { "status": 1, "message" : "Assessment of worker is successfully done."};
                                                    res.json(response);
                                                } else {
                                                    var response = { "status": 1, "message" : "Assessment of worker is successfully done." };
                                                    res.json(response);
                                                }
                                            });

                                        }else{
                                            var response = { "status": 1, "message" : "Assessment of worker is successfully done." };
                                            res.json(response);    
                                        }
                                        

                                    }).catch(function(error) {
                                        var response = { "status": 0, "message": "No record found for requests" };
                                        res.json(response);
                                    });

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "No record found for requests" };
                                    res.json(response);
                                });
                            } else {
                                var response = { "status": 1, "message" : "Assessment of worker is successfully done." };
                                res.json(response);
                            }
                            
                        }).catch(function(error) {
                            var response = { "status": 0, "message": "No record found for requests" };
                            res.json(response);
                        });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Failed to add average ratings" };
                    res.json(response);
                });
                
            }).catch(function(error) {
                var response = { "status": 0, "message": "Failed to add ratings" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Failed to get data by request id" };
            res.json(response);
        });

    },


    //FINAL ASSESSMENT REQUEST
    actionBookNextAssessor: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id   = req.body.user_id;
        var assessor_id   = req.body.assessorId;
        var worker_id   = req.body.worker_id;
        var request_date   = req.body.request_date;
        var start_time   = req.body.request_timeFrom;
        var end_time   = req.body.request_timeTo;
        
        Assessor.getCafeByWorker(worker_id).then(function(resultC) {
            
            var finalData = {"cafe_id": resultC[0].cafeuser_id, "worker_id" : worker_id, "assessor_id" : assessor_id, "request_assessLevel" : 1, "request_date" : request_date, "request_timeFrom" : start_time, "request_timeTo" : end_time, "created_at" : currentDate, "updated_at" : currentDate }
    
            Assessor.CheckRequestBooked(assessor_id, request_date, start_time, end_time).then(function(resultCB) {  
                var countData = resultCB.total;
               
                if(countData == 0 ) {

                    Assessor.finalAssessmentRequest(finalData, worker_id).then(function(resultF) {  
                        
                        if(resultF > 0) {

                            Assessor.getAssesssorName(user_id).then(function(resultSND) {  

                                var notData = { 'user_id' : assessor_id, 'notification_type' : 'Final Assessment Request', 'notification_detail_id' : resultF, 'notification_msg' : 'You have final assessment request from '+resultSND['assessors_name']+' ', 'created_at': currentDate, 'updated_at': currentDate };

                                Assessor.addNotificationData(notData).then(function(resultAN) {

                                    var response = { "status": 1, "message" : "Request for final assessment send successfully" };
                                    res.json(response);

                                }).catch(function(error) {
                                    var response = { "status": 0, "message": "No record found for requests" };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 0, "message": "No record found for requests" };
                                res.json(response);
                            });

                        } else {
                            var response = { "status": 1, "message" : "Failed to send request for final assessment" };
                            res.json(response);
                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Failed to send request" };
                        res.json(response);
                    });

                } else {
                    var response = { "status": 0, "message" : "The request already present for this date and time slot" };
                    res.json(response);
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "Failed to get request" };
                res.json(response);
            });
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for cafe user" };
            res.json(response);
        });
    },


    //GET ASSESSOR'S ALL REQUEST FOR PARTICULAR DATE
    actionRequestByDate: function(req, res, next) {

        var user_id   = req.body.user_id;
        var searchDate   = req.body.searchDate;
        
        Assessor.getAllRequestForDate(user_id, searchDate).then(function(result) {

            if(result.length > 0) {

                result.forEachAsync(function(element, index, arr) {
                    element.request_date = dateFormat(element.request_date, "yyyy-mm-dd");
                    element.type = 0;  //Booking
                    if(element.request_assessLevel == 0) {
                        element.request_assessLevel = 'First';
                    } else {
                        element.request_assessLevel = 'Final';
                    }

                }, function() {
                    var response = { "status": 1, "message" : "requests for date", "dateData": result };
                    res.json(response);
                });

            } else {

                Assessor.checkUnavailability(user_id, searchDate).then(function(resultU) {

                    if(resultU.length > 0) {

                        resultU.forEachAsync(function(element, index, arr) {
                            element.type = 1; // Unavailable

                        }, function() {
                            var response = { "status": 1, "message" : "requests for date", "dateData": resultU };
                            res.json(response);
                        });

                    } else {
                        var resultA = [{type : 2}];   //Available
                        var response = { "status": 1, "message" : "requests for date", "dateData": resultA };
                        res.json(response);
                    }

                }).catch(function(error) {
                    var response = { "status": 0, "message": "No record found for calender" };
                    res.json(response);
                });    

            } 
 
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });

    },


    //GET REQUEST DATA BY ID
    actionRequestById: function(req, res, next) {
        var request_id   = req.body.request_id;
        
        Assessor.getRequestData(request_id).then(function(result) {
            
            var response = { "status": 1, "message" : "request data", "requestById": result };
            res.json(response);
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });
    },  


    //UPDATE ASSESSOR'S AVAILABILITY
    actionUpdateAvailability: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var user_id   = req.body.user_id;
        var selectedDate   = req.body.selectedDate;
        var type = req.body.type;
        
            if(type == 1) {   // Unavailable

                var leaveData = {"assessor_id" : user_id, "assessorLeave_date" : selectedDate, "created_at" : currentDate, "updated_at" : currentDate }   

            } else {       // 2 Available
                var leaveData = {"is_deleted" : 1, "updated_at" : currentDate }   
            }
                                              
            Assessor.UpdateAvailability(leaveData, user_id, type, selectedDate).then(function(result) {
                if(type == 1) {

                    var response = { "status": 1, "message" : "Update unavailable successfully." };
                    res.json(response);
                } else if(type == 2) {
                    var response = { "status": 1, "message" : "Update available successfully." };
                    res.json(response);
                } else {
                    var response = { "status": 1, "message" : "Failed to update calender." };
                    res.json(response);
                }
                
                
            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for calender" };
                res.json(response);
            });
    },

    //RESCHEDULE REQUEST 
    actionRescheduleRequest: function(req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var request_id   = req.body.request_id;
        var rescheduleDate   = req.body.rescheduleDate;
        // var time_from   = req.body.time_from;
        // var time_to   = req.body.time_to;


		Assessor.getRequestData(request_id).then(function(resultRD) {
            var reDate = rescheduleDate;
            var reFromTime = resultRD[0].request_timeFrom;
            var reToTime = resultRD[0].request_timeTo;
            var assId = resultRD[0].assessor_id;

            Assessor.CheckRequestBookedExists(reDate, reFromTime, reToTime, request_id).then(function(resultCB) {
               
                if(resultCB.length == 1) {

                    var response = { "status": 0, "message" : "You already have booking on this time slot." };
                    res.json(response);

                } else {

                    Assessor.CheckLeaveExists(reDate, assId).then(function(resultCL) {

                        if(resultCL.length == 1) {

                            var response = { "status": 0, "message" : "You are on leave this date" };
                            res.json(response);
                        } else {

                            var rescheduleData = {"request_date" : rescheduleDate, "updated_at" : currentDate }   
                                               
            				Assessor.rescheduleRequest(rescheduleData, request_id).then(function(result) {

                                if(resultRD[0].request_assessLevel == 0) {
			                        var level = 'First level assessment';
			                    } else {
			                        var level = 'Final level assessment';
			                    }
			                    var msg = 'The Assessor '+ resultRD[0].assessors_name+' reschedule your '+ level +' request on '+ moment(resultRD[0].request_date).format("MMMM Do YYYY") +' for time slot '+ moment(resultRD[0].request_timeFrom, "HH:mm:ss").format("hh:mm A") +' - '+ moment(resultRD[0].request_timeTo, "HH:mm:ss").format("hh:mm A") +' ';
			                    var type = 'Reschedule Request';
			                    
			                    var notData = { 'user_id' : resultRD[0].cafe_id, 'notification_type' : type, 'notification_detail_id' : request_id, 'notification_msg' : msg, 'created_at': currentDate, 'updated_at': currentDate };

			                        Assessor.addNotificationData(notData).then(function(resultAN) {

			                            var response = { "status": 1, "message" : "Reschedule assessment request successfully." };
			                            res.json(response);

			                        }).catch(function(error) {
			                            var response = { "status": 0, "message": "No record found for requests" };
			                            res.json(response);
			                        });

			                            }).catch(function(error) {
			                                var response = { "status": 0, "message": "No record found for requests" };
			                                res.json(response);
			                            });
			                        }

                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No record found for requests exists" };
                        res.json(response);
                    });
                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "No record found for requests exists" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for requests" };
            res.json(response);
        });        
       
    },

    //GET ALL PROVINCE FOR SOUTH AFRICE
    actionGetProvince: function(req, res, next) {
                                        
        var userID = req.body.userID;
        var userRoleID = req.body.userRoleID;

        Assessor.getAllProvince(userID, userRoleID).then(function(result) {

            var response = { "status": 1, "message" : "Province data", "state":result };
            res.json(response);
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });
    },

    //GET ALL CITIES BY STATE ID
    actionGetAllCities: function(req, res, next) {
        
        var stateId   = req.body.stateId;

        Assessor.getAllCitiesById(stateId).then(function(result) {

            var response = { "status": 1, "message" : "Cities data", "cities":result };
            res.json(response);
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });
    },


    //assessor's leave data 
    actionGetUnavailabilityData: function(req, res, next) {

        var startDate = req.body.startDate;
        var assessor_id   = req.body.assessor_id;
        var count = 0;
         
        Assessor.checkUnavailability(assessor_id, startDate).then(function(result) {
            
            if(typeof result !== 'undefined' && result !== '') {
                count = result.length;       
                var response = { "status": 1, "message" : "Leaves data", "leaves":count };
                res.json(response);
            } else {
                var response = { "status": 0, "message" : " No Leaves data" };
                res.json(response);
            }
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });
    },


    //assessor's request data 
    actionGetRequestBookAssessor: function(req, res, next) {

        var startDate = req.body.startDate;
        var assessor_id   = req.body.assessor_id;
        var count = 0;
        
        Assessor.getAllRequestForDate(assessor_id, startDate).then(function(result) {
            
            if(typeof result !== 'undefined' && result !== '') {
                count = result.length;       
                var response = { "status": 1, "message" : "Assessment data", "assessment":count };
                res.json(response);
            } else {
                var response = { "status": 0, "message" : " No Leaves data" };
                res.json(response);
            }
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "No record found for calender" };
            res.json(response);
        });
    },


    //time slot of assessor's request data 
    actionGetTimeSlots: function(req, res, next) {

        var assessorId = req.body.assessorId;
        var workerID = req.body.workerID;
        var assessmentDate   = req.body.assessmentDate;
        
        Assessor.getSlotPeriod().then(function(result) {

            Assessor.getCafeByWorker(workerID).then(function(resultW) {
                    var workerName = resultW[0].workers_name;

                Assessor.getAssesssorName(assessorId).then(function(resultA) {
                    var assessorsName = resultA.assessors_name;

                    Assessor.getAllRequestCalenderDate(assessorId, assessmentDate).then(function(resultR) {

                        if(resultR.length > 0) {

                            resultR.forEachAsync(function(element, index, arr) {
                                element.stringC = '-';
                                element.slot = moment(element.request_timeFrom, "HH:mm").format("HH:mm") + element.stringC + moment(element.request_timeTo, "HH:mm").format("HH:mm") ;

                            }, function() {
                                var response = { "status": 1, "message" : "time slot data", "slotsTime":result.setting_value, "requestData":resultR, "workerNameHead" : workerName, "assessorsName": assessorsName };
                                res.json(response);
                            });

                        } else {

                            var response = { "status": 1, "message" : "time slot data", "slotsTime":result.setting_value, "requestData":'', "workerNameHead" : workerName, "assessorsName": assessorsName };
                            res.json(response);
                        }
                        
      
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "No record found for search date" };
                        res.json(response);
                    });

                }).catch(function(error) {
                    var response = { "status": 0, "message": "Falied to get assesssor name" };
                    res.json(response);
                });

            }).catch(function(error) {
                var response = { "status": 0, "message": "Falied to get worker name" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Falied to get slot period" };
            res.json(response);
        });
    }                                



}

module.exports = AssessorController;