var fs = require('fs');
var ClaimCategory = require('../models/ClaimCategory');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var ClaimCategoryController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            category_id: req.body.category_id,
            category_name: req.body.category_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        ClaimCategory.countClaimCategory(searchParams).then(function (result) {
            total = result[0].total;
            ClaimCategory.getClaimCategory(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "claimCategory": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var category_id = req.body.category_id;

        if (typeof category_id !== 'undefined' && category_id !== '') {
            ClaimCategory.singleClaimCategory(category_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "claimCategory": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var claimCategoryData = {
            category_name: req.body.category_name,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        ClaimCategory.addClaimCategory(claimCategoryData).then(function (result) {
            var response = { "status": 1, "message": "Claim Category created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Claim Category create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var category_id = req.body.category_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var claimCategoryData = {
            category_name: req.body.category_name,
            status: req.body.status,
            updated_at: currentDate
        };

        ClaimCategory.updateClaimCategory(claimCategoryData, category_id).then(function (result) {
            var response = { "status": 1, "message": "Claim Category updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Claim Category update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Claim Category!" };
        var category_id = req.body.category_id;

        ClaimCategory.deleteClaimCategory(category_id).then(function (result) {
            if(result == 0){
                var response = { status: 0, message: "You can't delete this category." };
            }else{
                response = { status: 1, message: "Your Claim Category has been deleted." };
            }  
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var category_id = req.body.category_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        ClaimCategory.statusClaimCategory(data, category_id).then(function(result) {
            if(result == 0){
                var response = { "status": 0, "message": "You can't disapprove this category." };
            }else{
                if(Status == 1){
                    var response = { "status": 1, "message": "Claim Category Approved" };                
                }else{
                    var response = { "status": 1, "message": "Claim Category Disapproved" };               
                }
            }             
            res.json(response);           
            
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = ClaimCategoryController;