var fs = require('fs');
var TasklistClient = require('../models/TasklistClient');
var commonHelper = require('../helper/common_helper.js');
var Client = require('../models/Client');
var Worker = require('../models/Worker');

var TasklistClientController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            tasklist_id: req.body.tasklist_id,
            tasklist_name: req.body.tasklist_name,
            tasklist_servicetype : req.body.tasklist_servicetype,
            status: req.body.status,
            created_at: req.body.created_at
        };

        TasklistClient.countTasklistClient(searchParams).then(function (result) {
            total = result[0].total;
            TasklistClient.getTasklistClient(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "tasklistClient": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var tasklist_id = req.body.tasklist_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var tasklistData = {
            tasklist_name: req.body.tasklist_name,
            tasklist_des: req.body.tasklist_des,
            tasklist_servicetype : req.body.tasklist_servicetype,
            tasklist_duration: req.body.tasklist_duration,
            // tasklist_cost: req.body.tasklist_cost,
            tasklist_type: req.body.tasklist_type,
            status: req.body.status,
            updated_at: currentDate
        };

        TasklistClient.updateTasklist(tasklistData, tasklist_id).then(function (result) {
            var response = { "status": 1, "message": "Client task updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Client task update failed!" };
            res.json(response);
        });
    },    

    actionView: function (req, res, next) {
        var tasklist_id = req.body.tasklist_id;

        if (typeof tasklist_id !== 'undefined' && tasklist_id !== '') {
            TasklistClient.singleTasklistClient(tasklist_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "tasklistClient": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this client task!" };
        var tasklist_id = req.body.tasklist_id;
        TasklistClient.deleteTasklistClient(tasklist_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your client task has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },    

    actionStatus: function(req, res, next) {
        var tasklist_id = req.body.tasklist_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        if(Status == 1){
            var msg = "Client Task Approved";         
        }else{
            var msg =  "Client Task Disapproved";                
        } 

        TasklistClient.statusTasklistClient(data, tasklist_id).then(function(result) {

            TasklistClient.singleTasklistClient(tasklist_id).then(function(resultC) {

                if(resultC.tasklist_client_id == 0) {

                    var response = { "status": 1, "message": msg };
                    res.json(response);

                } else {

                    Worker.getDeviceToken(resultC.tasklist_client_id).then(function(resultDT) {

                        var deviceToken = resultDT[0].device_token;
                        var deviceType  = resultDT[0].device_type;
                        var msgtitle    = msg;
                        var msgbody     = 'The task "'+ resultC.tasklist_name +'" has been approved, Book a Service Now!';

                        var notSendData = { 
                            user_id : resultC.tasklist_client_id, 
                            notification_type : msgtitle, 
                            notification_detail_id : tasklist_id, 
                            notification_msg : msgbody, 
                            created_at: currentDate, 
                            updated_at: currentDate 
                        };

                        Worker.addNotificationData(notSendData).then(function(resultAN) {

                            Worker.getNotResponse(resultAN).then(function(resultNR) {

                                Worker.notifyBadgeCount(resultC.tasklist_client_id).then(function(resultBC) {
                                    var badge = resultBC.length;

                                    if(deviceType == 2) {

                                        Client.sendNotificationClient(deviceToken,msgtitle,msgbody,badge,resultNR).then(function(resultCSN) {

                                            var response = { "status": 1, "message": msg };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": msg };
                                            res.json(response);
                                        });

                                    } else if(deviceType == 3) {

                                        Client.sendIOSNotification(deviceToken,msgtitle,msgbody,badge,resultNR).then(function(resultCSNI) {

                                            var response = { "status": 1, "message": msg };
                                            res.json(response);

                                        }).catch(function(error) {
                                            var response = { "status": 1, "message": msg };
                                            res.json(response);
                                        });

                                    } else {

                                        var response = { "status": 0, "message": "client not found" };
                                        res.json(response);
                                    }

                                }).catch(function(error) {
                                    var response = { "status": 1, "message": msg };
                                    res.json(response);
                                });

                            }).catch(function(error) {
                                var response = { "status": 1, "message": msg };
                                res.json(response);
                            });

                        }).catch(function(error) {
                            var response = { "status": 1, "message": msg };
                            res.json(response);
                        });


                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Try Again" };
                        res.json(response);
                    });

                }

            }).catch(function(error) {
                var response = { "status": 0, "message": "Try Again" };
                res.json(response);
            });

        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = TasklistClientController;