var fs = require('fs');
var md5 = require('md5');
var randomstring = require("randomstring");
var dateFormat = require('dateformat');
var nodemailer = require('nodemailer');

var Worker = require('../models/Worker');
var Client = require('../models/Client');
var Login = require('../models/Login');
var User = require('../models/User');

var commonHelper = require('../helper/common_helper.js');
var site_url = commonHelper.getSiteURL();

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: { 
        user: 'etpl18node@gmail.com', 
        pass: 'nodetest123' 
    }
});

var LoginController = {

    actionIndex: function (req, res, next) {
        var total = 0;

        var Username = req.body.username;
        var Password = md5(req.body.password);
        var LoginType = req.body.usertype;
        var device_token = req.body.device_token;
        var device_type = req.body.device_type;

        Login.checkLoginExists(Username).then(function (result) {
            total = result.length;
            if (total > 0) {
                if (result[0].status == 0) {
                	if(LoginType == 5){
                		var response = {
	                        "status": 0,
	                        "message": "Your account deactivated. Please check your email and click on the verification link."
	                    };
                	}else{
                		var response = {
	                        "status": 0,
	                        "message": "Your account is deactivated. Please contact to admin."
	                    };	
                	}                    
                    res.json(response);
                } else {
                    if (result[0].is_suspend == 1) {
                        var response = {
                            "status": 0,
                            "message": "Your account is suspend. Please contact to admin."
                        };
                        res.json(response);
                    } else {
                        Login.getLogin(Username, Password, LoginType, device_token, device_type).then(function (results) {

                            if (results == 0) {
                                if(LoginType == 3){
	                                var response = {
	                                    "status": 0,
	                                    "message": "Username and password do not match."
	                                };
	                            }else{
                            		var response = {
	                                    "status": 0,
	                                    "message": "Email address and password does not match."
	                                };
	                            }
                                res.json(response);
                            } else {
                                var userId =  results.user_id;
                                User.userDetails(userId, LoginType).then(function (resultS) {

									User.getImages().then(function (resultImage) {

										if(resultImage.length > 0) {

		                                	for (var k = 0; k < resultImage.length; k++) {
		                                		if(LoginType == 1) {
			                                        if(resultS.admin_image == resultImage[k].file_id) {
	                                                    resultS.admin_image = site_url + resultImage[k].file_base_url;
			                                        }
		                                    	} else if(LoginType == 2) {
			                                        if(resultS.cafeusers_image == resultImage[k].file_id) {
	                                                    resultS.cafeusers_image = site_url + resultImage[k].file_base_url;
			                                        }
		                                    	} else if(LoginType == 3) {
			                                        if(resultS.workers_image == resultImage[k].file_id) {
	                                                    resultS.workers_image = resultImage[k].file_base_url;
			                                        }
		                                    	} else if(LoginType == 4) {
			                                        if(resultS.assessors_image == resultImage[k].file_id) {
	                                                    resultS.assessors_image = site_url + resultImage[k].file_base_url;
			                                        }
		                                    	} else if(LoginType == 5) {
		                                    		if(resultS.clients_image == resultImage[k].file_id) {
	                                                    resultS.clients_image = resultImage[k].file_base_url;
			                                        }
		                                    	}
		                                    }

		                                    var response = {
		                                        "status": 1,
		                                        "message": "Logged in successfully.",
		                                        "data": resultS
		                                    };
		                                    res.json(response);

		                                } else {
		                                	var response = {
		                                        "status": 1,
		                                        "message": "Logged in successfully.",
		                                        "data": resultS
		                                    };
		                                    res.json(response);
		                                }

                                    }).catch(function (error) {
	                                    var response = {
	                                        "status": 0,
	                                        "message": "Image not found"
	                                    };
	                                    res.json(response);
	                                });

                                }).catch(function (error) {
                                    var response = {
                                        "status": 0,
                                        "message": "Add user failed"
                                    };
                                    res.json(response);
                                });

                            }
                        }).catch(function (error) {
                            var response = {
                                "status": 0,
                                "message": "Try again."
                            };
                            res.json(response);
                        });
                    }
                }

            } else {
                var response = {
                    "status": 0,
                    "message": "User does not exist."
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "Try again."
            };
            res.json(response);
        });
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();

        var Username = req.body.username;
        var Password = md5(req.body.password);
        var userRole = req.body.usertype;
        var socialID = req.body.social_id;
        var email    = req.body.email;
        var referID  = req.body.referID;
        var flag     = req.body.flag;

        var total = 0;
        var totals = 0;

        if(socialID !== ''){
            if(flag == 0){
				Login.checkUsersocial(Username, socialID).then(function (resultSocial) {                
			        totals = resultSocial.length;
			        // Register user when username and social not  exits
			        if (totals == 0) {
			            var register = {
			                user_username: req.body.username,
			                created_at: currentDate,
			                updated_at: currentDate,
			                user_role_id: userRole,
			                device_token: req.body.device_token,
			                device_type: req.body.device_type,
			                social_id: req.body.social_id,
			                user_registertype: req.body.registertype,
			                status: 1
			            };

			            var info = {
			                clients_name: req.body.name,
			                clients_email: req.body.email,
			                clients_phone: req.body.mobile,
			                clients_referID: randomstring.generate(6),
			                clients_image: req.body.file
			            };

			            Login.addUser(register, info, userRole).then(function (result) {

			                var transporter = nodemailer.createTransport({
			                    service: 'gmail',
			                    auth: {
			                        user: 'etpl18node@gmail.com',   
			                        pass: 'nodetest123'
			                    }
			                });

			                var mailOptions = {
			                    // template: 'mars',  
			                    to: email,
			                    subject: 'User Cafe Credentials',
			                    text: 'Username : '+ email +' and password : '+ Password +' '
			                };

			                transporter.sendMail(mailOptions, function(error, info){
			                    if (error) {
			                        var response = {
			                            "status": 0,
			                            "message": "E-mail Not Send"
			                        };
			                        res.json(response);
			                    } else {
			                        //Welcome Mail

			                        var userId =  result;
			                        User.userDetails(userId, userRole).then(function (resultS) {

			                            resultS.created_at = dateFormat(resultS.created_at, "yyyy-mm-dd hh:MM:ss");

			                            resultS.updated_at = dateFormat(resultS.updated_at, "yyyy-mm-dd hh:MM:ss");

			                            var response = {
			                                "status": 1,
			                                "message": "Login successfully.",
			                                "data": resultS
			                            };
			                            res.json(response);

			                        }).catch(function (error) {
			                            var response = {
			                                "status": 0,
			                                "message": "Add User failed"
			                            };
			                            res.json(response);
			                        });                               
			                    }
			                });
			                 
			            }).catch(function (error) {
			                var response = {
			                    "status": 0,
			                    "message": "Add User failed"
			                };
			                res.json(response);
			            });
			        }else{
			            if (resultSocial[0].status == 0) {
			                var response = {
			                    "status": 0,
			                    "message": "Your account deactivated."
			                };
			                res.json(response);
			            } else {
			                if (resultSocial[0].is_suspend == 1) {
			                    var response = {
			                        "status": 0,
			                        "message": "Your account suspend."
			                    };
			                    res.json(response);
			                } else {
			                    var updateData = {
			                        updated_at: currentDate,
			                        user_role_id: userRole,
			                        device_token: req.body.device_token                                
			                    };

			                    var userId =  resultSocial[0].user_id;

			                    Login.ChangePassword(updateData, userId).then(function (resultt) {
			                        // console.log(resultt);
			                        if(resultt){
			                            User.userDetails(userId, userRole).then(function (resultS) {
			                                resultS.created_at = dateFormat(resultS.created_at, "yyyy-mm-dd hh:MM:ss");

			                                resultS.updated_at = dateFormat(resultS.updated_at, "yyyy-mm-dd hh:MM:ss");

			                                var response = {
			                                    "status": 1,
			                                    "message": "Login successfully.",
			                                    "data": resultS
			                                };
			                                res.json(response);

			                            }).catch(function (error) {
			                                var response = {
			                                    "status": 0,
			                                    "message": "Add User failed"
			                                };
			                                res.json(response);
			                            });    
			                        }

			                    }).catch(function (error) {
			                        var response = {
			                            "status": 0,
			                            "message": "Add User failed"
			                        };
			                        res.json(response);
			                    });                            
			                                            
			                }
			            }
			        }       
			    }).catch(function (error) {
			        var response = {
			            "status": 0,
			            "message": "Add User failed"
			        };
			        res.json(response);
			    });              
	        }else{
        		// Flag 1
			    Login.checkUsersocialID(socialID).then(function (resultSocialID) {  
			        // console.log(resultSocialID);         
			        total = resultSocialID.length;
			        if (total == 0) {
			            Login.checkUserExists(Username).then(function (resultUser) {
			                totals = resultUser.length;
			                if (totals == 0) {
			                    var register = {
			                        user_username: req.body.username,
			                        created_at: currentDate,
			                        updated_at: currentDate,
			                        user_role_id: userRole,
			                        device_token: req.body.device_token,
			                        device_type: req.body.device_type,
			                        social_id: req.body.social_id,
			                        user_registertype: req.body.registertype,
			                        status: 0
			                    };

			                    var info = {
			                        clients_name: req.body.name,
			                        clients_email: req.body.email,
			                        clients_phone: req.body.mobile,
			                        clients_referID: randomstring.generate(6),
			                        clients_image: req.body.file
			                    };

			                    Login.addUser(register, info, userRole).then(function (result) {

			                        User.getTemplete('Registration').then(function(resultM) { 
										var URL = '';
										var html = resultM.email_content;
										html = html.replace('{{TempName}}', req.body.name);
										html = html.replace('{{TempEmailID}}', Username);
										html = html.replace('{{TempPassword}}', req.body.password);
										html = html.replace('{{TempURL}}', URL);
								
										var mailOptions = { 
											to: Username, 
											from: "Heavenly Sent <etpl18node@gmail.com>",
											subject: 'Login Details',  
											html: html 
										};
								
										transporter.sendMail(mailOptions, function(error, info){
											if (error) {
												var response = {
													"status": 0,
													"message": "Register successfully, but mail not send"
												};
												res.json(response);
											} else {
												var response = {
													"status": 0,
													"message": "Register successfully, please check your mail."
												};
												res.json(response);                               
											}                             
										});
								
									}).catch(function(error) {
										var response = {
											"status": 0,
											"message": "Register successfully, but mail not sSend"
										};
										res.json(response);
									}); 
			                         
			                    }).catch(function (error) {
			                        var response = {
			                            "status": 0,
			                            "message": "Add User failed"
			                        };
			                        res.json(response);
			                    });
			                } else {
			                    var response = {
			                        "status": 0,
			                        "message": "Email id already exist."
			                    };
			                    res.json(response);
			                }
			            }).catch(function (error) {
			                var response = {
			                    "status": 0,
			                    "message": "Try again."
			                };
			                res.json(response);
			            });
			            
			        }else{
			            if (resultSocialID[0].status == 0) {
			                var register = {
			                    user_username: req.body.username,
			                    created_at: currentDate,
			                    updated_at: currentDate,
			                    user_role_id: userRole,
			                    device_token: req.body.device_token,
			                    device_type: req.body.device_type,
			                    social_id: req.body.social_id,
			                    user_registertype: req.body.registertype,
			                    status: 0
			                };

			                var info = {
			                    clients_name: req.body.name,
			                    clients_email: req.body.email,
			                    clients_phone: req.body.mobile,
			                    clients_referID: randomstring.generate(6),
			                    clients_image: req.body.file
			                };

			                var userId =  resultSocialID[0].user_id;

			                Login.socialUpdated(register, info, userId).then(function (resultt) {
			                    if(resultt){
			                        User.getTemplete('Registration').then(function(resultM) { 
										var URL = '';
										var html = resultM.email_content;
										html = html.replace('{{TempName}}', req.body.name);
										html = html.replace('{{TempEmailID}}', Username);
										html = html.replace('{{TempPassword}}', req.body.password);
										html = html.replace('{{TempURL}}', URL);
								
										var mailOptions = { 
											to: Username, 
											from: "Heavenly Sent <etpl18node@gmail.com>",
											subject: 'Login Details',  
											html: html 
										};
								
										transporter.sendMail(mailOptions, function(error, info){
											if (error) {
												var response = {
													"status": 0,
													"message": "Register successfully, but mail not send"
												};
												res.json(response);
											} else {
												var response = {
													"status": 0,
													"message": "Register successfully, please check your mail."
												};
												res.json(response);                               
											}                             
										});
								
									}).catch(function(error) {
										var response = {
											"status": 0,
											"message": "Register successfully, but mail not sSend"
										};
										res.json(response);
									});  
			                    }

			                }).catch(function (error) {
			                    var response = {
			                        "status": 0,
			                        "message": "Add user failed"
			                    };
			                    res.json(response);
			                });
			                
			            } else {
			                if (resultSocialID[0].is_suspend == 1) {
			                    var response = {
			                        "status": 0,
			                        "message": "Your account suspend."
			                    };
			                    res.json(response);
			                } else {
			                    
			                    Login.checkUsersocial(Username, socialID).then(function (result) {
			                        totals = result.length;
			                        if(totals > 0){
			                            var userId =  resultSocialID[0].user_id;

			                            var updateData = {
			                                updated_at: currentDate,
			                                user_role_id: userRole,
			                                device_token: req.body.device_token                                
			                            };

			                            Login.ChangePassword(updateData, userId).then(function (resultt) {
			                                // console.log(resultt);
			                                if(resultt){
			                                    User.userDetails(userId, userRole).then(function (resultS) {
			                                        resultS.created_at = dateFormat(resultS.created_at, "yyyy-mm-dd hh:MM:ss");

			                                        resultS.updated_at = dateFormat(resultS.updated_at, "yyyy-mm-dd hh:MM:ss");

			                                        var response = {
			                                            "status": 1,
			                                            "message": "Login successfully.",
			                                            "data": resultS
			                                        };
			                                        res.json(response);

			                                    }).catch(function (error) {
			                                        var response = {
			                                            "status": 0,
			                                            "message": "Add user failed"
			                                        };
			                                        res.json(response);
			                                    });    
			                                }

			                            }).catch(function (error) {
			                                var response = {
			                                    "status": 0,
			                                    "message": "Add user failed"
			                                };
			                                res.json(response);
			                            });
			                        }else{
			                            var response = {
			                                "status": 0,
			                                "message": "Email id don't match."
			                            };
			                            res.json(response);
			                        }
			                    }).catch(function () {
			                        var response = {
			                            "status": 0,
			                            "message": "Add user failed"
			                        };
			                        res.json(response);
			                    });                                               
			                                            
			                }
			            }
			        }     
			    }).catch(function (error) {
			        var response = {
			            "status": 0,
			            "message": "Add User failed"
			        };
			        res.json(response);
			    });
	        }

        }else{            
            Login.checkUserExists(Username).then(function (resultUser) {
                total = resultUser.length;
                if (total == 0) {

					Login.checkMobileExists(req.body.mobile).then(function (resultMobile) {
						
						if(resultMobile[0][0].ClientD == 0 || resultMobile[1][0].workerD == 0 || resultMobile[2][0].cafeD == 0 || resultMobile[3][0].assD == 0) {

							var codeID = randomstring.generate(6);

							var register = {
								user_username: req.body.username,
								created_at: currentDate,
								updated_at: currentDate,
								user_password: Password,
								user_role_id: userRole,
								device_token: req.body.device_token,
								device_type: req.body.device_type,
								social_id: req.body.social_id,
								user_registertype: req.body.registertype,
								status: 0
							};

							var info = {
								clients_name: req.body.name,
								clients_email: req.body.email,
								clients_phone: req.body.mobile,
								refer_by_client : referID,
								clients_referID: codeID
							};

							if(referID !== '') {

								Login.checkReferCode(referID).then(function (resultR) {

									if(resultR.length > 0) {	                    		

										Login.addUser(register, info, userRole).then(function (result) {
											
											User.getTemplete('Registration').then(function(resultM) { 
												var URL = '';
												var html = resultM.email_content;
												html = html.replace('{{TempName}}', req.body.name);
												html = html.replace('{{TempEmailID}}', Username);
												html = html.replace('{{TempPassword}}', req.body.password);
												html = html.replace('{{TempURL}}', URL);

												var mailOptions = { 
													to: Username, 
													from: "Heavenly Sent <etpl18node@gmail.com>",
													subject: 'Login Details',  
													html: html 
												};

												transporter.sendMail(mailOptions, function(error, info){
													if (error) {
														var response = {
															"status": 0,
															"message": "Register successfully, but mail not send"
														};
														res.json(response);
													} else {
														var response = {
															"status": 0,
															"message": "Register successfully, please check your mail."
														};
														res.json(response);                               
													}                             
												});

											}).catch(function(error) {
												var response = {
													"status": 0,
													"message": "Register successfully, but mail not sSend"
												};
												res.json(response);
											}); 

										}).catch(function (error) {
											var response = {
												"status": 0,
												"message": "Add user failed"
											};
											res.json(response);
										});

									} else {
										var response = {
											"status": 0,
											"message": "Refer code is not exist. Please enter valid Refer code."
										};
										res.json(response);

									} 

								}).catch(function (error) {
									var response = {
										"status": 0,
										"message": "Not found"
									};
									res.json(response);
								});

							} else {

								Login.addUser(register, info, userRole).then(function (result) {

									User.getTemplete('Verification').then(function(resultM) { 
									var URL = '#!/activation-token/'+result+'/'+req.body.device_token+'/'+codeID+'/'+randomstring.generate(12);
										var html = resultM.email_content;
										html = html.replace('{{TempName}}', req.body.name);
										html = html.replace('{{TempEmailID}}', Username);
										html = html.replace('{{TempPassword}}', req.body.password);
										html = html.replace('{{TempURL}}', URL);

										var mailOptions = { 
											to: Username, 
											from: "Heavenly Sent <etpl18node@gmail.com>",
											subject: 'Login Details',  
											html: html 
										};

										transporter.sendMail(mailOptions, function(error, info){
											if (error) {
												var response = {
													"status": 0,
													"message": "Register successfully, but mail not send"
												};
												res.json(response);
											} else {
												var response = {
													"status": 0,
													"message": "Register successfully, please check your mail."
												};
												res.json(response);                               
											}                             
										});

									}).catch(function(error) {
										var response = {
											"status": 0,
											"message": "Register successfully, but mail not send"
										};
										res.json(response);
									}); 

								}).catch(function (error) {
									var response = {
										"status": 0,
										"message": "Add user failed"
									};
									res.json(response);
								});		                	
							}

						} else {
							var response = {
								"status": 0,
								"message": "Contact Number already exist."
							};
							res.json(response);
						}
					
					}).catch(function (error) {
						var response = {
							"status": 0,
							"message": "Try again."
						};
						res.json(response);
					});

                } else {
                    var response = {
                        "status": 0,
                        "message": "Email id already exist."
                    };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = {
                    "status": 0,
                    "message": "Try again."
                };
                res.json(response);
            });
        }        
    },

    //Change Password
    actionCreatePass: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
    
        var UserId = req.body.user_id;
        var NewPassword = md5(req.body.newpassword);
        var OldPassword = md5(req.body.oldpassword);

        var password = {
            user_password: NewPassword,
            updated_at: currentDate
        };

        var total = 0;

        Login.checkPassword(OldPassword, UserId).then(function (result) {
            total = result.length;
            if (total > 0) {
                // console.log(result);
                var Username = result[0].user_username;
                Login.ChangePassword(password, UserId).then(function (results) {
                	
                	User.getTemplete('Change Password').then(function(resultM) {
                		// console.log(resultM);
	                    var html = resultM.email_content;
	                    html = html.replace('{{TempName}}', 'Dear');
	                    html = html.replace('{{TempEmailID}}', Username);
	                    html = html.replace('{{TempPassword}}', req.body.newpassword);
	                    
	                    var mailOptions = { 
	                        to: Username, 
	                        from: "Heavenly Sent <etpl18node@gmail.com>",
	                        subject: 'Change Password',  
	                        html: html 
	                    };

	                    transporter.sendMail(mailOptions, function(error, info){
	                        if (error) {
						        var response = {
						            "status": 1,
						            "message": "Password Changed, But Mail Not Send"
						        };
						        res.json(response);
						    } else {
						        var response = {
			                        "status": 1,
			                        "message": "Password Changed, Please Check Your Mail."
			                    };
						        res.json(response);                               
						    }                             
	                    });
                    }).catch(function (error) {
		                var response = {
		                    "status": 1,
				            "message": "Password Changed, But Mail Not Send"
		                };
		                res.json(response);
		            });

                }).catch(function (error) {
                    var response = {
                        "status": 0,
                        "message": "Password change to failed"
                    };
                    res.json(response);
                });

            } else {
                var response = {
                    "status": 0,
                    "message": "Old password do not match."
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "Try again."
            };
            res.json(response);
        });
    },

    //Forget Password
    actionForget: function (req, res, next) {
    	var Email = req.body.email;
        var totals = 0;
        User.checkUserEmailExists(Email).then(function (result) {
            totals = result.length;
            // console.log(total);
            if (totals > 0) {
                if (result[0].status == 0) {
                    var response = {
                        "status": 0,
                        "message": "Your Account Deactivated."
                    };
                    res.json(response);
                } else {

                    if (result[0].is_suspend == 1) {
                        var response = {
                            "status": 0,
                            "message": "Your Account Suspend."
                        };
                        res.json(response);
                    } else {
                    	var UserId = result[0].user_id;
                    	var Username = result[0].user_username;
                    	var device_token = randomstring.generate(10);
		                Login.ChangeDeviceToken(device_token, UserId).then(function (results) {
		                	
		                	User.getTemplete('Forgot Password').then(function(resultM) {
		                		// console.log(resultM);
			                    var html = resultM.email_content;
			                    html = html.replace('{{TempName}}', 'Dear');
			                    html = html.replace('{{TempEmailID}}', Username);
			                    html = html.replace('{{TempURL}}', site_url+'/#!/forgot-password/'+UserId+'/'+device_token);
			                    
			                    var mailOptions = { 
			                        to: Username, 
			                        from: "Heavenly Sent <etpl18node@gmail.com>",
			                        subject: 'Forgot Password',  
			                        html: html 
			                    };

			                    transporter.sendMail(mailOptions, function(error, info){
			                        if (error) {
								        var response = {
								            "status": 0,
								            "message": "Forgot Password, But Mail Not Send. Please Try Again Later"
								        };
								        res.json(response);
								    } else {
								        var response = {
					                        "status": 1,
					                        "message": "Forgot Password, Please Check Your Mail. A reset password link has been sent to your email address."
					                    };
								        res.json(response);                               
								    }                             
			                    });
		                    }).catch(function (error) {
				                var response = {
				                    "status": 0,
						            "message": "Forgot Password, But Mail Not Send. Please Try Again Later"
				                };
				                res.json(response);
				            });

		                }).catch(function (error) {
		                    var response = {
		                        "status": 0,
		                        "message": "Forgot password to failed"
		                    };
		                    res.json(response);
		                });                                               
                    }
                }
            } else {
                var response = {
                    "status": 0,
                    "message": "Please enter valid mail address."
                };
                res.json(response);
            }
        });
    },

    actionValidateUser: function (req, res, next) {

        // var usertype = req.body.usertype;
        var token = req.body.token;

        Login.validateUser(token).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = {
                    "status": 1,
                    "message": "Record Found",
                    "data": result[0]
                };
                res.json(response);
            } else {
                var response = {
                    "status": 0,
                    "message": "No Record Exist"
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "No Record Exist"
            };
            res.json(response);
        });

    },

    actionLogout: function (req, res, next) {

        var usertype = req.body.usertype;
        var token = req.body.token;

        Login.logoutUser(usertype, token).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = {
                    "status": 1,
                    "message": "User Logout"
                };
                res.json(response);
            } else {
                var response = {
                    "status": 0,
                    "message": "No Record Exist"
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "No Record Exist"
            };
            res.json(response);
        });

    },

    actionLogoutApp: function (req, res, next) {

        var user_id = req.body.user_id;
        
        Login.logoutUserApp(user_id).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = {
                    "status": 1,
                    "message": "Logout successfully."
                };
                res.json(response);
            } else {
                var response = {
                    "status": 0,
                    "message": "No record exist"
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "No record exist"
            };
            res.json(response);
        });

    },

    actionCheckForgot: function (req, res, next) {
		var user_id = req.body.user_id;
        var device_token = req.body.device_token;

        Login.CheckForgot(user_id, device_token).then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = {
                    "status": 1,
                    "message": "Record Found."
                };
                res.json(response);
            } else {
                var response = {
                    "status": 0,
                    "message": "Link is expired."
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "No Record Exist."
            };
            res.json(response);
        });
    },

    actionSavePassword: function (req, res, next) {
    	var currentDate = commonHelper.getCurrentDateTime();
		
		var UserId = req.body.user_id;
        var device_token = req.body.device_token;
        var NewPassword = md5(req.body.user_password);
        
        var password = {
            user_password: NewPassword,
            updated_at: currentDate
        };

        Login.validateUser(device_token).then(function (results) {
        	
	        Login.ChangePassword(password, UserId).then(function (result) {
	        	if (typeof result != 'undefined' && result != '') {
	        		Login.logoutUserApp(UserId).then(function (result) {
			            
			        	var Username = results[0].user_username;
		        		User.getTemplete('Change Password').then(function(resultM) {
			        		var html = resultM.email_content;
			                html = html.replace('{{TempName}}', 'Dear');
			                html = html.replace('{{TempEmailID}}', Username);
			                html = html.replace('{{TempPassword}}', req.body.user_password);
			                
			                var mailOptions = { 
			                    to: Username, 
			                    from: "Heavenly Sent <etpl18node@gmail.com>",
			                    subject: 'Change Password',  
			                    html: html 
			                };

			                transporter.sendMail(mailOptions, function(error, info){
			                    if (error) {
							        var response = {
							            "status": 1,
							            "message": "Password Changed, But Mail Not Send"
							        };
							        res.json(response);
							    } else {
							        var response = {
				                        "status": 1,
				                        "message": "Password Changed, Please Check Your Mail."
				                    };
							        res.json(response);                               
							    }                             
			                });
			            }).catch(function (error) {
			                var response = {
			                    "status": 1,
					            "message": "Password Changed, But Mail Not Send"
			                };
			                res.json(response);
			            });

		            });
	            } else {
	                var response = {
	                    "status": 0,
	                    "message": "Token don't match."
	                };
	                res.json(response);
	            }

        	}).catch(function (error) {
	            var response = {
	                "status": 0,
	                "message": "Password change to failed"
	            };
	            res.json(response);
	        });

        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "Password change to failed"
            };
            res.json(response);
        });
    },

    actionActiveAccount: function (req, res, next) {
		var user_id = req.body.user_id;
        var device_token = req.body.device_token;
        var clients_referID = req.body.clients_referID;

        Login.activeAccount(user_id, device_token, clients_referID).then(function (result) {
        	if (typeof result !== 'undefined' && result !== '') {
            	if(result == 1){
            		var response = {
	                    "status": 1,
	                    "message": "Your account has been activated."
	                };
	                res.json(response);
            	}else{
            		var response = {
	                    "status": 0,
                    	"message": "Your token has ben expired."
	                };
	                res.json(response);
            	}       		
        	    
            } else {
                var response = {
                    "status": 0,
                    "message": "Your token has ben expired."
                };
                res.json(response);
            }
        }).catch(function (error) {
            var response = {
                "status": 0,
                "message": "No Record Exist."
            };
            res.json(response);
        });
    }
}

module.exports = LoginController;