var fs = require('fs');
var Suspension = require('../models/Suspension');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var SuspensionController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            category_id: req.body.category_id,
            category_type: req.body.category_type,
            created_at: req.body.created_at,
            status: req.body.status
        };

        Suspension.countData(searchParams).then(function (result) {
            total = result[0].total;
            Suspension.getData(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "category": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var category_id = req.body.category_id;

        if (typeof category_id !== 'undefined' && category_id !== '') {
            Suspension.singleData(category_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "category": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var addData = {
            category_id: req.body.category_id,
            category_type: req.body.category_type,
            category_time: req.body.category_time,
            category_period: req.body.category_period,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Suspension.addCategory(addData).then(function (result) {
            var response = { "status": 1, "message": "Category created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Failed to create!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var category_id = req.body.category_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var addData = {
            category_type: req.body.category_type,
            category_time: req.body.category_time,
            category_period: req.body.category_period,
            status: req.body.status,
            updated_at: currentDate
        };

        Suspension.updateCategory(addData, category_id).then(function (result) {
            var response = { "status": 1, "message": "Category updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "failed to update!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this category!" };
        var category_id = req.body.category_id;

        Suspension.deleteCategory(category_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your category has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var category_id = req.body.category_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Suspension.statusUpdate(data, category_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Suspension Category Approved" };                
            }else{
                var response = { "status": 1, "message": "Suspension Category Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = SuspensionController;