var fs = require('fs');
var Tutorial = require('../models/Tutorial');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var TutorialController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            tutorial_id: req.body.tutorial_id,
            tutorial_name: req.body.tutorial_name,
            status: req.body.status,
            role_id: req.body.role_id,
            created_at: req.body.created_at
        };

        Tutorial.countTutorial(searchParams).then(function (result) {
            total = result[0].total;
            Tutorial.getTutorial(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "tutorial": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var tutorial_id = req.body.tutorial_id;

        if (typeof tutorial_id !== 'undefined' && tutorial_id !== '') {
            Tutorial.singleTutorial(tutorial_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {

                    var response = { "status": 1, "message": "Record Found", "tutorial": result };
                    res.json(response);
                                          
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var tutorialData = {
            tutorial_type: req.body.tutorial_type,
            tutorial_name: req.body.tutorial_name,
            tutorial_des: req.body.tutorial_des,            
            role_id: req.body.role_id,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Tutorial.addTutorial(tutorialData).then(function (result) {
            var response = { "status": 1, "message": "Tutorial created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Tutorial create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var tutorial_id = req.body.tutorial_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var tutorialData = {
            tutorial_name: req.body.tutorial_name,
            tutorial_des: req.body.tutorial_des,
            role_id: req.body.role_id,
            status: req.body.status,
            updated_at: currentDate
        };

        Tutorial.updateTutorial(tutorialData, tutorial_id).then(function (result) {
            var response = { "status": 1, "message": "Tutorial updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Tutorial update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this tutorial!" };
        var tutorial_id = req.body.tutorial_id;

        Tutorial.deleteTutorial(tutorial_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your tutorial has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var tutorial_id = req.body.tutorial_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        Tutorial.statusTutorial(data, tutorial_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Tutorial Actived" };                
            }else{
                var response = { "status": 1, "message": "Tutorial Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    },


    actionGetImageData: function(req, res, next) {
        var fileId = req.body.fileId;
        
        Tutorial.getImageUrl(fileId).then(function (resultI) {

            var image = resultI[0].file_base_url;
            var response = { "status": 1, "message": "Record Found", "imageUrl": image };
            res.json(response);

        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }); 
    },    

    //API
    actionAllTutorial: function (req, res, next) {
        var role_id = req.body.role_id; 
        var total = 0;

        Tutorial.getAllTutorial(role_id).then(function (result) {
            total = result.length;
            // console.log(result);
            result.forEachAsync(function(element, index, arr) {
                element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
                element.updated_at = dateFormat(element.updated_at, "yyyy-mm-dd hh:MM:ss");
               
            }, function() {
                if(total > 0){
                    var response = { "status": 1, "message": "Record Found", "tutorial": result, "total": total };
                    res.json(response);
                }else{
                    var response = { "status": 1, "message": "No Record Found", "tutorial": '', "total": 0 };
                    res.json(response);
                }                
            });
            
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }

}

module.exports = TutorialController;