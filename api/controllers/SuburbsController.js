var fs = require('fs');
var Suburbs = require('../models/Suburbs');
var commonHelper = require('../helper/common_helper.js');

var SuburbsController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            suburbs_id: req.body.suburbs_id,
            suburbs_pick_name: req.body.suburbs_pick_name,
            suburbs_drop_name: req.body.suburbs_drop_name,
            status: req.body.status,
            ride_status: req.body.ride_status,
            created_at: req.body.created_at
        };

        Suburbs.countSuburbs(searchParams).then(function (result) {
            total = result[0].total;
            Suburbs.getSuburbs(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "suburbs": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var suburbs_id = req.body.suburbs_id;

        if (typeof suburbs_id !== 'undefined' && suburbs_id !== '') {
            Suburbs.singleSuburbs(suburbs_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "suburbs": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var suburbsData = {
            cafe_user_id: req.body.cafe_user_id,
            worker_user_id: req.body.worker_user_id,
            suburbs_pick_name: req.body.suburbs_pick_name,
            suburbs_pick_address: req.body.suburbs_pick_address,
            suburbs_pick_lat: req.body.suburbs_pick_lat,
            suburbs_pick_long: req.body.suburbs_pick_long,
            suburbs_drop_name: req.body.suburbs_drop_name,
            suburbs_drop_address: req.body.suburbs_drop_address,
            suburbs_drop_lat: req.body.suburbs_drop_lat,
            suburbs_drop_long: req.body.suburbs_drop_long,
            status: req.body.status,
            ride_status: req.body.ride_status,
            created_at: currentDate,
            updated_at: currentDate
        };

        Suburbs.addSuburbs(suburbsData).then(function (result) {
            var response = { "status": 1, "message": "FAQ created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "FAQ create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var faq_id = req.body.faq_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var faqData = {
            faq_question: req.body.faq_question,
            faq_answer: req.body.faq_answer,
            status: req.body.status,
            updated_at: currentDate
        };

        Faq.updateFaq(faqData, faq_id).then(function (result) {
            var response = { "status": 1, "message": "FAQ updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "FAQ update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this Suburbs!" };
        var suburbs_id = req.body.suburbs_id;

        Suburbs.deleteSuburbs(suburbs_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your Suburbs record has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var suburbs_id = req.body.suburbs_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status, ride_status: req.body.ride_status };

        Suburbs.statusSuburbs(data, suburbs_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Account Actived" };                
            }else{
                var response = { "status": 1, "message": "Account Deactived" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = SuburbsController;