﻿var fs = require('fs');
var CleaningArea = require('../models/CleaningArea');
var commonHelper = require('../helper/common_helper.js');

var CleaningAreaController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            area_id: req.body.area_id,
            area_name: req.body.area_name,
            status: req.body.status,
            created_at: req.body.created_at
        };

        CleaningArea.countCleaningArea(searchParams).then(function (result) {
            total = result[0].total;
            CleaningArea.getCleaningArea(limit, offset, searchParams, orderParams).then(function (result) {
                var response = { "status": 1, "message": "Record Found", "cleaningArea": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var area_id = req.body.area_id;

        if (typeof area_id !== 'undefined' && area_id !== '') {
            CleaningArea.singleCleaningArea(area_id).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "cleaningArea": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionCreate: function (req, res, next) {
        var currentDate = commonHelper.getCurrentDateTime();
        var cleaningAreaData = {
            area_name: req.body.area_name,
            area_clean_duration: req.body.area_clean_duration,
            //area_clean_cost: req.bod​y.area_clean_cost,
            status: req.body.status,
            created_at: currentDate,
            updated_at: currentDate
        };

        CleaningArea.addCleaningArea(cleaningAreaData).then(function (result) {
            var response = { "status": 1, "message": "Cleaning area created successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Cleaning area create failed!" };
            res.json(response);
        });
    },

    actionUpdate: function (req, res, next) {
        var area_id = req.body.area_id;
        var currentDate = commonHelper.getCurrentDateTime();
        var cleaningAreaData = {
            area_name: req.body.area_name,
            area_clean_duration: req.body.area_clean_duration,
            //area_clean_cost: req.body.area_clean_cost,
            status: req.body.status,
            updated_at: currentDate
        };

        CleaningArea.updateCleaningArea(cleaningAreaData, area_id).then(function (result) {
            var response = { "status": 1, "message": "Cleaning area updated successfully!" };
            res.json(response);
        }).catch(function (error) {
            var response = { "status": 0, "message": "Cleaning area update failed!" };
            res.json(response);
        });
    },

    actionDelete: function (req, res, next) {
        var response = { status: 0, message: "Unable to delete this cleaning area!" };
        var area_id = req.body.area_id;
        CleaningArea.deleteCleaningArea(area_id).then(function (result) {
            if (result) {
                response = { status: 1, message: "Your cleaning area has been deleted." };
            }
            res.json(response);
        }).catch(function (error) {
            res.json(response);
        });
    },

    actionStatus: function(req, res, next) {
        var area_id = req.body.area_id;
        var Status = req.body.status;
        var currentDate = commonHelper.getCurrentDateTime();
        var data = { status: req.body.status };

        CleaningArea.statusCleaningArea(data, area_id).then(function(result) {
            if(Status == 1){
                var response = { "status": 1, "message": "Cleaning Area Approved" };                
            }else{
                var response = { "status": 1, "message": "Cleaning Area Disapproved" };                
            } 
            res.json(response);           
        }).catch(function(error) {
            var response = { "status": 0, "message": "Try Again" };
            res.json(response);
        });
    }

}

module.exports = CleaningAreaController;
