var fs = require('fs');
var Website = require('../models/Website');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');

var WebsiteController = {

    actionIndex: function (req, res, next) {
        Website.allTypeData().then(function (result) {
            if (typeof result != 'undefined' && result != '') {
                var response = { "status": 1, "message": "Record Found", "setting": result[0], "social": result[1], "email": result[2], "banner": result[3] };
                res.json(response);
            } else {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            }
        }).catch(function (error) {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var page_slug = req.body.page_slug;

        if (typeof page_slug !== 'undefined' && page_slug !== '') {
            Website.singleWeb(page_slug).then(function (result) {
                if (typeof result != 'undefined' && result != '') {
                    var response = { "status": 1, "message": "Record Found", "page": result };
                    res.json(response);
                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }
            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });
        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    }  

    //API
    // actionAllFAQs: function (req, res, next) {
    //     var role_id = req.body.role_id; 
    //     var total = 0;

    //     Faq.getAllFaqs(role_id).then(function (result) {
    //         total = result.length;
    //         // console.log(total);
    //         result.forEachAsync(function(element, index, arr) {
    //             element.created_at = dateFormat(element.created_at, "yyyy-mm-dd hh:MM:ss");
    //             element.updated_at = dateFormat(element.updated_at, "yyyy-mm-dd hh:MM:ss");
               
    //         }, function() {
    //             if(total > 0){
    //                 var response = { "status": 1, "message": "Record Found", "faqs": result, "total": total };
    //                 res.json(response);
    //             }else{
    //                 var response = { "status": 1, "message": "No Record Found", "faqs": '', "total": 0 };
    //                 res.json(response);
    //             }                
    //         });
            
    //     }).catch(function (error) {
    //         var response = { "status": 0, "message": "No Record Exist" };
    //         res.json(response);
    //     });
    // }

}

module.exports = WebsiteController;