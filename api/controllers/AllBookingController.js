var fs = require('fs');
var AllBooking = require('../models/AllBooking');
var Payment = require('../models/Payment');
var commonHelper = require('../helper/common_helper.js');
var Array = require('node-array');
var dateFormat = require('dateformat');
// var site_url     = 'http://ec2-18-221-244-173.us-east-2.compute.amazonaws.com/heaven';
var site_url = 'http://localhost/heaven';

var AllBookingController = {

    actionIndex: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            booking_id: req.body.booking_id,
            clients_name: req.body.clients_name,
            workers_name: req.body.workers_name,
            booking_date: req.body.booking_date
         };

        AllBooking.countBookings(searchParams).then(function (resultO) {

            total = resultO[0].total;
                
                AllBooking.getBookings(limit, offset, searchParams, orderParams).then(function (result) {

                    var response = { "status": 1, "message": "Record Found", "allBooking": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                    res.json(response);

                }).catch(function (error) {

                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                });

        }).catch(function (error) {

            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionView: function (req, res, next) {
        var booking_id = req.body.booking_id;

        if (typeof booking_id !== 'undefined' && booking_id !== '') {

            AllBooking.singleBooking(booking_id).then(function (result) {

                if (typeof result != 'undefined' && result != '') {

                    Payment.getImages().then(function(resultImage) {
                                
                        if(resultImage.length > 0) {
                            
                            for (var k = 0; k < resultImage.length; k++) {

                                if(result.clients_image == resultImage[k].file_id) {
                                    result.clients_image = resultImage[k].file_base_url;
                                }
                                if(result.workers_image == resultImage[k].file_id) {
                                    result.workers_image = resultImage[k].file_base_url;
                                }
                            }
                                   
                                var response = { "status": 1, "message": "Record Found", "allBooking": result };
                                res.json(response);
                                    
                        } else {
                            var response = { "status": 1, "message": "Record Found", "allBooking": result };
                            res.json(response);
                        }   
                                       
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not Found" };
                        res.json(response);
                    });

                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionOnceOffCSV: function (req, res, next) {
        
        AllBooking.getBookingsOnceOffCSV().then(function (result) {

            if(result.length > 0) {

                AllBooking.getAmenities().then(function (resultA) {

                    for(var i=0; i<result.length; i++) {

                        for(var k=0; k<resultA.length; k++) {

                            if(result[i].booking_bedrooms == resultA[k].amenities_id) {
                                result[i].booking_bedrooms = resultA[k].amenities_count;
                            }
                            if(result[i].booking_bathrooms == resultA[k].amenities_id) {
                                result[i].booking_bathrooms = resultA[k].amenities_count;
                            }
                            if(result[i].booking_baskets == resultA[k].amenities_id) {
                                result[i].booking_baskets = resultA[k].amenities_count;
                            }
                        }
                        result[i].booking_date = dateFormat(result[i].booking_date, "yyyy-mm-dd");  
                    }

                    var response = { "status": 1, "message": "Record Found", "onceCSV": result};
                    res.json(response);

                }).catch(function (error) {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "No data found"};
                res.json(response);
            }

        }).catch(function (error) {

            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },    

    actionIndexWeek: function (req, res, next) {
        var limit = 10;
        var total = 0;
        var page = req.body.pageNum || 1;
        var offset = (limit * page) - limit;

        var orderParams;
        if (typeof req.body.orderColumn != 'undefined' && req.body.orderColumn != '' && typeof req.body.orderBy != 'undefined' && req.body.orderBy != '') {
            orderParams = {
                orderColumn: req.body.orderColumn,
                orderBy: req.body.orderBy
            };
        }

        var searchParams = {
            bookweekly_id: req.body.bookweekly_id,
            clients_name: req.body.clients_name,
            workers_name: req.body.workers_name,
            booking_date: req.body.booking_date
         };
         
        AllBooking.countBookingsWeek(searchParams).then(function (resultO) {

            total = resultO[0].total;
                
            AllBooking.getBookingsWeek(limit, offset, searchParams, orderParams).then(function (result) {

                var response = { "status": 1, "message": "Record Found", "allBookingWeek": result, "current": page, "pages": Math.ceil(total / limit), "total": total };
                res.json(response);

            }).catch(function (error) {

                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        }).catch(function (error) {

            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    },

    actionWeeklyView: function (req, res, next) {
        var bookweekly_id = req.body.bookweekly_id;

        if (typeof bookweekly_id !== 'undefined' && bookweekly_id !== '') {

            AllBooking.singleBookingWeek(bookweekly_id).then(function (result) {

                if (typeof result != 'undefined' && result != '') {

                    Payment.getImages().then(function(resultImage) {
                                
                        if(resultImage.length > 0) {
                            
                            for (var k = 0; k < resultImage.length; k++) {

                                if(result.clients_image == resultImage[k].file_id) {
                                    result.clients_image = resultImage[k].file_base_url;
                                }
                                if(result.workers_image == resultImage[k].file_id) {
                                    result.workers_image = resultImage[k].file_base_url;
                                }
                            }
                                  
                                var response = { "status": 1, "message": "Record Found", "allBookingWeek": result };
                                res.json(response);
                                    
                        } else {
                            var response = { "status": 1, "message": "Record Found", "allBookingWeek": result };
                            res.json(response);
                        }   
                                       
                    }).catch(function(error) {
                        var response = { "status": 0, "message": "Not Found" };
                        res.json(response);
                    });

                } else {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                }

            }).catch(function (error) {
                var response = { "status": 0, "message": "No Record Exist" };
                res.json(response);
            });

        } else {
            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        }
    },

    actionWeeklyCSV: function (req, res, next) {
        
        AllBooking.getBookingsWeeklyCSV().then(function (result) {

            if(result.length > 0) {

                AllBooking.getAmenities().then(function (resultA) {

                    for(var i=0; i<result.length; i++) {

                        for(var k=0; k<resultA.length; k++) {

                            if(result[i].booking_bedrooms == resultA[k].amenities_id) {
                                result[i].booking_bedrooms = resultA[k].amenities_count;
                            }
                            if(result[i].booking_bathrooms == resultA[k].amenities_id) {
                                result[i].booking_bathrooms = resultA[k].amenities_count;
                            }
                            if(result[i].booking_baskets == resultA[k].amenities_id) {
                                result[i].booking_baskets = resultA[k].amenities_count;
                            }
                        }
                        result[i].booking_date = dateFormat(result[i].booking_date, "yyyy-mm-dd");  
                    }

                    var response = { "status": 1, "message": "Record Found", "weeklyCSV": result};
                    res.json(response);

                }).catch(function (error) {
                    var response = { "status": 0, "message": "No Record Exist" };
                    res.json(response);
                });

            } else {
                var response = { "status": 0, "message": "No data found"};
                res.json(response);
            }

        }).catch(function (error) {

            var response = { "status": 0, "message": "No Record Exist" };
            res.json(response);
        });
    }
}

module.exports = AllBookingController;