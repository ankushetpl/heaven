var express = require('express');
var router = express.Router();
var path = require('path');
var ModulesController = require('../../controllers/ModulesController');

router.post('/', ModulesController.actionIndex);

router.post('/add', ModulesController.actionCreate);

router.post('/edit', ModulesController.actionUpdate);

router.post('/view', ModulesController.actionView);

router.post('/status', ModulesController.actionStatus);

router.post('/delete', ModulesController.actionDelete);

router.post('/AllList', ModulesController.actionAllList);

router.post('/parent', ModulesController.actionParent);

router.post('/check', ModulesController.actionCheck);

router.post('/all', ModulesController.actionAll);

module.exports = router;