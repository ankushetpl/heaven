var express = require('express');
var router = express.Router();
var TodayBookingController = require('../../controllers/TodayBookingController');

router.post('/', TodayBookingController.actionIndex);
router.post('/view', TodayBookingController.actionView);

module.exports = router;