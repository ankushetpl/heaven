var express = require('express');
var router = express.Router();
var BuyCreditController = require('../../controllers/BuyCreditController');

router.post('/', BuyCreditController.actionIndex);
router.post('/add', BuyCreditController.actionCreate);
router.post('/edit', BuyCreditController.actionUpdate);
router.post('/view', BuyCreditController.actionView);
router.post('/delete', BuyCreditController.actionDelete);
router.post('/status', BuyCreditController.actionStatus);

module.exports = router;