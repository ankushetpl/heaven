var express = require('express');
var router = express.Router();
var path = require('path');
var SkillsController = require('../../controllers/SkillsController');

router.post('/', SkillsController.actionIndex);

router.post('/add', SkillsController.actionCreate);

router.post('/edit', SkillsController.actionUpdate);

router.post('/view', SkillsController.actionView);

router.post('/status', SkillsController.actionStatus);

router.post('/delete', SkillsController.actionDelete);

router.post('/AllList', SkillsController.actionAllList);

router.post('/parent', SkillsController.actionParent);

router.post('/checkData', SkillsController.actionCheckData);

module.exports = router;