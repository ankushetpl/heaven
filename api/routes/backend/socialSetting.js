var express = require('express');
var router = express.Router();
var SocialController = require('../../controllers/SocialController');

router.post('/', SocialController.actionIndex);

router.post('/add', SocialController.actionCreate);

router.post('/edit', SocialController.actionUpdate);

router.post('/view', SocialController.actionView);

router.post('/delete', SocialController.actionDelete);

router.post('/status', SocialController.actionStatus);

router.post('/default', SocialController.actionDefault);

router.post('/allSocial', SocialController.actionAllSocial);

module.exports = router;