var express = require('express');
var router = express.Router();
var NotificationController = require('../../controllers/NotificationController');

router.post('/add', NotificationController.actionCreate);

module.exports = router;