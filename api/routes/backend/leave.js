var express = require('express');
var router = express.Router();
var LeaveController = require('../../controllers/LeaveController');

router.post('/', LeaveController.actionIndex);
router.post('/add', LeaveController.actionCreate);
router.post('/edit', LeaveController.actionUpdate);
router.post('/view', LeaveController.actionView);
router.post('/delete', LeaveController.actionDelete);
router.post('/status', LeaveController.actionStatus);

module.exports = router;