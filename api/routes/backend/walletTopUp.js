var express = require('express');
var router = express.Router();
var WalletTopUpController = require('../../controllers/WalletTopUpController');

router.post('/', WalletTopUpController.actionIndex);
router.post('/topUp', WalletTopUpController.actionTopUp);
router.post('/view', WalletTopUpController.actionView);

module.exports = router;