var express = require('express');
var router = express.Router();
var TasklistController = require('../../controllers/TasklistController');

router.post('/', TasklistController.actionIndex);
router.post('/add', TasklistController.actionCreate);
router.post('/edit', TasklistController.actionUpdate);
router.post('/view', TasklistController.actionView);
router.post('/delete', TasklistController.actionDelete);
router.post('/status', TasklistController.actionStatus);

module.exports = router;