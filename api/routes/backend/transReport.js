var express = require('express');
var router = express.Router();
var TransReportController = require('../../controllers/TransReportController');

router.post('/', TransReportController.actionIndex);

router.post('/view', TransReportController.actionView);

router.post('/allTranxData', TransReportController.actionAllTranxData);

module.exports = router;