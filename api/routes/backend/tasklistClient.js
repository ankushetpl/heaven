var express = require('express');
var router = express.Router();
var TasklistClientController = require('../../controllers/TasklistClientController');

router.post('/', TasklistClientController.actionIndex);
router.post('/edit', TasklistClientController.actionUpdate);
router.post('/view', TasklistClientController.actionView);
router.post('/status', TasklistClientController.actionStatus);
router.post('/delete', TasklistClientController.actionDelete);

module.exports = router;