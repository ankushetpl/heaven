var express = require('express');
var router = express.Router();
var WeekServiceController = require('../../controllers/WeekServiceController');

router.post('/', WeekServiceController.actionIndex);

router.post('/add', WeekServiceController.actionCreate);

router.post('/edit', WeekServiceController.actionUpdate);

router.post('/view', WeekServiceController.actionView);

router.post('/delete', WeekServiceController.actionDelete);

router.post('/status', WeekServiceController.actionStatus);

//API
router.post('/allWeekService', WeekServiceController.actionAllWeekService);

module.exports = router;