var express = require('express');
var router = express.Router();
var PaymentPendingController = require('../../controllers/PaymentPendingController');

router.post('/', PaymentPendingController.actionIndex);
router.post('/view', PaymentPendingController.actionView);
router.post('/delete', PaymentPendingController.actionDelete);
router.post('/pendingOnceCSV', PaymentPendingController.actionPendingOnceCSV);

router.post('/weekly', PaymentPendingController.actionIndexWeekly);
router.post('/weeklyView', PaymentPendingController.actionWeeklyView);
router.post('/weeklyDelete', PaymentPendingController.actionweeklyDelete);
router.post('/pendingWeeklyCSV', PaymentPendingController.actionPendingWeeklyCSV);

module.exports = router;