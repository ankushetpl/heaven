var express = require('express');
var router = express.Router();
var SuspensionController = require('../../controllers/SuspensionController');

router.post('/', SuspensionController.actionIndex);

router.post('/add', SuspensionController.actionCreate);

router.post('/edit', SuspensionController.actionUpdate);

router.post('/view', SuspensionController.actionView);

router.post('/delete', SuspensionController.actionDelete);

router.post('/status', SuspensionController.actionStatus);


module.exports = router;