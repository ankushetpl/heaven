var express = require('express');
var router = express.Router();
var SupportController = require('../../controllers/SupportController');

router.post('/', SupportController.actionIndex);

router.post('/add', SupportController.actionCreate);

router.post('/edit', SupportController.actionUpdate);

router.post('/view', SupportController.actionView);

module.exports = router;