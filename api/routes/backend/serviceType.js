var express = require('express');
var router = express.Router();
var ServiceTypeController = require('../../controllers/ServiceTypeController');

router.post('/', ServiceTypeController.actionIndex);

router.post('/add', ServiceTypeController.actionCreate);

router.post('/edit', ServiceTypeController.actionUpdate);

router.post('/view', ServiceTypeController.actionView);

router.post('/delete', ServiceTypeController.actionDelete);

router.post('/status', ServiceTypeController.actionStatus);

router.post('/checkBooking', ServiceTypeController.actionCheckBooking);

module.exports = router;