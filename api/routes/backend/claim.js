var express = require('express');
var router = express.Router();
var ClaimController = require('../../controllers/ClaimController');

router.post('/', ClaimController.actionIndex);

router.post('/edit', ClaimController.actionUpdate);

router.post('/view', ClaimController.actionView);

router.post('/delete', ClaimController.actionDelete);

router.post('/status', ClaimController.actionStatus);

router.post('/allCategory', ClaimController.actionAllCategory);

module.exports = router;