var express = require('express');
var router = express.Router();
var paymentRefundController = require('../../controllers/paymentRefundController');

router.post('/', paymentRefundController.actionIndex);
router.post('/week', paymentRefundController.actionIndexWeek);
router.post('/refund', paymentRefundController.actionRefund);
router.post('/view', paymentRefundController.actionView);

module.exports = router;