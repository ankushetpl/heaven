var express = require('express');
var router = express.Router();
var EmailController = require('../../controllers/EmailController');

router.post('/', EmailController.actionIndex);

router.post('/add', EmailController.actionCreate);

router.post('/edit', EmailController.actionUpdate);

router.post('/view', EmailController.actionView);

router.post('/delete', EmailController.actionDelete);

router.post('/status', EmailController.actionStatus);

router.post('/default', EmailController.actionDefault);

router.post('/emailtypes', EmailController.actionEmailtypes);

module.exports = router;