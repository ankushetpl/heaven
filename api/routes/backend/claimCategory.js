var express = require('express');
var router = express.Router();
var ClaimCategoryController = require('../../controllers/ClaimCategoryController');

router.post('/', ClaimCategoryController.actionIndex);
router.post('/add', ClaimCategoryController.actionCreate);
router.post('/edit', ClaimCategoryController.actionUpdate);
router.post('/view', ClaimCategoryController.actionView);
router.post('/delete', ClaimCategoryController.actionDelete);
router.post('/status', ClaimCategoryController.actionStatus);

module.exports = router;