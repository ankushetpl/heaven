var express = require('express');
var router = express.Router();
var LeaveManageController = require('../../controllers/LeaveManageController');

router.post('/', LeaveManageController.actionIndex);

router.post('/view', LeaveManageController.actionView);

router.post('/allLeaveType', LeaveManageController.actionAllLeaveType);

module.exports = router;