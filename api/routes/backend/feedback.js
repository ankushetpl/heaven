var express = require('express');
var router = express.Router();
var FeedbackController = require('../../controllers/FeedbackController');

router.post('/', FeedbackController.actionIndex);
router.post('/view', FeedbackController.actionView);
router.post('/delete', FeedbackController.actionDelete);
router.post('/deleteM', FeedbackController.actionDeleteM);

//API
router.post('/allfeedback', FeedbackController.actionAllFeedBack);
router.post('/add', FeedbackController.actionCreate);

module.exports = router;