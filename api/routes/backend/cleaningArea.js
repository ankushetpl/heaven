var express = require('express');
var router = express.Router();
var CleaningAreaController = require('../../controllers/CleaningAreaController');

router.post('/', CleaningAreaController.actionIndex);

router.post('/add', CleaningAreaController.actionCreate);

router.post('/edit', CleaningAreaController.actionUpdate);

router.post('/view', CleaningAreaController.actionView);

router.post('/delete', CleaningAreaController.actionDelete);

router.post('/status', CleaningAreaController.actionStatus);

module.exports = router;