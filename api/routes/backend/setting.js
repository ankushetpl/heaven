var express = require('express');
var router = express.Router();
var SettingController = require('../../controllers/SettingController');

router.post('/', SettingController.actionIndex);

router.post('/edit', SettingController.actionUpdate);

module.exports = router;