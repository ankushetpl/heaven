var express = require('express');
var router = express.Router();
var TutorialController = require('../../controllers/TutorialController');

router.post('/', TutorialController.actionIndex);
router.post('/add', TutorialController.actionCreate);
router.post('/edit', TutorialController.actionUpdate);
router.post('/view', TutorialController.actionView);
router.post('/delete', TutorialController.actionDelete);
router.post('/status', TutorialController.actionStatus);
router.post('/getImageData', TutorialController.actionGetImageData);

//API
router.post('/alltutorial', TutorialController.actionAllTutorial);

module.exports = router;