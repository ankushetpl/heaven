var express = require('express');
var router = express.Router();
var PromocodeController = require('../../controllers/PromocodeController');

router.post('/', PromocodeController.actionIndex);

router.post('/add', PromocodeController.actionCreate);

router.post('/edit', PromocodeController.actionUpdate);

router.post('/view', PromocodeController.actionView);

router.post('/delete', PromocodeController.actionDelete);

router.post('/status', PromocodeController.actionStatus);

module.exports = router;