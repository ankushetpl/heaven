var express = require('express');
var router = express.Router();
var AllBookingController = require('../../controllers/AllBookingController');

router.post('/', AllBookingController.actionIndex);

router.post('/view', AllBookingController.actionView);

router.post('/onceCSV', AllBookingController.actionOnceOffCSV);

router.post('/weekly', AllBookingController.actionIndexWeek);

router.post('/weeklyview', AllBookingController.actionWeeklyView);

router.post('/weeklyCSV', AllBookingController.actionWeeklyCSV);

module.exports = router;