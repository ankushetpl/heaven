var express = require('express');
var router = express.Router();
var EarlyPayController = require('../../controllers/EarlyPayController');

router.post('/', EarlyPayController.actionIndex);

router.post('/status', EarlyPayController.actionStatus);

router.post('/not', EarlyPayController.actionNot);

module.exports = router;