var express = require('express');
var router = express.Router();
var DashboardController = require('../../controllers/DashboardController');

router.post('/count', DashboardController.actionCount);
router.post('/notification', DashboardController.actionNotification);

module.exports = router;