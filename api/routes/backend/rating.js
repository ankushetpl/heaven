var express = require('express');
var router = express.Router();
var RatingController = require('../../controllers/RatingController');

router.post('/', RatingController.actionIndex);

router.post('/add', RatingController.actionCreate);

router.post('/edit', RatingController.actionUpdate);

router.post('/view', RatingController.actionView);

router.post('/delete', RatingController.actionDelete);

router.post('/status', RatingController.actionStatus);

//API
router.post('/allTypes', RatingController.actionAllTypes);

module.exports = router;