var express = require('express');
var router = express.Router();
var AmenitiesController = require('../../controllers/AmenitiesController');

router.post('/', AmenitiesController.actionIndex);
router.post('/add', AmenitiesController.actionCreate);
router.post('/edit', AmenitiesController.actionUpdate);
router.post('/view', AmenitiesController.actionView);
router.post('/delete', AmenitiesController.actionDelete);
router.post('/status', AmenitiesController.actionStatus);

module.exports = router;