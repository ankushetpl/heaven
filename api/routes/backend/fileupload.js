var express = require('express');
var router = express.Router();
var FileUploadController = require('../../controllers/FileUploadController');

router.post('/', FileUploadController.actionIndex);

router.post('/add', FileUploadController.actionCreate);

router.post('/view', FileUploadController.actionView);

module.exports = router;