var express = require('express');
var router = express.Router();
var FaqController = require('../../controllers/FaqController');

router.post('/', FaqController.actionIndex);

router.post('/add', FaqController.actionCreate);

router.post('/edit', FaqController.actionUpdate);

router.post('/view', FaqController.actionView);

router.post('/delete', FaqController.actionDelete);

router.post('/status', FaqController.actionStatus);

//API
router.post('/allFAQs', FaqController.actionAllFAQs);

module.exports = router;