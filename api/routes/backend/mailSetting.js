var express = require('express');
var router = express.Router();
var MailController = require('../../controllers/MailController');

router.post('/', MailController.actionIndex);

router.post('/add', MailController.actionCreate);

router.post('/edit', MailController.actionUpdate);

router.post('/view', MailController.actionView);

router.post('/delete', MailController.actionDelete);

router.post('/status', MailController.actionStatus);

router.post('/default', MailController.actionDefault);

router.post('/allMail', MailController.actionAllMail);

module.exports = router;