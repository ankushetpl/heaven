var express = require('express');
var router = express.Router();
var PageController = require('../../controllers/PageController');

router.post('/', PageController.actionIndex);

router.post('/add', PageController.actionCreate);

router.post('/edit', PageController.actionUpdate);

router.post('/view', PageController.actionView);

router.post('/delete', PageController.actionDelete);

router.post('/status', PageController.actionStatus);

router.post('/AllPage', PageController.actionAllPage);

module.exports = router;