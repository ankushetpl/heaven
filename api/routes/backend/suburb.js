var express = require('express');
var router = express.Router();
var SuburbController = require('../../controllers/SuburbController');

router.post('/', SuburbController.actionIndex);

router.post('/add', SuburbController.actionCreate);

router.post('/edit', SuburbController.actionUpdate);

router.post('/view', SuburbController.actionView);

router.post('/delete', SuburbController.actionDelete);

router.post('/status', SuburbController.actionStatus);

router.post('/getCity', SuburbController.getCity);

module.exports = router;