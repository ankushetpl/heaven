var express = require('express');
var router = express.Router();
var SuburbsController = require('../../controllers/SuburbsController');

router.post('/', SuburbsController.actionIndex);

router.post('/add', SuburbsController.actionCreate);

router.post('/edit', SuburbsController.actionUpdate);

router.post('/view', SuburbsController.actionView);

router.post('/delete', SuburbsController.actionDelete);

router.post('/status', SuburbsController.actionStatus);

module.exports = router;