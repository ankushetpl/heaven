var express = require('express');
var router = express.Router();
var PaymentSuccessController = require('../../controllers/PaymentSuccessController');

router.post('/', PaymentSuccessController.actionSuccessIndex);
router.post('/view', PaymentSuccessController.actionView);
router.post('/delete', PaymentSuccessController.actionDelete);
router.post('/successOnceCSV', PaymentSuccessController.actionSuccessOnceCSV);

router.post('/week', PaymentSuccessController.actionSuccessIndexWeek);
router.post('/weekview', PaymentSuccessController.actionViewWeek);
router.post('/weekdelete', PaymentSuccessController.actionDeleteWeek);
router.post('/successWeeklyCSV', PaymentSuccessController.actionSuccessWeeklyCSV);

module.exports = router;