var express = require('express');
var router = express.Router();
var BannerController = require('../../controllers/BannerController');

router.post('/', BannerController.actionIndex);

router.post('/add', BannerController.actionCreate);

router.post('/edit', BannerController.actionUpdate);

router.post('/view', BannerController.actionView);

router.post('/delete', BannerController.actionDelete);

router.post('/status', BannerController.actionStatus);

module.exports = router;