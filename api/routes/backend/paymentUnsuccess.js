var express = require('express');
var router = express.Router();
var PaymentUnsuccessController = require('../../controllers/PaymentUnsuccessController');

router.post('/', PaymentUnsuccessController.actionUnsuccessIndex);
router.post('/view', PaymentUnsuccessController.actionView);
router.post('/delete', PaymentUnsuccessController.actionDelete);
router.post('/unsuccessOnceCSV', PaymentUnsuccessController.actionUnsuccessOnceCSV);

router.post('/weekly', PaymentUnsuccessController.actionUnsuccessIndexWeekly);
router.post('/weeklyView', PaymentUnsuccessController.actionWeeklyView);
router.post('/weeklyDelete', PaymentUnsuccessController.actionWeeklyDelete);
router.post('/unsuccessWeeklyCSV', PaymentUnsuccessController.actionUnsuccessWeeklyCSV);

module.exports = router;