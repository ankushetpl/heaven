var express = require('express');
var router = express.Router();
var PaymentCancelController = require('../../controllers/PaymentCancelController');

router.post('/', PaymentCancelController.actionIndex);
router.post('/view', PaymentCancelController.actionView);
router.post('/delete', PaymentCancelController.actionDelete);
router.post('/cancelOnceCSV', PaymentCancelController.actionCancelOnceCSV);

router.post('/weekly', PaymentCancelController.actionIndexWeekly);
router.post('/weeklyView', PaymentCancelController.actionWeeklyView);
router.post('/weeklyDelete', PaymentCancelController.actionWeeklyDelete);
router.post('/cancelWeeklyCSV', PaymentCancelController.actionCancelWeeklyCSV);

module.exports = router;