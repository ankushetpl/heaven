var express = require('express');
var router = express.Router();
var WebsiteController = require('../../controllers/WebsiteController');

router.post('/', WebsiteController.actionIndex);

router.post('/view', WebsiteController.actionView);

// router.post('/changepassword', WebsiteController.actionCreatePass);

// router.post('/forgetpassword', WebsiteController.actionForget);

// router.post('/validate-user', WebsiteController.actionValidateUser);

// router.post('/logout', WebsiteController.actionLogout);


module.exports = router;