var express = require('express');
var router = express.Router();
var AssessorController = require('../../controllers/AssessorController');

router.post('/checklist', AssessorController.actionChecklist);

router.post('/notification', AssessorController.actionNotification);

router.post('/deleteNot', AssessorController.actionDeleteNot);

router.post('/requests', AssessorController.actionRequests);

router.post('/rejectWorker', AssessorController.actionRejectWorker);

router.post('/acceptWorker', AssessorController.actionAcceptWorker);

router.post('/workerProfile', AssessorController.actionWorkerProfile);

router.post('/myWorker', AssessorController.actionMyWorker);

router.post('/allAssessors', AssessorController.actionAllAssessors);

router.post('/calender', AssessorController.actionCalender);

router.post('/skills', AssessorController.actionSkills);

router.post('/skillById', AssessorController.actionSkillById);

router.post('/addskill', AssessorController.actionAddskill);

router.post('/workerRatings', AssessorController.actionWorkerRatings);

router.post('/bookNextAssessor', AssessorController.actionBookNextAssessor);

router.post('/requestByDate', AssessorController.actionRequestByDate);

router.post('/requestById', AssessorController.actionRequestById);

router.post('/updateAvailability', AssessorController.actionUpdateAvailability);

router.post('/rescheduleRequest', AssessorController.actionRescheduleRequest);

router.post('/getProvince', AssessorController.actionGetProvince);

router.post('/getAllCities', AssessorController.actionGetAllCities);

router.post('/getUnavailabilityData', AssessorController.actionGetUnavailabilityData);

router.post('/getRequestBookAssessor', AssessorController.actionGetRequestBookAssessor);

router.post('/getTimeSlots', AssessorController.actionGetTimeSlots);

module.exports = router;