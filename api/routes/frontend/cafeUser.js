var express = require('express');
var router = express.Router();
var CafeUserController = require('../../controllers/CafeUserController');

router.post('/allCountries', CafeUserController.actionAllCountries);

router.post('/allStates', CafeUserController.actionAllStates);

router.post('/allCities', CafeUserController.actionAllCities);

router.post('/allSuburbs', CafeUserController.actionAllSuburs);

router.post('/allSkills', CafeUserController.actionAllSkills);

router.post('/savePassword', CafeUserController.actionSavePassword);

router.post('/', CafeUserController.actionIndex);

router.post('/add', CafeUserController.actionCreate);

router.post('/update', CafeUserController.actionUpdate);

router.post('/updateImage', CafeUserController.actionUpdateImage);

router.post('/view', CafeUserController.actionView);

router.post('/allStatesByID', CafeUserController.actionAllStatesByID);

router.post('/allCitiesByID', CafeUserController.actionAllCitiesByID);

router.post('/allSuburbByID', CafeUserController.actionAllSuburbByID);

router.post('/allSuburbByMID', CafeUserController.actionAllSuburbByMID);

router.post('/allSuburbByIDWorker', CafeUserController.actionAllSuburbByIDWorker);

router.post('/allSuburbByCityId', CafeUserController.actionAllSuburbByCityId);

// Assessment
router.post('/checkAssessment', CafeUserController.actionCheckAssessment);

router.post('/checkAssessor', CafeUserController.actionCheckAssessor);

router.post('/checkLeave', CafeUserController.actionCheckLeave);

router.post('/assessmentList', CafeUserController.actionAssessmentList);

router.post('/viewAssessor', CafeUserController.actionViewAssessor);

router.post('/bookAssessor', CafeUserController.actionBookAssessor);

router.post('/booking', CafeUserController.actionBooking);

// Worker detail and count with status for backend.
router.post('/workerCount', CafeUserController.actionWorkerCount);

// router.post('/edit', CafeUserController.actionUpdate);
// router.post('/delete', CafeUserController.actionDelete);
// router.post('/status', CafeUserController.actionStatus);

//API
// router.post('/allFAQs', CafeUserController.actionAllFAQs);

module.exports = router;