var express = require('express');
var router = express.Router();
var WorkerController = require('../../controllers/WorkerController');


router.post('/update', WorkerController.actionUpdate);

router.post('/view', WorkerController.actionView);

router.post('/jobs', WorkerController.actionJobs);

router.post('/myJobs', WorkerController.actionMyJobs);

router.post('/history', WorkerController.actionHistory);

router.post('/substituteReq', WorkerController.actionSubstituteRequest);

router.post('/substituteSend', WorkerController.actionSubstituteSend);

router.post('/workersList', WorkerController.actionWorkersList);

router.post('/leaveReasonList', WorkerController.actionleaveReasonList);

router.post('/leaveApply', WorkerController.actionleaveApply);

router.post('/leaveDelete', WorkerController.actionleaveDelete);

router.post('/workerLeaves', WorkerController.actionworkerLeaves);

router.post('/calender', WorkerController.actioncalender);

router.post('/jobTime', WorkerController.actionjobTime);

router.post('/jobDetails', WorkerController.actionjobDetails);

router.post('/cabRequest', WorkerController.actioncabRequest);

router.post('/mySuburb', WorkerController.actionmySuburb);

router.post('/updateSuburb', WorkerController.actionupdateSuburb);

router.post('/allCities', WorkerController.actionallCities);

router.post('/ratingList', WorkerController.actionratingList);

router.post('/rateClient', WorkerController.actionrateClient);

router.post('/substituteAcceptReject', WorkerController.actionsubstituteAcceptReject);

router.post('/completeRateJobs', WorkerController.actioncompleteRateJobs);

router.post('/notification', WorkerController.actionnotification);     // android worker

// router.post('/notificationClient', WorkerController.actionnotificationClient);     // android client

router.post('/notList', WorkerController.actionnotList);

router.post('/readNot', WorkerController.actionreadNot);

router.post('/deleteNot', WorkerController.actionDeleteNot);

router.post('/notCount', WorkerController.actionnotCount);

router.post('/substituteWorker', WorkerController.actionSubstituteWorker);

router.post('/earlyPayRequest', WorkerController.actionEarlyPayRequest);

router.post('/forgetPass', WorkerController.actionForgetPass);

router.post('/resetPass', WorkerController.actionResetPass);

router.post('/serviceSuburbList', WorkerController.actionserviceSuburbList);


module.exports = router;