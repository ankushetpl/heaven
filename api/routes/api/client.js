var express = require('express');
var router = express.Router();
var ClientController = require('../../controllers/ClientController');


router.post('/update', ClientController.actionUpdate);

router.post('/view', ClientController.actionView);

router.post('/workerList', ClientController.actionWorkerList);

router.post('/workerBooking', ClientController.actionWorkerBooking);

router.post('/bookingPayment', ClientController.actionbookingPayment);

router.post('/checkPaygate', ClientController.actioncheckPaygate);

router.post('/bookServiceWorker', ClientController.actionbookServiceWorker);    //update workerid after bookingdata submitted

router.post('/addFavourite', ClientController.actionAddFavourite);

router.post('/favWorkers', ClientController.actionFavWorkers);

router.post('/history', ClientController.actionHistory);

router.post('/historyWeekly', ClientController.actionHistoryWeekly);

router.post('/addTasklist', ClientController.actionAddTasklist);

router.post('/deleteTasklist', ClientController.actionDeleteTasklist);

router.post('/myTasklist', ClientController.actionMyTasklist); // all approve & not approved task

router.post('/tasklist', ClientController.actionTasklist); // standrd & approved task list

router.post('/upcomingJobs', ClientController.actionupcomingJobs);

router.post('/deleteUpcoming', ClientController.actiondeleteUpcoming);

router.post('/claimCategory', ClientController.actionclaimCategory);

router.post('/claimWorker', ClientController.actionclaimWorker);

router.post('/tasktype', ClientController.actiontasktype);

router.post('/amenities', ClientController.actionamenities);

router.post('/workerSkills', ClientController.actionworkerSkills);

router.post('/workerSkillsNew', ClientController.actionworkerSkillsNew);

router.post('/claimBooking', ClientController.actionclaimBooking);

router.post('/instruction', ClientController.actioninstruction);

router.post('/getInstruction', ClientController.actiongetInstruction);

router.post('/promos', ClientController.actionpromos);

router.post('/jobDetails', ClientController.actionjobDetails);

router.post('/referral', ClientController.actionreferral);

router.post('/creditHistory', ClientController.actioncreditHistory);

router.post('/angelList', ClientController.actionangelList);

router.post('/ratingList', ClientController.actionratingList);

router.post('/rateWorker', ClientController.actionrateWorker);

router.post('/bookingListRate', ClientController.actionbookingListRate);

router.post('/myCredit', ClientController.actionmyCredit);

router.post('/iosNot', ClientController.actioniosNot);    // testing

router.post('/rateWeeklyList', ClientController.actionrateWeeklyList);  

router.post('/paymentHistory', ClientController.actionpaymentHistory); 

router.post('/updateDevice', ClientController.actionupdateDevice); 

router.post('/workerAvailable', ClientController.actionworkerAvailable); 

router.post('/rateWorkerWeek', ClientController.actionrateWorkerWeek); 

router.post('/supportMail', ClientController.actionSupportMail); 

router.post('/checkAvailWorkerPayment', ClientController.actionCheckAvailWorkerPayment); 

router.post('/checkFavAvailWorker', ClientController.actionCheckFavAvailWorker); 

router.post('/rescheduleBooking', ClientController.actionRescheduleBooking); 

router.post('/renewBooking', ClientController.actionRenewBooking); 

router.post('/buyCredit', ClientController.actionBuyCredit); 

router.post('/topUpPoint', ClientController.actiontopUpPoint); 

router.post('/normalEFT', ClientController.normalEFT); 


router.post('/checkWeekDays', ClientController.checkWeekDays); 

router.post('/getLatLong', ClientController.getLatLong); 

router.post('/paymentangelList', ClientController.actionpaymentAngelList);

// router.post('/checkWeekDays', ClientController.checkWeekDays); 




module.exports = router;