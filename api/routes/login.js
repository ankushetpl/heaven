var express = require('express');
var router = express.Router();
var LoginController = require('../controllers/LoginController');

router.post('/', LoginController.actionIndex);

router.post('/register', LoginController.actionCreate);

router.post('/changepassword', LoginController.actionCreatePass);

router.post('/forgetpassword', LoginController.actionForget);

router.post('/validate-user', LoginController.actionValidateUser);

router.post('/logout', LoginController.actionLogout);

router.post('/checkForgot', LoginController.actionCheckForgot);

router.post('/savePassword', LoginController.actionSavePassword);

router.post('/activeAccount', LoginController.actionActiveAccount);

//API
router.post('/logoutApp', LoginController.actionLogoutApp);

// router.post('/delete', LoginController.actionDelete);

module.exports = router;