(function () {
    'use strict';
    var authApp = angular.module('authApp', []);

    authenticateUser1.$inject = ['authServices', '$state']

    function authenticateUser1(authServices, $state) {
        return authServices.checkValidUser(false);
    } //END authenticateUser()

    authApp.config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('auth', {
                url: '/auth',
                views: {
                    '': {
                        templateUrl: 'app/layouts/auth/layout.html'
                    },
                    'content@auth': {
                        templateUrl: 'app/components/auth/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                //    auth1: authenticateUser1
                }
            })
            .state('auth.login', {
                url: '/login',
                views: {
                    'content@auth': {
                        templateUrl: 'app/components/auth/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                },
                resolve: {
                    auth1: authenticateUser1
                }
            })
            .state('auth.forgotpassword', {
                url: '/forgot-password',
                views: {
                    'content@auth': {
                        templateUrl: 'app/components/auth/forgotpassword.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                }
            });
    });
}());
(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkLogin = checkLogin;
        self.checkForgotPassword = checkForgotPassword;
        self.checkValidUser = checkValidUser;
        self.setAuthToken = setAuthToken;
        self.getAuthToken = getAuthToken;
        self.saveUserInfo = saveUserInfo;
        self.checkValidUrl = checkValidUrl;
        self.getUserProfile = getUserProfile;
        self.updateProfile = updateProfile;
        self.changePassword = changePassword;
        self.logout = logout;
        self.getImage = getImage;

        //to check if user is login and set user details in rootscope
        function checkLogin(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login';
            var requestData = {};

            if (typeof obj.email !== undefined && obj.email !== '') {
                requestData['username'] = obj.email;
            }
            if (typeof obj.password !== undefined && obj.password !== '') {
                requestData['password'] = obj.password;
            }
            if (typeof obj.usertype !== undefined && obj.usertype !== '') {
                requestData['usertype'] = obj.usertype;
            }
            if (typeof obj.device_token !== undefined && obj.device_token !== '') {
                requestData['device_token'] = obj.device_token;
            }
            if (typeof obj.device_type !== undefined && obj.device_type !== '') {
                requestData['device_type'] = obj.device_type;
            }
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                deferred.resolve(response);
            }, function (response) {
                $rootScope.isLogin = false;
                $rootScope.$broadcast('auth:login:required');
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkLogin();

        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'login/validate-user';
            var deferred = $q.defer();
            var requestData = {};
            // requestData['usertype'] = 1;
            requestData['token'] = getAuthToken();
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                
                if (response.data.status == 0) {
                    
                    $rootScope.$broadcast('auth:login:required');
                } else {
                    $rootScope.isLogin = true;
                    saveUserInfo(response.data);
                    if (isAuth === false) {
                        $rootScope.$broadcast('auth:login:success');
                    }
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function checkForgotPassword(email) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/forgetpassword';
            var requestData = { email : email };

            if (typeof email !== undefined && email !== '') {
                requestData['user_username'] = email;
            }
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkForgotPassword();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.data;
                $rootScope.user = user;
            }
            return user;
        } //END saveUserInfo();

        function checkValidUrl(role, location) {
            var urlData = location.split('/');
            var actualUrl = urlData[1];
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'valid-url';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("role_id", role);
                    formData.append("url", actualUrl);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        deferred.resolve(response);
                    } else {
                        $state.go('frontoffice.dashboard');
                    }
                }
            });
            return deferred.promise;
        }

        /* to get user profile data */
        function getUserProfile(user_id, role_id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};
            requestData['user_id'] = user_id;
            requestData['UserType'] = role_id;
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END getUserProfile()

        /* to update profile data */
        function updateProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'user/update';
            
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.usertype !== undefined && obj.usertype !== '') {
                    requestData['usertype'] = parseInt(obj.usertype);
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.name !== undefined && obj.name !== '') {
                    requestData['name'] = obj.name;
                }

                if (typeof obj.phone !== undefined && obj.phone !== '') {
                    requestData['phone'] = obj.phone;
                }

                if (typeof obj.address !== undefined && obj.address !== '') {
                    requestData['address'] = obj.address;
                }

                if (typeof obj.countryID !== undefined && obj.countryID !== '') {
                    requestData['cafeusers_country'] = parseInt(obj.countryID);
                }

                if (typeof obj.stateID !== undefined && obj.stateID !== '') {
                    requestData['cafeusers_state'] = parseInt(obj.stateID);
                }

                if (typeof obj.cityID !== undefined && obj.cityID !== '') {
                    requestData['cafeusers_city'] = parseInt(obj.cityID);
                }

                if (typeof obj.suburbID !== undefined && obj.suburbID !== '') {
                    requestData['cafeusers_suburb'] = parseInt(obj.suburbID);
                }

                if (typeof obj.imageID !== undefined && obj.imageID !== '') {
                    requestData['assessors_image'] = parseInt(obj.imageID);
                }
            }

            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateProfile();

        /* to update profile data */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/changepassword';
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;                    
                }

                if (typeof obj.newPassword !== undefined && obj.newPassword !== '') {
                    requestData['newpassword'] = obj.newPassword;
                }

                if (typeof obj.oldPassword !== undefined && obj.oldPassword !== '') {
                    requestData['oldpassword'] = obj.oldPassword;
                }
            }
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

        function logout() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/logout';
            var requestData = {};
            requestData['usertype'] = $rootScope.user.user_id;
            requestData['token'] = getAuthToken();
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                localStorage.removeItem('token');
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END logout()  

        /* to get single image */
        function getImage(file_id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }                        
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getImage();

        return self;
        
    };
})();
(function () {
    'use strict';
    angular.module('authApp').controller('AuthController', AuthController);

    AuthController.$inject = ['$rootScope', '$location', 'authServices', '$state', 'toastr', '$timeout', 'ngProgressFactory', '$ocLazyLoad'];

    function AuthController($rootScope, $location, authServices, $state, toastr, $timeout, ngProgressFactory, $ocLazyLoad) {
        var vm = this;
        vm.login = login;
        vm.forgotPassword = forgotPassword;
        
        vm.user = { device_token: '', device_type: '1' };
        $rootScope.bodyClass = '';

        vm.progressbar = ngProgressFactory.createInstance();
        
        $ocLazyLoad.load('../assets/front/css/style.css');

        function login(loginInfo) {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            
            authServices.checkLogin(loginInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        var token = response.data.data.device_token;
                        authServices.setAuthToken(token); //Set auth token
                        vm.progressbar.complete();
                        $state.go('frontoffice.dashboard');                        
                    }else{
                        toastr.error(response.data.message, "Log In");
                        vm.isDisabledButton = false;
                        vm.progressbar.complete();
                    }                    
                } else {
                    toastr.error(response.data.message, "Log In");
                    vm.isDisabledButton = false;
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Log In');
                vm.isDisabledButton = false;
                vm.progressbar.complete();
            });
        }; //END login()   

        function forgotPassword(email) {
            vm.progressbar.start();
            vm.isDisabledButton = true;

            authServices.checkForgotPassword(email).then(function (response) {
                if (response.status == 200) {
                    if(response.data.status == 1){
                        toastr.success(response.data.message, "Forgot Password");
                        vm.progressbar.complete();
                        $timeout( function(){
                            $state.go('auth');                            
                        }, 1000 );
                    }else{
                        toastr.error(response.data.message, "Forgot Password");
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                    }                    
                } else {
                    toastr.error(response.data.message, "Forgot Password");
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
                vm.progressbar.complete();
                vm.isDisabledButton = false;
            });
        }; //END forgotPassword() 
    };
}());
(function () {
    'use strict';
    angular.module('dashboardApp', []).controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$rootScope', '$state', '$location', 'authServices', 'dashboardService', 'toastr', '$ocLazyLoad', '$anchorScroll', 'ngProgressFactory', '$timeout'];
    
    function DashboardController($scope, $rootScope, $state, $location, authServices, dashboardService, toastr, $ocLazyLoad, $anchorScroll, ngProgressFactory, $timeout) {
        var vm = this;
        
        // vm.passwordValidator = passwordValidator;
        // $rootScope.bodyClass = 'fix-header fix-sidebar card-no-border';
        
        vm.setFlagC = false;
        vm.setFlagA = false;
        
        if($rootScope.user.user_role_id == 2){
            vm.setFlagC = true;
            $ocLazyLoad.load('../assets/front/css/style_internet_cafe.css');
            $rootScope.bodyClass = 'internetCafe';
        }

        if($rootScope.user.user_role_id == 4){
            vm.setFlagA = true;
            $ocLazyLoad.load('../assets/front/css/style_assasor.css');
            $rootScope.bodyClass = 'assasorCafe';
        }

        $anchorScroll();

        vm.progressbar = ngProgressFactory.createInstance();

        vm.closeMod = closeMod;
        vm.savePassword = savePassword;
        
        vm.passwordForm = { user_id : $rootScope.user.user_id, user_username : $rootScope.user.user_username  };
        $scope.regex = /^[a-zA-Z][^`~!#$%\^&*()+={}|[\]\\:;"<>?,/]*$/;
        // $scope.regex = /^[a-zA-Z\_\- ]*$[^`~!#$%\^&*()+={}|[\]\\:;"<>?,/]/;
        // $scope.regex = /^[a-zA-Z\_\- ]*$/;

        function closeMod(){
            authServices.logout().then(function (response) {
                if (response.status == 200) {
                    $state.go('auth.login');
                }
            }).catch(function (response) {
                toastr.error("Unable to Logout!<br/>Try again later", "Error");
            });
        }

        function savePassword(){
            vm.isDisabledButton = true;
            vm.progressbar.start();
            dashboardService.changePassword(vm.passwordForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        toastr.success(response.data.message, 'Heavenly Sent');
                        $state.reload();
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Heavenly Sent');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END savePassword();

        // function passwordValidator(password) {
        //     if (!password) return;
        //     if (password.length < 6) return "Password must be at least " + 6 + " characters long";
        //     if (!password.match(/[A-Z]/)) return "Password must have at least one capital letter";
        //     if (!password.match(/[0-9]/)) return "Password must have at least one number";
        //     return true;
        // };

        
    };

}());
(function() {
    "use strict";
    angular
        .module('dashboardApp')
        .service('dashboardService', dashboardService);

    dashboardService.$inject = ['$http', 'APPCONFIG', '$q'];

    function dashboardService($http, APPCONFIG, $q) {
        return {
            // updateProfile: updateProfile,
            changePassword: changePassword
        }

        /* update profile */
        // function updateProfile(obj) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'update-profile';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function(data) {
        //             var formData = new FormData();
        //             formData.append("token", obj.token);
        //             formData.append("firstname", obj.firstname);
        //             formData.append("lastname", obj.lastname);
        //             return formData;
        //         },
        //         headers: {
        //             'Content-Type': undefined
        //         }
        //     }).then(function(response) {
        //         deferred.resolve(response);
        //     }, function(error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // } //END updateProfile();

        /* change password */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'cafeUser/savePassword';
            var requestData = {};
            
            if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                requestData['user_id'] = obj.user_id;
            }

            if (typeof obj.new_password !== undefined && obj.new_password !== '') {
                requestData['user_password'] = obj.new_password;
            }

            if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                requestData['user_username'] = obj.user_username;
            }

            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

    }; //END dashboardService()
}());
(function () {

    angular.module('cafeUserApp', [
        // 'angucomplete-alt',
        // 'ngFileUpload',
        // 'angularjs-datetime-picker'
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var cafePath = './app/components/cafeUser/';
        $stateProvider
            .state('frontoffice.cafe', {
                url: 'cafe',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/index.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.create', {
                url: '/create',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/form.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.edit', {
                url: '/edit/:id',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/formEdit.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.view', {
                url: '/view/:id',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/view.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.notification', {
                url: '/notification',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/notification.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.booking', {
                url: '/booking',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/booking.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.appointform', {
                url: '/scheduleform/:id',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/appointmentform.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            });
    }

}());
(function () {
    "use strict";
    angular
        .module('cafeUserApp')
        .service('CafeUserService', CafeUserService);

    CafeUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function CafeUserService($http, APPCONFIG, $q) {

        /* get all Notifications */
        function getNotifications(userID) {

            var URL = APPCONFIG.APIURL + 'assessor/notification';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['user_id'] = userID;
            }

            return this.runHttp(URL, requestData);
        } //END getNotifications();    

        /* clear all Notifications */
        function clearAllNotifications(userID) {
           
            var URL = APPCONFIG.APIURL + 'assessor/deleteNot';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['user_id'] = userID;
            }

            return this.runHttp(URL, requestData);
        } //END clearAllNotifications();  

        /* save worker user */
        function saveWorker(obj) {
            var URL = APPCONFIG.APIURL + 'cafeUser/add';
            
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'cafeUser/update';
                }

                if (typeof obj.workerType !== undefined && obj.workerType !== '') {
                    requestData['worker_type'] = obj.workerType;
                }

                if(obj.workerType == 1){
                    if (typeof obj.worker_message !== undefined && obj.worker_message !== '') {
                        requestData['worker_message'] = obj.worker_message;
                    } 
                }else{
                    requestData['worker_message'] = '';
                }

                if (typeof obj.workers_address !== undefined && obj.workers_address !== '') {
                    requestData['workers_address'] = obj.workers_address;
                }

                if (typeof obj.workers_city !== undefined && obj.workers_city !== '') {
                    requestData['workers_city'] = obj.workers_city;
                }

                if (typeof obj.workers_codeConduct !== undefined && obj.workers_codeConduct !== '') {
                    requestData['workers_codeConduct'] = obj.workers_codeConduct;
                }

                if (typeof obj.workers_country !== undefined && obj.workers_country !== '') {
                    requestData['workers_country'] = obj.workers_country;
                }

                if (typeof obj.workers_criminalReport !== undefined && obj.workers_criminalReport !== '') {
                    requestData['workers_criminalReport'] = obj.workers_criminalReport;
                }

                if (typeof obj.digitalAgreement !== undefined && obj.digitalAgreement !== '') {
                    requestData['workers_digitalAgreement'] = obj.digitalAgreement;
                }

                if (typeof obj.workers_email !== undefined && obj.workers_email !== '') {
                    requestData['workers_email'] = obj.workers_email;
                }

                if (typeof obj.workers_expiryDateDay !== undefined && obj.workers_expiryDateDay !== '') {
                    requestData['workers_expiryDateDay'] = obj.workers_expiryDateDay;
                }

                if (typeof obj.workers_expiryDateMonth !== undefined && obj.workers_expiryDateMonth !== '') {
                    requestData['workers_expiryDateMonth'] = obj.workers_expiryDateMonth;
                }

                if (typeof obj.workers_expiryDateYear !== undefined && obj.workers_expiryDateYear !== '') {
                    requestData['workers_expiryDateYear'] = obj.workers_expiryDateYear;
                }

                if (typeof obj.friendlyDogs !== undefined && obj.friendlyDogs !== '') {
                    requestData['workers_friendlyDogs'] = obj.friendlyDogs;
                }

                if (typeof obj.friendlyCats !== undefined && obj.friendlyCats !== '') {
                    requestData['workers_friendlyCats'] = obj.friendlyCats;
                }

                if (typeof obj.workers_image !== undefined && obj.workers_image !== '') {
                    requestData['workers_image'] = obj.workers_image;
                }

                if (typeof obj.workers_mobile !== undefined && obj.workers_mobile !== '') {
                    requestData['workers_mobile'] = obj.workers_mobile;
                }

                if (typeof obj.workers_name !== undefined && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.workers_phone !== undefined && obj.workers_phone !== '') {
                    requestData['workers_phone'] = obj.workers_phone;
                }

                if (typeof obj.workers_photoIdentity !== undefined && obj.workers_photoIdentity !== '') {
                    requestData['workers_photoIdentity'] = obj.workers_photoIdentity;
                }

                if (typeof obj.workers_state !== undefined && obj.workers_state !== '') {
                    requestData['workers_state'] = obj.workers_state;
                }

                if (typeof obj.location_suburb !== undefined && obj.location_suburb !== '') {
                    requestData['location_suburb'] = obj.location_suburb;
                }

                // if (typeof obj.suburb2 !== undefined && obj.suburb2 !== '') {
                //     requestData['suburb2'] = obj.suburb2;
                // }

                // if (typeof obj.suburb3 !== undefined && obj.suburb3 !== '') {
                //     requestData['suburb3'] = obj.suburb3;
                // }

                // if (typeof obj.suburb4 !== undefined && obj.suburb4 !== '') {
                //     requestData['suburb4'] = obj.suburb4;
                // }

                if (typeof obj.cafeuser_id !== undefined && obj.cafeuser_id !== '') {
                    requestData['cafeuser_id'] = obj.cafeuser_id;
                }

                if (typeof obj.workers_birthDate !== undefined && obj.workers_birthDate !== '') {
                    requestData['workers_birthDate'] = obj.workers_birthDate;
                }

                if (typeof obj.workers_age !== undefined && obj.workers_age !== '') {
                    requestData['workers_age'] = obj.workers_age;
                }

                if (typeof obj.workers_idPassport !== undefined && obj.workers_idPassport !== '') {
                    requestData['workers_idPassport'] = obj.workers_idPassport;
                }

                if (typeof obj.workers_asylum !== undefined && obj.workers_asylum !== '') {
                    requestData['workers_asylum'] = obj.workers_asylum;
                }

                if (typeof obj.workers_uniform !== undefined && obj.workers_uniform !== '') {
                    requestData['workers_uniform'] = obj.workers_uniform;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveWorkerUser();

        /* to get all worker user  */
        function getWorkerList(pageNum, obj, orderInfo, userID) {
            var URL = APPCONFIG.APIURL + 'cafeUser';
            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof userID !== 'undefined' && userID !== '') {
                requestData['userID'] = userID;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.workers_phone !== 'undefined' && obj.workers_phone !== '') {
                    requestData['workers_phone'] = obj.workers_phone;
                }

                if (typeof obj.workers_suburb !== 'undefined' && obj.workers_suburb !== '') {
                    requestData['workers_suburb'] = parseInt(obj.workers_suburb);
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
                }

                if (typeof obj.end_at !== 'undefined' && obj.end_at !== '') {
                    requestData['end_at'] = moment(obj.end_at).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getWorkerList();

        /* to get all booking   */
        function getBookingList(pageNum, obj, orderInfo, userID) {
            var URL = APPCONFIG.APIURL + 'cafeUser/booking';
            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof userID !== 'undefined' && userID !== '') {
                requestData['userID'] = userID;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.stateID !== 'undefined' && obj.stateID !== '') {
                    requestData['stateID'] = parseInt(obj.stateID);
                }

                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['cityID'] = parseInt(obj.cityID);
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
                }

                if (typeof obj.end_at !== 'undefined' && obj.end_at !== '') {
                    requestData['end_at'] = moment(obj.end_at).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getBookingList();

        /* to get single worker */
        function getSingleWorker(user_id) {
            var URL = APPCONFIG.APIURL + 'cafeUser/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWorker();

        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get All States
        function getAllStates(userID, userRoleID){
            var URL = APPCONFIG.APIURL + 'assessor/getProvince';
            var requestData = {};

            if (typeof userID !== 'undefined' && userID !== '') {
                requestData['userID'] = parseInt(userID);
            }

            if (typeof userRoleID !== 'undefined' && userRoleID !== '') {
                requestData['userRoleID'] = parseInt(userRoleID);
            }

            return this.runHttp(URL, requestData);
        }//END getAllStates();

        //Get States
        function getStatesByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allStatesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_country !== 'undefined' && obj.workers_country !== '') {
                    requestData['ID'] = parseInt(obj.workers_country);
                }

                if (typeof obj.countryID !== 'undefined' && obj.countryID !== '') {
                    requestData['ID'] = parseInt(obj.countryID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get city 
        function getCityByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCitiesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_state !== 'undefined' && obj.workers_state !== '') {
                    requestData['ID'] = parseInt(obj.workers_state);
                }
                
                if (typeof obj.stateID !== 'undefined' && obj.stateID !== '') {
                    requestData['ID'] = parseInt(obj.stateID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get suburb by ID 
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }
                
                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['ID'] = parseInt(obj.cityID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        //Get Mult suburb by ID 
        function getSuburbByMID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByMID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }
                
                if (typeof obj.suburb1 !== 'undefined' && obj.suburb1 !== '') {
                    requestData['suburb1'] = parseInt(obj.suburb1);
                }

                if (typeof obj.suburb2 !== 'undefined' && obj.suburb2 !== '') {
                    requestData['suburb2'] = parseInt(obj.suburb2);
                }

                if (typeof obj.suburb3 !== 'undefined' && obj.suburb3 !== '') {
                    requestData['suburb3'] = parseInt(obj.suburb3);
                }

                if (typeof obj.suburb4 !== 'undefined' && obj.suburb4 !== '') {
                    requestData['suburb4'] = parseInt(obj.suburb4);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByMID();

        //Get all suburb 
        function getAllArea(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbs';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllArea();

        //Get all skill 
        function getAllSkill(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSkills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllSkill();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        /* to check Assessment */
        function checkAssessment(userID, workersID) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkAssessment';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['cafe_id'] = userID;
            }

            if (typeof workersID !== undefined && workersID !== '') {
                requestData['worker_id'] = workersID;
            }
                        
            return this.runHttp(URL, requestData);
        } //END checkAssessment();

        /* to delete a Internet Cafe Worker from database */
        function deleteWorker(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteWorker();

        /* to get all worker user  */
        function getAssessorList(pageNum, obj, getAssCountry) {
            var URL = APPCONFIG.APIURL + 'cafeUser/assessmentList';            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }
            
            if (typeof getAssCountry !== 'undefined' && getAssCountry !== '') {
                requestData['getAssCountry'] = getAssCountry;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.getAssCountry !== 'undefined' && obj.getAssCountry !== '') {
                    requestData['getAssCountry'] = parseInt(obj.getAssCountry);
                }
                
                if (typeof obj.stateID !== 'undefined' && obj.stateID !== '') {
                    requestData['stateID'] = parseInt(obj.stateID);
                }

                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['cityID'] = parseInt(obj.cityID);
                }

                if (typeof obj.suburb !== 'undefined' && obj.suburb !== '') {
                    requestData['suburb'] = parseInt(obj.suburb);
                }

                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
                }
            }

            return this.runHttp(URL, requestData);
        }//END getAssessorList();

        /* to check Assessor */
        function checkAssessor(assessorID, obj) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkAssessor';
            var requestData = {};

            if (typeof assessorID !== undefined && assessorID !== '') {
                requestData['assessorID'] = assessorID;
            }

            if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
            }else{
                requestData['start_at'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END checkAssessor();

        /* to check Assessor Leave */
        function checkLeave(assessorID, obj) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkLeave';
            var requestData = {};

            if (typeof assessorID !== undefined && assessorID !== '') {
                requestData['assessorID'] = assessorID;
            }

            if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
            }else{
                requestData['start_at'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END checkLeave();

        /* to get single Assessor */
        function getSingleAssessor(user_id, assessmentDate) {
            // console.log(assessmentDate);
            var URL = APPCONFIG.APIURL + 'cafeUser/viewAssessor';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof assessmentDate !== 'undefined' && assessmentDate !== '') {
                requestData['start_at'] = moment(assessmentDate).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getSingleAssessor();

        /* Booking request to the Assessor */
        function bookAssessmentRequest(obj, assessorID, userID, userName) {
            var URL = APPCONFIG.APIURL + 'cafeUser/bookAssessor';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['cafe_id'] = userID;
            }

            if (typeof userName !== undefined && userName !== '') {
                requestData['cafe_name'] = userName;
            }
            
            if (typeof assessorID !== undefined && assessorID !== '') {
                requestData['assessor_id'] = assessorID;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.worker_id !== 'undefined' && obj.worker_id !== '') {
                    requestData['worker_id'] = parseInt(obj.worker_id);
                }

                if (typeof obj.request_timeFrom !== 'undefined' && obj.request_timeFrom !== '') {
                    requestData['request_timeFrom'] = obj.request_timeFrom;
                }

                if (typeof obj.request_timeTo !== 'undefined' && obj.request_timeTo !== '') {
                    requestData['request_timeTo'] = obj.request_timeTo;
                }

                if (typeof obj.request_date !== 'undefined' && obj.request_date !== '') {
                    requestData['request_date'] = moment(obj.request_date).format('YYYY-MM-DD');
                }
            }

            return this.runHttp(URL, requestData);
        } //END bookAssessmentRequest();

        /* Save image */
        function saveImage(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/updateImage';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.fileImage !== 'undefined' && obj.fileImage !== '') {
                    requestData['fileImage'] = parseInt(obj.fileImage);
                }

                if (typeof obj.imageType !== 'undefined' && obj.imageType !== '') {
                    requestData['imageType'] = obj.imageType;
                }

                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                }
            }

            return this.runHttp(URL, requestData);
        }//END saveImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAllCountries: getAllCountries,
            getAllStates: getAllStates,
            getStatesByID: getStatesByID,
            getCityByID: getCityByID,
            getSuburbByID: getSuburbByID,
            getSuburbByMID: getSuburbByMID,
            getAllArea: getAllArea,
            getAllSkill: getAllSkill,
            getNotifications: getNotifications,
            clearAllNotifications: clearAllNotifications,
            getWorkerList: getWorkerList,
            getSingleWorker: getSingleWorker,
            getImage: getImage,
            saveWorker: saveWorker,
            deleteWorker: deleteWorker,
            checkAssessment: checkAssessment,
            getAssessorList: getAssessorList,
            checkAssessor: checkAssessor,
            checkLeave: checkLeave,
            getSingleAssessor: getSingleAssessor,
            bookAssessmentRequest: bookAssessmentRequest,
            getBookingList: getBookingList,
            saveImage: saveImage
        }

    };//END CafeUserService()
}());

(function () {
    "use strict";
    angular.module('cafeUserApp')
        .controller('CafeUserController', CafeUserController);

    CafeUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'CafeUserService', 'toastr', 'SweetAlert', 'Upload', '$timeout', '$anchorScroll', 'ngProgressFactory', 'APPCONFIG', '$locale'];

    function CafeUserController($scope, $rootScope, $state, $location, CafeUserService, toastr, SweetAlert, Upload, $timeout, $anchorScroll, ngProgressFactory, APPCONFIG, $locale) {
        var vm = this;

        $anchorScroll();
        vm.progressbar = ngProgressFactory.createInstance();
        $rootScope.bodyClass = 'internetCafe';

        var userID = $rootScope.user.user_id;
        var userRoleID = $rootScope.user.user_role_id;
        var userName = $rootScope.user.Name;
        
        vm.getNotification = getNotification;
        vm.clearAllNotification = clearAllNotification;
        vm.getWorkerList = getWorkerList;
        vm.getSingleWorker = getSingleWorker;
        vm.changeImagePopup = changeImagePopup;
        vm.closeImagePop = closeImagePop;
        vm.saveImage = saveImage;
        vm.saveWorker = saveWorker;
        vm.deleteWorker = deleteWorker;
        vm.Uploads = Uploads;
        vm.changePage = changePage;
        vm.formatDate = formatDate;
        vm.formatTime = formatTime;
        vm.formatDateOfBirth = formatDateOfBirth;
        vm.ageCalculate = ageCalculate;
        vm.workerAgeDisable = workerAgeDisable;
        vm.reset = reset;
        vm.sort = sort;
        vm.resetAssessment = resetAssessment;
        vm.resetWorkerRegi = resetWorkerRegi;
        vm.searchAssessment = searchAssessment;
        vm.getAssessmentList = getAssessmentList;
        vm.workerAssessment = workerAssessment;
        vm.bookAssessor = bookAssessor;
        vm.closePop = closePop;
        vm.resetBooking = resetBooking;
        vm.getBookingList = getBookingList;
        vm.searchBooking = searchBooking;
        vm.getAssessorProfile = getAssessorProfile;
        vm.closePopup = closePopup;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;
        vm.getSuburbByMID = getSuburbByMID;
        vm.getAllStates = getAllStates;

        vm.getAllArea = getAllArea;
        vm.getAllSkill = getAllSkill;

        var total = 0; // Mult Suburb
        vm.totalWorker = 0;
        vm.cafePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.imageFlag = false;
        vm.editFlag = false;
        vm.workerForm = { user_id: ''};
        vm.scheduleForm = [];
        vm.getAssCountry = '';
        
        vm.currentYear = new Date().getFullYear();
        vm.month = $locale.DATETIME_FORMATS.MONTH;
        vm.monthing= [];

        angular.forEach(vm.month, function(values, key) {
            vm.monthing.push({"id" : key + 1, "name" : values });
        });

        
        $scope.regex = /^[@a-zA-Z][^`~!#$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        $scope.stepsModel = [];
        $scope.stepsModel2 = [];
        $scope.imageIsLoaded = function(e) {
            //console.log(e.target.result);
            var imgStr = e.target.result;
            var imgTypes = imgStr.split(";");
            var types_ = imgTypes[0].split(":")[1];
            //console.log(types_);
            // if(!$scope.isVideo(types_)){
            //     angular.element("#uplVideo").val("");
            //     swal({ title: "Oops!", text: "video type is invalid.", type: "error" });
            //     return false;
            // }
           
        }

        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* to format time*/
        function formatTime(time) {
            return moment(time, "HH:mm:ss").format("HH:mm");
        }//END formatTime()

        /* to format date of birth*/
        function formatDateOfBirth(date) {
            return moment(date).format("YYYYMMDD");
        }//END formatDate()

        /* calculate age from date of birth */
        function ageCalculate(birthDate) {
            return moment().diff(moment(birthDate, 'YYYYMMDD'), 'years')
        }

        /* to format date of birth*/
        function workerAgeDisable() {
            this.myDate = new Date();
            vm.currentDate =  new Date(    
                this.myDate.getFullYear() - 21,    
                this.myDate.getMonth(),    
                this.myDate.getDate()  
            );
        }//END formatDate()
        
        /* for sorting worker user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getWorkerList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getWorkerList(newPage, searchInfo);
            getAssessmentList(newPage, searchInfo);
        }//END changePage();

        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[1] == "cafe") {
            vm.getAllArea();
        }

        if (path[2] == "notification") {
            vm.getNotification(userID);
        }
        vm.getNotification(userID);
         
        if(path[2] == "create"){
            vm.getAllCountries();
        }

        if (path[2] == "booking") {
            vm.getAllStates();
        }
                
        if (path[2] == "scheduleform") {
            var monthS = moment().add(1, 'days');
            // vm.myDate = new Date();
            vm.myDate = monthS._d;
                        
            vm.workerID = path[3];
            vm.getSingleWorker(path[3]);
            $timeout( function(){
                vm.progressbar.start();
                getAssessmentList(1, '');
            }, 2000 );                
        }

        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('frontoffice');
                return false;
            } else {
                vm.editFlag = true;
                vm.progressbar.start();
                vm.getAllCountries();
                vm.getAllArea();
                vm.getAllSkill();
                $timeout( function(){
                    vm.getSingleWorker(path[3]);
                }, 1000 );                
            }
        }

        /* to get notification list */
        function getNotification(userID) {
            CafeUserService.getNotifications(userID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $rootScope.countNot = response.data.count;
                        vm.allNotification = response.data.notifications;
                    } else {
                        vm.allNotification.error = "No data for notifications";
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Internet Cafe');
            });
        }//END getNotification(); 

        /* to clear all notifications */
        function clearAllNotification() {
            SweetAlert.swal({
                title: "Are you sure you want to delete all notifications?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CafeUserService.clearAllNotifications(userID).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.getNotification(userID);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Internet Cafe');
                    });
                }
            });
        }//END clearAllNotification();    

        /* to get worker list */
        function getWorkerList(newPage, obj) {
            vm.totalWorker = 0;
            vm.workerList = [];
            CafeUserService.getWorkerList(newPage, obj, vm.orderInfo, userID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.workers && response.data.workers.length > 0) {
                        vm.totalWorker = response.data.total;
                        vm.workerList = response.data.workers;

                        angular.forEach(vm.workerList, function(value, key) {
                            vm.workerList[key].assessmentFlag = '';

                            var suburb = [];
                            suburb = value.workers_suburb;
                            var arr = suburb.split(',');
                            
                            angular.forEach(vm.areaList, function(values) {
                                if(arr[0] == values.suburb_id ){
                                    vm.workerList[key].suburbA = values.suburb_name;
                                }

                                if(arr[1] == values.suburb_id ){
                                    vm.workerList[key].suburbB = values.suburb_name;
                                }

                                if(arr[2] == values.suburb_id ){
                                    vm.workerList[key].suburbC = values.suburb_name;
                                }

                                if(arr[3] == values.suburb_id ){
                                    vm.workerList[key].suburbD = values.suburb_name;
                                }
                            });

                            var workersID = value.user_id;
                            CafeUserService.checkAssessment(userID, workersID).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.status == 1) {
                                        if(responses.data.assessment.length == 1){
                                            vm.workerList[key].statusFlag = responses.data.assessment[0].status;
                                            if(responses.data.assessment[0].request_assessLevel == 0){
                                                vm.workerList[key].assessmentFlag = 0;
                                            }else{
                                                vm.workerList[key].assessmentFlag = 1;
                                            }
                                        }else{
                                            vm.assessment = responses.data.assessment;
                                            angular.forEach(vm.assessment, function(values) {
                                                vm.workerList[key].statusFlag = values.status;
                                                if(values.request_assessLevel == 0){
                                                    vm.workerList[key].assessmentFlag = 0;
                                                }else{
                                                    vm.workerList[key].assessmentFlag = 1;
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                            
                        });

                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWorkerList();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getWorkerList(1, '');
        }//END reset();

        /* to get worker booking list */
        function getBookingList(newPage, obj) {
            vm.progressbar.start();
            vm.totalBooking = 0;
            vm.bookingList = [];
            CafeUserService.getBookingList(newPage, obj, vm.orderInfo, userID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.booking && response.data.booking.length > 0) {
                        vm.totalBooking = response.data.total;
                        vm.bookingList = response.data.booking;

                        angular.forEach(vm.bookingList, function(value, key) {
                            vm.bookingList[key].assessmentFlag = '';

                            var workersID = value.user_id;
                            CafeUserService.checkAssessment(userID, workersID).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.status == 1) {
                                        if(responses.data.assessment.length == 1){
                                            vm.bookingList[key].statusFlag = responses.data.assessment[0].status;
                                            if(responses.data.assessment[0].request_assessLevel == 0){
                                                vm.bookingList[key].assessmentFlag = 0;
                                            }else{
                                                vm.bookingList[key].assessmentFlag = 1;
                                            }
                                        }else{
                                            vm.assessment = responses.data.assessment;
                                            angular.forEach(vm.assessment, function(values) {
                                                vm.bookingList[key].statusFlag = values.status;
                                                if(values.request_assessLevel == 0){
                                                    vm.bookingList[key].assessmentFlag = 0;
                                                }else{
                                                    vm.bookingList[key].assessmentFlag = 1;
                                                }
                                            });
                                        }
                                    }
                                }
                            });                            
                        });
                    }
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getBooking();

         /* to reset all search booking parameters in listing */
         function resetBooking() {
            vm.search = [];
            vm.hideCity = false;
            vm.hideSuburb = false;
            getBookingList(1, '');
        }//END resetBooking();

        /* call when all search booking parameters in listing */
        function searchBooking(newPage, searchInfo) {
            getBookingList(newPage, searchInfo);
        }//END searchBooking();

        /* to get single Assessor list */
        function getAssessorProfile(item,index) {
            vm.progressbar.start();
            vm.singleAssessor = [];
            var assessmentDate = item.request_date;
            var id = item.assessor_id;
            
            CafeUserService.getSingleAssessor(id, assessmentDate).then(function (response) {
                if (response.status == 200) {
                    
                    if (response.data.assessor != '') {
                        console.log(response.data.assessor);
                        vm.singleAssessor = response.data.assessor;
                        vm.singleAssessor.request_date = item.request_date;
                        vm.singleAssessor.request_timeFrom = item.request_timeFrom;
                        vm.singleAssessor.request_timeTo = item.request_timeTo;
                        vm.singleAssessor.request_assessLevel = item.request_assessLevel;
                        vm.singleAssessor.status = item.status;
                    }

                    $timeout( function(){
                        var logElem = angular.element("#Booking_details_model");
                        logElem.addClass("in show");
                        vm.progressbar.complete();
                    }, 1000 );                
                                        
                }            
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
            
        }//END getAssessorProfile();

        /* to hide popup */
        function closePopup(){
            var logElem = angular.element("#Booking_details_model");
            logElem.removeClass("in show");
        }//END closePopup();

        /* to get all area [Suburb]  */
        function getAllArea(){
            CafeUserService.getAllArea().then(function (response) {
                if (response.status == 200) {
                    if (response.data.area && response.data.area.length > 0) {
                        vm.areaList = response.data.area;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllArea();

        /* to get all skills  */
        function getAllSkill(){
            CafeUserService.getAllSkill().then(function (response) {
                if (response.status == 200) {
                    if (response.data.skill && response.data.skill.length > 0) {
                        vm.skillList = response.data.skill;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllSkill();

        /* Get All Countries */
        function getAllCountries(){
            CafeUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){     
            CafeUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }else{
                        vm.hideCity = true;
                        delete vm.workerForm.workers_state;
                        delete vm.workerForm.workers_city;
                        delete vm.workerForm.suburb1;
                        delete vm.workerForm.suburb2;
                        delete vm.workerForm.suburb3;
                        delete vm.workerForm.suburb4;

                        delete vm.search.cityID;
                        delete vm.search.suburb;
                    }                    
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            CafeUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        vm.hideSuburb = true;
                        delete vm.workerForm.workers_city;
                        delete vm.workerForm.suburb1;
                        delete vm.workerForm.suburb2;
                        delete vm.workerForm.suburb3;
                        delete vm.workerForm.suburb4;

                        if(typeof vm.search !== 'undefined'){
                            delete vm.search.cityID;
                            delete vm.search.suburb;    
                        }
                        
                    }                    
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            // console.log(obj);
            CafeUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList  = response.data.suburb;
                        // vm.suburbListA = response.data.suburb;
                        // vm.suburbListB = response.data.suburb;
                        // vm.suburbListC = response.data.suburb;
                        // vm.suburbListD = response.data.suburb;
                        vm.hideSuburb = false;          
                    }else{
                        vm.hideSuburb = true;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getSuburbByID();

        /* Get Mult Suburb by ID */
        function getSuburbByMID(obj, idType){
            var suburb1 = obj.suburb1;
            var suburb2 = obj.suburb2;
            var suburb3 = obj.suburb3;
            var suburb4 = obj.suburb4;

            CafeUserService.getSuburbByMID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        
                        if(vm.editFlag == true){
                            return false;
                        }

                        if(total == 4){
                            if(idType == 1){                                  
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name }
                                        vm.suburbListB.push(tet);
                                        vm.suburbListC.push(tet);
                                        vm.suburbListD.push(tet); 
                                    }                                                
                                });

                                angular.forEach(vm.suburbListB, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListB.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListC, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListC.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListD, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListD.splice(key,1);
                                    }
                                });                        
                            }

                            if(idType == 2){                                
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name };
                                        vm.suburbListA.push(tet);
                                        vm.suburbListC.push(tet);
                                        vm.suburbListD.push(tet); 
                                    }                                                
                                });
                                
                                angular.forEach(vm.suburbListA, function(value, key) {
                                    if(suburb2 == value.suburb_id){
                                        vm.suburbListA.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListC, function(value, key) {
                                    if(suburb2 == value.suburb_id){
                                        vm.suburbListC.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListD, function(value, key) {
                                    if(suburb2 == value.suburb_id){
                                        vm.suburbListD.splice(key,1);
                                    }
                                });                                
                            }

                            if(idType == 3){                                
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name }
                                        vm.suburbListB.push(tet);
                                        vm.suburbListA.push(tet);
                                        vm.suburbListD.push(tet); 
                                    }                                                
                                });

                                angular.forEach(vm.suburbListB, function(value, key) {
                                    if(suburb3 == value.suburb_id){
                                        vm.suburbListB.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListA, function(value, key) {
                                    if(suburb3 == value.suburb_id){
                                        vm.suburbListA.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListD, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListD.splice(key,1);
                                    }
                                });                                
                            }

                            if(idType == 4){                                
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name }
                                        vm.suburbListB.push(tet);
                                        vm.suburbListC.push(tet);
                                        vm.suburbListA.push(tet); 
                                    }                                                
                                });

                                angular.forEach(vm.suburbListB, function(value, key) {
                                    if(suburb4 == value.suburb_id){
                                        vm.suburbListB.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListC, function(value, key) {
                                    if(suburb4 == value.suburb_id){
                                        vm.suburbListC.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListA, function(value, key) {
                                    if(suburb4 == value.suburb_id){
                                        vm.suburbListA.splice(key,1);
                                    }
                                });                                
                            }
                        }else{

                            if(idType == 1){
                                if(typeof suburb2 == 'undefined'){
                                    vm.suburbListB = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListB, function(value, key) {
                                        if(suburb1 == value.suburb_id){
                                            vm.suburbListB.splice(key,1);              
                                        }                
                                    });
                                }

                                if(typeof suburb3 == 'undefined'){
                                    vm.suburbListC = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListC, function(value, key) {
                                        if(suburb1 == value.suburb_id){
                                            vm.suburbListC.splice(key,1);              
                                        }                
                                    });
                                }
                                
                                if(typeof suburb4 == 'undefined'){
                                    vm.suburbListD = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListD, function(value, key) {
                                        if(suburb1 == value.suburb_id){
                                            vm.suburbListD.splice(key,1);              
                                        }                
                                    });
                                }
                                total = total + 1; 
                            }
                        
                            if(idType == 2){                                
                                if(typeof suburb3 == 'undefined'){
                                    vm.suburbListC = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListC, function(value, key) {
                                        if(suburb2 == value.suburb_id){
                                            vm.suburbListC.splice(key,1);              
                                        }                
                                    });
                                }
                                
                                if(typeof suburb4 == 'undefined'){
                                    vm.suburbListD = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListD, function(value, key) {
                                        if(suburb2 == value.suburb_id){
                                            vm.suburbListD.splice(key,1);              
                                        }                
                                    });
                                }

                                if(typeof suburb1 == 'undefined'){
                                    vm.suburbListA = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListA, function(value, key) {
                                        if(suburb2 == value.suburb_id){
                                            vm.suburbListA.splice(key,1);              
                                        }                
                                    });
                                }
                                total = total + 1;
                            }
                        
                            if(idType == 3){                                
                                if(typeof suburb4 == 'undefined'){
                                    vm.suburbListD = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListD, function(value, key) {
                                        if(suburb3 == value.suburb_id){
                                            vm.suburbListD.splice(key,1);              
                                        }                
                                    });
                                }

                                if(typeof suburb1 == 'undefined'){
                                    vm.suburbListA = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListA, function(value, key) {
                                        if(suburb3 == value.suburb_id){
                                            vm.suburbListA.splice(key,1);                 
                                        }                
                                    });
                                }

                                if(typeof suburb2 == 'undefined'){
                                    vm.suburbListB = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListB, function(value, key) {
                                        if(suburb3 == value.suburb_id){
                                            vm.suburbListB.splice(key,1);                           
                                        }                
                                    });
                                }
                                total = total + 1;
                            }
                        
                            if(idType == 4){
                                if(typeof suburb1 == 'undefined'){
                                    vm.suburbListA = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListA, function(value, key) {
                                        if(suburb4 == value.suburb_id){
                                            vm.suburbListA.splice(key,1); 
                                        }                
                                    });
                                }

                                if(typeof suburb2 == 'undefined'){
                                    vm.suburbListB = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListB, function(value, key) {
                                        if(suburb4 == value.suburb_id){
                                            vm.suburbListB.splice(key,1);                           
                                        }                
                                    });
                                }

                                if(typeof suburb3 == 'undefined'){
                                    vm.suburbListC = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListC, function(value, key) {
                                        if(suburb4 == value.suburb_id){
                                            vm.suburbListC.splice(key,1);                           
                                        }                
                                    });
                                }
                                total = total + 1;
                            }                            
                        }
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });            
        }// END getSuburbByID();

        /* Get All States  */
        function getAllStates(){            
            CafeUserService.getAllStates(userID, userRoleID).then(function (response) {  
                if (response.status == 200) {
                    if (response.data.state && response.data.state.length > 0) {
                        vm.statesList = response.data.state;                        
                    }else{
                        vm.hideCity = true;
                    }                    
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllStates();

        /* to get single worker */
        function getSingleWorker(workers_id) {
            CafeUserService.getSingleWorker(workers_id).then(function (response) {                 
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorker = response.data.worker;
                        vm.workerForm = response.data.worker;

                        if(vm.workerForm.workers_mobile == 0) {
                            vm.workerForm.workers_mobile = '';
                        }

                        if(vm.workerForm.worker_type == 1) {
                            vm.workerForm.workerType = vm.workerForm.worker_type;
                        }

                        vm.singleWorker.assessmentFlag = '';
                        vm.workerForm.digitalAgreement = vm.workerForm.workers_digitalAgreement;
                        vm.workerForm.friendlyCats = vm.workerForm.workers_friendlyCats;
                        vm.workerForm.friendlyDogs = vm.workerForm.workers_friendlyDogs;
                        var worSub = vm.workerForm.workers_suburb;
                        var workerSuburbsId = worSub.split(',');
                        
                            var workerSub = [];
                            angular.forEach(vm.areaList, function(subValue,subKey) {
                                angular.forEach(workerSuburbsId, function(val) {
                                    if(subValue.suburb_id == val) {
                                        workerSub.push(subValue.suburb_name);
                                    }
                                });
                            });
                            vm.singleWorker.workerSubName = workerSub.toString();

                        angular.forEach(vm.singleWorker, function(value, key) {
                            
                            
                            // if(key == 'workers_suburb'){
                            //     if(value != ''){
                            //         var suburb = [];
                                    
                            //         suburb = value;
                            //         var arr = suburb.split(',');
                                    
                            //         angular.forEach(vm.areaList, function(values) {
                            //             if(arr[0] == values.suburb_id){
                            //                 // vm.singleWorker.suburbA = values.suburb_name;
                            //                 vm.workerForm.suburb1 = values.suburb_id;
                            //             }
                            //             console.log(vm.workerForm.suburb1);
                            //             // if(arr[1] == values.suburb_id ){
                            //             //     vm.singleWorker.suburbB = values.suburb_name;
                            //             //     vm.workerForm.suburb2 = values.suburb_id;
                            //             // }
        
                            //             // if(arr[2] == values.suburb_id ){
                            //             //     vm.singleWorker.suburbC = values.suburb_name;
                            //             //     vm.workerForm.suburb3 = values.suburb_id;
                            //             // }
        
                            //             // if(arr[3] == values.suburb_id ){
                            //             //     vm.singleWorker.suburbD = values.suburb_name;
                            //             //     vm.workerForm.suburb4 = values.suburb_id;
                            //             // }
                            //         });
                            //     }
                            // }

                            if(key == 'workers_image'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file = responses.data.file.file_base_url;
                                                vm.workerForm.workers_image1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_image = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_codeConduct'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file1 = responses.data.file.file_base_url;
                                                vm.workerForm.workers_codeConduct1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_codeConduct = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_criminalReport'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file2 = responses.data.file.file_base_url;
                                                vm.workerForm.workers_criminalReport1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_criminalReport = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_photoIdentity'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file3 = responses.data.file.file_base_url;
                                                vm.workerForm.workers_photoIdentity1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_photoIdentity = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_skills'){
                                if(value != ''){
                                    vm.skills = [];
                                    
                                    var skill = [];
                                    skill = value;
                                    var arrs = skill.split(',');

                                    for(var i = 0; i < arrs.length; i++ ){
                                    
                                        angular.forEach(vm.skillList, function(values) {
                                            if(arrs[i] == values.skills_id ){
                                                vm.skills.push(values.skills_name);
                                            }
                                                    
                                        });
                                    }
                                }
                            }
                        });

                        if(vm.singleWorker.workers_country != 0){
                            var country = vm.singleWorker.workers_country;
                            vm.getAssCountry = vm.singleWorker.workers_country;
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleWorker.country = value.name;
                                }                               
                            }); 
                        }

                        if(vm.singleWorker.workers_phone != 0){
                            var phone = vm.singleWorker.workers_phone;
                            // vm.singleWorker.workers_phone = phone.toString().slice(2);
                            vm.singleWorker.workers_phone = phone;
                        }

                        if(vm.singleWorker.workers_mobile != 0){
                            var mobile = vm.singleWorker.workers_mobile;
                            // vm.singleWorker.workers_mobile = mobile.toString().slice(2);
                            vm.singleWorker.workers_mobile = mobile;
                        }
                        
                        var workersID = workers_id; 
                        CafeUserService.checkAssessment(userID, workersID).then(function (responses) {
                            
                            if (responses.status == 200) {
                                if (responses.data.status == 1) {
                                    if(responses.data.assessment.length == 1){
                                        vm.singleWorker.statusFlagR = 0;
                                        vm.singleWorker.statusFlag = responses.data.assessment[0].status;
                                        if(responses.data.assessment[0].request_assessLevel == 0){
                                            vm.singleWorker.assessmentFlag = 0;
                                        }else{
                                            vm.singleWorker.assessmentFlag = 1;
                                        }

                                        if(vm.singleWorker.statusFlag == 4){
                                            var month = moment(responses.data.assessment[0].request_date).add(6, 'months').format("YYYY-MM-DD"); 

                                            var now = moment().format("YYYY-MM-DD");
                                            
                                            if(now >= month ){
                                                vm.singleWorker.statusFlagR = 1;
                                            }
                                        }

                                    }else{
                                        vm.assessment = responses.data.assessment;
                                        angular.forEach(vm.assessment, function(values) {
                                            vm.singleWorker.statusFlagR = 0;
                                            vm.singleWorker.statusFlag = values.status;
                                            if(values.request_assessLevel == 0){
                                                vm.singleWorker.assessmentFlag = 0;
                                            }else{
                                                vm.singleWorker.assessmentFlag = 1;
                                            }

                                            if(values.status == 4){
                                                var month = moment(values.request_date).add(6, 'months').format("YYYY-MM-DD"); 
    
                                                var now = moment().format("YYYY-MM-DD");
    
                                                if(now >= month ){
                                                    vm.singleWorker.statusFlagR = 1;
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });

                        vm.getStatesByID(vm.singleWorker);
                        vm.getCityByID(vm.singleWorker);
                        vm.getSuburbByID(vm.singleWorker);

                        
                        delete vm.workerForm.workers_zip;
                        delete vm.workerForm.workers_suspend_start;
                        delete vm.workerForm.workers_suspend_end;
                        delete vm.workerForm.workers_skills;
                        delete vm.workerForm.workers_lat;
                        delete vm.workerForm.workers_long;
                        delete vm.workerForm.workers_booking_rating;

                        vm.isDisabled = true;
                        
                        $timeout( function(){
                            vm.progressbar.complete();
                        }, 300 );
                    } else {
                        vm.progressbar.complete();
                        toastr.error(response.data.message, 'Internet Cafe');
                        $state.go('frontoffice');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                toastr.error(error.data.error, 'Internet Cafe');
            });
        }//END getSingleWorker();

        /* Image upload function */
        function Uploads(file, flag){ 
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    if(flag == 1){
                        vm.workerForm.workers_image = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;                        
                        vm.imageFlag = true;
                    }
                    if(flag == 2){
                        vm.workerForm.workers_photoIdentity = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;
                        vm.imageFlag = true;
                    }
                    if(flag == 3){
                        vm.workerForm.workers_criminalReport = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;
                        vm.imageFlag = true;
                    }
                    if(flag == 4){
                        vm.workerForm.workers_codeConduct = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;
                        vm.imageFlag = true;
                    }                                      
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error('Internal server error', 'Error');
            });
        }//END Uploads();

        /*Image Upload*/
        vm.upload = function (file, idType) {
            
            if(idType == 1){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }                
            }

            if(idType == 2){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }
            }
            
            if(idType == 3){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }
            }

            if(idType == 4){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }
            }            
           
        };//END Image Upload

        /* Change image when worker profile */
        function changeImagePopup(imageType, id, fileType){
            vm.imageForm = [];
            vm.imageForm.imageType = imageType;
            vm.imageForm.user_id = id;
            
            var file_id = fileType;
            CafeUserService.getImage(file_id).then(function (responses) {
                if (responses.status == 200) {
                    if (responses.data.file && responses.data.file != '') {
                        vm.imageForm.file = responses.data.file.file_base_url;
                    }

                    var imageElem = angular.element("#imageDetails_model");
                    imageElem.addClass("in show");
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeImagePopup();

        /* to hide popup */
        function closeImagePop(){
            var logElem = angular.element("#imageDetails_model");
            logElem.removeClass("in show");
        }//END closeImagePop();

        /* to save log after add and edit  */
        function saveImage() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            if (vm.imageForm.file !== '') { //check if from is valid
                
                if(vm.imageForm.file.size > 0 && vm.imageForm.file.name !== '' ){
                    vm.upload(vm.imageForm.file, vm.imageForm.imageType ); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        CafeUserService.saveImage(vm.imageForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.success(response.data.message, 'Internet Cafe');
                                    var logElem = angular.element("#imageDetails_model");
                                    logElem.removeClass("in show");
                                    $state.reload();                      
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Internet Cafe');
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Internet Cafe');
                            }                            
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Internet Cafe');
                        });
                    }
                }, 10000 );
            }
        }//END saveImage(); 

        /* to save worker user after add and edit  */
        function saveWorker(imgname1, imgname2, imgname3, imgname4) {
            // console.log(vm.workerForm.workers_idPassport);
            // console.log(vm.workerForm.workers_asylum);
            if(typeof vm.workerForm.workers_mobile !== "undefined"){
                if(vm.workerForm.workers_mobile == vm.workerForm.workers_phone){
                    toastr.error("Alternate Number must not be same as Contact Number!", 'Internet Cafe');
                    return false;
                }
            }
            
            if(typeof vm.workerForm.workers_birthDate !== "undefined"){
                var birthDate = vm.formatDateOfBirth(vm.workerForm.workers_birthDate);
                vm.workerForm.workers_age = vm.ageCalculate(birthDate);
            }

            if(typeof vm.workerForm.workers_expiryDateDay == "undefined" || vm.workerForm.workers_expiryDateDay == ''){
                vm.workerForm.workers_expiryDateDay = '';
            } 

            // if(typeof vm.workerForm.workers_city !== "undefined"){
                if(vm.workerForm.workers_idPassport == vm.workerForm.workers_asylum){
                    toastr.error("ID/Passport number and Asylum number should not be same.", 'Internet Cafe');
                    return false;
                }
            // }

            vm.isDisabledButton = true;
            vm.progressbar.start();
            
            
            if(vm.editFlag == false){
                if(typeof imgname1 !== 'undefined'){
                    vm.upload(imgname1[0], 1);
                }

                if(typeof imgname2 !== 'undefined'){
                    vm.upload(imgname2[0], 2);
                }else{
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error("Please upload photo ID!", 'Internet Cafe');
                    return false;
                }

                if(typeof imgname3 !== 'undefined'){
                    vm.upload(imgname3[0], 3);
                }else{
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error("Please upload criminal report!", 'Internet Cafe');
                    return false;
                }

                if(typeof imgname4 !== 'undefined'){
                    vm.upload(imgname4[0], 4);
                }else{
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error("Please upload code of conduct!", 'Internet Cafe');
                    return false;
                }
            }
                
            $timeout( function(){                
                vm.workerForm.cafeuser_id = userID;
                // console.log(vm.workerForm);
                CafeUserService.saveWorker(vm.workerForm).then(function (response) {
                    // console.log(response);
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            vm.progressbar.complete();
                            toastr.success(response.data.message, 'Internet Cafe');
                            $state.go('frontoffice.cafe');
                        } else {
                            if (response.data.status == 2) {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Internet Cafe');
                            }else{
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Internet Cafe');
                            }                        
                        }
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Internet Cafe');
                    }
                }, function (error) {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error('Internal server error', 'Internet Cafe');
                });                    
            }, 5000 );
        }//END saveWorker();

        /** to delete a Internet Cafe Worker **/
        function deleteWorker(id, index) {                                  
            SweetAlert.swal({
                title: "Are you sure you want to delete this Worker?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CafeUserService.deleteWorker(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.workerList.splice(index, 1);
                            vm.totalWorker = vm.totalWorker - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Internet Cafe');
                    });
                }
            });                   
        }//END deleteWorker();

        /* to reset all search Assessment parameters in listing */
        function resetAssessment() {
            vm.search = [];
            vm.hideCity = false;
            vm.hideSuburb = false;
            getAssessmentList(1, '');
        }//END resetAssessment();


        /* to reset worker register form */
        function resetWorkerRegi() {
           vm.workerForm = '';
           
        }//END resetAssessment();        

        /* call when all search Assessment parameters in listing */
        function searchAssessment(newPage, searchInfo) {
            getAssessmentList(newPage, searchInfo);
        }//END searchAssessment();

        /* to get Assessor list */
        function getAssessmentList(newPage, obj) {
            // obj.getAssCountry = vm.getAssCountry;
            vm.totalAssessor = 0;
            vm.assessorList = [];
            CafeUserService.getAssessorList(newPage, obj, vm.getAssCountry).then(function (response) {
                if (response.status == 200) {
                    if (response.data.assessors && response.data.assessors.length > 0) {
                        vm.totalAssessor = response.data.total;
                        vm.assessorList = response.data.assessors;

                        angular.forEach(vm.assessorList, function(value, key) {
                            
                            var assessorID = value.user_id;
                            // CafeUserService.checkLeave(assessorID, obj).then(function (respon) {
                            //     if (respon.status == 200) {
                            //         if (respon.data.status == 1 && respon.data.dataCheck == 1) {
                            //             vm.assessorList.splice(key);
                            //             vm.totalAssessor = vm.totalAssessor - 1;
                            //         }else{
                                        CafeUserService.checkAssessor(assessorID, obj).then(function (responses) {
                                            if (responses.status == 200) {
                                                if (responses.data.status == 1) {
                                                    if(responses.data.assessment.length == 4 ){
                                                        vm.assessorList.splice(key);
                                                        vm.totalAssessor = vm.totalAssessor - 1;
                                                    }else{
                                                        if(typeof obj !== 'undefined' && obj !== ''){
                                                            if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                                                                vm.assessorList[key].assessmentDate = obj.start_at;
                                                            }else{
                                                                vm.assessorList[key].assessmentDate = vm.myDate;
                                                            }
                                                        }else{
                                                            vm.assessorList[key].assessmentDate = vm.myDate;
                                                        }
                                                    }
                                                }else{
                                                    if(typeof obj !== 'undefined' && obj !== ''){
                                                        if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                                                            vm.assessorList[key].assessmentDate = obj.start_at;
                                                        }else{
                                                            vm.assessorList[key].assessmentDate = vm.myDate;
                                                        }
                                                    }else{
                                                        vm.assessorList[key].assessmentDate = vm.myDate;
                                                    }
                                                }
                                            }
                                        });
                            //         }
                            //     }
                            // });
                            
                        });
                        vm.progressbar.complete();
                    }
                }               
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAssessmentList();

        /* to get single Assessor list */
        function workerAssessment(id, obj, index) {
            vm.progressbar.start();
            vm.singleAssessor = [];
            vm.timeSlot = [];
            vm.timeSlots = [];
            var defaultSlot = 0;
            var assessmentDate = '';
            if(typeof obj !== 'undefined' && obj !== ''){
                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    assessmentDate = obj.start_at;
                }else{
                    assessmentDate = vm.myDate;
                }
            }else{
                assessmentDate = vm.myDate;
            }

            CafeUserService.getSingleAssessor(id, assessmentDate).then(function (response) {
                if (response.status == 200) {
                    
                    if (response.data.assessor != '') {
                        vm.singleAssessor = response.data.assessor;
                    }

                    if (response.data.timeSlot != '') {
                        vm.timeSlot = response.data.timeSlot;
                    }

                    if (response.data.defaultSlot != '') {
                        defaultSlot = parseInt(response.data.defaultSlot.setting_value);
                        defaultSlot = (defaultSlot * 60);
                    }
                    
                    vm.timeSlots = createTimeSlots(9, 18, defaultSlot);
                    
                    $timeout( function(){
                        
                        if(vm.timeSlot.length == ''){
                            angular.forEach(vm.timeSlots, function(value1, key1) {
                                
                                var path = value1.timeSlotKey.split("-");  
                                var startTime = path[0];
                                var endTime = path[1]; 
                                vm.timeSlots[key1].bookFlag = 0;
                                vm.timeSlots[key1].request_date = assessmentDate;
                                vm.timeSlots[key1].request_timeFrom = startTime;
                                vm.timeSlots[key1].request_timeTo = endTime;
                                vm.timeSlots[key1].worker_id = vm.workerID;
                            
                            });
                        }else{
                            angular.forEach(vm.timeSlots, function(value1, key1) {
                                angular.forEach(vm.timeSlot, function(value2, key2) { 
                                    
                                    var time = vm.formatTime(value2.request_timeFrom)+'-'+vm.formatTime(value2.request_timeTo);
                                    var path = value1.timeSlotKey.split("-");  
                                    var startTime = path[0];
                                    var endTime = path[1]; 
                                    
                                    vm.timeSlots[key1].request_date = assessmentDate;
                                    vm.timeSlots[key1].request_timeFrom = startTime;
                                    vm.timeSlots[key1].request_timeTo = endTime;
                                    vm.timeSlots[key1].worker_id = vm.workerID;

                                    if(value1.timeSlotKey == time){                
                                        vm.timeSlots[key1].bookFlag = 1;
                                    } 
    
                                });
                            });
                        }
                        
                        var logElem = angular.element("#bo_accepted_new_model");
                        logElem.addClass("in show");
                        vm.progressbar.complete();
                    }, 1000 );                
                                        
                }            
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
            
        }//END workerAssessment();

        /* to hide popup */
        function closePop(){
            var logElem = angular.element("#bo_accepted_new_model");
            logElem.removeClass("in show");
        }//END closePop();

        /* create time slots */
        function createTimeSlots(startHour, endHour, interval) {
            if (!startHour) {
                endHour = 8;
            }
            if (!endHour) {
                endHour = 20;
            }
            var timeSlots = [], dateTime = new Date(), timeStr = '';
            dateTime.setHours(startHour, 0, 0, 0);
            while (new Date(dateTime.getTime() + interval * 60000).getHours() < endHour) {
                timeStr = addZero(dateTime.getHours()) + ':' + addZero(dateTime.getMinutes());
                timeStr += '-';
                dateTime = new Date(dateTime.getTime() + interval * 60000);
                timeStr += dateTime.getHours() + ':' + addZero(dateTime.getMinutes());
                // timeSlots.push(timeStr);
                timeSlots.push({timeSlotKey : timeStr, bookFlag : 0 });
            }
            return timeSlots;
        }//END createTimeSlots();

        /* add zero */
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }//END addZero();

        /* Book worker for assessment to assessor */
        function bookAssessor(obj, id) {
            var assessorID = id;
            CafeUserService.bookAssessmentRequest(obj, assessorID, userID, userName).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Internet Cafe');
                        $state.go('frontoffice.cafe');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Internet Cafe');
            });
            
        }//END bookAssessor();
        
    }//END CafeUserController

}());

(function () {

    angular.module('assessorApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var assessorPath = 'app/components/assessor/';
        $stateProvider
            .state('frontoffice.assessor', {
                url: 'assessor',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/index.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.requests', {
                url: '/requests',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/request.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.calender', {
                url: '/calender',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/calender.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.checklist', {
                url: '/checklist',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/checklist.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.angels', {
                url: '/angels',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/my_added_angels.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.notification', {
                url: '/notification',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/notification.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.workerView', {
                url: '/workerView/:id/:request_id',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/worker_profile.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.allAssessors', {
                url: '/allAssessors/:ids',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/book_assessor.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.assessment', {
                url: '/assessment/:id/:request_id',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/assessment.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })            

    }

}());
(function () {
    "use strict";
    angular
        .module('assessorApp')
        .service('AssessorService', AssessorService);

    AssessorService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AssessorService($http, APPCONFIG, $q) {

        /* get all checklist data */
        function getChecklist() {
            var URL = APPCONFIG.APIURL + 'assessor/checklist';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getChecklist();

        /* get all Notifications */
        function getNotifications(user_id) {

            var URL = APPCONFIG.APIURL + 'assessor/notification';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getNotifications();    

        /* clear all Notifications */
        function clearAllNotifications(user_id) {
           
            var URL = APPCONFIG.APIURL + 'assessor/deleteNot';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END clearAllNotifications();                

        /* get all requests */
        function getAllRequests(pageNum, obj, orderInfo, user_id) {

            var URL = APPCONFIG.APIURL + 'assessor/requests';
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof user_id !== 'undefined' && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.name !== 'undefined' && obj.name !== '') {
                    requestData['name'] = obj.name;
                }

                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }

                if (typeof obj.endDate !== 'undefined' && obj.endDate !== '') {
                    requestData['endDate'] = moment(obj.endDate).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        } //END getAllRequests(); 

        /* reject worker request */
        function rejectRequest(request_id) {

            var URL = APPCONFIG.APIURL + 'assessor/rejectWorker';
            var requestData = {};

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END rejectRequest();  

        /* accept worker request */
        function acceptRequest(request_id) {

            var URL = APPCONFIG.APIURL + 'assessor/acceptWorker';
            var requestData = {};

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END acceptRequest();  


        /* to get single worker */
        function getSingleWorker(user_id, request_id) {
            var URL = APPCONFIG.APIURL + 'assessor/workerProfile';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWorker();


        /* to my added workers */
        function myAddedAngels(pageNum, obj, orderInfo, assessor_id) {

            var URL = APPCONFIG.APIURL + 'assessor/myWorker';
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof assessor_id !== 'undefined' && assessor_id !== '') {
                requestData['assessor_id'] = assessor_id;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.name !== 'undefined' && obj.name !== '') {
                    requestData['name'] = obj.name;
                }

                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }

                if (typeof obj.endDate !== 'undefined' && obj.endDate !== '') {
                    requestData['endDate'] = moment(obj.endDate).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.level !== 'undefined' && obj.level !== '') {
                    requestData['level'] = obj.level;
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        } //END myAddedAngels();

        /* to get all assessors listing  */
        function getAllAssessors(pageNum, obj, orderInfo, user_id, workerId) {

            var URL = APPCONFIG.APIURL + 'assessor/allAssessors';
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof user_id !== 'undefined' && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof workerId !== 'undefined' && workerId !== '') {
                requestData['workerId'] = workerId;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }

            }

            // if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
            //     if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
            //         requestData['orderColumn'] = orderInfo.orderColumn;
            //     }
            //     if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
            //         requestData['orderBy'] = orderInfo.orderBy;
            //     }
            // }

            return this.runHttp(URL, requestData);
        } //END getAllAssessors();  


        /* assessor's leave data */
        function getUnavailabilityData(assessor_id, obj) {
            var URL = APPCONFIG.APIURL + 'assessor/getUnavailabilityData';
            var requestData = {};

            if (typeof assessor_id !== 'undefined' && assessor_id !== '') {
                requestData['assessor_id'] = assessor_id;
            }

            if(typeof obj !== 'undefined' && obj !== ''){

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }else{
                    requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
                }
            }else{
                requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getUnavailabilityData();


       /* assessor's rquest data */
        function getRequestBookAssessor(assessor_id, obj) {
            var URL = APPCONFIG.APIURL + 'assessor/getRequestBookAssessor';
            var requestData = {};

            if (typeof assessor_id !== 'undefined' && assessor_id !== '') {
                requestData['assessor_id'] = assessor_id;
            }
            
            if(typeof obj !== 'undefined' && obj !== ''){

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }else{
                    requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
                }
            }else{
                requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getRequestBookAssessor();           


        /* to get all skill set */
        function getAllSkillSet() {
            var URL = APPCONFIG.APIURL + 'assessor/skills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getAllSkillSet();                    


        /* to get skill name by id */
        function getSkillById(id) {
            var URL = APPCONFIG.APIURL + 'assessor/skillById';
            var requestData = {};

            if (typeof id !== undefined && id !== '') {
                requestData['id'] = id;
            }

            return this.runHttp(URL, requestData);
        } //END getSkillById();        


        /* to save skill in skill set */
        function saveExtraSkill(skillName) {
            var URL = APPCONFIG.APIURL + 'assessor/addskill';
            var requestData = {};

            if (typeof skillName !== undefined && skillName !== '') {
                requestData['skillName'] = skillName;
            }

            return this.runHttp(URL, requestData);
        } //END saveExtraSkill(); 

        /* get assessor's time slot for particular date */
        function getTimeSlots(assessorId, workerID, assessmentDate) {
            var URL = APPCONFIG.APIURL + 'assessor/getTimeSlots';
            var requestData = {};

            if (typeof assessorId !== undefined && assessorId !== '') {
                requestData['assessorId'] = assessorId;
            }

            if (typeof workerID !== undefined && workerID !== '') {
                requestData['workerID'] = workerID;
            }

            if (typeof assessmentDate !== undefined && assessmentDate !== '') {
                requestData['assessmentDate'] = moment(assessmentDate).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getTimeSlots();  


        /* Book worker's next assessment to other assessor */
        function bookNextAssessmentRequest(user_id, obj) {
            var URL = APPCONFIG.APIURL + 'assessor/bookNextAssessor';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof obj.assessorId !== undefined && obj.assessorId !== '') {
                requestData['assessorId'] = obj.assessorId;
            }

            if (typeof obj.worker_id !== undefined && obj.worker_id !== '') {
                requestData['worker_id'] = obj.worker_id;
            }

            if (typeof obj.request_date !== undefined && obj.request_date !== '') {
                requestData['request_date'] = obj.request_date;
            }

            if (typeof obj.request_timeFrom !== undefined && obj.request_timeFrom !== '') {
                requestData['request_timeFrom'] = obj.request_timeFrom;
            }

            if (typeof obj.request_timeTo !== undefined && obj.request_timeTo !== '') {
                requestData['request_timeTo'] = obj.request_timeTo;
            }

            return this.runHttp(URL, requestData);
        } //END bookNextAssessmentRequest();   


        /* save  worker's assessment ratings */
        function saveAssessmentRatings(worker, request, rateId, ratePoint, user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/workerRatings';
            var requestData = {};

            if (typeof worker !== undefined && worker !== '') {
                requestData['worker'] = worker;
            }

            if (typeof request !== undefined && request !== '') {
                requestData['request'] = request;
            }

            if (typeof rateId !== undefined && rateId !== '') {
                requestData['rateId'] = rateId;
            }

            if (typeof ratePoint !== undefined && ratePoint !== '') {
                requestData['ratePoint'] = ratePoint;
            }

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END saveAssessmentRatings();   


        /* to get calender dates */
        function getCalenderData(user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/calender';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getCalenderData();                    


        /* to get assessors's all request data for date */
        function getRequestAsByDate(user_id, searchDate) {
            var URL = APPCONFIG.APIURL + 'assessor/requestByDate';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof searchDate !== undefined && searchDate !== '') {
                requestData['searchDate'] = searchDate;
            }

            return this.runHttp(URL, requestData);
        } //END getRequestAsByDate();      

        /* save Unavailability */
        function saveUnavailable(obj, user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/updateAvailability';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.selectedDate !== 'undefined' && obj.selectedDate !== '') {
                    requestData['selectedDate'] = obj.selectedDate;
                }

                if (typeof obj.type !== 'undefined' && obj.type !== '') {
                    requestData['type'] = obj.type;
                }

                if (typeof user_id !== 'undefined' && user_id !== '') {
                    requestData['user_id'] = user_id;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveUnavailable();


        /* save Availability  */
        function saveAvailable(obj, user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/updateAvailability';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.selectedDate !== 'undefined' && obj.selectedDate !== '') {
                    requestData['selectedDate'] = obj.selectedDate;
                }

                if (typeof obj.type !== 'undefined' && obj.type !== '') {
                    requestData['type'] = obj.type;
                }

                if (typeof user_id !== 'undefined' && user_id !== '') {
                    requestData['user_id'] = user_id;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveAvailable();


        /* to get request data by id*/
        function getRequestDataById(request_id) {
            var URL = APPCONFIG.APIURL + 'assessor/requestById';
            var requestData = {};

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END getRequestDataById();      



        /* save Reschedule  */
        function saveReschedule(obj) {
            var URL = APPCONFIG.APIURL + 'assessor/rescheduleRequest';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.rescheduleDate !== 'undefined' && obj.rescheduleDate !== '') {
                    requestData['rescheduleDate'] = obj.rescheduleDate;
                }

                if (typeof obj.request_id !== 'undefined' && obj.request_id !== '') {
                    requestData['request_id'] = obj.request_id;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveReschedule();


        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get States
        function getStatesByID(userID, userRoleID){
            var URL = APPCONFIG.APIURL + 'assessor/getProvince';
            var requestData = {};

            if (typeof userID !== 'undefined' && userID !== '') {                
                requestData['userID'] = parseInt(userID);            
            }            
            if (typeof userRoleID !== 'undefined' && userRoleID !== '') {                
                requestData['userRoleID'] = parseInt(userRoleID);            
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get city 
        function getCityByID(stateId){
            var URL = APPCONFIG.APIURL + 'assessor/getAllCities';
            var requestData = {};

                if (typeof stateId !== 'undefined' && stateId !== '') {
                    requestData['stateId'] = parseInt(stateId);
                }
            
            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get suburb by ID 
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }
                
                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['ID'] = parseInt(obj.cityID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        //Get all suburb 
        function getAllArea(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbs';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllArea();

        //Get all skill 
        function getAllSkill(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSkills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllSkill();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();         

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getChecklist: getChecklist,
            getNotifications:getNotifications,
            clearAllNotifications: clearAllNotifications,
            getAllRequests:getAllRequests,
            rejectRequest:rejectRequest,
            acceptRequest:acceptRequest,
            getSingleWorker:getSingleWorker,
            myAddedAngels:myAddedAngels,
            getAllAssessors:getAllAssessors,
            getUnavailabilityData:getUnavailabilityData,
            getRequestBookAssessor:getRequestBookAssessor,
            getAllSkillSet:getAllSkillSet,
            getSkillById:getSkillById,
            saveExtraSkill:saveExtraSkill,
            bookNextAssessmentRequest:bookNextAssessmentRequest,
            saveAssessmentRatings:saveAssessmentRatings,
            getTimeSlots:getTimeSlots,
            getCalenderData:getCalenderData,
            getRequestAsByDate:getRequestAsByDate,
            saveUnavailable:saveUnavailable,
            saveAvailable:saveAvailable,
            getRequestDataById:getRequestDataById,
            saveReschedule:saveReschedule,
            getAllCountries:getAllCountries,
            getStatesByID:getStatesByID,
            getCityByID:getCityByID,
            getSuburbByID:getSuburbByID,
            getAllArea:getAllArea,
            getAllSkill:getAllSkill,
            getImage:getImage
        }

    };//END WorkerUserService()
}());

(function () {
    "use strict";
    angular.module('assessorApp')
        .controller('AssessorController', AssessorController);

    AssessorController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AssessorService', 'toastr', 'SweetAlert','$timeout','APPCONFIG', 'ngProgressFactory', '$anchorScroll'];

    function AssessorController($scope, $rootScope, $state, $location, AssessorService, toastr, SweetAlert, $timeout, APPCONFIG, ngProgressFactory, $anchorScroll) {
        var vm = this;

        $anchorScroll();
        vm.progressbar = ngProgressFactory.createInstance();
        $rootScope.bodyClass = 'assasorCafe';

      
        var user_id = $rootScope.user.user_id;
        var userRoleID = $rootScope.user.user_role_id;

        vm.today = new Date();
        vm.ErrorToster = ErrorToster;
        
        vm.getChecklist = getChecklist;
        vm.getNotification = getNotification;
        vm.clearAllNotification = clearAllNotification;
        vm.getAllRequests = getAllRequests;
        vm.rejectRequest = rejectRequest;
        vm.acceptRequest = acceptRequest;
        vm.getWorkerProfile = getWorkerProfile;
        vm.getMyAddedAngels = getMyAddedAngels;
        vm.getAllAssessors = getAllAssessors;
        vm.getAllSkillSet = getAllSkillSet;
        vm.addExtraSkillArray = addExtraSkillArray; 
        vm.saveExtraSkill = saveExtraSkill;
        vm.getTimeSlots = getTimeSlots;
        vm.getCalender = getCalender;
        vm.bookNextAssessment = bookNextAssessment;
        vm.getAssessmentRatings = getAssessmentRatings;
        vm.saveAssessmentRatings = saveAssessmentRatings;
        vm.getRequestByDate = getRequestByDate;
        vm.saveUnavailable = saveUnavailable;
        vm.saveAvailable = saveAvailable;
        vm.rescheduleRequest = rescheduleRequest;
        vm.saveReschedule = saveReschedule;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;

        vm.getAllArea = getAllArea;
        vm.getAllSkill = getAllSkill;

        vm.convertTime = convertTime;
        vm.formatDate = formatDate;
        vm.dateFormatData = dateFormatData;
        vm.changePage = changePage;
        vm.changePageAssessor = changePageAssessor;
        vm.totalRequests = 0;
        vm.listPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.reset = reset;
        vm.resetAssessment = resetAssessment;
        vm.resetCalender = resetCalender;
        vm.isDisabled = isDisabled;
        vm.bookPastDate = bookPastDate;

        vm.events = [];
        vm.extraSkillArray = [];
        vm.newSkills = [];
        vm.ratingIdData = [];
        vm.ratingPointData = [];
		
        vm.statusList = [{ id : 0, status: 'Pending'}, { id : 1, status: 'Accepted'}, { id : 2, status: 'Rejected'}, { id : 3, status: 'Qualified'}, { id : 4, status: 'Disqualified'}];
        vm.levelList = [{ id : 0, level: 'First'}, { id : 1, level: 'Final'}];

        vm.getMyAddedAngels();
        vm.getCalender();
        
        var monthS = moment().add(1, 'days');
        // vm.myDate = monthS._d;
        vm.startDate = monthS._d;
        
        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[2] == "notification") {
            vm.getNotification(user_id);
        }
        
        vm.getNotification(user_id);
        
        if (path[2] == "workerView" || path[2] == "assessment") {
            if (path[3] == "") {
                $state.go('frontoffice');
                return false;
            } else {
                vm.getAllCountries();
                vm.getAllArea();
                vm.getAllSkill();
                $timeout( function(){
                    vm.progressbar.start();
                    vm.getWorkerProfile(path[3],path[4]);
                    vm.getTimeSlots(path[3],path[4],path[5])
                }, 500 );                
            }
        }

        if(path[2] == "allAssessors"){
            if (path[3] == "") {
                $state.go('frontoffice');
                return false;
            } else {
                vm.getAllAssessors(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }
       
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* to format date with yy-mm-dd*/
        function dateFormatData(date) {
            return moment(date).format("YYYY-MM-DD");
        }//END formatDate()        

        function convertTime(time) {
            return moment(time, "HH:mm:ss").format("hh:mm A");
        }

        /* disable past date for booking*/
        function bookPastDate() {
            this.myDate = new Date();
            vm.currentDate =  new Date(    
                this.myDate.getFullYear(),    
                this.myDate.getMonth(),    
                this.myDate.getDate() + 1 
            );
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAllRequests(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAllRequests(newPage, searchInfo);

            if(path[2] == "assessors"){
                getAllAssessors(path[3], newPage, searchInfo);
            }
            getMyAddedAngels(newPage, searchInfo);
        }//END changePage();

        /* call when page changes */
        function changePageAssessor(newPage, searchInfo) {
            getAllAssessors(path[3], newPage, searchInfo);
        }//END changePageAssessor();
        

        /* to get checklist */
        function ErrorToster() {
            
            toastr.error('You can assest angel only on or before date of assessment', 'Assessment');
                
        }//END ErrorToster();

        /* to get checklist */
        function getChecklist() {
            AssessorService.getChecklist().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allChecklist = response.data.checklist;
                    } else {
                        toastr.error(response.data.message, 'Checklist');
                        $state.go('frontoffice.assessor');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Checklist');
            });
        }//END checklist();


        /* to get notification list */
        function getNotification(user_id) {
            AssessorService.getNotifications(user_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $rootScope.countNot = response.data.count;
                        vm.allNotification = response.data.notifications;
                    } else {
                        vm.allNotification.error = "No data for notifications";
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Notification');
            });
        }//END getNotification(); 

        /* to clear all notifications */
        function clearAllNotification() {
            SweetAlert.swal({
                title: "Are you sure you want to delete all notifications",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AssessorService.clearAllNotifications(user_id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.getNotification(user_id);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessor');
                    });
                }
            });
        }//END clearAllNotification();         

        /* get all request from cafe users */
        function getAllRequests(newPage, obj) {
            vm.progressbar.start();
            vm.allRequests = [];
            vm.countAllRequests = 0;

            AssessorService.getAllRequests(newPage, obj, vm.orderInfo, user_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.allRequests = response.data.request;
                        vm.countAllRequests = response.data.requestCount;
                        
                        angular.forEach(vm.allRequests, function(value1, key1) {
                            if(value1.skills != '') {
                                
                                AssessorService.getSkillById(value1.skills).then(function (response) {
                                    
                                    if (response.status == 200) {
                                        if (response.data.status == 1) {
                                            
                                            var skillname = response.data.skillname;
                                            vm.allRequests[key1].skills = skillname.join();
                                        }
                                    }

                                }, function (error) {
                                    // toastr.error(error.data.error, 'Angels');
                                });
                            }
                            
                        });
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllRequests();  

        /* to reject the worker assessment request */
        function rejectRequest(user_id, id) {
            AssessorService.rejectRequest(id).then(function (response) {
                if (response.data.status == 1) {
                    vm.rejectData = response.data.requestData;
                    SweetAlert.swal("Booking Request Rejected !", "The assessment booking request with the Angel "+ vm.rejectData.workers_name +" has been rejected.", "success");
                    vm.getAllRequests();
                    vm.getWorkerProfile(user_id, id);
                    vm.getNotification(user_id);
                } else {
                    SweetAlert.swal("Rejected!", response.data.message, "error");
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END rejectRequest();  


        /* to accept the worker assessment request */
        function acceptRequest(user_id, id) {

            AssessorService.acceptRequest(id).then(function (response) {
                
                if (response.data.status == 1) {
                    vm.acceptData = response.data.requestData;
                    SweetAlert.swal("Booking Request Accepted !", "The assessment booking with the Angel "+ vm.acceptData.workers_name +" has been added to your calendar on "+ formatDate(vm.acceptData.request_date) +" for the time slot "+ convertTime(vm.acceptData.request_timeFrom) +" - "+ convertTime(vm.acceptData.request_timeTo) +" AND If any other request on same slot is auto rejected. ", "success");
                    vm.getAllRequests();
                    vm.getWorkerProfile(user_id, id);

                    vm.getNotification(user_id);
                    
                } else {
                    SweetAlert.swal("Accepted!", response.data.message, "error");
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END acceptRequest();  


        /* to get single worker */
        function getWorkerProfile(workers_id, request_id) {
            vm.assessmentWorker = workers_id;
            vm.assessmentRequest = request_id;
            AssessorService.getSingleWorker(workers_id, request_id).then(function (response) { 

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorker = response.data.worker;
                        vm.requestData = response.data.request[0];
                        // console.log(vm.singleWorker);
                        angular.forEach(vm.singleWorker, function(value, key) {
                            // console.log(key);
                            if(key == 'workers_suburb'){
                                if(value != ''){
                                    var suburb = [];
                                    
                                    suburb = value;
                                    var arr = suburb.split(',');
                                    
                                    angular.forEach(vm.areaList, function(values) {
                                        if(arr[0] == values.suburb_id ){
                                            vm.singleWorker.suburbA = values.suburb_name;
                                        }
        
                                        if(arr[1] == values.suburb_id ){
                                            vm.singleWorker.suburbB = values.suburb_name;
                                        }
        
                                        if(arr[2] == values.suburb_id ){
                                            vm.singleWorker.suburbC = values.suburb_name;
                                        }
        
                                        if(arr[3] == values.suburb_id ){
                                            vm.singleWorker.suburbD = values.suburb_name;
                                        }
                                    });
                                    
                                }
                            }

                            if(key == 'workers_image'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_image = responses.data.file.file_base_url;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_codeConduct'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_codeConduct = responses.data.file.file_base_url;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_criminalReport'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_criminal_report = responses.data.file.file_base_url;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_photoIdentity'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_photoIdentity = responses.data.file.file_base_url;
                                            }
                                           
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_skills'){
                                if(value != ''){
                                    vm.skills = [];
                                    
                                    var skill = [];
                                    skill = value;
                                    var arrs = skill.split(',');

                                    for(var i = 0; i < arrs.length; i++ ){
                                    
                                        angular.forEach(vm.skillList, function(values) {
                                            if(arrs[i] == values.skills_id ){
                                                vm.skills.push(values.skills_name);
                                            }
                                                    
                                        });
                                    }
                                }
                            }

                        });

                        if(vm.singleWorker.workers_country != 0){
                            var country = vm.singleWorker.workers_country;
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleWorker.country = value.name;
                                }                               
                            }); 
                        }

                        vm.getStatesByID(vm.singleWorker);
                        vm.getCityByID(vm.singleWorker);
                        // vm.getSuburbByID(vm.singleWorker);

                        $timeout( function(){
                            vm.progressbar.complete();
                        }, 300 );
                    } else {
                        vm.progressbar.complete();
                        toastr.error(response.data.message, 'Assessor');
                        $state.go('frontoffice');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getSingleWorker();  

       /* Get All Countries */
        function getAllCountries(){
            AssessorService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(){
            AssessorService.getStatesByID(user_id, userRoleID).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.state && response.data.state.length > 0) {
                        vm.statesList = response.data.state;   
                        // console.log(vm.statesList);                     
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(stateId){
            AssessorService.getCityByID(stateId).then(function (response) {
                if (response.status == 200) {
                    if (response.data.cities && response.data.cities.length > 0) {
                        vm.cityList = response.data.cities;
                        vm.hideCity = false;
                        // vm.hideSuburb = false;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            AssessorService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getSuburbByID();  
        
         /* to get all area [Suburb]  */
        function getAllArea(){
            AssessorService.getAllArea().then(function (response) {
                if (response.status == 200) {
                    if (response.data.area && response.data.area.length > 0) {
                        vm.areaList = response.data.area;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllArea();

        /* to get all skills  */
        function getAllSkill(){
            AssessorService.getAllSkill().then(function (response) {
                if (response.status == 200) {
                    if (response.data.skill && response.data.skill.length > 0) {
                        vm.skillList = response.data.skill;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllSkill();                                    

        /* to get my added angels list */
        function getMyAddedAngels(newPage, obj) {
            vm.allAddedWorkers = [];
            vm.countAllAddedWorkers = 0;

            AssessorService.myAddedAngels(newPage, obj, vm.orderInfo, user_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allAddedWorkers = response.data.allWorkers;
                        vm.countAllAddedWorkers = response.data.workerCount;
                        
                        angular.forEach(vm.allAddedWorkers, function(value1, key1) {
                            if(value1.skills != '') {
                                
                                AssessorService.getSkillById(value1.skills).then(function (response) {
                                    
                                    if (response.status == 200) {
                                        if (response.data.status == 1) {
                                            
                                            var skillname = response.data.skillname;
                                            vm.allAddedWorkers[key1].skills = skillname.join();
                                        }
                                    }

                                }, function (error) {
                                    // toastr.error(error.data.error, 'Angels');
                                });
                            }
                            
                        });
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Workers');
            });
        }//END getMyAddedAngels(); 


        /* to get all assessors listing */
        function getAllAssessors(workerId, newPage, obj) {
            vm.allAssessors = [];
            vm.countAllAssessors = 0;
            var assessorId = 0;
            vm.bookWorkerID = 0;
            
            AssessorService.getAllAssessors(newPage, obj, vm.orderInfo, user_id, workerId ).then(function (response) { 
                console.log(response);               
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allAssessors = response.data.assessors;
                        vm.countAllAssessors = response.data.assessorCount;
                        vm.bookWorkerID = workerId;
                        
                        angular.forEach(vm.allAssessors, function(value, key) {
                            assessorId = value.user_id;
                            
                            AssessorService.getUnavailabilityData(assessorId, obj).then(function (responsesL) {
                                if (responsesL.status == 200) {
                                    if (responsesL.data.status == 1 && responsesL.data.leaves == 1) {
                                        vm.allAssessors.splice(key, 1);
                                        vm.countAllAssessors = vm.countAllAssessors -1;
                                    } else {

                                        AssessorService.getRequestBookAssessor(assessorId, obj).then(function (responses) {
                                            if (responses.status == 200) {
                                                if (responses.data.status == 1) {
                                                    if(responses.data.assessment.length == 4 ){
                                                        vm.allAssessors.splice(key, 1);
                                                        vm.countAllAssessors = vm.countAllAssessors -1;
                                                    }else{
                                                        if (typeof obj !== 'undefined' && obj !== '') {
                                                            if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                                                                vm.allAssessors[key].assessmentDate = obj.startDate;
                                                            }else{
                                                                vm.allAssessors[key].assessmentDate = vm.startDate;
                                                            }
                                                        }else{
                                                            vm.allAssessors[key].assessmentDate = vm.startDate;
                                                        }
                                                        
                                                    }
                                                }else{
                                                    if (typeof obj !== 'undefined' && obj !== '') {
                                                        if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                                                            vm.allAssessors[key].assessmentDate = obj.startDate;
                                                        }else{
                                                            vm.allAssessors[key].assessmentDate = vm.startDate;
                                                        }
                                                    }else{
                                                        vm.allAssessors[key].assessmentDate = vm.startDate;
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });                           
                        });  
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllAssessors(); 


        /* to get all skill set */
        function getAllSkillSet() {
            vm.allSkillSet = [];
            vm.ratingId = [];
            vm.ratings = [];
            
            AssessorService.getAllSkillSet().then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allSkillSet = response.data.skillList;
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllSkillSet(); 

        /* store extra skill array  */
        function addExtraSkillArray(extraSkill) {
            
            if ( extraSkill == undefined || extraSkill === '') {
                toastr.error('Extra skill should not be empty.', 'Assessor');
            } else {
                if(vm.extraSkillArray.length > 0) {
                    $scope.val = false;
                    angular.forEach(vm.extraSkillArray, function(value1, key1) {

                           if(value1.skillName == extraSkill) {
                             $scope.val = true;
                             var Elem = angular.element("#extraSkill");
                             Elem.val('');
                            } 
                    });

                } else {
                    // vm.extraSkillArray.push({'skillName' : extraSkill});
                    // var Elem = angular.element("#extraSkill");
                    // Elem.val('');
                }

                if(vm.extraSkillArray.length == 20) {
                    toastr.error('The maximum limit to add extra skills is 20', 'Assessor');
                } else {
                    if(!$scope.val){
                        vm.extraSkillArray.push({'skillName' : extraSkill});
                        var Elem = angular.element("#extraSkill");
                        Elem.val('');
                    };
                }
            }
                
        } //END addExtraSkillArray

        /* save extra skill to skill set */
        function saveExtraSkill(newSkillArray) {
            // console.log(newSkillArray.skillName);
            
            AssessorService.saveExtraSkill(newSkillArray).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {

                        toastr.success(response.data.message, 'Assessor');

                        vm.newSkills = response.data.newSkill;
                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                // toastr.error(error.data.error, 'Assessor');
            });
        }//END saveExtraSkill();         


        /* get time slot for next assessment booking */
        function getTimeSlots(assessorId, workerID, assessmentDate) {
            
            var slotTime = 0;
            vm.timeslot = '';
            vm.allRequestData = [];
            vm.nextBookAssessor = '';
            vm.nextBookAssessorID = '';
            vm.WorkerNameHead = '';
            vm.popHeadDate = assessmentDate;
            
            AssessorService.getTimeSlots(assessorId, workerID, assessmentDate).then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        slotTime = parseInt(response.data.slotsTime);
                        slotTime = (slotTime * 60 );

                        vm.timeslot = createTimeSlots(9, 18, slotTime);

                        vm.nextBookAssessor = response.data.assessorsName;
                        vm.nextBookAssessorID = assessorId;
                        vm.WorkerNameHead = response.data.workerNameHead;
                        
                        if(response.data.requestData.length !== 0 && response.data.requestData !== '') {

                            vm.allRequestData = response.data.requestData;
                            
                            angular.forEach(vm.timeslot, function(value1, key1) {
                                angular.forEach(vm.allRequestData, function(value2, key2){

                                    var path = value1.timeSlotKey.split("-");                                  
                                    var startTime = path[0];                                
                                    var endTime = path[1];    

                                    vm.timeslot[key1].request_date = moment(assessmentDate).format('YYYY-MM-DD');;                                    
                                    vm.timeslot[key1].request_timeFrom = moment(startTime, "HH:mm:ss").format("HH:mm:ss");                                    
                                    vm.timeslot[key1].request_timeTo = moment(endTime, "HH:mm:ss").format("HH:mm:ss");                             
                                    if(value1.timeSlotKey == value2.slot){                                    
                                        vm.timeslot[key1].bookFlag = 1;                      
                                    }

                                });
                            });

                            var Elem = angular.element("#timeSlot-modal");
                            Elem.addClass("in show");
                         
                        } else {
                            angular.forEach(vm.timeslot, function(value1, key1) {
                                var path = value1.timeSlotKey.split("-");                                  
                                var startTime = path[0];                                
                                var endTime = path[1];                                 
                                                                  
                                vm.timeslot[key1].request_date = moment(assessmentDate).format('YYYY-MM-DD');;                                    
                                vm.timeslot[key1].request_timeFrom = moment(startTime, "HH:mm:ss").format("HH:mm:ss");                      ;                                    
                                vm.timeslot[key1].request_timeTo = moment(endTime, "HH:mm:ss").format("HH:mm:ss");
                            });

                            var Elem = angular.element("#timeSlot-modal");
                            Elem.addClass("in show");
                        }
                       
                    } else {
                        // toastr.error(response.data.message, 'Assessor');
                    }
                    vm.progressbar.complete();
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END getTimeSlots();


        /* Book worker's next assessment to other assessor */
        function bookNextAssessment() {
            // SweetAlert.swal({
            //     title: "Please click confirm to send the booking request to the Assessor",
            //     text: "",
            //     // type: "warning",
            //     showCancelButton: true,
            //     confirmButtonColor: "#ff6600", confirmButtonText: "Confirm",
            //     cancelButtonText: "No",
            //     closeOnConfirm: false,
            //     closeOnCancel: true,
            //     html: true
            // }, function (isConfirm) {
            //     if (isConfirm) {
                    // console.log(vm.nextBook);
                    AssessorService.bookNextAssessmentRequest(user_id, vm.nextBook).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {
                                // vm.progressbar.complete();
                                toastr.success(response.data.message, 'Assessor');
                                $state.go('frontoffice.assessor.angels');
                                
                            } else {
                                toastr.error(response.data.message, 'Assessor');
                            }
                        }
                    }, function (error) {
                        toastr.error(error.data.message, 'Assessor');
                    });
            //     }    
            // });        
        }//END bookNextAssessment();


        /* Give ratings to angel on assessment points */
        function getAssessmentRatings(points, id) {
            
            vm.ratingIdData.push(id);
            vm.ratingPointData.push(points);
            
        }//END getAssessmentRatings();


        /* Save ratings to angel on assessment points */
        function saveAssessmentRatings(worker, request, rateId, ratePoint) {
            // SweetAlert.swal({
            //     title: "Are you sure you want to submit ratings for the Domestic Angel ? ",
            //     text: "",
            //     // type: "warning",
            //     showCancelButton: true,
            //     confirmButtonColor: "#ff6600", confirmButtonText: "Confirm",
            //     cancelButtonText: "No",
            //     closeOnConfirm: false,
            //     closeOnCancel: true,
            //     html: true
            // }, function (isConfirm) {
            //     if (isConfirm) {
                   
                    AssessorService.saveAssessmentRatings(worker, request, rateId, ratePoint, user_id).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {

                                toastr.success(response.data.message, 'Assessor');
                                $state.go('frontoffice.assessor.angels');
                            } else {
                                toastr.error(response.data.message, 'Assessor');
                            }
                        } else {
                            toastr.error(response.data.message, 'Assessor');
                        }
                    }, function (error) {
                        toastr.error('Internal server error', 'Assessor');
                    });
            //     }    
            // });  
        }//END saveAssessmentRatings();        

        /* to reset all search Assessment parameters in listing */
        function resetAssestForm() {
            getAllSkillSet();
        }//END resetAssestForm();

        /* to reset all search Assessment parameters in listing */
        function resetAssessment() {
            // vm.search = [];
            var Elem = angular.element("#timeSlot-modal");
                Elem.removeClass("in show");
                // getAllAssessors();
        }//END resetAssessment();


        /* to get assessors's calender */
        function getCalender() {
            AssessorService.getCalenderData(user_id).then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $scope.events = response.data.calender;

                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllAssessors(); 

       /* to get assessors's all request data for date */
        function getRequestByDate() {
            
            
            $scope.optionsObj = {
                // editable: true,
                // droppable: true,
                selectable: true,
                eventLimit: true,
                select: function (start, end, jsEvent, view) {
                   
                    vm.selectedDate = start.format();
                    vm.requestBydate = [];
                    AssessorService.getRequestAsByDate(user_id, start.format()).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {

                                vm.requestBydate = response.data.dateData;
                                
                                var Elem = angular.element("#responsive-modal");
                                Elem.addClass("in show");
                                
                            } 
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessor');
                    });
                    
                }
            };
            
        }//END getAllAssessors();         

        /* to save saveUnavailable */
        function saveUnavailable() {
            AssessorService.saveUnavailable(vm.unavailableForm, user_id).then(function (response) {

                var Elem = angular.element("#responsive-modal");
                Elem.removeClass("in show");
                Elem.css('display', 'none');

                var Elem = angular.element(".modal-backdrop");
                Elem.removeClass("in");
                // $scope.showModal = false;
                vm.getCalender();

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessor');
                         $state.go('frontoffice.assessor.calender');
                         vm.getCalender();
                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END saveUnavailable();


        /* to save saveAvailable */
        function saveAvailable() {
            AssessorService.saveAvailable(vm.availableForm, user_id).then(function (response) {

                var Elem = angular.element("#responsive-modal");
                    Elem.removeClass("in show");
                    $scope.showModal = false;
                    vm.getCalender();

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessor');
                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END saveAvailable();

        /* to save rescheduleRequest */
        function rescheduleRequest(request_id) {
            
            vm.requestDataById = [];
            AssessorService.getRequestDataById(request_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.request_id = request_id;
                        vm.requestDataById = response.data.requestById[0];
                        // console.log(vm.request_id);
                        var Elem = angular.element("#exampleModal");
                        Elem.addClass("in show");
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        
        }//END rescheduleRequest();


        /* to save saveReschedule */
        function saveReschedule() {
            AssessorService.saveReschedule(vm.rescheduleForm).then(function (response) {

                var Elem = angular.element("#exampleModal");
                    Elem.removeClass("in show");
                    vm.getCalender();

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessor');

                        var Elem = angular.element("#responsive-modal");
                        Elem.removeClass("in show");

                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END saveReschedule();

        /* to reset all search parameters in listing */
        function resetCalender() {
            getCalender();
            var Elem = angular.element("#responsive-modal");
            Elem.removeClass("in show");
        }//END reset();   
    

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAllRequests(1, '');
            getMyAddedAngels(1, '');
        }//END reset();      
    
        function createTimeSlots(startHour, endHour, interval) {            
            if (!startHour) {                
                endHour = 8;            
            }            
            if (!endHour) {                
                endHour = 20;            
            }            
            var timeSlots = [], dateTime = vm.startDate, timeStr = '';            
            
            dateTime.setHours(startHour, 0, 0, 0); 

            while (new Date(dateTime.getTime() + interval * 60000).getHours() < endHour) {                
                timeStr = addZero(dateTime.getHours()) + ':' + addZero(dateTime.getMinutes());                
                timeStr += '-';                
                dateTime = new Date(dateTime.getTime() + interval * 60000);                
                timeStr += dateTime.getHours() + ':' + addZero(dateTime.getMinutes());                
                timeSlots.push({timeSlotKey : timeStr, bookFlag : 0});            
            }            
            return timeSlots;        
        }   

        function addZero(i) {            
            if (i < 10) {                
                i = "0" + i;            
            }            
            return i;        
        }

           
    }

}());

(function () {
    'use strict';
    var frontApp = angular.module('frontofficeApp', [
        'authApp',
        'dashboardApp',
        'cafeUserApp',
        'assessorApp'       
    ]);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        return authServices.checkValidUser(true);
    } //END authenticateUser()

    frontApp.config(funConfig);
    frontApp.run(funRun);

    frontApp.component("headerComponent", {
        templateUrl: 'app/layouts/header.html',
        controller: 'FrontofficeController',
        controllerAs: 'header'
    });

    frontApp.component("headerloginComponent", {
        templateUrl: 'app/layouts/auth/header.html',
        controller: 'FrontofficeController',
        controllerAs: 'headerlogin'
    });

    frontApp.component("menuComponent", {
        templateUrl: 'app/layouts/menu.html',
        controller: 'FrontofficeController',
        controllerAs: 'menu'
    });

    frontApp.component("footerComponent", {
        templateUrl: 'app/layouts/footer.html',
        controller: 'FrontofficeController',
        controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('frontoffice', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'app/layouts/layout.html'
                    },
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.dashboard', {
                url: 'dashboard',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.profile', {
                url: 'profile',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/profile.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.updateprofile', {
                url: 'updateProfile',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/update.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'frontoffice'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.changePassword', {
                url: 'change-password',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/change_password.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'frontoffice'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            });         
    }


    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions', 'Idle'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions, Idle) {
        $rootScope.isLogin = false;

        $rootScope.$on('auth:login:success', function (event, data) {
            $state.go('frontoffice.dashboard');
        }); // Event fire after login successfully

        $rootScope.$on('auth:access:denied', function (event, data) {
            $state.go('auth.login');
        }); //Event fire after check access denied for user

        $rootScope.$on('auth:login:required', function (event, data) {
            $state.go('auth.login');
        }); //Event fire after logout

        // $transitions.onStart({ to: '**' }, function ($transition$) {
        //     $rootScope.showBreadcrumb = true;
        //     authServices.checkValidUser(true);
        //     //console.log($rootScope.user);
        //     //console.log($location.$$path);
        //     if ($transition$.to().name == 'frontoffice' || $transition$.to().name == 'frontoffice.dashboard') {
        //         $rootScope.showBreadcrumb = false;
        //     }
        // });
        Idle.watch();
    }

}());
(function () {
    "use strict";
    angular.module('frontofficeApp')
        .controller('FrontofficeController', FrontofficeController);

    FrontofficeController.$inject = ['$scope', '$location', '$state', 'authServices', 'FrontofficeService', '$rootScope', 'toastr', 'SweetAlert', '$timeout', 'Upload', 'ngProgressFactory', '$anchorScroll', 'APPCONFIG', '$ocLazyLoad', 'CafeUserService', 'AssessorService', 'Idle', 'Keepalive'];

    function FrontofficeController($scope, $location, $state, authServices, FrontofficeService, $rootScope, toastr, SweetAlert, $timeout, Upload, ngProgressFactory, $anchorScroll, APPCONFIG, $ocLazyLoad, CafeUserService, AssessorService, Idle, Keepalive) {

        $scope.started = false;

        function closeModals() {
            if ($scope.warning) {
                $scope.warning.close();
                $scope.warning = null;
            }

            if ($scope.timedout) {
                $scope.timedout.close();
                $scope.timedout = null;
            }
        }

        $scope.$on('IdleStart', function() {
            closeModals();
            var logElem = angular.element("#session_model");
            logElem.addClass("in show");      
        });

        $scope.$on('IdleEnd', function() {
            closeModals();
        });

        $scope.$on('IdleTimeout', function() {
            closeModals();
            var logElem = angular.element("#session_model");
            logElem.addClass("in show");
        });

        $scope.start = function() {
            closeModals();
            Idle.watch();
            $scope.started = true;
        };

        $scope.stop = function() {
            closeModals();
            Idle.unwatch();
            $scope.started = false;
        };

        var vm = this;

        vm.isActive = isActive;
        vm.getCurrentState = getCurrentState;
        
        if($state.current.name == 'frontoffice') {
           $rootScope.headerTitle = 'Dashboard';
        } else if($state.current.name == 'frontoffice.changePassword') {
            $rootScope.headerTitle = 'Change Password';
        }

        console.log($state.current.name, $rootScope.headerTitle);

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu
        
        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

        // NEW CODE
        var textUserName = '';
        if($rootScope.isLogin == true){
            if($rootScope.user.user_passCount == 1){
                var logElem = angular.element("#myModal_first_updat_password");
                logElem.addClass("in show");                
            }

            var userID = $rootScope.user.user_id;
            var userRoleID = $rootScope.user.user_role_id;    
        }       

        vm.setFlagC = false;
        vm.setFlagA = false;
        
        if(userRoleID == 2){
            vm.setFlagC = true;
            $ocLazyLoad.load('../assets/front/css/style_internet_cafe.css');
            $rootScope.headerTitle = 'Internet Cafe';
            textUserName = 'Internet Cafe';  
            $rootScope.bodyClass = 'internetCafe';                      
        }

        if(userRoleID == 4){
            vm.setFlagA = true;
            $ocLazyLoad.load('../assets/front/css/style_assasor.css');
            $rootScope.headerTitle = 'Assessor';
            textUserName = 'Assessor';
            $rootScope.bodyClass = 'assasorCafe';
        }

        $anchorScroll();
        
        vm.logout = logout;
        vm.logoutSession = logoutSession;
        vm.allPage = allPage;
        vm.getAllData = getAllData; 
        vm.getProfileInfo = getProfileInfo;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.formatDate = formatDate;
        vm.getNotification = getNotification;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;
        
        vm.progressbar = ngProgressFactory.createInstance();
        
        vm.profileForm = {}; 
        vm.passwordForm = {};
        vm.imageFlag = false;

        $scope.regex = /^[a-zA-Z0-9]{8,20}[^`~!#@$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
        $scope.regexUpdate = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        vm.allPage();
        vm.getAllData();
        vm.getProfileInfo();
        vm.getNotification();
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[1] == "profile" || path[1] == "updateProfile" ) {
            vm.progressbar.start();
            vm.getAllCountries();
            $timeout( function(){                
                vm.getProfileInfo();
                vm.progressbar.complete();
            }, 1000 );                
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* get all page data */
        function allPage(){
            vm.pagelist = [];
            FrontofficeService.allPage().then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.pagelist = response.data.data;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END allPage();

        /* get all data */
        function getAllData(){
            FrontofficeService.getAllData().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.headerSide = response.data.setting;
                        vm.socialSide = response.data.social;
                        vm.emailSide = response.data.email;
                        // vm.bannerSide = response.data.banner;
                        
                        // angular.forEach(vm.bannerSide, function(value, key) {
                        //     vm.bannerSide[key].file = '';
                        //     var file_id = value.banner_image;
                        //     FrontofficeService.getImage(file_id).then(function (responses) {
                        //         if (responses.status == 200) {
                        //             if (responses.data.file && responses.data.file != '') {
                        //                 vm.bannerSide[key].file = responses.data.file.file_base_url;
                        //             }
                        //         }
                        //     });
                        // });    

                        vm.headerSide[4].file = '';
                        var file_id = vm.headerSide[4].setting_value;
                        FrontofficeService.getImage(file_id).then(function (responses) {
                            if (responses.status == 200) {
                                if (responses.data.file && responses.data.file != '') {
                                    vm.headerSide[4].file = responses.data.file.file_base_url;
                                }
                            }
                        });

                        // vm.getSingleWeb('about');
                    }                    
                }

            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getAllData();
        
        /* to change passowrd */
        function changePassword() {
            vm.isDisabledButton = true;
            vm.progressbar.start();

            vm.passwordForm.user_id = userID;
            authServices.changePassword(vm.passwordForm).then(function (response) {
                if(response.status == 200){
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Success');
                        authServices.logout().then(function (response) {
                            if (response.status == 200) {
                                vm.progressbar.complete();
                                $state.go('auth.login');
                            }
                        }).catch(function (response) {
                            vm.isDisabledButton = false;
                            vm.progressbar.complete();
                            toastr.error("Unable to Logout!<br/>Try again later", "Error");
                        });
                    } else {
                        vm.isDisabledButton = false;
                        vm.progressbar.complete();
                        toastr.error(response.data.message, 'Error');
                    }
                }    
            }, function (error) {
                vm.isDisabledButton = false;
                vm.progressbar.complete();
                toastr.error(error.data.message, 'Error');
            });
        }//END changePassword()   
        
        /* logout */
        function logout() {
            SweetAlert.swal({
                title: "Are you sure you want to Logout?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f62d51", confirmButtonText: "Yes, Logout!",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    authServices.logout().then(function (response) {
                        if (response.status == 200) {
                            $state.go('auth.login');
                        }
                    }).catch(function (response) {
                        toastr.error("Unable to Logout!<br/>Try again later", "Error");
                    });
                }
            });
        }//END logout();

        /* Session logout */
        function logoutSession() {
            authServices.logout().then(function (response) {
                if (response.status == 200) {
                    $state.go('auth.login');
                }
            }).catch(function (response) {
                toastr.error("Unable to Logout!<br/>Try again later", "Error");
            });
        }//END logoutSession();

        /* get profile data */
        function getProfileInfo() {

            if(typeof userID !== 'undefined' && userID !== '' && typeof userRoleID !== 'undefined' && userRoleID !== ''){

                authServices.getUserProfile(userID, userRoleID).then(function (response) {
                    
                    if (response.data.status == 1) {
                        vm.singleProfile = response.data.data;                    
                        
                        $rootScope.user.Name = '';
                        vm.profileForm.name = '';
                        vm.profileForm.country = '';
                        vm.profileForm.file = '';

                        vm.profileForm.user_id = response.data.data.user_id;
                        vm.profileForm.usertype = response.data.data.user_role_id;
                        vm.profileForm.status = response.data.data.status; 

                        if(userRoleID == 2){
                            
                            $rootScope.user.Name = response.data.data.cafeusers_name;
                            $rootScope.nameUser = response.data.data.cafeusers_name;
                            vm.profileForm.name = response.data.data.cafeusers_name;
                            vm.profileForm.email = response.data.data.cafeusers_email;
                            vm.profileForm.phone = response.data.data.cafeusers_phone;
                            vm.profileForm.address = response.data.data.cafeusers_address;
                            vm.profileForm.countryID = response.data.data.cafeusers_country;
                            vm.profileForm.stateID = response.data.data.cafeusers_state;
                            vm.profileForm.cityID = response.data.data.cafeusers_city;
                            vm.profileForm.suburbID = response.data.data.cafeusers_suburb;
                            vm.profileForm.imageID = response.data.data.cafeusers_image;

                            if(response.data.data.cafeusers_image != 0){
                                var file_id = response.data.data.cafeusers_image;
                                authServices.getImage(file_id).then(function (responses) {
                                    if (responses.status == 200) {
                                        if (responses.data.file && responses.data.file != '') {
                                            vm.profileForm.file = responses.data.file.file_base_url;
                                            $rootScope.file = responses.data.file.file_base_url;
                                        }
                                    }                            
                                }, function (error) {
                                    toastr.error(error.data.error, 'Error');
                                });
                            } 

                            if(response.data.data.cafeusers_country != 0){
                                var country = response.data.data.cafeusers_country;
                                angular.forEach(vm.countryList, function(value, key) {
                                    if(value.id == country ){
                                        vm.profileForm.country = value.name;
                                    }                               
                                }); 
                            }
                        }

                        if(userRoleID == 4){
                            
                            $rootScope.user.Name = response.data.data.assessors_name;
                            $rootScope.nameUser = response.data.data.assessors_name;
                            vm.profileForm.name = response.data.data.assessors_name;
                            vm.profileForm.email = response.data.data.assessors_email;
                            vm.profileForm.phone = response.data.data.assessors_phone;
                            vm.profileForm.address = response.data.data.assessors_address;
                            vm.profileForm.countryID = response.data.data.assessors_country;
                            vm.profileForm.stateID = response.data.data.assessors_state;
                            vm.profileForm.cityID = response.data.data.assessors_city;
                            vm.profileForm.suburbID = response.data.data.assessors_suburb;
                            vm.profileForm.imageID = response.data.data.assessors_image;

                            if(response.data.data.assessors_image != 0){
                                var file_id = response.data.data.assessors_image;
                                authServices.getImage(file_id).then(function (responses) {
                                    if (responses.status == 200) {
                                        if (responses.data.file && responses.data.file != '') {
                                            vm.profileForm.file = responses.data.file.file_base_url;
                                            $rootScope.file = responses.data.file.file_base_url;
                                        }
                                    }                            
                                }, function (error) {
                                    toastr.error(error.data.error, 'Error');
                                });
                            } 

                            if(response.data.data.assessors_country != 0){
                                var country = response.data.data.assessors_country;
                                vm.singleProfile.country = '';
                                angular.forEach(vm.countryList, function(value, key) {
                                    if(value.id == country ){
                                        vm.profileForm.country = value.name;
                                    }                               
                                }); 
                                
                            }
                        }
                        vm.isDisabled = true;
                        vm.getStatesByID(vm.profileForm);
                        vm.getCityByID(vm.profileForm);
                        vm.getSuburbByID(vm.profileForm);
                                                            
                    } else {
                        toastr.error(response.data.error, 'Error');
                    }
                }, function (error) {
                    toastr.error('Something went wrong', 'Error');
                });
            }
        }//END getProfileInfo();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    vm.profileForm.imageID = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to update profile data */
        function updateProfile() {
            vm.isDisabledButton = true;
            vm.progressbar.start();

            if (vm.profileForm.file !== '') { //check if from is valid
                
                if(vm.profileForm.file.size > 0 && vm.profileForm.file.name !== '' ){
                    vm.upload(vm.profileForm.file); //call upload function   
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        authServices.updateProfile(vm.profileForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    toastr.success(response.data.message, textUserName);
                                    $state.go('frontoffice.profile');
                                    // $state.reload();
                                } else {
                                    if (response.data.status == 2) {
                                        vm.progressbar.complete();
                                        vm.isDisabledButton = false;
                                        toastr.error(response.data.message, textUserName);
                                    }else{
                                        if (response.data.status == 3) {
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, textUserName);
                                        }else{
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, textUserName);
                                        }
                                    }                        
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, textUserName);
                            }
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', textUserName);
                        });
                    }
                }, 9000 );
            }            
        }//END updateProfile()

        /* Get All Countries */
        function getAllCountries(){
            CafeUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){
            CafeUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }else{
                        vm.hideCity = true;
                        vm.hideSuburb = true;
                        delete vm.profileForm.stateID;
                        delete vm.profileForm.cityID;
                        delete vm.profileForm.suburbID;
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            CafeUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        vm.hideSuburb = true;
                        delete vm.profileForm.cityID;
                        delete vm.profileForm.suburbID;                        
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            CafeUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }else{
                        vm.hideSuburb = true;
                        delete vm.profileForm.suburbID;
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getSuburbByID();

        /* to get notification list */
        function getNotification() {
            AssessorService.getNotifications(userID).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $rootScope.countNot = response.data.count;
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Notification');
            });
        }//END getNotification();

    }

}());
(function () {
    "use strict";
    angular
        .module('frontofficeApp')
        .service('FrontofficeService', FrontofficeService);

    FrontofficeService.$inject = ['$http', 'APPCONFIG', '$q'];

    function FrontofficeService($http, APPCONFIG, $q) {

        /* to get single web page */
        function getSingleWeb(page_slug) {
            var URL = APPCONFIG.APIURL + 'website/view';
            var requestData = {};

            if (typeof page_slug !== undefined && page_slug !== '') {
                requestData['page_slug'] = page_slug;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeb();

        /* to get all web page */
        function allPage() {
            var URL = APPCONFIG.APIURL + 'page/AllPage';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END allPage();

        /* get all data */
        function getAllData() {
            var URL = APPCONFIG.APIURL + 'website';

            var requestData = {}; 
                       
            return this.runHttp(URL, requestData);
        }//END getAllData();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,            
            allPage: allPage,
            getAllData: getAllData,
            getImage: getImage
        }

    };//END FrontofficeService()
}());

"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',
    'ngProgress',    
    'ngPassword',
    'oc.lazyLoad',
    'ngIdle',
    'angular-fullcalendar', 
    '720kb.datepicker', 
    'jkAngularRatingStars',    
    'frontofficeApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://192.168.100.125:3000/' // local
    // 'APIURL': 'http://localhost:3000/' // local
    // 'APIURL': 'http://18.222.244.130:9000/' // testing
});

// app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
//     $urlRouterProvider.otherwise('/');    
// });

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider, KeepaliveProvider, IdleProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');
    
    // IdleProvider.idle(100);
    // IdleProvider.timeout(5);
    // KeepaliveProvider.interval(10);   

    IdleProvider.idle(30*60);
    IdleProvider.timeout(60);
    KeepaliveProvider.interval(10*60);

});

// RANGE
app.filter( 'range', function() {
    var filter = 
        function(arr, lower, upper) {
          for (var i = lower; i <= upper; i++) arr.push(i)
          return arr
        }
    return filter
    }
);

// UNIQUE ARRAY
app.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});

// Show Uppercase letter
app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.directive("filesModel1", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel1).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});

app.directive("filesModel2", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel2).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});

app.directive("filesModel3", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel3).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});

app.directive("filesModel4", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel4).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});