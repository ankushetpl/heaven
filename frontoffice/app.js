"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',
    'ngProgress',    
    'ngPassword',
    'oc.lazyLoad',
    'ngIdle',
    'angular-fullcalendar', 
    '720kb.datepicker', 
    'jkAngularRatingStars',    
    'frontofficeApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://192.168.100.125:3000/' // local
    // 'APIURL': 'http://localhost:3000/' // local
    // 'APIURL': 'http://18.222.244.130:9000/' // testing
});

// app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
//     $urlRouterProvider.otherwise('/');    
// });

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider, KeepaliveProvider, IdleProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');
    
    // IdleProvider.idle(100);
    // IdleProvider.timeout(5);
    // KeepaliveProvider.interval(10);   

    IdleProvider.idle(30*60);
    IdleProvider.timeout(60);
    KeepaliveProvider.interval(10*60);

});

// RANGE
app.filter( 'range', function() {
    var filter = 
        function(arr, lower, upper) {
          for (var i = lower; i <= upper; i++) arr.push(i)
          return arr
        }
    return filter
    }
);

// UNIQUE ARRAY
app.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});

// Show Uppercase letter
app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.directive("filesModel1", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel1).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});

app.directive("filesModel2", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel2).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});

app.directive("filesModel3", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel3).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});

app.directive("filesModel4", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    reader.onload = $scope.imageIsLoaded;
                    reader.readAsDataURL(file);
                }
                //end of code
                $parse(attrs.filesModel4).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
});