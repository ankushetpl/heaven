(function () {
    'use strict';
    var frontApp = angular.module('frontofficeApp', [
        'authApp',
        'dashboardApp',
        'cafeUserApp',
        'assessorApp'       
    ]);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        return authServices.checkValidUser(true);
    } //END authenticateUser()

    frontApp.config(funConfig);
    frontApp.run(funRun);

    frontApp.component("headerComponent", {
        templateUrl: 'app/layouts/header.html',
        controller: 'FrontofficeController',
        controllerAs: 'header'
    });

    frontApp.component("headerloginComponent", {
        templateUrl: 'app/layouts/auth/header.html',
        controller: 'FrontofficeController',
        controllerAs: 'headerlogin'
    });

    frontApp.component("menuComponent", {
        templateUrl: 'app/layouts/menu.html',
        controller: 'FrontofficeController',
        controllerAs: 'menu'
    });

    frontApp.component("footerComponent", {
        templateUrl: 'app/layouts/footer.html',
        controller: 'FrontofficeController',
        controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('frontoffice', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'app/layouts/layout.html'
                    },
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.dashboard', {
                url: 'dashboard',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/index.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.profile', {
                url: 'profile',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/profile.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'profile'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.updateprofile', {
                url: 'updateProfile',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/update.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'frontoffice'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('frontoffice.changePassword', {
                url: 'change-password',
                views: {
                    'content@frontoffice': {
                        templateUrl: 'app/components/dashboard/change_password.html',
                        controller: 'FrontofficeController',
                        controllerAs: 'frontoffice'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            });         
    }


    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions', 'Idle'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions, Idle) {
        $rootScope.isLogin = false;

        $rootScope.$on('auth:login:success', function (event, data) {
            $state.go('frontoffice.dashboard');
        }); // Event fire after login successfully

        $rootScope.$on('auth:access:denied', function (event, data) {
            $state.go('auth.login');
        }); //Event fire after check access denied for user

        $rootScope.$on('auth:login:required', function (event, data) {
            $state.go('auth.login');
        }); //Event fire after logout

        // $transitions.onStart({ to: '**' }, function ($transition$) {
        //     $rootScope.showBreadcrumb = true;
        //     authServices.checkValidUser(true);
        //     //console.log($rootScope.user);
        //     //console.log($location.$$path);
        //     if ($transition$.to().name == 'frontoffice' || $transition$.to().name == 'frontoffice.dashboard') {
        //         $rootScope.showBreadcrumb = false;
        //     }
        // });
        Idle.watch();
    }

}());