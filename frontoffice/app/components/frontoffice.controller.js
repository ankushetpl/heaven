(function () {
    "use strict";
    angular.module('frontofficeApp')
        .controller('FrontofficeController', FrontofficeController);

    FrontofficeController.$inject = ['$scope', '$location', '$state', 'authServices', 'FrontofficeService', '$rootScope', 'toastr', 'SweetAlert', '$timeout', 'Upload', 'ngProgressFactory', '$anchorScroll', 'APPCONFIG', '$ocLazyLoad', 'CafeUserService', 'AssessorService', 'Idle', 'Keepalive'];

    function FrontofficeController($scope, $location, $state, authServices, FrontofficeService, $rootScope, toastr, SweetAlert, $timeout, Upload, ngProgressFactory, $anchorScroll, APPCONFIG, $ocLazyLoad, CafeUserService, AssessorService, Idle, Keepalive) {

        $scope.started = false;

        function closeModals() {
            if ($scope.warning) {
                $scope.warning.close();
                $scope.warning = null;
            }

            if ($scope.timedout) {
                $scope.timedout.close();
                $scope.timedout = null;
            }
        }

        $scope.$on('IdleStart', function() {
            closeModals();
            var logElem = angular.element("#session_model");
            logElem.addClass("in show");      
        });

        $scope.$on('IdleEnd', function() {
            closeModals();
        });

        $scope.$on('IdleTimeout', function() {
            closeModals();
            var logElem = angular.element("#session_model");
            logElem.addClass("in show");
        });

        $scope.start = function() {
            closeModals();
            Idle.watch();
            $scope.started = true;
        };

        $scope.stop = function() {
            closeModals();
            Idle.unwatch();
            $scope.started = false;
        };

        var vm = this;

        vm.isActive = isActive;
        vm.getCurrentState = getCurrentState;
        
        if($state.current.name == 'frontoffice') {
           $rootScope.headerTitle = 'Dashboard';
        } else if($state.current.name == 'frontoffice.changePassword') {
            $rootScope.headerTitle = 'Change Password';
        }

        console.log($state.current.name, $rootScope.headerTitle);

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu
        
        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

        // NEW CODE
        var textUserName = '';
        if($rootScope.isLogin == true){
            if($rootScope.user.user_passCount == 1){
                var logElem = angular.element("#myModal_first_updat_password");
                logElem.addClass("in show");                
            }

            var userID = $rootScope.user.user_id;
            var userRoleID = $rootScope.user.user_role_id;    
        }       

        vm.setFlagC = false;
        vm.setFlagA = false;
        
        if(userRoleID == 2){
            vm.setFlagC = true;
            $ocLazyLoad.load('../assets/front/css/style_internet_cafe.css');
            $rootScope.headerTitle = 'Internet Cafe';
            textUserName = 'Internet Cafe';  
            $rootScope.bodyClass = 'internetCafe';                      
        }

        if(userRoleID == 4){
            vm.setFlagA = true;
            $ocLazyLoad.load('../assets/front/css/style_assasor.css');
            $rootScope.headerTitle = 'Assessor';
            textUserName = 'Assessor';
            $rootScope.bodyClass = 'assasorCafe';
        }

        $anchorScroll();
        
        vm.logout = logout;
        vm.logoutSession = logoutSession;
        vm.allPage = allPage;
        vm.getAllData = getAllData; 
        vm.getProfileInfo = getProfileInfo;
        vm.updateProfile = updateProfile;
        vm.changePassword = changePassword;
        vm.formatDate = formatDate;
        vm.getNotification = getNotification;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;
        
        vm.progressbar = ngProgressFactory.createInstance();
        
        vm.profileForm = {}; 
        vm.passwordForm = {};
        vm.imageFlag = false;

        $scope.regex = /^[a-zA-Z0-9]{8,20}[^`~!#@$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
        $scope.regexUpdate = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        vm.allPage();
        vm.getAllData();
        vm.getProfileInfo();
        vm.getNotification();
        
        /* to extract parameters from url */
        var path = $location.path().split("/");
        if (path[1] == "profile" || path[1] == "updateProfile" ) {
            vm.progressbar.start();
            vm.getAllCountries();
            $timeout( function(){                
                vm.getProfileInfo();
                vm.progressbar.complete();
            }, 1000 );                
        }
        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* get all page data */
        function allPage(){
            vm.pagelist = [];
            FrontofficeService.allPage().then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.pagelist = response.data.data;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END allPage();

        /* get all data */
        function getAllData(){
            FrontofficeService.getAllData().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.headerSide = response.data.setting;
                        vm.socialSide = response.data.social;
                        vm.emailSide = response.data.email;
                        // vm.bannerSide = response.data.banner;
                        
                        // angular.forEach(vm.bannerSide, function(value, key) {
                        //     vm.bannerSide[key].file = '';
                        //     var file_id = value.banner_image;
                        //     FrontofficeService.getImage(file_id).then(function (responses) {
                        //         if (responses.status == 200) {
                        //             if (responses.data.file && responses.data.file != '') {
                        //                 vm.bannerSide[key].file = responses.data.file.file_base_url;
                        //             }
                        //         }
                        //     });
                        // });    

                        vm.headerSide[4].file = '';
                        var file_id = vm.headerSide[4].setting_value;
                        FrontofficeService.getImage(file_id).then(function (responses) {
                            if (responses.status == 200) {
                                if (responses.data.file && responses.data.file != '') {
                                    vm.headerSide[4].file = responses.data.file.file_base_url;
                                }
                            }
                        });

                        // vm.getSingleWeb('about');
                    }                    
                }

            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getAllData();
        
        /* to change passowrd */
        function changePassword() {
            vm.isDisabledButton = true;
            vm.progressbar.start();

            vm.passwordForm.user_id = userID;
            authServices.changePassword(vm.passwordForm).then(function (response) {
                if(response.status == 200){
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Success');
                        authServices.logout().then(function (response) {
                            if (response.status == 200) {
                                vm.progressbar.complete();
                                $state.go('auth.login');
                            }
                        }).catch(function (response) {
                            vm.isDisabledButton = false;
                            vm.progressbar.complete();
                            toastr.error("Unable to Logout!<br/>Try again later", "Error");
                        });
                    } else {
                        vm.isDisabledButton = false;
                        vm.progressbar.complete();
                        toastr.error(response.data.message, 'Error');
                    }
                }    
            }, function (error) {
                vm.isDisabledButton = false;
                vm.progressbar.complete();
                toastr.error(error.data.message, 'Error');
            });
        }//END changePassword()   
        
        /* logout */
        function logout() {
            SweetAlert.swal({
                title: "Are you sure you want to Logout?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f62d51", confirmButtonText: "Yes, Logout!",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    authServices.logout().then(function (response) {
                        if (response.status == 200) {
                            $state.go('auth.login');
                        }
                    }).catch(function (response) {
                        toastr.error("Unable to Logout!<br/>Try again later", "Error");
                    });
                }
            });
        }//END logout();

        /* Session logout */
        function logoutSession() {
            authServices.logout().then(function (response) {
                if (response.status == 200) {
                    $state.go('auth.login');
                }
            }).catch(function (response) {
                toastr.error("Unable to Logout!<br/>Try again later", "Error");
            });
        }//END logoutSession();

        /* get profile data */
        function getProfileInfo() {

            if(typeof userID !== 'undefined' && userID !== '' && typeof userRoleID !== 'undefined' && userRoleID !== ''){

                authServices.getUserProfile(userID, userRoleID).then(function (response) {
                    
                    if (response.data.status == 1) {
                        vm.singleProfile = response.data.data;                    
                        
                        $rootScope.user.Name = '';
                        vm.profileForm.name = '';
                        vm.profileForm.country = '';
                        vm.profileForm.file = '';

                        vm.profileForm.user_id = response.data.data.user_id;
                        vm.profileForm.usertype = response.data.data.user_role_id;
                        vm.profileForm.status = response.data.data.status; 

                        if(userRoleID == 2){
                            
                            $rootScope.user.Name = response.data.data.cafeusers_name;
                            $rootScope.nameUser = response.data.data.cafeusers_name;
                            vm.profileForm.name = response.data.data.cafeusers_name;
                            vm.profileForm.email = response.data.data.cafeusers_email;
                            vm.profileForm.phone = response.data.data.cafeusers_phone;
                            vm.profileForm.address = response.data.data.cafeusers_address;
                            vm.profileForm.countryID = response.data.data.cafeusers_country;
                            vm.profileForm.stateID = response.data.data.cafeusers_state;
                            vm.profileForm.cityID = response.data.data.cafeusers_city;
                            vm.profileForm.suburbID = response.data.data.cafeusers_suburb;
                            vm.profileForm.imageID = response.data.data.cafeusers_image;

                            if(response.data.data.cafeusers_image != 0){
                                var file_id = response.data.data.cafeusers_image;
                                authServices.getImage(file_id).then(function (responses) {
                                    if (responses.status == 200) {
                                        if (responses.data.file && responses.data.file != '') {
                                            vm.profileForm.file = responses.data.file.file_base_url;
                                            $rootScope.file = responses.data.file.file_base_url;
                                        }
                                    }                            
                                }, function (error) {
                                    toastr.error(error.data.error, 'Error');
                                });
                            } 

                            if(response.data.data.cafeusers_country != 0){
                                var country = response.data.data.cafeusers_country;
                                angular.forEach(vm.countryList, function(value, key) {
                                    if(value.id == country ){
                                        vm.profileForm.country = value.name;
                                    }                               
                                }); 
                            }
                        }

                        if(userRoleID == 4){
                            
                            $rootScope.user.Name = response.data.data.assessors_name;
                            $rootScope.nameUser = response.data.data.assessors_name;
                            vm.profileForm.name = response.data.data.assessors_name;
                            vm.profileForm.email = response.data.data.assessors_email;
                            vm.profileForm.phone = response.data.data.assessors_phone;
                            vm.profileForm.address = response.data.data.assessors_address;
                            vm.profileForm.countryID = response.data.data.assessors_country;
                            vm.profileForm.stateID = response.data.data.assessors_state;
                            vm.profileForm.cityID = response.data.data.assessors_city;
                            vm.profileForm.suburbID = response.data.data.assessors_suburb;
                            vm.profileForm.imageID = response.data.data.assessors_image;

                            if(response.data.data.assessors_image != 0){
                                var file_id = response.data.data.assessors_image;
                                authServices.getImage(file_id).then(function (responses) {
                                    if (responses.status == 200) {
                                        if (responses.data.file && responses.data.file != '') {
                                            vm.profileForm.file = responses.data.file.file_base_url;
                                            $rootScope.file = responses.data.file.file_base_url;
                                        }
                                    }                            
                                }, function (error) {
                                    toastr.error(error.data.error, 'Error');
                                });
                            } 

                            if(response.data.data.assessors_country != 0){
                                var country = response.data.data.assessors_country;
                                vm.singleProfile.country = '';
                                angular.forEach(vm.countryList, function(value, key) {
                                    if(value.id == country ){
                                        vm.profileForm.country = value.name;
                                    }                               
                                }); 
                                
                            }
                        }
                        vm.isDisabled = true;
                        vm.getStatesByID(vm.profileForm);
                        vm.getCityByID(vm.profileForm);
                        vm.getSuburbByID(vm.profileForm);
                                                            
                    } else {
                        toastr.error(response.data.error, 'Error');
                    }
                }, function (error) {
                    toastr.error('Something went wrong', 'Error');
                });
            }
        }//END getProfileInfo();

        /*Image Upload*/
        vm.upload = function (file) {
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    vm.profileForm.imageID = resp.data.data;
                    vm.imageFlag = true;
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error(resp.data.message, 'Error');
            });
        };//END Image Upload

        /* to update profile data */
        function updateProfile() {
            vm.isDisabledButton = true;
            vm.progressbar.start();

            if (vm.profileForm.file !== '') { //check if from is valid
                
                if(vm.profileForm.file.size > 0 && vm.profileForm.file.name !== '' ){
                    vm.upload(vm.profileForm.file); //call upload function   
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        authServices.updateProfile(vm.profileForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    toastr.success(response.data.message, textUserName);
                                    $state.go('frontoffice.profile');
                                    // $state.reload();
                                } else {
                                    if (response.data.status == 2) {
                                        vm.progressbar.complete();
                                        vm.isDisabledButton = false;
                                        toastr.error(response.data.message, textUserName);
                                    }else{
                                        if (response.data.status == 3) {
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, textUserName);
                                        }else{
                                            vm.progressbar.complete();
                                            vm.isDisabledButton = false;
                                            toastr.error(response.data.message, textUserName);
                                        }
                                    }                        
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, textUserName);
                            }
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', textUserName);
                        });
                    }
                }, 9000 );
            }            
        }//END updateProfile()

        /* Get All Countries */
        function getAllCountries(){
            CafeUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){
            CafeUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }else{
                        vm.hideCity = true;
                        vm.hideSuburb = true;
                        delete vm.profileForm.stateID;
                        delete vm.profileForm.cityID;
                        delete vm.profileForm.suburbID;
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            CafeUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        vm.hideSuburb = true;
                        delete vm.profileForm.cityID;
                        delete vm.profileForm.suburbID;                        
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            CafeUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }else{
                        vm.hideSuburb = true;
                        delete vm.profileForm.suburbID;
                    }
                } else {
                    toastr.error(response.data.message, textUserName);
                }
            }, function (error) {
                toastr.error('Internal server error', textUserName);
            });
        }// END getSuburbByID();

        /* to get notification list */
        function getNotification() {
            AssessorService.getNotifications(userID).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $rootScope.countNot = response.data.count;
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Notification');
            });
        }//END getNotification();

    }

}());