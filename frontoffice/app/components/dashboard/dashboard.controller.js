(function () {
    'use strict';
    angular.module('dashboardApp', []).controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$rootScope', '$state', '$location', 'authServices', 'dashboardService', 'toastr', '$ocLazyLoad', '$anchorScroll', 'ngProgressFactory', '$timeout'];
    
    function DashboardController($scope, $rootScope, $state, $location, authServices, dashboardService, toastr, $ocLazyLoad, $anchorScroll, ngProgressFactory, $timeout) {
        var vm = this;
        
        // vm.passwordValidator = passwordValidator;
        // $rootScope.bodyClass = 'fix-header fix-sidebar card-no-border';
        
        vm.setFlagC = false;
        vm.setFlagA = false;
        
        if($rootScope.user.user_role_id == 2){
            vm.setFlagC = true;
            $ocLazyLoad.load('../assets/front/css/style_internet_cafe.css');
            $rootScope.bodyClass = 'internetCafe';
        }

        if($rootScope.user.user_role_id == 4){
            vm.setFlagA = true;
            $ocLazyLoad.load('../assets/front/css/style_assasor.css');
            $rootScope.bodyClass = 'assasorCafe';
        }

        $anchorScroll();

        vm.progressbar = ngProgressFactory.createInstance();

        vm.closeMod = closeMod;
        vm.savePassword = savePassword;
        
        vm.passwordForm = { user_id : $rootScope.user.user_id, user_username : $rootScope.user.user_username  };
        $scope.regex = /^[a-zA-Z][^`~!#$%\^&*()+={}|[\]\\:;"<>?,/]*$/;
        // $scope.regex = /^[a-zA-Z\_\- ]*$[^`~!#$%\^&*()+={}|[\]\\:;"<>?,/]/;
        // $scope.regex = /^[a-zA-Z\_\- ]*$/;

        function closeMod(){
            authServices.logout().then(function (response) {
                if (response.status == 200) {
                    $state.go('auth.login');
                }
            }).catch(function (response) {
                toastr.error("Unable to Logout!<br/>Try again later", "Error");
            });
        }

        function savePassword(){
            vm.isDisabledButton = true;
            vm.progressbar.start();
            dashboardService.changePassword(vm.passwordForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        toastr.success(response.data.message, 'Heavenly Sent');
                        $state.reload();
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Heavenly Sent');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END savePassword();

        // function passwordValidator(password) {
        //     if (!password) return;
        //     if (password.length < 6) return "Password must be at least " + 6 + " characters long";
        //     if (!password.match(/[A-Z]/)) return "Password must have at least one capital letter";
        //     if (!password.match(/[0-9]/)) return "Password must have at least one number";
        //     return true;
        // };

        
    };

}());