(function() {
    "use strict";
    angular
        .module('dashboardApp')
        .service('dashboardService', dashboardService);

    dashboardService.$inject = ['$http', 'APPCONFIG', '$q'];

    function dashboardService($http, APPCONFIG, $q) {
        return {
            // updateProfile: updateProfile,
            changePassword: changePassword
        }

        /* update profile */
        // function updateProfile(obj) {
        //     var deferred = $q.defer();
        //     var URL = APPCONFIG.APIURL + 'update-profile';
        //     $http({
        //         method: 'POST',
        //         url: URL,
        //         processData: false,
        //         transformRequest: function(data) {
        //             var formData = new FormData();
        //             formData.append("token", obj.token);
        //             formData.append("firstname", obj.firstname);
        //             formData.append("lastname", obj.lastname);
        //             return formData;
        //         },
        //         headers: {
        //             'Content-Type': undefined
        //         }
        //     }).then(function(response) {
        //         deferred.resolve(response);
        //     }, function(error) {
        //         deferred.reject(error);
        //     });
        //     return deferred.promise;
        // } //END updateProfile();

        /* change password */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'cafeUser/savePassword';
            var requestData = {};
            
            if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                requestData['user_id'] = obj.user_id;
            }

            if (typeof obj.new_password !== undefined && obj.new_password !== '') {
                requestData['user_password'] = obj.new_password;
            }

            if (typeof obj.user_username !== undefined && obj.user_username !== '') {
                requestData['user_username'] = obj.user_username;
            }

            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

    }; //END dashboardService()
}());