(function () {
    "use strict";
    angular
        .module('assessorApp')
        .service('AssessorService', AssessorService);

    AssessorService.$inject = ['$http', 'APPCONFIG', '$q'];

    function AssessorService($http, APPCONFIG, $q) {

        /* get all checklist data */
        function getChecklist() {
            var URL = APPCONFIG.APIURL + 'assessor/checklist';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getChecklist();

        /* get all Notifications */
        function getNotifications(user_id) {

            var URL = APPCONFIG.APIURL + 'assessor/notification';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getNotifications();    

        /* clear all Notifications */
        function clearAllNotifications(user_id) {
           
            var URL = APPCONFIG.APIURL + 'assessor/deleteNot';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END clearAllNotifications();                

        /* get all requests */
        function getAllRequests(pageNum, obj, orderInfo, user_id) {

            var URL = APPCONFIG.APIURL + 'assessor/requests';
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof user_id !== 'undefined' && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.name !== 'undefined' && obj.name !== '') {
                    requestData['name'] = obj.name;
                }

                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }

                if (typeof obj.endDate !== 'undefined' && obj.endDate !== '') {
                    requestData['endDate'] = moment(obj.endDate).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        } //END getAllRequests(); 

        /* reject worker request */
        function rejectRequest(request_id) {

            var URL = APPCONFIG.APIURL + 'assessor/rejectWorker';
            var requestData = {};

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END rejectRequest();  

        /* accept worker request */
        function acceptRequest(request_id) {

            var URL = APPCONFIG.APIURL + 'assessor/acceptWorker';
            var requestData = {};

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END acceptRequest();  


        /* to get single worker */
        function getSingleWorker(user_id, request_id) {
            var URL = APPCONFIG.APIURL + 'assessor/workerProfile';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWorker();


        /* to my added workers */
        function myAddedAngels(pageNum, obj, orderInfo, assessor_id) {

            var URL = APPCONFIG.APIURL + 'assessor/myWorker';
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof assessor_id !== 'undefined' && assessor_id !== '') {
                requestData['assessor_id'] = assessor_id;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.name !== 'undefined' && obj.name !== '') {
                    requestData['name'] = obj.name;
                }

                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }

                if (typeof obj.endDate !== 'undefined' && obj.endDate !== '') {
                    requestData['endDate'] = moment(obj.endDate).format('YYYY-MM-DD');
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.level !== 'undefined' && obj.level !== '') {
                    requestData['level'] = obj.level;
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }

            return this.runHttp(URL, requestData);
        } //END myAddedAngels();

        /* to get all assessors listing  */
        function getAllAssessors(pageNum, obj, orderInfo, user_id, workerId) {

            var URL = APPCONFIG.APIURL + 'assessor/allAssessors';
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof user_id !== 'undefined' && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof workerId !== 'undefined' && workerId !== '') {
                requestData['workerId'] = workerId;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.city !== 'undefined' && obj.city !== '') {
                    requestData['city'] = obj.city;
                }

                if (typeof obj.state !== 'undefined' && obj.state !== '') {
                    requestData['state'] = obj.state;
                }

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }

            }

            // if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
            //     if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
            //         requestData['orderColumn'] = orderInfo.orderColumn;
            //     }
            //     if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
            //         requestData['orderBy'] = orderInfo.orderBy;
            //     }
            // }

            return this.runHttp(URL, requestData);
        } //END getAllAssessors();  


        /* assessor's leave data */
        function getUnavailabilityData(assessor_id, obj) {
            var URL = APPCONFIG.APIURL + 'assessor/getUnavailabilityData';
            var requestData = {};

            if (typeof assessor_id !== 'undefined' && assessor_id !== '') {
                requestData['assessor_id'] = assessor_id;
            }

            if(typeof obj !== 'undefined' && obj !== ''){

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }else{
                    requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
                }
            }else{
                requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getUnavailabilityData();


       /* assessor's rquest data */
        function getRequestBookAssessor(assessor_id, obj) {
            var URL = APPCONFIG.APIURL + 'assessor/getRequestBookAssessor';
            var requestData = {};

            if (typeof assessor_id !== 'undefined' && assessor_id !== '') {
                requestData['assessor_id'] = assessor_id;
            }
            
            if(typeof obj !== 'undefined' && obj !== ''){

                if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                    requestData['startDate'] = moment(obj.startDate).format('YYYY-MM-DD');
                }else{
                    requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
                }
            }else{
                requestData['startDate'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getRequestBookAssessor();           


        /* to get all skill set */
        function getAllSkillSet() {
            var URL = APPCONFIG.APIURL + 'assessor/skills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END getAllSkillSet();                    


        /* to get skill name by id */
        function getSkillById(id) {
            var URL = APPCONFIG.APIURL + 'assessor/skillById';
            var requestData = {};

            if (typeof id !== undefined && id !== '') {
                requestData['id'] = id;
            }

            return this.runHttp(URL, requestData);
        } //END getSkillById();        


        /* to save skill in skill set */
        function saveExtraSkill(skillName) {
            var URL = APPCONFIG.APIURL + 'assessor/addskill';
            var requestData = {};

            if (typeof skillName !== undefined && skillName !== '') {
                requestData['skillName'] = skillName;
            }

            return this.runHttp(URL, requestData);
        } //END saveExtraSkill(); 

        /* get assessor's time slot for particular date */
        function getTimeSlots(assessorId, workerID, assessmentDate) {
            var URL = APPCONFIG.APIURL + 'assessor/getTimeSlots';
            var requestData = {};

            if (typeof assessorId !== undefined && assessorId !== '') {
                requestData['assessorId'] = assessorId;
            }

            if (typeof workerID !== undefined && workerID !== '') {
                requestData['workerID'] = workerID;
            }

            if (typeof assessmentDate !== undefined && assessmentDate !== '') {
                requestData['assessmentDate'] = moment(assessmentDate).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getTimeSlots();  


        /* Book worker's next assessment to other assessor */
        function bookNextAssessmentRequest(user_id, obj) {
            var URL = APPCONFIG.APIURL + 'assessor/bookNextAssessor';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof obj.assessorId !== undefined && obj.assessorId !== '') {
                requestData['assessorId'] = obj.assessorId;
            }

            if (typeof obj.worker_id !== undefined && obj.worker_id !== '') {
                requestData['worker_id'] = obj.worker_id;
            }

            if (typeof obj.request_date !== undefined && obj.request_date !== '') {
                requestData['request_date'] = obj.request_date;
            }

            if (typeof obj.request_timeFrom !== undefined && obj.request_timeFrom !== '') {
                requestData['request_timeFrom'] = obj.request_timeFrom;
            }

            if (typeof obj.request_timeTo !== undefined && obj.request_timeTo !== '') {
                requestData['request_timeTo'] = obj.request_timeTo;
            }

            return this.runHttp(URL, requestData);
        } //END bookNextAssessmentRequest();   


        /* save  worker's assessment ratings */
        function saveAssessmentRatings(worker, request, rateId, ratePoint, user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/workerRatings';
            var requestData = {};

            if (typeof worker !== undefined && worker !== '') {
                requestData['worker'] = worker;
            }

            if (typeof request !== undefined && request !== '') {
                requestData['request'] = request;
            }

            if (typeof rateId !== undefined && rateId !== '') {
                requestData['rateId'] = rateId;
            }

            if (typeof ratePoint !== undefined && ratePoint !== '') {
                requestData['ratePoint'] = ratePoint;
            }

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END saveAssessmentRatings();   


        /* to get calender dates */
        function getCalenderData(user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/calender';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getCalenderData();                    


        /* to get assessors's all request data for date */
        function getRequestAsByDate(user_id, searchDate) {
            var URL = APPCONFIG.APIURL + 'assessor/requestByDate';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof searchDate !== undefined && searchDate !== '') {
                requestData['searchDate'] = searchDate;
            }

            return this.runHttp(URL, requestData);
        } //END getRequestAsByDate();      

        /* save Unavailability */
        function saveUnavailable(obj, user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/updateAvailability';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.selectedDate !== 'undefined' && obj.selectedDate !== '') {
                    requestData['selectedDate'] = obj.selectedDate;
                }

                if (typeof obj.type !== 'undefined' && obj.type !== '') {
                    requestData['type'] = obj.type;
                }

                if (typeof user_id !== 'undefined' && user_id !== '') {
                    requestData['user_id'] = user_id;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveUnavailable();


        /* save Availability  */
        function saveAvailable(obj, user_id) {
            var URL = APPCONFIG.APIURL + 'assessor/updateAvailability';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.selectedDate !== 'undefined' && obj.selectedDate !== '') {
                    requestData['selectedDate'] = obj.selectedDate;
                }

                if (typeof obj.type !== 'undefined' && obj.type !== '') {
                    requestData['type'] = obj.type;
                }

                if (typeof user_id !== 'undefined' && user_id !== '') {
                    requestData['user_id'] = user_id;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveAvailable();


        /* to get request data by id*/
        function getRequestDataById(request_id) {
            var URL = APPCONFIG.APIURL + 'assessor/requestById';
            var requestData = {};

            if (typeof request_id !== undefined && request_id !== '') {
                requestData['request_id'] = request_id;
            }

            return this.runHttp(URL, requestData);
        } //END getRequestDataById();      



        /* save Reschedule  */
        function saveReschedule(obj) {
            var URL = APPCONFIG.APIURL + 'assessor/rescheduleRequest';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.rescheduleDate !== 'undefined' && obj.rescheduleDate !== '') {
                    requestData['rescheduleDate'] = obj.rescheduleDate;
                }

                if (typeof obj.request_id !== 'undefined' && obj.request_id !== '') {
                    requestData['request_id'] = obj.request_id;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveReschedule();


        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get States
        function getStatesByID(userID, userRoleID){
            var URL = APPCONFIG.APIURL + 'assessor/getProvince';
            var requestData = {};

            if (typeof userID !== 'undefined' && userID !== '') {                
                requestData['userID'] = parseInt(userID);            
            }            
            if (typeof userRoleID !== 'undefined' && userRoleID !== '') {                
                requestData['userRoleID'] = parseInt(userRoleID);            
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get city 
        function getCityByID(stateId){
            var URL = APPCONFIG.APIURL + 'assessor/getAllCities';
            var requestData = {};

                if (typeof stateId !== 'undefined' && stateId !== '') {
                    requestData['stateId'] = parseInt(stateId);
                }
            
            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get suburb by ID 
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }
                
                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['ID'] = parseInt(obj.cityID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        //Get all suburb 
        function getAllArea(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbs';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllArea();

        //Get all skill 
        function getAllSkill(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSkills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllSkill();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();         

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getChecklist: getChecklist,
            getNotifications:getNotifications,
            clearAllNotifications: clearAllNotifications,
            getAllRequests:getAllRequests,
            rejectRequest:rejectRequest,
            acceptRequest:acceptRequest,
            getSingleWorker:getSingleWorker,
            myAddedAngels:myAddedAngels,
            getAllAssessors:getAllAssessors,
            getUnavailabilityData:getUnavailabilityData,
            getRequestBookAssessor:getRequestBookAssessor,
            getAllSkillSet:getAllSkillSet,
            getSkillById:getSkillById,
            saveExtraSkill:saveExtraSkill,
            bookNextAssessmentRequest:bookNextAssessmentRequest,
            saveAssessmentRatings:saveAssessmentRatings,
            getTimeSlots:getTimeSlots,
            getCalenderData:getCalenderData,
            getRequestAsByDate:getRequestAsByDate,
            saveUnavailable:saveUnavailable,
            saveAvailable:saveAvailable,
            getRequestDataById:getRequestDataById,
            saveReschedule:saveReschedule,
            getAllCountries:getAllCountries,
            getStatesByID:getStatesByID,
            getCityByID:getCityByID,
            getSuburbByID:getSuburbByID,
            getAllArea:getAllArea,
            getAllSkill:getAllSkill,
            getImage:getImage
        }

    };//END WorkerUserService()
}());
