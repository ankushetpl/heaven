(function () {

    angular.module('assessorApp', [
        'angucomplete-alt',
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var assessorPath = 'app/components/assessor/';
        $stateProvider
            .state('frontoffice.assessor', {
                url: 'assessor',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/index.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.requests', {
                url: '/requests',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/request.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.calender', {
                url: '/calender',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/calender.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.checklist', {
                url: '/checklist',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/checklist.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.angels', {
                url: '/angels',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/my_added_angels.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.notification', {
                url: '/notification',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/notification.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.workerView', {
                url: '/workerView/:id/:request_id',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/worker_profile.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.allAssessors', {
                url: '/allAssessors/:ids',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/book_assessor.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })
            .state('frontoffice.assessor.assessment', {
                url: '/assessment/:id/:request_id',
                views: {
                    'content@frontoffice': {
                        templateUrl: assessorPath + 'views/assessment.html',
                        controller: 'AssessorController',
                        controllerAs: 'assessor'
                    }
                }
            })            

    }

}());