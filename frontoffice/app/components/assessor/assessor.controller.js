(function () {
    "use strict";
    angular.module('assessorApp')
        .controller('AssessorController', AssessorController);

    AssessorController.$inject = ['$scope', '$rootScope', '$state', '$location', 'AssessorService', 'toastr', 'SweetAlert','$timeout','APPCONFIG', 'ngProgressFactory', '$anchorScroll'];

    function AssessorController($scope, $rootScope, $state, $location, AssessorService, toastr, SweetAlert, $timeout, APPCONFIG, ngProgressFactory, $anchorScroll) {
        var vm = this;

        $anchorScroll();
        vm.progressbar = ngProgressFactory.createInstance();
        $rootScope.bodyClass = 'assasorCafe';

      
        var user_id = $rootScope.user.user_id;
        var userRoleID = $rootScope.user.user_role_id;

        vm.today = new Date();
        vm.ErrorToster = ErrorToster;
        
        vm.getChecklist = getChecklist;
        vm.getNotification = getNotification;
        vm.clearAllNotification = clearAllNotification;
        vm.getAllRequests = getAllRequests;
        vm.rejectRequest = rejectRequest;
        vm.acceptRequest = acceptRequest;
        vm.getWorkerProfile = getWorkerProfile;
        vm.getMyAddedAngels = getMyAddedAngels;
        vm.getAllAssessors = getAllAssessors;
        vm.getAllSkillSet = getAllSkillSet;
        vm.addExtraSkillArray = addExtraSkillArray; 
        vm.saveExtraSkill = saveExtraSkill;
        vm.getTimeSlots = getTimeSlots;
        vm.getCalender = getCalender;
        vm.bookNextAssessment = bookNextAssessment;
        vm.getAssessmentRatings = getAssessmentRatings;
        vm.saveAssessmentRatings = saveAssessmentRatings;
        vm.getRequestByDate = getRequestByDate;
        vm.saveUnavailable = saveUnavailable;
        vm.saveAvailable = saveAvailable;
        vm.rescheduleRequest = rescheduleRequest;
        vm.saveReschedule = saveReschedule;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;

        vm.getAllArea = getAllArea;
        vm.getAllSkill = getAllSkill;

        vm.convertTime = convertTime;
        vm.formatDate = formatDate;
        vm.dateFormatData = dateFormatData;
        vm.changePage = changePage;
        vm.changePageAssessor = changePageAssessor;
        vm.totalRequests = 0;
        vm.listPerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.reset = reset;
        vm.resetAssessment = resetAssessment;
        vm.resetCalender = resetCalender;
        vm.isDisabled = isDisabled;
        vm.bookPastDate = bookPastDate;

        vm.events = [];
        vm.extraSkillArray = [];
        vm.newSkills = [];
        vm.ratingIdData = [];
        vm.ratingPointData = [];
		
        vm.statusList = [{ id : 0, status: 'Pending'}, { id : 1, status: 'Accepted'}, { id : 2, status: 'Rejected'}, { id : 3, status: 'Qualified'}, { id : 4, status: 'Disqualified'}];
        vm.levelList = [{ id : 0, level: 'First'}, { id : 1, level: 'Final'}];

        vm.getMyAddedAngels();
        vm.getCalender();
        
        var monthS = moment().add(1, 'days');
        // vm.myDate = monthS._d;
        vm.startDate = monthS._d;
        
        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[2] == "notification") {
            vm.getNotification(user_id);
        }
        
        vm.getNotification(user_id);
        
        if (path[2] == "workerView" || path[2] == "assessment") {
            if (path[3] == "") {
                $state.go('frontoffice');
                return false;
            } else {
                vm.getAllCountries();
                vm.getAllArea();
                vm.getAllSkill();
                $timeout( function(){
                    vm.progressbar.start();
                    vm.getWorkerProfile(path[3],path[4]);
                    vm.getTimeSlots(path[3],path[4],path[5])
                }, 500 );                
            }
        }

        if(path[2] == "allAssessors"){
            if (path[3] == "") {
                $state.go('frontoffice');
                return false;
            } else {
                vm.getAllAssessors(path[3]);
            }
        }

        function isDisabled() {
            vm.isDisabled = true;
        }
       
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* to format date with yy-mm-dd*/
        function dateFormatData(date) {
            return moment(date).format("YYYY-MM-DD");
        }//END formatDate()        

        function convertTime(time) {
            return moment(time, "HH:mm:ss").format("hh:mm A");
        }

        /* disable past date for booking*/
        function bookPastDate() {
            this.myDate = new Date();
            vm.currentDate =  new Date(    
                this.myDate.getFullYear(),    
                this.myDate.getMonth(),    
                this.myDate.getDate() + 1 
            );
        }//END formatDate()

        /* for sorting category list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getAllRequests(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getAllRequests(newPage, searchInfo);

            if(path[2] == "assessors"){
                getAllAssessors(path[3], newPage, searchInfo);
            }
            getMyAddedAngels(newPage, searchInfo);
        }//END changePage();

        /* call when page changes */
        function changePageAssessor(newPage, searchInfo) {
            getAllAssessors(path[3], newPage, searchInfo);
        }//END changePageAssessor();
        

        /* to get checklist */
        function ErrorToster() {
            
            toastr.error('You can assest angel only on or before date of assessment', 'Assessment');
                
        }//END ErrorToster();

        /* to get checklist */
        function getChecklist() {
            AssessorService.getChecklist().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allChecklist = response.data.checklist;
                    } else {
                        toastr.error(response.data.message, 'Checklist');
                        $state.go('frontoffice.assessor');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Checklist');
            });
        }//END checklist();


        /* to get notification list */
        function getNotification(user_id) {
            AssessorService.getNotifications(user_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $rootScope.countNot = response.data.count;
                        vm.allNotification = response.data.notifications;
                    } else {
                        vm.allNotification.error = "No data for notifications";
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Notification');
            });
        }//END getNotification(); 

        /* to clear all notifications */
        function clearAllNotification() {
            SweetAlert.swal({
                title: "Are you sure you want to delete all notifications",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    AssessorService.clearAllNotifications(user_id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.getNotification(user_id);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessor');
                    });
                }
            });
        }//END clearAllNotification();         

        /* get all request from cafe users */
        function getAllRequests(newPage, obj) {
            vm.progressbar.start();
            vm.allRequests = [];
            vm.countAllRequests = 0;

            AssessorService.getAllRequests(newPage, obj, vm.orderInfo, user_id).then(function (response) {
                if (response.status == 200) {
                    vm.progressbar.complete();
                    if (response.data.status == 1) {
                        vm.allRequests = response.data.request;
                        vm.countAllRequests = response.data.requestCount;
                        
                        angular.forEach(vm.allRequests, function(value1, key1) {
                            if(value1.skills != '') {
                                
                                AssessorService.getSkillById(value1.skills).then(function (response) {
                                    
                                    if (response.status == 200) {
                                        if (response.data.status == 1) {
                                            
                                            var skillname = response.data.skillname;
                                            vm.allRequests[key1].skills = skillname.join();
                                        }
                                    }

                                }, function (error) {
                                    // toastr.error(error.data.error, 'Angels');
                                });
                            }
                            
                        });
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllRequests();  

        /* to reject the worker assessment request */
        function rejectRequest(user_id, id) {
            AssessorService.rejectRequest(id).then(function (response) {
                if (response.data.status == 1) {
                    vm.rejectData = response.data.requestData;
                    SweetAlert.swal("Booking Request Rejected !", "The assessment booking request with the Angel "+ vm.rejectData.workers_name +" has been rejected.", "success");
                    vm.getAllRequests();
                    vm.getWorkerProfile(user_id, id);
                    vm.getNotification(user_id);
                } else {
                    SweetAlert.swal("Rejected!", response.data.message, "error");
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END rejectRequest();  


        /* to accept the worker assessment request */
        function acceptRequest(user_id, id) {

            AssessorService.acceptRequest(id).then(function (response) {
                
                if (response.data.status == 1) {
                    vm.acceptData = response.data.requestData;
                    SweetAlert.swal("Booking Request Accepted !", "The assessment booking with the Angel "+ vm.acceptData.workers_name +" has been added to your calendar on "+ formatDate(vm.acceptData.request_date) +" for the time slot "+ convertTime(vm.acceptData.request_timeFrom) +" - "+ convertTime(vm.acceptData.request_timeTo) +" AND If any other request on same slot is auto rejected. ", "success");
                    vm.getAllRequests();
                    vm.getWorkerProfile(user_id, id);

                    vm.getNotification(user_id);
                    
                } else {
                    SweetAlert.swal("Accepted!", response.data.message, "error");
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END acceptRequest();  


        /* to get single worker */
        function getWorkerProfile(workers_id, request_id) {
            vm.assessmentWorker = workers_id;
            vm.assessmentRequest = request_id;
            AssessorService.getSingleWorker(workers_id, request_id).then(function (response) { 

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorker = response.data.worker;
                        vm.requestData = response.data.request[0];
                        // console.log(vm.singleWorker);
                        angular.forEach(vm.singleWorker, function(value, key) {
                            // console.log(key);
                            if(key == 'workers_suburb'){
                                if(value != ''){
                                    var suburb = [];
                                    
                                    suburb = value;
                                    var arr = suburb.split(',');
                                    
                                    angular.forEach(vm.areaList, function(values) {
                                        if(arr[0] == values.suburb_id ){
                                            vm.singleWorker.suburbA = values.suburb_name;
                                        }
        
                                        if(arr[1] == values.suburb_id ){
                                            vm.singleWorker.suburbB = values.suburb_name;
                                        }
        
                                        if(arr[2] == values.suburb_id ){
                                            vm.singleWorker.suburbC = values.suburb_name;
                                        }
        
                                        if(arr[3] == values.suburb_id ){
                                            vm.singleWorker.suburbD = values.suburb_name;
                                        }
                                    });
                                    
                                }
                            }

                            if(key == 'workers_image'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_image = responses.data.file.file_base_url;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_codeConduct'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_codeConduct = responses.data.file.file_base_url;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_criminalReport'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_criminal_report = responses.data.file.file_base_url;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_photoIdentity'){
                                if(value != ''){
                                    var file_id = value;
                                    AssessorService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.workers_photoIdentity = responses.data.file.file_base_url;
                                            }
                                           
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_skills'){
                                if(value != ''){
                                    vm.skills = [];
                                    
                                    var skill = [];
                                    skill = value;
                                    var arrs = skill.split(',');

                                    for(var i = 0; i < arrs.length; i++ ){
                                    
                                        angular.forEach(vm.skillList, function(values) {
                                            if(arrs[i] == values.skills_id ){
                                                vm.skills.push(values.skills_name);
                                            }
                                                    
                                        });
                                    }
                                }
                            }

                        });

                        if(vm.singleWorker.workers_country != 0){
                            var country = vm.singleWorker.workers_country;
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleWorker.country = value.name;
                                }                               
                            }); 
                        }

                        vm.getStatesByID(vm.singleWorker);
                        vm.getCityByID(vm.singleWorker);
                        // vm.getSuburbByID(vm.singleWorker);

                        $timeout( function(){
                            vm.progressbar.complete();
                        }, 300 );
                    } else {
                        vm.progressbar.complete();
                        toastr.error(response.data.message, 'Assessor');
                        $state.go('frontoffice');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getSingleWorker();  

       /* Get All Countries */
        function getAllCountries(){
            AssessorService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(){
            AssessorService.getStatesByID(user_id, userRoleID).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.state && response.data.state.length > 0) {
                        vm.statesList = response.data.state;   
                        // console.log(vm.statesList);                     
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(stateId){
            AssessorService.getCityByID(stateId).then(function (response) {
                if (response.status == 200) {
                    if (response.data.cities && response.data.cities.length > 0) {
                        vm.cityList = response.data.cities;
                        vm.hideCity = false;
                        // vm.hideSuburb = false;                        
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            AssessorService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList = response.data.suburb;
                        vm.hideSuburb = false;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getSuburbByID();  
        
         /* to get all area [Suburb]  */
        function getAllArea(){
            AssessorService.getAllArea().then(function (response) {
                if (response.status == 200) {
                    if (response.data.area && response.data.area.length > 0) {
                        vm.areaList = response.data.area;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllArea();

        /* to get all skills  */
        function getAllSkill(){
            AssessorService.getAllSkill().then(function (response) {
                if (response.status == 200) {
                    if (response.data.skill && response.data.skill.length > 0) {
                        vm.skillList = response.data.skill;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllSkill();                                    

        /* to get my added angels list */
        function getMyAddedAngels(newPage, obj) {
            vm.allAddedWorkers = [];
            vm.countAllAddedWorkers = 0;

            AssessorService.myAddedAngels(newPage, obj, vm.orderInfo, user_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allAddedWorkers = response.data.allWorkers;
                        vm.countAllAddedWorkers = response.data.workerCount;
                        
                        angular.forEach(vm.allAddedWorkers, function(value1, key1) {
                            if(value1.skills != '') {
                                
                                AssessorService.getSkillById(value1.skills).then(function (response) {
                                    
                                    if (response.status == 200) {
                                        if (response.data.status == 1) {
                                            
                                            var skillname = response.data.skillname;
                                            vm.allAddedWorkers[key1].skills = skillname.join();
                                        }
                                    }

                                }, function (error) {
                                    // toastr.error(error.data.error, 'Angels');
                                });
                            }
                            
                        });
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Workers');
            });
        }//END getMyAddedAngels(); 


        /* to get all assessors listing */
        function getAllAssessors(workerId, newPage, obj) {
            vm.allAssessors = [];
            vm.countAllAssessors = 0;
            var assessorId = 0;
            vm.bookWorkerID = 0;
            
            AssessorService.getAllAssessors(newPage, obj, vm.orderInfo, user_id, workerId ).then(function (response) { 
                console.log(response);               
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allAssessors = response.data.assessors;
                        vm.countAllAssessors = response.data.assessorCount;
                        vm.bookWorkerID = workerId;
                        
                        angular.forEach(vm.allAssessors, function(value, key) {
                            assessorId = value.user_id;
                            
                            AssessorService.getUnavailabilityData(assessorId, obj).then(function (responsesL) {
                                if (responsesL.status == 200) {
                                    if (responsesL.data.status == 1 && responsesL.data.leaves == 1) {
                                        vm.allAssessors.splice(key, 1);
                                        vm.countAllAssessors = vm.countAllAssessors -1;
                                    } else {

                                        AssessorService.getRequestBookAssessor(assessorId, obj).then(function (responses) {
                                            if (responses.status == 200) {
                                                if (responses.data.status == 1) {
                                                    if(responses.data.assessment.length == 4 ){
                                                        vm.allAssessors.splice(key, 1);
                                                        vm.countAllAssessors = vm.countAllAssessors -1;
                                                    }else{
                                                        if (typeof obj !== 'undefined' && obj !== '') {
                                                            if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                                                                vm.allAssessors[key].assessmentDate = obj.startDate;
                                                            }else{
                                                                vm.allAssessors[key].assessmentDate = vm.startDate;
                                                            }
                                                        }else{
                                                            vm.allAssessors[key].assessmentDate = vm.startDate;
                                                        }
                                                        
                                                    }
                                                }else{
                                                    if (typeof obj !== 'undefined' && obj !== '') {
                                                        if (typeof obj.startDate !== 'undefined' && obj.startDate !== '') {
                                                            vm.allAssessors[key].assessmentDate = obj.startDate;
                                                        }else{
                                                            vm.allAssessors[key].assessmentDate = vm.startDate;
                                                        }
                                                    }else{
                                                        vm.allAssessors[key].assessmentDate = vm.startDate;
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            }, function (error) {
                                toastr.error(error.data.error, 'Error');
                            });                           
                        });  
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllAssessors(); 


        /* to get all skill set */
        function getAllSkillSet() {
            vm.allSkillSet = [];
            vm.ratingId = [];
            vm.ratings = [];
            
            AssessorService.getAllSkillSet().then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allSkillSet = response.data.skillList;
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllSkillSet(); 

        /* store extra skill array  */
        function addExtraSkillArray(extraSkill) {
            
            if ( extraSkill == undefined || extraSkill === '') {
                toastr.error('Extra skill should not be empty.', 'Assessor');
            } else {
                if(vm.extraSkillArray.length > 0) {
                    $scope.val = false;
                    angular.forEach(vm.extraSkillArray, function(value1, key1) {

                           if(value1.skillName == extraSkill) {
                             $scope.val = true;
                             var Elem = angular.element("#extraSkill");
                             Elem.val('');
                            } 
                    });

                } else {
                    // vm.extraSkillArray.push({'skillName' : extraSkill});
                    // var Elem = angular.element("#extraSkill");
                    // Elem.val('');
                }

                if(vm.extraSkillArray.length == 20) {
                    toastr.error('The maximum limit to add extra skills is 20', 'Assessor');
                } else {
                    if(!$scope.val){
                        vm.extraSkillArray.push({'skillName' : extraSkill});
                        var Elem = angular.element("#extraSkill");
                        Elem.val('');
                    };
                }
            }
                
        } //END addExtraSkillArray

        /* save extra skill to skill set */
        function saveExtraSkill(newSkillArray) {
            // console.log(newSkillArray.skillName);
            
            AssessorService.saveExtraSkill(newSkillArray).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {

                        toastr.success(response.data.message, 'Assessor');

                        vm.newSkills = response.data.newSkill;
                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                // toastr.error(error.data.error, 'Assessor');
            });
        }//END saveExtraSkill();         


        /* get time slot for next assessment booking */
        function getTimeSlots(assessorId, workerID, assessmentDate) {
            
            var slotTime = 0;
            vm.timeslot = '';
            vm.allRequestData = [];
            vm.nextBookAssessor = '';
            vm.nextBookAssessorID = '';
            vm.WorkerNameHead = '';
            vm.popHeadDate = assessmentDate;
            
            AssessorService.getTimeSlots(assessorId, workerID, assessmentDate).then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        slotTime = parseInt(response.data.slotsTime);
                        slotTime = (slotTime * 60 );

                        vm.timeslot = createTimeSlots(9, 18, slotTime);

                        vm.nextBookAssessor = response.data.assessorsName;
                        vm.nextBookAssessorID = assessorId;
                        vm.WorkerNameHead = response.data.workerNameHead;
                        
                        if(response.data.requestData.length !== 0 && response.data.requestData !== '') {

                            vm.allRequestData = response.data.requestData;
                            
                            angular.forEach(vm.timeslot, function(value1, key1) {
                                angular.forEach(vm.allRequestData, function(value2, key2){

                                    var path = value1.timeSlotKey.split("-");                                  
                                    var startTime = path[0];                                
                                    var endTime = path[1];    

                                    vm.timeslot[key1].request_date = moment(assessmentDate).format('YYYY-MM-DD');;                                    
                                    vm.timeslot[key1].request_timeFrom = moment(startTime, "HH:mm:ss").format("HH:mm:ss");                                    
                                    vm.timeslot[key1].request_timeTo = moment(endTime, "HH:mm:ss").format("HH:mm:ss");                             
                                    if(value1.timeSlotKey == value2.slot){                                    
                                        vm.timeslot[key1].bookFlag = 1;                      
                                    }

                                });
                            });

                            var Elem = angular.element("#timeSlot-modal");
                            Elem.addClass("in show");
                         
                        } else {
                            angular.forEach(vm.timeslot, function(value1, key1) {
                                var path = value1.timeSlotKey.split("-");                                  
                                var startTime = path[0];                                
                                var endTime = path[1];                                 
                                                                  
                                vm.timeslot[key1].request_date = moment(assessmentDate).format('YYYY-MM-DD');;                                    
                                vm.timeslot[key1].request_timeFrom = moment(startTime, "HH:mm:ss").format("HH:mm:ss");                      ;                                    
                                vm.timeslot[key1].request_timeTo = moment(endTime, "HH:mm:ss").format("HH:mm:ss");
                            });

                            var Elem = angular.element("#timeSlot-modal");
                            Elem.addClass("in show");
                        }
                       
                    } else {
                        // toastr.error(response.data.message, 'Assessor');
                    }
                    vm.progressbar.complete();
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END getTimeSlots();


        /* Book worker's next assessment to other assessor */
        function bookNextAssessment() {
            // SweetAlert.swal({
            //     title: "Please click confirm to send the booking request to the Assessor",
            //     text: "",
            //     // type: "warning",
            //     showCancelButton: true,
            //     confirmButtonColor: "#ff6600", confirmButtonText: "Confirm",
            //     cancelButtonText: "No",
            //     closeOnConfirm: false,
            //     closeOnCancel: true,
            //     html: true
            // }, function (isConfirm) {
            //     if (isConfirm) {
                    // console.log(vm.nextBook);
                    AssessorService.bookNextAssessmentRequest(user_id, vm.nextBook).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {
                                // vm.progressbar.complete();
                                toastr.success(response.data.message, 'Assessor');
                                $state.go('frontoffice.assessor.angels');
                                
                            } else {
                                toastr.error(response.data.message, 'Assessor');
                            }
                        }
                    }, function (error) {
                        toastr.error(error.data.message, 'Assessor');
                    });
            //     }    
            // });        
        }//END bookNextAssessment();


        /* Give ratings to angel on assessment points */
        function getAssessmentRatings(points, id) {
            
            vm.ratingIdData.push(id);
            vm.ratingPointData.push(points);
            
        }//END getAssessmentRatings();


        /* Save ratings to angel on assessment points */
        function saveAssessmentRatings(worker, request, rateId, ratePoint) {
            // SweetAlert.swal({
            //     title: "Are you sure you want to submit ratings for the Domestic Angel ? ",
            //     text: "",
            //     // type: "warning",
            //     showCancelButton: true,
            //     confirmButtonColor: "#ff6600", confirmButtonText: "Confirm",
            //     cancelButtonText: "No",
            //     closeOnConfirm: false,
            //     closeOnCancel: true,
            //     html: true
            // }, function (isConfirm) {
            //     if (isConfirm) {
                   
                    AssessorService.saveAssessmentRatings(worker, request, rateId, ratePoint, user_id).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {

                                toastr.success(response.data.message, 'Assessor');
                                $state.go('frontoffice.assessor.angels');
                            } else {
                                toastr.error(response.data.message, 'Assessor');
                            }
                        } else {
                            toastr.error(response.data.message, 'Assessor');
                        }
                    }, function (error) {
                        toastr.error('Internal server error', 'Assessor');
                    });
            //     }    
            // });  
        }//END saveAssessmentRatings();        

        /* to reset all search Assessment parameters in listing */
        function resetAssestForm() {
            getAllSkillSet();
        }//END resetAssestForm();

        /* to reset all search Assessment parameters in listing */
        function resetAssessment() {
            // vm.search = [];
            var Elem = angular.element("#timeSlot-modal");
                Elem.removeClass("in show");
                // getAllAssessors();
        }//END resetAssessment();


        /* to get assessors's calender */
        function getCalender() {
            AssessorService.getCalenderData(user_id).then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $scope.events = response.data.calender;

                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        }//END getAllAssessors(); 

       /* to get assessors's all request data for date */
        function getRequestByDate() {
            
            
            $scope.optionsObj = {
                // editable: true,
                // droppable: true,
                selectable: true,
                eventLimit: true,
                select: function (start, end, jsEvent, view) {
                   
                    vm.selectedDate = start.format();
                    vm.requestBydate = [];
                    AssessorService.getRequestAsByDate(user_id, start.format()).then(function (response) {
                        if (response.status == 200) {
                            if (response.data.status == 1) {

                                vm.requestBydate = response.data.dateData;
                                
                                var Elem = angular.element("#responsive-modal");
                                Elem.addClass("in show");
                                
                            } 
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Assessor');
                    });
                    
                }
            };
            
        }//END getAllAssessors();         

        /* to save saveUnavailable */
        function saveUnavailable() {
            AssessorService.saveUnavailable(vm.unavailableForm, user_id).then(function (response) {

                var Elem = angular.element("#responsive-modal");
                Elem.removeClass("in show");
                Elem.css('display', 'none');

                var Elem = angular.element(".modal-backdrop");
                Elem.removeClass("in");
                // $scope.showModal = false;
                vm.getCalender();

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessor');
                         $state.go('frontoffice.assessor.calender');
                         vm.getCalender();
                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END saveUnavailable();


        /* to save saveAvailable */
        function saveAvailable() {
            AssessorService.saveAvailable(vm.availableForm, user_id).then(function (response) {

                var Elem = angular.element("#responsive-modal");
                    Elem.removeClass("in show");
                    $scope.showModal = false;
                    vm.getCalender();

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessor');
                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END saveAvailable();

        /* to save rescheduleRequest */
        function rescheduleRequest(request_id) {
            
            vm.requestDataById = [];
            AssessorService.getRequestDataById(request_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.request_id = request_id;
                        vm.requestDataById = response.data.requestById[0];
                        // console.log(vm.request_id);
                        var Elem = angular.element("#exampleModal");
                        Elem.addClass("in show");
                    } 
                }
            }, function (error) {
                toastr.error(error.data.error, 'Assessor');
            });
        
        }//END rescheduleRequest();


        /* to save saveReschedule */
        function saveReschedule() {
            AssessorService.saveReschedule(vm.rescheduleForm).then(function (response) {

                var Elem = angular.element("#exampleModal");
                    Elem.removeClass("in show");
                    vm.getCalender();

                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Assessor');

                        var Elem = angular.element("#responsive-modal");
                        Elem.removeClass("in show");

                    } else {
                        toastr.error(response.data.message, 'Assessor');
                    }
                } else {
                    toastr.error(response.data.message, 'Assessor');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Assessor');
            });
        }//END saveReschedule();

        /* to reset all search parameters in listing */
        function resetCalender() {
            getCalender();
            var Elem = angular.element("#responsive-modal");
            Elem.removeClass("in show");
        }//END reset();   
    

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getAllRequests(1, '');
            getMyAddedAngels(1, '');
        }//END reset();      
    
        function createTimeSlots(startHour, endHour, interval) {            
            if (!startHour) {                
                endHour = 8;            
            }            
            if (!endHour) {                
                endHour = 20;            
            }            
            var timeSlots = [], dateTime = vm.startDate, timeStr = '';            
            
            dateTime.setHours(startHour, 0, 0, 0); 

            while (new Date(dateTime.getTime() + interval * 60000).getHours() < endHour) {                
                timeStr = addZero(dateTime.getHours()) + ':' + addZero(dateTime.getMinutes());                
                timeStr += '-';                
                dateTime = new Date(dateTime.getTime() + interval * 60000);                
                timeStr += dateTime.getHours() + ':' + addZero(dateTime.getMinutes());                
                timeSlots.push({timeSlotKey : timeStr, bookFlag : 0});            
            }            
            return timeSlots;        
        }   

        function addZero(i) {            
            if (i < 10) {                
                i = "0" + i;            
            }            
            return i;        
        }

           
    }

}());
