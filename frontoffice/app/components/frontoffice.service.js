(function () {
    "use strict";
    angular
        .module('frontofficeApp')
        .service('FrontofficeService', FrontofficeService);

    FrontofficeService.$inject = ['$http', 'APPCONFIG', '$q'];

    function FrontofficeService($http, APPCONFIG, $q) {

        /* to get single web page */
        function getSingleWeb(page_slug) {
            var URL = APPCONFIG.APIURL + 'website/view';
            var requestData = {};

            if (typeof page_slug !== undefined && page_slug !== '') {
                requestData['page_slug'] = page_slug;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeb();

        /* to get all web page */
        function allPage() {
            var URL = APPCONFIG.APIURL + 'page/AllPage';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END allPage();

        /* get all data */
        function getAllData() {
            var URL = APPCONFIG.APIURL + 'website';

            var requestData = {}; 
                       
            return this.runHttp(URL, requestData);
        }//END getAllData();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,            
            allPage: allPage,
            getAllData: getAllData,
            getImage: getImage
        }

    };//END FrontofficeService()
}());
