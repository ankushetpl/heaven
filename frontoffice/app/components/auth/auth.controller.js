(function () {
    'use strict';
    angular.module('authApp').controller('AuthController', AuthController);

    AuthController.$inject = ['$rootScope', '$location', 'authServices', '$state', 'toastr', '$timeout', 'ngProgressFactory', '$ocLazyLoad'];

    function AuthController($rootScope, $location, authServices, $state, toastr, $timeout, ngProgressFactory, $ocLazyLoad) {
        var vm = this;
        vm.login = login;
        vm.forgotPassword = forgotPassword;
        
        vm.user = { device_token: '', device_type: '1' };
        $rootScope.bodyClass = '';

        vm.progressbar = ngProgressFactory.createInstance();
        
        $ocLazyLoad.load('../assets/front/css/style.css');

        function login(loginInfo) {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            
            authServices.checkLogin(loginInfo).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        var token = response.data.data.device_token;
                        authServices.setAuthToken(token); //Set auth token
                        vm.progressbar.complete();
                        $state.go('frontoffice.dashboard');                        
                    }else{
                        toastr.error(response.data.message, "Log In");
                        vm.isDisabledButton = false;
                        vm.progressbar.complete();
                    }                    
                } else {
                    toastr.error(response.data.message, "Log In");
                    vm.isDisabledButton = false;
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Log In');
                vm.isDisabledButton = false;
                vm.progressbar.complete();
            });
        }; //END login()   

        function forgotPassword(email) {
            vm.progressbar.start();
            vm.isDisabledButton = true;

            authServices.checkForgotPassword(email).then(function (response) {
                if (response.status == 200) {
                    if(response.data.status == 1){
                        toastr.success(response.data.message, "Forgot Password");
                        vm.progressbar.complete();
                        $timeout( function(){
                            $state.go('auth');                            
                        }, 1000 );
                    }else{
                        toastr.error(response.data.message, "Forgot Password");
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                    }                    
                } else {
                    toastr.error(response.data.message, "Forgot Password");
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                }
            },function (error) {
                toastr.error(error.data.error, 'Error');
                vm.progressbar.complete();
                vm.isDisabledButton = false;
            });
        }; //END forgotPassword() 
    };
}());