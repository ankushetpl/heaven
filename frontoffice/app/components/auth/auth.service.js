(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkLogin = checkLogin;
        self.checkForgotPassword = checkForgotPassword;
        self.checkValidUser = checkValidUser;
        self.setAuthToken = setAuthToken;
        self.getAuthToken = getAuthToken;
        self.saveUserInfo = saveUserInfo;
        self.checkValidUrl = checkValidUrl;
        self.getUserProfile = getUserProfile;
        self.updateProfile = updateProfile;
        self.changePassword = changePassword;
        self.logout = logout;
        self.getImage = getImage;

        //to check if user is login and set user details in rootscope
        function checkLogin(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login';
            var requestData = {};

            if (typeof obj.email !== undefined && obj.email !== '') {
                requestData['username'] = obj.email;
            }
            if (typeof obj.password !== undefined && obj.password !== '') {
                requestData['password'] = obj.password;
            }
            if (typeof obj.usertype !== undefined && obj.usertype !== '') {
                requestData['usertype'] = obj.usertype;
            }
            if (typeof obj.device_token !== undefined && obj.device_token !== '') {
                requestData['device_token'] = obj.device_token;
            }
            if (typeof obj.device_type !== undefined && obj.device_type !== '') {
                requestData['device_type'] = obj.device_type;
            }
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                $rootScope.isLogin = true;
                deferred.resolve(response);
            }, function (response) {
                $rootScope.isLogin = false;
                $rootScope.$broadcast('auth:login:required');
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkLogin();

        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'login/validate-user';
            var deferred = $q.defer();
            var requestData = {};
            // requestData['usertype'] = 1;
            requestData['token'] = getAuthToken();
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                
                if (response.data.status == 0) {
                    
                    $rootScope.$broadcast('auth:login:required');
                } else {
                    $rootScope.isLogin = true;
                    saveUserInfo(response.data);
                    if (isAuth === false) {
                        $rootScope.$broadcast('auth:login:success');
                    }
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function checkForgotPassword(email) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/forgetpassword';
            var requestData = { email : email };

            if (typeof email !== undefined && email !== '') {
                requestData['user_username'] = email;
            }
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
            return deferred.promise;
        } //END checkForgotPassword();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.data;
                $rootScope.user = user;
            }
            return user;
        } //END saveUserInfo();

        function checkValidUrl(role, location) {
            var urlData = location.split('/');
            var actualUrl = urlData[1];
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'valid-url';
            $http({
                method: 'POST',
                url: URL,
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("role_id", role);
                    formData.append("url", actualUrl);
                    return formData;
                },
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': undefined
                }
            }).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        deferred.resolve(response);
                    } else {
                        $state.go('frontoffice.dashboard');
                    }
                }
            });
            return deferred.promise;
        }

        /* to get user profile data */
        function getUserProfile(user_id, role_id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'user/view';
            var requestData = {};
            requestData['user_id'] = user_id;
            requestData['UserType'] = role_id;
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }//END getUserProfile()

        /* to update profile data */
        function updateProfile(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'user/update';
            
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = parseInt(obj.user_id);
                    URL = APPCONFIG.APIURL + 'user/update';
                }

                if (typeof obj.usertype !== undefined && obj.usertype !== '') {
                    requestData['usertype'] = parseInt(obj.usertype);
                }

                if (typeof obj.status !== undefined && obj.status !== '') {
                    requestData['status'] = obj.status;
                }

                if (typeof obj.name !== undefined && obj.name !== '') {
                    requestData['name'] = obj.name;
                }

                if (typeof obj.phone !== undefined && obj.phone !== '') {
                    requestData['phone'] = obj.phone;
                }

                if (typeof obj.address !== undefined && obj.address !== '') {
                    requestData['address'] = obj.address;
                }

                if (typeof obj.countryID !== undefined && obj.countryID !== '') {
                    requestData['cafeusers_country'] = parseInt(obj.countryID);
                }

                if (typeof obj.stateID !== undefined && obj.stateID !== '') {
                    requestData['cafeusers_state'] = parseInt(obj.stateID);
                }

                if (typeof obj.cityID !== undefined && obj.cityID !== '') {
                    requestData['cafeusers_city'] = parseInt(obj.cityID);
                }

                if (typeof obj.suburbID !== undefined && obj.suburbID !== '') {
                    requestData['cafeusers_suburb'] = parseInt(obj.suburbID);
                }

                if (typeof obj.imageID !== undefined && obj.imageID !== '') {
                    requestData['assessors_image'] = parseInt(obj.imageID);
                }
            }

            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END updateProfile();

        /* to update profile data */
        function changePassword(obj) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/changepassword';
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;                    
                }

                if (typeof obj.newPassword !== undefined && obj.newPassword !== '') {
                    requestData['newpassword'] = obj.newPassword;
                }

                if (typeof obj.oldPassword !== undefined && obj.oldPassword !== '') {
                    requestData['oldpassword'] = obj.oldPassword;
                }
            }
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END changePassword();

        function logout() {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'login/logout';
            var requestData = {};
            requestData['usertype'] = $rootScope.user.user_id;
            requestData['token'] = getAuthToken();
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                localStorage.removeItem('token');
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END logout()  

        /* to get single image */
        function getImage(file_id) {
            var deferred = $q.defer();
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }                        
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } //END getImage();

        return self;
        
    };
})();