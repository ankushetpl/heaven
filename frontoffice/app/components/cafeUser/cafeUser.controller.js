(function () {
    "use strict";
    angular.module('cafeUserApp')
        .controller('CafeUserController', CafeUserController);

    CafeUserController.$inject = ['$scope', '$rootScope', '$state', '$location', 'CafeUserService', 'toastr', 'SweetAlert', 'Upload', '$timeout', '$anchorScroll', 'ngProgressFactory', 'APPCONFIG', '$locale'];

    function CafeUserController($scope, $rootScope, $state, $location, CafeUserService, toastr, SweetAlert, Upload, $timeout, $anchorScroll, ngProgressFactory, APPCONFIG, $locale) {
        var vm = this;

        $anchorScroll();
        vm.progressbar = ngProgressFactory.createInstance();
        $rootScope.bodyClass = 'internetCafe';

        var userID = $rootScope.user.user_id;
        var userRoleID = $rootScope.user.user_role_id;
        var userName = $rootScope.user.Name;
        
        vm.getNotification = getNotification;
        vm.clearAllNotification = clearAllNotification;
        vm.getWorkerList = getWorkerList;
        vm.getSingleWorker = getSingleWorker;
        vm.changeImagePopup = changeImagePopup;
        vm.closeImagePop = closeImagePop;
        vm.saveImage = saveImage;
        vm.saveWorker = saveWorker;
        vm.deleteWorker = deleteWorker;
        vm.Uploads = Uploads;
        vm.changePage = changePage;
        vm.formatDate = formatDate;
        vm.formatTime = formatTime;
        vm.formatDateOfBirth = formatDateOfBirth;
        vm.ageCalculate = ageCalculate;
        vm.workerAgeDisable = workerAgeDisable;
        vm.reset = reset;
        vm.sort = sort;
        vm.resetAssessment = resetAssessment;
        vm.resetWorkerRegi = resetWorkerRegi;
        vm.searchAssessment = searchAssessment;
        vm.getAssessmentList = getAssessmentList;
        vm.workerAssessment = workerAssessment;
        vm.bookAssessor = bookAssessor;
        vm.closePop = closePop;
        vm.resetBooking = resetBooking;
        vm.getBookingList = getBookingList;
        vm.searchBooking = searchBooking;
        vm.getAssessorProfile = getAssessorProfile;
        vm.closePopup = closePopup;

        vm.getAllCountries = getAllCountries;
        vm.getStatesByID = getStatesByID;
        vm.getCityByID = getCityByID;
        vm.getSuburbByID = getSuburbByID;
        vm.getSuburbByMID = getSuburbByMID;
        vm.getAllStates = getAllStates;

        vm.getAllArea = getAllArea;
        vm.getAllSkill = getAllSkill;

        var total = 0; // Mult Suburb
        vm.totalWorker = 0;
        vm.cafePerPage = 10; // this should match however many results your API puts on one page
        vm.pagination = { current: 1 };
        vm.orderInfo = {};
        vm.imageFlag = false;
        vm.editFlag = false;
        vm.workerForm = { user_id: ''};
        vm.scheduleForm = [];
        vm.getAssCountry = '';
        
        vm.currentYear = new Date().getFullYear();
        vm.month = $locale.DATETIME_FORMATS.MONTH;
        vm.monthing= [];

        angular.forEach(vm.month, function(values, key) {
            vm.monthing.push({"id" : key + 1, "name" : values });
        });

        
        $scope.regex = /^[@a-zA-Z][^`~!#$%\^&*()_+={}|[\]\\:;"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        $scope.stepsModel = [];
        $scope.stepsModel2 = [];
        $scope.imageIsLoaded = function(e) {
            //console.log(e.target.result);
            var imgStr = e.target.result;
            var imgTypes = imgStr.split(";");
            var types_ = imgTypes[0].split(":")[1];
            //console.log(types_);
            // if(!$scope.isVideo(types_)){
            //     angular.element("#uplVideo").val("");
            //     swal({ title: "Oops!", text: "video type is invalid.", type: "error" });
            //     return false;
            // }
           
        }

        
        /* to format date*/
        function formatDate(date) {
            return moment(date).format("MMMM Do YYYY");
        }//END formatDate()

        /* to format time*/
        function formatTime(time) {
            return moment(time, "HH:mm:ss").format("HH:mm");
        }//END formatTime()

        /* to format date of birth*/
        function formatDateOfBirth(date) {
            return moment(date).format("YYYYMMDD");
        }//END formatDate()

        /* calculate age from date of birth */
        function ageCalculate(birthDate) {
            return moment().diff(moment(birthDate, 'YYYYMMDD'), 'years')
        }

        /* to format date of birth*/
        function workerAgeDisable() {
            this.myDate = new Date();
            vm.currentDate =  new Date(    
                this.myDate.getFullYear() - 21,    
                this.myDate.getMonth(),    
                this.myDate.getDate()  
            );
        }//END formatDate()
        
        /* for sorting worker user list */
        function sort(keyname) {
            vm.sortKey = keyname; //set the sortKey to the param passed
            vm.reverse = !vm.reverse; //if true make it false and vice versa
            vm.orderInfo = { orderColumn: vm.sortKey, orderBy: vm.reverse == true ? 'ASC' : 'DESC' };
            getWorkerList(1, '');
        }//END sort()

        /* call when page changes */
        function changePage(newPage, searchInfo) {
            getWorkerList(newPage, searchInfo);
            getAssessmentList(newPage, searchInfo);
        }//END changePage();

        /* to extract parameters from url */
        var path = $location.path().split("/");        
        if (path[1] == "cafe") {
            vm.getAllArea();
        }

        if (path[2] == "notification") {
            vm.getNotification(userID);
        }
        vm.getNotification(userID);
         
        if(path[2] == "create"){
            vm.getAllCountries();
        }

        if (path[2] == "booking") {
            vm.getAllStates();
        }
                
        if (path[2] == "scheduleform") {
            var monthS = moment().add(1, 'days');
            // vm.myDate = new Date();
            vm.myDate = monthS._d;
                        
            vm.workerID = path[3];
            vm.getSingleWorker(path[3]);
            $timeout( function(){
                vm.progressbar.start();
                getAssessmentList(1, '');
            }, 2000 );                
        }

        if (path[2] == "edit" || path[2] == "view") {
            if (path[3] == "") {
                $state.go('frontoffice');
                return false;
            } else {
                vm.editFlag = true;
                vm.progressbar.start();
                vm.getAllCountries();
                vm.getAllArea();
                vm.getAllSkill();
                $timeout( function(){
                    vm.getSingleWorker(path[3]);
                }, 1000 );                
            }
        }

        /* to get notification list */
        function getNotification(userID) {
            CafeUserService.getNotifications(userID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        $rootScope.countNot = response.data.count;
                        vm.allNotification = response.data.notifications;
                    } else {
                        vm.allNotification.error = "No data for notifications";
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Internet Cafe');
            });
        }//END getNotification(); 

        /* to clear all notifications */
        function clearAllNotification() {
            SweetAlert.swal({
                title: "Are you sure you want to delete all notifications?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CafeUserService.clearAllNotifications(userID).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.getNotification(userID);
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Internet Cafe');
                    });
                }
            });
        }//END clearAllNotification();    

        /* to get worker list */
        function getWorkerList(newPage, obj) {
            vm.totalWorker = 0;
            vm.workerList = [];
            CafeUserService.getWorkerList(newPage, obj, vm.orderInfo, userID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.workers && response.data.workers.length > 0) {
                        vm.totalWorker = response.data.total;
                        vm.workerList = response.data.workers;

                        angular.forEach(vm.workerList, function(value, key) {
                            vm.workerList[key].assessmentFlag = '';

                            var suburb = [];
                            suburb = value.workers_suburb;
                            var arr = suburb.split(',');
                            
                            angular.forEach(vm.areaList, function(values) {
                                if(arr[0] == values.suburb_id ){
                                    vm.workerList[key].suburbA = values.suburb_name;
                                }

                                if(arr[1] == values.suburb_id ){
                                    vm.workerList[key].suburbB = values.suburb_name;
                                }

                                if(arr[2] == values.suburb_id ){
                                    vm.workerList[key].suburbC = values.suburb_name;
                                }

                                if(arr[3] == values.suburb_id ){
                                    vm.workerList[key].suburbD = values.suburb_name;
                                }
                            });

                            var workersID = value.user_id;
                            CafeUserService.checkAssessment(userID, workersID).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.status == 1) {
                                        if(responses.data.assessment.length == 1){
                                            vm.workerList[key].statusFlag = responses.data.assessment[0].status;
                                            if(responses.data.assessment[0].request_assessLevel == 0){
                                                vm.workerList[key].assessmentFlag = 0;
                                            }else{
                                                vm.workerList[key].assessmentFlag = 1;
                                            }
                                        }else{
                                            vm.assessment = responses.data.assessment;
                                            angular.forEach(vm.assessment, function(values) {
                                                vm.workerList[key].statusFlag = values.status;
                                                if(values.request_assessLevel == 0){
                                                    vm.workerList[key].assessmentFlag = 0;
                                                }else{
                                                    vm.workerList[key].assessmentFlag = 1;
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                            
                        });

                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getWorkerList();

        /* to reset all search parameters in listing */
        function reset() {
            vm.search = [];
            getWorkerList(1, '');
        }//END reset();

        /* to get worker booking list */
        function getBookingList(newPage, obj) {
            vm.progressbar.start();
            vm.totalBooking = 0;
            vm.bookingList = [];
            CafeUserService.getBookingList(newPage, obj, vm.orderInfo, userID).then(function (response) {
                if (response.status == 200) {
                    if (response.data.booking && response.data.booking.length > 0) {
                        vm.totalBooking = response.data.total;
                        vm.bookingList = response.data.booking;

                        angular.forEach(vm.bookingList, function(value, key) {
                            vm.bookingList[key].assessmentFlag = '';

                            var workersID = value.user_id;
                            CafeUserService.checkAssessment(userID, workersID).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.status == 1) {
                                        if(responses.data.assessment.length == 1){
                                            vm.bookingList[key].statusFlag = responses.data.assessment[0].status;
                                            if(responses.data.assessment[0].request_assessLevel == 0){
                                                vm.bookingList[key].assessmentFlag = 0;
                                            }else{
                                                vm.bookingList[key].assessmentFlag = 1;
                                            }
                                        }else{
                                            vm.assessment = responses.data.assessment;
                                            angular.forEach(vm.assessment, function(values) {
                                                vm.bookingList[key].statusFlag = values.status;
                                                if(values.request_assessLevel == 0){
                                                    vm.bookingList[key].assessmentFlag = 0;
                                                }else{
                                                    vm.bookingList[key].assessmentFlag = 1;
                                                }
                                            });
                                        }
                                    }
                                }
                            });                            
                        });
                    }
                    vm.progressbar.complete();
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getBooking();

         /* to reset all search booking parameters in listing */
         function resetBooking() {
            vm.search = [];
            vm.hideCity = false;
            vm.hideSuburb = false;
            getBookingList(1, '');
        }//END resetBooking();

        /* call when all search booking parameters in listing */
        function searchBooking(newPage, searchInfo) {
            getBookingList(newPage, searchInfo);
        }//END searchBooking();

        /* to get single Assessor list */
        function getAssessorProfile(item,index) {
            vm.progressbar.start();
            vm.singleAssessor = [];
            var assessmentDate = item.request_date;
            var id = item.assessor_id;
            
            CafeUserService.getSingleAssessor(id, assessmentDate).then(function (response) {
                if (response.status == 200) {
                    
                    if (response.data.assessor != '') {
                        console.log(response.data.assessor);
                        vm.singleAssessor = response.data.assessor;
                        vm.singleAssessor.request_date = item.request_date;
                        vm.singleAssessor.request_timeFrom = item.request_timeFrom;
                        vm.singleAssessor.request_timeTo = item.request_timeTo;
                        vm.singleAssessor.request_assessLevel = item.request_assessLevel;
                        vm.singleAssessor.status = item.status;
                    }

                    $timeout( function(){
                        var logElem = angular.element("#Booking_details_model");
                        logElem.addClass("in show");
                        vm.progressbar.complete();
                    }, 1000 );                
                                        
                }            
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
            
        }//END getAssessorProfile();

        /* to hide popup */
        function closePopup(){
            var logElem = angular.element("#Booking_details_model");
            logElem.removeClass("in show");
        }//END closePopup();

        /* to get all area [Suburb]  */
        function getAllArea(){
            CafeUserService.getAllArea().then(function (response) {
                if (response.status == 200) {
                    if (response.data.area && response.data.area.length > 0) {
                        vm.areaList = response.data.area;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllArea();

        /* to get all skills  */
        function getAllSkill(){
            CafeUserService.getAllSkill().then(function (response) {
                if (response.status == 200) {
                    if (response.data.skill && response.data.skill.length > 0) {
                        vm.skillList = response.data.skill;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }//END getAllSkill();

        /* Get All Countries */
        function getAllCountries(){
            CafeUserService.getAllCountries().then(function (response) {
                if (response.status == 200) {
                    if (response.data.countries && response.data.countries.length > 0) {
                        vm.countryList = response.data.countries;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllCountries();

        /* Get States by ID */
        function getStatesByID(obj){     
            CafeUserService.getStatesByID(obj).then(function (response) {                
                if (response.status == 200) {
                    if (response.data.states && response.data.states.length > 0) {
                        vm.statesList = response.data.states;                        
                    }else{
                        vm.hideCity = true;
                        delete vm.workerForm.workers_state;
                        delete vm.workerForm.workers_city;
                        delete vm.workerForm.suburb1;
                        delete vm.workerForm.suburb2;
                        delete vm.workerForm.suburb3;
                        delete vm.workerForm.suburb4;

                        delete vm.search.cityID;
                        delete vm.search.suburb;
                    }                    
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getStatesByID();

        /* Get City by ID */
        function getCityByID(obj){
            CafeUserService.getCityByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.citys && response.data.citys.length > 0) {
                        vm.cityList = response.data.citys;
                        vm.hideCity = false;
                        vm.hideSuburb = false;                        
                    }else{
                        vm.hideCity = true;
                        vm.hideSuburb = true;
                        delete vm.workerForm.workers_city;
                        delete vm.workerForm.suburb1;
                        delete vm.workerForm.suburb2;
                        delete vm.workerForm.suburb3;
                        delete vm.workerForm.suburb4;

                        if(typeof vm.search !== 'undefined'){
                            delete vm.search.cityID;
                            delete vm.search.suburb;    
                        }
                        
                    }                    
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getCityByID();

        /* Get Suburb by ID */
        function getSuburbByID(obj){
            // console.log(obj);
            CafeUserService.getSuburbByID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        vm.suburbList  = response.data.suburb;
                        // vm.suburbListA = response.data.suburb;
                        // vm.suburbListB = response.data.suburb;
                        // vm.suburbListC = response.data.suburb;
                        // vm.suburbListD = response.data.suburb;
                        vm.hideSuburb = false;          
                    }else{
                        vm.hideSuburb = true;
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getSuburbByID();

        /* Get Mult Suburb by ID */
        function getSuburbByMID(obj, idType){
            var suburb1 = obj.suburb1;
            var suburb2 = obj.suburb2;
            var suburb3 = obj.suburb3;
            var suburb4 = obj.suburb4;

            CafeUserService.getSuburbByMID(obj).then(function (response) {
                if (response.status == 200) {
                    if (response.data.suburb && response.data.suburb.length > 0) {
                        
                        if(vm.editFlag == true){
                            return false;
                        }

                        if(total == 4){
                            if(idType == 1){                                  
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name }
                                        vm.suburbListB.push(tet);
                                        vm.suburbListC.push(tet);
                                        vm.suburbListD.push(tet); 
                                    }                                                
                                });

                                angular.forEach(vm.suburbListB, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListB.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListC, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListC.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListD, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListD.splice(key,1);
                                    }
                                });                        
                            }

                            if(idType == 2){                                
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name };
                                        vm.suburbListA.push(tet);
                                        vm.suburbListC.push(tet);
                                        vm.suburbListD.push(tet); 
                                    }                                                
                                });
                                
                                angular.forEach(vm.suburbListA, function(value, key) {
                                    if(suburb2 == value.suburb_id){
                                        vm.suburbListA.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListC, function(value, key) {
                                    if(suburb2 == value.suburb_id){
                                        vm.suburbListC.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListD, function(value, key) {
                                    if(suburb2 == value.suburb_id){
                                        vm.suburbListD.splice(key,1);
                                    }
                                });                                
                            }

                            if(idType == 3){                                
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name }
                                        vm.suburbListB.push(tet);
                                        vm.suburbListA.push(tet);
                                        vm.suburbListD.push(tet); 
                                    }                                                
                                });

                                angular.forEach(vm.suburbListB, function(value, key) {
                                    if(suburb3 == value.suburb_id){
                                        vm.suburbListB.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListA, function(value, key) {
                                    if(suburb3 == value.suburb_id){
                                        vm.suburbListA.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListD, function(value, key) {
                                    if(suburb1 == value.suburb_id){
                                        vm.suburbListD.splice(key,1);
                                    }
                                });                                
                            }

                            if(idType == 4){                                
                                angular.forEach(response.data.suburb, function(value, key) {
                                    if(suburb1 != value.suburb_id && suburb2 != value.suburb_id && suburb3 != value.suburb_id && suburb4 != value.suburb_id){
                                        var tet = { "suburb_id" : value.suburb_id, "suburb_name" : value.suburb_name }
                                        vm.suburbListB.push(tet);
                                        vm.suburbListC.push(tet);
                                        vm.suburbListA.push(tet); 
                                    }                                                
                                });

                                angular.forEach(vm.suburbListB, function(value, key) {
                                    if(suburb4 == value.suburb_id){
                                        vm.suburbListB.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListC, function(value, key) {
                                    if(suburb4 == value.suburb_id){
                                        vm.suburbListC.splice(key,1);
                                    }
                                });

                                angular.forEach(vm.suburbListA, function(value, key) {
                                    if(suburb4 == value.suburb_id){
                                        vm.suburbListA.splice(key,1);
                                    }
                                });                                
                            }
                        }else{

                            if(idType == 1){
                                if(typeof suburb2 == 'undefined'){
                                    vm.suburbListB = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListB, function(value, key) {
                                        if(suburb1 == value.suburb_id){
                                            vm.suburbListB.splice(key,1);              
                                        }                
                                    });
                                }

                                if(typeof suburb3 == 'undefined'){
                                    vm.suburbListC = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListC, function(value, key) {
                                        if(suburb1 == value.suburb_id){
                                            vm.suburbListC.splice(key,1);              
                                        }                
                                    });
                                }
                                
                                if(typeof suburb4 == 'undefined'){
                                    vm.suburbListD = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListD, function(value, key) {
                                        if(suburb1 == value.suburb_id){
                                            vm.suburbListD.splice(key,1);              
                                        }                
                                    });
                                }
                                total = total + 1; 
                            }
                        
                            if(idType == 2){                                
                                if(typeof suburb3 == 'undefined'){
                                    vm.suburbListC = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListC, function(value, key) {
                                        if(suburb2 == value.suburb_id){
                                            vm.suburbListC.splice(key,1);              
                                        }                
                                    });
                                }
                                
                                if(typeof suburb4 == 'undefined'){
                                    vm.suburbListD = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListD, function(value, key) {
                                        if(suburb2 == value.suburb_id){
                                            vm.suburbListD.splice(key,1);              
                                        }                
                                    });
                                }

                                if(typeof suburb1 == 'undefined'){
                                    vm.suburbListA = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListA, function(value, key) {
                                        if(suburb2 == value.suburb_id){
                                            vm.suburbListA.splice(key,1);              
                                        }                
                                    });
                                }
                                total = total + 1;
                            }
                        
                            if(idType == 3){                                
                                if(typeof suburb4 == 'undefined'){
                                    vm.suburbListD = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListD, function(value, key) {
                                        if(suburb3 == value.suburb_id){
                                            vm.suburbListD.splice(key,1);              
                                        }                
                                    });
                                }

                                if(typeof suburb1 == 'undefined'){
                                    vm.suburbListA = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListA, function(value, key) {
                                        if(suburb3 == value.suburb_id){
                                            vm.suburbListA.splice(key,1);                 
                                        }                
                                    });
                                }

                                if(typeof suburb2 == 'undefined'){
                                    vm.suburbListB = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListB, function(value, key) {
                                        if(suburb3 == value.suburb_id){
                                            vm.suburbListB.splice(key,1);                           
                                        }                
                                    });
                                }
                                total = total + 1;
                            }
                        
                            if(idType == 4){
                                if(typeof suburb1 == 'undefined'){
                                    vm.suburbListA = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListA, function(value, key) {
                                        if(suburb4 == value.suburb_id){
                                            vm.suburbListA.splice(key,1); 
                                        }                
                                    });
                                }

                                if(typeof suburb2 == 'undefined'){
                                    vm.suburbListB = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListB, function(value, key) {
                                        if(suburb4 == value.suburb_id){
                                            vm.suburbListB.splice(key,1);                           
                                        }                
                                    });
                                }

                                if(typeof suburb3 == 'undefined'){
                                    vm.suburbListC = response.data.suburb;
                                }else{
                                    angular.forEach(vm.suburbListC, function(value, key) {
                                        if(suburb4 == value.suburb_id){
                                            vm.suburbListC.splice(key,1);                           
                                        }                
                                    });
                                }
                                total = total + 1;
                            }                            
                        }
                    }
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });            
        }// END getSuburbByID();

        /* Get All States  */
        function getAllStates(){            
            CafeUserService.getAllStates(userID, userRoleID).then(function (response) {  
                if (response.status == 200) {
                    if (response.data.state && response.data.state.length > 0) {
                        vm.statesList = response.data.state;                        
                    }else{
                        vm.hideCity = true;
                    }                    
                } else {
                    toastr.error(response.data.message, 'Internet Cafe');
                }
            }, function (error) {
                toastr.error('Internal server error', 'Internet Cafe');
            });
        }// END getAllStates();

        /* to get single worker */
        function getSingleWorker(workers_id) {
            CafeUserService.getSingleWorker(workers_id).then(function (response) {                 
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleWorker = response.data.worker;
                        vm.workerForm = response.data.worker;

                        if(vm.workerForm.workers_mobile == 0) {
                            vm.workerForm.workers_mobile = '';
                        }

                        if(vm.workerForm.worker_type == 1) {
                            vm.workerForm.workerType = vm.workerForm.worker_type;
                        }

                        vm.singleWorker.assessmentFlag = '';
                        vm.workerForm.digitalAgreement = vm.workerForm.workers_digitalAgreement;
                        vm.workerForm.friendlyCats = vm.workerForm.workers_friendlyCats;
                        vm.workerForm.friendlyDogs = vm.workerForm.workers_friendlyDogs;
                        var worSub = vm.workerForm.workers_suburb;
                        var workerSuburbsId = worSub.split(',');
                        
                            var workerSub = [];
                            angular.forEach(vm.areaList, function(subValue,subKey) {
                                angular.forEach(workerSuburbsId, function(val) {
                                    if(subValue.suburb_id == val) {
                                        workerSub.push(subValue.suburb_name);
                                    }
                                });
                            });
                            vm.singleWorker.workerSubName = workerSub.toString();

                        angular.forEach(vm.singleWorker, function(value, key) {
                            
                            
                            // if(key == 'workers_suburb'){
                            //     if(value != ''){
                            //         var suburb = [];
                                    
                            //         suburb = value;
                            //         var arr = suburb.split(',');
                                    
                            //         angular.forEach(vm.areaList, function(values) {
                            //             if(arr[0] == values.suburb_id){
                            //                 // vm.singleWorker.suburbA = values.suburb_name;
                            //                 vm.workerForm.suburb1 = values.suburb_id;
                            //             }
                            //             console.log(vm.workerForm.suburb1);
                            //             // if(arr[1] == values.suburb_id ){
                            //             //     vm.singleWorker.suburbB = values.suburb_name;
                            //             //     vm.workerForm.suburb2 = values.suburb_id;
                            //             // }
        
                            //             // if(arr[2] == values.suburb_id ){
                            //             //     vm.singleWorker.suburbC = values.suburb_name;
                            //             //     vm.workerForm.suburb3 = values.suburb_id;
                            //             // }
        
                            //             // if(arr[3] == values.suburb_id ){
                            //             //     vm.singleWorker.suburbD = values.suburb_name;
                            //             //     vm.workerForm.suburb4 = values.suburb_id;
                            //             // }
                            //         });
                            //     }
                            // }

                            if(key == 'workers_image'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file = responses.data.file.file_base_url;
                                                vm.workerForm.workers_image1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_image = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_codeConduct'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file1 = responses.data.file.file_base_url;
                                                vm.workerForm.workers_codeConduct1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_codeConduct = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_criminalReport'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file2 = responses.data.file.file_base_url;
                                                vm.workerForm.workers_criminalReport1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_criminalReport = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_photoIdentity'){
                                if(value != ''){
                                    var file_id = value;
                                    CafeUserService.getImage(file_id).then(function (responses) {
                                        if (responses.status == 200) {
                                            if (responses.data.file && responses.data.file != '') {
                                                vm.singleWorker.file3 = responses.data.file.file_base_url;
                                                vm.workerForm.workers_photoIdentity1 = responses.data.file.file_original_name;
                                                vm.workerForm.workers_photoIdentity = file_id;
                                            }
                                        }
                                    }, function (error) {
                                        toastr.error(error.data.error, 'Error');
                                    });
                                }
                            }

                            if(key == 'workers_skills'){
                                if(value != ''){
                                    vm.skills = [];
                                    
                                    var skill = [];
                                    skill = value;
                                    var arrs = skill.split(',');

                                    for(var i = 0; i < arrs.length; i++ ){
                                    
                                        angular.forEach(vm.skillList, function(values) {
                                            if(arrs[i] == values.skills_id ){
                                                vm.skills.push(values.skills_name);
                                            }
                                                    
                                        });
                                    }
                                }
                            }
                        });

                        if(vm.singleWorker.workers_country != 0){
                            var country = vm.singleWorker.workers_country;
                            vm.getAssCountry = vm.singleWorker.workers_country;
                            angular.forEach(vm.countryList, function(value, key) {
                                if(value.id == country ){
                                    vm.singleWorker.country = value.name;
                                }                               
                            }); 
                        }

                        if(vm.singleWorker.workers_phone != 0){
                            var phone = vm.singleWorker.workers_phone;
                            // vm.singleWorker.workers_phone = phone.toString().slice(2);
                            vm.singleWorker.workers_phone = phone;
                        }

                        if(vm.singleWorker.workers_mobile != 0){
                            var mobile = vm.singleWorker.workers_mobile;
                            // vm.singleWorker.workers_mobile = mobile.toString().slice(2);
                            vm.singleWorker.workers_mobile = mobile;
                        }
                        
                        var workersID = workers_id; 
                        CafeUserService.checkAssessment(userID, workersID).then(function (responses) {
                            
                            if (responses.status == 200) {
                                if (responses.data.status == 1) {
                                    if(responses.data.assessment.length == 1){
                                        vm.singleWorker.statusFlagR = 0;
                                        vm.singleWorker.statusFlag = responses.data.assessment[0].status;
                                        if(responses.data.assessment[0].request_assessLevel == 0){
                                            vm.singleWorker.assessmentFlag = 0;
                                        }else{
                                            vm.singleWorker.assessmentFlag = 1;
                                        }

                                        if(vm.singleWorker.statusFlag == 4){
                                            var month = moment(responses.data.assessment[0].request_date).add(6, 'months').format("YYYY-MM-DD"); 

                                            var now = moment().format("YYYY-MM-DD");
                                            
                                            if(now >= month ){
                                                vm.singleWorker.statusFlagR = 1;
                                            }
                                        }

                                    }else{
                                        vm.assessment = responses.data.assessment;
                                        angular.forEach(vm.assessment, function(values) {
                                            vm.singleWorker.statusFlagR = 0;
                                            vm.singleWorker.statusFlag = values.status;
                                            if(values.request_assessLevel == 0){
                                                vm.singleWorker.assessmentFlag = 0;
                                            }else{
                                                vm.singleWorker.assessmentFlag = 1;
                                            }

                                            if(values.status == 4){
                                                var month = moment(values.request_date).add(6, 'months').format("YYYY-MM-DD"); 
    
                                                var now = moment().format("YYYY-MM-DD");
    
                                                if(now >= month ){
                                                    vm.singleWorker.statusFlagR = 1;
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });

                        vm.getStatesByID(vm.singleWorker);
                        vm.getCityByID(vm.singleWorker);
                        vm.getSuburbByID(vm.singleWorker);

                        
                        delete vm.workerForm.workers_zip;
                        delete vm.workerForm.workers_suspend_start;
                        delete vm.workerForm.workers_suspend_end;
                        delete vm.workerForm.workers_skills;
                        delete vm.workerForm.workers_lat;
                        delete vm.workerForm.workers_long;
                        delete vm.workerForm.workers_booking_rating;

                        vm.isDisabled = true;
                        
                        $timeout( function(){
                            vm.progressbar.complete();
                        }, 300 );
                    } else {
                        vm.progressbar.complete();
                        toastr.error(response.data.message, 'Internet Cafe');
                        $state.go('frontoffice');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                toastr.error(error.data.error, 'Internet Cafe');
            });
        }//END getSingleWorker();

        /* Image upload function */
        function Uploads(file, flag){ 
            var URL = APPCONFIG.APIURL + 'files/add';
            Upload.upload({
                url: URL,
                data: {
                    file: file
                }
            }).then(function (resp) {
                if (resp.data.status === 1) {
                    if(flag == 1){
                        vm.workerForm.workers_image = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;                        
                        vm.imageFlag = true;
                    }
                    if(flag == 2){
                        vm.workerForm.workers_photoIdentity = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;
                        vm.imageFlag = true;
                    }
                    if(flag == 3){
                        vm.workerForm.workers_criminalReport = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;
                        vm.imageFlag = true;
                    }
                    if(flag == 4){
                        vm.workerForm.workers_codeConduct = resp.data.data;
                        vm.imageForm.fileImage = resp.data.data;
                        vm.imageFlag = true;
                    }                                      
                } else {
                    vm.imageFlag = false;
                }
            }, function (resp) {
                toastr.error('Internal server error', 'Error');
            });
        }//END Uploads();

        /*Image Upload*/
        vm.upload = function (file, idType) {
            
            if(idType == 1){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }                
            }

            if(idType == 2){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }
            }
            
            if(idType == 3){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }
            }

            if(idType == 4){
                if(typeof file !== 'undefined'){
                    Uploads(file, idType); 
                }
            }            
           
        };//END Image Upload

        /* Change image when worker profile */
        function changeImagePopup(imageType, id, fileType){
            vm.imageForm = [];
            vm.imageForm.imageType = imageType;
            vm.imageForm.user_id = id;
            
            var file_id = fileType;
            CafeUserService.getImage(file_id).then(function (responses) {
                if (responses.status == 200) {
                    if (responses.data.file && responses.data.file != '') {
                        vm.imageForm.file = responses.data.file.file_base_url;
                    }

                    var imageElem = angular.element("#imageDetails_model");
                    imageElem.addClass("in show");
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END changeImagePopup();

        /* to hide popup */
        function closeImagePop(){
            var logElem = angular.element("#imageDetails_model");
            logElem.removeClass("in show");
        }//END closeImagePop();

        /* to save log after add and edit  */
        function saveImage() {
            vm.isDisabledButton = true;
            vm.progressbar.start();
            if (vm.imageForm.file !== '') { //check if from is valid
                
                if(vm.imageForm.file.size > 0 && vm.imageForm.file.name !== '' ){
                    vm.upload(vm.imageForm.file, vm.imageForm.imageType ); //call upload function  
                }else{
                    vm.imageFlag = true;
                }
                
                $timeout( function(){
                    if(vm.imageFlag == true){                        
                        CafeUserService.saveImage(vm.imageForm).then(function (response) {
                            if (response.status == 200) {
                                if (response.data.status == 1) {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.success(response.data.message, 'Internet Cafe');
                                    var logElem = angular.element("#imageDetails_model");
                                    logElem.removeClass("in show");
                                    $state.reload();                      
                                } else {
                                    vm.progressbar.complete();
                                    vm.isDisabledButton = false;
                                    toastr.error(response.data.message, 'Internet Cafe');
                                }
                            } else {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Internet Cafe');
                            }                            
                        }, function (error) {
                            vm.progressbar.complete();
                            vm.isDisabledButton = false;
                            toastr.error('Internal server error', 'Internet Cafe');
                        });
                    }
                }, 10000 );
            }
        }//END saveImage(); 

        /* to save worker user after add and edit  */
        function saveWorker(imgname1, imgname2, imgname3, imgname4) {
            console.log(vm.workerForm);
            if(typeof vm.workerForm.workers_mobile !== "undefined"){
                if(vm.workerForm.workers_mobile == vm.workerForm.workers_phone){
                    toastr.error("Alternate Number must not be same as Contact Number!", 'Internet Cafe');
                    return false;
                }
            }
            
            if(typeof vm.workerForm.workers_birthDate !== "undefined"){
                var birthDate = vm.formatDateOfBirth(vm.workerForm.workers_birthDate);
                vm.workerForm.workers_age = vm.ageCalculate(birthDate);
            }

            if(typeof vm.workerForm.workers_expiryDateDay == "undefined" || vm.workerForm.workers_expiryDateDay == ''){
                vm.workerForm.workers_expiryDateDay = '';
            } 

            // if(typeof vm.workerForm.workers_city !== "undefined"){
                if(vm.workerForm.workers_idPassport == vm.workerForm.workers_asylum){
                    toastr.error("ID/Passport number and Asylum number should not be same.", 'Internet Cafe');
                    return false;
                }
            // }

            vm.isDisabledButton = true;
            vm.progressbar.start();
            
            
            if(vm.editFlag == false){
                if(typeof imgname1 !== 'undefined'){
                    vm.upload(imgname1[0], 1);
                }

                if(typeof imgname2 !== 'undefined'){
                    vm.upload(imgname2[0], 2);
                }else{
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error("Please upload photo ID!", 'Internet Cafe');
                    return false;
                }

                if(typeof imgname3 !== 'undefined'){
                    vm.upload(imgname3[0], 3);
                }else{
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error("Please upload criminal report!", 'Internet Cafe');
                    return false;
                }

                if(typeof imgname4 !== 'undefined'){
                    vm.upload(imgname4[0], 4);
                }else{
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error("Please upload code of conduct!", 'Internet Cafe');
                    return false;
                }
            }
                
            $timeout( function(){                
                vm.workerForm.cafeuser_id = userID;
                // console.log(vm.workerForm);
                CafeUserService.saveWorker(vm.workerForm).then(function (response) {
                    // console.log(response);
                    if (response.status == 200) {
                        if (response.data.status == 1) {
                            vm.progressbar.complete();
                            toastr.success(response.data.message, 'Internet Cafe');
                            $state.go('frontoffice.cafe');
                        } else {
                            if (response.data.status == 2) {
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Internet Cafe');
                            }else{
                                vm.progressbar.complete();
                                vm.isDisabledButton = false;
                                toastr.error(response.data.message, 'Internet Cafe');
                            }                        
                        }
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Internet Cafe');
                    }
                }, function (error) {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error('Internal server error', 'Internet Cafe');
                });                    
            }, 5000 );
        }//END saveWorker();

        /** to delete a Internet Cafe Worker **/
        function deleteWorker(id, index) {                                  
            SweetAlert.swal({
                title: "Are you sure you want to delete this Worker?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true,
                html: true
            }, function (isConfirm) {
                if (isConfirm) {
                    CafeUserService.deleteWorker(id).then(function (response) {
                        if (response.data.status == 1) {
                            SweetAlert.swal("Deleted!", response.data.message, "success");
                            vm.workerList.splice(index, 1);
                            vm.totalWorker = vm.totalWorker - 1; 
                        } else {
                            SweetAlert.swal("Deleted!", response.data.message, "error");
                        }
                    }, function (error) {
                        toastr.error(error.data.error, 'Internet Cafe');
                    });
                }
            });                   
        }//END deleteWorker();

        /* to reset all search Assessment parameters in listing */
        function resetAssessment() {
            vm.search = [];
            vm.hideCity = false;
            vm.hideSuburb = false;
            getAssessmentList(1, '');
        }//END resetAssessment();


        /* to reset worker register form */
        function resetWorkerRegi() {
           vm.workerForm = '';
           
        }//END resetAssessment();        

        /* call when all search Assessment parameters in listing */
        function searchAssessment(newPage, searchInfo) {
            getAssessmentList(newPage, searchInfo);
        }//END searchAssessment();

        /* to get Assessor list */
        function getAssessmentList(newPage, obj) {
            // obj.getAssCountry = vm.getAssCountry;
            vm.totalAssessor = 0;
            vm.assessorList = [];
            CafeUserService.getAssessorList(newPage, obj, vm.getAssCountry).then(function (response) {
                if (response.status == 200) {
                    if (response.data.assessors && response.data.assessors.length > 0) {
                        vm.totalAssessor = response.data.total;
                        vm.assessorList = response.data.assessors;

                        angular.forEach(vm.assessorList, function(value, key) {
                            
                            var assessorID = value.user_id;
                            // CafeUserService.checkLeave(assessorID, obj).then(function (respon) {
                            //     if (respon.status == 200) {
                            //         if (respon.data.status == 1 && respon.data.dataCheck == 1) {
                            //             vm.assessorList.splice(key);
                            //             vm.totalAssessor = vm.totalAssessor - 1;
                            //         }else{
                                        CafeUserService.checkAssessor(assessorID, obj).then(function (responses) {
                                            if (responses.status == 200) {
                                                if (responses.data.status == 1) {
                                                    if(responses.data.assessment.length == 4 ){
                                                        vm.assessorList.splice(key);
                                                        vm.totalAssessor = vm.totalAssessor - 1;
                                                    }else{
                                                        if(typeof obj !== 'undefined' && obj !== ''){
                                                            if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                                                                vm.assessorList[key].assessmentDate = obj.start_at;
                                                            }else{
                                                                vm.assessorList[key].assessmentDate = vm.myDate;
                                                            }
                                                        }else{
                                                            vm.assessorList[key].assessmentDate = vm.myDate;
                                                        }
                                                    }
                                                }else{
                                                    if(typeof obj !== 'undefined' && obj !== ''){
                                                        if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                                                            vm.assessorList[key].assessmentDate = obj.start_at;
                                                        }else{
                                                            vm.assessorList[key].assessmentDate = vm.myDate;
                                                        }
                                                    }else{
                                                        vm.assessorList[key].assessmentDate = vm.myDate;
                                                    }
                                                }
                                            }
                                        });
                            //         }
                            //     }
                            // });
                            
                        });
                        vm.progressbar.complete();
                    }
                }               
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAssessmentList();

        /* to get single Assessor list */
        function workerAssessment(id, obj, index) {
            // console.log(id);
            // console.log(obj);
            // console.log(index);
            vm.progressbar.start();
            vm.singleAssessor = [];
            vm.timeSlot = [];
            vm.timeSlots = [];
            var defaultSlot = 0;
            var assessmentDate = '';
            if(typeof obj !== 'undefined' && obj !== ''){
                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    assessmentDate = obj.start_at;
                }else{
                    assessmentDate = vm.myDate;
                }
            }else{
                assessmentDate = vm.myDate;
            }

            CafeUserService.getSingleAssessor(id, assessmentDate).then(function (response) {
                if (response.status == 200) {
                    // console.log(response.data.assessor);
                    if (response.data.assessor != '') {
                        vm.singleAssessor = response.data.assessor;
                    }

                    if (response.data.timeSlot != '') {
                        vm.timeSlot = response.data.timeSlot;
                    }

                    if (response.data.defaultSlot != '') {
                        defaultSlot = parseInt(response.data.defaultSlot.setting_value);
                        defaultSlot = (defaultSlot * 60);
                    }
                    
                    vm.timeSlots = createTimeSlots(9, 18, defaultSlot);
                    
                    $timeout( function(){
                        
                        if(vm.timeSlot.length == ''){
                            angular.forEach(vm.timeSlots, function(value1, key1) {
                                
                                var path = value1.timeSlotKey.split("-");  
                                var startTime = path[0];
                                var endTime = path[1]; 
                                vm.timeSlots[key1].bookFlag = 0;
                                vm.timeSlots[key1].request_date = assessmentDate;
                                vm.timeSlots[key1].request_timeFrom = startTime;
                                vm.timeSlots[key1].request_timeTo = endTime;
                                vm.timeSlots[key1].worker_id = vm.workerID;
                            
                            });
                        }else{
                            angular.forEach(vm.timeSlots, function(value1, key1) {
                                angular.forEach(vm.timeSlot, function(value2, key2) { 
                                    
                                    var time = vm.formatTime(value2.request_timeFrom)+'-'+vm.formatTime(value2.request_timeTo);
                                    var path = value1.timeSlotKey.split("-");  
                                    var startTime = path[0];
                                    var endTime = path[1]; 
                                    
                                    vm.timeSlots[key1].request_date = assessmentDate;
                                    vm.timeSlots[key1].request_timeFrom = startTime;
                                    vm.timeSlots[key1].request_timeTo = endTime;
                                    vm.timeSlots[key1].worker_id = vm.workerID;

                                    if(value1.timeSlotKey == time){                
                                        vm.timeSlots[key1].bookFlag = 1;
                                    } 
    
                                });
                            });
                        }
                        
                        var logElem = angular.element("#bo_accepted_new_model");
                        logElem.addClass("in show");
                        vm.progressbar.complete();
                    }, 1000 );                
                                        
                }            
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
            
        }//END workerAssessment();

        /* to hide popup */
        function closePop(){
            var logElem = angular.element("#bo_accepted_new_model");
            logElem.removeClass("in show");
        }//END closePop();

        /* create time slots */
        function createTimeSlots(startHour, endHour, interval) {
            if (!startHour) {
                endHour = 8;
            }
            if (!endHour) {
                endHour = 20;
            }
            var timeSlots = [], dateTime = new Date(), timeStr = '';
            dateTime.setHours(startHour, 0, 0, 0);
            while (new Date(dateTime.getTime() + interval * 60000).getHours() < endHour) {
                timeStr = addZero(dateTime.getHours()) + ':' + addZero(dateTime.getMinutes());
                timeStr += '-';
                dateTime = new Date(dateTime.getTime() + interval * 60000);
                timeStr += dateTime.getHours() + ':' + addZero(dateTime.getMinutes());
                // timeSlots.push(timeStr);
                timeSlots.push({timeSlotKey : timeStr, bookFlag : 0 });
            }
            return timeSlots;
        }//END createTimeSlots();

        /* add zero */
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }//END addZero();

        /* Book worker for assessment to assessor */
        function bookAssessor(obj, id) {
            var assessorID = id;
            // console.log(obj);
            // console.log(id);
            CafeUserService.bookAssessmentRequest(obj, assessorID, userID, userName).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        toastr.success(response.data.message, 'Internet Cafe');
                        $state.go('frontoffice.cafe');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Internet Cafe');
            });
            
        }//END bookAssessor();
        
    }//END CafeUserController

}());
