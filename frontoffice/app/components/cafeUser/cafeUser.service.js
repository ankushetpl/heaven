(function () {
    "use strict";
    angular
        .module('cafeUserApp')
        .service('CafeUserService', CafeUserService);

    CafeUserService.$inject = ['$http', 'APPCONFIG', '$q'];

    function CafeUserService($http, APPCONFIG, $q) {

        /* get all Notifications */
        function getNotifications(userID) {

            var URL = APPCONFIG.APIURL + 'assessor/notification';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['user_id'] = userID;
            }

            return this.runHttp(URL, requestData);
        } //END getNotifications();    

        /* clear all Notifications */
        function clearAllNotifications(userID) {
           
            var URL = APPCONFIG.APIURL + 'assessor/deleteNot';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['user_id'] = userID;
            }

            return this.runHttp(URL, requestData);
        } //END clearAllNotifications();  

        /* save worker user */
        function saveWorker(obj) {
            var URL = APPCONFIG.APIURL + 'cafeUser/add';
            
            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {

                if (typeof obj.user_id !== undefined && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                    URL = APPCONFIG.APIURL + 'cafeUser/update';
                }

                if (typeof obj.workerType !== undefined && obj.workerType !== '') {
                    requestData['worker_type'] = obj.workerType;
                }

                if(obj.workerType == 1){
                    if (typeof obj.worker_message !== undefined && obj.worker_message !== '') {
                        requestData['worker_message'] = obj.worker_message;
                    } 
                }else{
                    requestData['worker_message'] = '';
                }

                if (typeof obj.workers_address !== undefined && obj.workers_address !== '') {
                    requestData['workers_address'] = obj.workers_address;
                }

                if (typeof obj.workers_city !== undefined && obj.workers_city !== '') {
                    requestData['workers_city'] = obj.workers_city;
                }

                if (typeof obj.workers_codeConduct !== undefined && obj.workers_codeConduct !== '') {
                    requestData['workers_codeConduct'] = obj.workers_codeConduct;
                }

                if (typeof obj.workers_country !== undefined && obj.workers_country !== '') {
                    requestData['workers_country'] = obj.workers_country;
                }

                if (typeof obj.workers_criminalReport !== undefined && obj.workers_criminalReport !== '') {
                    requestData['workers_criminalReport'] = obj.workers_criminalReport;
                }

                if (typeof obj.digitalAgreement !== undefined && obj.digitalAgreement !== '') {
                    requestData['workers_digitalAgreement'] = obj.digitalAgreement;
                }

                if (typeof obj.workers_email !== undefined && obj.workers_email !== '') {
                    requestData['workers_email'] = obj.workers_email;
                }

                if (typeof obj.workers_expiryDateDay !== undefined && obj.workers_expiryDateDay !== '') {
                    requestData['workers_expiryDateDay'] = obj.workers_expiryDateDay;
                }

                if (typeof obj.workers_expiryDateMonth !== undefined && obj.workers_expiryDateMonth !== '') {
                    requestData['workers_expiryDateMonth'] = obj.workers_expiryDateMonth;
                }

                if (typeof obj.workers_expiryDateYear !== undefined && obj.workers_expiryDateYear !== '') {
                    requestData['workers_expiryDateYear'] = obj.workers_expiryDateYear;
                }

                if (typeof obj.friendlyDogs !== undefined && obj.friendlyDogs !== '') {
                    requestData['workers_friendlyDogs'] = obj.friendlyDogs;
                }

                if (typeof obj.friendlyCats !== undefined && obj.friendlyCats !== '') {
                    requestData['workers_friendlyCats'] = obj.friendlyCats;
                }

                if (typeof obj.workers_image !== undefined && obj.workers_image !== '') {
                    requestData['workers_image'] = obj.workers_image;
                }

                if (typeof obj.workers_mobile !== undefined && obj.workers_mobile !== '') {
                    requestData['workers_mobile'] = obj.workers_mobile;
                }

                if (typeof obj.workers_name !== undefined && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.workers_phone !== undefined && obj.workers_phone !== '') {
                    requestData['workers_phone'] = obj.workers_phone;
                }

                if (typeof obj.workers_photoIdentity !== undefined && obj.workers_photoIdentity !== '') {
                    requestData['workers_photoIdentity'] = obj.workers_photoIdentity;
                }

                if (typeof obj.workers_state !== undefined && obj.workers_state !== '') {
                    requestData['workers_state'] = obj.workers_state;
                }

                if (typeof obj.location_suburb !== undefined && obj.location_suburb !== '') {
                    requestData['location_suburb'] = obj.location_suburb;
                }

                // if (typeof obj.suburb2 !== undefined && obj.suburb2 !== '') {
                //     requestData['suburb2'] = obj.suburb2;
                // }

                // if (typeof obj.suburb3 !== undefined && obj.suburb3 !== '') {
                //     requestData['suburb3'] = obj.suburb3;
                // }

                // if (typeof obj.suburb4 !== undefined && obj.suburb4 !== '') {
                //     requestData['suburb4'] = obj.suburb4;
                // }

                if (typeof obj.cafeuser_id !== undefined && obj.cafeuser_id !== '') {
                    requestData['cafeuser_id'] = obj.cafeuser_id;
                }

                if (typeof obj.workers_birthDate !== undefined && obj.workers_birthDate !== '') {
                    requestData['workers_birthDate'] = obj.workers_birthDate;
                }

                if (typeof obj.workers_age !== undefined && obj.workers_age !== '') {
                    requestData['workers_age'] = obj.workers_age;
                }

                if (typeof obj.workers_idPassport !== undefined && obj.workers_idPassport !== '') {
                    requestData['workers_idPassport'] = obj.workers_idPassport;
                }

                if (typeof obj.workers_asylum !== undefined && obj.workers_asylum !== '') {
                    requestData['workers_asylum'] = obj.workers_asylum;
                }

                if (typeof obj.workers_uniform !== undefined && obj.workers_uniform !== '') {
                    requestData['workers_uniform'] = obj.workers_uniform;
                }
            }

            return this.runHttp(URL, requestData);
        } //END saveWorkerUser();

        /* to get all worker user  */
        function getWorkerList(pageNum, obj, orderInfo, userID) {
            var URL = APPCONFIG.APIURL + 'cafeUser';
            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof userID !== 'undefined' && userID !== '') {
                requestData['userID'] = userID;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.workers_name !== 'undefined' && obj.workers_name !== '') {
                    requestData['workers_name'] = obj.workers_name;
                }

                if (typeof obj.workers_phone !== 'undefined' && obj.workers_phone !== '') {
                    requestData['workers_phone'] = obj.workers_phone;
                }

                if (typeof obj.workers_suburb !== 'undefined' && obj.workers_suburb !== '') {
                    requestData['workers_suburb'] = parseInt(obj.workers_suburb);
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
                }

                if (typeof obj.end_at !== 'undefined' && obj.end_at !== '') {
                    requestData['end_at'] = moment(obj.end_at).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getWorkerList();

        /* to get all booking   */
        function getBookingList(pageNum, obj, orderInfo, userID) {
            var URL = APPCONFIG.APIURL + 'cafeUser/booking';
            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            if (typeof userID !== 'undefined' && userID !== '') {
                requestData['userID'] = userID;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.stateID !== 'undefined' && obj.stateID !== '') {
                    requestData['stateID'] = parseInt(obj.stateID);
                }

                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['cityID'] = parseInt(obj.cityID);
                }

                if (typeof obj.status !== 'undefined' && obj.status !== '') {
                    requestData['status'] = parseInt(obj.status);
                }

                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
                }

                if (typeof obj.end_at !== 'undefined' && obj.end_at !== '') {
                    requestData['end_at'] = moment(obj.end_at).format('YYYY-MM-DD');
                }
            }

            if (typeof orderInfo !== 'undefined' && orderInfo !== '') {
                if (typeof orderInfo.orderColumn !== 'undefined' && orderInfo.orderColumn !== '') {
                    requestData['orderColumn'] = orderInfo.orderColumn;
                }
                if (typeof orderInfo.orderBy !== 'undefined' && orderInfo.orderBy !== '') {
                    requestData['orderBy'] = orderInfo.orderBy;
                }
            }
            
            return this.runHttp(URL, requestData);
        }//END getBookingList();

        /* to get single worker */
        function getSingleWorker(user_id) {
            var URL = APPCONFIG.APIURL + 'cafeUser/view';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWorker();

        //Get All Country
        function getAllCountries(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCountries';
            var requestData = {};
            return this.runHttp(URL, requestData);
        }//END getAllCountries();

        //Get All States
        function getAllStates(userID, userRoleID){
            var URL = APPCONFIG.APIURL + 'assessor/getProvince';
            var requestData = {};

            if (typeof userID !== 'undefined' && userID !== '') {
                requestData['userID'] = parseInt(userID);
            }

            if (typeof userRoleID !== 'undefined' && userRoleID !== '') {
                requestData['userRoleID'] = parseInt(userRoleID);
            }

            return this.runHttp(URL, requestData);
        }//END getAllStates();

        //Get States
        function getStatesByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allStatesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_country !== 'undefined' && obj.workers_country !== '') {
                    requestData['ID'] = parseInt(obj.workers_country);
                }

                if (typeof obj.countryID !== 'undefined' && obj.countryID !== '') {
                    requestData['ID'] = parseInt(obj.countryID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getStatesByID();

        //Get city 
        function getCityByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allCitiesByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_state !== 'undefined' && obj.workers_state !== '') {
                    requestData['ID'] = parseInt(obj.workers_state);
                }
                
                if (typeof obj.stateID !== 'undefined' && obj.stateID !== '') {
                    requestData['ID'] = parseInt(obj.stateID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getCityByID();

        //Get suburb by ID 
        function getSuburbByID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }
                
                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['ID'] = parseInt(obj.cityID);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByID();

        //Get Mult suburb by ID 
        function getSuburbByMID(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbByMID';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.workers_city !== 'undefined' && obj.workers_city !== '') {
                    requestData['ID'] = parseInt(obj.workers_city);
                }
                
                if (typeof obj.suburb1 !== 'undefined' && obj.suburb1 !== '') {
                    requestData['suburb1'] = parseInt(obj.suburb1);
                }

                if (typeof obj.suburb2 !== 'undefined' && obj.suburb2 !== '') {
                    requestData['suburb2'] = parseInt(obj.suburb2);
                }

                if (typeof obj.suburb3 !== 'undefined' && obj.suburb3 !== '') {
                    requestData['suburb3'] = parseInt(obj.suburb3);
                }

                if (typeof obj.suburb4 !== 'undefined' && obj.suburb4 !== '') {
                    requestData['suburb4'] = parseInt(obj.suburb4);
                }
            }

            return this.runHttp(URL, requestData);
        }//END getSuburbByMID();

        //Get all suburb 
        function getAllArea(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSuburbs';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllArea();

        //Get all skill 
        function getAllSkill(){
            var URL = APPCONFIG.APIURL + 'cafeUser/allSkills';
            var requestData = {};

            return this.runHttp(URL, requestData);
        }//END getAllSkill();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        /* to check Assessment */
        function checkAssessment(userID, workersID) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkAssessment';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['cafe_id'] = userID;
            }

            if (typeof workersID !== undefined && workersID !== '') {
                requestData['worker_id'] = workersID;
            }
                        
            return this.runHttp(URL, requestData);
        } //END checkAssessment();

        /* to delete a Internet Cafe Worker from database */
        function deleteWorker(user_id) {
            var URL = APPCONFIG.APIURL + 'user/delete';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            return this.runHttp(URL, requestData);
        } //END deleteWorker();

        /* to get all worker user  */
        function getAssessorList(pageNum, obj, getAssCountry) {
            var URL = APPCONFIG.APIURL + 'cafeUser/assessmentList';            
            var requestData = {};

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }
            
            if (typeof getAssCountry !== 'undefined' && getAssCountry !== '') {
                requestData['getAssCountry'] = getAssCountry;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.getAssCountry !== 'undefined' && obj.getAssCountry !== '') {
                    requestData['getAssCountry'] = parseInt(obj.getAssCountry);
                }
                
                if (typeof obj.stateID !== 'undefined' && obj.stateID !== '') {
                    requestData['stateID'] = parseInt(obj.stateID);
                }

                if (typeof obj.cityID !== 'undefined' && obj.cityID !== '') {
                    requestData['cityID'] = parseInt(obj.cityID);
                }

                if (typeof obj.suburb !== 'undefined' && obj.suburb !== '') {
                    requestData['suburb'] = parseInt(obj.suburb);
                }

                if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                    requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
                }
            }

            return this.runHttp(URL, requestData);
        }//END getAssessorList();

        /* to check Assessor */
        function checkAssessor(assessorID, obj) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkAssessor';
            var requestData = {};

            if (typeof assessorID !== undefined && assessorID !== '') {
                requestData['assessorID'] = assessorID;
            }

            if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
            }else{
                requestData['start_at'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END checkAssessor();

        /* to check Assessor Leave */
        function checkLeave(assessorID, obj) {
            var URL = APPCONFIG.APIURL + 'cafeUser/checkLeave';
            var requestData = {};

            if (typeof assessorID !== undefined && assessorID !== '') {
                requestData['assessorID'] = assessorID;
            }

            if (typeof obj.start_at !== 'undefined' && obj.start_at !== '') {
                requestData['start_at'] = moment(obj.start_at).format('YYYY-MM-DD');
            }else{
                requestData['start_at'] = moment(new Date()).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END checkLeave();

        /* to get single Assessor */
        function getSingleAssessor(user_id, assessmentDate) {
            // console.log(assessmentDate);
            var URL = APPCONFIG.APIURL + 'cafeUser/viewAssessor';
            var requestData = {};

            if (typeof user_id !== undefined && user_id !== '') {
                requestData['user_id'] = user_id;
            }

            if (typeof assessmentDate !== 'undefined' && assessmentDate !== '') {
                requestData['start_at'] = moment(assessmentDate).format('YYYY-MM-DD');
            }

            return this.runHttp(URL, requestData);
        } //END getSingleAssessor();

        /* Booking request to the Assessor */
        function bookAssessmentRequest(obj, assessorID, userID, userName) {
            var URL = APPCONFIG.APIURL + 'cafeUser/bookAssessor';
            var requestData = {};

            if (typeof userID !== undefined && userID !== '') {
                requestData['cafe_id'] = userID;
            }

            if (typeof userName !== undefined && userName !== '') {
                requestData['cafe_name'] = userName;
            }
            
            if (typeof assessorID !== undefined && assessorID !== '') {
                requestData['assessor_id'] = assessorID;
            }

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.worker_id !== 'undefined' && obj.worker_id !== '') {
                    requestData['worker_id'] = parseInt(obj.worker_id);
                }

                if (typeof obj.request_timeFrom !== 'undefined' && obj.request_timeFrom !== '') {
                    requestData['request_timeFrom'] = obj.request_timeFrom;
                }

                if (typeof obj.request_timeTo !== 'undefined' && obj.request_timeTo !== '') {
                    requestData['request_timeTo'] = obj.request_timeTo;
                }

                if (typeof obj.request_date !== 'undefined' && obj.request_date !== '') {
                    requestData['request_date'] = moment(obj.request_date).format('YYYY-MM-DD');
                }
            }

            return this.runHttp(URL, requestData);
        } //END bookAssessmentRequest();

        /* Save image */
        function saveImage(obj){
            var URL = APPCONFIG.APIURL + 'cafeUser/updateImage';
            var requestData = {};

            if (typeof obj !== 'undefined' && obj !== '') {
                
                if (typeof obj.fileImage !== 'undefined' && obj.fileImage !== '') {
                    requestData['fileImage'] = parseInt(obj.fileImage);
                }

                if (typeof obj.imageType !== 'undefined' && obj.imageType !== '') {
                    requestData['imageType'] = obj.imageType;
                }

                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                }
            }

            return this.runHttp(URL, requestData);
        }//END saveImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,
            getAllCountries: getAllCountries,
            getAllStates: getAllStates,
            getStatesByID: getStatesByID,
            getCityByID: getCityByID,
            getSuburbByID: getSuburbByID,
            getSuburbByMID: getSuburbByMID,
            getAllArea: getAllArea,
            getAllSkill: getAllSkill,
            getNotifications: getNotifications,
            clearAllNotifications: clearAllNotifications,
            getWorkerList: getWorkerList,
            getSingleWorker: getSingleWorker,
            getImage: getImage,
            saveWorker: saveWorker,
            deleteWorker: deleteWorker,
            checkAssessment: checkAssessment,
            getAssessorList: getAssessorList,
            checkAssessor: checkAssessor,
            checkLeave: checkLeave,
            getSingleAssessor: getSingleAssessor,
            bookAssessmentRequest: bookAssessmentRequest,
            getBookingList: getBookingList,
            saveImage: saveImage
        }

    };//END CafeUserService()
}());
