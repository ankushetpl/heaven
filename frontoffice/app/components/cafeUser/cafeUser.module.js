(function () {

    angular.module('cafeUserApp', [
        // 'angucomplete-alt',
        // 'ngFileUpload',
        // 'angularjs-datetime-picker'
        //'ui.sortable'
    ]).config(config);

    function config($stateProvider, $urlRouterProvider) {

        var cafePath = './app/components/cafeUser/';
        $stateProvider
            .state('frontoffice.cafe', {
                url: 'cafe',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/index.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.create', {
                url: '/create',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/form.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.edit', {
                url: '/edit/:id',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/formEdit.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.view', {
                url: '/view/:id',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/view.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.notification', {
                url: '/notification',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/notification.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.booking', {
                url: '/booking',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/booking.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            })
            .state('frontoffice.cafe.appointform', {
                url: '/scheduleform/:id',
                views: {
                    'content@frontoffice': {
                        templateUrl: cafePath + 'views/appointmentform.html',
                        controller: 'CafeUserController',
                        controllerAs: 'cafe'
                    }
                }
            });
    }

}());