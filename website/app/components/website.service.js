(function () {
    "use strict";
    angular
        .module('websiteApp')
        .service('WebsiteService', WebsiteService);

    WebsiteService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WebsiteService($http, APPCONFIG, $q) {

        /* to get single web page */
        function getSingleWeb(page_slug) {
            var URL = APPCONFIG.APIURL + 'website/view';
            var requestData = {};

            if (typeof page_slug !== undefined && page_slug !== '') {
                requestData['page_slug'] = page_slug;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeb();

        /* to get all web page */
        function allPage() {
            var URL = APPCONFIG.APIURL + 'page/AllPage';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END allPage();

        /* to check password link web page */
        function getPasswordForm(dataForm){
            var URL = APPCONFIG.APIURL + 'login/checkForgot';
            var requestData = {};

            if (typeof dataForm !== undefined && dataForm !== '') {
                if (typeof dataForm.user_id !== undefined && dataForm.user_id !== '') {
                    requestData['user_id'] = dataForm.user_id;
                }

                if (typeof dataForm.device_token !== undefined && dataForm.device_token !== '') {
                    requestData['device_token'] = dataForm.device_token;
                }
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeb();

        /* to save password web page */
        function savePassword(obj){
            var URL = APPCONFIG.APIURL + 'login/savePassword';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                }

                if (typeof obj.device_token !== 'undefined' && obj.device_token !== '') {
                    requestData['device_token'] = obj.device_token;
                }

                if (typeof obj.new_password !== 'undefined' && obj.new_password !== '') {
                    requestData['user_password'] = obj.new_password;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END savePassword();

        /* to active account web page */
        function setActivationToken(dataForm){
            var URL = APPCONFIG.APIURL + 'login/activeAccount';
            var requestData = {};

            if (typeof dataForm !== undefined && dataForm !== '') {
                if (typeof dataForm.user_id !== undefined && dataForm.user_id !== '') {
                    requestData['user_id'] = dataForm.user_id;
                }

                if (typeof dataForm.device_token !== undefined && dataForm.device_token !== '') {
                    requestData['device_token'] = dataForm.device_token;
                }

                if (typeof dataForm.clients_referID !== undefined && dataForm.clients_referID !== '') {
                    requestData['clients_referID'] = dataForm.clients_referID;
                }
            }

            return this.runHttp(URL, requestData);
        } //END setActivationToken();

        /* to save contact web page */
        function saveContact(obj){
            var URL = APPCONFIG.APIURL + 'support/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.contactUs_name !== 'undefined' && obj.contactUs_name !== '') {
                    requestData['contactUs_name'] = obj.contactUs_name;
                }

                if (typeof obj.contactUs_from !== 'undefined' && obj.contactUs_from !== '') {
                    requestData['contactUs_from'] = obj.contactUs_from;
                }

                if (typeof obj.contactUs_subject !== 'undefined' && obj.contactUs_subject !== '') {
                    requestData['contactUs_subject'] = obj.contactUs_subject;
                }

                if (typeof obj.contactUs_msg !== 'undefined' && obj.contactUs_msg !== '') {
                    requestData['contactUs_msg'] = obj.contactUs_msg;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveContact();

        /* to get all role */
        function getAllRoll(pageNum) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = {}; 

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            requestData['orderColumn'] = 'role_name'; 
            requestData['orderBy'] = 'ASC';          
            return this.runHttp(URL, requestData);
        }//END getAllRoll();

        /* to get faq by id */
        function getFAQ(id) {
            var URL = APPCONFIG.APIURL + 'faq/allFAQs';

            var requestData = {}; 
            
            if (typeof id !== 'undefined' && id !== '') {
                requestData['role_id'] = id;
            }
           
            return this.runHttp(URL, requestData);
        }//END getFAQ();

        /* get all data */
        function getAllData() {
            var URL = APPCONFIG.APIURL + 'website';

            var requestData = {}; 
                       
            return this.runHttp(URL, requestData);
        }//END getAllData();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,            
            getSingleWeb: getSingleWeb,
            allPage: allPage,
            getPasswordForm: getPasswordForm,
            savePassword: savePassword,
            setActivationToken: setActivationToken,
            saveContact: saveContact,
            getAllRoll: getAllRoll,
            getFAQ: getFAQ,
            getAllData: getAllData,
            getImage: getImage
        }

    };//END WebsiteService()
}());
