(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkValidUser = checkValidUser;
        
        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'login/validate-user';
            var deferred = $q.defer();
            var requestData = {};
            // requestData['usertype'] = 1;
            requestData['token'] = getAuthToken();
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                if (response.data.status == 0) {
                    $rootScope.$broadcast('auth:login:required');
                } else {
                    $rootScope.isLogin = true;
                    saveUserInfo(response.data);
                    if (isAuth === false) {
                        $rootScope.$broadcast('auth:login:success');
                    }
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.data;
                $rootScope.user = user;
            }
            return user;
        } //END saveUserInfo();


        return self;
        
    };
})();