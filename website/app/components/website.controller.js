(function () {
    "use strict";
    angular.module('websiteApp')
        .controller('WebsiteController', WebsiteController);

    WebsiteController.$inject = ['$scope', '$location', '$state', 'WebsiteService', '$rootScope', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function WebsiteController($scope, $location, $state, WebsiteService, $rootScope, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.isActive = isActive;
        vm.getCurrentState = getCurrentState;
        vm.getSingleWeb = getSingleWeb;
        vm.getPasswordForm = getPasswordForm;
        vm.savePassword = savePassword;
        vm.setActivationToken = setActivationToken;
        vm.saveContact = saveContact;
        vm.resetForm = resetForm;
        vm.topHeader = topHeader;
        
        console.log($state.current.name, $rootScope.headerTitle);

        vm.passwordForm = { user_id : '' };
        $scope.form = {};
        vm.editFlag = false;

        $scope.regex = /^[a-zA-Z0-9]{8,20}[^`~!#@$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        vm.progressbar = ngProgressFactory.createInstance();
        vm.progressbar.setColor('blue');

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

        /* to extract parameters from url */
        var path = $location.path().split("/");      
        if (path[1] == "forgot-password") {
            if (path[3] == "") {
                $state.go('website');
                return false;
            } else {
                vm.editFlag = true;
                $timeout( function(){
                    vm.getPasswordForm(path[2], path[3]);
                }, 100 );
            }
        }

        if (path[1] == "activation-token") {
            if (path[5] == "") {
                $state.go('website');
                return false;
            } else {
                $timeout( function(){
                    vm.setActivationToken(path[2], path[3], path[4]);
                }, 500 );
            }
        }
        
        function getPasswordForm(id, code){
            var dataForm = { user_id : id, device_token : code };
            
            WebsiteService.getPasswordForm(dataForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.passwordForm.user_id = id;
                        vm.passwordForm.device_token = code;  
                    } else {
                        toastr.error(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getPasswordForm();
        
        function savePassword(){
            vm.isDisabledButton = true;
            vm.progressbar.start();
            WebsiteService.savePassword(vm.passwordForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        toastr.success(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END savePassword();

        function topHeader(){
            $anchorScroll();
        }

        /* to get single web page */
        function getSingleWeb(page_id) {
            $anchorScroll();
            WebsiteService.getSingleWeb(page_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.page;                        
                    } else {
                        toastr.error(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getSingleWeb();

        /* to active account */
        function setActivationToken(id, token, codeID){
            var dataForm = { user_id : id, device_token : token, clients_referID : codeID };
            
            WebsiteService.setActivationToken(dataForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.go('website');
                        return false;
                    } else {
                        SweetAlert.swal("Error!", response.data.message, "error");
                        $state.go('website');
                        return false;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        } //End setActivationToken();

        /* save contact details */
        function saveContact(){
            vm.isDisabledButton = true;
            vm.progressbar.start();
            WebsiteService.saveContact(vm.contactForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        $anchorScroll();
                        toastr.success(response.data.message, 'Heavenly Sent');
                        $state.reload();
                        return true;
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Heavenly Sent');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Heavenly Sent');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Heavenly Sent');
            });
        }//END saveContact();

        /* to reset all parameters */
        function resetForm(){
            vm.contactForm = {};
        }//END resetForm();

    }

}());


