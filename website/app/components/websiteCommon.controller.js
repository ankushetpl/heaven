(function () {
    "use strict";
    angular.module('websiteApp')
        .controller('WebsiteCommonController', WebsiteCommonController);

    WebsiteCommonController.$inject = ['$scope', '$location', '$state', 'WebsiteService', '$rootScope', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function WebsiteCommonController($scope, $location, $state, WebsiteService, $rootScope, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.isActive = isActive;
        vm.getSingleWeb = getSingleWeb;
        vm.allPage = allPage;
        vm.getAllRoll = getAllRoll;
        vm.getFAQ = getFAQ;
        vm.getAllData = getAllData; 
        
        $scope.accordion = {
            current: null
        };
        
        $scope.oneAtATime = true;

        console.log($state.current.name, $rootScope.headerTitle);

        vm.allPage();
        vm.getAllData();

        vm.progressbar = ngProgressFactory.createInstance();
        
        /* to extract parameters from url */
        var path = $location.path().split("/");      
        if (path[1] != "") {
            $timeout( function(){
                vm.getSingleWeb(path[1]);
            }, 1000 );
        }

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        /* to get single web page */
        function getSingleWeb(page_slug) {
            $anchorScroll();
            WebsiteService.getSingleWeb(page_slug).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.page;                        
                    }                    
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getSingleWeb();

        /* get all page data */
        function allPage(){
            vm.pagelist = [];
            WebsiteService.allPage().then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.pagelist = response.data.data;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END allPage();

        /* all role type */
        function getAllRoll() {
            vm.roleList = [];
            WebsiteService.getAllRoll('1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllRoll()

        /* get faq data */
        function getFAQ(id){
            vm.allFaq = [];
            WebsiteService.getFAQ(id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allFaq = response.data.faqs;
                    }                    
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getFAQ();

        /* get all data */
        function getAllData(){
            vm.logCheck = [];
            
            if(typeof $rootScope.user !== 'undefined' && $rootScope.user !== ''){
                if($rootScope.user.user_role_id == 2 || $rootScope.user.user_role_id == 4 ){
                    vm.logCheck.login = 1;
                    vm.logCheck.URL = 'frontoffice/#!/auth/login';
                }else{
                    vm.logCheck.login = 1;
                    vm.logCheck.URL = 'backoffice/#!/auth/login';
                }
            }else{
                vm.logCheck.login = 0;
                vm.logCheck.URL = 'frontoffice/#!/auth/login';
            }

            WebsiteService.getAllData().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.headerSide = response.data.setting;
                        vm.socialSide = response.data.social;
                        vm.emailSide = response.data.email;
                        vm.bannerSide = response.data.banner;
                        
                        angular.forEach(vm.bannerSide, function(value, key) {
                            vm.bannerSide[key].file = '';
                            var file_id = value.banner_image;
                            WebsiteService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.bannerSide[key].file = responses.data.file.file_base_url;
                                    }
                                }
                            });
                        });    

                        vm.headerSide[4].file = '';
                        var file_id = vm.headerSide[4].setting_value;
                        WebsiteService.getImage(file_id).then(function (responses) {
                            if (responses.status == 200) {
                                if (responses.data.file && responses.data.file != '') {
                                    vm.headerSide[4].file = responses.data.file.file_base_url;
                                }
                            }
                        });

                        vm.getSingleWeb('about');
                    }                    
                }

            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getAllData();
        
    }

}());


