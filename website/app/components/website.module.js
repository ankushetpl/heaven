(function () {
    'use strict';
    var webSiteApp = angular.module('websiteApp', ['authApp']);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        return authServices.checkValidUser(true);
    } //END authenticateUser()

    webSiteApp.config(funConfig);
    webSiteApp.run(funRun);

    webSiteApp.component("headerComponent", {
        templateUrl: 'website/app/layouts/header.html',
        controller: 'WebsiteCommonController',
        controllerAs: 'header'
    });

    webSiteApp.component("footerComponent", {
        templateUrl: 'website/app/layouts/footer.html',
        controller: 'WebsiteCommonController',
        controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('website', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'website/app/layouts/layout.html'
                    },
                    'content@website': {
                        templateUrl: 'website/app/components/links/index.html',
                        controller: 'WebsiteCommonController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.viewPage', {
                url: ':text',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/commonPage.html',
                        controller: 'WebsiteCommonController',
                        controllerAs: 'website'
                    }
                }
            })
            .state('website.faq', {
                url: 'faq',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/faq.html',
                        controller: 'WebsiteCommonController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.resetPassword', {
                url: 'forgot-password/:id/:code',
                // url: 'forgot-passwords',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/forgotPassword.html',
                        controller: 'WebsiteController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.activation', {
                url: 'activation-token/:id/:token/:codeID/:randomString',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/index.html',
                        controller: 'WebsiteController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.contactus', {
                url: 'contactus',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/contactus.html',
                        controller: 'WebsiteController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            });
    }

    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions', 'authServices'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions, authServices) {
        $rootScope.isLogin = false;
        
        $rootScope.$on('auth:login:success', function (event, data) {
            // $state.go('backoffice.dashboard');
        }); // Event fire after login successfully

        $rootScope.$on('auth:access:denied', function (event, data) {
            // $state.go('auth.login');
        }); //Event fire after check access denied for user

        $rootScope.$on('auth:login:required', function (event, data) {
            // $state.go('auth.login');
        }); //Event fire after logout

        $transitions.onStart({ to: '**' }, function ($transition$) {
            // $rootScope.showBreadcrumb = true;
            // authServices.checkValidUser(true);
            // console.log($rootScope.user);
            //console.log($location.$$path);

        });
    }

}());