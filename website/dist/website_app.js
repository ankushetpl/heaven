(function () {
    'use strict';
    var authApp = angular.module('authApp', []);

    // authenticateUser1.$inject = ['authServices', '$state']

    // function authenticateUser1(authServices, $state) {
    //     return authServices.checkValidUser(false);
    // } //END authenticateUser()

    // authApp.config(function ($stateProvider, $urlRouterProvider) {
    //     $stateProvider
    //         .state('auth', {
    //             url: '/auth',
    //             views: {
    //                 '': {
    //                     templateUrl: 'app/layouts/auth/layout.html'
    //                 },
    //                 'content@auth': {
    //                     templateUrl: 'app/components/auth/login.html',
    //                     controller: 'AuthController',
    //                     controllerAs: 'auth'
    //                 }
    //             },
    //             resolve: {
    //             //    auth1: authenticateUser1
    //             }
    //         })
    //         .state('auth.login', {
    //             url: '/login',
    //             views: {
    //                 'content@auth': {
    //                     templateUrl: 'app/components/auth/login.html',
    //                     controller: 'AuthController',
    //                     controllerAs: 'auth'
    //                 }
    //             },
    //             resolve: {
    //                 auth1: authenticateUser1
    //             }
    //         })
    //         .state('auth.forgotpassword', {
    //             url: '/forgot-password',
    //             views: {
    //                 'content@auth': {
    //                     templateUrl: 'app/components/auth/forgotpassword.html',
    //                     controller: 'AuthController',
    //                     controllerAs: 'auth'
    //                 }
    //             }
    //         });
    // });
})();
(function () {
    "use strict";

    angular
        .module('authApp')
        .service('authServices', authServices);

    authServices.$inject = ['$q', '$http', '$location', '$rootScope', 'APPCONFIG', '$state'];

    var someValue = '';

    function authServices($q, $http, $location, $rootScope, APPCONFIG, $state) {

        self.checkValidUser = checkValidUser;
        
        function checkValidUser(isAuth) {
            var URL = APPCONFIG.APIURL + 'login/validate-user';
            var deferred = $q.defer();
            var requestData = {};
            // requestData['usertype'] = 1;
            requestData['token'] = getAuthToken();
            
            $http({
                method: 'POST',
                url: URL,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                if (response.data.status == 0) {
                    $rootScope.$broadcast('auth:login:required');
                } else {
                    $rootScope.isLogin = true;
                    saveUserInfo(response.data);
                    if (isAuth === false) {
                        $rootScope.$broadcast('auth:login:success');
                    }
                }
                deferred.resolve();
            }).catch(function (response) {
                $rootScope.isLogin = false;
                if (isAuth === false) {
                    deferred.resolve();
                } else {
                    $rootScope.$broadcast('auth:login:required');
                    deferred.resolve();
                }
            });
            return deferred.promise;
        } //END checkValidUser();

        function setAuthToken(userInfo) {
            // localStorage.setItem('token', userInfo.headers('Access-token'));
            localStorage.setItem('token', userInfo);
        } //END setAuthToken();

        function getAuthToken() {
            if (localStorage.getItem('token') != undefined && localStorage.getItem('token') != null)
                return localStorage.getItem('token');
            else
                return null;
        } //END getAuthToken();

        function saveUserInfo(data) {
            var user = {};
            if (data.status == 1) {
                user = data.data;
                $rootScope.user = user;
            }
            return user;
        } //END saveUserInfo();


        return self;
        
    };
})();
(function () {
    'use strict';
    var webSiteApp = angular.module('websiteApp', ['authApp']);

    authenticateUser.$inject = ['authServices', '$state']

    function authenticateUser(authServices, $state) {
        return authServices.checkValidUser(true);
    } //END authenticateUser()

    webSiteApp.config(funConfig);
    webSiteApp.run(funRun);

    webSiteApp.component("headerComponent", {
        templateUrl: 'website/app/layouts/header.html',
        controller: 'WebsiteCommonController',
        controllerAs: 'header'
    });

    webSiteApp.component("footerComponent", {
        templateUrl: 'website/app/layouts/footer.html',
        controller: 'WebsiteCommonController',
        controllerAs: 'footer'
    });

    // App Config
    function funConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('website', {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'website/app/layouts/layout.html'
                    },
                    'content@website': {
                        templateUrl: 'website/app/components/links/index.html',
                        controller: 'WebsiteCommonController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.viewPage', {
                url: ':text',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/commonPage.html',
                        controller: 'WebsiteCommonController',
                        controllerAs: 'website'
                    }
                }
            })
            .state('website.faq', {
                url: 'faq',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/faq.html',
                        controller: 'WebsiteCommonController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.resetPassword', {
                url: 'forgot-password/:id/:code',
                // url: 'forgot-passwords',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/forgotPassword.html',
                        controller: 'WebsiteController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.activation', {
                url: 'activation-token/:id/:token/:codeID/:randomString',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/index.html',
                        controller: 'WebsiteController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            })
            .state('website.contactus', {
                url: 'contactus',
                views: {
                    'content@website': {
                        templateUrl: 'website/app/components/links/contactus.html',
                        controller: 'WebsiteController',
                        controllerAs: 'website'
                    }
                },
                resolve: {
                    auth: authenticateUser
                }
            });
    }

    // App Run
    funRun.$inject = ['$http', '$rootScope', '$state', '$location', '$log', '$transitions', 'authServices'];

    function funRun($http, $rootScope, $state, $location, $log, $transitions, authServices) {
        $rootScope.isLogin = false;
        
        $rootScope.$on('auth:login:success', function (event, data) {
            // $state.go('backoffice.dashboard');
        }); // Event fire after login successfully

        $rootScope.$on('auth:access:denied', function (event, data) {
            // $state.go('auth.login');
        }); //Event fire after check access denied for user

        $rootScope.$on('auth:login:required', function (event, data) {
            // $state.go('auth.login');
        }); //Event fire after logout

        $transitions.onStart({ to: '**' }, function ($transition$) {
            // $rootScope.showBreadcrumb = true;
            // authServices.checkValidUser(true);
            // console.log($rootScope.user);
            //console.log($location.$$path);

        });
    }

}());
(function () {
    "use strict";
    angular.module('websiteApp')
        .controller('WebsiteController', WebsiteController);

    WebsiteController.$inject = ['$scope', '$location', '$state', 'WebsiteService', '$rootScope', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function WebsiteController($scope, $location, $state, WebsiteService, $rootScope, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.isActive = isActive;
        vm.getCurrentState = getCurrentState;
        vm.getSingleWeb = getSingleWeb;
        vm.getPasswordForm = getPasswordForm;
        vm.savePassword = savePassword;
        vm.setActivationToken = setActivationToken;
        vm.saveContact = saveContact;
        vm.resetForm = resetForm;
        vm.topHeader = topHeader;
        
        console.log($state.current.name, $rootScope.headerTitle);

        vm.passwordForm = { user_id : '' };
        $scope.form = {};
        vm.editFlag = false;

        $scope.regex = /^[a-zA-Z0-9]{8,20}[^`~!#@$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
        $scope.regexName = /^[a-zA-Z][^`~!#@$%\^&*()_+={}|[\]\\:;"<>?,./0-9]*$/;

        vm.progressbar = ngProgressFactory.createInstance();
        vm.progressbar.setColor('blue');

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        function getCurrentState() {
            return $state.current.name;
        }

        $rootScope.$on("StartLoader", function () {
            $rootScope.loader = true;
        });

        $rootScope.$on("CloseLoader", function () {
            $rootScope.loader = false;
        });

        /* to extract parameters from url */
        var path = $location.path().split("/");      
        if (path[1] == "forgot-password") {
            if (path[3] == "") {
                $state.go('website');
                return false;
            } else {
                vm.editFlag = true;
                $timeout( function(){
                    vm.getPasswordForm(path[2], path[3]);
                }, 100 );
            }
        }

        if (path[1] == "activation-token") {
            if (path[5] == "") {
                $state.go('website');
                return false;
            } else {
                $timeout( function(){
                    vm.setActivationToken(path[2], path[3], path[4]);
                }, 500 );
            }
        }
        
        function getPasswordForm(id, code){
            var dataForm = { user_id : id, device_token : code };
            
            WebsiteService.getPasswordForm(dataForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.passwordForm.user_id = id;
                        vm.passwordForm.device_token = code;  
                    } else {
                        toastr.error(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getPasswordForm();
        
        function savePassword(){
            vm.isDisabledButton = true;
            vm.progressbar.start();
            WebsiteService.savePassword(vm.passwordForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        toastr.success(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    }
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END savePassword();

        function topHeader(){
            $anchorScroll();
        }

        /* to get single web page */
        function getSingleWeb(page_id) {
            $anchorScroll();
            WebsiteService.getSingleWeb(page_id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.page;                        
                    } else {
                        toastr.error(response.data.message, 'Heavenly Sent');
                        $state.go('website');
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getSingleWeb();

        /* to active account */
        function setActivationToken(id, token, codeID){
            var dataForm = { user_id : id, device_token : token, clients_referID : codeID };
            
            WebsiteService.setActivationToken(dataForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        SweetAlert.swal("Success!", response.data.message, "success");
                        $state.go('website');
                        return false;
                    } else {
                        SweetAlert.swal("Error!", response.data.message, "error");
                        $state.go('website');
                        return false;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        } //End setActivationToken();

        /* save contact details */
        function saveContact(){
            vm.isDisabledButton = true;
            vm.progressbar.start();
            WebsiteService.saveContact(vm.contactForm).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        $anchorScroll();
                        toastr.success(response.data.message, 'Heavenly Sent');
                        $state.reload();
                        return true;
                    } else {
                        vm.progressbar.complete();
                        vm.isDisabledButton = false;
                        toastr.error(response.data.message, 'Heavenly Sent');
                    }
                } else {
                    vm.progressbar.complete();
                    vm.isDisabledButton = false;
                    toastr.error(response.data.message, 'Heavenly Sent');
                }
            }, function (error) {
                vm.progressbar.complete();
                vm.isDisabledButton = false;
                toastr.error('Internal server error', 'Heavenly Sent');
            });
        }//END saveContact();

        /* to reset all parameters */
        function resetForm(){
            vm.contactForm = {};
        }//END resetForm();

    }

}());



(function () {
    "use strict";
    angular.module('websiteApp')
        .controller('WebsiteCommonController', WebsiteCommonController);

    WebsiteCommonController.$inject = ['$scope', '$location', '$state', 'WebsiteService', '$rootScope', 'toastr', 'SweetAlert', '$timeout', 'ngProgressFactory', '$anchorScroll'];

    function WebsiteCommonController($scope, $location, $state, WebsiteService, $rootScope, toastr, SweetAlert, $timeout, ngProgressFactory, $anchorScroll) {
        var vm = this;

        vm.isActive = isActive;
        vm.getSingleWeb = getSingleWeb;
        vm.allPage = allPage;
        vm.getAllRoll = getAllRoll;
        vm.getFAQ = getFAQ;
        vm.getAllData = getAllData; 
        
        $scope.accordion = {
            current: null
        };
        
        $scope.oneAtATime = true;

        console.log($state.current.name, $rootScope.headerTitle);

        vm.allPage();
        vm.getAllData();

        vm.progressbar = ngProgressFactory.createInstance();
        
        /* to extract parameters from url */
        var path = $location.path().split("/");      
        if (path[1] != "") {
            $timeout( function(){
                vm.getSingleWeb(path[1]);
            }, 1000 );
        }

        //to show active links in side bar
        function isActive(route) {
            var active = (route === $location.path());
            return active;
        } //END isActive active menu

        /* to get single web page */
        function getSingleWeb(page_slug) {
            $anchorScroll();
            WebsiteService.getSingleWeb(page_slug).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.singleFaq = response.data.page;                        
                    }                    
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getSingleWeb();

        /* get all page data */
        function allPage(){
            vm.pagelist = [];
            WebsiteService.allPage().then(function (response) {
                if (response.status == 200) {
                    if (response.data.data && response.data.data.length > 0) {
                        vm.pagelist = response.data.data;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END allPage();

        /* all role type */
        function getAllRoll() {
            vm.roleList = [];
            WebsiteService.getAllRoll('1').then(function (response) {
                if (response.status == 200) {
                    if (response.data.roles && response.data.roles.length > 0) {
                        vm.roleList = response.data.roles;
                    }
                }
            }, function (error) {
                toastr.error(error.data.error, 'Error');
            });
        }//END getAllRoll()

        /* get faq data */
        function getFAQ(id){
            vm.allFaq = [];
            WebsiteService.getFAQ(id).then(function (response) {
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.allFaq = response.data.faqs;
                    }                    
                }
            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getFAQ();

        /* get all data */
        function getAllData(){
            vm.logCheck = [];
            
            if(typeof $rootScope.user !== 'undefined' && $rootScope.user !== ''){
                if($rootScope.user.user_role_id == 2 || $rootScope.user.user_role_id == 4 ){
                    vm.logCheck.login = 1;
                    vm.logCheck.URL = 'frontoffice/#!/auth/login';
                }else{
                    vm.logCheck.login = 1;
                    vm.logCheck.URL = 'backoffice/#!/auth/login';
                }
            }else{
                vm.logCheck.login = 0;
                vm.logCheck.URL = 'frontoffice/#!/auth/login';
            }

            WebsiteService.getAllData().then(function (response) {
                
                if (response.status == 200) {
                    if (response.data.status == 1) {
                        vm.headerSide = response.data.setting;
                        vm.socialSide = response.data.social;
                        vm.emailSide = response.data.email;
                        vm.bannerSide = response.data.banner;
                        
                        angular.forEach(vm.bannerSide, function(value, key) {
                            vm.bannerSide[key].file = '';
                            var file_id = value.banner_image;
                            WebsiteService.getImage(file_id).then(function (responses) {
                                if (responses.status == 200) {
                                    if (responses.data.file && responses.data.file != '') {
                                        vm.bannerSide[key].file = responses.data.file.file_base_url;
                                    }
                                }
                            });
                        });    

                        vm.headerSide[4].file = '';
                        var file_id = vm.headerSide[4].setting_value;
                        WebsiteService.getImage(file_id).then(function (responses) {
                            if (responses.status == 200) {
                                if (responses.data.file && responses.data.file != '') {
                                    vm.headerSide[4].file = responses.data.file.file_base_url;
                                }
                            }
                        });

                        vm.getSingleWeb('about');
                    }                    
                }

            }, function (error) {
                toastr.error(error.data.error, 'Heavenly Sent');
            });
        }//END getAllData();
        
    }

}());



(function () {
    "use strict";
    angular
        .module('websiteApp')
        .service('WebsiteService', WebsiteService);

    WebsiteService.$inject = ['$http', 'APPCONFIG', '$q'];

    function WebsiteService($http, APPCONFIG, $q) {

        /* to get single web page */
        function getSingleWeb(page_slug) {
            var URL = APPCONFIG.APIURL + 'website/view';
            var requestData = {};

            if (typeof page_slug !== undefined && page_slug !== '') {
                requestData['page_slug'] = page_slug;
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeb();

        /* to get all web page */
        function allPage() {
            var URL = APPCONFIG.APIURL + 'page/AllPage';
            var requestData = {};

            return this.runHttp(URL, requestData);
        } //END allPage();

        /* to check password link web page */
        function getPasswordForm(dataForm){
            var URL = APPCONFIG.APIURL + 'login/checkForgot';
            var requestData = {};

            if (typeof dataForm !== undefined && dataForm !== '') {
                if (typeof dataForm.user_id !== undefined && dataForm.user_id !== '') {
                    requestData['user_id'] = dataForm.user_id;
                }

                if (typeof dataForm.device_token !== undefined && dataForm.device_token !== '') {
                    requestData['device_token'] = dataForm.device_token;
                }
            }

            return this.runHttp(URL, requestData);
        } //END getSingleWeb();

        /* to save password web page */
        function savePassword(obj){
            var URL = APPCONFIG.APIURL + 'login/savePassword';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.user_id !== 'undefined' && obj.user_id !== '') {
                    requestData['user_id'] = obj.user_id;
                }

                if (typeof obj.device_token !== 'undefined' && obj.device_token !== '') {
                    requestData['device_token'] = obj.device_token;
                }

                if (typeof obj.new_password !== 'undefined' && obj.new_password !== '') {
                    requestData['user_password'] = obj.new_password;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END savePassword();

        /* to active account web page */
        function setActivationToken(dataForm){
            var URL = APPCONFIG.APIURL + 'login/activeAccount';
            var requestData = {};

            if (typeof dataForm !== undefined && dataForm !== '') {
                if (typeof dataForm.user_id !== undefined && dataForm.user_id !== '') {
                    requestData['user_id'] = dataForm.user_id;
                }

                if (typeof dataForm.device_token !== undefined && dataForm.device_token !== '') {
                    requestData['device_token'] = dataForm.device_token;
                }

                if (typeof dataForm.clients_referID !== undefined && dataForm.clients_referID !== '') {
                    requestData['clients_referID'] = dataForm.clients_referID;
                }
            }

            return this.runHttp(URL, requestData);
        } //END setActivationToken();

        /* to save contact web page */
        function saveContact(obj){
            var URL = APPCONFIG.APIURL + 'support/add';

            var requestData = {};
            if (typeof obj !== 'undefined' && obj !== '') {
                if (typeof obj.contactUs_name !== 'undefined' && obj.contactUs_name !== '') {
                    requestData['contactUs_name'] = obj.contactUs_name;
                }

                if (typeof obj.contactUs_from !== 'undefined' && obj.contactUs_from !== '') {
                    requestData['contactUs_from'] = obj.contactUs_from;
                }

                if (typeof obj.contactUs_subject !== 'undefined' && obj.contactUs_subject !== '') {
                    requestData['contactUs_subject'] = obj.contactUs_subject;
                }

                if (typeof obj.contactUs_msg !== 'undefined' && obj.contactUs_msg !== '') {
                    requestData['contactUs_msg'] = obj.contactUs_msg;
                }
            }
            
            return this.runHttp(URL, requestData);
        } //END saveContact();

        /* to get all role */
        function getAllRoll(pageNum) {
            var URL = APPCONFIG.APIURL + 'role';

            var requestData = {}; 

            if (typeof pageNum !== 'undefined' && pageNum !== '') {
                requestData['pageNum'] = pageNum;
            }

            requestData['orderColumn'] = 'role_name'; 
            requestData['orderBy'] = 'ASC';          
            return this.runHttp(URL, requestData);
        }//END getAllRoll();

        /* to get faq by id */
        function getFAQ(id) {
            var URL = APPCONFIG.APIURL + 'faq/allFAQs';

            var requestData = {}; 
            
            if (typeof id !== 'undefined' && id !== '') {
                requestData['role_id'] = id;
            }
           
            return this.runHttp(URL, requestData);
        }//END getFAQ();

        /* get all data */
        function getAllData() {
            var URL = APPCONFIG.APIURL + 'website';

            var requestData = {}; 
                       
            return this.runHttp(URL, requestData);
        }//END getAllData();

        /* to get single image */
        function getImage(file_id) {
            var URL = APPCONFIG.APIURL + 'files/view';
            var requestData = {};

            if (typeof file_id !== undefined && file_id !== '') {
                requestData['file_id'] = file_id;
            }
                        
            return this.runHttp(URL, requestData);
        } //END getImage();

        function runHttp(URL, requestData) {
            var deferred = $q.defer();
            $http({
                url: URL,
                method: "POST",
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return {
            runHttp: runHttp,            
            getSingleWeb: getSingleWeb,
            allPage: allPage,
            getPasswordForm: getPasswordForm,
            savePassword: savePassword,
            setActivationToken: setActivationToken,
            saveContact: saveContact,
            getAllRoll: getAllRoll,
            getFAQ: getFAQ,
            getAllData: getAllData,
            getImage: getImage
        }

    };//END WebsiteService()
}());

"use strict";

var app = angular.module('heavenApp', [
    'ui.router',
    'ui.router.compat',
    'ui.bootstrap',
    'angularValidator',
    'toastr',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angularUtils.directives.dirPagination',
    'ngMaterial',
    'ngFileUpload',
    'ngProgress',    
    'websiteApp'
]);

app.constant('APPCONFIG', {
    'APIURL': 'http://192.168.100.125:3000/' // local
     // 'APIURL': 'http://localhost:3000/' // local
    // 'APIURL': 'http://18.222.244.130:9000/' // testing
});

// app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
//     $urlRouterProvider.otherwise('/');    
// });

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, paginationTemplateProvider) {

    angular.extend(toastrConfig, {
        allowHtml: true,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    // paginationTemplateProvider.setPath('app/layouts/customPagination.tpl.html');

    $urlRouterProvider.otherwise('/');

});
