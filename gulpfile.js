var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css');

// var browserSync = require('browser-sync').create();

// gulp.task('browserSync', function() {
//     browserSync.init({
//        server: {
//           baseDir: 'build'
//        },
//     })
// });

gulp.task('backoffice_vendor_css', function () {
    var backoffice_css_files = [
        './assets/admin/assets/plugins/bootstrap/css/bootstrap.min.css',
        './assets/admin/css/colors/red.css',
        './vendor/angular-toastr/dist/angular-toastr.css',
        './vendor/angucomplete-alt/angucomplete-alt.css',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.css',
        './vendor/angular-material/angular-material.css',
        './vendor/oi.select/dist/select.min.css',
        './vendor/angularjs-datepicker/dist/angular-datepicker.min.css',
        './assets/common/css/sweetalert.css',
        './vendor/ngprogress/ngProgress.css',
        './assets/admin/css/custom.css',
        './assets/common/css/custom.css'
    ]

    return gulp.src(backoffice_css_files)
        .pipe(gp_concat('backoffice_vendor.css'))
        .pipe(gulp.dest('backoffice/dist'));
});

gulp.task('backoffice_vendor_js', function () {
    var backoffice_js_files = [
        './vendor/jquery/dist/jquery.min.js',
        './assets/admin/assets/plugins/bootstrap/js/popper.min.js',
        './assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js',
        './assets/admin/js/jquery.slimscroll.js',
        './assets/admin/js/waves.js',
        './vendor/angular/angular.min.js',
        './vendor/angular-ui-router/release/angular-ui-router.min.js',
        './vendor/angular-sanitize/angular-sanitize.min.js',
        './vendor/angular-toastr/dist/angular-toastr.tpls.js',
        './vendor/tg-angular-validator/dist/angular-validator.min.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/ngSweetAlert/SweetAlert.min.js',
        './assets/common/js/sweetalert.min.js',
        './vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/ng-file-upload/ng-file-upload.min.js',
        './vendor/moment/min/moment-with-locales.js',
        './vendor/lodash/lodash.js',
        './vendor/angucomplete-alt/dist/angucomplete-alt.min.js',
        './vendor/angular-aria/angular-aria.js',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
        './vendor/angular-material/angular-material.js',
        './vendor/oi.select/dist/select-tpls.min.js',
        './vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        './assets/common/js/custom.js',
        './vendor/ngmap/build/scripts/ng-map.min.js',
        './vendor/angular-ckeditor/angular-ckeditor.js',
        './vendor/ng-youtube-embed/build/ng-youtube-embed.min.js',
        './vendor/angularjs-datepicker/dist/angular-datepicker.js',
        './vendor/ngprogress/build/ngprogress.js'
    ]

    return gulp.src(backoffice_js_files)
        .pipe(gp_concat('backoffice_vendor.js'))
        .pipe(gulp.dest('backoffice/dist'));
});

var backOfficePath = './backoffice/app/components/'
gulp.task('backoffice_app_js', function () {
    var backoffice_app_files = [

        backOfficePath + 'auth/auth.module.js',
        backOfficePath + 'auth/auth.service.js',
        backOfficePath + 'auth/auth.controller.js',

        backOfficePath + 'dashboard/dashboard.controller.js',
        backOfficePath + 'dashboard/dashboard.service.js',

        backOfficePath + 'rating/rating.module.js',
        backOfficePath + 'rating/rating.service.js',
        backOfficePath + 'rating/rating.controller.js',

        backOfficePath + 'role/role.module.js',
        backOfficePath + 'role/role.service.js',
        backOfficePath + 'role/role.controller.js',

        backOfficePath + 'cafeUser/cafeUser.module.js',
        backOfficePath + 'cafeUser/cafeUser.service.js',
        backOfficePath + 'cafeUser/cafeUser.controller.js',

        backOfficePath + 'faq/faq.module.js',
        backOfficePath + 'faq/faq.service.js',
        backOfficePath + 'faq/faq.controller.js',

        backOfficePath + 'workerUser/workerUser.module.js',
        backOfficePath + 'workerUser/workerUser.service.js',
        backOfficePath + 'workerUser/workerUser.controller.js',

        backOfficePath + 'assessorsUser/assessorsUser.module.js',
        backOfficePath + 'assessorsUser/assessorsUser.service.js',
        backOfficePath + 'assessorsUser/assessorsUser.controller.js',

        backOfficePath + 'clientUser/clientUser.module.js',
        backOfficePath + 'clientUser/clientUser.service.js',
        backOfficePath + 'clientUser/clientUser.controller.js',

        backOfficePath + 'adminUser/adminUser.module.js',
        backOfficePath + 'adminUser/adminUser.service.js',
        backOfficePath + 'adminUser/adminUser.controller.js',

        backOfficePath + 'leave/leave.module.js',
        backOfficePath + 'leave/leave.service.js',
        backOfficePath + 'leave/leave.controller.js',

        backOfficePath + 'tutorial/tutorial.module.js',
        backOfficePath + 'tutorial/tutorial.service.js',
        backOfficePath + 'tutorial/tutorial.controller.js',

        backOfficePath + 'checklist/checklist.module.js',
        backOfficePath + 'checklist/checklist.service.js',
        backOfficePath + 'checklist/checklist.controller.js', 
        
        backOfficePath + 'skills/skills.module.js',
        backOfficePath + 'skills/skills.service.js',
        backOfficePath + 'skills/skills.controller.js',
        
        backOfficePath + 'fileUpload/fileUpload.module.js',        
        backOfficePath + 'fileUpload/fileUpload.service.js',
        backOfficePath + 'fileUpload/fileUpload.controller.js',
        
        backOfficePath + 'cabBook/suburbs.module.js',
        backOfficePath + 'cabBook/suburbs.service.js',
        backOfficePath + 'cabBook/suburbs.controller.js',

        backOfficePath + 'page/page.module.js',
        backOfficePath + 'page/page.service.js',
        backOfficePath + 'page/page.controller.js',
        
        backOfficePath + 'tasklist/tasklist.module.js',
        backOfficePath + 'tasklist/tasklist.service.js',
        backOfficePath + 'tasklist/tasklist.controller.js', 

        backOfficePath + 'tasklistClient/tasklistClient.module.js',
        backOfficePath + 'tasklistClient/tasklistClient.service.js',
        backOfficePath + 'tasklistClient/tasklistClient.controller.js',

        backOfficePath + 'feedback/feedback.module.js',
        backOfficePath + 'feedback/feedback.service.js',
        backOfficePath + 'feedback/feedback.controller.js',

        backOfficePath + 'claimCategory/claimCategory.module.js',
        backOfficePath + 'claimCategory/claimCategory.service.js',
        backOfficePath + 'claimCategory/claimCategory.controller.js',

        backOfficePath + 'claim/claim.module.js',
        backOfficePath + 'claim/claim.service.js',
        backOfficePath + 'claim/claim.controller.js',

        backOfficePath + 'serviceType/serviceType.module.js',
        backOfficePath + 'serviceType/serviceType.service.js',
        backOfficePath + 'serviceType/serviceType.controller.js',

        backOfficePath + 'weekService/weekService.module.js',
        backOfficePath + 'weekService/weekService.service.js',
        backOfficePath + 'weekService/weekService.controller.js',

        backOfficePath + 'cleaningArea/cleaningArea.module.js',
        backOfficePath + 'cleaningArea/cleaningArea.service.js',
        backOfficePath + 'cleaningArea/cleaningArea.controller.js', 

        backOfficePath + 'amenities/amenities.module.js',
        backOfficePath + 'amenities/amenities.service.js',
        backOfficePath + 'amenities/amenities.controller.js',

        backOfficePath + 'promocode/promocode.module.js',
        backOfficePath + 'promocode/promocode.service.js',
        backOfficePath + 'promocode/promocode.controller.js',

        backOfficePath + 'suburb/suburb.module.js',
        backOfficePath + 'suburb/suburb.service.js',
        backOfficePath + 'suburb/suburb.controller.js',

        backOfficePath + 'email/email.module.js',
        backOfficePath + 'email/email.service.js',
        backOfficePath + 'email/email.controller.js',

        backOfficePath + 'todayBooking/todayBooking.module.js',
        backOfficePath + 'todayBooking/todayBooking.service.js',
        backOfficePath + 'todayBooking/todayBooking.controller.js',

        backOfficePath + 'allBooking/allBooking.module.js',
        backOfficePath + 'allBooking/allBooking.service.js',
        backOfficePath + 'allBooking/allBooking.controller.js',
        backOfficePath + 'allBooking/weeklyBooking.controller.js',

        backOfficePath + 'paymentSuccess/paymentSuccess.module.js',
        backOfficePath + 'paymentSuccess/paymentSuccess.service.js',
        backOfficePath + 'paymentSuccess/paymentSuccess.controller.js',
        backOfficePath + 'paymentSuccess/paymentSuccessWeek.controller.js',

        backOfficePath + 'paymentUnsuccess/paymentUnsuccess.module.js',
        backOfficePath + 'paymentUnsuccess/paymentUnsuccess.service.js',
        backOfficePath + 'paymentUnsuccess/paymentUnsuccess.controller.js',
        backOfficePath + 'paymentUnsuccess/paymentUnsuccessWeek.controller.js',

        backOfficePath + 'paymentCancel/paymentCancel.module.js',
        backOfficePath + 'paymentCancel/paymentCancel.service.js',
        backOfficePath + 'paymentCancel/paymentCancel.controller.js',
        backOfficePath + 'paymentCancel/paymentCancelWeek.controller.js',

        backOfficePath + 'paymentPending/paymentPending.module.js',
        backOfficePath + 'paymentPending/paymentPending.service.js',
        backOfficePath + 'paymentPending/paymentPending.controller.js',
        backOfficePath + 'paymentPending/paymentPendingWeek.controller.js',

        backOfficePath + 'setting/setting.module.js',
        backOfficePath + 'setting/setting.service.js',
        backOfficePath + 'setting/setting.controller.js',

        backOfficePath + 'socialSetting/socialSetting.module.js',
        backOfficePath + 'socialSetting/socialSetting.service.js',
        backOfficePath + 'socialSetting/socialSetting.controller.js',

        backOfficePath + 'emailSetting/emailSetting.module.js',
        backOfficePath + 'emailSetting/emailSetting.service.js',
        backOfficePath + 'emailSetting/emailSetting.controller.js',

        backOfficePath + 'bannerSetting/bannerSetting.module.js',
        backOfficePath + 'bannerSetting/bannerSetting.service.js',
        backOfficePath + 'bannerSetting/bannerSetting.controller.js',

        // backOfficePath + 'modules/modules.module.js',
        // backOfficePath + 'modules/modules.service.js',
        // backOfficePath + 'modules/modules.controller.js',

        // backOfficePath + 'subAdminUser/subAdminUser.module.js',
        // backOfficePath + 'subAdminUser/subAdminUser.service.js',
        // backOfficePath + 'subAdminUser/subAdminUser.controller.js',

        backOfficePath + 'leaveManage/leaveManage.module.js',
        backOfficePath + 'leaveManage/leaveManage.service.js',
        backOfficePath + 'leaveManage/leaveManage.controller.js',

        backOfficePath + 'notification/notification.module.js',
        backOfficePath + 'notification/notification.service.js',
        backOfficePath + 'notification/notification.controller.js',

        backOfficePath + 'suspension/suspension.module.js',
        backOfficePath + 'suspension/suspension.service.js',
        backOfficePath + 'suspension/suspension.controller.js',

        backOfficePath + 'earlyPay/earlyPay.module.js',
        backOfficePath + 'earlyPay/earlyPay.service.js',
        backOfficePath + 'earlyPay/earlyPay.controller.js',

        backOfficePath + 'support/support.module.js',
        backOfficePath + 'support/support.service.js',
        backOfficePath + 'support/support.controller.js',

        backOfficePath + 'buyCredit/buyCredit.module.js',
        backOfficePath + 'buyCredit/buyCredit.service.js',
        backOfficePath + 'buyCredit/buyCredit.controller.js',

        backOfficePath + 'transReport/transReport.module.js',
        backOfficePath + 'transReport/transReport.service.js',
        backOfficePath + 'transReport/transReport.controller.js',

        backOfficePath + 'walletTopUp/walletTopUp.module.js',
        backOfficePath + 'walletTopUp/walletTopUp.service.js',
        backOfficePath + 'walletTopUp/walletTopUp.controller.js',

        backOfficePath + 'paymentRefund/paymentRefund.module.js',
        backOfficePath + 'paymentRefund/paymentRefund.service.js',
        backOfficePath + 'paymentRefund/paymentRefund.controller.js',
        
        backOfficePath + 'backoffice.module.js',
        backOfficePath + 'backoffice.controller.js',
        'backoffice/app.js'
    ]

    return gulp.src(backoffice_app_files)
        .pipe(gp_concat('backoffice_app.js'))
        .pipe(gulp.dest('backoffice/dist'));
});


//frontoffice gulp files start here
gulp.task('frontoffice_vendor_css', function () {
    var frontoffice_css_files = [        
        './vendor/angular-toastr/dist/angular-toastr.css',
        './vendor/angucomplete-alt/angucomplete-alt.css',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.css',
        './vendor/angular-material/angular-material.css',
        './vendor/oi.select/dist/select.min.css',
        './vendor/angularjs-datepicker/dist/angular-datepicker.min.css',
        './vendor/ngprogress/ngProgress.css',
        './vendor/angular-jk-rating-stars/dist/jk-rating-stars.min.css',       // rating js
        './assets/front/calender/css/fullcalendar.min.css',    // fullcalender
        './assets/common/css/sweetalert.css',        
        './assets/front/css/bootstrap.min.css',
        // './assets/front/css/easy-responsive-tabs.css',
        './assets/front/css/media.css',
        './assets/front/css/font-awesome/css/font-awesome.css',
        './assets/front/fonts/stylesheet.css'           
    ]

    return gulp.src(frontoffice_css_files)
        .pipe(gp_concat('frontoffice_vendor_css.css'))
        .pipe(gulp.dest('frontoffice/dist'));
});

gulp.task('frontoffice_vendor_js', function () {
    var frontoffice_js_files = [
        './vendor/jquery/dist/jquery.min.js',
        './vendor/angular/angular.min.js',
        './vendor/angular-ui-router/release/angular-ui-router.min.js',
        './vendor/angular-sanitize/angular-sanitize.min.js',
        './vendor/angular-toastr/dist/angular-toastr.tpls.js',
        './vendor/tg-angular-validator/dist/angular-validator.min.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/ngSweetAlert/SweetAlert.min.js',        
        './vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/ng-file-upload/ng-file-upload.min.js',
        './vendor/moment/min/moment-with-locales.js',
        './vendor/lodash/lodash.js',
        './vendor/angucomplete-alt/dist/angucomplete-alt.min.js',
        './vendor/angular-aria/angular-aria.js',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
        './vendor/angular-material/angular-material.js',
        './vendor/oi.select/dist/select-tpls.min.js',
        './vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',        
        './vendor/angular-password/angular-password.js',
        './vendor/ngmap/build/scripts/ng-map.min.js',
        './vendor/angular-ckeditor/angular-ckeditor.js',
        './vendor/ng-youtube-embed/build/ng-youtube-embed.min.js',
        './vendor/angularjs-datepicker/dist/angular-datepicker.js',
        './vendor/ngprogress/build/ngprogress.js',
        './vendor/angular-jk-rating-stars/dist/jk-rating-stars.min.js',   // rating js
        './vendor/oclazyload/dist/ocLazyLoad.min.js',
        './vendor/oclazyload/dist/ocLazyLoad.require.min.js',
        './vendor/ng-idle/angular-idle.min.js',

        './assets/front/calender/js/moment.min.js',
        './assets/front/calender/js/fullcalendar.min.js',  // fullcalender
        './assets/front/calender/js/angular-fullcalendar.js',  // fullcalender

        './assets/admin/assets/plugins/bootstrap/js/popper.min.js',
        './assets/admin/assets/plugins/bootstrap/js/bootstrap.min.js',
        './assets/admin/js/jquery.slimscroll.js',
        './assets/common/js/sweetalert.min.js',
        './assets/common/js/custom.js',
        './assets/front/js/bootstrap.min.js',
        // './assets/front/js/easy-responsive-tabs.js',
        './assets/front/js/jquery-clock-timepicker.min.js',        
        './assets/front/js/custom.js'        
    ]

    return gulp.src(frontoffice_js_files)
        .pipe(gp_concat('frontoffice_vendor_js.js'))
        .pipe(gulp.dest('frontoffice/dist'));
});

var frontPath = './frontoffice/app/components/'
gulp.task('frontoffice_app_js', function () {
    var frontoffice_app_files = [

        // Login
        frontPath + 'auth/auth.module.js',
        frontPath + 'auth/auth.service.js',
        frontPath + 'auth/auth.controller.js',

        // frontPath + 'dashboard/dashboard.module.js',
        frontPath + 'dashboard/dashboard.controller.js',
        frontPath + 'dashboard/dashboard.service.js',
        
        //Cafe Section
        frontPath + 'cafeUser/cafeUser.module.js',
        frontPath + 'cafeUser/cafeUser.service.js',
        frontPath + 'cafeUser/cafeUser.controller.js',  

        //Assessor Section
        frontPath + 'assessor/assessor.module.js',
        frontPath + 'assessor/assessor.service.js',
        frontPath + 'assessor/assessor.controller.js',        
       
        frontPath + 'frontoffice.module.js',
        frontPath + 'frontoffice.controller.js',
        frontPath + 'frontoffice.service.js',
        'frontoffice/app.js'
    ]

    return gulp.src(frontoffice_app_files)
        .pipe(gp_concat('frontoffice_app.js'))
        .pipe(gulp.dest('frontoffice/dist'));
});

gulp.task('website_vendor_css', function () {
    var website_css_files = [        
        './vendor/angular-toastr/dist/angular-toastr.min.css',
        './vendor/angucomplete-alt/angucomplete-alt.css',
        './vendor/angularjs-datetime-picker/angularjs-datetime-picker.css',
        './vendor/angular-material/angular-material.min.css',
        './vendor/oi.select/dist/select.min.css',
        './vendor/ngprogress/ngProgress.css',
        './assets/common/css/sweetalert.css',
        './assets/website/css/bootstrap.min.css',        
        './assets/website/fonts/stylesheet.css',
        './assets/website/css/font-awesome-4.7.0/css/font-awesome.css',
        './assets/website/css/hover-min.css',
        './assets/website/css/easy-responsive-tabs.css',
        './assets/website/css/style.css',
        './assets/website/css/media.css'
    ]

    return gulp.src(website_css_files)
        .pipe(gp_concat('website_vendor_css.css'))
        .pipe(minifyCSS())
        // .pipe(browserSync.reload({
        //     stream: true
        //  }))
        .pipe(gulp.dest('website/dist'));
});

gulp.task('website_vendor_js', function () {
    var website_js_files = [
        './vendor/jquery/dist/jquery.min.js',
        './vendor/angular/angular.min.js',
        './vendor/angular-ui-router/release/angular-ui-router.min.js',
        './vendor/angular-sanitize/angular-sanitize.min.js',
        './vendor/angular-toastr/dist/angular-toastr.tpls.js',
        './vendor/tg-angular-validator/dist/angular-validator.min.js',
        './vendor/angular-animate/angular-animate.min.js',
        './vendor/angularUtils-pagination/dirPagination.js',
        './vendor/ngSweetAlert/SweetAlert.min.js',
        './vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
        './vendor/ng-file-upload/ng-file-upload.min.js',
        // './vendor/moment/min/moment-with-locales.js',
        // './vendor/lodash/lodash.js',
        // './vendor/angucomplete-alt/dist/angucomplete-alt.min.js',
        './vendor/angular-aria/angular-aria.js',
        // './vendor/angularjs-datetime-picker/angularjs-datetime-picker.js',
        './vendor/angular-material/angular-material.js',
        // './vendor/oi.select/dist/select-tpls.min.js',
        // './vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        // './vendor/ngmap/build/scripts/ng-map.min.js',
        // './vendor/angular-ckeditor/angular-ckeditor.js',
        // './vendor/ng-youtube-embed/build/ng-youtube-embed.min.js',
        './vendor/ngprogress/build/ngprogress.js',
        './assets/common/js/sweetalert.min.js',
        './assets/website/js/bootstrap.min.js',
        './assets/website/js/easy-responsive-tabs.js',
        './assets/website/js/custom.js'
    ]

    return gulp.src(website_js_files)
        .pipe(gp_concat('website_vendor_js.js'))
        .pipe(gulp.dest('website/dist'));
});

var websitePath = './website/app/components/'
gulp.task('website_app_js', function () {
    var website_app_files = [

        // Login
        websitePath + 'auth/auth.module.js',
        websitePath + 'auth/auth.service.js',

        websitePath + 'website.module.js',
        websitePath + 'website.controller.js',
        websitePath + 'websiteCommon.controller.js',
        websitePath + 'website.service.js',
        'app.js'
    ]

    return gulp.src(website_app_files)
        .pipe(gp_concat('website_app.js'))
        .pipe(gulp.dest('website/dist'));
});

gulp.task('default', ['backoffice_vendor_css', 'backoffice_vendor_js', 'backoffice_app_js', 'frontoffice_vendor_css', 'frontoffice_vendor_js', 'frontoffice_app_js', 'website_vendor_css', 'website_vendor_js', 'website_app_js'], function () {
    gulp.watch('backoffice/**/*.*', function () {
        gulp.run('backoffice_app_js');
    });

    gulp.watch('frontoffice/**/*.*', function () {
        gulp.run('frontoffice_app_js');
    });

    gulp.watch('website/**/*.*', function () {
        gulp.run('website_app_js');
    });

    gulp.watch('assets/**/*.*', function () {
        gulp.run('backoffice_vendor_css');
        gulp.run('backoffice_vendor_js');
        gulp.run('frontoffice_vendor_css');
        gulp.run('frontoffice_vendor_js');
        gulp.run('website_vendor_css');
        gulp.run('website_vendor_js');
    });    
});

// gulp.task('default', ['backoffice_vendor_css', 'backoffice_vendor_js', 'backoffice_app_js'], function () {
//     gulp.watch('backoffice/**/*.*', function () {
//         gulp.run('backoffice_app_js');
//     });
//     gulp.watch('assets/**/*.*', function () {
//         gulp.run('backoffice_vendor_css');
//         gulp.run('backoffice_vendor_js');
//     });
// });





